#include <bits/stdc++.h>

using namespace std;

int main (void)
{

#ifdef JUDGE
    freopen ("prepare.in", "rt", stdin);
    freopen ("prepare.out", "wt", stdout);
#endif

    int n;
    int p[101], t[101], *pmin = p, *tmin = t, *pmax = p, *tmax = t;
    int pcnt = 0, tcnt = 0;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf ("%d", p + i);

        pmin = p[i] < *pmin ? p + i : pmin;
        pmax = p[i] > *pmax ? p + i : pmax;
    }

    for (int i = 0; i < n; i++)
    {
        scanf ("%d", t + i);

        tmin = t[i] < *tmin ? t + i : tmin;
        tmax = t[i] > *tmax ? t + i : tmax;
    }

    int ans = 0;

    for (int i = 0; i < n; i++)
    {
        if (p[i] > t[i])
            pcnt++;
        else
            tcnt++;

        ans += max (p[i], t[i]);
    }

    if (tcnt == 0 and pcnt)
        ans = max (ans - *pmin + t[pmin - p], ans - p[tmax - t] + *tmax);

    if (pcnt == 0 and tcnt)
        ans = max (ans - *tmin + p[tmin - t], ans - t[pmax - p] + *pmax);

    printf ("%d\n", ans);

    return 0;
}

