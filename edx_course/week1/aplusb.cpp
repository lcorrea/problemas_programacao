#include <bits/stdc++.h>

using namespace std;

int main (void)
{

#ifdef JUDGE
	freopen("aplusb.in", "rt", stdin);
	freopen("aplusb.out", "wt", stdout);
#endif

	int a, b;

	scanf ("%d %d", &a, &b);

	printf ("%d\n", a + b);

	return 0;
}

