#include <bits/stdc++.h>

using namespace std;

int main (void)
{
#ifdef JUDGE
    freopen ("aplusbb.in", "rt", stdin);
    freopen ("aplusbb.out", "wt", stdout);
#endif

    long long int a, b;

    scanf ("%lld %lld", &a, &b);

    printf ("%lld\n", a + b*b);

    return 0;
}

