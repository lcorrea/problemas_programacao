#include <bits/stdc++.h>

using namespace std;

class BIT
{
    private:
        vector <int> bit, v;
        int n;

    public:
        BIT(int _n, vector <int> &_v)
        {
            n = _n;
            bit.assign(n+1, 0);
            v = _v;
        }

        void update(int i, int val)
        {
            while (i <= n)
            {
                bit[i] += val;
                i += i & -i;
            }
        }

        int query(int i)
        {
            int sum = 0;

            while (i > 0)
            {
                sum += bit[i];
                i -= i & -i;
            }

            return sum;
        }

        int countInversions()
        {
            int inv = 0;
            vector <int> _v = v;

            sort(_v.begin(), _v.end());

            for (int i = 0; i < n; i++)
            {
                auto it = lower_bound(_v.begin(), _v.end(), v[i]);
                int rank = it - _v.begin() + 1;
                v[i] = rank;
            }

            for (int i = n-1; i >= 0; i--)
            {
                inv += query(v[i]-1);
                update(v[i], 1);
            }

            return inv;
        }
};

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {
        vector <int>  v(n);
        for (int i = 0; i < n; i++)
            scanf ("%d", &v[i]);

        BIT bit(n, v);

        puts (bit.countInversions() & 1 ? "Marcelo" : "Carlos");
    }

    return 0;
}

