#include <bits/stdc++.h>

using namespace std;

class ST
{
    private:
        vector <int> st, v, lazy;
        int n;

        void build(int p, int l, int r)
        {
            if (l == r)
            {
                st[p] = v[l];
                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            build(left, l, mid);
            build(right, mid+1, r);

            st[p] = st[left] + st[right];
        }

        void checkNode (int p, int l, int r)
        {
            if (lazy[p])
            {
                if (l != r)
                {
                    int left = p << 1;
                    int right = left | 1;

                    //propagation
                    lazy[left] = lazy[p];
                    lazy[right] = lazy[p];
                }

                st[p] = lazy[p]*(r-l+1);
                lazy[p] = 0;
            }
        }

        int query(int p, int l, int r, int i, int j)
        {
            checkNode(p, l, r);

            if (i > r or j < l) return 0;

            if (i <= l and j >= r) return st[p];

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            int ql = query(left, l, mid, i, j);
            int qr = query(right, mid+1, r, i, j);

            return ql + qr;
        }

        void update(int p, int l, int r, int i, int j, int val)
        {
            checkNode(p, l, r);

            if (i > r or j < l) return;

            if (i <= l and j >= r)
            {
                st[p] = val*(r-l+1);

                if (l != r)
                {
                    int left = p << 1;
                    int right = left | 1;

                    lazy[left] = val;
                    lazy[right] = val;
                }

                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid+1, r, i, j, val);

            st[p] = st[left] + st[right];
        }

    public:
        ST(int _n, vector <int> &_v)
        {
            v = _v;
            n = _n;

            st.assign(n << 2, 0);
            lazy.assign(n << 2, 0);

            build(1, 0, n-1);
        }

        int query(int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }

        void update(int i, int j, int val)
        {
            update(1, 0, n-1, i, j, val);
        }
};

int main (void)
{
    int n, q;

    scanf ("%d %d", &n, &q);

    vector <int> caixa(n);

    for (int i = 0; i < n; i++)
        scanf ("%d", &caixa[i]);

    ST segTree(n, caixa);

    for (int k = 0; k < q; k++)
    {
        int op, i, j, v;

        scanf ("%d", &op);

        if (op == 1)
        {
            scanf ("%d %d %d", &i, &j, &v);
            segTree.update(i-1, j-1, v);
        }
        else
        {
            scanf ("%d %d", &i, &j);
            printf ("%d\n", segTree.query(i-1, j-1));
        }
    }

    return 0;
}

