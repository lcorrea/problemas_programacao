#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int e[100001], d[100001], v[100001];
    int n;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i);

    int me = v[0];
    int md = v[n-1];

    for (int i = 1; i < n; i++)
    {
        e[i] = v[i] < me;
        me = max(v[i], me);
    }

    for (int i = n-2; i >= 0; i--)
    {
        d[i] = v[i] < md;
        md = max(v[i], md);
    }

    int cnt = 0;
    for (int i = 0; i < n; i++)
        cnt += d[i] and e[i];

    printf ("%d\n", cnt);

    return 0;
}

