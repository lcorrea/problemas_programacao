#include <bits/stdc++.h>

using namespace std;

int get(int n)
{
    int cnt = 1;
    bitset <12000000> bs;

    bs.set();

    for (int i = 2; i < 12000000; i++)
        if (bs[i])
        {
            if (cnt == n)
            {
                cnt = i;
                break;
            }

            cnt++;

            for (int j = i*2; j < 12000000; j+=i)
                bs[j] = false;
        }

    return cnt;
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    printf ("%d\n", get(n));

    return 0;
}

