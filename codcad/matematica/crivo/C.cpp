#include <bits/stdc++.h>

using namespace std;

int g[1500000];

void crivo()
{
    int cnt = 1;
    bitset <1500000> bs;
    
    bs.set();

    for (int i = 2; i < 1500000; i++)
        if (bs[i])
        {
            g[i] = cnt++;

            for (int j = i*2; j < 1500000; j+=i)
                bs[j] = false;
        }
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    crivo();

    while (n--)
    {
        int p;

        scanf ("%d", &p);
        printf ("%d\n", g[p]);
    }

    return 0;
}

