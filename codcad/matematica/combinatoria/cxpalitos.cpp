#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long ull;

int main (void)
{
    int n, m;
    
    scanf ("%d %d",&n, &m);

    if (ceil(n / (double) m) <= 3)
    {
         ull x = min(m, n-2);
         ull med = (n - x) / 2;
         ull t = med - (med > 1 ? 1 : 0);
         ull q = x - t + 1;

         ull y = min(x, (ull) ceil(n/2.0));
         ull qmax = n - y;
         ull qmin = n - x;
         ull qtd = qmax - qmin + 1LL;
         ull hmax = ceil(qmax/2.0);
         ull hmin = qmin/2;
         ull j = hmax - hmin;
         ull sm = ((1LL + (j + 1)) * qtd) / 2;
         ull ans = 3LL * qtd * sm;
         
         printf("%lld\n", qmin);
         printf("%lld\n", qmax);
         printf("%lld\n", qtd);
         printf("%lld\n", j);
         printf("%lld\n", ans);
    }
    else
        puts("0");

    return 0;
}
