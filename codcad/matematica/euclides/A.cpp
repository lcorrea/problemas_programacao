#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, r = 0;

    scanf ("%d", &n);

    for (int i = 0, a; i < n; i++)
        scanf ("%d", &a), r = __gcd(r, a);

    printf ("%d\n", r);

    return 0;
}

