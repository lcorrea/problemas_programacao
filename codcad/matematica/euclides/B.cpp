#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

int main (void)
{
    ull a, b, c, d;

    scanf ("%llu %llu %llu %llu", &a, &b, &c, &d);

    ull r1 = __gcd(a, b);
    ull r2 = __gcd(c, d);

    a /= r1; b /= r1;
    c /= r2; d /= r2;

    a = d*a + c*b;
    b = d*b;

    ull r3 = __gcd(a, b);

    printf ("%llu %llu\n", a/r3, b/r3);

    return 0;
}

