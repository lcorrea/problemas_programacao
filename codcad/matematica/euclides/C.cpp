#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

int main (void)
{
    ull a, b;

    scanf ("%llu %llu", &a, &b);

    printf ("%llu\n", a*b/__gcd(a, b));

    return 0;
}

