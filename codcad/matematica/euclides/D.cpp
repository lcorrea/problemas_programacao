#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    unsigned long long int n, m;

    scanf ("%llu %llu", &n, &m);

    while(__gcd(n, m) != 1)
        m--;

    printf ("%llu\n", m);

    return 0;
}

