#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    unsigned int orr = 0;
    unsigned int n,x;

    scanf ("%u", &n);

    for (int i = 0; i < n; i++)
        scanf ("%u", &x), orr |= x;

    printf ("%u\n", orr);

    return 0;
}

