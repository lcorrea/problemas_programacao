#include <bits/stdc++.h>

using namespace std;

void crivo(int n)
{
    if (n < 2) return;
    
    bitset<100001> bs;
    
    bs.set();
    
    for (int i = 2; i <= n; i++)
        if (bs[i])
            for (int j = i*2; j <= n; j+=i)
                bs[j] = false;

    printf("2");
    for (int i = 3; i <= n; i++)
        if (bs[i])
            printf (" %d", i);
    putchar('\n');
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    crivo(n);

    return 0;
}

