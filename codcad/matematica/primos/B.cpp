#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

bool composto(ull n)
{
    if (n == 1)
        return false;

    if (n % 2 == 0)
        return true;

    for (ull i = 3, limt = sqrt(n)+1; i < limt; i+=2)
        if (n % i == 0)
            return true;

    return false;
}

int main (void)
{
    ull n;

    scanf ("%llu", &n);

    puts (composto(n) ? "S" : "N");

    return 0;
}

