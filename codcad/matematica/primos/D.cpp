#include <bits/stdc++.h>

#define maxN 1000000000

using namespace std;

int main (void)
{
    int a, b, c, d, ans = maxN;

    scanf ("%d %d %d %d", &a, &b, &c, &d);

    for (int i = 1, maxDivNum = sqrt(c); i <= maxDivNum; i++) {
        
        if (c % i == 0)
        {
            int n = c / i;

            if (i % a == 0 and i % b and d % i)
                ans = min (ans, i);

            if (n % a == 0 and n % b and d % n)
                ans = min (ans, n);
        }
    }

    printf ("%d\n", ans == maxN ? -1 : ans);

    return 0;
}

