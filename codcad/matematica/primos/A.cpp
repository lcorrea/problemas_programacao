#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

bool primo(ull n)
{
    if(!(n & 1) and n != 2ull)
        return false;

    if (n == 2)
        return true;

    for (int i = 3, lim = sqrt(n)+1; i < lim; i+=2)
        if (n % i == 0)
            return false;
    
    return true;
}

int main (void)
{
    ull n;

    scanf ("%llu", &n);

    puts (n !=1 and primo(n) ? "S" : "N");

    return 0;
}

