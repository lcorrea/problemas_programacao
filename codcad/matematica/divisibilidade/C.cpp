#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[100001];

    scanf ("%s", str);

    int sum = 0;

    for (int i = 0, x = 1; str[i]; i++, x *= -1)
        sum += (str[i] - '0')*x;

    if (sum % 11)
        puts ("N");
    else
        puts ("S");

    return 0;
}

