#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[100001];

    scanf ("%s", str);

    int len = strlen(str);
    int last = (len == 1 and ((str[0] - '0')%4==0)) ? 4 : -1;
    int sum = 0;

    if (len > 1)
        last = (str[len-1] - '0') + 10*(str[len-2] - '0');

    for (int i = 0; str[i]; i++)
        sum += str[i] - '0';

    if (sum == 0)
    {
        puts ("S\nS\nS");
        return 0;
    }

    if (last % 4 == 0)
        puts ("S");
    else
        puts ("N");

    if (sum % 9 == 0)
        puts ("S");
    else
        puts ("N");

    if (last % 25 == 0)
        puts ("S");
    else
        puts ("N");

    return 0;
}

