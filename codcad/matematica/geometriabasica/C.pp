#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int lados[3];

    for (int i = 0; i < 3; i++)
        scanf ("%d", lados+i);
 
    sort(lados, lados+3);

    if (lados[2] < lados[1] + lados[0])
    {
        int c = lados[2]*lados[2];
        int s = lados[0]*lados[0] + lados[1]*lados[1];

        if (c == s)
            puts ("r");
        else if (c > s)
            puts ("o");
        else
            puts ("a");
    }
    else
        puts ("n");

    return 0;
}

