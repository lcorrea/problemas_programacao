#include <bits/stdc++.h>

using namespace std;

//#1 - descobrir menor caminho de c-1 ateh qualquer outro vertice
//  1.1 - guardar menores caminhos em minC[]
//#2 - descobrir menor caminho de k ateh qualquer outro vertice.
//  2.1 - se em qualquer cidade u visitada for uma cidade da rota de servico
//  entao d[c-1] = min(d[u] + minC[u], d[c-1])

typedef vector < vector < pair <int, int> > > Graph;

int minC[255];
int dist[255];
int n, m, c, k;

void dijkstra(int v, int graph[255][255], int d[255])
{
    priority_queue < pair <int, int> > pq;
    
    for (int i = 0; i < 255; i++)
        d[i] = INT_MAX;

    d[v] = 0;
    pq.push(make_pair(d[v], v));

    while(!pq.empty())
    {
        int s = pq.top().second;
        int ds = -pq.top().first;

        pq.pop();

        if (s <= c-1)
        {
            d[c-1] = min(d[c-1], ds + minC[s]);
            continue;
        }

        for (int t = 0; t < n; t++)
        {
            if (graph[s][t] == INT_MAX)
                continue;

            int dt = ds + graph[s][t];

            if (dt + 1 <= d[t])
            {
                d[t] = dt;
                pq.push(make_pair(-dt, t));
            }
        }
    }
}

int main (void)
{
    while (scanf ("%d %d %d %d", &n, &m, &c, &k), n)
    {
        int graph[255][255];

        for (int i = 0; i < 255; i++)
            for (int j = 0; j < 255; j++)
                graph[i][j] = INT_MAX;

        for (int i = 0, u, v, p; i < m; i++)
        {
            scanf ("%d %d %d", &u, &v, &p);
            graph[u][v] = graph[v][u] = p;
        }
        
        minC[c-1] = 0;
        for(int i = c-2; i >= 0; i--)
            minC[i] = minC[i+1] + graph[i][i+1];
        
        dijkstra(k, graph, dist);

        printf ("%d\n", dist[c-1]);
    }

    return 0;
}

