#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

int main (void)
{
    int n, m;
    Graph graph;
    vector <int> tasks, in;

    scanf ("%d %d", &n, &m);

    graph.resize(n);
    in.assign(n, 0);

    for (int i = 0, u, v; i < m; i++)
    {
        scanf ("%d %d", &u, &v);
        graph[u].push_back(v);
        in[v]++;
    }

    priority_queue <int, vector <int>, greater<int> > pq;

    for (int i = 0; i < n; i++)
        if (in[i] == 0)
            pq.push(i);

    while (!pq.empty())
    {
        int u = pq.top(); pq.pop();
        
        if (in[u] < 0)
            break;

        tasks.push_back(u);
        for (int i = 0; i < (int) graph[u].size(); i++)
        {
            int v = graph[u][i];
            
            if (--in[v] == 0)
                pq.push(v);
        }
    }

    if ((int) tasks.size() < n)
        puts ("*");
    else
        for (int i = 0; i < n; ++i)
            printf ("%d\n", tasks[i]);

    return 0;
}

