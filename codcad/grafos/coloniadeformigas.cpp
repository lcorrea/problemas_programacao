#include <bits/stdc++.h>

using namespace std;

typedef vector < vector < pair <int, int> > > Graph;

Graph graph;
vector <int> pre_lca, parent, level, dist_section, dist_parent;
int sqrtn, tree_height;

void pre_processing_tree(int node, int lvl)
{
    level[node] = lvl;
    tree_height = max(tree_height, lvl);

    for (int i = 0; i < (int) graph[node].size(); i++)
    {
        int v = graph[node][i].first;
        int d = graph[node][i].second;

        if (parent[v] == -1)
        {
            dist_parent[v] = d;
            parent[v] = node;
            pre_processing_tree(v, lvl+1);
        }
    }
}

void pre_processing_lca(int node)
{
    if (level[node] < sqrtn)
        pre_lca[node] = 0;
    else
        if ((level[node] % sqrtn) == 0)
            pre_lca[node] = parent[node];
        else
            pre_lca[node] = pre_lca[parent[node]];

    if ((level[node] % sqrtn) == 0)
        dist_section[node] = dist_parent[node];
    else
        dist_section[node] = dist_parent[node] + dist_section[parent[node]];

    for (int i = 0; i < (int) graph[node].size(); i++)
    {
        int v = graph[node][i].first;

        if (pre_lca[v] == -1)
            pre_processing_lca(v);
    }
}

//calcula lca em sqrt(n)

long long int lca(int u, int v)
{
    long long int sum = 0;
    while (pre_lca[u] != pre_lca[v])
        if (level[u] > level[v])
        {
            sum += dist_section[u];
            u = pre_lca[u];
        }
        else
        {
            sum += dist_section[v];
            v = pre_lca[v];
        }

    while (u != v)
        if (level[u] > level[v])
        {
            sum += dist_parent[u];
            u = parent[u];
        }
        else
        {
            sum += dist_parent[v];
            v = parent[v];
        }

    return sum;
}

int main (void)
{
    int n;

    while(scanf("%d", &n), n)
    {
        graph.clear();
        graph.resize(n);
        level.assign(n, 0);
        dist_section.assign(n, 0);
        dist_parent.assign(n, 0);
        parent.assign(n, -1);
        pre_lca.assign(n, -1);

        for (int i = 1, l, v; i <= n-1; i++)
        {
            scanf ("%d %d", &v, &l);

            graph[i].push_back(make_pair(v, l));
            graph[v].push_back(make_pair(i, l));
        }

        parent[0] = dist_parent[0] = 0;
        pre_processing_tree(0, 0);
        sqrtn = sqrt(tree_height);

        int q, u, v;

        scanf ("%d", &q);
        scanf ("%d %d", &u, &v);

        printf ("%lld", lca(u, v));
        for (int i = 2; i <= q; i++)
        {
            scanf ("%d %d", &u, &v);
            printf (" %lld", lca(u, v));
        }
        putchar('\n');
    }

    return 0;
}

