#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

Graph graph;
int N, M, I;
int k[501];
int in[501];
int v[501];
int y[501];

void update(int a, int b)
{
    swap(k[v[a]], k[v[b]]);
    swap(v[a], v[b]);
}

int dfs(int u, int mink)
{
    mink = min(mink, k[u]);

    for (int i = 0; i < (int) graph[u].size(); i++)
        if (y[graph[u][i]] < 0)
            mink = min(mink, dfs(graph[u][i], mink));

    return y[u] = mink;
}

void query(int a)
{
    int mink = INT_MAX;
    
    memset(y, -1, sizeof(y));

    for (int i = 0; i < (int) graph[v[a]].size(); i++)
        if (y[graph[v[a]][i]] < 0)
            mink = min(mink, dfs(graph[v[a]][i], INT_MAX));

    y[v[a]] = mink;

    if (mink == INT_MAX) puts ("*");
    else printf ("%d\n", mink);
}

int main (void)
{
    while (scanf ("%d %d %d%*c", &N, &M, &I) != EOF)
    {
        graph.clear();
        graph.resize(N+1);

        for (int i = 1; i <= N; i++)
            scanf ("%d%*c", k + i), v[i] = i;
        
        for (int i = 0, a, b; i < M; i++)
        {
            scanf ("%d %d%*c", &a, &b);

            graph[b].push_back(a);
        }

        for (int i = 0; i < I; i++)
        {
            char op;
            int a, b;

            scanf ("%c%*c", &op);

            if (op == 'T')
            {
                scanf ("%d %d%*c", &a, &b);
                update(a, b);
            }
            else
            {
                scanf ("%d%*c", &a);
                query(a);
            }
        }
    }

    return 0;
}

