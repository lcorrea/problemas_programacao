#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

Graph graph(1001);
int v[1001];

void dfs(int u)
{
    v[u] = 1;

    for (int i = 0; i < (int) graph[u].size(); i++)
        if (!v[graph[u][i]])
            dfs(graph[u][i]);
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    for (int i = 0; i < m; i++)
    {
        int a, b;

        scanf ("%d %d", &a, &b);

        graph[a].push_back(b);
        graph[b].push_back(a);
    }

    int cnt = 0;
    for (int i = 1; i <= n; i++)
        if (!v[i])
            dfs(i), cnt++;

    printf ("%d\n", cnt);

    return 0;
}

