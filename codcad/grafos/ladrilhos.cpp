#include <bits/stdc++.h>

using namespace std;

const int dy[4] = {0, 0, 1, -1};
const int dx[4] = {-1, 1, 0, 0};

int mosaico[202][202];

int dfs (int i, int j)
{
    int cnt = 1;
    int t = mosaico[i][j];
    mosaico[i][j] = -1;

    for (int k = 0; k < 4; k++)
        if (mosaico[i+dy[k]][j+dx[k]] == t)
            cnt += dfs(i+dy[k], j+dx[k]);

    return cnt;
}

int main (void)
{
    int h, l;

    memset (mosaico, -1, sizeof(mosaico));

    scanf ("%d %d", &h, &l);

    for (int i = 1; i <= h; i++)
        for (int j = 1; j <= l; j++)
            scanf ("%d", &mosaico[i][j]);

    int minCnt = INT_MAX;

    for(int i = 1; i <= h; i++)
        for (int j = 1; j <= l; j++)
            if (mosaico[i][j] >= 0)
                minCnt = min(minCnt, dfs(i, j));

    printf ("%d\n", minCnt);

    return 0;
}

