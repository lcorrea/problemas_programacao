#include <bits/stdc++.h>

using namespace std;

typedef vector < vector < pair <int, int> > > Graph;

int dijkstra(int v, Graph &graph)
{
    int d[graph.size()];
    priority_queue < pair <int, int> > pq;

    for (int i = 0; i < (int) graph.size(); i++)
        d[i] = INT_MAX;

    d[v] = 0;
    pq.push(make_pair(d[v], v));
    
    while (!pq.empty())
    {
        int u = pq.top().second;
        int s = -pq.top().first;
        
        pq.pop();

        if (u == (int) graph.size() - 1) break;

        for (int i = 0; i < (int) graph[u].size(); i++)
        {
            int w = graph[u][i].first;
            int ds = s + graph[u][i].second;

            if (ds + 1 <= d[w])
            {
                d[w] = ds;
                pq.push(make_pair(-ds, w));
            }
        }
    }

    return d[(int) graph.size()-1];
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    Graph graph(n+2);

    for (int i = 0, s, t, b; i < m; i++)
    {
        scanf ("%d %d %d", &s, &t, &b);

        graph[s].push_back(make_pair(t, b));
        graph[t].push_back(make_pair(s, b));
    }
    
    printf ("%d\n", dijkstra(0, graph));

    return 0;
}

