#include <bits/stdc++.h>

using namespace std;

int mapa[102][102], n;
const int dx[4] = {0, 0, -1, 1};
const int dy[4] = {-1, 1, 0, 0};

int bfs(int i, int j)
{
    int d[102][102];
    priority_queue< pair<int, pair <int, int> > > pq;

    for (int k = 0; k <= 101; k++)
        for (int l = 0; l <= 101; l++)
            d[k][l] = INT_MAX;

    d[i][j] = mapa[i][j];
    pq.push(make_pair(-d[i][j], make_pair(i, j)));

    while (!pq.empty())
    {
        int s = -pq.top().first;
        int x = pq.top().second.second, y = pq.top().second.first;

        pq.pop();

        //printf ("# %d %d\n", y, x);
        for (int k = 0; k < 4; k++)
        {
            int newi = dy[k]+y;
            int newj = dx[k]+x;
            
            //printf ("\t%d %d %d\n", newi, newj, d[newi][newj]);

            if (mapa[newi][newj] >= 0)
            {
                int ds = s + mapa[newi][newj];

                if (ds + 1 <= d[newi][newj])
                {
                    d[newi][newj] = ds;
                    pq.push(make_pair(-ds, make_pair(newi, newj)));
                }
            }
        }
    }

    return d[n][n];
}


int main (void)
{
    memset (mapa, -1, sizeof(mapa));

    scanf ("%d", &n);

    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n; j++)
            scanf ("%d", &mapa[i][j]);

    printf ("%d\n", bfs(1, 1));

    return 0;
}

