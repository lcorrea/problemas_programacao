#include <bits/stdc++.h>

using namespace std;

char mapa[1002][1002];

bool calc(int i, int j)
{
    const int dx[4] = {0, 0, 1, -1};
    const int dy[4] = {1, -1, 0, 0};
    bool x = 0;

    for (int k = 0; k < 4 and !x; k++)
        x = mapa[i+dy[k]][j+dx[k]] == '.';

    return x;
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    for (int i = 0; i <= n+1; i++)
        for (int j = 0; j <= m+1; j++)
            mapa[i][j] = '.';

    for (int i = 1; i <= n; i++)
    {
        getchar();
        for (int j = 1; j <= m; j++)
            scanf ("%c", &mapa[i][j]);
    }

    int cnt = 0;
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            cnt += mapa[i][j] == '#' and calc(i, j);

    printf ("%d\n", cnt);

    return 0;
}

