#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

Graph graph;

vector <int> pre_lca, parent, level;
int sqrtn, tree_height = 0;

int pre_processing_tree (int lvl, int node)
{
    level[node] = lvl;
    tree_height = max(lvl, tree_height);

    for (int i = 0; i < (int) graph[node].size(); i++)
    {
        int v = graph[node][i];

        if (parent[v] == -1)
        {
            parent[v] = node;
            pre_processing_tree(lvl+1, v);
        }
    }
}

void pre_processing_lca (int u)
{
    if (level[u] < sqrtn)
       pre_lca[u] = 0;
    else
        if ((level[u] % sqrtn) == 0) // begin of section
            pre_lca[u] = parent[u];
        else
            pre_lca[u] = pre_lca[parent[u]];

    for (int i = 0; i < (int) graph[u].size(); i++)
        if (pre_lca[graph[u][i]] == -1)
            pre_processing_lca(graph[u][i]);
}

int lca(int u, int v)
{
    while (pre_lca[u] != pre_lca[v])
        if (level[u] > level[v])
            u = pre_lca[u];
        else
            v = pre_lca[v];

    while (u != v)
        if (level[u] > level[v])
            u = parent[u];
        else
            v = parent[v];

    return u;
}

int main (void)
{
    int n;
    int cartas[50001][2];

    scanf ("%d", &n);

    graph.resize(n);
    level.resize(n);
    pre_lca.assign(n, -1);
    parent.assign(n, -1);
    memset(cartas, -1, sizeof(cartas));
    
    for (int i = 0, c; i < n; i++)
    {
        scanf ("%d", &c);

        if (cartas[c][0] == -1)
            cartas[c][0] = i;
        else
            cartas[c][1] = i;
    }


    for (int i = 1, a, b; i <= n-1; i++)
    {
        scanf ("%d %d", &a, &b);

        graph[a-1].push_back(b-1);
        graph[b-1].push_back(a-1);
    }
    
    parent[0] = 0;
    pre_processing_tree(0, 0);
    sqrtn = sqrt(tree_height);

   // for (int i = 0; i < n; i++)
   //     printf ("%d => lvl: %d "
   //             "parent: %d "
   //             "carta: %d\n", i+1, level[i], parent[i]+1, cartas[i]);
   // printf ("tree high: %d\n", tree_height);

    pre_processing_lca(0);
    
    long long int sum = 0;
    //calcular lca em O(sqrt(h)), onde h eh a altura da arvore
    for (int i = 1; i <= n/2; i++)
    {
        int u = cartas[i][0];
        int v = cartas[i][1];
        int x = lca(cartas[i][0], cartas[i][1]);

        /*printf ("carta %d (%d, %d) = %d\n", 
                i, cartas[i][0]+1, cartas[i][1]+1, x+1);*/
        sum += level[u] + level[v] - 2*level[x];
    }

    printf ("%lld\n", sum);

    
    return 0;
}

