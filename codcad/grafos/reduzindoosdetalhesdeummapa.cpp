#include <bits/stdc++.h>

using namespace std;

typedef vector < pair <int, pair <int, int> > > Graph;

class UnionFind
{
    private:
        vector <int> p, h;
        int n;

    public:
        UnionFind (int _n)
        {
            n = _n;

            h.assign(n, 0);
            p.resize(n);

            for (int i = 0; i < n; i++)
                p[i] = i;
        }

        int find (int i)
        {
            return i == p[i] ? i : p[i] = find(p[i]);
        }

        void join (int i, int j)
        {
            if (find(i) == find(j)) return;

            i = find(i); j = find(j);

            if (h[i] < h[j])
                p[i] = j;
            else
            {
                p[j] = i;
                h[i] += h[i] == h[j];
            }
        }
};

int kruskal(Graph &graph, int n)
{
    UnionFind unionFind(n);

    sort(graph.begin(), graph.end());
    
    int minCost = 0;
    
    for (int i = 0; i < (int) graph.size(); i++)
    {
        int u = graph[i].second.first;
        int v = graph[i].second.second;
        int d = graph[i].first;

        if (unionFind.find(u) != unionFind.find(v))
            minCost += d, unionFind.join(u, v);
    }

    return minCost;
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    Graph graph;
    for (int i = 0, u, v, p; i < m; i++)
    {
        scanf ("%d %d %d", &u, &v, &p);

        graph.push_back(make_pair(p, make_pair(u-1, v-1)));
        //graph.push_back(make_pair(p, make_pair(v-1, u-1)));
    }

    printf ("%d\n", kruskal(graph, n));

    return 0;
}

