#include <bits/stdc++.h>

using namespace std;

typedef vector < vector < pair <int, int> > > Graph;

int prim(Graph &graph)
{
    int dist[graph.size()], vis[graph.size()];
    priority_queue < pair <int, int> > pq;

    for (int i = 0; i < (int) graph.size(); i++)
        dist[i] = INT_MAX, vis[i] = 0;

    dist[0] = 0; vis[0] = 1;
    pq.push(make_pair(dist[0], 0));

    while (!pq.empty())
    {
        int u = pq.top().second;
        int d = -pq.top().first;

        vis[u] = 1;
        pq.pop();

        for (int i = 0; i < (int) graph[u].size(); i++)
        {
            int v = graph[u][i].first;
            int duv = graph[u][i].second;

            if (duv < dist[v] and !vis[v])
            {
                dist[v] = duv;
                pq.push(make_pair(-dist[v], v));
            }
        }
    }

    int minCost = 0;

    for (int i = 0; i < (int) graph.size(); i++)
        minCost += dist[i];

    return minCost;
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    Graph graph(n);

    for (int i = 0, u, v, p; i < m; i++)
    {
        scanf ("%d %d %d", &u, &v, &p);

        graph[u].push_back(make_pair(v, p));
        graph[v].push_back(make_pair(u, p));
    }

    printf ("%d\n", prim(graph));

    return 0;
}

