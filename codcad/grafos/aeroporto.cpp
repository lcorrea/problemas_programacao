#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

int main (void)
{
    int a, v, teste = 1;

    while (scanf ("%d %d", &a, &v), a != 0)
    {
        int maxV = 0;
        Graph graph(a+1);

        for (int i = 0; i < v; i++)
        {
            int u, v;

            scanf ("%d %d", &u, &v);

            graph[u].push_back(v);
            graph[v].push_back(u);

            maxV = max(maxV, (int) max(graph[u].size(), graph[v].size()));
        }

        printf ("Teste %d\n", teste++);
        for (int i = 1, cnt = 0; i <= a; i++)
        {
            if (graph[i].size() == maxV)
                if (cnt == 0)
                    printf ("%d", i), cnt++;
                else
                    printf (" %d", i);
        }
        puts("\n");

    }

    return 0;
}

