#include <bits/stdc++.h>

using namespace std;

#define INF 1000000000

int main (void)
{
    int n, m;
    int adjMat[200][200];

    scanf ("%d %d", &n, &m);

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            adjMat[i][j] = i == j ? 0 : INF;

    for (int i = 0, u, v, w; i < m; i++)
    {
        scanf ("%d %d %d", &u, &v, &w);
        
        //u--; v--;
        w = min(adjMat[u][v], w);
        adjMat[u][v] = adjMat[v][u] = w;
    }

    for (int k = 0; k < n; k++)
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                adjMat[i][j] = min (adjMat[i][j], adjMat[i][k] + adjMat[k][j]);

    int ans = INF;
    for (int i = 0; i < n; i++)
    {
        int max_dist = 0;
        for (int j = 0; j < n; j++)
            max_dist = max(max_dist, adjMat[i][j]);

        ans = min(ans, max_dist);
    }

    printf ("%d\n", ans);

    return 0;
}

