#include <bits/stdc++.h>

using namespace std;

typedef vector < unordered_map <int, int > > Graph;

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    Graph graph(n+1);

    for (int i = 0; i < m; i++)
    {
        int a, b, c;

        scanf ("%d %d %d", &a, &b, &c);

        if (a == 1)
            graph[c][b] = graph[b][c] = 1;
        else
            puts (graph[c][b] ? "1" : "0");
    }

    return 0;
}

