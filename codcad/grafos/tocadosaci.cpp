#include <bits/stdc++.h>

using namespace std;

const int dx[4] = {0, 0, -1, 1};
const int dy[4] = {-1, 1, 0, 0};

int mapa[1002][1002];

int dfs (int i, int j)
{
    if (mapa[i][j] == 3)
        return 1;

    int cnt = 0;
    mapa[i][j] = 0;
    for (int k = 0; k < 4; k++)
        if (mapa[i+dy[k]][j+dx[k]])
            if (cnt = dfs(i+dy[k], j+dx[k]))
                return 1 + cnt;
    return 0;
}

int main (void)
{
    int n, m, x, y;

    scanf ("%d %d", &n, &m);

    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
        {
            scanf ("%d", &mapa[i][j]);

            if (mapa[i][j] == 2)
                x = j, y = i;
        }

    printf ("%d\n", dfs(y, x));

    return 0;
}

