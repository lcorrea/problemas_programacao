#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

Graph graph(10001);
int mLvl = 0;
int cntLvl[10001];
int pLvl[10001];
int p[10001];

void dfs(int u, int lvl)
{
    mLvl = max(mLvl, lvl);
    cntLvl[lvl]++;
    pLvl[lvl] += p[u];

    for(int i = 0; i < (int) graph[u].size(); i++)
        dfs(graph[u][i], lvl+1);
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    for (int i = 1, x; i <= n; i++)
    {
        scanf ("%d", &x);

        graph[x].push_back(i);
    }

    for (int i = 0, x; i < m; i++)
        scanf ("%d", &x), p[x] = 1;
    
    dfs(0, 0);

    for (int lvl = 1; lvl <= mLvl; lvl++)
        printf ("%.02lf%c", 100.0*(pLvl[lvl] / (double) cntLvl[lvl]),
                lvl == mLvl ? '\n' : ' ');

    return 0;
}

