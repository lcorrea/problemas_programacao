#include <bits/stdc++.h>

using namespace std;

/*
 * algoritmo - kruskal - (MST, greedy)
 *  na hora de usar as arestas, priorizar as ferrovias
 */

typedef vector < pair <int, pair <int, int> > > Graph;

class UnionFind
{
    private:
        vector <int> h, p;

    public:
        int count;
        
        UnionFind(int n): count(n)
        {
            h.assign(n, 0);
            p.resize(n);

            for (int i = 0; i < n; i++)
                p[i] = i;
        }

        int find (int i)
        {
            return i == p[i] ? i : p[i] = find(p[i]);
        }

        void join (int i, int j)
        {
            if (find(i) == find(j)) return;

            count--;
            i = find(i); j = find(j);

            if (h[i] < h[j])
                p[i] = j;
            else
            {
                p[j] = i;
                h[i] += h[i] == h[j];
            }
        }
};

int kruskal(Graph &graph, UnionFind &unionFind)
{
    int minCost = 0;

    sort(graph.begin(), graph.end());

    for (int i = 0; i < (int) graph.size(); i++)
    {
        int d = graph[i].first;
        int u = graph[i].second.first;
        int v = graph[i].second.second;

        if (unionFind.find(u) != unionFind.find(v))
            minCost += d, unionFind.join(u, v);
    }

    return minCost;
}

int main (void)
{
    int n, f, r;

    scanf ("%d %d %d", &n, &f, &r);

    Graph graph1, graph2;
    UnionFind unionFind(n);
    
    for (int i = 0, u, v, p; i < f; i++)
    {
        scanf ("%d %d %d", &u, &v, &p);
        graph1.push_back(make_pair(p, make_pair(u-1, v-1)));
    }

    for (int i = 0, u, v, p; i < r; i++)
    {
        scanf ("%d %d %d", &u, &v, &p);
        graph2.push_back(make_pair(p, make_pair(u-1, v-1)));
    }

    int minCost = kruskal(graph1, unionFind);

    if (unionFind.count > 1)
        minCost += kruskal(graph2, unionFind);

    printf ("%d\n", minCost);

    return 0;
}

