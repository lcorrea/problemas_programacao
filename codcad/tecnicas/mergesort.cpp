#include <bits/stdc++.h>

using namespace std;

void mergeSort (int *v, int n)
{
    if (n == 1)
        return;

    int na = n >> 1;
    int nb = n - na;
    int va[na], vb[nb];
    
    for (int i = 0; i < na; i++)
        va[i] = v[i];

    for (int i = 0; i < nb; i++)
        vb[i] = v[i+na];

    mergeSort(va, na);
    mergeSort(vb, nb);

    int ia = 0, ib = 0, i = 0;

    while (ia < na and ib < nb)
    {
        if (va[ia] > vb[ib])
            v[i++] = va[ia++];
        else
            v[i++] = vb[ib++];
    }

    while(ia < na)
        v[i++] = va[ia++];

    while(ib < nb)
        v[i++] = vb[ib++];

    return;
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    int v[n];

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i);
    
    mergeSort(v, n);

    printf ("%d", v[0]);
    for (int i = 1; i < n; i++)
        printf (" %d", v[i]);
    printf ("\n");

    return 0;
}

