#include <bits/stdc++.h>

using namespace std;

int tb[1010][1010];

int calc (int n, int m, int *s1, int *s2)
{
    if (n < 0 or m < 0)
        return 0;

    if (tb[n][m] >= 0)
        return tb[n][m];

    if (s1[n] == s2[m])
        return tb[n][m] = 1 + calc (n-1, m-1, s1, s2);

    return tb[n][m] = max(calc(n-1, m, s1, s2), calc(n, m-1, s1, s2));
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    int s1[n], s2[n];

    for (int i = 0; i < n; i++)
        scanf ("%d", s1 + i), s2[i] = s1[i];

    memset (tb, -1, sizeof(tb));

    sort (s2, s2 + n);

    int lcs = calc (n-1, n-1, s1, s2);

    printf ("%d\n", lcs);
    
    return 0;
}

