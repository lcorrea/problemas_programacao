#include <bits/stdc++.h>

using namespace std;

int cnt(int *v, int n)
{
    if (n == 1)
        return 0;

    int p1 = n >> 1;
    int p2 = n - p1;

    int v1[p1], v2[p2];

    for (int i = 0; i < p1; i++)
        v1[i] = v[i];

    for (int i = 0; i < p2; i++)
        v2[i] = v[i+p1];

    int cnt1 = cnt(v1, p1);
    int cnt2 = cnt(v2, p2);

    int i = 0, i1 = 0, i2 = 0;

    while (i1 < p1 and i2 < p2)
    {
        if (v2[i2] < v1[i1])
        {
            cnt2 += p1 - i1;
            v[i++] = v2[i2++];
        }
        else
            v[i++] = v1[i1++];
    }

    while (i1 < p1)
        v[i++] = v1[i1++];

    while (i2 < p2)
        v[i++] = v2[i2++];

    return cnt1 + cnt2;
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    int v[n];

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i);

    printf ("%d\n", cnt(v, n));

    return 0;
}

