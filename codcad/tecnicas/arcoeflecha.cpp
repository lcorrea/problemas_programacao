#include <bits/stdc++.h>

using namespace std;

long long int cnt (double *v, int n)
{
    if (n == 1)
        return 0;

    int p1 = n >> 1;
    int p2 = n - p1;
    double v1[p1], v2[p2];

    for (int i = 0; i < p1; i++)
        v1[i] = v[i];

    for (int i = 0; i < p2; i++)
        v2[i] = v[i+p1];

    long long int cnt1 = cnt(v1, p1);
    long long int cnt2 = cnt(v2, p2);

    int i1 = 0, i2 = 0, i = 0;

    while (i1 < p1 and i2 < p2)
    {
        if (v1[i1] > v2[i2])
            v[i++] = v1[i1++];
        else
        {
            cnt2 += p1 - i1;
            v[i++] = v2[i2++];
        }
    }

    while (i1 < p1)
        v[i++] = v1[i1++];

    while (i2 < p2)
        v[i++] = v2[i2++];

    return cnt1 + cnt2;
}

int main (void)
{
    int n;

    scanf("%d", &n);

    double dist[n];

    for (int i = 0, x, y; i < n; i++)
    {
        scanf ("%d %d", &x, &y);

        dist[i] = hypot(x, y);
    }

    printf ("%lld\n", cnt(dist, n));

/*    for (int i = 0; i < n; i++)
        printf ("%lf ", dist[i]);
    putchar ('\n');
*/
    return 0;
}

