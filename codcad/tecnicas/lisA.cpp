#include <bits/stdc++.h>

using namespace std;

int lis (char *str)
{
    vector <char> pilha;

    for (int i = 0; str[i]; i++)
    {
        auto it = upper_bound(pilha.begin(), pilha.end(), str[i]);

        if (it == pilha.end()) pilha.push_back(str[i]);
        else *it = str[i];
    }

    return pilha.size();
}

int main (void)
{
    char str[300001];

    scanf ("%s", str);

    printf ("%d\n", lis(str));

    return 0;
}

