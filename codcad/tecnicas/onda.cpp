#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int m;
    int v[100010];

    scanf ("%d", &m);

    for (int i = 0; i < m; i++)
        scanf ("%d", v + i);

    int inc[100010], dec[100010], aux[100010];
    int len = 1;

    inc[0] = 1;
    aux[0] = v[0];
    for (int i = 1; i < m; i++)
    {
        if (v[i] < aux[0])
            aux[0] = v[i];
        else if (v[i] > aux[len-1])
            aux[len++] = v[i];
        else
        {
            int *p = lower_bound(aux, aux+len, v[i]);
            *p = v[i];
        }
        inc[i] = len;
    }

    reverse(v, v+m);
    dec[0] = 1;
    aux[0] = v[0];
    len = 1;

    for (int i = 1; i < m; i++)
    {
        if (v[i] < aux[0])
            aux[0] = v[i];
        else if (v[i] > aux[len-1])
            aux[len++] = v[i];
        else
        {
            int *p = lower_bound(aux, aux+len, v[i]);
            *p = v[i];
        }
        dec[i] = len;
    }

    reverse(dec, dec + m);

    if (m == 1 or m == 2) puts ("0");
    else if (inc[m-1] == 1 or dec[0] == 1) puts ("1");
    else
    {
        //printf ("%d %d\n", inc[0], inc[1]);
        //printf ("%d %d\n", dec[1], dec[2]);
        int maxCnt = 0;//max(inc[0]+dec[1], inc[1] + dec[2]);

        for (int i = 2; i < m; i++)
            maxCnt = max(maxCnt, min(inc[i], dec[i]));

        printf ("%d\n", maxCnt*2 - 1);
    }

    return 0;
}

