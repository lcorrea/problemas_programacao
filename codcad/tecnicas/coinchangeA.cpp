#include <bits/stdc++.h>

using namespace std;

int dp[1001];

bool calc(int x, vector <int> &c)
{
    if (x < 0)
        return false;
    else if (dp[x] >= 0)
        return dp[x];
    else if (x == 0)
        return dp[x] = true;
    else
    {
        dp[x] = false;

        for (int i = 0; i < (int) c.size(); i++)
            dp[x] = calc(x - c[i], c) ? true : dp[x];
    }

    return dp[x];
}

int main (void)
{
    memset (dp, -1, sizeof(dp));

    int n, x;

    scanf ("%d %d", &n, &x);

    vector <int> c(n);

    for (int i = 0; i < n; i++)
        scanf ("%d", &c[i]);

    puts (calc(x, c) ? "S" : "N");

    return 0;
}

