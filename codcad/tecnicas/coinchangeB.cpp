#include <bits/stdc++.h>

using namespace std;

int dp[1001];
vector <int> c;

int calc(int x)
{
    if (dp[x] >= 0)
        return dp[x];
    else if (x == 0)
        return dp[x] = 0;
    else
    {
        dp[x] = INT_MAX;

        for (int i = 0; i < (int) c.size(); i++)
            if (x >= c[i])
            {
                int a = calc(x-c[i]);

                if (a < dp[x])
                    dp[x] = a + 1;
            }
    }

    return dp[x];
}


int main (void)
{
    int n, x;

    scanf ("%d %d", &n, &x);

    memset(dp, -1, sizeof(dp));
    
    c.resize(n);
    for (int i = 0; i < n; i++)
        scanf ("%d", &c[i]);

    puts (calc (x) < 10 ? "S" : "N");

    return 0;
}

