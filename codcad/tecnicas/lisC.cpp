#include <bits/stdc++.h>

using namespace std;

size_t lis(vector <int> &v)
{
  vector <int> pilha;

  for (auto i = 0; i < v.size(); i++)
  {
      auto it = lower_bound(pilha.begin(), pilha.end(), v[i]);

      if (it == pilha.end()) pilha.push_back(v[i]);
      else *it = v[i];
  }

  return pilha.size();
}

int main (void)
{
    int n, m;
    unordered_map <int, int> s;
    vector <int> v;

    scanf ("%d %d", &n, &m);

    for (int i = 0, a; i < n; i++)
    {
        scanf("%d", &a);

        s[a] = i;
    }

    for (int i = 0, a; i < m; i++)
    {
        scanf("%d", &a);

        if (s.find(a) != s.end())
            v.push_back(s[a]);
    }

    printf ("%d\n", (int) lis(v));
/*
    v.resize(n);
    for (int i = 0, a; i < n; i++)
        scanf ("%d", &v[i]);

    printf ("%d\n", (int) lis(v));*/

    return 0;
}

