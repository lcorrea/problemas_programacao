#include <bits/stdc++.h>

using namespace std;

int kadane(int *v, int n)
{
    int ans = 0, s = 0;
    int j = 0, k = 0, l = 0, m = 0;

    for (int i = 0; i < n; i++)
    {
        if (s + v[i] >= s)
            m = i;

        s += v[i];

        if (s >= ans)
        {
            j = l; k = m;
            ans = s;
        }

        if (s < 0)
        {
            s = 0;
            l = i + 1;
            m = i + 1;
        }
    }

    if(!ans) return ans;

    int sum[(n << 1) + 1];
    int tb[(n << 1) + 1];

    sum[0] = v[0];
    for (int i = 1, len = (n << 1) - 1; i < len; i++)
        sum[i] = v[i] + sum[i-1];

    tb[0] = ans;
    for (int i = 1; i < n; i++)
    {
        if (i >= j)
        {
            tb[i] = max(0, tb[i-1] - v[i-1]*-1);
            j = i;

            if (tb[i] < sum[i+n-1] - sum[i-1])
            {
                tb[i] = sum[i+n-1] - sum[i-1];
                k = i+n-1;
            }
        }
        else
        {
            if (tb[i-1] < sum[i+n-1] - sum[i-1])
                j = i, k = i+n-1;
            else
                tb[i] = tb[i-1];
        }

        ans = max(ans, tb[i]);
    }

    return ans;
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    int v[(n << 1) + 1];

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i), v[i+n] = v[i];

    printf ("%d\n", kadane(v, n));

    return 0;
}

