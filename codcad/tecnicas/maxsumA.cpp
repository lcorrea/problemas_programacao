#include <bits/stdc++.h>

using namespace std;

int kadane(int *v, int n)
{
    int ans = 0, maxS = 0;

    for (int i = 0; i < n; i++)
    {
        maxS = max(0, maxS + v[i]);

        ans = max(ans, maxS);
    }

    return ans;
}

int kadane2(int *v, int n)
{
    int ans = 0, sum = 0;
    int j = 0, k = 0, l = 0, m = 0;

    for (int i = 0; i < n; i++)
    {
        if (sum + v[i] >= sum)
            m = i;

        sum += v[i];

        if (sum >= ans)
        {
            j = l; k = m;
            ans = sum;
        }

        if (sum < 0)
        {
            sum = 0;
            l = i + 1;
            m = i + 1;
        }
    }

    printf ("%d %d\n", j, k);

    return ans;
}

int main (void)
{
    int n;
    
    scanf ("%d", &n);

    int v[n];

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i);

    printf ("%d*\n", kadane2(v, n));

    return 0;
}

