#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

Graph graph(100001);
int w[100001], in[100001], parent[100001]; 
int mem[100001][2];

int calc (int u, int sel)
{
    if (mem[u][sel] > INT_MIN)
        return mem[u][sel];

    //caso1 => pai de u nao selecionado, u selecionado
    //caso2 => pai de u selecionado
    //caso3 => pai de u nao selecionado, u tbm nao selecionado
    //          caso3 = min(caso1, caso2)
    int caso1 = w[u], caso2 = 0;

    for (int i = 0; i < (int) graph[u].size(); i++)
    {
        int v = graph[u][i];

        if (v == parent[u]) continue;

        parent[v] = u;

        //caso1
        caso1 += calc(v, 1);
        caso2 += calc(v, 0);
    }

    if (sel)
        mem[u][1] = caso2;
    else
        mem[u][0] = max(caso1, caso2);

    return mem[u][sel];
}

int main (void)
{
    int n, root = 1;

    for (int i = 0; i < 100001; i++)
        mem[i][0] = mem[i][1] = INT_MIN;
    
    scanf ("%d", &n);

    for (int i = 1, a, b; i <= n - 1; i++)
    {
        scanf ("%d %d", &a, &b);

        graph[a].push_back(b);
        graph[b].push_back(a);
        in[a] += 1;
        in[b] += 1;

        if (in[a] > in[b])
            root = (in[root] < in[a]) ? a : root;
        else
            root = (in[root] < in[b]) ? b : root;
    }

    for (int i = 1; i <= n; i++)
        scanf ("%d", &w[i]);

    parent[root] = root;
    printf ("%d\n", calc(1, 0));

    return 0;
}

