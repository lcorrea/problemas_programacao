#include <bits/stdc++.h>

using namespace std;

int main (void)
{ 
    int v[100001] = {0}, pre[100001] = {0}, suf[100001] = {0};
    int max_suf[100001] = {0};
    int ans = 0, maxS = 0;
    int n;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i);
 
    for (int i = 0; i < n; i++)
    {
        maxS = max(0, maxS + v[i]);
        ans = max(ans, maxS);
    }

    pre[0] = v[0];
    for (int i = 1; i < n; i++)
        pre[i] = pre[i-1] + v[i];

    suf[n-1] = v[n-1];
    max_suf[n-1] = v[n-1];
    for (int i = n-2; i >= 0; --i)
    {
        suf[i] = suf[i+1] + v[i];
        max_suf[i] = max(max_suf[i+1], suf[i]);
    }

    for (int i = 0; i <= n-2; i++)
        ans = max(ans, pre[i] + max_suf[i+1]);

    printf ("%d\n", ans);

    return 0;
}

