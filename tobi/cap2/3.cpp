/*
	problem 3
	times
	by lucas correa
	time: 00:28:52
*/
#include <iostream>
#include <set>
#include <algorithm>

using namespace std;

struct jogador
{
	string nome;
	unsigned int hab;
};

bool comp1(struct jogador a, struct jogador b)
{
	return a.hab > b.hab;
}

int main(void)
{
	struct jogador alunos[10000];
	set<string> times[1000];
	set<string>::const_iterator it;
	unsigned int n, t, i, j = 0;

	cin >> n >> t;

	for(i = 0; i < n; i++)
		cin >> alunos[i].nome >> alunos[i].hab;
	
	sort(alunos, alunos+n, comp1);

	while(n)
	{
		for(i = 0; i < t && n; i++, n--)
			times[i].insert(alunos[j++].nome);
	}

	for(i = 0; i < t; i++)
	{
		cout << "Time " << i + 1 << '\n';

		for(it = times[i].begin(); it != times[i].end(); it++)
			cout << *it << '\n';

		cout << '\n';
	}
	
	return 0;
}

