/*
	problem 6
	fila
	by lucas correa
	time: 00:13:28
*/
#include <iostream>
#include <deque>
#include <algorithm>

using namespace std;

int main(void)
{
	deque<unsigned int> fila;
	unsigned int n, m;

	cin >> n;

	while(n--)
	{
		cin >> m;
		fila.push_back(m);
	}

	cin >> n;

	while(n--)
	{
		cin >> m;
		fila.erase(find(fila.begin(), fila.end(), m));
	}

	cout << fila.front();
	fila.pop_front();

	while(!fila.empty())
	{
		cout << ' ' << fila.front();
		fila.pop_front();
	}

	cout << endl;
	
	return 0;
}

