/*
	problem 7
	notas
	by lucas correa
	time: 00:19:45
*/
#include <iostream>
#include <map>
#include <utility>
#include <algorithm>

using namespace std;

bool comp(pair<unsigned int, unsigned int> a, pair<unsigned int, unsigned int> b)
{

	if(a.second == b.second)
		return a.first < b.first;
	
	return a.second < b.second;

}

int main(void)
{
	map<unsigned int, unsigned int> notas;
	pair<unsigned int, unsigned int> m;
	unsigned int n, nota;

	cin >> n;

	while(n--)
	{
		cin >> nota;

		if(notas.find(nota) != notas.end())
			notas[nota] +=  1;
		else
			notas[nota] = 1;
	}

	m = *max_element(notas.begin(), notas.end(), comp);

	cout << m.first << endl;

	return 0;
}

