/*
	problem 2
	expressoes
	by lucas correa
	time 01:07:44
*/
#include <iostream>
#include <stack>

using namespace std;

int main(void)
{
	stack<char> start_expr;
	string expr;
	char rsp;
	unsigned int t, i;

	start_expr.push('@');

	cin >> t;

	while(t--)
	{
		rsp = 'S';

		cin >> expr;

		if(expr.length()%2)
			rsp = 'N';

		for(i = 0; i < expr.length() && rsp == 'S'; i++)
		{
			
			switch(expr[i])
			{
				case (')'):
					if(start_expr.top() != '(')
						rsp = 'N';

					start_expr.pop();
				break;		
				case ('}'):
					if(start_expr.top() != '{')
						rsp = 'N';

					start_expr.pop();
				break;
				case (']'):
					if(start_expr.top() != '[')
						rsp = 'N';

					start_expr.pop();
				break;
				default:
					start_expr.push(expr[i]);
				break;
			}
		}

		cout << rsp << endl;
	}

	return 0;
}

