/*
	problem 8
	telefone
	by lucas correa
	time: 00:25:18
*/
#include <iostream>
#include <map>
#include <cctype>

using namespace std;

int main(void)
{
	map<char, unsigned int> num;
	unsigned int j;
	char i;
	string numero;
	
	cin >> numero;
	
	for(i = 'A', j = 2; i <= 'O'; j++)
	{
		num[i++] = j;
		num[i++] = j;
		num[i++] = j;
	}

	num['P'] = 7;
	num['Q'] = 7;
	num['R'] = 7;
	num['S'] = 7;

	num['T'] = 8;
	num['U'] = 8;
	num['V'] = 8;
	
	num['W'] = 9;
	num['X'] = 9;
	num['Y'] = 9;
	num['Z'] = 9;

	for(j = 0; j < numero.length(); j++)
	{
		if(isalpha(numero[j]))
			cout << num[numero[j]];
		else
			cout << numero[j];
	}

	cout << endl;

	return 0;
}

