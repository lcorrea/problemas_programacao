#include <iostream>
#include <map>
#include <cmath>

using namespace std;

int main(void)
{
	map<int, int> casas;
	int n, m, casa, tempo = 0, i = 0;
	int anterior = 0, atual;
	
	cin >> n >> m;

	while(n--)
	{
		cin >> casa;
		casas[casa] = i++;
	}

	while(m--)
	{
		cin >> casa;

		tempo += fabs(casas[casa] - anterior);

		anterior = casas[casa];
	}

	cout << tempo << endl;

	return 0;
}

