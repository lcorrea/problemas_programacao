/*
	problem 4
	vende-se
	by lucas correa
	time: 00:54:39
*/
#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	unsigned int k, n, i, dist;
	unsigned int predios[100000];
	
	cin >> n >> k;

	for(i = 0; i < n; i++)
		cin >> predios[i];
	
	sort(predios, predios+n);

	dist = predios[n-1];

	for(i = 0; i <= k; i++)
		dist = min(dist, predios[i+(n-k)-1] - predios[i]);

	cout << dist << endl;

	return 0;
}

