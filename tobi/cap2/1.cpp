/*
	problem 1
	banco
	by lucas correa
	time: 01:46:44
*/
#include <iostream>
#include <algorithm>
#include <queue>
#include <utility>

using namespace std;

int main(void)
{
	queue< pair<int, int> > fila;
	pair<int, int> cliente;
	int caixas[10], t, d, c, n, i, *p;
	unsigned int espera = 0;

	cin >> c >> n;

	fill(caixas, caixas+10, 0);

	for(i = 0; i < n; i++)
	{
		cin >> t >> d;
		fila.push(pair<unsigned int, unsigned int>(t, d));
	}

	while(!fila.empty())
	{
		cliente = fila.front();

		p = min_element(caixas, caixas+c);

		if(*p < cliente.first)
			*p =  cliente.first + cliente.second;
		else
		{
			if(*p - cliente.first > 20)
				espera++;

			*p += cliente.second;
		}
		
		fila.pop();
	}
	
	cout << espera << endl;

	return 0;
}

