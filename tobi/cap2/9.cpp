#include <iostream>
#include <queue>
#include <vector>

using namespace std;

struct vendedor{
	unsigned int id;
	unsigned int tempo;
};

bool operator <(const struct vendedor a, const struct vendedor b)
{
	if(a.tempo == b.tempo)
		return a.id >= b.id;
	
	return a.tempo >= b.tempo;
}

int main(void)
{
	unsigned int n, l, tempo;
	struct vendedor a;
	priority_queue<vendedor> v;
	vector<unsigned int> ligacoes;
	
	cin >> n >> l;

	ligacoes.resize(n, 0);

	for(unsigned int i = 0; i < n; i++)
	{
		a.id = i;
		a.tempo = 0;
		v.push(a);
	}

	for(unsigned int i = 0; i < l; i++)
	{
		cin >> tempo;

		a = v.top();
		v.pop();

		a.tempo += tempo;
		ligacoes[a.id]++;

		v.push(a);
	}

	for(unsigned int i = 0; i < n; i++)
		cout << i+1 << ' ' << ligacoes[i] << endl;

	return 0;
}

