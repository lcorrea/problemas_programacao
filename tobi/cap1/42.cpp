#include <stdio.h>

int l, c;
int patio[1010][1010];

void percorrer (int *a, int *b)
{
	int newa[] = {0, 1, -1, 0};
	int newb[] = {-1, 0, 0, 1};
	bool fim = 0;

	while (!fim)
	{
		fim = 1;
		patio[*a][*b] = 0;
		for (int i = 0; i < 4; i++)
		{
			int posa = *a + newa[i];
			int posb = *b + newb[i];
			if (posa >= 0 and posa < l and posb >= 0 and posb < c)
			{
				if (patio[posa][posb])
				{
					*a = posa;
					*b = posb;
					fim = 0;
					break;
				}
			}
		}
	}

}

int main (void)
{
	int a, b;

	scanf ("%d %d", &l, &c);
	scanf ("%d %d", &a, &b);
	a-=1;b-=1;

	for (int i = 0; i < l; i++)
		for (int j = 0; j < c; j++)
			scanf ("%d", &patio[i][j]);

	percorrer (&a, &b);
	printf ("%d %d\n", ++a, ++b);

	return 0;
}

