/*
	problem 10
	colchao
	by lucas correa
*/
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	int v;
	unsigned int hl[2], x = 0, b = 0;
	vector<unsigned int> abc(3);
	vector<unsigned int>::const_iterator it;

	cin >> abc[0];
	cin >> abc[1];
	cin >> abc[2];

	cin >> hl[0] >> hl[1];

	sort(abc.begin(), abc.end());

	it = upper_bound(abc.begin(), abc.end(), hl[0]);

	x = it - abc.begin();
	
	it = upper_bound(abc.begin(), abc.end(), hl[1]);

	b = it - abc.begin();

	if(b && x)
	{
		if(b+x > 2)
		{
			cout << 'S' << endl;
			return 0;
		}
	}
	
	cout << 'N' << endl;

	return 0;
}
