/*
	problem 28
	tetris
	by lucas correa
	time: 00:53:04
*/
#include <iostream>
#include <cstring>
#include <algorithm>
#include <numeric>

using namespace std;

struct jogador {
	char name[15];
	unsigned int pts;
};

bool comp(struct jogador a, struct jogador b)
{
	if(a.pts == b.pts)
		return (strcmp(a.name, b.name) < 0);

	return (a.pts > b.pts);
}

int main(void)
{
	struct jogador jogadores[1000];
	unsigned int j, testes = 0, notas[12], i, k, last;

	cin >> j;

	while(j)
	{
		testes++;

		for(k = 0; k < j; k++)
		{
			cin >> jogadores[k].name;

			for(i = 0; i < 12; i++)
				cin >> notas[i];
			
			sort(notas, notas+12);

			jogadores[k].pts = accumulate(notas+1, notas+11, 0);
		}

		sort(jogadores, jogadores+j, comp);
		last = jogadores[0].pts;

		cout << "Teste " << testes << '\n';

		for(i = 1, k =0; k < j ; k++)
		{
			if(jogadores[k].pts != last)
			{
				last = jogadores[k].pts;
				i = k+1;
			}

			cout << i << ' ' << last << ' ' << jogadores[k].name << '\n';
		}

		cout << endl;

		cin >> j;
	}
	
	return 0;
}

