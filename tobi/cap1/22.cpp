/*
	problem 22
	guerra por territorio
	by lucas correa
	time: 00:18:32
*/
#include <iostream>
#include <numeric>

using namespace std;

int main(void)
{
	unsigned int n, secao[100000], div, i, A, B;

	cin >> n;
	
	for(i = 0; i < n; i++)
		cin >> secao[i];

	div = n/2;

	A = accumulate(secao, secao + div, 0);
	B = accumulate(secao + div, secao+n, 0);

	while(A != B)
	{
		if(A > B)
			div--;
		else
			div++;
		A = accumulate(secao, secao + div, 0);
		B = accumulate(secao + div, secao+n, 0);
	}

	cout << div << endl;

	return 0;
}

