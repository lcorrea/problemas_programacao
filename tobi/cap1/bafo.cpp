#include <stdio.h>

int main (void)
{
	int a, b, n, r, f1, f2,i;

	scanf ("%d", &n);
	for (i = 1; n; i++)
	{
		a = 0; b = 0;
		while (n--)
		{
			scanf ("%d %d", &f1, &f2);
			a += f1; b+=f2;
		}

		printf ("Teste %d\n%s\n\n", i, (a > b) ? ("Aldo") : ("Beto"));

		scanf ("%d", &n);
	}

	return 0;
}

