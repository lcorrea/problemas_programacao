#include <stdio.h>
#include <set>
#include <algorithm>

#define MAX 100050

using namespace std;

int main (void)
{
	int a[MAX], b[MAX];
	int m, n, i, bi;

	scanf ("%d %d", &n, &m);
	for (i = 0; i < n; i++) scanf ("%d", a + i);

	sort (a, a+n);

	bool blef = false;
	for (i = 0; i < m; i++)
	{
		scanf ("%d", &bi);
		blef = true;
		if (binary_search (a, a+n, bi)) blef = false;
		else
		{
			int inic = 0;
			int fim = i - 1;

			while (fim >= 0 and inic <= i)
			{
				int r = b[inic] + b[fim];

				if (r > bi) fim--;
				else if (r < bi) inic++;
				else { blef = false; break; }
			}
		}

		if (blef) break;

		int p = i - 1;

		while (p >= 0 and b[p] > bi)
		{
			b[p+1] = b[p];
			p--;
		}

		b[p+1] = bi;
	}

	if (blef) printf ("%d\n", bi);
	else puts ("sim");

	return 0;
}

