/*
	problem 34
	corrida
	by lucas correa
	time: 00:07:44
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int v1, v2, s1, s2, n1, n2;

	cin >> n1 >> s1 >> v1 >> n2 >> s2 >> v2;

	if((float) s1/v1 > (float) s2/v2)
		cout << n2;
	else
		cout << n1;

	cout << endl;

	return 0;
}

