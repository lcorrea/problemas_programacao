/*
	problem 27
	soma das casas
	by lucas correa
	time: 00:54:41
*/
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	unsigned int k, n, casa, i;
	unsigned int casas[100000];

	cin >> n;

	for(i = 0; i < n; i++)
	{
		cin >> casas[i];
	}

	cin >> k;
	
	n = upper_bound(casas, casas+n, k) - casas;

	for(i = 0; i < n; i++)
	{
		//PODE MELHORAR ISSO!!
		//MAS DESSE JEITO PASSA!
		if(binary_search(casas+i, casas+n, k - casas[i]))
		{
			casa = find(casas+i, casas + n, k - casas[i]) - casas;
			cout << casas[i] << ' ' << casas[casa] << endl;
			break;
		}

	}

	return 0;
}

