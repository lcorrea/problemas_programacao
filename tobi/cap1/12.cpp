/*
	problem 12
	auto estrada
	by lucas correa
	time: 00:10:51
*/
#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	unsigned int sum = 0, size;
	char map;

	cin >> size;

	while(size--)
	{
		cin >> map;

		switch(map)
		{
			case ('P'):
			case ('C'):
				sum += 2;
			break;
			case('A'):
				sum++;
			break;
		}
	}

	cout << sum << endl;

	return 0;
}

