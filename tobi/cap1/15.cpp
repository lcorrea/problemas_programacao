/*
	problem 15
	desafio do maior numero
	by lucas correa
	time: 00:03:58
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int num, maxS = 0;

	cin >> num;

	while(num)
	{
		maxS = max(maxS, num);
		cin >> num;
	}

	cout << maxS << endl;

	return 0;
}

