/*
	problem 14
	consecutivos
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	int n, count = 1, maxC = 1;
	int v, va;

	cin >> n;

	cin >> va;

	for(unsigned int i = 1; i < n; i++)
	{
		cin >> v;
		
		if(v == va)
			count++;
		else
		{
			va = v;
			maxC = max(maxC, count);
			count = 1;
		}
	}

	maxC = max(maxC, count);

	cout << maxC << endl;

	return 0;
}

