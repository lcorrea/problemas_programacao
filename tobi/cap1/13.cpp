/*
	problem 13
	chocolate
	by lucas correa
	time: 00:18:09
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int l, cnt = 4;

	cin >> l;

	l /= 2;

	while(l >= 2)
	{
		cnt = cnt*4;
		l /= 2;
	}

	cout << cnt << endl;
		
	return 0;
}

