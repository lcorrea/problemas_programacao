/*
	problem 7
	receita de bolo
	by lucas correa
	time: 00:04:39
*/

#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	unsigned int a,b,c, m;

	cin >> a >> b >> c;

	a /= 2;
	b /= 3;
	c /= 5;
	
	m = min(a, min(b, c));

	cout << m << endl;

	return 0;
}
