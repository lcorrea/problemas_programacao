#include <iostream>

using namespace std;

int main(void)
{
	unsigned int a, b, c, d;

	cin >> a >> b >> c >> d;

	if(a*b == c*d || a*c == b*d || a*d == c*b)
		cout << "S\n";
	else
		cout << "N\n";

	return 0;
}

