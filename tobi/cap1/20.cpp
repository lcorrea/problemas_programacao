/*
	problem 20
	frequencia na aula
	by lucas correa
	time: 00:03:09
*/
#include <iostream>
#include <set>

using namespace std;

int main(void)
{
	set<unsigned int> registros;
	unsigned int n, rg;

	cin >> n;

	while(n--)
	{
		cin >> rg;
		registros.insert(rg);
	}

	cout << registros.size() << endl;

	return 0;
}

