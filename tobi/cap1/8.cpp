/*
	problem 8
	tira-teima
	by lucas correa
	time: 00:08:33
*/
#include <iostream>

using namespace std;

int main(void)
{
	int p;

	cin >> p;

	if(p > 432 || p < 0)
	{
		cout << "fora" << endl;
		return 0;
	}

	cin >> p;

	if(p > 468 || p < 0)
	{
		cout << "fora" << endl;
		return 0;
	}

	cout << "dentro" << endl;
	
	return 0;
}

