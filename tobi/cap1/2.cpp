/*
	problem 2
	corrida
	by lucas correa
	time: 00:04:23
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int c, n;

	cin >> c >> n;

	cout << c%n << endl;

	return 0;
}
