/*
	problem 26
	olimpiadas
	by lucas correa
	time: 00:29:55
*/
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

struct p {
	unsigned int med;
	unsigned int name;
};

bool comp(struct p a, struct p b)
{
	if(a.med == b.med)
		return (a.name < b.name);

	return (a.med > b.med);
}

int main(void)
{
	unsigned int n, m, o, p, b;
	struct p paises[100];

	cin >> n >> m;

	for(unsigned int i = 0; i < n; i++)
	{
		paises[i].med = 0;
		paises[i].name = i+1;
	}

	while(m--)
	{
		cin >> o >> p >> b;
		
		paises[o-1].med++;
		paises[p-1].med++;
		paises[b-1].med++;
	}

	sort(paises, paises+n, comp);

	cout << paises[0].name;

	for(unsigned int i = 1; i < n; i++)
	{
		cout << ' ' << paises[i].name;
	}

	cout << endl;

	return 0;
}

