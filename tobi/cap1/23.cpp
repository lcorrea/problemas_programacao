/*
	problem 23
	quermesse
	by lucas correa
	time: 00:06:38
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, teste = 0, ingresso, i, ganhador;

	cin >> n;

	while(n)
	{
		teste++;
		i = 0;

		while(n--)
		{
			i++;
			cin >> ingresso;
			
			if(i == ingresso)
				ganhador = i;
		}

		cout << "Teste " << teste << '\n';
		cout << ganhador << '\n' << endl;

		cin >> n;
	}

	return 0;
}

