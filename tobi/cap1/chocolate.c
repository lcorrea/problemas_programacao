#include <stdio.h>

int main (void)
{
	int total = 0;
	int n, p;

	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%d", &p);
		total += p - 1;
	}
	printf ("%d\n", total);

	return 0;
}
