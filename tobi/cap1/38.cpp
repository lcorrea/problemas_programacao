#include <iostream>
#include <cmath>

using namespace std;

unsigned int A, B;

unsigned int verifica(unsigned int x, unsigned int y)
{
	return ((x >= A && y >= B) || (y >= A && x >= B));
}

int main(void)
{
	unsigned int a1,b1, a2, b2, x,y;
	char c = 'S';

	cin >> a1 >> b1 >> a2 >> b2 >> A >> B;

	if(!verifica(a1, b1))
	{
		if(!verifica(a2, b2))
		{
			x = a1+a2;
			y = min(b1,b2);

			if(!verifica(x,y))
			{
				x = min(a1,b2);
				y = b1+a2;

				if(!verifica(x,y))
				{
					x = a1+b2;
					y = min(b1,a2);

					if(!verifica(x,y))
					{
						x = min(a2,a1);
						y = b1+b2;

						if(!verifica(x,y))
						{
							c = 'N';
						}
					}
				}
			}
		}
	}

	cout << c << endl;

	return 0;
}

