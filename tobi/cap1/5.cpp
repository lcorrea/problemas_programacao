/*
	problem 5
	cometa
	by lucas correa
	time: 00:34:00
*/

#include <iostream>

using namespace std;

int main(void)
{
	unsigned int ano, prox;

	cin >> ano;

	prox = ano - 1986;

	if(prox >= 76)
	{
		prox = prox%76;

		cout << ano + (76 - prox);
	}
	else
		cout << 2062;

	cout << endl;	

	return 0;
}
