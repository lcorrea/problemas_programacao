/*
	problem 30
	avioes de papel
	by lucas correa
	time: 00:05:34
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int c, p, f;

	cin >> c >> p >> f;

	if(c*f > p)
		cout << 'N';
	else
		cout << 'S';

	cout << endl;

	return 0;
}

