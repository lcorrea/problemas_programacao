#include <stdio.h>

int main (void)
{
	int i, n, diff, v;

	for (i = 1, scanf ("%d", &n); n; i++, scanf ("%d", &n))
	{
		printf ("Teste %d\n", i);
		diff = 0;
		while (n--)
		{
			scanf ("%d", &v);
			diff += v;
			scanf ("%d", &v);
			diff -= v;
			printf ("%d\n", diff);
		}
		putchar ('\n');
	}

	return 0;
}
