#include <stdio.h>

int main (void)
{
	int n, env;
	int maxenv = 1000000;

	scanf ("%d", &n);
	while (n--)
	{
		scanf ("%d", &env);
		if (env < maxenv)
			maxenv = env;
	}

	printf ("%d\n", maxenv);		

	return 0;
}

