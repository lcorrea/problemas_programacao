#include <iostream>

using namespace std;

int main(void)
{
	unsigned int l, c, s, e, sum = 0, f = 0;

	cin >> l >> c;

	cin >> s >> e;

	sum += e;

	l--;

	while(l--)
	{
		
		cin >> s >> e;
			
		sum -= s;

		sum += e;
		
		if(sum > c)
			f = 1;
	}

	if(f)
		cout << 'S' << endl;
	else
		cout << 'N' << endl;

	return 0;
}

