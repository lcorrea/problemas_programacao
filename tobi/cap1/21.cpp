/*
	problem 21
	garcom
	by lucas correa
	time: 00:03:20
*/

#include <iostream>

using namespace std;

int main(void)
{
	unsigned int sum = 0, l, c, n;

	cin >> n;
	
	while(n--)
	{
		cin >> l >> c;
		if(l > c)
			sum += c;
	}

	cout << sum << endl;

	return 0;
}

