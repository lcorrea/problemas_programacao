/*
	problem 32
	cartas
	by lucas correa
	time: 00:21:57
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int v[5], i;
	char r = 'N';

	for(i = 0; i < 5; i++)
		cin >> v[i];

	for(i = 1; i < 5; i++)
		if(v[i] < v[i-1])
			break;
	
	if(i == 5)
		r = 'C';
	else
	{
		for(i = 1; i < 5; i++)
			if(v[i] > v[i-1])
				break;

		if(i == 5)
			r = 'D';
	}

	cout << r << endl;

	return 0;
}

