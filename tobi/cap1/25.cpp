/*
	problem 25
	lista de chamada
	by lucas correa
	time 00:15:00
*/
#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

int main(void)
{
	vector<string> alunos;
	string aluno;
	unsigned int k, n;

	cin >> n >> k;

	alunos.resize(n);

	for(unsigned int i = 0; i < n; i++)
		cin >> alunos[i];

	sort(alunos.begin(), alunos.end());

	cout << alunos[k-1] << endl;

	return 0;
}

