/*
	problem 11
	album de fotos
	by lucas correa
*/
#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int ab[2], foto1[2], foto2[2];
	int possib = 0,s;

	cin >> ab[0] >> ab[1];

	cin >> foto1[0] >> foto1[1] >> foto2[0] >> foto2[1];

	s = foto1[0] + foto2[1];
	if(s <= ab[0])
	{
		
		if(max(foto1[1], foto2[0]) <= ab[1])
			possib++;
	}
	
	if(s <= ab[1])
	{
		if(max(foto1[1], foto2[0]) <= ab[0])
			possib++;
	}

	s = foto1[1] + foto2[1];	
	if(s <= ab[0])
	{
		if(max(foto1[0], foto2[0]) <= ab[1])
			possib++;
	}
	
	if(s <= ab[1])
	{
		if(max(foto1[0], foto2[0]) <= ab[0])
			possib++;
	}
	
	s = foto1[0] + foto2[0];
	if(s <= ab[0])
	{
		if(max(foto1[1], foto2[1]) <= ab[1])
			possib++;
	}
	
	if(s <= ab[1])
	{
		if(max(foto1[1], foto2[1]) <= ab[0])
			possib++;
	}

	s = foto1[1] + foto2[0];
	if(s <= ab[0])
	{
		if(max(foto1[0], foto2[1]) <= ab[1])
			possib++;
	}
	
	if(s <= ab[1])
	{
		if(max(foto1[0], foto2[1]) <= ab[0])
			possib++;
	}

	if(possib)
		cout << 'S' << endl;
	else
		cout << 'N' << endl;

	return 0;
}

