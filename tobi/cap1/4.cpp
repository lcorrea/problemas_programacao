/*
	problem 4
	pedagio
	by lucas correa
	time: 00:08:10
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int l, d, k, p, total;

	cin >> l >> d >> k >> p;

	total = l*k;
	total += (l/d)*p;

	cout << total << endl;

	return 0;
}

