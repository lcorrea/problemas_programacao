/*
	problem 37
	gangorra
	by lucas correa
	time: 00:07:50
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int c1,c2,p1,p2;
	int r = 0;

	cin >> p1 >> c1 >> p2 >> c2;

	r = c2*p2 - c1*p1;

	if(r > 0)
		r = 1;
	else if (r < 0)
		r = -1;

	cout << r << endl;

	return 0;
}
