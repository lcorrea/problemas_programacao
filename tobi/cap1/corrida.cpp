#include <stdio.h>
#include <algorithm>
#include <utility>

using namespace std;

int main (void)
{
	int n, m, i;
	pair <int, unsigned int> carros[100];

	scanf ("%d %d", &n, &m);
	for (i=0; i < n; i++)
	{
		int j, t;

		carros[i].first = i+1;
		carros[i].second = 0;
		for (j=0; j < m; j++)
		{
			scanf ("%d", &t);
			carros[i].second += t;
		}
	}
	sort (carros, carros+n);
	for (i=0; i < 3; i++)
		printf ("%d\n", carros[i].first);

	return 0;
}

