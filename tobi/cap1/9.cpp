/*
	problem 9
	bocha
	by lucas correa
	time: 00:13:12
*/
#include <iostream>
#include <set>

using namespace std;

int main(void)
{
	set<unsigned int> a;
	set<unsigned int>::const_iterator it;
	unsigned int v;

	cin >> v;
	a.insert(v);
	cin >> v;
	a.insert(v);
	cin >> v;
	a.insert(v);

	it = a.begin();

	it++;

	cout << *it << endl;

	return 0;
}

