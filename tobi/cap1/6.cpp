/*
	problem 6
	campeonato
	by lucas correa
	time: 00:16:30
*/
#include <iostream>

using namespace std;

int main(void)
{
 	int c1,c2,c3, f1,f2,f3;

	cin >> c1 >> c2 >> c3 >> f1 >> f2 >> f3;

	c1 = c1*3 + c2;
	f1 = f1*3 + f2;

	if(c1 > f1)
		cout << 'C';
	else if (c1 == f1)
	{
		if(c3 > f3)
			cout << 'C';
		else if(c3 == f3)
			cout << '=';
		else
			cout << 'F';
	}
	else
		cout << 'F';
	
	cout << endl;

	return 0;
}

