#include <stdio.h>

int main (void)
{
	int t, tot = 0, i;

	for (i = 0; i < 4; i++)
	{
		scanf ("%d", &t);
		tot += t;
	}

	printf ("%d\n", tot - 3);

	return 0;
}

