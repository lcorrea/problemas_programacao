/*
	problem 29
	aviao
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	int f, c, e, b;

	cin >> f >> c >> e >> b;

	int p = b%c;

	int rst = (p)?(1):(0);

	if(f*c < c*(e-1) + b)
		cout << "PROXIMO VOO";
	else
	{
	 	cout << e - 1 + b/c + rst << ' ';

		if(p)
			cout << (char) ('A' + p - 1);
		else
			cout << (char) ('A' + c - 1);
	}

	cout << endl;

	return 0;
}

