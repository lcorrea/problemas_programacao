#include <stdio.h>
#include <string.h>

int main (void)
{
	int n, m, a, i = 0, f, p;
	int faixas[10010], premios[10010];
	scanf ("%d %d", &n, &m);
	
	for (a = 0; a < n - 1; a++)
	{
		scanf ("%d", &f);
		faixas[a] = f;
	}

	for (a = 0; a < n; a++)
	{
		scanf ("%d", &f);
		premios[a] = f;
	}

	for (i = 0; i < m; i++)
	{
		scanf ("%d", &f);

		if (f < faixas[0]) p = premios[0];
		else if (f >= faixas[n-2]) p = premios[n-1];
		else
		{
			a = 1;
			while (f >= faixas[a]) a++;
			p = premios[a];
		}
		printf ("%d", p);
		if (i+1<m) putchar (' ');
	}
	putchar ('\n');

	return 0;
}

