//solucao O(n)
#include <stdio.h>
#include <string.h>

using namespace std;

int main (void)
{
	int teste = 1;
	int p, s, inic, fim;
	int praia[10010];

	while (scanf ("%d %d", &p, &s), p and s)
	{
		printf ("Teste %d\n", teste++);
		memset (praia, 0, sizeof (int) * (p+1));

		while (s--)
		{
			scanf ("%d %d", &inic, &fim);
			memset (praia+inic, 1, sizeof (int) * (fim - inic));
		}

		for (int i = 0; i <= p; i++)
		{
			if (praia[i])
			{
				printf ("%d ", i);
				while (i <= p and praia[i]) i++;
				printf ("%d\n", i);
			}
		}
		putchar ('\n');
	}

	return 0;
}

