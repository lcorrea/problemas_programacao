#include <stdio.h>
#include <algorithm>
#include <vector>

using namespace std;

vector <int> vet;

int getn (int ans)
{
	int n = 0;

	for (int i = 0; i < vet.size(); i++)
		n += vet[i] / ans;

	return n;
}

int bSearch (int N, int inic, int fim)
{
	int m;
	int ans = 0;
	int n;

	while (inic <= fim)
	{
		m = (inic + fim)/2;

		if (getn (m) < N) fim = m - 1;
		else inic = m + 1, ans = m;
	}

	return ans;
}	

int main (void)
{
	int k, n, m;

	scanf ("%d\n%d", &n, &k);
	
	vet.assign (k, 0);

	for (int i = 0; i < k; i++)
		scanf ("%d", &m), vet[i] = m;
	
	m = bSearch (n, 0, 10001);

	printf ("%d\n", m);

	return 0;
}

