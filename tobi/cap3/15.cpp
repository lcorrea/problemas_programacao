#include <stdio.h>
#include <algorithm>
#include <numeric>

using namespace std;

int main (void)
{
	int quadrado[3][3];
	int sums[8] = {0};
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			scanf ("%d", &quadrado[i][j]);
			if (i == j)
				sums[6] += quadrado[i][j];
			if (i+j == 2)
				sums[7] += quadrado[i][j];
			sums[j+3] += quadrado[i][j];
			sums[i] += quadrado[i][j];
		}
	}

	int maxsum = 0;

	if (sums[6] == 0 or sums[7] == 0)
		maxsum = accumulate (sums, sums+3, 0) / 2;
	else
		maxsum = *max_element (sums, sums+8);

	bool resolved = true;
	//tenta resolver por linha
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (quadrado[i][j] == 0)
			{
				if (sums[i] == 0)
					quadrado[i][j] = maxsum - sums[j+3];
				else if (count (quadrado[i], quadrado[i]+3, 0) < 2)
					quadrado[i][j] = maxsum - sums[i];
				else { resolved = false; continue; }
				sums[i] += quadrado[i][j];
				sums[j+3] += quadrado[i][j];
	}	}	}

	//tenta resolver por coluna
	if (resolved == false)
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (quadrado[i][j] == 0)
					quadrado[i][j] = maxsum - sums[j+3];

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			printf ("%d ", quadrado[i][j]);
			//putchar ( ((j+1) < 3) ? (' ') : ('\n'));
		}
		putchar ('\n');
	}

	return 0;
}

