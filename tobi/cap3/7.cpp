#include <stdio.h>

int main (void)
{
	int teste = 1;
	int m, result, n;

	while (scanf ("%d", &m), m)
	{
		printf ("Teste %d\n", teste++);
		result = 0;
		while(m--)
		{
			scanf ("%d", &n);
			result += n;
		}
		printf ("%d\n\n", result);
	}

	return 0;
}

