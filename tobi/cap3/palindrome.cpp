#include <bits/stdc++.h>

using namespace std;

char p[2001][2001], str[2001];

int ispalindrome (int i, int j, char str[2001])
{
    if (i > j)
        return 1;
    
    if (p[i][j] >= 0)
        return p[i][j];
    
    if (i == j)
        return p[i][j] = 1;

    if (str[i] == str[j])
        return p[i][j] = ispalindrome(i+1, j-1, str);

    return p[i][j] = 0;
}

int main (void)
{
    int n, teste=1;

    while (scanf ("%d", &n), n)
    {
        memset(p, -1, sizeof(p));

        scanf ("%s", str);

        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                ispalindrome(i, j, str);

        printf ("Teste %d\n", teste++);

    }

    return 0;
}

