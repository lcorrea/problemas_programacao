#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main (void)
{
	int s, x, y;
	unsigned int teste = 1;
	vector <int> mx (1000, 0), my(1000, 0);

	cin >> s;

	while (s)
	{
		cout << "Teste " << teste << endl;

		for (int i = 0; i < s; i++)
		{
			cin >> x >> y;
			mx[i] = x;
			my[i] = y;
		}

		sort (mx.begin (), mx.begin() + s);
		sort (my.begin (), my.begin() + s);

		cout << mx[s/2] << ' ' << my[s/2] << '\n' << endl;

		cin >> s;
		teste++;
	}

	return 0;
}

