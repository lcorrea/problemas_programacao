#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main (void)
{
	unsigned int teste = 1;
	int n, m, i;

	cin >> n >> m;

	while (n and m)
	{
		vector <int> temp(n);
		int sum = 0, maxM = 0, minM = 0, t;

		cout << "Teste " << teste << endl;
		teste++;

		for (i = 0; i < m; i++)
		{
			cin >> t;
			sum += t;
			temp[i] = t;
		}

		maxM = sum/m;
		minM = sum/m;

		for (int k = 0, med = 0; i < n; i++, k++)
		{
			cin >> t;

			temp[i] = t;
			sum -= temp[k];
			sum += t;
			
			med = sum/m;
			maxM = max (maxM, med);
			minM = min (minM, med);
		}

		cout << minM << ' ' << maxM << '\n' << endl;

		cin >> n >> m;
	}

	return 0;
}

