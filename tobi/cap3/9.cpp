//guloso - progressao aritmetica
#include <stdio.h>

int main (void)
{
	int seq[100010];
	int n;

	scanf ("%d", &n);

	for (int i =0;i<n;i++)
		scanf ("%d", seq + i);

	if (n < 3)
	{
		puts("1");
		return 0;
	}
	int resp = 1;
	int b = 0;

	for (int i = 1; i < n; i++)
	{
		if (seq[b+1] - seq[b] != seq[i] - seq[i-1])
		{
			b = i++;
			resp++;
		}
	}

	printf ("%d\n", resp);

	return 0;
}

