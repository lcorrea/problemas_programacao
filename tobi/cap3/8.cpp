//a busca para ver se caiu em um buraco pode ser O(1)...
#include <stdio.h>

int main (void)
{
	const int pos[8][2] = {2, 1, 1, 2, -1, 2, -2, 1, -2, -1, -1, -2, 1, -2, 2, -1};
	const int buracos[4][2] = {3, 1, 3, 2, 5, 2, 4, 5};
	int init[2] = {3, 4}, n, p, mov = 0;
	bool stop = false;

	scanf ("%d", &n);

	while (n-- and !stop)
	{
		mov++;

		scanf ("%d", &p);
		p--;
		
		for (int i = 0; i < 4; i++)
		{
			if (init[0] + pos[p][0] == buracos[i][0] and
				init[1] + pos[p][1] == buracos[i][1])
			{
				stop = true;
				break;
			}
		}

		init[0] += pos[p][0];
		init[1] += pos[p][1];
	}

	printf ("%d\n", mov);

	return 0;	
}

