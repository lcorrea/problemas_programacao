#include <iostream>
#include <queue>

using namespace std;

int main (void)
{
	int m, n;
	queue <char> times;
	char equipe1, equipe2;

	for (char time = 'A'; time <= 'P'; time++)
		times.push(time);

	while (times.size() != 1)
	{
		equipe1 = times.front(); times.pop();
		equipe2 = times.front(); times.pop();

		cin >> m >> n;

		if (m > n)
			times.push(equipe1);
		else
			times.push(equipe2);
	}

	cout << times.front() << endl;	

	return 0;
}

