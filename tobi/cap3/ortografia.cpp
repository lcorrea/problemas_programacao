#include <bits/stdc++.h>

using namespace std;

int calc(const char word1[40], const char word2[40])
{
    int m = strlen(word1), n = strlen(word2);
    int tb[40][40] = {0};

    for (int j = 0; j <= m; j++)
        tb[0][j] = j;

    for (int i = 0; i <= n; i++)
        tb[i][0] = i;

    for (int i = 1; i <= n; i++)
    {

        for (int j = 1; j <= m; j++)
        {

            if (word2[i-1] == word1[j-1])
                tb[i][j] = tb[i-1][j-1];
            else
                tb[i][j] = 1 + min(tb[i-1][j],
                               min(tb[i][j-1], tb[i-1][j-1]));
        }
    }

    return tb[n][m];
}

int main (void)
{
    char dict[1010][40], word[40];
    int n, m;

    scanf ("%d %d%*c", &n, &m);

    for (int i = 0; i < n; i++)
        scanf ("%s", dict[i]);

    for (int i = 0; i < m; i++)
    {
        int words_in_line = 0;

        scanf ("%s", word);

        for (int j = 0; j < n; j++)
        {
            if (calc(dict[j], word) <= 2)
                if (words_in_line++)
                    printf(" %s", dict[j]);
                else
                    printf("%s", dict[j]);
        }

        putchar ('\n');
    }

    return 0;
}

