#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int x, y, n, itens[101], teste = 1;

    while (scanf ("%d%d%d", &x, &y, &n), x and y and n)
    {
        int total = 0;
        bool tb[2][10001] =  {0};

        for (int i = 0; i < n; i++)
            scanf ("%d", itens + i), total += itens[i];

        int t1 = min(x, y) + total - max(x, y);

        if (t1 < 0 or t1 % 2)
        {
            printf ("Teste %d\nN\n\n", teste++);
            continue;
        }

        t1 = t1 / 2;

        //subset sum calculation
        tb[0][0] = tb[1][0] = true;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j <= t1; j++)
            {
                tb[1][j] = tb[0][j];

                if (j >= itens[i])
                    tb[1][j] = tb[1][j] or tb[0][j-itens[i]];
            }

            memcpy(tb[0], tb[1], sizeof(tb[0]));
        }

        printf ("Teste %d\n", teste++);
        puts (tb[1][t1] ? "S\n" : "N\n");
    }

    return 0;
}

