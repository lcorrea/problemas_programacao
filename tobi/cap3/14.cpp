#include <stdio.h>
#include <algorithm>
#include <queue>
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vvi graph;
queue <int> nextToVisit;

int main (void)
{
	int n, m, initPos;
	int caverna[10][10];
	vi exits;

	scanf ("%d %d", &n, &m);
	graph.resize (n*m);
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			scanf ("%d", &caverna[i][j]);
			
			if (caverna[i][j] == 3)
				initPos = i*m + j;

			if (caverna[i][j] == 0)
				exits.push_back (i*m + j);
		}
	}

	int x[4] = {1, 0, -1, 0};
	int y[4] = {0, 1, 0, -1};
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (caverna[i][j] == 2) continue;
			for (int k = 0; k < 4; k++)
			{
				if (i + y[k] >= 0 and i + y[k] < n)
				{
					if (j + x[k] >= 0 and j + x[k] < m)
					{
						if (caverna[i+y[k]][j+x[k]] != 2)
							graph[i*m + j].push_back ((i + y[k])*m + (j + x[k]));
					}
				}
			}
		}
	}

	vi dist(n*m, 1000000); dist[initPos] = 0;
	nextToVisit.push (initPos);
	while (!nextToVisit.empty())
	{
		int u = nextToVisit.front(); nextToVisit.pop();

		for (int j = 0; j < (int) graph[u].size(); j++)
		{
			int v = graph[u][j];

			if (dist[v] == 1000000)
			{
				dist[v] = dist[u] + 1;
				nextToVisit.push(v);
			}
		}
	}

	int minExit = dist[exits[0]];
	for (int i = 1; i < exits.size(); i++)
		minExit = min (dist[exits[i]], minExit);
	
	printf ("%d\n", minExit);

	return 0;
}

