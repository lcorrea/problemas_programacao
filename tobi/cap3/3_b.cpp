#include <iostream>
#include <set>
#include <vector>
#include <utility>
#include <bitset>

using namespace std;

#define modSum(a, b) (((a%n) + (b%n)) % n)
typedef pair <int, int> ii;

int main (void)
{
	vector <int> indDir;
	vector <int> indTor;
	int n, e;
	int t, d, i;
	bitset<10000001> toras;

	cin >> n >> e;

	for (int j = 0; j < e; j++)
	{
		cin >> t >> d;

		indioTora.push_back (t-1);
		indioDir.push_back (d);
		toras[t-1] = 1;
	}

	string valueStart = toras.to_string();
	int hits = 0;

	do
	{
		hits++;

		for (i = 0; i != indioTora.end(); i++)
		{
			t = i->first;
			d = i->second;
			int newPos = modSum (t, d);
			set <ii>::iterator samePos;

			samePos = indioTora.find (ii(t, -d));
			
			bool clearBit = samePos == indioTora.end();

			if (toras.test (newPos))
			{

				samePos = indioTora.find (ii(newPos, -d));

				if (samePos == indioTora.end())
				{
					i->first = newPos;
				}

				samePos->second = d;
				i->second = -d;
			}

			if (clearBit)
				toras[t] = 0;
		}

	}while (valueStart != toras.to_string());

	return 0;
}
