#include <iostream>
#include <algorithm>
#include <vector>
#include <set>

#define check(a, b) ((a < n and a >= 0 and b < n and b >= 0))

using namespace std;

typedef set <int> vi;

int n;
int ilha[100][100] = {0};

void pistas (int x, int y, int d)
{
	int diff;
	int x1, y1, x2, y2;

	vi p;

	for (int i = 0; i <= d; i++)
	{
		diff = d - i;
		x1 = x + i;
		x2 = x - i;
		y1 = y + diff;
		y2 = y - diff;

		if (check(x1, y1))
			p.insert(x1 + y1*n);
		if (check(x1, y2))
			p.insert(x1 + y2*n);
		if (check(x2, y1))
			p.insert(x2 + y1*n);
		if (check(x2, y2))
			p.insert(x2 + y2*n);
	}
	
	vi::const_iterator it;

	for (it = p.begin(); it != p.end(); it++)
	{
		ilha[*it/n][*it%n]++;
	}
}

int main (void)
{
	int k, x, y, d;

	cin >> n >> k;
	
	int maxe[n], m = 0, mp, *p, mi;
	
	for (int i = 0; i < k; i++)
	{
		cin >> x >> y >> d;

		pistas(x, y, d);
	}

	for (int i = 0; i < n; i++)
	{
			p = max_element(ilha[i], ilha[i] + n);
			maxe[i] = *p;
			if (m < maxe[i])
			{
			       m = maxe[i];
			       mi = i;
			       mp = p - ilha[i];
			}
	}

	if (count(maxe, maxe + n, m) == 1 && count(ilha[mi], ilha[mi]+n, m) == 1)
		cout << mp << ' ' << mi << endl;
	else
		cout << -1 << ' ' << -1 << endl;

	return 0;
}

