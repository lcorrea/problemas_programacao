#include <iostream> 

using namespace std;

int main (void)
{
	int n, second, first, total;

	cin >> n;

	total = 10*n;

	if (n)
	{
		cin >> first;

		for (int i = 1; i < n; i++)
		{
			cin >> second;

			if (second - first < 11)
				total -= (10 - (second - first));

			first = second;
		}
	}

	cout << total << endl;

	return 0;
}

