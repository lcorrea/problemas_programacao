#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    cin >> n;

    while (n--)
    {
        int x, y, cnt = 0;
        char tb[2] = {'*', '.'};

        cin >> x >> y;

        for (int i = 0; i < x; i++)
        {
            cnt = i % 2;
            for (int j = 0; j < y; j++)
                putchar (tb[cnt++%2]);
            putchar('\n');
        }
    }

    return 0;
}

