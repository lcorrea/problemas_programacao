/*
	problem HAMSTER1
	Hamster flight
	by lucas correa

*/
#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

#define points_length(a, b, c) (a*(b*sin(2*c)/10))
#define points_heigth(a, b, c) (a*(b*pow(sin(c), 2))/20)

int main(void)
{
	unsigned int k1, k2, vo, t = 0;
	double p1, points = 0, pi_per_2;
	double o, f;

	pi_per_2 = acos(-1)/2;

	cin >> t;
	
	while(t--)
	{
		f = pi_per_2;
		o = 0;

		cin >> vo >> k1 >> k2;
		vo = vo*vo;
		
		while(fabs(o - f) >= 10e-9)
		{
			p1 = points_length(k1, vo, o);
			p1 += points_heigth(k2, vo, o);

			points = points_length(k1, vo, f);
			points += points_heigth(k2,vo, f);

			if(p1 > points)
				f = (f + o)/2;
			else
				o = (f + o)/2;
		}
	
		printf("%.3f %.3f\n", f, points);
	}

	return 0;
}

