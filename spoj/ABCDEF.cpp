#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int v[1000];
	int abc[1000001], def[1000001], it_abc = 0, it_def = 0;
	int cnt = 0;

	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
		scanf ("%d", v + i);

	for (int a = 0; a < n; a++)
		for (int b = 0; b < n; b++)
			for (int c = 0; c < n; c++)
			{
				abc[it_abc++] = v[a]*v[b] + v[c];
				if(v[a])
					def[it_def++] = v[a]*(v[b] + v[c]);
			}

	sort (def, def + it_def);

	for (int i = 0; i < it_abc; i++)
	{
		int *start_def = lower_bound(def, def+it_def, abc[i]);
		int *end_def = upper_bound (def, def+it_def, abc[i]);

		cnt += (end_def - start_def);
	}

	printf ("%d\n", cnt);

	return 0;
}

