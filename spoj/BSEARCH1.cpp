/*
	problem BSEARCH1
	Binary search
	by lucas correa
*/
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

long long int binary_search(long long unsigned int n,
			    vector<long long int>& array,
			    long long int input)
{
	long long int p = 0, r = n-1, q = 0;

	while(p <= r)
	{
		q = ((p+r)/2);
		
		if(array[q] == input)
		{
			vector<long long int>::const_iterator i;
			
			i = lower_bound(array.begin(), array.begin() + q, input);
			return (i - array.begin());

		}
		else if(array[q] > input)
			r = q - 1;
		else
			p = q + 1;
	}

	return -1;
}

int main(void)
{
	vector<long long int> array;
	long long int input, ind;

	long long unsigned int n, q;

	cin >> n >> q;

	array.resize(n);
	
	for(long long unsigned int i = 0; i < n; i++)
	{	
		scanf("%lld%*c", &array[i]);
	}

	while(q--)
	{
		scanf("%lld%*c", &input);
		
		ind = binary_search(n, array, input);

		printf("%lld\n", ind);
	}

	return 0;
}

