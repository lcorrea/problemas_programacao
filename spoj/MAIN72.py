from sys import stdin, stdout

t = int(stdin.readline())
num = [0 for i in range(110)]
tb = [[False for j in range(100010)] for i in range(2)]

for i in range(t):

    n = int(stdin.readline())

    num[:n] = map(int, stdin.readline().split())
    max_sum = sum(num[:n]) + 1

    tb[0][0] = tb[1][0] = True

    for i in range(1, n+1):
        for j in range(max_sum):

            tb[1][j] = tb[0][j]

            if (j >= num[i-1]):
                tb[1][j] = tb[1][j] or tb[0][j - num[i-1]]

        tb[0][::] = tb[1][::]

    total = 0

    for subsum in range(max_sum):
        total += subsum if tb[1][subsum] else 0

    stdout.write(str(total) + '\n')

