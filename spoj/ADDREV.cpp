#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char str1[100], str2[100];
	int n;

	cin >> n;

	while (n--)
	{
		long long a, b;

		cin >> str1 >> str2;

		reverse (str1, str1 + strlen(str1));
		reverse (str2, str2 + strlen(str2));

		sscanf (str1, "%lld", &a);
		sscanf (str2, "%lld", &b);

		a+=b;

		sprintf (str1, "%lld", a);

		reverse (str1, str1 + strlen(str1));

		sscanf (str1, "%lld", &a);

		printf ("%lld\n", a);
	}

	return 0;
}

