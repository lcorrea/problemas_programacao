#include <bits/stdc++.h>

using namespace std;

int main ()
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int num[110] = {0};
        bool tb[2][100010] = {0};
        int n, max_subset_sum = 0;

        scanf ("%d", &n);

        for (int i = 0; i < n; i++)
        {
            scanf ("%d", num + i);
            max_subset_sum += num[i];
        }

        tb[0][0] = tb[1][0] = true;

        for (int i = 1; i < n + 1; i++)
        {
            for (int j = 0; j <= max_subset_sum; j++)
            {
                tb[1][j] = tb[0][j];

                if (j >= num[i-1])
                    tb[1][j] = tb[1][j] or tb[0][j - num[i-1]];
            }

            memcpy(tb[0], tb[1], sizeof(tb[0]));
        }
        
        int total = 0;

        for (int subsum = 0; subsum <= max_subset_sum; subsum++)
            total += tb[1][subsum] ? subsum : 0;

        printf ("%d\n", total);
        
    }

    return 0;
}

