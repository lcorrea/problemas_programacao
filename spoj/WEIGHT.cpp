#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
namespace IO{
    const int SIZE = 1 << 19;
    char buff[SIZE], *p = buff + SIZE;
    char read_char(){
        if( p == buff + SIZE ){
            fread( buff, 1, SIZE, stdin );
            p = buff;
        }
        return *(p++);
    }
    inline int read_str(char * s){
        char c;
        while( isspace(c = read_char()) );
        while( !isspace(c = read_char()) ) {
            *s=c;
            s++;
        }
        *s=0;
        return 0;
    }

    inline int read_int(){
        char c,pc;
        pc=c=read_char(); while( (!isdigit(c)) ) {pc=c;c=read_char();}
        int r = c-'0';
        while( isdigit( c = read_char() ) ) 
            r = 10*r + c - '0';
        if(pc=='-')
            return -1*r;
        return r;
    }
}
using namespace IO;
using namespace std;
#define MAX 1000005

long long in[MAX], psum, sum, res1, res;
int N;

int main(){
    int t;
    t=read_int();
    while(t--){
        res = res1=sum=0;
        //cin >> read_int();
        N=read_int();
        for(int i=N-1;i>=0;i--)
            in[i]=read_int(), printf ("%lld, ", in[i]);
        putchar ('\n');
        for(int i=0;i<N-1;i++) // N-1 is left
        {
            //psum = sum;
            sum = sum + in[i];
            res1= res1 + in[i] + sum;
            if(sum < 0){
                res = res  + res1;
                sum = 0;
                res1=0;
            }
            cout << res <<' '<< res1 << ' ' << sum << endl;
        }
        res = res + res1 + in[N-1];
        cout << res << endl;
    }
    return 0;
}
