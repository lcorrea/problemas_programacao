#include <bits/stdc++.h>

using namespace std;

int n, m;

int color (int tb[1001][1001], int i, int j)
{
	int cnt = 0;
	int incx[4] = {-1, 0, 0, 1};
	int incy[4] = {0, 1, -1, 0};

	tb[i][j] = 1;

	for (int k = 0; k < 4; k++)
	{
		int newpos[2] = {incy[k] + i, incx[k] + j };

		if (newpos[0] < n and newpos[0] >= 0)
			if (newpos[1] < m and newpos[1] >= 0)
				cnt += tb[newpos[0]][newpos[1]];
	}

	return cnt;
}	

int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		int tb[1001][1001] = {0};
		int score = 0;

		scanf ("%d%d", &n, &m);

		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				score += color(tb, i, j);

		printf ("%d\n", score);
	}

	return 0;
}

