#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

typedef pair <int, int> ii;

int grid[100][100];
int dist1[100][100], dist2[100][100];
int n, m;

void add_cell (ii u, ii v, queue<ii> &q, int dist[100][100])
{
	int x = u.first, y = u.second;
	int newy = v.second, newx = v.first;

	if (!grid[newy][newx] and dist[newy][newx] == INF)
	{
		ii newpos = make_pair(newx, newy);

		dist[newy][newx] = dist[y][x] + 1;
		q.push (newpos);
	}
}

int bfs (ii from, ii target, int k, int dist[100][100])
{
	queue <ii> q;

	dist[from.second][from.first] = 0;
	q.push (from);

	while (!q.empty())
	{
		int x = q.front().first;
		int y = q.front().second;

		if (q.front() == target) break;

		q.pop();

		for (int newy = y - k < 0 ? 0 : y - k; newy < n and newy <= y + k; newy++)
		{
			for (int newx = x - k < 0 ? 0 : x - k; newx < m and newx <= x + k; newx++)
			{
				if (abs(newx - x) + abs(newy - y) <= k)
					add_cell(make_pair(x, y), make_pair(newx, newy),
							q, dist);
			}
		}

	}

	return dist[target.second][target.first];
}


int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		int k1, k2;

		scanf ("%d%d%d%d", &n, &m, &k1, &k2);

		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
			{
				scanf ("%d", &grid[i][j]);
				dist1[i][j] = dist2[i][j] = INF;
			}

		int r1 = bfs (make_pair (0, 0),
				make_pair (m-1, 0), k1, dist1);

		int r2 = bfs (make_pair (m-1, 0),
				make_pair (0, 0), k2, dist2);

		int min_mov = INF;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (!grid[i][j])
					min_mov = min (min_mov, max(dist1[i][j], dist2[i][j]));

		printf ("%d\n", min_mov == INF ? -1 : min_mov);
	}

	return 0;
}

