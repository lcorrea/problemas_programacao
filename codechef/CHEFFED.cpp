#include <bits/stdc++.h>

using namespace std;

char number[40];

int s(int x)
{
    if (x < 10)
        return x;

    int sum = 0;

    sprintf (number, "%d", x);

    for (int i = 0; number[i] != '\0'; i++)
        sum += number[i] - '0';

    return sum;
}

int main (void)
{
    int n;
    int cnt = 0;

    scanf ("%d", &n);

    for (int i = 0, limt = n > 90 ? 90 : n; i <= limt; i++)
    {
        int x = n - i;
        int a = s(x);

        if (x + a + s(a) == n)
            cnt++;
    }

    printf ("%d\n", cnt);

    return 0;
}

