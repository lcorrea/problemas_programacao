#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int v[4];

    scanf ("%d%d%d%d", v, v+1, v+2, v+3);

    do
    {
        if (v[0]/(double)v[1] == v[2]/(double)v[3])
        {
            puts ("Possible");
            return 0;
        }

    }while (next_permutation(v, v+4));

    puts ("Impossible");

    return 0;
}

