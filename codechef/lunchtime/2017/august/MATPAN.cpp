#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int alpha[26];
    int n, t;

    scanf ("%d", &t);

    while(t--)
    {
        char str[50001];

        for (int i = 0; i < 26; i++)
            scanf ("%d", alpha + i);

        scanf ("%s", str);

        for (int i = 0; str[i]; i++)
            alpha[str[i]-'a'] = 0;
        
        unsigned long long int p = 0;
        for (int i = 0; i < 26; i++)
            p += alpha[i];

        printf ("%llu\n", p);
    }

    return 0;
}

