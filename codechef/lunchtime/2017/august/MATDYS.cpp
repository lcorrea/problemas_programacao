#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;
        ull  k;

        scanf ("%d %llu", &n, &k);

        ull high = (n == 64) ? ULLONG_MAX : (1 << n) - 1;
        ull low = 0ULL;
        ull mid;

        while(low <= high and k)
        {
            mid = high - ((high - low) >> 1);
            
            if (k & 1ULL)
                low = mid;
            else
                high = mid-1;

            k >>= 1;
        }

        printf ("%llu\n", k + low);
    }

    return 0;
}

