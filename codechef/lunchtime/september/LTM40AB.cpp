#include <bits/stdc++.h>

using namespace std;

long long int calc (int a, int b, int limit)
{
    int l = limit == b;
    int dab = b - a + (!l);

    return (long long int) dab * (n - l);
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int a, b, c, d;

        scanf ("%d%d%d%d", &a, &b, &c, &d);

        if (b < a or d < c or c < a)
            puts ("0");
        else if (c > b)
            printf ("%lld\n", (b - a + 1ll) * (d - c + 1ll));
        else
        {
            long long int dab = calc(a, b, c, d - c + 1);
            long long int dcd = calc(a, b, d, d - c + 1);

            printf ("dab: %lld\n", dab);
            printf ("dcd: %lld\n", dcd);

            printf ("%lld\n", dab + dcd);
        }
    }

    return 0;
}

