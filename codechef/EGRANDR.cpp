#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		int n;
		long long v = 0;
		bool fail = false;
		bool full = false;

		scanf ("%d", &n);

		for (int i = 0, a; i < n; i++)
		{
			scanf ("%d", &a);

			if (a == 2) fail = true;

			if (a == 5) full = true;

			v += a;
		}

		if (fail) puts ("No");
		else if (full and (v / (double) n) >= 4)
			puts ("Yes");
		else
			puts ("No");
	}

	return 0;
}

