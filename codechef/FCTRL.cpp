#include <bits/stdc++.h>

using namespace std;


int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;
        int cnt = 0;
        int tmp = 5;

        scanf ("%d", &n);

        while (tmp <= n)
        {
            cnt += n/tmp;

            tmp = tmp*5;
        }

        printf ("%d\n", cnt);

    }

    return 0;
}

