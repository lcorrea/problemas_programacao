#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		int n, found = 0, area = 1;
		map <int, int> sticks;
		map <int, int>::reverse_iterator it;

		scanf ("%d", &n);

		for (int i = 0, a; i < n; i++)
		{
			scanf ("%d", &a);

			sticks[a]++;
		}

		for (it = sticks.rbegin(); found != 2 and it != sticks.rend(); it++)
		{
			if (it->second >= 4)
			{
				if (!found)
					area = it->first * it->first;
				else
					area *= it->first;

				found = 2;
				break;
			}
			if (it->second >= 2)
				area *= it->first, found++;
		}

		if(found == 2)
			printf ("%d\n", area);
		else
			puts ("-1");
	}

	return 0;
}

