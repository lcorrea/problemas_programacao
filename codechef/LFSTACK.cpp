#include <bits/stdc++.h>

using namespace std;

bool get_element (int it, deque <int> &list, vector < deque < int > > &lfs)
{
  if (it >= list.size())
    return true;

  for (int i = 0; i < (int) lfs.size(); i++)
  {
    if (lfs[i].size() and lfs[i].front() == list[it])
    {
      int v = lfs[i].front();

      lfs[i].pop_front();

      if (get_element(it+1, list, lfs))
        return true;

      lfs[i].push_front(v);
    }
  }

  return false;
}

int main (void)
{
  int t;

  scanf ("%d", &t);

  while (t--)
  {
    vector < deque < int > > lfs;
    deque < int > seq;
    int n, cnt = 0;

    scanf ("%d", &n);

    if (n == 1)
    {
      int sz;

      scanf ("%d", &sz);

      stack <int> thread;

      for (int i = 0, a; i < sz; i++)
        scanf ("%d", &a), thread.push(a);

      bool ok = true;
      for (int i = 0, a; i < sz; i++)
      {
        scanf ("%d", &a);

        if (!thread.empty())
        {
          if (thread.top() != a)
            ok = false;

          thread.pop();
        }

      }

      puts (ok ? "Yes" : "No");
    }
    else
    {

      lfs.resize(n);

      for (int i = 0; i < n;i++)
      {
        int sz;

        scanf ("%d", &sz);
        for (int j = 0, a; j < sz; j++)
          scanf ("%d", &a), lfs[i].push_back(a);
        cnt += sz;
      }

      for (int i = 0, a; i < cnt; i++)
        scanf ("%d", &a), seq.push_front(a);

      puts (get_element(0, seq, lfs) ? "Yes" : "No");
    }
  }

  return 0;
}

