#include <bits/stdc++.h>

using namespace std;

int a[10001], b[10001], tmp[10001];
int n;

int process_block (int i, int v)
{
	if (i == n) return v;

	int s = a[i];
	int last = 0;
	int bn = 0;

	if (b[i-1])
	{
		last = b[i-1];

		b[i-1] = 0;
		s += last;
	}

	if (i == n - 1 and b[n-1])
	{
		bn = b[n-1];
		b[n-1] = 0;
		s += bn;
	}

	if (v == s)
		s = process_block(i+1, v);
	else if (s + b[i] == v)
	{
		int current = b[i];

		b[i] = 0;
		s = process_block(i+1, v);
		b[i] = current;
	}
	else if (i <= n - 2 and s + b[i] + b[i+1] == v)
	{
		int current = b[i];
		int next = b[i+1];

		b[i] = b[i+1] = 0;
		s = process_block (i+1, v);
		b[i] = current;
		b[i+1] = next;
	}
	else if (i <= n-2 and s + b[i+1] == v)
	{
		int next = b[i+1];

		b[i+1] = 0;
		s = process_block (i+1, v);
		b[i+1] = next;
	}
	else
		s = -1;

	if (!b[i-1]) b[i-1] = last;
	if (!b[n-1]) b[n-1] = bn;

	return s;
}

int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		scanf ("%d", &n);

		for (int i = 0; i < n; i++)
			scanf ("%d", b + i);
		for (int i = 0; i < n; i++)
			scanf ("%d", a + i);

		if (n == 1) { printf ("%d\n", a[0]+b[0]); continue; }
		else
		{
			int current = b[0];
			int next = b[1];

			int way1 = process_block (1, a[0]);

			b[0] = 0;
			int way2 = process_block (1, a[0] + current);
			b[0] = current;

			b[1] = 0;
			int way3 = process_block (1, a[0] + next);
			b[1] = next;

			b[0] = b[1] = 0;
			int way4 = process_block (1, a[0] + current + next);

			printf ("%d\n", max(way1, max(way2, max(way3, way4))));
		}

	}

	return 0;
}

