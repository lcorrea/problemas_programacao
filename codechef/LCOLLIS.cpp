#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;

	scanf ("%d%*c", &t);

	while (t--)
	{
		int b, g, cnt = 0;
		char tb[100][100];

		scanf ("%d %d%*c", &b, &g);

		for (int i = 0; i < b; i++)
		{
			for (int j = 0; j < g; j++)
				tb[i][j] = getchar ();
			getchar ();
		}

		for (int j = 0; j < g; j++)
		{
			int likes = 0;

			for (int i = 0; i < b; i++)
				likes += tb[i][j] == '1';

			cnt += likes*(likes-1)/2;
		}

		printf ("%d\n", cnt);
	}

	return 0;
}

