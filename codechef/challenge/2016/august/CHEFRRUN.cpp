#include <bits/stdc++.h>

using namespace std;

void dfs (int i, int n, int *a, int *v, int *stop, stack <int> *cycle)
{
  int next = ((i - 1 + (1 + a[i])) % n) + 1;

  v[i] = 1;
  cycle->push (i);

  if (v[next])
  {
    if (!stop[next])
    {
      int top;

      while (!cycle->empty() and (top = cycle->top()) != next)
      {
        stop[top] = top;
        cycle->pop();
      }
    }

    while (!cycle->empty())
    {
      stop[cycle->top()] = next;
      cycle->pop();
    }

    return;
  }

  dfs (next, n, a, v, stop, cycle);
}

int main (void)
{
  int t, n;
  int stop[100001];
  int a[100001];
  int v[100001];

  scanf ("%d", &t);

  while (t--)
  {
    int cnt = 0;

    scanf ("%d", &n);

    for (int i = 1; i <= n; i++)
    {
      stop[i] = 0;
      v[i] = 0;

      scanf ("%d", a + i);
    }

    for (int i = 1; i <= n; i++)
    {
      if (!v[i])
      {
        stack <int> cycle;

        dfs (i, n, a, v, stop, &cycle);
      }

      cnt += stop[i] == i;
    }

    printf ("%d\n", cnt);
  }

  return 0;
}

