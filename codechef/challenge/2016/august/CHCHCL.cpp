#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int t;
  int long long n, m;

  scanf ("%d", &t);

  while (t--)
  {
    scanf ("%lld%lld", &n, &m);

    puts ((n*m - 1LL) % 2LL ? "Yes" : "No");
  }

  return 0;
}

