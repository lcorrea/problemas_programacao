def solve (number):
    zeros = 0
    ones = 0

    for i in number:
        if (i == '0'):
            zeros += 1
        else:
            ones += 1

    if (len (number) - zeros == 1 or len (number) - ones == 1):
        return True
    else:
        return False


n = int(input())

while (n >= 1):
    number = input()

    if (solve (number)):
        print ("Yes")
    else:
        print ("No")

    n -= 1

