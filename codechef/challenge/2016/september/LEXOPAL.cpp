#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int Case;

  scanf ("%d", &Case);

  while (Case--)
  {
    char str[13245];
    bool ok = true;

    scanf ("%s", str);

    int len = strlen (str);

    for (int i = 0; i < len/2 and ok; i++)
    {
      if (isalpha (str[i]) and isalpha(str[len - 1 - i]))
      {
        if (str[i] == str[len - 1 - i])
          continue;
        else
          ok = false;
      }
      else if (str[i] == '.' and str[len - 1 - i] == '.')
        str[i] = str[len - 1 - i] = 'a';
      else if (isalpha (str[i]))
        str[len - 1 - i] = str[i];
      else
        str[i] = str[len - 1 - i];
    }

    if (len % 2 and !isalpha(str[len/2]))
      str[len/2] = 'a';

    puts (ok ? str : "-1");
  }

  return 0;
}

