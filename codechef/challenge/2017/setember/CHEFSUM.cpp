#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;

        scanf ("%d", &n);

        unsigned long long prefix[n], suffix[n];
        int v[n];

        for (int i = 0; i < n; i++)
            scanf ("%d", v + i);

        prefix[0] = v[0];
        for (int i = 1; i < n; i++)
            prefix[i] = v[i] + prefix[i-1];

        suffix[n-1] = v[n-1];
        for (int i = n - 2; i >= 0; --i)
            suffix[i] = suffix[i+1] + v[i];

        int minV = 0;
        unsigned long long int minT = suffix[minV] + prefix[minV];
        unsigned long long int minI;

        for (int i = 0; i < n; i++)
        {
            minI = suffix[i] + prefix[i];

            if (minI < minT)
            {
                minT = minI;
                minV = i;
            }
        }

        printf("%d\n", minV+1);
    }

    return 0;
}

