#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;

        scanf ("%d", &n);

        if (n == 2)
            puts ("2 1");
        else
        {
            deque <int> q;
            
            q.push_back(2); q.push_back(3); q.push_back(1);

            for (int i = 4; i <= n; i++)
            {
                if (i & 1)
                    q.insert(q.begin()+(q.size()-1), i);
                else
                {
                    int a = q.back(); q.pop_back();
                    int b = q.back(); q.pop_back();

                    q.push_back(a);
                    q.push_back(i);
                    q.push_back(b);
                }
            }

            for (auto i = q.begin(); i != q.end(); i++)
                printf ("%d%c", *i, (i+1)==q.end() ? '\n' : ' ');
        }
    }

    return 0;
}

