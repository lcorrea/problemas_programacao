/*
 * a solucao mais lixo que jah fiz...
 * perdao tourist :(
 */

#include <bits/stdc++.h>

using namespace std;

inline void put(char ch, int qnt, int *sz, string &s1)
{
    int len = min(*sz, qnt);
    
    for (int i = 0; i < len; i++)
        s1 += ch;

    *sz -= len;
}

inline void make_sol (int &a, int &b, int x, int y, char cha, char chb, string &s1)
{
    int i = 0;
    while (a > 0 and b > 0)
    {
        i++;
        put(cha, x, &a, s1);
        i++;
        put(chb, y, &b, s1);
    }
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        char str[100001];
        int x, y, sa = 0, sb = 0, saa = 0, sbb = 0;
        int reala = 0, realb = 0;

        scanf ("%s", str);
        scanf ("%d %d", &x, &y);

       for (int i = 0; str[i]; i++)
            if (str[i] == 'a') sa++;
            else sb++;

       saa = reala = sa;
       sbb = realb = sb;


        string s1, s2;

        make_sol(sa, sb, 1, 1, 'a', 'b', s1);
        make_sol(sbb, saa, 1, 1, 'b', 'a', s2);
        
        for (int i = 0; sa > 0 and i < s1.length(); i++)
        {
            if (s1[i] == 'a')
            {
                int c = min(sa, x-1);
                s1.insert(i==0?0:i+1, c, 'a');
                sa -= c; 
                i += c;
            }
        }
        
        for (int i = 0; sbb > 0 and i < s2.length(); i++)
        {
            if (s2[i] == 'b')
            {
                int c = min(sbb, y-1);
                s2.insert(i==0?0:i+1, c, 'b');
                sbb -= c; 
                i += c;
            }
        }

        for (int i = 0; sb > 0 and i < s1.length(); i++)
        {
            if (s1[i] == 'b')
            {
                int c = min(sb, y-1);
                s1.insert(i-1, c, 'b');
                sb -= c;
                i += c;
            }
        }
        
        for (int i = 0; saa > 0 and i < s2.length(); i++)
        {
            if (s2[i] == 'a')
            {
                int c = min(saa, x-1);
                s2.insert(i-1, c, 'a');
                saa -= c;
                i += c;
            }
        }

        int s1cnt = 0, s2cnt = 0;
        while (sa > 0)
        {
            if (s1.back() == 'a')
                s1 += '*', s1cnt++;
            put('a', x, &sa, s1);
        }    
    
        while (sbb > 0)
        {
            if (s2.back() == 'b')
                s2 += '*', s2cnt++;
            put('b', y, &sbb, s2);
        }

        while (sb > 0)
        {
            if (s1.back() == 'b')
                s1 += '*', s1cnt++;
            put('b', y, &sb, s1);
        }
        
        while (saa > 0)
        {
            if (s2.back() == 'a')
                s2 += '*', s2cnt++;
            put('a', x, &saa, s2);
        }

        puts(s1cnt <= s2cnt ? s1.c_str() : s2.c_str());
    }

    return 0;
}

