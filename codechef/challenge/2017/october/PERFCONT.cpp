#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n, p;

        scanf ("%d %d", &n, &p);

        const int a1 = p >> 1;
        const int a2 = p / 10;
        int hard, cakew;

        hard = cakew = 0;

        for (int i = 0; i < n; i++)
        {
            int x;

            scanf ("%d", &x);

            if (x <= a2)
                hard++;
            else if (x >= a1)
                cakew++;
        }

        puts (hard == 2 and cakew == 1 ? "yes" : "no");
    }

    return 0;
}

