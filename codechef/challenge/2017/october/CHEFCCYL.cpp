#include <bits/stdc++.h>

using namespace std;

class Cycle
{
    bool see;
    int total, n;
    vector <int> min_dist;
    vector <int> w;
    vector <int> prefix;

    void set_prefix()
    {
        prefix.assign(n, 0);

        for (int i = 1; i < n; i++)
            prefix[i] = prefix[i-1] + w[i-1];

        total = accumulate(w.begin(), w.end(), 0);
    }

    public:

    Cycle (): see(false)
    {
    };

    void assign()
    {
        if (see) return;

        n = (int) w.size();
        set_prefix();
        printf ("total %d\n", total);
        
        min_dist = prefix;
    }

    void assign(int head)
    {
        if (see)
        {
            process(head-1);
            return;
        }

        see = true;

        n = (int) w.size();

        set_prefix();
        min_dist.resize(n, 1000000000);

        
        process(head-1);
    }

    int get_dist(int i)
    {
        return min_dist[i-1];
    }

    int get_dist(int i, int j)
    {
        i-=1;j-=1;
        if (j > i)
                return min(total + prefix[i] - prefix[j],
                        prefix[j] - prefix[i]);
            else
                return min(total + prefix[j] - prefix[i],
                        prefix[i] - prefix[j]);
    }

    void add_dist(int dist)
    {
        w.push_back(dist);
    }
    
    void process (int h)
    {
        for (int i = 0; i < n; i++)
        {
            if (h > i)
                min_dist[i] = min(min_dist[i], min(
                        total + prefix[i] - prefix[h],
                        prefix[h] - prefix[i]));
            else
                min_dist[i] = min(min_dist[i], min(
                        total + prefix[h] - prefix[i],
                        prefix[i] - prefix[h]));
        }
        
        printf ("head %d\n", h);
        for (int i = 0; i < n; i++)
            printf ("%d\n", min_dist[i]);
        puts("foi");
    }
};

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n, q;

        scanf ("%d %d", &n, &q);

        Cycle cycles[n], global;
        //vector < unordered_map<int, int> > global(n);

        for (int i = 0; i < n; i++)
        {
            int len;

            scanf ("%d", &len);

            for (int j = 0, dist; j < len; j++)
            {
                scanf ("%d", &dist);
                cycles[i].add_dist(dist);
            }
        }

        for (int i = 0; i < n; i++)
        {
            int v1, v2, w;

            scanf ("%d %d %d", &v1, &v2, &w);
            global.add_dist(w);
            printf ("cycle %d\n", i+1);
            cycles[i].assign(v1);
            printf ("cycle %d\n", i+2);
            cycles[(i+1)%n].assign(v2);
        }

        global.assign();

        for (int i = 0; i < q; i++)
        {
            int v1, v2;
            int c1, c2;

            scanf ("%d %d %d %d",
                    &v1, &c1,
                    &v2, &c2);

            printf ("%d\n",
                    global.get_dist(c1, c2) +
                    cycles[c1-1].get_dist(v1) +
                    cycles[c2-1].get_dist(v2)
                   );
        }
    }

    return 0;
}

