#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    long long int v[100001], diff[100001];
    long long int mex, acc, n, k;

    scanf ("%d", &t);
    
    while (t--)
    {
        scanf ("%lld %lld", &n, &k);

        set <int> s;
        for (int i = 0, a; i < n; i++)
            scanf ("%d", &a), s.insert(a);

        int sz = 0;
        for (auto x : s)
            v[sz++] = x;

        mex = -1LL, acc = v[0];

        if (acc > k)
        {
            printf ("%lld\n", k);
            continue;
        }

        for (int i = 1; i < sz; i++)
        {
            diff[i] = v[i] - v[i-1] - 1LL;
            
            if (acc + diff[i] > k)
            {
                mex = v[i-1]+(k-acc)+1LL;
                break;
            }

            acc += diff[i];
        }

        if (mex != -1LL)
            printf ("%lld\n", mex);
        else
            printf ("%lld\n", v[n-1]+(k-acc)+1LL);
    }

    return 0;
}

