#include <bits/stdc++.h>

using namespace std;

inline int getk (int n)
{
    return UINT_MAX - (99999 - (n - 99991));
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;

        scanf ("%d", &n);

        unsigned int summ = getk(n);
        unsigned int acc = 0;
        vector <unsigned int> a;

        while (acc + 100000 < summ)
        {
            a.push_back(100000);
            acc += 100000;
        }
        summ -= acc;

        acc = 0;
        while (a.size() < n - 2)
        {
            a.push_back(1);
            acc += 1;
        }
        summ -= acc;
        a.push_back(summ-1);
        a.push_back(1);

        for (int i = 0; i < n; i++)
            printf ("%u%c", a[i], i == n-1 ? '\n' : ' ');
    }

    return 0;
}

