#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int test;

    scanf ("%d", &test);

    while (test--) {
        int n;
        int y[20000], x[20000];

        scanf ("%d", &n);

        for (int i = 0; i < n; i++)
            scanf ("%d", x + i);
        
        for (int i = 0; i < n; i++)
            scanf ("%d", y + i);
        
        unsigned long long int t1 = 0, t2 = 0;

        for (int i = 0; i < n; i++)
        {
            if (i % 2) {
                t1 += y[i];
                t2 += x[i];
            } else {
                t1 += x[i];
                t2 += y[i];
            }
        }

        printf ("%llu\n", min(t1, t2));
    }

    return 0;
}

