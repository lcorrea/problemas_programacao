#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int test;

    scanf ("%d", &test);

    while (test--) {
        int n;
        int seq[100001];

        scanf ("%d", &n);

        for (int i = 0; i < n; i++)
            scanf ("%d", seq + i);

        sort(seq, seq + n);

        if ((seq[1] - seq[0]) != 1) {
            printf ("%d\n", seq[0]);
            continue;
        }
        else if ((seq[n-1] - seq[n-2]) != 1) {
            printf ("%d\n", seq[n-1]);
            continue;
        }
        else {
            for (int i = 2; i < n; i++)
                if ((seq[i] - seq[i-1]) != 1) {
                    printf ("%d\n", seq[i]);
                    break;
                }
        }
    }

    return 0;
}

