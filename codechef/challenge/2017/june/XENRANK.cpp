#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        unsigned long long x, y;
        int u, v;

        scanf ("%d %d", &u, &v);

        y = (1ULL*v)*(1ULL + 1ULL*v)/2ULL + 1ULL;
        x = 1ULL*u*((2ULL+v) + (1ULL+v+u))/2ULL;

        printf ("%llu\n", y + x);
    }

    return 0;
}

