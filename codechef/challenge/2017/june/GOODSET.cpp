#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);
    
    while (t--)
    {
        int n;
        vector <int> v;

        scanf ("%d", &n);

        for (int i = 499; i >= 399; i--)
            v.push_back(i);

        printf ("%d", v[0]);
        for (int i = 2; i <= n; i++)
            printf (" %d", v[i]);
        putchar ('\n');
    }

    return 0;
}

