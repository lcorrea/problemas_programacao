#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;
typedef priority_queue < ii, vector < ii >, greater < ii > > min_pq;

void connect (min_pq &pq, int pos, char line[100001], int dist[100001], int n)
{
	if (pos > 0 and line[pos-1] == '0')
	{
		ii v1 = make_pair (dist[pos] - dist[pos-1], pos - 1);
		pq.push(v1);
	}

	if (pos < n - 1 and line[pos + 1] == '0')
	{
		ii v2 = make_pair (dist[pos+1] - dist[pos], pos+1);
		pq.push(v2);
	}
}

int main (void)
{
	int t;

	scanf ("%d%*c", &t);

	while (t--)
	{
		int n, v;
		long long len = 0;
		char line[100001];
		int dist[100001];
		bool visited[100001];
		min_pq pq;

		scanf ("%d%*c", &n);
		scanf ("%s%*c", line);

		v = n;

		for (int i = 0; i < n; i++)
			scanf ("%d%*c", &dist[i]), visited[i] = false;

		for (int i = 0; i < n; i++)
			if (line[i] == '1')
				connect (pq, i, line, dist, n), v--;

		while (v)
		{
			ii v1 = pq.top(); pq.pop();

			if (!visited[v1.second])
			{
				visited[v1.second] = true;
				len += v1.first;
				line[v1.second] = '1';
				connect (pq, v1.second, line, dist, n);
				v--;
			}
		}

		printf ("%lld\n", len);
	}

	return 0;
}

