#include <bits/stdc++.h>

using namespace std;

void factors (long long n, map<int, int> &mp, bool *found)
{
	if (*found) return;

	long long z = 2;

	while (z*z <= n and !(*found))
	{
		if (n%z == 0)
		{
			mp[z]++;

			*found = mp[z] >= 2;

			if (*found) { printf ("%lld\n", z); break;}

			n /= z;
		}
		else
			z++;
	}

	if (n > 1)
	{
		mp[n]++;
		*found = mp[n] >= 2;
		if (found)
			printf ("%lld\n", n);
	}
}

int main (void)
{
	int t, n;

	scanf ("%d", &t);

	while (t--)
	{
		map <int, int> mp;
		bool found = false;
		scanf ("%d", &n);

		for (int i = 0; i < n; i++)
		{
			long long f;
			scanf ("%lld", &f);
			if (!found)
				factors(f, mp, &found);
		}

	}

	return 0;
}

