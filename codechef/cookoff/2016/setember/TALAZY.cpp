#include <bits/stdc++.h>

using namespace std;

long long int calc (long long int n, long long int m, long long int b)
{
    if (n == 1)
        return m;

    if (n%2)
        return m*((n+1ll)/2ll) + b + calc (n - (n+1ll)/2ll, 2ll*m, b);
    else
        return m*n/2ll + b + calc (n/2ll, 2ll*m, b);
}

int main (void)
{
    int t;
    long long int n, b, m;

    scanf ("%d", &t);

    while (t--)
    {

        scanf ("%lld%lld%lld", &n, &b, &m);

        printf ("%lld\n", calc(n, m, b));
    }
    
    return 0;
}

