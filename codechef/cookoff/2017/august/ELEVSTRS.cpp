#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n, v1, v2;

        scanf ("%d %d %d", &n, &v1, &v2);

        double elev = 2.0*n / (double) v2;
        double stairs = sqrt(2)*n / (double) v1;

        puts (elev < stairs ? "Elevator" : "Stairs");
    }   

    return 0;
}

