#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n, k;

        scanf ("%d %d", &n, &k);

        vector <int> v(n);

        for (int i = 0; i < n; i++)
            scanf ("%d", &v[i]);
        sort(v.begin(), v.end());
        
        for (int i = 0; i < k; i++)
            v.push_back(1000);

        printf ("%d\n", v[(n+k)>>1]);
    }

    return 0;
}

