#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

int main()
{
    int t;

    scanf ("%d", &t);

    ull prefix[100001] = {0};

    ull i = 2;
    while (i <= 100000)
    {
        prefix[i] += prefix[i-1] + i*i;
        i++;
    }

    while (t--)
    {
        ull y, cnt = 0;

        scanf ("%llu", &y);

        ull i = (ull)sqrt(y);
        printf ("%llu %llu\n", i, prefix[i]);
        cnt = y*i;
        printf ("%llu\n", cnt - prefix[i]);
    }

    return 0;
}

