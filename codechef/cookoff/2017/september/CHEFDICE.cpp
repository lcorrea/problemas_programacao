#include <bits/stdc++.h>

using namespace std;

bool is_sol(int sol[7], vector < set<int> > &denied)
{
    for (int i = 1; i <= 6; i++)
        if (sol[i] == 0 or denied[sol[i]].find(i) != denied[sol[i]].end())
            return false;
    return true;
}

bool try_all (int i, vector < set<int> > &denied, int sol[7])
{
    if (is_sol(sol, denied))
        return true;

    for (int j = 1; j <= 6; j++)
    {
        if (i==j)continue;

        if (sol[j] == i)
        {
            sol[i] = j;
            if (try_all(i+1, denied, sol))
                return true;
            else
            {
                sol[i] = 0;
                sol[j] = 0;
                return false;
            }
        }
        else if (denied[i].find(j) == denied[i].end() and sol[j] == 0)
        {
            sol[i] = j;
            sol[j] = i;
            if (try_all(i+1, denied, sol))
                return true;
            sol[i] = 0;
            sol[j] = 0;
        }
    }

    return false;
}
 
int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int moves_ok = true;
        int n, last;
        vector < set <int> > denied(7);

        scanf ("%d", &n);
        scanf ("%d", &last);

        for (int i = 1, a; i < n; i++)
        {
            scanf ("%d", &a);

            if (last == a)
                moves_ok = false;

            denied[last].insert(a);
            denied[a].insert(last);
            last = a;
        }

        if (!moves_ok)
        {
            puts ("-1");
            continue;
        }

        int sol[7] = {0};

        if (!try_all(1, denied, sol))
            puts ("-1");
        else
            for (int i = 1; i <= 6; i++)
                printf ("%d%c", sol[i], i==6?'\n':' ');
    }

    return 0;
}

