#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {

        map <string, int> tb;
        int n;

        scanf ("%d", &n);

        tb["cakewalk"] = 0;
        tb["simple"] = 0;
        tb["easy"] = tb["easy-medium"] = 0;
        tb["medium"] = tb["medium-hard"] = 0;
        tb["hard"] = 0;

        for (int i = 0; i < n; i++)
        {
            char str[100];

            scanf ("%s", str);

            tb[str]++;
        }

        if (tb["cakewalk"] >= 1)
            if (tb["simple"] >= 1)
                if (tb["easy"] >= 1)
                    if (tb["easy-medium"] >= 1 or tb["medium"] >= 1)
                        if (tb["medium-hard"] >= 1 or tb["hard"] >= 1)
                        {
                            puts ("Yes");
                            continue;
                        }
        puts ("No");
    }

    return 0;
}

