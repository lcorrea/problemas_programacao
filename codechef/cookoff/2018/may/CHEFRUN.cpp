#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf("%d", &t);

    while (t--) {
        int x1, x2, x3, v1, v2;

        scanf("%d %d %d", &x1, &x2, &x3);
        scanf("%d %d", &v1, &v2);

        double t1 = (x3 - x1) / (double) v1;
        double t2 = (x2 - x3) / (double) v2;

        if (t1 > t2)
            puts("Kefa");
        else if (t2 > t1)
            puts("Chef");
        else
            puts("Draw");
    }

    return 0;
}

