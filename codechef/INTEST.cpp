#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, k, i;
	int cnt = 0;

	scanf ("%d%d", &n, &k);

	while (n--)
	{
		scanf ("%d", &i);
		cnt += !(i%k);
	}

	printf ("%d\n", cnt);

	return 0;
}

