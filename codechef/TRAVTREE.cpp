#include <bits/stdc++.h>

using namespace std;

typedef vector <int> vi;
typedef vector < vi > tree;

tree world, route;
vi visited;

void print_path (set<int> &intersect, vi &p, int a, int b, int day)
{
	for (int i = 0, sz = route[a].size(); i < sz; i++)
		intersect.insert (route[a][i]);

	route[a].push_back(day);
	if (a == b)
		return;

	print_path (intersect, p, p[a], b, day);
}

int main (void)
{
	int n, days;

	scanf ("%d", &n);

	world.resize(n);
	route.resize(n);

	for (int i = 1, a, b; i < n; i++)
	{
		scanf ("%d %d", &a, &b);
		
		world[a-1].push_back(b-1);
		world[b-1].push_back(a-1);
	}

	scanf ("%d", &days);

	vi p(n);

	for (int i = 1, a, b; i <= days; i++)
	{
		queue <int> q;
		visited.assign(n, 0);

		scanf ("%d%d", &a, &b);
		q.push(a-1);
		
		while (!q.empty())
		{
			int u = q.front(); q.pop();

			if (u == b-1) break;

			for (int j = 0, sz = (int) world[u].size(); j < sz; j++)
			{
				int v = world[u][j];

				if (!visited[v])
				{
					visited[v] = 1;
					q.push(v);
					p[v] = u;
				}
			}
		}

		set <int> intersect;

		//puts ("\nPath\n-----");
		print_path (intersect, p, b-1, a-1, i);
		//puts ("-----\nans: ");

		printf ("%d\n", intersect.size());

		//puts ("\nend\n");


		//path (intersect, p, route, i, b, a);
	}


	return 0;
}

