#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  set<char> k;
  string str;

  cin >> str;

  for (int i = 0; str[i]; i++)
    k.insert (str[i]);

  int n;

  cin >> n;

  while (n--)
  {
    bool ok = true;

    cin >> str;

    for (int i = 0; str[i] and ok; i++)
      if (k.find(str[i]) == k.end())
          ok = false;
    
    puts (ok ? "Yes" : "No");
  }

  return 0;
}

