#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout
    
    tb = [0] * 10001
    w = [0] * 10001
    c = [0] * 10001
    
    input_data = map(int, stdin.read().split())
    
    tests = next(input_data)
    
    for t in range(tests):
        
        e = next(input_data)
        f = next(input_data)
        n = next(input_data)

        for i in range(n):
            c[i] = next(input_data)
            w[i] = next(input_data)
    
        W = f - e
        
        for wi in range(1, W+1):
            tb[wi] = 1000000000
            for i in range(n):
                x = wi - w[i]
                if (x >= 0 and tb[x] != 1000000000):
                    if (tb[wi] > tb[x] + c[i]):
                        tb[wi] = tb[x] + c[i]
        
        if (tb[W] == 1000000000):
            stdout.write("This is impossible.\n")
        else:
            stdout.write("The minimum amount of money in the piggy-bank is {:d}.\n".format(tb[W]))

if __name__ == "__main__":
    main()
