#!/usr/bin/env python3

def backpack(V, mainGoods, itens):

    tb = [[0 for i in range(V+1)] for i in range(62)]
    k = 1
    
    for good in mainGoods:
        for v in range(10, V+1, 10): #sempre multiplo de 10

            item, dep = good

            tb[k][v] = tb[k-1][v]

            ndep = len(dep)

            for mask in range(1 << ndep): #gera o conjunto
                
                vi = itens[item][0]
                p = itens[item][0] * itens[item][1]
                
                for i in range(ndep): #processa o conjunto
                    
                    if ((1 << i) & mask):
                        vdep, cdep = itens[dep[i]]
                        vi += vdep
                        p += vdep * cdep

                #solucao dp
                if (v >= vi):
                    p += tb[k-1][v-vi]
                    if (p > tb[k][v]):
                        tb[k][v] = p
        k += 1

    return tb[k-1][V]

def main():
    
    from sys import stdin, stdout

    T = int(stdin.readline())

    attach = [False] * 62
    itens = [[0,0] for i in range(62)]

    for i in range(T):
        
        V, N = map(int, stdin.readline().split())
        
        deps = [[] for i in range(N+1)]

        for j in range(1, N+1):

            v, c, u = map(int, stdin.readline().split())

            if (u):
                deps[u].append(j)
                attach[j] = True
            else:
                attach[j] = False

            itens[j][:] = v, c

        mainGoods = ((i, deps[i]) for att, i in zip(attach, range(N+1)) if not att)

        next(mainGoods) #1-indexed

        print(backpack(V, mainGoods, itens))

if __name__ == "__main__":
    main()

