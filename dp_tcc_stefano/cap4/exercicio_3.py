#!/usr/bin/env python3

#
# exercicio 3
#

def recursive_binary_search(x, v, ini, fim):
    if (ini == fim):
        return v[ini] == x
    check = ini + ((fim - ini) // 2)
    if (v[check] > x):
        fim = check
        return recursive_binary_search(x, v, ini, fim)
    elif (v[check] < x):
        ini = check + 1
        return recursive_binary_search(x, v, ini, fim)
    return True

v = [10, 23, 43, 50, 55, 70, 80, 99, 100]

print(recursive_binary_search(23, v, 0, len(v) - 1))

