#!/usr/bin/env python3

#
# exercicio 1:
#
# sum(i, n, v) calcula a soma dos elementos positivos do indice i ate n-1
#
def sum(i, n, v):

    if (i == n):
        return 0

    return (v[i] if (v[i] >= 0) else 0) + sum(i+1, n, v)


v = [1, 4, 9, -128, 0, 5, -9, -19]

print(sum(0, len(v), v))
