#!/usr/bin/env python3

#
# exercicio 1:
#
# sum(i, n, v) calcula a soma dos elementos positivos do indice i ate n-1
#
def sum(i, n, v):

    if (i == n-1):
        return v[i] if (v[i] >= 0) else 0

    return (v[i] if (v[i] >= 0) else 0) + sum(i+1, n, v)


v = [1, 4, 9, -128, 0, 5, -9, -19]

print(sum(0, len(v), v))

#
# exercicio 2
#
# retorna True se o valor x está entre os elementos de indice i até n-1
#
# vetor para busca: v
# valor buscado: x
# tamanho do vetor: n
# estado atual: i
#
#

def recursive_linear_search(i, n, v, x):
    if (i == n or v[i] > x):
        return False
    
    if (v[i] == x):
        return True
    
    return recursive_linear_search(i+1, n, v, x)


v = [3, 4, 5, 6, 9, 10, 78, 234, 554, 1000]

print(recursive_linear_search(0, len(v), v, 2))

#
# exercicio 3
#

def recursive_binary_search(x, v, ini, fim):
    if (ini == fim):
        return v[ini] == x

    check = ini + ((fim - ini) // 2)
    
    if (v[check] > x):
        fim = check
        return recursive_binary_search(x, v, ini, fim)
    elif (v[check] < x):
        ini = check + 1
        return recursive_binary_search(x, v, ini, fim)
    return True

v = [10, 23, 43, 50, 55, 70, 80, 99, 100]

print(recursive_binary_search(23, v, 0, len(v) - 1))

#
# exercicio 4
#

def hanoi_tower_solver(n, ini = 1, fim = 3, free = 2):
    moves = 1
    
    if (n == 1):
        print('move peça 1 de %d para %d' % (ini, fim))
        return 1
    else:
        moves += hanoi_tower_solver(n-1, ini, free, fim)

    print('move peça %d de %d para %d' % (n, ini, fim))

    moves += hanoi_tower_solver(n-1, free, fim, ini)

    return moves

n = int(input())

print(hanoi_tower_solver(n))

#
# exercicio 5
#

def binomio_newton(n, p):

    #
    #
    # formula recursiva pode ser derivada do triangulo de pascal
    # que tambem representa os coeficientes gerados no desenvolvimento
    # de um binomio elevado a uma potencia.
    #
    # C(p, n) = C(n-1, p-1) + C(n-1, p)
    #
    #

    if (p > n):
        return 0

    if (n == 1 or p == 0):
        return 1

    return binomio_newton(n-1, p-1) + binomio_newton(n-1, p)

print(binomio_newton(5, 2))

