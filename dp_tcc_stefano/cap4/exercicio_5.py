#!/usr/bin/env python3

#
# exercicio 5
#

def binomio_newton(n, p):

    #
    #
    # formula recursiva pode ser derivada do triangulo de pascal
    # que tambem representa os coeficientes gerados no desenvolvimento
    # de um binomio elevado a uma potencia.
    #
    # C(p, n) = C(n-1, p-1) + C(n-1, p)
    # 

    if (p > n):
        return 0

    if (n == 1 or p == 0):
        return 1

    return binomio_newton(n-1, p-1) + binomio_newton(n-1, p)

print(binomio_newton(5, 2))
