#!/usr/bin/env python3

#
# exercicio 2
#
# retorna True se o valor x está entre os elementos de indice i até n-1
#
# vetor para busca: v
# valor buscado: x
# tamanho do vetor: n
# estado atual: i
#
#

def recursive_linear_search(i, n, v, x):
    if (i == n or v[i] > x):
        return False
    
    if (v[i] == x):
        return True
    
    return recursive_linear_search(i+1, n, v, x)


v = [3, 4, 5, 6, 9, 10, 78, 234, 554, 1000]

print(recursive_linear_search(0, len(v), v, 2))

