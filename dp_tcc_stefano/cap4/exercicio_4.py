#!/usr/bin/env python3

#
# exercicio 4
#


def hanoi_tower_solver(n, ini = 1, fim = 3, free = 2):
    moves = 1
    
    if (n == 1):
        print('move peça 1 de %d para %d' % (ini, fim))
        return 1
    else:
        moves += hanoi_tower_solver(n-1, ini, free, fim)

    print('move peça %d de %d para %d' % (n, ini, fim))

    moves += hanoi_tower_solver(n-1, free, fim, ini)

    return moves

n = int(input())

print(hanoi_tower_solver(n))
