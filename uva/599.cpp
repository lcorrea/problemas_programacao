#include <bits/stdc++.h>

using namespace std;

vector <char> V;
vector <int> visited(30);
vector < vector <int> > adjList(30);

int dfs(int u)
{
    int cnt = 1;
    visited[u] = 1;

    for (int i = 0; i < (int) adjList[u].size(); i++)
        if (visited[adjList[u][i]]==0)
            cnt += dfs(adjList[u][i]);

    return cnt;
}

int main (void)
{
    int Cases;

    scanf ("%d%*c", &Cases);

    while (Cases--)
    {
        char u, v, f;
        
        for (;;)
        {
            scanf ("%c", &f);

            if (f == '*')
            {
                while((f = getchar()) == '*');
                break;
            }

            scanf ("%c,%c)%*c", &u, &v);

            adjList[u-'A'].push_back(v-'A');
            adjList[v-'A'].push_back(u-'A');
        }
        

        f = '*';
        while (f != '\n')
        {
            v = getchar();
            f = getchar();

            V.push_back(v);
        }

        int trees = 0, acorns = 0;

        for (int i = 0; i < (int) V.size(); i++)
            if (visited[V[i]-'A'] == 0)
                if (dfs(V[i]-'A') == 1)
                    acorns++;
                else
                    trees++;

        printf ("There are %d tree(s)"
                " and %d acorn(s).\n", trees, acorns);
        
        V.clear();
        adjList.clear(); adjList.resize(30);
        visited.assign(30, 0);
    }

    return 0;
}

