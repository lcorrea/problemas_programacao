#include <bits/stdc++.h>

using namespace std;

vector < vector <int> > blocks(25);
int position[25];

void unstack (vector <int> &v, int a)
{
	int i = 0, sz = 0;

	for (i = 0, sz = v.size(); i < sz and v[i] != a; i++);

	for (int j = i + 1; j < sz; j++)
	{
		int back = v[j];

		blocks[back].push_back(back);
		position[back] = back;
	}

	v.erase(v.begin() + i + 1, v.end());
}

void pile (vector <int> &va, vector <int> &vb, int a, int new_pos)
{
	int i = 0, sz = 0;

	for (i = 0, sz = va.size(); i < sz and va[i] != a; i++);

	for (int j = i; j < sz; j++)
	{
		vb.push_back (va[j]);
		position[va[j]] = new_pos;
	}

	va.erase (va.begin() + i, va.end());
}

void print_blocks(int n)
{
	for (int i = 0; i < n; i++)
	{
		printf ("%d:", i);

		for (int j = 0, sz = blocks[i].size(); j < sz; j++)
			printf (" %d", blocks[i][j]);
		putchar ('\n');
	}
}

int main (void)
{
	char instrucao1[20], instrucao2[20];
	int n, a, b;

	for (int i = 0; i < 25; i++)
		blocks[i].push_back(i), position[i] = i;

	scanf ("%d", &n);

	while (scanf ("%s", instrucao1), strcmp (instrucao1, "quit"))
	{
		scanf ("%d %s %d", &a, instrucao2, &b);

		if (a == b or position[a] == position[b]); 
		else if (!strcmp (instrucao1, "move"))
		{
			unstack (blocks[position[a]], a);
			blocks[position[a]].pop_back();
			
			if (!strcmp(instrucao2, "onto"))
				unstack(blocks[position[b]], b);
			
			blocks[position[b]].push_back(a);
			position[a] = position[b];
		}
		else
		{
			if (!strcmp(instrucao2, "onto"))
				unstack(blocks[position[b]], b);

			pile (blocks[position[a]], blocks[position[b]], a, position[b]);
		}

	}

	print_blocks (n);
	
	return 0;
}

