#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {
        priority_queue <int> pq;

        for (int i = 0, a; i < n; i++)
        {
            scanf ("%d", &a);

            pq.push(-a);
        }

        int mCost = 0;
        while (pq.size() >= 2)
        {
            int a = -pq.top(); pq.pop();
            int b = -pq.top(); pq.pop();

            mCost += a + b;

            pq.push(-(a+b));
        }

        printf ("%d\n", mCost);
    }

    return 0;
}

