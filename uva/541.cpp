#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		int cnti = 0, cntj = 0, chi = 0, chj = 0;
		int matrix[110][110] = {0};
		int sum_col[110] = {0}, sum_lin[110] = {0};

		for (int i = 0; i < n; i++)
		{
			int line = 0;

			for (int j = 0; j < n; j++)
			{
				scanf ("%d", &matrix[i][j]);

				sum_col[j] += matrix[i][j];
				line += matrix[i][j];
			}

			sum_lin[i] += line;

			if (line%2) cnti++, chi = i;
		}

		for (int i = 0; i < n; i++)
			if (sum_col[i] % 2)
				cntj++, chj = i;

		if (cntj == 0 and cnti == 0) puts ("OK");
		else if (cntj == 1 and cnti == 1) printf ("Change bit (%d,%d)\n", chi+1, chj+1);
		else puts ("Corrupt");
	}

	return 0;
}

