#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int tab[3][3];
	map <char, int> bottle;

	bottle['B'] = 0;
	bottle['C'] = 2;
	bottle['G'] = 1;

	while (1)
	{
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (scanf ("%d", &tab[i][j]) == EOF)
					return 0;

		char order[4] = "BCG", ans[4] = "BCG";
		unsigned int min_mov = 0xffffffff;

		do
		{
			int mov = 0;

			for (int k = 0; k < 3; k++)
				for (int i = 0; i < 3; i++)
					if (bottle[order[i]] != bottle[order[k]])
						mov += tab[i][bottle[order[k]]];

			if (mov < min_mov)
			{
				strcpy (ans, order);
				min_mov = mov;
			}

		}while (next_permutation(order, order+3));

		printf ("%s %d\n", ans, min_mov);
	}

	return 0;
}

