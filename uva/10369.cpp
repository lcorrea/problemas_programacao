#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

typedef pair <double, int> di;
typedef vector < di > vdi;

priority_queue <di, vdi, greater<di> > pq;

void process (int u, int p, double adjmat[510][510], int visited[510])
{
	visited[u] = 1;

	for (int i = 0; i < p; i++)
		if (!visited[i] and i != u)
			pq.push (make_pair(adjmat[u][i], i));
}

int main (void)
{
	int n;

	scanf ("%d", &n);

	while (n--)
	{
		int s, p;
		int visited[510] = {0};
		double adjmat[510][510] = {0};
		vector < pair <int, int> > points(510);
		priority_queue <double> D;

		scanf ("%d %d", &s, &p);

		for (int i = 0, x, y; i < p; i++)
			scanf ("%d %d", &x, &y), points[i].first = x, points[i].second = y;

		for (int i = 0; i < p; i++)
		{
			for (int j = 0; j < p; j++)
			{
				if (i==j);
				else
					adjmat[i][j] = adjmat[j][i] = hypot (
						points[i].first - points[j].first,
						points[i].second - points[j].second
						);

				printf (" %.2lf", adjmat[i][j]);
			}
			putchar ('\n');
		}

		process (0, p, adjmat, visited);

		while (!pq.empty())
		{
			di outpost = pq.top();

			pq.pop();

			if (!visited[outpost.second])
				D.push(outpost.first), process (outpost.second, p, adjmat, visited);
		}

		for (int i = 0; i < s; i++)
			printf ("%.2lf\n", D.top()), D.pop();

		printf ("%.2lf\n", D.top());
	}

	return 0;
}

