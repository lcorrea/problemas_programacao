#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);
    while(n)
    {
        int limite = 98765 / n;
        bool ok = false;

        for (int b = 1234; b <= limite; b++)
        {
            int a = b*n;
            bool digits[10] = {0};

            if (b < 10000)
                digits[0] = true;

            int tmp = a;
            while(tmp)
                digits[tmp%10] = true, tmp /= 10;

            tmp = b;
            while(tmp)
                digits[tmp%10] = true, tmp /= 10;

            int check = 0;
            for (int digit = 0; digit <= 9; digit++)
                check += digits[digit];

            if (check == 10)
            {
                ok = true;
                printf ("%05d / %05d = %d\n", a, b, n);
            }
        }

        if (!ok)
            printf ("There are no solutions for %d.\n", n);
        
        scanf ("%d", &n);

        if (n)
            putchar('\n');
    }

    return 0;
}

