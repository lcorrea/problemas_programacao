#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	double p;

	while (scanf ("%d %lf%*c", &n, &p) != EOF)
		printf ("%.0lf\n", pow (p, 1.0/((double)n)));

	return 0;
}

