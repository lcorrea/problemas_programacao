#include <bits/stdc++.h>

using namespace std;

class unionFind
{
	vector <int> p, h;
	int totalSets;

	public:
		unionFind(int n)
		{
			totalSets = n;
			p.assign(n, 0);
			h.assign(n, 0);

			for (int i = 0; i < n; i++)
				p[i] = i;
		}

		int findSet (int a)
		{
			return p[a] == a ? a : p[a] = findSet(p[a]);
		}

		bool isSameSet (int a, int b)
		{
			return findSet(a) == findSet(b);
		}

		int getTotalSets()
		{
			return totalSets;
		}

		void join(int a, int b)
		{
			if (isSameSet(a, b)) return;

			int i = p[a], j = p[b];

			totalSets--;

			if (h[i] > h[j]) p[j] = i;
			else
			{
				p[i] = j;

				if (h[i] == h[j])
					h[j]++;
			}
		}
};

int main (void)
{
	int n, m;
	int cnt = 1;

	while (scanf ("%d %d", &n, &m), n and m)
	{
		unionFind sets(n);

		for (int i = 0, a, b; i < m; i++)
		{
			scanf ("%d %d", &a, &b);
			sets.join(a, b);
		}

		printf ("Case %d: %d\n", cnt++, sets.getTotalSets());
	}

	return 0;
}

