#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, C = 0;

    while (scanf ("%d", &n) != EOF)
    {
        unordered_map <int, vector <int> > adjList;
        vector <int> counter(30, 0), current(30, 0);
        int m;
        int awake = 0, y = 0;
        char w[4];

        scanf ("%d", &m);
        scanf ("%s", w);

        counter[w[0]-'A'] = 4;
        counter[w[1]-'A'] = 4;
        counter[w[2]-'A'] = 4;

        for (int i = 0; i < m; i++)
        {
            char l[3];

            scanf ("%s", l);

            adjList[l[0]-'A'].push_back(l[1]-'A');
            adjList[l[1]-'A'].push_back(l[0]-'A');
        }

        bool ok = true;
        while (ok)
        {
            ok = false;

            //puts ("#####");
            for (auto it = adjList.begin(); it != adjList.end(); it++)
            {
                //printf ("%c => %d\n", it->first+'A', current[it->first]);
                if (counter[it->first] < 3)
                {
                    int u = it->first;

                    current[u] = 0;//counter[u];
                    for (int i = 0; i < (int) adjList[u].size(); i++)
                    {
                        int v = adjList[u][i];

                        current[u] += counter[v] >= 3;
                    }
                    
                    
                    if (current[it->first] >= 3)
                        ok = true, awake++;//, printf ("wake up %c (%d)\n", it->first+'A', y);

                }
            }

            //puts ("---");

            for (auto it = adjList.begin(); it != adjList.end(); it++)
                if (counter[it->first] < 3)
                    counter[it->first] = current[it->first];

            y += ok;
        }

        //printf ("%d %d\n", awake, n-3);
        if (awake < n-3)
            puts ("THIS BRAIN NEVER WAKES UP");
        else
            printf ("WAKE UP IN, %d, YEARS\n", y);
    }

    return 0;
}

