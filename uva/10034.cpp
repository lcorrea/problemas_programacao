#include <bits/stdc++.h>

using namespace std;

typedef priority_queue < pair <double, int>, vector < pair <double, int> >, greater< pair<double, int> > > minPriorityQueue;

void process (minPriorityQueue& pq, double **table, int *visited, int v, int node)
{
	visited[node] = 1;

	for (int i = 0; i < v; i++)
		if (!visited[i] and i != node)
			pq.push(make_pair(table[node][i], i));
}

int main (void)
{
	int n;

	cin >> n;

	while (n--)
	{
		int v;

		cin >> v;

		pair <double, double> points[v];
		double **table;
		int *visited;

		table = new double*[v];
		visited = new int[v];

		for (int i = 0; i < v; i++)
		{
			visited[i] = 0;
			table[i] = new double[v];
			for (int j = 0; j < v; j++)
				table[i][j] = 0;

			cin >> points[i].first >> points[i].second;
		}

		for (int i = 0; i < v; i++)
			for (int j = 0; j < v; j++)
				if (!table[i][j] and j != i)
					table[i][j] = table[j][i] = hypot (points[i].first - points[j].first, points[i].second - points[j].second);
		
		double ink = 0.0;
		minPriorityQueue pq;

		process(pq, table, visited, v, 0);

		while (!pq.empty())
		{
			int node = pq.top().second;
			double dist = pq.top().first;

			pq.pop();		

			if (!visited[node])
				ink += dist, process(pq, table, visited, v, node);
		}

		printf ("%.2lf%s", ink, n ? "\n\n" : "\n");
	}

	return 0;
}

