#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

int main (void)
{
	int c, q, s;
	int teste = 1;

	scanf ("%d %d %d", &c, &s, &q);

	while (c and s and q)
	{
		int adjMat[c][c];

		for (int i = 0; i < c; i++)
			for (int j = 0; j < c; j++)
				adjMat[i][j] = INF;

		for (int i = 0, c1, c2, d; i < s; i++)
		{
			scanf ("%d %d %d", &c1, &c2, &d);
			adjMat[c1-1][c2-1] = adjMat[c2-1][c1-1] = d;
		}

		//modified floydW
		for (int k = 0; k < c; k++)
			for (int i = 0; i < c; i++)
				for (int j = 0; j < c; j++)
					adjMat[i][j] = min (adjMat[i][j], max(adjMat[i][k], adjMat[k][j]));

		printf ("Case #%d\n", teste++);
		for (int i = 0, c1, c2; i < q; i++)
		{
			scanf ("%d %d", &c1, &c2);
			
			if(adjMat[c1-1][c2-1] < INF)
				printf ("%d\n", adjMat[c1-1][c2-1]);
			else
				puts ("no path");
		}

		scanf ("%d %d %d", &c, &s, &q);

		if (c and s and q)
			putchar ('\n');
	}

	return 0;
}

