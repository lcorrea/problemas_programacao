#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    priority_queue < pair <int, int> > pq;
    int v[3010] = {0};

    for (;;)
    {
        char str[20];

        scanf ("%s", str);

        if (*str == 'R')
        {
            int id, t;

            scanf ("%d %d", &id, &t);
            
            v[id] = t;
            pq.push(make_pair(-t, -id));
        }
        else break;
    }

    int n;

    scanf ("%d", &n);
    
    while (n--)
    {
        int t = -pq.top().first;
        int id = -pq.top().second; pq.pop();

        pq.push(make_pair(-(t+v[id]), -id));
        printf ("%d\n", id);
    }

    return 0;
}

