#include <iostream>
#include <queue>
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

queue <int> nextToVisit;
vvi graph;
vi visited;

void buscaFamilia (int genitor)
{
	int vtx;

	nextToVisit.push(genitor);
	visited[genitor] = 1;

	while (!nextToVisit.empty())
	{
		vtx = nextToVisit.front();
		nextToVisit.pop();

		for (int i = 0, newVtx = 0; i < graph[vtx].size(); i++)
		{
			newVtx = graph[vtx][i];

			if (visited[newVtx] == -1)
			{
				visited[newVtx] = 1;
				nextToVisit.push (newVtx);
			}
		}
	}
}

int main (void)
{
	int n, m, parente2, parente1;
	int familias = 0;

	cin >> n >> m;

	graph.resize(n+1);
	visited.assign (n+1, -1);

	while (m--)
	{
		cin >> parente1 >> parente2;

		graph[parente1].push_back (parente2);
		graph[parente2].push_back (parente1);
	}

	for (int i = 1; i < graph.size(); i++)
	{
		if (visited[i] == -1)
		{
			familias++;
			buscaFamilia (i);
		}
	}

	cout << familias << endl;

	return 0;
}

