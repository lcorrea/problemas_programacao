#include <stdio.h>

int main(void)
{
	unsigned int a = 0, b = 0, teste = 0, partidas = 0;
	char nomeA[11], nomeB[11];

	scanf("%d%*c", &partidas);
	while(partidas)
	{
		teste++;

		scanf("%10[^\n]%*c", nomeA);
		scanf("%10[^\n]%*c", nomeB);

		printf("Teste %d\n", teste);
		
		do
		{
			scanf("%d%*c", &a);
			scanf("%d%*c", &b);
			a += b;

			if(a%2 == 0)
				printf("%s\n", nomeA);
			else
				printf("%s\n", nomeB);
			partidas--;

		}while(partidas);
		
		putchar('\n');
		
		scanf("%d%*c", &partidas);
	}

	return 0;
}
