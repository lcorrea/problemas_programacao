/*
	problem 10868
	chuva
	by lucas correa
	time 00:37:01
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int m[2][10000], n, i, j;

	cin >> n;

	for(j = 0; j < 2; j++)
	{
		for(i = 0; i < n*n; i++)
			cin >> m[j][i];
	}
	
	for(j = 0; j < n; j++)
	{
		cout << m[0][j*n] + m[1][j*n];

		for(i = 1; i < n; i++)
			cout << ' ' << m[0][j*n+i] + m[1][j*n+i];

		cout << endl;
	}

	return 0;
}

