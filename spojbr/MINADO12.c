#include <stdio.h>

int tab[55];
int main (void)
{
	int n, i, v;

	scanf ("%d", &n);

	for (i = 0; i < n; i++)
	{
		scanf ("%d", &v);
		
		if (v)
		{
			tab[i]++;
			if (i - 1 >= 0) tab[i-1]++;
			if (i + 1 < n) tab[i+1]++;
		}
	}

	for (i = 0; i < n; i++)
		printf ("%d\n", tab[i]);

	return 0;
}

