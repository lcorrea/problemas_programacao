/*
	problem 1894
	jogo de varetas
	by lucas correa
	time: 01:04:54
*/
#include <stdio.h>

int main(void)
{
	unsigned int varetas, Vi, quadrados = 0;//, resto = 0, tmp;

	scanf("%u%*c", &varetas);

	while(varetas)
	{
		while(varetas--)
		{
			scanf("%*u %u%*c", &Vi);
			
			quadrados += Vi/2;
			/*tmp = Vi%4;
			resto += (tmp%2)?(tmp - 1):(tmp);*/
		}

		printf("%u\n", quadrados/2);

		//resto = 0;
		quadrados = 0;

		scanf("%u%*c", &varetas);
	}

	return 0;
}

