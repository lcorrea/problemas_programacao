/*
	problem 19939
	carnaval
	by lucas correa
	time: 00:13:49
*/
#include <cstdio>
#include <algorithm>

using namespace std;

int main(void)
{
	float nota;
	float notas[5];
	unsigned int i = 0;

	for(i = 0; i < 5; i++)
		scanf("%f%*c", &notas[i]);

	sort(notas, notas+5);

	nota = 0;

	for(i = 1; i < 4; i++)
		nota += notas[i];
	
	printf("%.1f\n", nota);

	return 0;
}

