#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <cctype>

using namespace std;

struct time{
	char nome[16];
	unsigned int j;
	unsigned int pts;
	unsigned int gp;
	unsigned int gc;
};

void tabela(struct time *times, unsigned int qnt)
{	
	unsigned int pos = 0;
	struct time timeA = times[0];

	timeA.pts = 24311; //gambiarra do algoritmo para imprimir tabela

	for(unsigned int i = 0; i < qnt; i++)
	{
		if(times[i].pts == timeA.pts && times[i].gp == timeA.gp &&
		   (times[i].gp - times[i].gc) == (timeA.gp - timeA.gc))
			printf("   ");
		else
		{
			timeA = times[i];
			pos = i+1;
			printf("%2u.", pos);
		}
		
		printf(" %15s %3u %3u %3u %3u %3d",
				times[i].nome,
				times[i].pts,
				times[i].j,
				times[i].gp,
				times[i].gc,
				times[i].gp - times[i].gc);
		
		if(times[i].j)
		{
			double percent = times[i].pts*100.0/(times[i].j*3.0);
			printf(" %6.2lf", percent);
		}
		else
			printf("    N/A");
		
		putchar('\n');
	}

}

bool comp(struct time a, struct time b)
{
	if(a.pts == b.pts)
	{
		if((a.gp - a.gc) == (b.gp - b.gc))
		{
			if(a.gp == b.gp)
				return (strcasecmp(a.nome, b.nome) < 0);
			else
				return (a.gp > b.gp);
		}
		else
			return ((int) (a.gp - a.gc) > (int)(b.gp - b.gc));
	}
	else
		return (a.pts > b.pts);

}

int main(void)
{
	unsigned int t, g, i, j, k, gA, gB;
	struct time times[28], timeA, timeB;
	string nomeA, nomeB;

	cin >> t >> g;

	while(t)
	{
		for(i = 0; i < t; i++)
		{
			scanf("%s", times[i].nome);
			
			times[i].gp = 0;
			times[i].gc = 0;
			times[i].j = 0;
			times[i].pts = 0;
		}

		for(i = 0; i < g; i++)
		{
			cin >> nomeA >> gA;

			scanf("%*s");

			cin >> gB >> nomeB;

			for(j = 0; j < t; j++)
			{
				if(nomeA.compare(times[j].nome) == 0)
				{
					times[j].j++;
					times[j].gp += gA;
					times[j].gc += gB;

					for(k = 0; k < t; k++)
					{
						if(nomeB.compare(times[k].nome) == 0)
						{
							times[k].j++;
							times[k].gp += gB;
							times[k].gc += gA;

							if(gA > gB)
								times[j].pts += 3;
							else if(gA < gB)
								times[k].pts += 3;
							else
							{
								times[k].pts += 1;
								times[j].pts += 1;
							}

							break;
						}
					}

					break;
				}
			}
		}

		sort(times, times+t, comp);

		tabela(times, t);

		cout << endl;
		
		cin >> t >> g;
	}

	return 0;
}

