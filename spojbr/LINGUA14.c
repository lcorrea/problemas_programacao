#include <stdio.h>

int main (void)
{
	char c;
	int p = 0;

	c = getchar ();
	while (c != '\n')
	{
		if (c == ' ' || p)
			putchar (c), p = 1;
		p = !p;
		
		c = getchar ();
	}
	putchar (c);

	return 0;
}

