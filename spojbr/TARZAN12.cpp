#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

void dfs (int vtx);

typedef vector <int> vi;
typedef vector <vi> vii;

vi visitados;
vii graph;

void dfs (int vtx)
{
	visitados[vtx] = 1;

	for (unsigned int i = 0; i < graph[vtx].size(); i++)
	{
		if (visitados[graph[vtx][i]] == -1)
			dfs (graph[vtx][i]);
	}
}

int main (void)
{
	int n, d, xi, yi, cnt = 0;

	vi x, y;

	cin >> n >> d;

	graph.resize(n);
	visitados.assign(n, -1);

	for (unsigned int i = 0; i < n; i++)
	{
		cin >> xi >> yi;
		
		x.push_back(xi), y.push_back(yi);
	}

	for (unsigned int i = 0; i < n; i++)
	{
		for (unsigned int j = 0; j < n; j++)
		{
			if (hypot(x[i]-x[j], y[i]-y[j]) <= d && i != j)
			{
				graph[i].push_back(j);
				graph[j].push_back(i);
			}
		}
	}

	for (unsigned int i = 0; i < graph.size(); i++)
	{
		if (visitados[i] == -1)
		{
			cnt++;
			dfs (i);
		}

	}

	if (cnt == 1)
		cout << 'S' << endl;
	else
		cout << 'N' << endl;

	return 0;
}

