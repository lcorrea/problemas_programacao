/*
	problem 5477
	troca de cartas
	by lucas correa
	time: 00:56:17
*/
#include <iostream>
#include <algorithm>
#include <set>
#include <vector>

using namespace std;

int main(void)
{
	set<unsigned int> A, B;
	vector<unsigned int> possib;
	vector<unsigned int>::const_iterator ita;
	unsigned int qntA, qntB, carta, ATroc, BTroc;

	cin >> qntA >> qntB;

	while(qntA)
	{
		possib.clear();
		possib.resize(max(qntA, qntB));
		
		A.clear();
		B.clear();

		while(qntA--)
		{
			cin >> carta;
			A.insert(carta);
		}

		while(qntB--)
		{
			cin >> carta;
			B.insert(carta);
		}
		
		ita = set_difference(A.begin(), A.end(),
				      B.begin(), B.end(),
				      possib.begin());

		ATroc = ita - possib.begin();

		ita = set_difference(B.begin(), B.end(),
				      A.begin(), A.end(),
				      possib.begin());
		
		BTroc = ita - possib.begin();

		cout << min(ATroc, BTroc) << endl;

		cin >> qntA >> qntB;
	}

	return 0;
}

