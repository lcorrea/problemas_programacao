/*
	problem 11078
	braceletes magicos
	by lucas correa
	time: 01:29:19 //aprendendo c++ stl
*/
#include <iostream>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>

using namespace std;

unsigned int check(char *seq, char *bracel)
{
	string bracelete(bracel);
	string cmp = bracelete;

	cmp.append(bracelete);

	if(string::npos != cmp.find(seq))
		return 1;
		
	reverse(bracelete.begin(), bracelete.end());
	
	if(string::npos == bracelete.find(seq))
		return 0;
	
	return 1;
}


int main(void)
{
	char bracelete[10001], sequencia[101];
	unsigned int t = 0;

	scanf("%u%*c", &t);

	while(t--)
	{
		scanf("%s %[^\n]%*c", sequencia, bracelete);
		if(check(sequencia, bracelete))
			puts("S");
		else
			puts("N");
	}

	return 0;
}

