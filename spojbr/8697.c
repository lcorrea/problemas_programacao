#include <stdio.h>

int main(void)
{
	unsigned int n = 0, m = 0;

	while(scanf("%d%*c%d%*c", &n, &m) != EOF)
	{
		printf("%d\n", n - m);
	}
	return 0;
}
