#include <stdio.h>

int main(void)
{
	unsigned int N;
	int x = 0, soma = 0;
	
	scanf("%d%*c", &N);
	while(N--)
	{
		scanf("%d%*c", &x);
		soma += x;
	}
	printf("%d\n", soma);
	return 0;		
}
