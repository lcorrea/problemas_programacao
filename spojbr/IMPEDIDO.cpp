/*
	problem 2928
	ele esta impedido
	by lucas correa
	time: 00:37:57
*/

#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	unsigned short int a, d, i;
	unsigned int zaga[11], atac, artilheiro = 10e4;

	cin >> a >> d;

	while(a)
	{
		artilheiro = 10001;

		for(i = 0; i < a; i++)
		{
			cin >> atac;
			if(atac < artilheiro)
				artilheiro = atac;
		}

		for(i = 0; i < d; i++)
			cin >> zaga[i];

		sort(zaga, zaga + d);

		if(artilheiro < zaga[1])
			cout << 'Y' << endl;
		else if((artilheiro == zaga[1] && artilheiro == zaga[0]) || artilheiro >= zaga[1])
			cout << 'N' << endl;

		cin >> a >> d;
	}
	
	return 0;
}

