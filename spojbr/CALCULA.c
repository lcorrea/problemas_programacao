/*
	problem 1334
	Calculando
	by lucas correa
	time: 00:11:34
*/
#include <stdio.h>

int main(void)
{
	int sum = 0, num = 0;
	unsigned int m, casoTeste = 1;

	scanf("%u%*c", &m);
	while(m)
	{
		while(m--)
		{
			scanf("%d", &num);
			sum += num;
		}

		printf("Teste %u\n%d\n\n", casoTeste++, sum);
		sum = 0;
		scanf("%*c%u%*c", &m);
	}

	return 0;
}
