#include <bits/stdc++.h>

using namespace std;

typedef struct
{
  int pts;
  int start;
  int end;
}Task;

int dp (int i, int cur_time, vector <Task> &task, int mem[10001])
{
  int faz = 0, nao_faz = 0;

  if (i < 0)
    return 0;

  if (mem[i] != -1)
  {
    printf ("reused %d => %d\n", i, mem[i]);
    return mem[i];
  }

  if (task[i].end <= cur_time)
    faz = task[i].pts + dp (i - 1, task[i].start, task, mem);

  nao_faz = dp(i - 1, cur_time, task, mem);

  return mem[i] = max (nao_faz, faz);
}

bool cmp (Task a, Task b)
{
  if (a.start == b.start)
  {
    if (a.end == b.end)
      return a.pts > b.pts;
    return a.end < b.end;
  }

  return a.start < b.start;
}

int main (void)
{
  int n;
  const char dias[5][4] = {"Seg", "Ter", "Qua", "Qui", "Sex"};

  while (scanf ("%d", &n), n)
  {
    int pts_dia[5] = {0};
    vector <vector <Task> > tasks(5);

    for (int i = 0; i < n; i++)
    {
      char dia[4];
      int pts;
      int s_m, s_h;
      int e_m, e_h;

      scanf ("%*d %d %s %d:%d %d:%d", &pts, dia, &s_h, &s_m, &e_h, &e_m);

      Task t;
      
      t.pts = pts;
      t.start = s_m + s_h*60;
      t.end = e_m + e_h*60;

      for (int d = 0; d < 5; d++)
        if (!strcmp(dia, dias[d]))
        {
          tasks[d].push_back (t);
          break;
        }
    }

    int total = 0;
    for (int i = 0; i < 5; i++)
    {
      if (tasks[i].size() > 0)
      {
        int cur_time = 24*60;
        int day_tasks = tasks[i].size();
        int mem[10001];

        memset (mem, -1, sizeof(mem));
        sort (tasks[i].begin(), tasks[i].end(), cmp);

        pts_dia[i] = dp(day_tasks - 1, cur_time, tasks[i], mem);
        total += pts_dia[i];
      }
    }

    printf ("Total de pontos: %d\n", total);
    for (int i = 0; i < 5; i++)
      printf ("%s: %d\n", dias[i], pts_dia[i]);

  }

  return 0;
}

