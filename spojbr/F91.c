/*
	problem 1831
	F91
	by lucas correa
	time: 00:08:03
*/

#include <stdio.h>

/* --- pegadinha ---
int f91(int n)
{
	return (n<=100)?(f91(f91(n + 11))):(n - 10);
}
*/

int main(void)
{
	int n;
	
	scanf("%d%*c", &n);

	while(n)
	{
		/* 'pegadinha'
		printf("f91(%d) = %d\n", n, f91(n));
		*/
		printf("f91(%d) = %d\n", n, (n>100)?(n-10):(91));
		scanf("%d%*c", &n);
	}

	return 0;
}

