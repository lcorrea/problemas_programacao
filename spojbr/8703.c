#include <stdio.h>

int main(void)
{
	unsigned int n, val = 7;

	scanf("%u%*c", &n);

	if(n>100)
	{
		val += (n - 100)*5;
		n -= n - 100;
	}

	if(n>30)
	{
		val += (n - 30)*2;
		n -= n - 30;
	}

	if(n>10)
	{
		val += (n - 10);
		n -= n - 10;
	}
	
	printf("%u\n", val);

	return 0;
}

