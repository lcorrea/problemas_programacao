#include <stdio.h>

int main(void)
{
	unsigned int N = 0, div = 2;
	
	scanf("%d%*c", &N);
	while(div < N)
	{
		if(N % div == 0)
		{
			puts("nao");
			return 0;
		}

		div++;
	}
	
	puts("sim");
	return 0;
}
