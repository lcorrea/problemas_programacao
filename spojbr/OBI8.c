#include <stdio.h>
#include <stdlib.h>

int main (void)
{
	int n, p, total =  0,p1, p2;

	scanf ("%d %d", &n, &p);

	while (n--)
	{
		scanf ("%d %d", &p1, &p2);

		total += ((p1 + p2) >= p);
	}

	printf ("%d\n", total);

	return 0;
}

