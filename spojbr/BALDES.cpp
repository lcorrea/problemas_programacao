#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>

using namespace std;

int main (void)
{
	int v[100000];
	int n, c, b;

	cin >> n;

	while (n)
	{
		vector <int> v (n);
		c = 0;

		for (int i = 0; i < n; i++)
			cin >> v[i];

		for (int i = 0; i < n; i++)
		{
			if (v[i] != i+1)
			{
				c += abs (i - v[v[i] - 1])*2 - 1;
				swap (v[i], v[v[i] - 1]);
				i--;
			}
		}

		if (c % 2)
			cout << "Marcelo" << endl;
		else
			cout << "Carlos" << endl;

		cin >> n;
	}

	return 0;
}

