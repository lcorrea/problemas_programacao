/*
	problem 1734
	quem vai ser reprovado
	by lucas correa
	time: 00:39:15 (aprendendo stl)
*/
#include <iostream>
#include <utility>

using namespace std;

int main(void)
{
	unsigned short int n = 0;
	unsigned int instancia = 1;
	pair<string, unsigned short int> reprovado, aluno;

	cin >> n;

	while(!cin.eof())
	{
		cin >> reprovado.first >> reprovado.second;
		
		while(--n)
		{
			cin >> aluno.first >> aluno.second;

			if(aluno.second < reprovado.second)
				reprovado = aluno;
			else if(aluno.second == reprovado.second)
			{
				if(aluno.first > reprovado.first)
					reprovado = aluno;
			}
		}

		cout << "Instancia " << instancia << '\n';
		cout << reprovado.first << '\n' << endl;

		instancia++;

		cin >> n;
	}

	return 0;
}

