#include <iostream>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

vector<unsigned int> km;
set<unsigned int> aux;

unsigned int eddingtonN(void)
{
	unsigned int x, cnt;
	set<unsigned int> E;
	set<unsigned int>::const_iterator it;

	sort(km.begin(), km.end());

	for(it = aux.begin(); it != aux.end(); it++)
	{

		cnt = count(km.begin(), km.end(), *it);

		cnt += km.size() - (upper_bound(km.begin(),km.end(), *it) - km.begin());
		E.insert(min(*it, cnt));
	}

	return *E.rbegin();

}


int main(void)
{
	unsigned int n, x;
	unsigned int E;

	cin >> n;

	while(n)
	{
		km.clear();
		aux.clear();

		while(n--)
		{
			cin >> x;

			km.push_back(x);
			aux.insert(x);
		}

		E = eddingtonN();

		cout << E << endl;

		cin >> n;
	}

	return 0;
}

