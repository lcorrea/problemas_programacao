#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {

        int v[n], *maxn = v;

        memset (v, 0, sizeof(int)*n);

        for (int i = 0; i < n; i++)
            for (int j = 0, x; j < n; j++)
                scanf ("%d", &x),
                      v[j] += x,
                      maxn = *maxn < v[j] ? v + j : maxn;

        printf ("%d\n", *maxn);
    }

    return 0;
}

