#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmp (const void *a, const void *b) { return *((const int *)a) - *((const int *)b); }

int main (void)
{
	int n, k, env;
	int envelopes[1000010];

	scanf ("%d %d", &n, &k);
	memset (envelopes, 0, sizeof(int)*(k+1));

	while (n--)
	{
		scanf ("%d", &env);
		envelopes[env-1]++;
	}

	qsort (envelopes, k, sizeof(int), cmp);
	printf ("%d\n", envelopes[0]);

	return 0;
}

