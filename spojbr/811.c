#include <stdio.h>

int main(void)
{
	unsigned int n = 0, i = 0;
	int tmp, r;
	unsigned int teste = 1;
	
	scanf("%u%*c", &n);
	while(n)
	{
		i = 0;
		
		do
		{
			scanf("%d%*c", &tmp);
			if(++i == tmp)
				r = tmp;
		}while(--n);

		printf("Teste %u\n%u\n", teste++, r);

		scanf("%u%*c", &n);
	}

	return 0;
}

