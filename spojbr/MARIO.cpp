/*
	problem 1890
	mario
	by lucas correa

*/

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	vector<int> M;
	vector<long int>::const_iterator f;
	int l, mov, i, cnt, e;

	cin >> e >> l;

	while(e || l)
	{
		mov = 0;
		M.clear();
		M.resize(l, 0);

		for(i = 0; i < l; i++)
			cin >> M[i];

		for(i = 0; i < l; i++)
		{
			cnt = upper_bound(M.begin(), M.end(), M[i] + (e-1)) - M.begin();
			
			mov = max(cnt - i, mov);
		}

		cout << e - mov << endl;

		cin >> e >> l;
	}

	return 0;
}

