/*
	problem 3773
	ELEICOES
	by lucas correa
	time: 00:25:38
*/
#include <iostream>
#include <algorithm>
#include <map>
#include <utility>

using namespace std;

bool comp(pair<int, unsigned int> a, pair<int, unsigned int> b)
{
	return (a.second < b.second); 
}

int main(void)
{
	map<int, unsigned int> votos;
	map<int, unsigned int>::iterator it;
	unsigned int n;
	int eleitor;
	
	cin >> n;

	while(n--)
	{
		cin >> eleitor;

		it = votos.find(eleitor);

		if(it != votos.end())
			it->second++;
		else
			votos.insert(pair<int, unsigned int>(eleitor, 1));
	}

	pair<int, unsigned int> vencedor  = *max_element(votos.begin(), votos.end(), comp);

	cout << vencedor.first << endl;

	return 0;	
}

