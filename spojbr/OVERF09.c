/*
	problem 11647
	overflow
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	unsigned int max, p, q;
	char op;
	
	while(scanf("%d%*c", &max) != EOF)
	{
		scanf("%d %c %d%*c", &p, &op, &q);

		if(op == '*')
			p *= q;
		else
			p += q;

		if(p <= max)
		{
			puts("OK");
			continue;
		}
		
		puts("OVERFLOW");
	}
	return 0;
}
