#include <stdio.h>

int main (void)
{
	char p, c;
	int word = 1;
	int nwords = 0, total = 0;

	p = getchar(); getchar();
	c = getchar();
	while (c != '\n')
	{
		if (c == ' ')
			word = 1, total++;

		if (word && c == p)
			nwords++, word = 0;

		c = getchar();
	}
	total++;

	printf ("%.1f\n", ((float)nwords) / total * 100.0);

	return 0;
}

