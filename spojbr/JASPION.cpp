/*
	problem 3087
	o fantastico jaspion
	by lucas correa
	time: 00:51:13
*/

#include <iostream>
#include <cstdio>
#include <cstring>
#include <map>
#include <utility>

using namespace std;

int main(void)
{
	char c;
	unsigned int instancias = 0, lines, palavras, j, i;
	map<string, string> dicio;
	string palavra, significado;

	cin >> instancias;

	while(instancias--)
	{
		dicio.clear();

		scanf("%u %u%*c", &palavras, &lines);

		while(palavras--)
		{
			getline(cin, palavra);
			if(palavra.size())
				getline(cin, dicio[palavra]);
		}
		
		while(lines--)
		{
			i = 0;
			getline(cin, palavra);
			
			while(i < palavra.size())
			{
				significado.clear();
				while(i < palavra.size())
				{
					significado.push_back(palavra[i++]);
					
					if(palavra[i] == ' ')
						break;
				}

				if(dicio.find(significado) != dicio.end())
					cout << dicio[significado];
				else
					cout << significado;

				if(palavra[i++] == ' ')
					cout << ' ';
			}

			cout << endl;
		}

		cout << endl;
	}

	return 0;
}

