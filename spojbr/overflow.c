#include <stdio.h>

int main (void)
{
	long long int max;
	int a, b;
	char c;
	int resp = 0;

	scanf ("%lld%*c%d %c %d", &max, &a, &c, &b);
	
	switch (c)
	{
		case ('+'):
			resp = a + b <= max;
			break;
		default:
			resp = a * b <= max;
			break;
	}

	puts ((resp) ? ("OK") : ("OVERFLOW"));

	return 0;
}

