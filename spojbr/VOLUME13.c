#include <stdio.h>

int main (void)
{
	int v, t, p;

	scanf ("%d %d", &v, &t);

	while (t--)
	{
		scanf ("%d", &p);
		if (p > 0)
		{
			if (v + p <= 100) v+=p;
			else v=100;
		}
		else
		{
			if (v + p >= 0) v+=p;
			else v = 0;
		}
	}
	printf ("%d\n", v);

	return 0;
}

