/*
	problem 19773
	palavras ordenadas
	by lucas correa
	time: 00:07:44

*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(void)
{

	unsigned int p;
	unsigned char i, size;
	char palavra[43], max = 'a', tmp;

	scanf("%u%*c", &p);

	while(p--)
	{
	 	scanf("%[^\n]%*c", palavra);
		
		max = toupper(palavra[0]);

		for(i = 1, size = strlen(palavra); i < size; i++)
		{
			tmp = toupper(palavra[i]);
			if(tmp > max)
				max = tmp;
			else
				break;
		}

		printf("%s: ", palavra);
		
		if(i >= size)
			puts("O");
		else
			puts("N");
	}

	return 0;
}

