#include <stdio.h>

typedef unsigned long int luint;
typedef unsigned int uint;

luint hanoi2(uint n)
{
	luint mov = 0;
	if(n == 1)
		return 1;
		
	mov += 2*hanoi2(n-1) + 1;
	
	return mov;
}

int main(void)
{
	uint n = 0, teste = 1;

	scanf("%u%*c", &n);

	while(n)
	{
		printf("Teste %u\n%lu\n\n", teste++, hanoi2(n));
		scanf("%u%*c", &n);
	}
		
	return 0;
}
