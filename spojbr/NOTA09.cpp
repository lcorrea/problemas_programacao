#include <stdio.h>

int main (void)
{
	int nota;
	char letra;

	scanf ("%d", &nota);

	if (nota == 0) letra = 'E';
	else if (nota < 36) letra = 'D';
	else if (nota < 61) letra = 'C';
	else if (nota < 86) letra = 'B';
	else letra = 'A';

	printf ("%c\n", letra);
	return 0;
}

