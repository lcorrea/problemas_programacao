#include <stdio.h>

int main(void)
{
	int seq = 0, soma = 0, instancia = 0;
	unsigned int n = 0, i = 0, b = 1;

	while(scanf("%d%*c", &n) != EOF)
	{
		instancia++;
		
		for(soma = 0, i = 0; i < n; i++)
		{
			scanf("%d%*c", &seq);

			if(soma == seq && b)
			{
				b = 0;
				printf("Instancia %d\n%d\n\n", instancia, seq);
			}
			
			soma += seq;
		}
		
		if(b && n)
			printf("Instancia %d\nnao achei\n\n", instancia);
		else if(n == 0)
			printf("Instancia %d\n0\n\n", instancia);
		
		b = 1;
	}

	return 0;
}
