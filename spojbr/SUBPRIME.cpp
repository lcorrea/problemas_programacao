/*
	problem 5478
	sub-prime
	by lucas correa
	time: 00:20:45
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int b, n, d, c, v, i;
	int banco[20];

	cin >> b >> n;

	while(b)
	{

		for(i = 0; i < b; i++)
			cin >> banco[i];
		
		while(n--)
		{
			cin >> d >> c >> v;
			banco[d-1] -= v;
			banco[c-1] += v;
		}

		d = 0;

		for(i = 0; i < b; i++)
			if(banco[i] < 0)
				d = 1;

		if(d)
			cout << 'N';
		else
			cout << 'S';

		cout << endl;

		cin >> b >> n;
	}

	return 0;
}

