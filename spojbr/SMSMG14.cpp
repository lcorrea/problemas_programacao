#include <cstdio>
#include <iostream>

using namespace std;

void get_msg(string &sender, string &msg)
{
	unsigned int i = 0;
	string line;

	getline(cin, line);

	i = line.find(":");

	sender = line.substr(0, i++);
	msg = line.substr(i);

}

bool concat_msg(string &msg, string &msg2)
{
	if(msg.length() + msg2.length() + 1 <= 160)
	{
		msg = msg + " " + msg2;
		
		return true;
	}

	return false;
}

int main(void)
{
	string text, text2;
	string sender, s_at;
	unsigned int l_s = 0, g_s = 0, i = 0;
	unsigned int n,l,g;

	scanf("%d%*c%d%*c%d%*c", &n, &l, &g);

	while(n && l && g)
	{
		l_s = 0;
		g_s = 0;

		get_msg(sender, text);
		n--;

		while(n)
		{
			i = 0;

			do
			{
				get_msg(s_at, text2);
				n--;

				if(sender == s_at && concat_msg(text, text2))
					i++;
				else
					break;

			}while(n);

			if(sender == "Gustavo")
				g_s += i*g;
			else
				l_s += i*l;

			text = text2;
			sender = s_at;
		}

		cout << l_s << " " << g_s << endl;
		
		scanf("%d%*c%d%*c%d%*c", &n, &l, &g);
	}

	return 0;

}

