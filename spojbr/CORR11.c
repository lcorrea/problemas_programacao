#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct pair
{
	int id;
	unsigned long long int t;
}ii;

int cmp (const void *a, const void *b)
{
	ii p1 = *((ii *) a);
	ii p2 = *((ii *) b);
	
	if (p1.t == p2.t) return 0;
	if (p1.t < p2.t) return -1;
	return 1;
}


int main ()
{
	int n, m, i;
	ii carros[101];

	scanf ("%d %d", &n, &m);
	memset (carros, 0, sizeof(ii)*n);

	for (i = 0; i < n; i++)
	{
		int j, t;
		carros[i].id = i+1;
		for (j = 0; j < m; j++)
		{
			scanf ("%d", &t);
			carros[i].t += t;
		}
	}

	qsort (carros, n, sizeof(ii), cmp);

	for (i = 0; i < 3; i++)
		printf ("%d\n", carros[i].id);

	return 0;
}

