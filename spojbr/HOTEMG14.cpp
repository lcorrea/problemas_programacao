#include <iostream>
#include <list>

using namespace std;

struct hotel
{
	string nome;
	unsigned int dist;
	unsigned int prec;
	unsigned int quali;
};

list<struct hotel> lista;

int dominante(struct hotel a, struct hotel b)
{
	//retorna um numero maior que 0 se hotel a domina b
	//retorna um numero menor que 0 se hotel b domina a
	//retorna 0 se sao relativamente iguais
	unsigned char me = 0, p = 0;

	if(a.dist < b.dist)
		me++;
	else if(b.dist < a.dist)
		p++;

	if(a.prec < b.prec)
		me++;
	else if (b.prec < a.prec)
		p++;

	if(a.quali > b.quali)
		me++;
	else if(b.quali > a.quali)
		p++;
	
	if(me >= 1 && p == 0)
		return 1;
	else if (p >= 1 && me == 0)
		return -1;
	
	
	return 0;
}

void verifica(struct hotel *h)
{
	list<struct hotel>::iterator it;
	int result;

	for(it = lista.begin(); it != lista.end(); it++)
	{
		result = dominante(*it, *h);

		if(result < 0)
		{
			it = lista.erase(it);
			--it;
		}
		else if(result > 0)
		{
			return;
		}
	}

	lista.push_back(*h);
}

int main(void)
{
	struct hotel h1;
	unsigned int n;
	list<struct hotel>::const_iterator i;

	cin >> n;

	while(n)
	{
		lista.clear();
		
		cin >> h1.nome >> h1.dist >> h1.prec >> h1.quali;

		lista.push_back(h1);

		n--;

		while(n--)
		{
			cin >> h1.nome >> h1.dist >> h1.prec >> h1.quali;

			verifica(&h1);
						
		}

		for(i = lista.begin(); i != lista.end(); i++)
			cout << i->nome << endl;
		
		cout << '*' << endl;

		cin >> n;
	}

	return 0;
}

