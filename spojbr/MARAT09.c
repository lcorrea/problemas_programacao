#include <stdio.h>

int main (void)
{
	int n, d;
	int r = 1, i;
	int pn, pa = 0;

	scanf ("%d %d", &n, &d);
	scanf ("%d", &pa);
	pa += d;

	for (i = 1; i < n; i++)
	{
		scanf ("%d", &pn);

		if (pa >= pn)	pa = pn + d;
		else	pa = -1;
	}
		
	puts ((pa >= 42195) ? ("S") : ("N"));

	return 0;
}

