/*
	problem 2838
	brincadeira das tentativas
	by lucas correa
	time: 01:36:20
*/
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	unsigned short int n, i;
	
	scanf("%hu%*c", &n);

	while(n)
	{
		int fig[n];

		for(i = 0; i < n; i++)
			scanf("%d%*c", &fig[i]);

		sort(fig, fig + n);
		
		do
		{
			for(i = 0; i < n - 1; i++)
				printf("%d ", fig[i]);

			printf("%d\n", fig[i]);

		}while(next_permutation(fig, fig + n));

		putchar('\n');

		scanf("%hu%*c", &n);	
	}

	return 0;
}

