/*
	problem 1885
	posicoes livres
	by lucas correa
	time: 4 dias
*/
#include <stdio.h>
#include <string.h>

int main(void)
{
	unsigned int w, h, n;
	unsigned int x1,x2,y1,y2, tmp;
	unsigned int i, j;
	
	scanf("%u %u %u\n\n", &w, &h, &n);

	while(w||h||n)
	{
		unsigned int tab[h][w];
		memset(tab, 0, sizeof(unsigned int)*w*h);
		
		while(n--)
		{
			scanf("%u %u %u %u%*c", &x1,&y1,&x2,&y2);
			if(x1 > x2)
			{
				tmp = x2;
				x2 = x1;
				x1 = tmp;
			}

			if(y1 > y2)
			{
				tmp = y2;
				y2 = y1;
				y1 = tmp;
			}
				
			for(i = y1 - 1; i < y2; i++)
			{
				for(j = x1 - 1; j < x2; j++)
					tab[i][j] = 1;
			}
		}
		
		tmp = 0;
		for(i = 0; i < h; i++)
		{
			for(j = 0; j < w; j++)
			{
				if(tab[i][j] == 0)
					tmp++;
			}
		}

		printf("There");
		if(tmp == 1)
			printf(" is one empty spot.\n");
		else if(tmp)
			printf(" are %u empty spots.\n", tmp);
		else
			printf(" is no empty spots.\n");

		scanf("%u %u %u\n\n", &w, &h, &n);
	}

	return 0;
}
