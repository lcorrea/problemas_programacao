#include <stdio.h>

int main(void)
{
	int N;
	int fat = 1;

	scanf("%d%*c", &N);

	while(N)
		fat *= N--;
	
	printf("%d\n", fat);

	return 0;
}
	
