#include <stdio.h>

typedef unsigned int uint;
typedef unsigned int uchar;

uchar vencedor(uint a, uint b)
{
	return (a>b)?(0):(1);
}

int main(void)
{
	uint i, j, a, b;
	char vencedores[15];
	
	for(i = 0, j = 0; i < 15; i+=2, j++)
	{
		scanf("%u %u%*c", &a, &b);
		vencedores[j] = vencedor(a,b) + 'A' + i;
	}
	
	for(i = 0; i < 14; i+=2)
	{
		scanf("%u %u%*c", &a, &b);
		vencedores[j++] = vencedores[vencedor(a,b) + i];
	}
	
	printf("%c\n", vencedores[14]);

	return 0;
}

