/*
	problem 1367
	proteja sua senha
	by lucas correa
*/

#include <iostream>
#include <algorithm>
#include <set>
#include <vector>

using namespace std;

int main(void)
{
	char letras[6], letra;	
	unsigned int n, i, digit, teste = 0;
	unsigned int senha[6];
	set<unsigned int> senhas[6], digitos[5];
	vector<unsigned int> v;
	vector<unsigned int>::const_iterator it;

	cin >> n;

	while(n)
	{
		teste++;
		
		for(i = 0; i < 5; i++)
		{
			senhas[i].clear();
			cin >> digit;
			senhas[i].insert(digit);
			cin >> digit;
			senhas[i].insert(digit);
		}
		
		for(i = 0; i < 6; i++)
			cin >> letras[i];
		
		n--;

		while(n--)
		{
		
			for(i = 0; i < 5; i++)
			{
				digitos[i].clear();
				cin >> digit;
				digitos[i].insert(digit);
				cin >> digit;
				digitos[i].insert(digit);
			}

			for(i = 0; i < 6; i++)
			{
				cin >> letra;

				v.clear();
				v.resize(senhas[letras[i] - 'A'].size()+2);

				it = set_intersection(
					senhas[letras[i] - 'A'].begin(),
					senhas[letras[i] - 'A'].end(),
					digitos[letra - 'A'].begin(),
					digitos[letra - 'A'].end(),
					v.begin()
					);

				v.resize(it - v.begin());

				if(v.size() > 1)
				{
					for(it = v.begin(); it != v.end();it++)
						senhas[letras[i] - 'A'].insert(*it);
				}
				else
					senha[i] = v[0];
			}
		}

		cout << "Teste " << teste << '\n';

		for(i = 0; i < 5; i++)
			cout << senha[i] << ' ';
		cout << senha[i] << '\n' << endl;

		cin >> n;
	}

	return 0;
}

