#include <iostream>
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

int tab[200][200] = {0};
int n, m;

int paint (int x, int y)
{
	int count = 1;
	int dx[8] = { -1, 0, 1, -1, 1, 1, 0, -1};
	int dy[8] = { -1, -1, -1, 0, 0, 1, 1 , 1};

	tab[y][x] = 1;

	for (unsigned int i = 0; i < 8; i++)
	{
		int nx = x + dx[i];
		int ny = y + dy[i];

		if (nx >= m or nx < 0 or ny >= n or ny < 0)
			continue;
		else if (tab[ny][nx] == 0)
		{
			count += paint(nx, ny);
		}
	}

	return count;
}

int main (void)
{
	int k, x, y, a, b;

	cin >> n >> m >> x >> y >> k;

	while (k--)
	{
		cin >> a >> b;

		tab[a-1][b-1] = 1;
	}

	cout << paint (y-1, x-1) << endl;

	return 0;
}

