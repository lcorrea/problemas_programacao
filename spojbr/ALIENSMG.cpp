/*
	problem 19765
	Alienigenas
	by lucas correa
	time: 00:36:18
*/
#include <iostream>
#include <algorithm>
#include <string>
#include <set>

using namespace std;

int main(void)
{
	unsigned int n, i;
	set<string> species;
	string sequencia;

	cin >> n;

	while(n)
	{
		for(i = 0; i < n; i++)
		{
			cin >> sequencia;
			sort(sequencia.begin(), sequencia.end());
			species.insert(sequencia);
		}

		cout << species.size() << endl;

		species.clear();

		cin >> n;
	}

	return 0;
}

