#include <bits/stdc++.h>

using namespace std;

pair <int, int> good[61];
pair <int, int> att[61][3];
int tb[61][32010];
int g, n;

int calc(int i, int vmax)
{
   if (tb[i][vmax] != -1)
        return tb[i][vmax];
    
    if (i == n)
        return tb[i][vmax] = 0;
    
    if (good[i].first == -1)
        return tb[i][vmax] = calc(i+1, vmax);

    int a, b; a = b = INT_MIN;

    if (good[i].first <= vmax)
    {
        int c = vmax - good[i].first;
        int d = good[i].second * good[i].first;
        
        a = calc(i+1, c) + d;

        for (int j = 0; j < 3 and att[i][j].first != -1; j++)
        {
            int e = c - att[i][j].first;
            int f = d + att[i][j].first * att[i][j].second;
            if (e >= 0)
                a = max(a, calc(i+1, e) + f);
         }
    }

    b = calc(i+1, vmax);

    return tb[i][vmax] = max(a, b);
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while(t--)
    {
        int v;

        memset (tb, -1, sizeof(tb));
        memset (att, -1, sizeof(att));
        memset (good, -1, sizeof(good));

        scanf ("%d %d", &v, &n);

        g = 0;
        for (int i = 1, a,b,c; i <= n; i++)
        {
            scanf ("%d %d %d", &a, &b, &c);

            if (c)
            {
                for (int j = 0; j < 3; j++)
                    if (att[c][j].first == -1)
                    {
                        att[c][j] = make_pair(a, b);
                        break;
                    }
            }
            else
                good[i] = make_pair(a, b), g++;
        }


        //for (int i = 0; i < g; i++)
        //    for (int j = 0; j < 3; j++)
        //        printf ("%d%c", att[i][j].first, j == 2 ? '\n' : ' ');

        printf ("%d\n", calc(1, v));
    }

    return 0;
}

