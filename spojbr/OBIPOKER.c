#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int frequencia[13], n, i, j;
int carta, cartas[5];

int cmp (const void *a, const void *b) { return *((int *)a) - *((int *)b); }

int maxFreq (void)
{
	int index = 0;
	for (j = 0; j < 13; j++)
		if (frequencia[j] > frequencia[index])
			index = j;
	return index;
}

int pontuacao (void)
{
	int maxIndex;	
	if ((cartas[1] == cartas[0] + 1) && (cartas[2] == cartas[0] + 2) &&
			(cartas[3] == cartas[0] + 3) && (cartas[4] == cartas[0] + 4))
		return cartas[0] + 200;

	/*heuristica para encontrar as outras condicoes*/
	maxIndex = maxFreq();	/*indice da carta que se repetiu mais vezes*/
	carta = maxIndex + 1;	/*numero da carta que se repetiu mais vezes*/

	if (frequencia[maxIndex] >= 4) { return carta + 180; }

	if (frequencia[maxIndex] == 3)
	{
		frequencia[maxIndex] = 0;
		maxIndex = maxFreq(); /*busca a segunda maior combinacao de cartas*/

		if (frequencia[maxIndex] == 2) { return carta + 160; }
		else { return carta + 140; }
	}

	if (frequencia[maxIndex] == 2)
	{
		frequencia[maxIndex] = 0;
		maxIndex = maxFreq();
		if (frequencia[maxIndex] == 2) { return (maxIndex+1)*3 + carta*2 + 20; }
		else { return carta; }
	}

	return 0;
}

int main (void)
{
	scanf ("%d", &n);
	for (i = 1; i <= n; i++)
	{
		memset (frequencia, 0, sizeof frequencia);
		printf ("Teste %d\n", i);

		for (j = 0; j < 5; j++)
		{
			scanf ("%d", &carta);
			frequencia[carta-1]++;
			cartas[j] = carta;
		}
		qsort (cartas, 5, sizeof (int), cmp);
		printf ("%d\n\n", pontuacao());
	}

	return 0;
}

