#include <stdio.h>

int main(void)
{
	unsigned int p = 0, cel[100][100], maxP = 0;
	unsigned int n, m, i = 0, j = 0;
	scanf("%u %u%*c", &n, &m);

	for(i = 0; i < n; i++)
	{
		for(j = 0; j < m; j++)
		{
			scanf("%u%*c", &p);
			cel[i][j] = p;
		}
	}

	//max produtividade colunas
	for(i = 0; i < m; i++)
	{
		p = 0;
	 	for(j = 0; j < n; j++)
		{
			p += cel[j][i];	
		}

		if(p > maxP)
			maxP = p;
	}

	//max produtividade colunas e linhas
	for(i = 0; i < n; i++)
	{
		p = 0;
		for(j = 0; j < m; j++)
		{
			p += cel[i][j];
		}

		if(p > maxP)
			maxP = p;
	}

	printf("%u\n", maxP);

	return 0;
}
