#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, mov = 0;
	int h[1010], total = 0, ok = 1;

	scanf ("%d %d%*c", &n, &m);

	for (int i = 0, a; i < n; i++)
	{
		scanf ("%d%*c", &a);
		h[i] = abs (m - a);
		total += h[i];
	}

	while (total and ok)
	{
		ok = 0;
		int *pmax = h;
		
		for (int i = 0; i < n - 1; i++)
			if (min (h[i], h[i+1]) >= min (*pmax, *(pmax+1)))
				ok = 1, pmax = h + i;

		int dec = min (*pmax, *(pmax+1));

		if (!dec) break;

		mov += dec;
		total -= 2*dec;
		*pmax -= dec;
		*(pmax+1) -= dec;
	}

	/*for (int i = 0; i < n; i++)
		mov += h[i];*/

	printf ("%d\n", mov);

	return 0;
}

