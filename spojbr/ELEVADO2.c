/*
	problem 8778
	Elevador
	by lucas correa
	time: 00:20:15

*/
#include <stdio.h>

int main(void)
{
	int sum = 0;
	unsigned int c = 0, n = 0, p = 0;
	char info = 'N';

	scanf("%u %u%*c", &n, &c);

	while(n--)
	{	
	
		scanf("%u%*c", &p);
		sum -= p;
		scanf("%u%*c", &p);
		sum += p;
		
		if(sum > c)
		{
			info = 'S';
			c = sum;
		}
	
	}

	printf("%c\n", info);

	return 0;
}
