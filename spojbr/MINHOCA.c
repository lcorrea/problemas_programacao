#include <stdio.h>

#define max(a, b) ((a>b)?(a):(b))

int campo[210];

int main (void)
{
	int n, m, i, j, p, maxP = 0;

	scanf ("%d %d", &n, &m);

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			scanf ("%d", &p);
			campo[j] += p;
			campo[m+i] += p;
			maxP = max(maxP, max(campo[j], campo[m+i]));
		}
	}

	printf ("%d\n", maxP);

	return 0;
}

