#include <stdio.h>

int city[510][510];

int main (void)
{
	int n, x, y, total = 0;
	
	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%d %d", &x, &y);

		if (city[y][x] == 1) total++, city[y][x] = 2;
		else if (city[y][x] == 0) city[y][x] = 1;
		else;

	}
	printf ("%d\n", total>0);
	return 0;
}


