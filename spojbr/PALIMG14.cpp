#include <iostream>
#include <algorithm>

using namespace std;

bool check(string str)
{
	string rstr = str;
	
	reverse(rstr.begin(), rstr.end());

	return str == rstr;
}

int main(void)
{
	string p, rp;
	unsigned int cnt, i;

	cin >> p;

	while(!cin.eof())
	{
		cnt = 0;
		i = 0;

		while(!check(p+rp))
		{
			cnt++;
			rp = p[i++] + rp;
		}

		rp.clear();

		cout << cnt << endl;

		cin >> p;
	}

	return 0;
}

