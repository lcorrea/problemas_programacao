/*
	problem 840
	cofrinhos da vo vitoria
	by lucas correa
	time: 00:12:47
*/
#include <stdio.h>

int main(void)
{
	unsigned int j, z, casoTestes = 1, depo = 0;
	int cofreJ = 0, cofreZ = 0;

	scanf("%u%*c", &depo);
	
	while(depo)
	{	
		printf("Teste %d\n", casoTestes++);

		while(depo--)
		{
			scanf("%u %u%*c", &j, &z);
			cofreJ += j;
			cofreZ += z;
			printf("%d\n", cofreJ - cofreZ);
		}

		putchar('\n');
		
		cofreZ = 0;
		cofreJ = 0;

		scanf("%u%*c", &depo);
	}

	return 0;
}
