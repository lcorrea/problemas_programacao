#include <bits/stdc++.h>

using namespace std;

int cnt = 0, n;
char tab[501][501];
bool visited[501][501];

bool dfs(int i, int j)
{
    int newi = i, newj = j;

    if (i >= n or j >= n or i < 0 or j < 0)
        return false;

    if (visited[i][j])
        return tab[i][j] != '*';

    visited[i][j] = 1;

    switch(tab[i][j])
    {
        case '*':
            return false;

        case '<':
            newj = j - 1;
            break;
        case '>':
            newj = j + 1;
            break;
        case 'A':
            newi = i - 1;
            break;
        case 'V':
            newi = i + 1;
            break;
    }

    if (dfs(newi, newj))
        return true;

    tab[i][j] = '*';

    return false;
}

int main (void)
{
    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%s", tab[i]);

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (visited[i][j] == 0)
                dfs(i, j);

    int cnt = 0;

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cnt += tab[i][j] != '*';

    printf ("%d\n", cnt);

    return 0;
}

