#include <iostream>
#include <vector>
#include <bitset>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vii;

vi cmp;

void dfs (vii &graph, int v);

int componentesC (vii &graph)
{
	int count = 0;
	for (unsigned int a = 1; a <= graph.size(); a++)
	{
		if (cmp[a] == -1)
		{
			count++;

			dfs (graph, a);
		} 
	}

	return count;
}

void dfs (vii &graph, int v)
{
	cmp[v] = 1;

	for (unsigned int a = 0; a < graph[v].size(); a++)
	{
		if (cmp[graph[v][a]] == - 1)
			dfs (graph, graph[v][a]);
	}
}

int main(int argc, char **argv)
{
	int n, m, amigoA, amigoB;

	cin >> n >> m;

	vii graph(n+1);
	cmp.assign(n+1, -1);

	for (unsigned int i = 0; i < m; i++)
	{
		cin >> amigoA >> amigoB;

		graph[amigoA].push_back(amigoB);
		graph[amigoB].push_back(amigoA);
	}

	cout << componentesC (graph) << endl; 

	return 0;
}

