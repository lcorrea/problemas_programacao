#include <stdio.h>

#define min(a, b) ((a<b)?(a):(b))

int main (void)
{
	int n, m, H, h, i;
	unsigned int e = 40000000, a, b, mina=1;

	scanf ("%d", &n);

	for (i = 1; i <= n; i++)
	{
		scanf ("%d", &m);
		a = 0;
		b = 0;
		scanf ("%d", &H);
		m--;
		while (m--)
		{
			scanf ("%d", &h);
			a+= (h>H) ? (h - H):(0);
			b+= (h<H) ? (H - h):(0);
			H = h;
		}

		if ((a = min(a,b)) < e) e = a, mina = i;
	}
	printf ("%d\n", mina);

	return 0;
}

