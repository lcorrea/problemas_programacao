#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(void)
{
	char word[21];
	unsigned int sum = 0, i = 1;
	unsigned char size;
	while(scanf("%s%*c", word) != EOF)
	{
		sum = 0;
		size = strlen(word);
		while(size)
		{
			size--;
			sum += (isupper(word[size]))?(word[size] - 'A' + 27):(word[size] - 'a' + 1);
		}
		
		for(i = 2; i < sum;i++)
		{
			if(sum%i == 0)
				break;
		}
		
		if(sum == i || sum == 1)	//nao sei pq 1 eh considerado primo pelo problema
			puts("It is a prime word.");
		else
			puts("It is not a prime word.");
	}

	return 0;
}
