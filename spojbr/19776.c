#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* itoa36(unsigned long int value, char *str)
{
	unsigned long int mod = value;
	char num36, i = 0 ;
	
	do{
		mod = (value%36);
		num36 = (mod > 9) ? (mod - 10 + 'A'):(mod + '0');
		str[i++] = num36;
		value /= 36;
	}while(value);

	str[i] = 0;
	
	return str;
}


int main(void)
{
	unsigned long int sum = 0;
	char a36[7], b36[7], sum36[20], size = 0;
	
	scanf("%s %s%*c", a36, b36);

	while(a36[0] != '0')
	{
		sum = 0;

		sum = strtoul(a36, NULL, 36);
		sum += strtoul(b36, NULL, 36);
		//printf("%lu\n", sum);
		itoa36(sum, sum36);
		size = strlen(sum36);
		while(size)
			putchar(sum36[--size]);
		putchar('\n');
		scanf("%s %s%*c", a36, b36);

	}

	return 0;
}

