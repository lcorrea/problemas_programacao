#include <stdio.h>

int main (void)
{
	unsigned int a, f, t = 0;
	int n;
	const unsigned int F = 40000000UL;

	scanf ("%u", &a);
	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%u", &f);

		if (a*f >= F) t++;
	}
	printf ("%u\n", t);

	return 0;
}

