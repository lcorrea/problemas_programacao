#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

typedef pair <int, int> ii;
typedef vector <ii> vii;

bool cmp (ii a, ii b)
{
	return a.second < b.second;
}

int main (void)
{
	int consultas = 0;
	int entrada, saida, ultimaSaida;
	int pacientes;

	cin >> pacientes;

	vii horarios (pacientes);

	for (int i = 0; i < pacientes; i++)
	{
		cin >> entrada >> ultimaSaida;

		horarios[i] = ii(entrada, ultimaSaida);
	}

	sort (horarios.begin(), horarios.end(), cmp);

	int fim = 0;

	for (int i = 0; i < pacientes; i++)
	{
		if (horarios[i].first >= fim)
		{
			consultas++;
			fim = horarios[i].second;
		}
	}

	cout << consultas << endl;

	return 0;
}

