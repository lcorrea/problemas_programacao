#include <stdio.h>

#define min(a, b) ((a<b)?(a):(b))

int main (void)
{
	int n, s, i, m;

	scanf ("%d %d", &n, &s);
	m = s;
	while (n--)
	{
		scanf ("%d", &i);
		s += i;
		m = min (s, m);
	}
	printf ("%d\n", m);

	return 0;
}
