#include <bits/stdc++.h>

using namespace std;

int line (int x, int c)
{
	return ceil(x/(float)c);
}

int col (int x, int c)
{
	int r = x % c;

	return r ? r : c;
}

int main (void)
{
	int l, c;

	while (scanf ("%d%d", &l, &c) != EOF and l and c)
	{
		int sala[l][c];
		vector < pair <int, int> > L, C;

		for (int i = 0; i < l; i++)
			for (int j = 0; j < c; j++)
				scanf ("%d", &sala[i][j]);

		for (int i = 0; i < l; i++)
		{
			int lin = line (sala[i][0], c);

			while (lin != i + 1)
			{
				L.push_back (make_pair(lin, i+1));
				for (int j = 0; j < c; j++)
					swap (sala[lin - 1][j], sala[i][j]);

				lin = line(sala[i][0], c);
			}
		}

		for (int j = 0; j < c; j++)
		{
			int cl = col (sala[0][j], c);

			while (cl != j + 1)
			{
				C.push_back (make_pair(cl, j+1));
				for (int i = 0; i < l; i++)
					swap(sala[i][cl - 1], sala[i][j]);

				cl = col (sala[0][j], c);
			}
		}

		printf ("%d\n", C.size() + L.size());
		for (int i = 0, sz = L.size(); i < sz; i++)
			printf ("L %d %d\n", L[i].first, L[i].second);
		for (int i = 0, sz = C.size(); i < sz; i++)
			printf ("C %d %d\n", C[i].first, C[i].second);
	}

	return 0;
}

