#include <stdio.h>

int main(void)
{
	int n = 0;
	while(scanf("%d%*c", &n)  != EOF) printf("%d\n", n*n);
	return 0;
}
