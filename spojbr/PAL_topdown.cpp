#include <bits/stdc++.h>

using namespace std;

char str[2001];
bool pal[2001][2001] = {0};
int n, tb[2001][2001];

int dp(int i, int j)
{
    if (tb[i][j] != -1)
        return tb[i][j];

    if (i == j or pal[i][j])
        return tb[i][j] = 1;

    tb[i][j] = n+1;
    for (int k = i; k <= j-1; k++)
        tb[i][j] = min(tb[i][j], dp(i, k) + dp(k+1, j));

	return tb[i][j];
}

int main()
{
    int teste = 1;

    while (scanf ("%d", &n), n)
    {
        scanf ("%s", str);

        for (int i=0; i<n; i++)
            pal[i][i] = 1;

        for (int j=1; j<n; j++)
            for (int i=j-1; i>=0; i--)
                pal[i][j] = ((str[i]==str[j]) && (i+1>=j-1 || pal[i+1][j-1]));
                
        memset(tb, -1, sizeof(tb));
        
        printf ("Teste %d\n%d\n\n", teste++, dp(0, n-1));
    }

    return 0;
}

