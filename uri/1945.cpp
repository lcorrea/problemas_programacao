#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cctype>
#include <algorithm>
#include <map>

using namespace std;

map <string, int> memory;
string lastVar;

void simulator (char *str)
{
	char *variable = strtok (str, " "); strtok (NULL, " ");
	char *r1 = strtok (NULL, " ");

	lastVar = variable; //cout << "-- " << lastVar << " --" << endl;

	if (isdigit(r1[0])) memory[variable] = atoi(r1);
	else memory[variable] = memory[r1];
	
	r1 = strtok (NULL, " ");

	if (r1 != NULL)
	{
		char *r2 = strtok (NULL, " ");

		if (isdigit(r2[0]))
			memory[variable] += atoi(r2);
		else
			memory[variable] += memory[r2];
	}
}

int main (void)
{
	string instruction;

	getline(cin, instruction);
	while (!cin.eof())
	{
		char str[instruction.length()+1];
		strcpy (str, instruction.c_str());
		simulator(str);
		//cout <<  memory[lastVar] << endl;
		getline(cin, instruction);
	}
	cout <<  memory[lastVar] << endl;

	return 0;
}

