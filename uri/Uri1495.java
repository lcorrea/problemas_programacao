import java.util.*;
import java.math.BigInteger;
import java.io.*;

public class Uri1495 {

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    while (in.hasNext()) {

      int n = in.nextInt();
      int g = in.nextInt();
      int points = 0, empates = 0;
      ArrayList<Integer> sg = new ArrayList<Integer>();

      for (int i = 0, s, r; i < n; i++) {
        s = in.nextInt();
        r = in.nextInt();

        if (s > r)
          points += 3;
        else if (s == r)
          empates++;
        else
          sg.add (r - s);
      }

      if (empates <= g) {

        points += empates * 3;
        g -= empates;

        Collections.sort(sg);

        Iterator<Integer> it = sg.iterator();
        while (it.hasNext()) {
          int p = it.next();

          if (p <= g) {
            g -= p;

            if (g == 0)
              points += 1;
            else {
              points += 3;
              g -= 1;
            }
          }
          else
            break;
        }

      }
      else
        points += g * 3 + (empates - g);

      System.out.println (points);
    }
  }
}

