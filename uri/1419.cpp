#include <bits/stdc++.h>

using namespace std;

int proccess (vector <int>& V)
{
	int cnt = 1, pts = 0, last, monster;

	scanf ("%d", &last);
	pts += last;

	for (int i = 1, sz = V.size(); i < sz; i++)
	{
		scanf ("%d", &monster);
		pts += monster;
		
		if (last == monster) cnt++;
		else last = monster, cnt = 1;

		V[i] = cnt == 3;
	}

	return pts;
}

int main (void)
{
	int r;

	while (scanf ("%d", &r), r)
	{
		vector <int> M(r, 0), L(r, 0);
		int ptM = 0, ptL = 0, i;

		ptM = proccess (M);
		ptL = proccess (L);

		for (i = 0; i < r and !M[i] and !L[i]; i++);

		if (i < r)
		{
			if (M[i] and !L[i])
				ptM += 30;
			else if (L[i] and !M[i])
				ptL += 30;
			else;
		}

		puts (ptM >= ptL ? ptM == ptL ? "T" : "M" : "L");
	}

	return 0;
}

