/*
	problem 1072
	interval 2
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, cnt = 0;
	long int x;

	cin >> n;

	for(unsigned int i = 0; i < n; i++)
	{
		cin >> x;

		if(x >= 10 && x <= 20)
			cnt++;
	}
	
	cout << cnt << " in" << endl;
	cout << n - cnt << " out" << endl;

	return 0;
}

