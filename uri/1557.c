#include <stdio.h>
#include <math.h>

int main(void)
{
	//printf("%*d\n", 4, 4);
	int col,lin,a,mat;
	int t;
	scanf("%d", &mat);
	while(mat)
	{
	
		t = ceil(log10(pow(2,2*mat)/4));

		for(lin=1,a=1;lin<=mat;lin++)
		{
			a = (int) pow(2, lin-1);
			printf("%*d", t, a);
			for(col=2;col<=mat;col++)
				printf(" %*d", t, a *= 2 );
			printf("\n");

		}

		printf("\n");
		scanf("%d", &mat);
	}


	return 0;
}
