#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	long long m = 0;
	long long n;

	while (scanf ("%lld", &n), n)
	{
		m = n;

		while (n!=1)
			n = (n%2) ? (n*3ll + 1ll) : (n >> 1), m = max (m, n);

		printf ("%lld\n", m);
	}

	return 0;
}

