#include <cstdio>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

typedef pair <int, int> ii;
typedef pair <int, ii> edge;
typedef vector <edge> EdgeList;
typedef vector <int> vi;

EdgeList graph;
vi p, height;
int familias;

int findSet (int i) { return p[i] == i ? i : (p[i] = findSet(p[i])); }

bool isSameSet (int i, int j) { return findSet(i) == findSet(j); }

void join(int i, int j)
{
	if (!isSameSet(i, j))
	{
		int x = findSet(i), y = findSet(j);
		familias--;
		if (height[x] > height[y]) p[y] = x;
		else
		{
			p[x] = y;
			if (height[x] == height[y]) height[y]++;
		}
	}
}

int main (void)
{
	int v;
	unsigned long long int minCust = 0;

	scanf ("%d", &v);
	familias = v;
	height.resize(v+1, 0);
	p.resize(v+1); for (int i = 1; i <= v; i++) p[i] = i;

	for (int i = 1, k, j, c; i < v; i++)
	{
		scanf ("%d", &k);

		while (k--)
		{
			scanf ("%d %d", &j, &c);
			graph.push_back (edge(c, ii(i, j)));
		}
	}

	/**************************************
	 *  	- kruskal's algorithm -
	 **************************************/
	sort (graph.begin(), graph.end());

	int e = graph.size();

	for (int i = 0; i < e; i++)
	{
		edge front = graph[i];
		if (!isSameSet(front.second.first, front.second.second))
		{
			minCust += front.first;
			join (front.second.first, front.second.second);
		}
	}

	printf ("%d %llu\n", familias, minCust);

	return 0;
}

