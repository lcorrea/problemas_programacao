#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int m, n, Teste = 1;

  while (scanf ("%d%d", &n, &m), n and m)
  {
    int v[n];
    int min_t = 0, max_t = 0, sum = 0;
    int f = 0;

    for (int i = 0; i < n; i++)
      scanf ("%d", v + i);

    for (int i = 0; i < m; i++)
      sum += v[i];

    min_t = sum / m;
    max_t = min_t;

    while (f + m < n)
    {
      sum += v[f + m];
      sum -= v[f++];

      min_t = min (min_t, sum / m);
      max_t = max (max_t, sum / m);
    }

    printf ("Teste %d\n%d %d\n\n", Teste++, min_t, max_t);
  }


  return 0;
}

