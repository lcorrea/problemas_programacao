#include <iostream>
#include <cctype>
#include <cstring>

using namespace std;

int main (void)
{
	string line;

	while (getline (cin, line))
	{
		int i = 0;
		bool last = true;
		while (i < line.size())
		{
			while (i < line.size() and line[i] == ' ')
				cout << ' ', i++;

			for (int j = 0; i < line.size() and line[i] != ' '; j++, i++)
			{
				if (last) cout << (char) toupper(line[i]);
				else cout << (char) tolower(line[i]);

				last = !last;
			}
		}

		cout << endl;
	}

	return 0;
}

