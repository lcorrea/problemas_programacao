/*
	problem 1064
	Positives and Average
	by lucas correa
*/

#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	float num, sum = 0;
	int count = 0;

	for(unsigned int i = 0; i < 6; i++)
	{
		cin >> num;

		if(num > 0.0)
		{
			sum += num;
			count++;
		}
	}
	sum /= count;

	cout << count << " valores positivos" << endl;
	printf("%.1f\n", sum);

	return 0;
}

