#include <bits/stdc++.h>

using namespace std;

int p[10001];
bool visited[10000];
vector < vector <int> > adjlist;
int n, m;

bool detectCycle(int u)
{
    bool ok = true;

    visited[u] = true;

    for (int i = 0; ok and i < (int) adjlist[u].size(); i++)
    {
        if (p[u] == adjlist[u][i]) continue;

        if (!visited[adjlist[u][i]])
        {
            p[adjlist[u][i]] = u;
            ok = detectCycle(adjlist[u][i]);
        }
        else
            return false;
    }

    return ok;
}

int main (void)
{
    while (EOF != scanf ("%d %d", &n, &m))
    {
        bool ok = true;
        
        adjlist.clear();
        adjlist.resize(n);
        memset(visited, 0, sizeof(visited));

        for (int i = 0, a, b; i < m; i++)
        {
            scanf ("%d %d", &a, &b);

            if (a == b) ok = false;

            adjlist[a-1].push_back(b-1);
            adjlist[b-1].push_back(a-1);
        }

        for (int i = 0; i < n and ok; i++)
            if (!visited[i])
            {
                p[i] = i;
                ok = detectCycle(i);
            }

        puts (ok ? "Seguro" : "Inseguro");
    }

    return 0;
}

