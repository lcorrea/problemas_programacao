#include <stdio.h>
#include <string.h>

void printFrame (int x, int y, char frame[][26], int n)
{
	int i, j;
	frame[y][x] = 'X';
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
			putchar (frame[i][j]);
		putchar ('\n');
	}
	frame[y][x] = 'O';
}

int main ()
{
	int n, x, y;
	char frame[26][26];

	memset (frame, 'O', sizeof(char)*26*26);

	while (scanf ("%d", &n), n)
	{
		if (n==1) { puts("X\n@"); continue; }
		x = n/2, y = n/2;
		int upperBound = n/2 + 1;
		int lowerBound = n/2 - 1;
		while (upperBound < n || lowerBound >= 1)
		{
			//direita
			while (x <= upperBound)
			{
				printFrame(x, y, frame, n);
				puts ("@");
				x++;
			}
			
			//cima
			x--;
			y--;
			while (y >= lowerBound)
			{
				printFrame(x, y, frame, n);
				puts ("@");
				y--;
			}
		
			//esquerda
			y = lowerBound;
			x--;
			while (x >= lowerBound)
			{
				printFrame(x, y, frame, n);
				puts("@");
				x--;
			}
			
			//baixo
			x=lowerBound;
			y++;
			while (y <= upperBound)
			{
				printFrame(x,y, frame, n);
				puts("@");
				y++;
			}
		
			//direita
			x++;
			y--;
			while (x <= upperBound)
			{
				printFrame(x, y, frame, n);
				puts ("@");
				x++;
			}
			upperBound++;
			lowerBound--;
		}
	}
	
	return 0;
}

