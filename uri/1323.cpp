#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	long long r, n;

	while (scanf ("%lld", &n), n)
	{
		r = n*n;
		n--;

		while (n >= 1)
			r  += n*n, n--;
		printf ("%lld\n", r);
	}

	return 0;
}

