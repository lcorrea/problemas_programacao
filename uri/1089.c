#include <stdio.h>

#define reta(a, b) ((a < b)?(1):(-1))

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		int ret = 0, first, last, cur;
		int peaks = 0, i;

		scanf ("%d", &first); 
		scanf ("%d", &last); cur = last;
		
		ret = reta(first, cur);

		for (i=2; i <= n - 1; i++)
		{
			scanf ("%d", &cur);

			if (ret != reta(last, cur))
			{
				peaks++;
				ret = (ret==1) ? (-1) : (1);
			}
			last = cur;
		}

		if (i < n)
			scanf ("%d", &cur);

		if (ret != reta(cur, first)) peaks++;

		printf ("%d\n", peaks);

	}

	return 0;
}

