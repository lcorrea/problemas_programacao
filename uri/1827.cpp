#include <iostream>

using namespace std;

int main (void)
{
	unsigned int n;

	cin >> n;

	while (!cin.eof ())
	{
		for (unsigned int a = 0; a < n; a++)
		{
			for (unsigned int b = 0; b < n; b++)
			{
				if (a >= (n / 3) && a + n / 3 < n && b >= (n / 3) && b + n /3 < n )
				{
					if (a == n/2 && b == n/2)
					{
						cout << 4;
					}
					else
					{
						cout << 1;
					}
				}
				else if (a == b)
				{
					cout << 2;
				}
				else if (a + b == n - 1)
				{
					cout << 3;
				}
				else
				{
					cout << 0;
				}
			}

			cout << endl;
		}

		cout << endl;

		cin >> n;
	}

	return 0;
}

