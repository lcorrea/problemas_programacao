#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    for (int i = 0; i < t; i++)
    {
        int b, n;
        vector < vector <int> > table;

        scanf ("%d%d", &b, &n);
        
        table.resize (b);
        for (int i = 0, a; i < n; i++)
        {
            scanf ("%d", &a);

            table[a % b].push_back (a);
        }
        
        for (int i = 0; i < b; i++)
        {
            printf ("%d -> ", i);

            for (int j = 0, sz = table[i].size(); j < sz; j++)
                printf ("%d -> ", table[i][j]);
            puts ("\\");
        }

        if (i < t-1) putchar ('\n');
    }

    return 0;
}

