#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    while (n--)
    {
        char name[100000];
        double grau;
        double notas[7], *maxn, *minn, total = 0;

        scanf ("%s", name);
        scanf ("%lf", &grau);

        maxn = minn = notas;

        for (int i = 0; i < 7; i++)
        {
            scanf ("%lf", notas + i);

            total += notas[i];

            minn = (*minn > notas[i]) ? notas + i : minn;
            maxn = (*maxn < notas[i]) ? notas + i : maxn;
        }

        printf ("%s %.02lf\n", name, (total - *maxn - *minn)*grau);
    }

    return 0;
}

