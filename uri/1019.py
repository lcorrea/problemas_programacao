#!/usr/bin/env python3

sec = int(input())
hours, sec = sec / 3600, sec % 3600
minutes, sec = sec / 60, sec % 60

print("%d:%d:%d" % (hours, minutes, sec))
