#include <iostream>
#include <algorithm>

using namespace std;

int main (void)
{
	int prova;
	int casos, n, notamaxima, seq,indx, maxseq;

	cin >> casos;
	
	for (int i = 1; i <= casos; i++)
	{
		cin >> n;
		seq = 1;
		notamaxima = 0;
		indx = 0;
		maxseq = 1;

		for (int j = 0; j < n; j++)
		{
		       	cin >> prova;

			if (prova > notamaxima) seq = 1, notamaxima = prova, indx = j, maxseq = 1;
			else if (prova == notamaxima and seq + indx == j) seq++;
			else if (prova == notamaxima) seq = 1, indx = j;
			else maxseq = max (seq, maxseq), seq = 0;

		}
		maxseq = max (seq, maxseq);
		
		cout << "Caso #" << i << ": " << maxseq << endl;
	}
	
	return 0;
}

