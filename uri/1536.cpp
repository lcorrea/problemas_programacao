#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	int n;

	cin >> n;

	while (n--)
	{
		int a, b, pta = 0, ptb = 0, ga = 0, gb = 0, sa = 0, sb = 0;
		char c;

		scanf ("%d %*c %d", &a, &b);
		gb += b; sa += a - b; sb += b - a;

		if (a == b) pta++, ptb++;
		else (pta += 3*(a>b)), ptb += 3*(a<b);

		scanf ("%d %*c %d", &b, &a);
		ga += a; sa += a - b; sb += b - a;

		if (a == b) pta++, ptb++;
		else pta += 3*(a>b), ptb += 3*(a<b);

		if (pta > ptb)	c = '1';
		else if (ptb > pta) c = '2';
		else
		{
			if (sa > sb) c = '1';
			else if (sb > sa) c = '2';
			else
			{
				if (ga > gb) c = '1';
				else if (gb > ga) c = '2';
				else
				{
					cout << "Penaltis" << endl;
					continue;
				}
			}
		}
		cout << "Time ";
		cout << c << endl;
	}

	return 0;
}

