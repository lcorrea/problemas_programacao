#include <iostream>
#include <algorithm>
#include <set>
#include <vector>
#include <map>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;
typedef set<int> si;

vvi S, G;
map<int, int> funcionarios;
vi visitados;
vi idades;
int minA = 110;

void minAge (int vtx)
{
	visitados[vtx] = 1;

	for (vi::const_iterator it = S[vtx].begin(); it != S[vtx].end(); it++)
	{
		if (visitados[*it] == 0)
		{
			minA = min (minA, idades[*it]);
			minAge(*it);
		}
	}
}

void mSwap(int a, int b)
{
	int aa = a, bb = b;
	a = funcionarios[a], b = funcionarios[b];
	swap(G[a], G[b]);
	swap(S[a], S[b]);
	swap(idades[a], idades[b]);
	swap(funcionarios[aa], funcionarios[bb]);
}

int main (void)
{
	int n, m, i, x, y;
	char c;

	cin >> n >> m >> i;

	while (!cin.eof())
	{
		S.clear();
		G.clear();
		funcionarios.clear();

		S.resize(n);
		G.resize(n);

		idades.assign(n, 0);
		visitados.assign(n, 0);


		for (int j = 0; j < n; j++)
		{
			funcionarios[j+1] = j;
			cin >> idades[j];
		}

		for (int j = 0; j < m; j++)
		{
			cin >> x >> y;

			x = funcionarios[x];
			y = funcionarios[y];

			G[x].push_back(y);
			S[y].push_back(x);
		}

		for (int ki = 0; ki < i; ki++)
		{
			cin >> c;

			switch(c)
			{
				case ('T'):
				{	
					int a, b;

					cin >> a >> b;

					mSwap(a, b);
				}
				break;
				case ('P'):
				{
					int a;
					cin >> a;

					a = funcionarios[a];

					minAge (a);

					if (minA == 110)
						cout << '*';
					else
						cout << minA;
					cout << endl;
					minA = 110;
					fill(visitados.begin(), visitados.end(), 0);
				}
				break;
			}
		}

		cin >> n >> m >> i;		
	}

	return 0;
}

