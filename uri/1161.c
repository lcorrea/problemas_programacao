/*
	problem 1161
	factorial sum
	by lucas correa
*/
#include <stdio.h>

unsigned long long int factorial(int num)
{
	unsigned long long int factorial = 1;

	while(num)
	{
		factorial *= num;
		num--;
	}

	return factorial;
}

int main (void)
{
	unsigned long long int sum = 0; 
	int num;
	
	while(scanf("%d%*c", &num) != EOF)
	{

		sum += factorial(num);

		scanf("%d%*c", &num);
		
		sum += factorial(num);

		printf("%llu\n", sum);

		sum = 0;
	}

	return 0;
}
