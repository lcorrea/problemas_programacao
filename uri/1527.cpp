#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
    vector <int> h; 
    vector <int> p;
    vector <int> points;

    public:

    UnionFind(int n, vector <int> &pts)
    {
        h.resize(n, 0);
        p.resize(n);
        points.resize(n);

        for (int i = 0; i < n; i++)
            p[i] = i, points[i] = pts[i];
    }

    int find(int a)
    {
        return p[a] == a ? a : p[a] = find(p[a]);
    }

    void join(int a, int b)
    {
        int pa = find(a);
        int pb = find(b);

        if (pa == pb)
            return;

        if (h[pa] > h[pb]) {
            points[pa] += points[pb];
            p[pb] = pa;
        }
        else
        {
            points[pb] += points[pa];
            p[pa] = pb;

            if (h[pa] == h[pb])
                h[pb]++;
        }
    }

    int getPoints(int a)
    {
        int pa = find(a);

        return points[pa];
    }
};

int main (void)
{
    int n, m;

    while (scanf ("%d %d", &n, &m), n and m)
    {
        int ans = 0;

        vector <int> points(n);

        for (int i = 0; i < n; i++)
            scanf ("%d", &points[i]);

        UnionFind guilds(n, points);

        for (int i = 0, q, a, b; i < m; i++)
        {
            scanf ("%d %d %d", &q, &a, &b);

            a--; b--;

            if (q == 1)
                guilds.join (a, b);
            else
            {
                int rg = guilds.find(0);
                int ag = guilds.find(a);
                int bg = guilds.find(b);
                int pta = guilds.getPoints(a);
                int ptb = guilds.getPoints(b);

                if (ag == rg and bg != rg)
                    ans += pta > ptb;
                if (bg == rg and ag != rg)
                    ans += ptb > pta;
            }
        }

        printf ("%d\n", ans);
    }

    return 0;
}

