#include <bits/stdc++.h>

using namespace std;

int main()
{
    const int dx[4] = {0, 0, -1, 1};
    const int dy[4] = {-1, 1, 0, 0};
    int n, m, k, l;
    int dist[1001][1001];
    int houses[100001];
    int teste = 1;
    queue <int> q;

    while (scanf ("%d %d", &n, &m), n and m)
    {
        memset(dist, -1, sizeof(dist));

        scanf ("%d", &k);

        for (int i = 0; i < k; i++)
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            houses[i] = (a-1)*n + (b-1);
        }

        sort(houses, houses+k);

        for (int i = 0; i < k; i++)
        {
            int y = houses[i] / n;
            int x = houses[i] % n;

            dist[y][x] = houses[i];

            q.push(dist[y][x]);
        }

        while (!q.empty())
        {
            int y = q.front() / n;
            int x = q.front() % n;

            q.pop();

            for (int i = 0; i < 4; i++)
            {
                int newy = y + dy[i];
                int newx = x + dx[i];

                if (newy >= 0 and newy < n)
                    if (newx >= 0 and newx < m)
                        if (dist[newy][newx] == -1)
                        {
                            dist[newy][newx] = dist[y][x];
                            q.push(newy*n + newx);
                        }
            }
        }

        scanf ("%d", &l);

        if (teste > 1) putchar('\n');

        printf ("Instancia %d\n", teste++);
        for (int i = 0, a, b; i < l; i++)
        {
            scanf ("%d %d", &a, &b);

            int ansy = (dist[a-1][b-1] / n) + 1;
            int ansx = (dist[a-1][b-1] % n) + 1;

            printf ("%d %d\n", ansy, ansx);
        }
    }
}

