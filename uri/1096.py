#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    for i in range(1,10, 2):
        for j in range(7, 4, -1):
            stdout.write("I=%d J=%d\n" % (i, j))

if __name__ == "__main__":
    main()

