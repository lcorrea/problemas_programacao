#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int f[40], r[40];

int fib(int i)
{
	if (f[i] != -1) return f[i];

	f[i] = fib(i-1) + fib(i-2);
	r[i] = 2 + r[i-1] + r[i-2];

	return f[i];
}

int main (void)
{
	int n, x;

	memset (f, -1, sizeof(f));
	memset (r, 0, sizeof(r));

	f[0] = 0; f[1] = 1; r[1] = r[0] = 0;

	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%d", &x);
		fib(x);

		printf ("fib(%d) = %d calls = %d\n", x, r[x], f[x]);
	}

	return 0;
}

