/*
	problem 1542
	reading books
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	int q, p, d, n;
	
	scanf("%d%*c", &q);
	while(q)
	{
		scanf("%d %d%*c", &d, &p);
		n = q*d*p/(p-q);
		printf("%d pagina", n);

		if(n > 1)
			putchar('s');

		putchar('\n');
		scanf("%d%*c", &q);
	}

	return 0;
}
