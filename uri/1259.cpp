#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    
    while (cin >> n, !cin.eof())
    {
        vector <int> odd;
        vector <int> even;

        for (int i = 0, a; i < n; i++)
        {
            cin >> a;

            if (a%2)
                odd.push_back (a);
            else
                even.push_back(a);
        }

        sort (even.begin(), even.end());
        sort (odd.rbegin(), odd.rend());

        for (int i = 0, sz = (int) even.size(); i < sz; i++)
            cout << even[i] << endl;
        for (int i = 0, sz = (int) odd.size(); i < sz; i++)
            cout << odd[i] << endl;
    }

    return 0;
}

