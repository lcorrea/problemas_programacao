#include <bits/stdc++.h>

using namespace std;

bool check (int mvx, int mvy, int mat[6][6])
{
    bool ok = false;
    for (int i = 1; i <= 4; i++) {
        for (int j = 1; j <= 4; j++) {
            if (mat[i][j] == 2048) return false;
            else if (mat[i][j]) {
                int next = mat[i+mvy][j+mvx];
                ok |= (mat[i][j] == next) or (next == 0);
            }
        }
    }

    return ok;
}

int main (void)
{
    int n;

    scanf("%d", &n);

    while (n--) {
        int mat[6][6] = {0};
        char str[4][20] = {"DOWN", "LEFT", "RIGHT", "UP"};
        int mvx[4] = {0, -1, 1, 0};
        int mvy[4] = {1, 0, 0, -1};

        memset(mat, -1, sizeof(mat));
        for (int i = 1; i <= 4; i++)
            for (int j = 1; j <= 4; j++)
                scanf("%d", &mat[i][j]);

        int mov = 0;
        for (int i = 0; i < 4; i++)
            if (check(mvx[i], mvy[i], mat)) 
                printf(mov++ ? " %s" : "%s", str[i]);
        
        puts(mov ? "" : "NONE");
    }

    return 0;
}

