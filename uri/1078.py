#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    n = int(stdin.readline())

    for i in range(1, 11):
        stdout.write("{:d} x {:d} = {:d}\n".format(i, n, i*n))

if __name__ == "__main__":
    main()

