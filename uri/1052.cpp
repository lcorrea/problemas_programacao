#include <iostream>

using namespace std;

int main(void)
{
	unsigned int month;
	string months[12] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

	cin >> month;

	cout << months[month-1] << endl;

	return 0;
}

