#include <iostream>

using namespace std;

typedef unsigned long long int u64_t;

u64_t fat[17];

u64_t calc (u64_t f)
{
	if (fat[f] != 0)
		return fat[f];

	return fat[f] = f*calc(f-1);
}

int main (void)
{
	string word;
	fat[1] = 1;

	while (cin >> word, word[0] != '0')
		cout << calc(word.length()) << endl;

	return 0;
}

