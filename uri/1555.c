#include <stdio.h>

#define max(a, b) ((a>b)?(a):(b))

int main (void)
{
	int n, x, y;
	int x2, y2, rr, rb, rc, maxn;
	scanf ("%d", &n);
	while (n--)
	{
		scanf ("%d %d", &x, &y);
		x2 = x*x;
		y2 = y*y;
		rr = 9*x2 + y2;
		rb = 2*x2 + 25*y2;
		rc = -100*x + y2*y;
		maxn = max (rr, max(rb, rc));
		if (maxn == rr) puts ("Rafael ganhou");
		else if (maxn == rc) puts ("Carlos ganhou");
		else puts ("Beto ganhou");		
	}

	return 0;
}


