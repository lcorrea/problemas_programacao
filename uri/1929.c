#include <stdio.h>

#define isTriangle(a,b,c) (((a+b)>c) && ((a+c)>b) && ((b+c)>a))

int main (void)
{
		int a, b, c, d;

		scanf ("%d %d %d %d", &a, &b, &c, &d);

		if (isTriangle(a,b,c) || isTriangle(a,b,d) ||
			isTriangle(c,b,d) || isTriangle(a,c,d))
				puts ("S");
		else
				puts ("N");

		return 0;
}

