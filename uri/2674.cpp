#include <bits/stdc++.h>

using namespace std;

int primes[100001];

void crivo()
{
    memset(primes, 1, sizeof(primes));
    
    primes[0] = 0;
    primes[1] = 0;
    primes[2] = 1;

    for (unsigned long long int i = 2; i <= 100000; i++) {

        if (primes[i]) {

            for (unsigned long long int j = i*i; j <= 100000; j += i) {

                primes[j] = 0;
            }
        }
    }
}

int main (void)
{
    int n;
    crivo();

    while (EOF != scanf("%d", &n)) {

        if (primes[n]) {

            bool super = true;

            while (n and super) {

                super = primes[n % 10];

                n /= 10;
            }

            puts(super ? "Super" : "Primo");
        }
        else
            puts("Nada");
    }

    return 0;
}

