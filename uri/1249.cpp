#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char alphabet[26];
	const char a = 'a';

	for (int i = 0; i < 26; i++)
		alphabet[i] = a + i;

	char ch;
	do{
		int i = 0;
		while ((ch = getchar()) != EOF)
		{
			if (isalpha(ch))
			{
				char c = tolower(ch);
				char p = alphabet[((c - a) + 13)%26];
				
				putchar(isupper(ch) ? (toupper(p)) : (p));
			}
			else
				putchar (ch);
		}
	}while (ch != EOF);

	return 0;
}

