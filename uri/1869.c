#include <stdio.h>

int main (void)
{
	unsigned long long int n, r;
	int len;
	char base32[33], number[1000];

	for (len = 0; len < 10; len++)
		base32[len] = len + '0';

	for (r = 'A'; len < 32 ; len++, r++)
		base32[len] = r;

	while (scanf ("%llu", &n), n)
	{
		len = 0;
		while (n)
		{
			r = n % 32ull;
			n /= 32ull;
			number[len++] = base32[r];
		}

		while (len) putchar (number[--len]);
		putchar ('\n');
	}
	puts ("0");

	return 0;
}

