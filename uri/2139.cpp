#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int m, d;
  int days = 359, tb[13][32] = {0};

  for (int i = 1; i <= 7; i++)
    for (int j = 1, last = i == 2 ? 29 : 30 + i%2; j <= last; j++, days--)
      tb[i][j] = days;

  for (int i = 1; i <= 5; i++)
    for (int j = 1, last = 30 + i%2; j <= last; j++, days--)
      tb[i+7][j] = days;

  while (EOF != scanf ("%d%d", &m, &d))
  {
    if (tb[m][d] < 0)
      puts ("Ja passou!");
    else if (tb[m][d] == 1)
      puts ("E vespera de natal!");
    else if (tb[m][d] == 0)
      puts ("E natal!");
    else
      printf ("Faltam %d dias para o natal!\n", tb[m][d]);
  }

  return 0;
}

