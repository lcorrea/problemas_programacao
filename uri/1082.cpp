#include <bits/stdc++.h>

using namespace std;

vector <int> visited;
vector < vector <int> > graph;

void dfs (int v, set <char>& component)
{
	visited[v] = 1;

	for (int i = 0; i < graph[v].size(); i++)
	{
		int u = graph[v][i];

		if (!visited[u])
		{
			component.insert (u+'a');
			dfs (u, component);
		}
	}
}

int main (void)
{
	int n, t = 1;
	
	scanf ("%d%*c", &n);

	while (n--)
	{
		int v, e;
		vector < set <char> > components;

		printf ("Case #%d:\n", t++);

		scanf ("%d %d%*c", &v, &e);

		visited.clear(); visited.assign(v, 0);
		graph.clear();	graph.resize (v);

		for (int i = 0; i < e; i++)
		{
			char u, x;

			scanf ("%c %c%*c", &u, &x);

			graph[u-'a'].push_back (x-'a');
			graph[x-'a'].push_back (u-'a');
		}

		for (int i = 0; i < v; i++)
		{
			if (!visited[i])
			{
				set <char> cp;

				cp.insert(i+'a');
				dfs (i, cp);
				components.push_back (cp);
			}
		}

		for (int i = 0, sz = components.size(); i < sz; i++)
		{
			for (set <char>::const_iterator it = components[i].begin(); it != components[i].end(); it++)
				printf ("%c,", *it);
			putchar ('\n');
		}

		printf ("%d connected components\n\n", components.size());
	}

	return 0;
}

