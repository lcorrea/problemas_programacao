#include <bits/stdc++.h>

using namespace std;

vector < vector <int> > graph;
vector <int> dfs_num;
vector <int> dfs_low;
vector <int> dfs_parent;
int dfs_root;
int dfs_counter;
int root_children;
int bridges;

void detect_bridge(int u)
{
  dfs_low[u] = dfs_num[u] = dfs_counter++;

  for (int i = 0; i < (int) graph[u].size(); i++)
  {
    int v = graph[u][i];

    if (!dfs_num[v])
    {
      dfs_parent[v] = u;

      if (u == dfs_root)
        root_children++;

      detect_bridge(v);

      if (dfs_low[v] >= dfs_num[u]); //articulation vertex

      if (dfs_low[v] > dfs_num[u])
        bridges++;
      
      dfs_low[u] = min(dfs_low[u], dfs_low[v]);
    }
    else if (v != dfs_parent[u])
      dfs_low[u] = min(dfs_low[u], dfs_num[v]);
  }
}

int main (void)
{
  int c, p;

  while (scanf ("%d%d", &c, &p) != EOF)
  {
    bridges = 0;
    dfs_counter = 0;
    dfs_num.assign (c, 0);
    dfs_low.assign (c, 0);
    dfs_parent.assign (c, 0);
    graph.clear();
    graph.resize(c);

    for (int i = 0, u, v; i < p; i++)
    {
      scanf ("%d%d", &u, &v);

      graph[u-1].push_back(v-1);
      graph[v-1].push_back(u-1);
    }

    for (int i = 0; i < c; i++)
    {
      if (!dfs_num[i])
      {
        dfs_root = i;
        root_children = 0;
        detect_bridge(i);
      }
    }

    printf ("%d\n", bridges);
  }

  return 0;
}

