#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <bitset>

#define MODULUS 1000000007ULL

using namespace std;

typedef long long unsigned int ull;

vector <ull> fat;
bitset <10000010> bs;
vector <ull> primes;

void sieve (ull upperbound)
{
	ull size_p = upperbound + 1;
	
	bs.set();

	bs[0] = bs[1] = 0;

	for (ull i = 2; i <= size_p; i++)
	{
		if (bs[i])
		{
			for (ull j = i * i; j <= size_p; j += i)
				bs[j] = 0;

			primes.push_back(i);
		}
	}
}



ull factorial (ull n)
{
	if (fat[n] == 0)
		fat[n] = ((n % MODULUS) * (factorial(n-1) % MODULUS)) % MODULUS;
	
	return fat[n];
}

ull sumDiv (ull N)
{
	ull PF_idx = 0, PF = primes[PF_idx], ans = 1;
	ull p_ans = 0;
	// start from ans = 1
	while (PF * PF <= N)
	{
		ull power = 0;
		while (N % PF == 0) { N /= PF; power++; }
		ans *= ((ull)pow((double)PF, power + 1.0) - 1) / (PF - 1);
		PF = primes[++PF_idx];
	 }
	 if (N != 1) ans *= ((ull)pow((double)N, 2.0) - 1) / (N - 1); // last

	 return ans;
}

int main (void)
{
	ull n;

	fat.assign(100005, 0);

	fat[0] = 1;
	fat[1] = 1;

	sieve (100005);

	cin >> n;

	while (!cin.eof())
	{
		n = factorial (n);

		unsigned int sum = 0;

		for (unsigned int i = 1; i < n; i++)
			if (n % i == 0)
				sum += i;

		cout << sum % MODULUS << ' ' << n << endl;

		cin >> n;
	}

	return 0;
}

