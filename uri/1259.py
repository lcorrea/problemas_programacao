#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    par, imp = [], []
    n = int(stdin.readline())

    for i in map(int, stdin.readlines()):
       imp.append(i) if (i & 1) else par.append(i)

    par.sort()
    imp.sort(reverse = True)

    ans = lambda x: stdout.write(str(x) + '\n')

    for i in par:
        ans(i)

    for i in imp:
        ans(i)

if __name__ == "__main__":
    main()

