#include <bits/stdc++.h>

using namespace std;

inline bool check(int die[6])
{
    int n[7] = {0};

    for (int i = 0; i < 6; i++) {
        if (die[i] >= 1 and die[i] <= 6 and n[die[i]] == 0)
            n[die[i]] = 1;
        else
            return false;
    }

    if ((die[0] + die[5]) == 7)
        if ((die[1] + die[3]) == 7)
            if ((die[2] + die[4]) == 7)
                return true;

    return false;
}

int main (void)
{
    int n;

    scanf("%d", &n);

    while (n--) {
        int die[6];

        for (int i = 0; i < 6; i++)
            scanf("%d", die + i);

        puts (check(die) ? "SIM" : "NAO");
    }

    return 0;
}

