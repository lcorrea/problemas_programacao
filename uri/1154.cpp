#include <iostream>
#include <cstdio>

using namespace std;

int main (void)
{
	float m = 0;
	unsigned int qnt = 0;
	int age;

	cin >> age;

	while (age >= 0)
	{
		m += age;
		qnt++;
		cin >> age;
	}

	printf ("%.2f\n", (float) m / qnt);

	return 0;
}

