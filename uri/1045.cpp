#include <iostream>
#include <algorithm>

using namespace std;

bool comp(double a, double b)
{
	return a > b;
}

int main(void)
{
	double abc[3];

	cin >> abc[0] >> abc[1] >> abc[2];

	sort(abc,abc+3, comp);

	if(abc[0] >= abc[1] + abc[2])
		cout << "NAO FORMA TRIANGULO\n";
	else
	{
		if(abc[0]*abc[0] == abc[1]*abc[1] + abc[2]*abc[2])
			cout << "TRIANGULO RETANGULO\n";

		else if(abc[0]*abc[0] > abc[1]*abc[1] + abc[2]*abc[2])
			cout << "TRIANGULO OBTUSANGULO\n";

		else if(abc[0]*abc[0] < abc[1]*abc[1] + abc[2]*abc[2])
			cout << "TRIANGULO ACUTANGULO\n";

		if(abc[0]*3 == abc[0] + abc[1] + abc[2])
			cout << "TRIANGULO EQUILATERO\n";
		else if(abc[0] == abc[1] || abc[0] == abc[2] || abc[1] == abc[2])
			cout << "TRIANGULO ISOSCELES\n";
	}


	return 0;

}

