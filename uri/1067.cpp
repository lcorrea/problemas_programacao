/*
	problem 1067
	odd numbers
	by lucas correa
*/

#include <iostream>

using namespace std;

int main(void)
{
	unsigned int x, n = 1;

	cin >> x;

	while(n <= x)
	{
		cout << n << endl;
		n += 2;
	}

	return 0;
}

