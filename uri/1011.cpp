/*
	problem 1011
	sphere
	by lucas correa
*/

#include <stdio.h>
#include <math.h>

#define PI 3.14159

int main(void)
{
	unsigned int r;

	scanf("%u%*c", &r);

	printf("VOLUME = %.3f\n", (4/3.0)*r*r*r*PI);

	return 0;
}

