#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;

	scanf ("%d", &t);

	while(t--)
	{
		unsigned long long cnt = 1;
		string pw;

		cin >> pw;

		for (int i = 0, len = pw.length(); i < len; i++)
		{
			char ch = tolower (pw[i]);

			if (ch == 'a' or ch == 'e' or ch == 'i' or ch == 'o' or ch == 's')
				cnt *= 3;
			else
				cnt *= 2;
		}

		printf ("%llu\n", cnt);
	
	}

	return 0;
}

