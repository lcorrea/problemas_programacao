#!/usr/bin/env python3

numbers = [int(x) for x in input().split()]

for i in sorted(numbers):
    print(i)

print("")

for i in numbers:
    print(i)
