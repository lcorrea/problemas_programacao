#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

typedef pair <int, int> ii;
typedef vector <int> vi;
typedef vector < ii > vii;
typedef vector < vii > AdjList;

int main (void)
{
  int c, v;

  scanf ("%d%d", &c, &v);

  AdjList graph(c);

  for (int i = 0, s, t, p; i < v; i++)
  {
    scanf ("%d%d%d", &s, &t, &p);

    graph[s-1].push_back (make_pair (t-1, p));
    graph[t-1].push_back (make_pair (s-1, p));
  }


  vector <int> dist_impar(c, INF);
  vector <int> dist_par (c, INF);
  priority_queue <ii, vector <ii>, greater <ii> > pq;

  dist_par[0] = 0;
  pq.push (ii(dist_par[0], 0));

  while (!pq.empty())
  {
    int u = pq.top().second;
    int dist_u = pq.top().first;
    pq.pop();

    if (dist_u > dist_impar[u] and dist_u > dist_par[u])
      continue;

    for (int i = 0; i < (int) graph[u].size(); i++)
    {
      ii v = graph[u][i];

      if (dist_par[u] + v.second < dist_impar[v.first])
      {
        dist_impar[v.first] = dist_par[u] + v.second;

        pq.push(ii(dist_impar[v.first], v.first));
      }

      if (dist_impar[u] + v.second < dist_par[v.first])
      {
        dist_par[v.first] = dist_impar[u] + v.second;

        pq.push(ii(dist_par[v.first], v.first));
      }
    }
  }

  printf ("%d\n", dist_par[c-1] == INF ? -1 : dist_par[c-1]);

  return 0;
}

