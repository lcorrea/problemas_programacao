#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int b[m], n, m;
    set<int> A, B;

    scanf ("%d%d", &n, &m);

    for (int i = 0, x; i < n; i++)
    {
        scanf ("%d", &x);

        A.insert(x);
    }

    for (int i = 0; i < m; i++)
        scanf ("%d", b + i);

    for (int i = 0; i < m; i ++)
    {
        if (A.find(b[i]) == A.end())
        {
            bool found = false;

            for (set<int>::iterator it = B.begin();
                    it != B.end() and !found; it++)
            {
                int x = b[i] - (*it);

                found = B.find(x) != B.end();
            }

            if (!found)
                return printf ("%d\n", b[i]), 0;
        }

        B.insert(b[i]);
    }

    puts ("sim");

    return 0;
}

