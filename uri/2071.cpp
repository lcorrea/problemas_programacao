#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;

typedef struct
{
    ii max;
    ii mPrefix;
    ii mSuffix;
}Node;

class ST {
    private:
        vector <Node> st;
        vector <int> v, prefix, suffix;
        int n;

        int max(int i, int j)
        {
            return (i > j) ? i : j;
        }

        ii max (ii a, ii b)
        {
            int pa = prefixSum(a);
            int pb = prefixSum(b);

            if (pa == pb)
                return ((a.second - a.first + 1) > (b.second - b.first + 1)) ? a : b;

            return (pa > pb) ? a : b;
        }

        void build (int p, int l, int r)
        {
            if (l == r)
            {
                st[p].max = st[p].mPrefix = st[p].mSuffix = {l, l};
                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            build (left, l, mid);
            build (right, mid+1, r);

            Node vl = st[left];
            Node vr = st[right];

            st[p].mPrefix = max(vl.mPrefix,
                    max( {l, mid}, {l, vr.mPrefix.second}));

            st[p].mSuffix = max (vr.mSuffix, 
                    max ( {mid+1, r}, {vl.mSuffix.first, r}));

            st[p].max = max (vr.max,
                    max ( vl.max, {vl.mSuffix.first, vr.mPrefix.second}));
/*
            printf ("vl:\n"
                    "\tmax[%d %d]: = %d\n"
                    "\tmPrefix[%d %d]: %d\n"
                    "\tmSuffix[%d %d]: %d\n",
                    vl.max.first, vl.max.second, prefixSum(vl.max),
                    vl.mPrefix.first, vl.mPrefix.second, prefixSum(vl.mPrefix),
                    vl.mSuffix.first, vl.mSuffix.second, prefixSum(vl.mSuffix));
            
            printf ("vr:\n"
                    "\tmax[%d %d]: = %d\n"
                    "\tmPrefix[%d %d]: %d\n"
                    "\tmSuffix[%d %d]: %d\n",
                    vr.max.first, vr.max.second, prefixSum(vr.max),
                    vr.mPrefix.first, vr.mPrefix.second, prefixSum(vr.mPrefix),
                    vr.mSuffix.first, vr.mSuffix.second, prefixSum(vr.mSuffix));
            
            printf ("#[%d, %d] = %d\n", vl.mSuffix.first, vr.mPrefix.second, prefixSum({vl.mSuffix.first, vr.mPrefix.second}));
 
            printf ("build(%d, %d):\n"
                    "\t max = [%d, %d]\n"
                    "\t mPrefix = [%d, %d]\n"
                    "\t mSuffix = [%d, %d]\n",
                    l, r,
                    st[p].max.first, st[p].max.second,
                    st[p].mPrefix.first, st[p].mPrefix.second,
                    st[p].mSuffix.first, st[p].mSuffix.second);*/
        }

        Node query (int p, int l, int r, int i, int j)
        {
            //printf ("%d %d\n", l, r);
            if (i > r or j < l)
            {
                Node x;

                x.max = x.mPrefix = x.mSuffix = {-1, -1};

                return x;
            }

            if (i <= l and j >= r)
                return st[p];

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            Node vl = query (left, l, mid, i, j);
            Node vr = query (right, mid+1, r, i, j);

/*            printf ("query(%d, %d):\n"
                    "\tleft(%d, %d): [%d %d]\n"
                    "\tright(%d, %d): [%d %d]\n",
                    //"\tInterval(%d, %d) = %d\n"
                    //"\tmax: [%d, %d] = %d\n",
                    l, r,
                    l, mid, vl.max.first, vl.max.second,
                    mid+1, r, vr.max.first, vr.max.second);
                    //pInterval.first, pInterval.second, prefixSum(pInterval),
//                    ans.max.first, ans.max.second, ans.max.second - ans.max.first + 1);*/

            if (vl.max.first == -1)
                return vr;

            if (vr.max.first == -1)
                return vl;

            Node ans;

            ans.mPrefix = max(vl.mPrefix,
                    max( {l, mid}, {l, vr.mPrefix.second}));

            ans.mSuffix = max (vr.mSuffix, 
                    max ( {mid+1, r}, {vl.mSuffix.first, r}));

            ans.max = max (vr.max,
                    max ( vl.max, {vl.mSuffix.first, vr.mPrefix.second}));

            return ans;
        }

    public:
        ST(int _n, vector <int> &_v)
        {
            n = _n;
            v = _v;

            Node x; x.max = x.mPrefix = x.mSuffix = {0,0};
            st.assign (n << 2, x);
            prefix.assign (n, 0);

            prefix[0] = v[0];
            for (int i = 1; i < n; i++)
                prefix[i] += prefix[i-1] + v[i];//, printf ("[0, %d] = %d\n", i, prefix[i]);

            build (1, 0, n-1);
        }

        int prefixSum (ii x)
        {
            int i = x.first;
            int j = x.second;

            if (i == 0)
                return prefix[j];

            return prefix[j] - prefix[i-1];
        }

        Node query (int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }
};

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n, q;

        scanf ("%d", &n);

        vector <int> v(n);
        for (int i = 0; i < n; i++)
            scanf ("%d", &v[i]);

        ST sTree(n, v);

        scanf ("%d", &q);

        while (q--)
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            Node x = sTree.query(a-1, b-1);
            printf ("%d %d\n", sTree.prefixSum(x.max),
                    x.max.second - x.max.first + 1);

            //puts ("\n\n\n");
        }

    }

    return 0;
}

