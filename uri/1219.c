#include <stdio.h>
#include <math.h>

#define max(a,b) ((a>b)?(a):(b))

int main (void)
{
	int a, b, c;

	while (scanf ("%d %d %d", &a, &b, &c) != EOF)
	{
		double k = (a+b+c)/2.0;
		double areat = sqrt (k*(k-a)*(k-b)*(k-c));
		double circle1 = M_PI*pow(areat/k, 2);
		double circle2 = M_PI*pow(a*b*c/(4*areat), 2);
		printf ("%.4lf %.4lf %.4lf\n", circle2 - areat, areat - circle1, circle1);
	}

	return 0;
}
