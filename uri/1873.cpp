#include <cstdio>
#include <map>
#include <string>

using namespace std;


int main (void)
{
	int table[6][6] = {0};
	int n;
	map <string, int> m;

	m["pedra"] = 1;
	m["papel"] = 2;
	m["tesoura"] = 3;
	m["lagarto"] = 4;
	m["spock"] = 5;
	
	table[3][2] = 1;
	table[2][1] = 1;
	table[1][4] = 1;
	table[4][5] = 1;
	table[5][3] = 1;
	table[3][4] = 1;
	table[4][2] = 1;
	table[2][5] = 1;
	table[5][1] = 1;
	table[1][3] = 1;

	scanf ("%d", &n);

	while (n--)
	{
		char rajesh[20], sheldon[20];

		scanf ("%s %s", rajesh, sheldon);

		if (m[rajesh] == m[sheldon]) puts ("empate");
		else if (table[m[rajesh]][m[sheldon]]) puts ("rajesh");
		else puts ("sheldon");
	}

	return 0;
}

