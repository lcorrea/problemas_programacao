#include <stdio.h>
#include <string.h>

#define max(a, b) ((a>b)?(a):(b))

int main (void)
{
	int n, maxlen = 0;
	char c;
	char lines[110][55];
	int l;
	scanf ("%d%*c", &n);
	while (n)
	{
		maxlen = 0;
		for (l = 0; l < n; l++)
		{
			while (c = getchar(), c == ' ');
			int i = 0;
			do
			{
				if (c == ' ')
				{
					lines[l][i++] = c;
					while (c = getchar(), c == ' ');
				}
				if (c == '\n') break;
				lines[l][i++] = c;
			} while (c = getchar(), c != '\n');
			i--;
			while (i > 0 && lines[l][i] == ' ') lines[l][i--] = '\0';
			lines[l][i+1] = '\0';
			maxlen = max (i+1, maxlen);
		}

		for (l = 0; l < n; l++)
		{
			int blanks = maxlen - strlen (lines[l]);
			while (blanks--) putchar (' ');
			puts (lines[l]);
		}
		scanf ("%d%*c", &n);
		if (n) putchar ('\n');
	}

	return 0;
}

