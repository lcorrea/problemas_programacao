#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int c;

	cin >> c;

	while (c--)
	{
		int p, win = 0;
		map <char, int> team; team['R'] = 0; team['G'] = 1; team['B'] = 2;
		map <char, int> pt;
		int tb[3][3] = { {0, 2, 1}, {1, 0, 2}, {2, 1, 0}};
		
		cin >> p;

		for (int i = 0; i < p; i++)
		{
			char a, b;
			
			cin >> a >> b;

			pt[a] += tb[team[a]][team[b]];
			win = max (pt[a], win);
		}

		int cnt = 0;

		cnt += win == pt['R'];
		cnt += win == pt['G'];
		cnt += win == pt['B'];

		if (cnt == 3)
			cout << "trempate";
		else if (cnt == 2)
			cout << "empate";
		else
			cout << (win == pt['R'] ? "red" : win == pt['G'] ? "green" : "blue");

		cout << endl;
	}

	return 0;
}

