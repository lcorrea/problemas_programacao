#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, totalKg = 0;
	double totalMoney = 0;
	
	scanf ("%d%*c", &n);

	for (int i = 1; i <= n; i++)
	{
		double spent;

		scanf ("%lf%*c", &spent);
		totalMoney += spent;

		char ch;
		int fruits = 1;

		while ((ch = getchar()) != '\n')
			fruits += isblank (ch);

		printf ("day %d: %d kg\n", i, fruits);

		totalKg += fruits;
	}

	printf ("%.02lf kg by day\n", totalKg/(double)n);
	printf ("R$ %.02lf by day\n", totalMoney/n);

	return 0;
}

