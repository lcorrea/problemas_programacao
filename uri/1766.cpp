#include <bits/stdc++.h>

using namespace std;

typedef pair <int, pair <int , pair < double, string > > > reindeer;

#define create_reindeer(a, b, c, d) (make_pair(a, make_pair(b, make_pair(c, d))))
#define get_name_reindeer(a) (a.second.second.second)

int main (void)
{
	int t;
	vector <reindeer> r(10000);

	cin >> t;

	for (int j = 1; j <= t; j++)
	{
		int a, b;
		int n, m;
		double c;
		string d;

		cin >> n >> m;

		for (int i = 0; i < n; i++)
		{
			cin >> d >> a >> b >> c;

			r[i] = create_reindeer(-a, b, c, d);
		}

		sort (r.begin(), r.begin() + n);

		printf ("CENARIO {%d}\n", j);
		for (int i = 0; i < m; i++)
		{
			string name = get_name_reindeer(r[i]);
			printf ("%d - %s\n", i+1, name.c_str());
		}
	}

	return 0;
}

