#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int x,y, s = 0;

	cin >> x >> y;

	if(x>y)
		swap(x,y);

	while(x <= y)
	{
		if(x%13)
			s += x;
		x++;
	}

	cout << s << endl;

	return 0;
}
