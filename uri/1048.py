#!/usr/bin/env python3

salario = float(input())

ans = lambda s, r: print("Novo salario: %.02f\nReajuste ganho: %.02f\nEm percentual: %d %%" % (s*(1.0 + r/100.0), s * r / 100.0, r))

if (salario < 400.01):
    ans(salario, 15)
elif (salario < 800.01):
    ans(salario, 12)
elif (salario < 1200.01):
    ans(salario, 10)
elif (salario <= 2000):
    ans(salario, 7)
else:
    ans(salario, 4)
