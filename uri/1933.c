#include <stdio.h>

#define max(a, b) ((a>b)?(a):(b))

int main (void)
{
	int a, b;
	scanf ("%d %d", &a, &b);
	printf ("%d\n", max(a,b));
	return 0;
}

