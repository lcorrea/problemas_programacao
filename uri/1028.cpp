/*
	problem 1028
	collectable cards
	by lucas correa
	time: 00:11:34
*/

#include <iostream>

using namespace std;


unsigned int gcd(unsigned int a, unsigned int b)
{
	unsigned int r = 0;

	while(b != 0)
	{
		r = a%b;
		a = b;
		b = r;
	}

	return a;
	
}


int main(void)
{
	unsigned int f1, f2, n;

	cin >> n;
	while(n--)
	{

		cin >> f1 >> f2;

		//get gdc
		cout << gcd(f1,f2) << endl;

	}

	return 0;
}
