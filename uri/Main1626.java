import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.BitSet;

public class Main1626 {
	static long[] fatn = new long[100002];
	static long[] primes = new long[100002];
	static long pIndex = 0;
	static BitSet bs = new BitSet (10e9 + 8);
	static final long modulo = 1000000007;

	public static void main (String[] args) throws IOException {
		Arrays.fill (fatn, 0);
		fatn[1] = 1;
		System.out.println  (calcFat(100000));
	}

	static void sieve ()
	{
		for (long i = 2; i <= modulo; i++)
		{
			if (bs.get (i))
			{
				for (long j = i*i; j <= modulo; j += i)
					bs.clear (j);
				primes[pIndex++] = i
			}

		}

	}

	static long calcFat (int n)
	{
		if (fatn[n] != 0) return fatn[n];

		for (int i = 2; i < 100001; i++)
			fatn[i] = ((fatn[i-1] % modulo) * i) % modulo;

		return fatn[n];
	}

	static long calcSumDiv (int n)
	{
		
	}

}

