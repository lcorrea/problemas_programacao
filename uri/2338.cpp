#include <bits/stdc++.h>

using namespace std;

map <string, char> dic;

string decode_letter(char *str)
{
    string simbol;
    char *ptr = strtok (str, ".");

    while (ptr != NULL)
    {
        simbol += (string(ptr) == "=") ? '.' : '-';

        ptr = strtok (NULL, ".");
    }

    return simbol;
}

string decode_word(char *w)
{
    char letter[200], *ptra, *ptrb;
    int i, len;
    string word;

    len = strlen (w);
    ptra = w;
    ptrb = strstr(w, "...");

    while (ptrb != NULL)
    {
        i = 0;
        while (ptra < ptrb)
            letter[i++] = *ptra++;
        letter[i] = '\0';

        word += dic[decode_letter(letter)];

        ptra += 3;
        ptrb = strstr(ptrb+3, "...");
    }

    word += dic[decode_letter(ptra)];

    return word;
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    char morse[26][10] = {
        ".-",
        "-...",
        "-.-.",
        "-..",
        ".",
        "..-.",
        "--.",
        "....",
        "..",
        ".---",
        "-.-",
        ".-..",
        "--",
        "-.",
        "---",
        ".--.",
        "--.-",
        ".-.",
        "...",
        "-",
        "..-",
        "...-",
        ".--",
        "-..-",
        "-.--",
        "--.."
    };


    for (char j = 0, i = 'a'; i <= 'z'; i++, j++)
        dic[morse[j]] = i;

    while (t--)
    {
        char str[1001], w[200], *ptra, *ptrb;
        int i, len;
        string message;

        scanf ("%s", str);

        len = strlen (str);
        ptra = str;
        ptrb = strstr(str, "......");

        while (ptrb != NULL)
        {
            i = 0;
            while (ptra < ptrb)
                w[i++] = *ptra++;
            w[i] = '\0';

            if (message.length())
                message += ' ' + decode_word(w);
            else
                message = decode_word(w);

            ptra += 6;
            ptrb = strstr(ptrb+6, "......");
        }

        if (message.length())
            message += ' ' + decode_word(ptra);
        else
            message = decode_word(ptra);

        printf ("%s\n", message.c_str());
    }

    return 0;
}

