#include <bits/stdc++.h>

using namespace std;

int n, m;
char zone[51][51];
int v[51][51];

void dfs (int i, int j)
{
  const int dx[4] = {-1, 0, 1, 0};
  const int dy[4] = {0, -1, 0, 1};

  v[i][j] = 1;
  zone[i][j] = 'T';

  for (int k = 0, x, y; k < 4; k++)
  {
    x = j + dx[k];
    y = i + dy[k];

    if (x >= 0 and x < m)
      if (y >= 0 and y < n)
        if (!v[y][x] and zone[y][x] != 'X')
          dfs (y, x);
  }
}

int main (void)
{
  while (scanf ("%d%d", &n, &m), n and m)
  {
    memset (v, 0, sizeof(v));

    for (int i = 0; i < n; i++)
      scanf ("%s", zone[i]);

    for (int i = 0; i < n; i++)
      for (int j = 0; j < m; j++)
        if (!v[i][j] and zone[i][j] == 'T')
          dfs(i, j);

    for (int i = 0; i < n; i++)
      printf ("%s\n", zone[i]);
    putchar ('\n');
  }

  return 0;
}

