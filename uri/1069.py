#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    n = int(stdin.readline())

    for Case in range(n):
        diamantes = 0
        ch_open = 0

        for ch in stdin.readline():
            if (ch == '<'):
                ch_open += 1
            elif (ch == '>' and ch_open):
                diamantes += 1
                ch_open -= 1

        stdout.write("%d\n" % (diamantes))

if __name__ == "__main__":
    main()

