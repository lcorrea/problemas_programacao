#include <bits/stdc++.h>

using namespace std;

class Team
{
    public:
        int pts,
            score,
            received,
            id;

        double average_basket ()
        {
            return received ? score / (double) received : score;
        }

        Team (int _id)
        {
            pts = score = received = 0;
            id = _id;
        }
};

bool cmp (Team *a, Team *b)
{
    if (a->pts == b->pts)
    {
        double _a = a->average_basket();
        double _b = b->average_basket();

        if (_a == _b)
        {
            if (a->pts == b->pts)
                return a->id < b->id;

            return a->score < b->score;
        }

        return _a < _b;
    }

    return a->pts < b->pts;
}

int main (void)
{
    int n, instancia = 1;

    scanf ("%d", &n);

    while (n)
    {
        vector <Team *> teams;
        int a, b, sc_a, sc_b;

        for (int i = 1; i <= n; i++)
            teams.push_back ( new Team(i));

        for (int i = 0, m = n*(n - 1)/2; i < m; i++)
        {
            scanf ("%d%d%d%d", &a, &sc_a, &b, &sc_b);

            teams[a-1]->score += sc_a; teams[a-1]->received += sc_b;
            teams[b-1]->score += sc_b; teams[b-1]->received += sc_a;

            if (sc_a > sc_b)
            {
                teams[a-1]->pts += 2;
                teams[b-1]->pts += 1;
            }
            else
            {
                teams[b-1]->pts += 2;
                teams[a-1]->pts += 1;
            }
        }

        sort (teams.rbegin(), teams.rend(), cmp);

        printf ("Instancia %d\n", instancia++);
        printf ("%d", teams[0]->id);
        for (int i = 1; i < n; i++)
            printf (" %d", teams[i]->id);
        putchar ('\n');

        scanf ("%d", &n);

        if (n)
            putchar ('\n');
    }

    return 0;
}

