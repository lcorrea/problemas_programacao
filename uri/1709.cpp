#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int p;
	int deck[400000];

	while (scanf ("%d", &p) != EOF)
	{
		for (int i = 1, a = p/2 + 1, b = 1; i < p; i+=2)
			deck[i-1] = a++, deck[i] = b++;

		int cnt = 1;
		int ptr = 0;

		do
		{
			cnt++;
			ptr = deck[ptr] - 1;
		}while (deck[ptr] != 1);

		printf ("%d\n", cnt);
	}
	return 0;
}

