#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf("%d", &n), n) {
        int cnt = 0;

        for (int i = 0, c, v; i < n; i++) {
            scanf("%d %d", &c, &v);
            cnt += v/2;
        }

        printf("%d\n", cnt/2);
    }

    return 0;
}

