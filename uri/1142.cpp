#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, p = 1;
	
	cin >> n;

	while(n--)
	{
		for(unsigned int a = p; a < p + 3; a++)
			cout << a << ' ';
		cout << "PUM" << endl;
		p+=4;
	}

	return 0;
}
