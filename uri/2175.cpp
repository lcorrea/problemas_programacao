#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    pair <float, string> p[3];

    p[0].second = "Otavio";
    cin >> p[0].first;

    p[1].second = "Bruno";
    cin >> p[1].first;

    p[2].second = "Ian";
    cin >> p[2].first;

    sort (p, p + 3);
    
    if (p[0].first == p[1].first)
        cout << "Empate" << endl;
    else
        cout << p[0].second << endl;

    return 0;
}

