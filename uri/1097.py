#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    j = 7
    for i in range(1, 10, 2):
        for l in range(j, j-3, -1):
            stdout.write("I=%d J=%d\n" % (i, l))
        j += 2


if __name__ == "__main__":
    main()

