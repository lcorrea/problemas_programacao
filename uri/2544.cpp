#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
        printf ("%d\n", (int) log2(n));

    return 0;
}

