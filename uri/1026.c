/*
	problem 1026
	To Carry or not to Carry
	by lucas correa
*/
#include <stdio.h>

unsigned char simple_adder(unsigned int a, unsigned int b)
{
	return (a==b)?(0):(1);
}

int main(void)
{
	unsigned int a, b, sum = 0;
	unsigned char i;
	while(scanf("%u %u%*c", &a, &b) != EOF)
	{

		for(i = 0; i < 32; i++)
		{
			sum |= (simple_adder((a & (1<<i)), (b & (1<<i))) << i);
		}

		printf("%u\n", sum);

		sum = 0;
	}

	return 0;
}
