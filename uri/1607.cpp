#include <iostream>

#define mod(a) ((a < 0) ? (a%26 + 26):(a%26))
using namespace std;

int main (void)
{
	int t, op = 0;
	string a, b;
	ios_base::sync_with_stdio (false);

	cin >> t;
	while (t--)
	{
		op = 0;
		cin >> a >> b;

		for (int i = 0; i < a.length(); i++)
		{
			op += mod(((int) b[i] - a[i]));
		}

		cout << op << endl;
	}

	return 0;
}

