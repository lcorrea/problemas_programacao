#include <stdio.h>
#include <math.h>

int main (void)
{
	double a, b;
	scanf ("%lf %lf", &a, &b);
	printf ("%.02lf%%\n", (b-a)/a*100.0);

	return 0;
}

