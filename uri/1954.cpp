//#include <cstdio>
#include <iostream>
#include <cstring>
#include <vector>
#include <set>

using namespace std;

#define MOD 1000000007
#define UNEXPLORED -1

vector <int> graph[100001], ts;
set<int> aux[100001];
bool visited[100001];
int discovery[100001], dp[100001] = {0};
int low[100001], component[100001], parent[100001];
int sc[100001], sn = 0;
int n, m, s, t, a, b, unexplored, tempo = 0;

int calc(int u)
{
    if (dp[u] != -1)
        return dp[u];

    dp[u] = 0;
    for (set<int>::const_iterator it = aux[u].begin(); it != aux[u].end(); it++)
        dp[u] = (dp[u] + calc(*it))%MOD;

    return dp[u];
}

void tarjan(int u)
{
    int w;
    --unexplored;
    low[u] = discovery[u] = tempo++;
    sc[sn++] = u;
    visited[u] = true;

    for (int i = 0; i < (int) graph[u].size(); i++)
    {
        int v = graph[u][i];
        if (discovery[v] == UNEXPLORED)
        {
            parent[v] = u;
            tarjan(v);
        }

        if (visited[v])
            low[u] = min(low[u], low[v]);
    }

    if (low[u] == discovery[u])
    {
        do{
            w = sc[--sn];
            component[w] = u;
            visited[w] = 0;
        }while (sn and w != u);
    }
}

int main (void)
{
    memset(discovery, -1, sizeof(discovery));
    memset(dp, -1, sizeof(dp));
    
    scanf ("%d %d %d %d", &n, &m, &s, &t);

    unexplored = n;
    for (int i = 0; i < m; i++)
    {
        scanf ("%d %d",&a, &b);
        graph[a].push_back(b);
    }

    for (int i = 1; i <= n; i++)
        if (discovery[i] == UNEXPLORED)
        {
            parent[i] = i;
            tarjan(i);
            if (unexplored == 0)
                break;
        }

    for (int u = 1; u <= n; u++)
        for (int i = 0, v; i < (int) graph[u].size(); i++)
        {
            v = graph[u][i];
            if (component[v] != component[u])
                aux[component[u]].insert(component[v]);
        }

    dp[component[t]] = 1;

    printf ("%d\n", calc(component[s]));

    return 0;
}

