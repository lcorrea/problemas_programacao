#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int r = 0, i = 2;
    int cards[5];

    for (int i = 0; i < 5; i++)
        scanf ("%d", cards + i);

    r = cards[1] - cards[0];

    for (int r1 = r; r1/abs(r1) == r/abs(r) and i < 5; i++)
        r1 = cards[i] - cards[i-1];

    if (i == 5)
        puts (r > 0 ? "C" : "D");
    else
        puts ("N");

    return 0;
}

