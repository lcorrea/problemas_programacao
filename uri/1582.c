/*
	problem 1582
	the pythagorean theorem
	by lucas correa
	time: 02:31:21
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int comp(const void *a, const void *b)
{
	return *(unsigned int *)a - *(unsigned int *)b;
}

unsigned int gdc(unsigned int a, unsigned int b)
{
	unsigned int p = 1, i = (a>b)?(b):(a), gdc = 0;
	
	for(; p <= i ; p++)			//--------------
		if(a%p == 0 && 0 == b%p)	// brute force
			gdc = p;		//---------------
			
	return gdc;
}

int main(void)
{

	unsigned int sum = 0, num = 0, tripla[3];
	unsigned char prime = 0;

	while(scanf("%u %u %u%*c", tripla, tripla + 1, tripla + 2) != EOF)
	{
		sum = 0;
		printf("tripla");

		qsort(tripla, 3, sizeof(unsigned int), comp);
		
		sum += pow(tripla[num++], 2);
		sum += pow(tripla[num++], 2);

		if(pow(tripla[num], 2) == sum)
		{
			printf(" pitagorica");

			if(gdc(gdc(tripla[0], tripla[1]), tripla[2]) == 1)
				printf(" primitiva");	
		}

		putchar('\n');

		num = 0;
	}

	return 0;
}
