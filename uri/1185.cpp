#include <iostream>
#include <cstdio>

using namespace std;

int main (void)
{
	char op;
	float v[12][12], result = 0;
	int cnt = 0;

	cin >> op;

	for (unsigned int a = 0; a < 12; a++)
	{
		for (int b = 0, c = 11 - a; b < 12; b++, c--)
		{
			cin >> v[a][b];

			if (c > 0)
			{
				result += v[a][b];
				cnt++;
			}
		
		}
	}
	
	if (op == 'M')
		result = (float) result/cnt;

	printf ("%.1f\n", result);

	return 0;
}

