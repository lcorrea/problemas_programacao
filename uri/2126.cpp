#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  char n1[90], n2[90];
  int t = 1;

  while (scanf ("%s", n1) != EOF)
  {
    char *ptr = n2, *last = NULL;
    int cnt = 0;

    scanf ("%s", n2);

    while ( (ptr = strstr (ptr, n1)) != NULL)
    {
      cnt++;
      last = ptr;
      ptr++;
    }

    printf ("Caso #%d:\n", t++);

    if (cnt)
      printf ("Qtd.Subsequencias: %d\nPos: %d\n\n", cnt, last - n2 + 1);
    else
      puts ("Nao existe subsequencia\n");
  }

  return 0;
}

