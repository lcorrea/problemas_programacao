#include <iostream>
#include <cstdio>

using namespace std;

int main (void)
{
	char op;
	float v[12][12], result = 0;
	int cnt = 0;

	cin >> op;

	for (unsigned int a = 0; a < 12; a++)
	{
		for (unsigned int b = 0; b < 12; b++)
		{
			cin >> v[a][b];

			if (b > a)
			{
				result += v[a][b];
				cnt++;
			}
		
		}
	}
	
	if (op == 'M')
		result = (float) result/cnt;

	printf ("%.1f\n", result);

	return 0;
}

