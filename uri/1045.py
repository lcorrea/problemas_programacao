#!/usr/bin/env python3

a, b, c = sorted(map(float, input().split()))[::-1]

a2, b2, c2 = a * a, b * b , c * c

if (a >= b + c):
    print("NAO FORMA TRIANGULO")
    exit(0)

if (a2 == b2 + c2):
    print("TRIANGULO RETANGULO")

if (a2 > b2 + c2):
    print("TRIANGULO OBTUSANGULO")

if (a2 < b2 + c2):
    print("TRIANGULO ACUTANGULO")

if (a == b and b == c):
    print("TRIANGULO EQUILATERO")
elif ((a == b and b != c) or (a == c and a != b) or (c == b and b != a)):
    print("TRIANGULO ISOSCELES")

