#include <stdio.h>

int main (void)
{
	int qt;
	unsigned int m, n;
	char p1[101], p2[101], op1[10], op2[10];

	scanf ("%d", &qt);

	while (qt--)
	{
		scanf ("%s %s %s %s%*c", p1, op1, p2, op2);
		scanf ("%u %u", &n, &m);
		if ((n+m) % 2)
		{
			if (op1[0] == 'I') puts (p1);
			else puts (p2);
		}
		else
		{
			if (op1[0] == 'P') puts (p1);
			else puts (p2);
		}
	}
	return 0;
}

