#!/usr/bin/python3

n = int(input())
names = []
acc = 0
wa = 0

for i in range(n):
    t, name = input().split()

    names.append(name)

    if (t == '+'):
        acc += 1
    else:
        wa += 1

names.sort()

for i in range(n):
    print(names[i])

print("Se comportaram: %d | Nao se comportaram: %d" % (acc, wa))

