#include <stdio.h>
#include <math.h>

int main (void)
{
	int l;
	const double sqrt3_4 = sqrt(3)/4.0*8.0/5.0;

	while (scanf ("%d", &l) != EOF)
	{
		double areat = pow(l, 2)*sqrt3_4;
		printf ("%.2lf\n", areat);
	}

	return 0;
}

