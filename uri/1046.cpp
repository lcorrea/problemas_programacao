/*
	problem 1046
	game time
	by lucas correa
	time 00:04:07
*/
#include <iostream>

using namespace std;

int main(void)
{	
	unsigned int a, b;

	cin >> a >> b;

	cout << "O JOGO DUROU ";

	if(b > a)
		cout << b - a;
	else
		cout << (b + 24) - a;
	
	cout << " HORA(S)" << endl;
	
	return 0;
}

