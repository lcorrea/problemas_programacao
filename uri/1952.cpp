#include <bits/stdc++.h>

using namespace std;

#define INF 1000000000

typedef pair <int, pair <int, int> > iii;

int bfs (iii u, iii v, int n, int m, int l)
{
    bool check[100][100][100] = {0};
    int dist[100][100][100] = {0};
    //const int diffy[24] = {-2, -2, -2, -2, -1, -1, -1, -1,  0,  0,  0,  0,  0, 0,  0,  0, 1,  1,  1, 1,  2, 2,  2, 2};
    //const int diffx[24] = {-1,  1,  0,  0, -2,  2,  0,  0, -2, -2, -1, -1,  1, 1,  2,  2, 0,  0, -2, 2, -1, 1,  0, 0};
    //const int diffk[24] = { 0,  0, -1,  1,  0,  0, -2,  2, -1,  1, -2,  2, -2, 2, -1,  1, -2, 2,  0, 0,  0, 0, -1, 1};
    queue <iii> q;

    check[u.first][u.second.first][u.second.second] = true;

    const int diff[3] = {0, 1, 2};

    q.push(u);

    while (!q.empty())
    {
        iii w = q.front(); q.pop();

        if (w == v) break;

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    if (k != j and j != i and k != i)
                        for (int x = 0; x < 2; x++)
                            for (int y = 0; y < 2; y++)
                                for (int z = 0; z < 2; z++)
                                {
                                    printf ("%d %d %d\n", diff[i], diff[j], diff[k]);
                                    int a = w.first + ((x&1) ? -diff[i] : diff[i]);
                                    int b = w.second.first + ((y&1) ? -diff[j] : diff[j]);
                                    int c = w.second.second + ((z&1) ? -diff[k] : diff[k]);

                                    if (a >= 0 and a < n)
                                        if (b >= 0 and b < m)
                                            if (c >= 0 and c < l)
                                                if (!check[a][b][c])
                                                {
                                                    dist[a][b][c] = dist[w.first][w.second.first][w.second.second] + 1;
                                                    check[a][b][c] = true;
                                                    q.push({a, {b, c}});
                                                }
                                }
    }

    return dist[v.first][v.second.first][v.second.second];
}

void next(iii w, int n, int m, int l)
{
    const int diff[3] = {0, 1, 2};
    
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            for (int k = 0; k < 3; k++)
                if (k != j and j != i and k != i)
                {
                    printf ("%d %d %d\n", diff[i], diff[j], diff[k]);
                    /*for (int x = 0; x < 2; x++)
                        for (int y = 0; y < 2; y++)
                            for (int z = 0; z < 2; z++)
                            {
                                int a = w.first + ((x&1) ? -diff[i] : diff[i]);
                                int b = w.second.first + ((y&1) ? -diff[j] : diff[j]);
                                int c = w.second.second + ((z&1) ? -diff[k] : diff[k]);

                                if (a >= 0 and a < n)
                                    if (b >= 0 and b < m)
                                        if (c >= 0 and c < l)
                                                printf ("%d %d %d (%d %d %d)\n",
                                                        a, b, c,
                                                        x&1 ? -diff[i] : diff[i],
                                                        y&1 ? -diff[j] : diff[j],
                                                        z&1 ? -diff[k] : diff[k]);
                            }*/
                }

}

int main (void)
{
    int n, m, l;

    iii a, b;

    scanf ("%d %d %d", &n, &m, &l);
    scanf ("%d %d %d", &a.first, &a.second.first, &a.second.second);
    scanf ("%d %d %d", &b.first, &b.second.first, &b.second.second);

    a.first--; a.second.first--; a.second.second--;
    b.first--; b.second.first--; b.second.second--;

    next(a, n, m, l);
    //printf ("%d\n", bfs(a, b, n, m, l));

    return 0;
}

