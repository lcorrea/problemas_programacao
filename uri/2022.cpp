#include <bits/stdc++.h>

using namespace std;

class Gift
{
    public:
        string name;
        int pref;
        double value;

        Gift (string str, double v, int p)
        {
            name = str;
            pref = p;
            value = v;
        }
};

bool cmp (Gift *a, Gift *b)
{
    if (a->pref == b->pref)
    {
        if (a->value == b->value)
            return a->name > b->name;

        return a->value > b->value;
    }

    return a->pref < b->pref;
}

int main (void)
{
    char name[1000];
    int n;

    while (scanf ("%s %d%*c", name, &n) != EOF)
    {
        vector <Gift *> list;
        string str;
        double v;
        int p;

        for (int i = 0; i < n; i++)
        {
            getline (cin, str);
            cin >> v >> p;
            cin.ignore();

            list.push_back (new Gift (str, v, p));
        }

        sort (list.rbegin(), list.rend(), cmp);

        printf ("Lista de %s\n", name);
        for (int i = 0; i < n; i++)
            printf ("%s - R$%.02lf\n", list[i]->name.c_str(), list[i]->value);
        putchar ('\n');
    }

    return 0;
}

