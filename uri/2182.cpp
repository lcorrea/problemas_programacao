#include <bits/stdc++.h>

using namespace std;

typedef vector < vector < pair<int,int> > > Graph;

Graph graph;
vector<bool> seen;

void dfs(int u, int &house, int &holes, int &paths)
{
	seen[u] = true;
	house = max(house, u);

	for (auto v : graph[u])
	{
		if (!seen[v.first])
			dfs(v.first, house, holes, paths);

		holes += v.second;
		paths++;
	}
}

int main (void)
{
	int n, m;

	scanf ("%d %d", &n, &m);

	graph.resize(n);
	seen.assign(n, false);

	for (int i = 0; i < m; i++)
	{
		int x, y, b;

		scanf ("%d %d %d", &x, &y, &b);

		graph[x-1].push_back({y-1, b});
		graph[y-1].push_back({x-1, b});
	}


	int holes = 0, paths = 0, house = 0;

	dfs(0, house, holes, paths);

	holes >>= 1; paths >>= 1; house += 1;

	double average = paths ? (holes / (double)paths) : 0.0;

	int ans = house;

	for (int i = 1; i < n; i++)
	{
		if (!seen[i])
		{
			holes = 0, paths = 0, house = i;

			dfs(i, house, holes, paths);

			holes >>= 1; paths >>= 1; house += 1;

			double avg = paths ? (holes / (double)paths) : 0.0;

			if (avg < average)
			{
				ans = house;
				average = avg;
			}
            else if (avg == average)
                ans = max(ans, house);
		}
	}

	printf ("%d\n", ans);

	return 0;
}

