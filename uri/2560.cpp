#include <iostream>
#include <queue>

using namespace std;

int main (void)
{
    int b, n;

    while (scanf ("%d %d", &n, &b) != EOF)
    {
        long long unsigned int score = 0;
        unsigned int sum = 0;
        int x = 0, y = b - 1;
        int v[n];
        priority_queue < pair <int, int> > pqMin, pqMax;

        for (int i = 0; i < n; i++)
        {
            scanf ("%d", v + i);

            sum += v[i];
            pqMin.push ({-v[i], -i});
            pqMax.push ({v[i], -i});

            if (i == y) //final da bateria
            {
                while (!(-pqMin.top().second >= x))
                    pqMin.pop();

                while (!(-pqMax.top().second >= x))
                    pqMax.pop();

                score += sum + pqMin.top().first - pqMax.top().first;
                sum -= v[x];
                x++;
                y++;
            }
       }

        printf ("%llu\n", score);
    }

    return 0;
}

