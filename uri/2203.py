from math import hypot
from sys import stdin, stdout

for line in stdin:

    xf, yf, xi, yi, v1, r1, r2 = map(int, line.split())

    if (r1 + r2 >= hypot(xf - xi, yf - yi) + 1.5 * v1):
        print ("Y")
    else:
        print ("N")

