#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <set>

using namespace std;

int main (void)
{
	set <int> especies;
	int florest[202][202];
	int n;

	scanf ("%d", &n);

	for (int i=1; i <= n; i++)
		for (int j=1; j <= n; j++)
			scanf ("%d", &florest[i][j]);

	for (int i = n*2; i >= 1; i--)
	{
		int x, y;
		scanf ("%d %d", &y, &x);
		especies.insert (florest[y][x]);
	}

	printf ("%d\n", especies.size());

	return 0;
}

