#include <bits/stdc++.h>

using namespace std;

double calc (int r1, int r2, int h)
{
	double teta = asin ((h - r1 - r2)/((double)r1 + r2));
	double l = cos(teta)*((double) r1 + r2) + r1 + r2;

	return l;
}

int main()
{ 
	int h, l, r1, r2;

	while (scanf ("%d %d %d %d", &l, &h, &r1, &r2) != EOF)
	{
		if (!(h and l and r1 and r2)) break;

		double min_l = min (h, l);
		double max_r = max (r1, r2);

		if (min_l >= (max_r * 2.0))
		{
			double ha = max_r*2.0;
			double la = calc (r1, r2, ha);

			if ((la <= h and ha <= l) or (la <= l and ha <= h))
			{ puts ("S"); continue; }

			ha = max(h, l);
			la = calc (r1, r2, ha);

			if (la <= min (h, l)) { puts ("S"); }
			else puts ("N");
		}
		else
			puts ("N");
	}

	return 0;
}

