/*
	problem 1009
	salary with bonus
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	double salary, sold;

	while(scanf("%*[^\n]%*c") != EOF)
	{
		scanf("%lf%*c%lf%*c", &salary, &sold);
		printf("TOTAL = R$ %.2f\n", salary + 0.15*sold);
	}

	return 0;
}
