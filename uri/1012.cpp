/*
	problem 1012
	area
	by lucas correa
*/
#include <iostream>
#include <stdio.h>

using namespace std;

#define PI 3.14159

int main(void)
{
	float a, b, c;
	
	cin >> a >> b >> c;

	printf("TRIANGULO: %.3f\n", a*c/2);
	printf("CIRCULO: %.3f\n", PI*c*c);
	printf("TRAPEZIO: %.3f\n", (b+a)*c/2);
	printf("QUADRADO: %.3f\n", b*b);
	printf("RETANGULO: %.3f\n", a*b);

	return 0;
}

