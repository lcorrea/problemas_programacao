#include <iostream>
#include <cstdio>
#include <string>
#include <algorithm>

using namespace std;

int main (void)
{
	int c;
	unsigned int u;
	char cents[3];
	char unidades[15];

	while (scanf ("%u%*c%d%*c", &u, &c) != EOF)
	{
		int sz = sprintf (unidades, "%u", u);
		string value;

		sprintf (cents, "%02d", c);
		reverse (unidades, unidades+sz);

		for (int i = 0, comma = 1; i < sz; i++, comma++)
		{
			if (comma % 4 == 0)
				value = value + ',', comma = 1;
			value = value + unidades[i];
		}

		reverse (value.begin(), value.end());

		value = value + '.' + cents;

		printf ("$%s\n", value.c_str());
	}

	return 0;
}

