#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> point;

bool check (point a, point b, point c)
{
	printf ("(%d, %d), (%d, %d), (%d, %d)\n", a.first, a.second,
			b.first, b.second,
			c.first, c.second);

	double dab = hypot (a.first - b.first, a.second - b.second);
	double dac = hypot (a.first - c.first, a.second - c.second);
	double dbc = hypot (b.first - c.first, b.second - c.second);

	if (dab == dac or dac == dbc or dab == dbc)
		return true;

	return false;
}

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		point points[n];
		int cnt = 0;

		for (int i = 0; i < n; i++)
			scanf ("%d %d", &points[i].first,
					&points[i].second);

		for (int i = 0; i <= n - 3; i++)
			for (int j = i + 1; j <= n - 2; j++)
				for (int k = j + 1; k <= n - 1; k++)
					cnt += check (points[i],
							points[j],
							points[k]);

		printf ("%d\n", cnt);

	}

	return 0;
}

