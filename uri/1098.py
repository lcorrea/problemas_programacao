#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    # --- #
    k, i, j = 0, 0, 1
    
    while (i < 2):
        if (k % 10):
            for l in range(3):
                stdout.write("I=%d.%d J=%d.%d\n" % (i,k,j+l,k))
        else:
            if (k != 0):
                i, j, k = i+1, j+1, 0
            
            for l in range(3):
                stdout.write("I=%d J=%d\n" % (i, j+l))
        k += 2


if __name__ == "__main__":
    main()

