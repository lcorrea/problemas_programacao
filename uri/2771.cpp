#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    while (EOF != scanf("%d %d", &n, &k)) {
        
        vector<int> notes(n);
        vector<double> m;

        for (int i = 0; i < n; i++) {
            scanf("%d", &notes[i]);
        }

        for (int i = 0, cnt = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                for (int _k = j+1; _k < n; _k++) {
                    m.push_back((notes[i] + notes[j] + notes[_k])/3.0);
                }
            }
        }

        sort(m.rbegin(), m.rend());

        printf("%.1lf\n", m[k-1]);
    }
    

    return 0;
}

