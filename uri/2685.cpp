#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    const char horario[][25] = {
        "Bom Dia!!",
        "Boa Tarde!!",
        "Boa Noite!!",
        "De Madrugada!!"};

    while (scanf("%d", &n) != EOF) {

        int qd = (n / 90) % 4;

        if (n <= 360)
            printf("%s\n",horario[qd]);
    }

    return 0;
}

