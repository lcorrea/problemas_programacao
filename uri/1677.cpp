#include <cstdio>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vvi digraph, dag, SCC;
vi visited(5001), sc(5001), dfs_low(5001), dfs_num(5001), S, ts;

int tick, numSCC;

void tarjanSCC (int u)
{
	visited[u] = 1;
	S.push_back(u);

	dfs_low[u] = dfs_num[u] = tick++;

	for (int i = 0; i < digraph[u].size(); i++)
	{
		if (dfs_num[digraph[u][i]] == 0) tarjanSCC(digraph[u][i]);

		if (visited[digraph[u][i]])
			dfs_low[u] = min (dfs_low[u], dfs_low[digraph[u][i]]);
	}

	if (dfs_num[u] == dfs_low[u])
	{
		int v;
		for (;;)
		{
			v = S.back(); S.pop_back();
			sc[v] = numSCC;
			SCC[numSCC].push_back (v);
			visited[v] = 0;
			if (v==u) break;
		}
		numSCC++;
	}
}

void invTS (int u)
{
	visited[u] = 1;

	for (int i = 0; i < dag[u].size(); i++)
		if (visited[dag[u][i]]==0)
			invTS (dag[u][i]);
	ts.push_back (u);
}


int main (void)
{
	int n, m, u, v;

	while (scanf ("%d", &n), n)
	{
		set <int> sol;

		scanf ("%d", &m);

		fill (visited.begin(), visited.end(), 0);
		fill (dfs_num.begin(), dfs_num.end(), 0);
		fill (dfs_low.begin(), dfs_low.end(), 0);
		fill (sc.begin(), sc.end(), -1);
		S.clear();
		ts.clear();
		digraph.clear(); digraph.resize(n+1);
		SCC.clear(); SCC.resize(n+1);
		tick = numSCC = 1;

		while (m--)
		{
			scanf ("%d %d", &u, &v);
			digraph[u].push_back (v);
		}

		/**************************
		 *  - tarjan's algorithm -
		 **************************/
		for (v = 1; v <= n; v++)
			if (dfs_num[v] == 0)
				tarjanSCC(v);
		
		/*for (v = 1; v <= n; v++)
			printf ("%d ", sc[v]);
		putchar ('\n');*/

		/******************************
		 *	 - mount dag -
		 ******************************/
		vi out(numSCC, 0);
		dag.clear(); dag.resize(numSCC+1);
		for (v = 1; v <= n; v++)
		{
			for (u = 0; u < digraph[v].size(); u++)
			{
				if (sc[v] != sc[digraph[v][u]])
				{
//					printf ("sc[%d] = %d\n", v , sc[v]);
					dag[sc[v]].push_back (sc[digraph[v][u]]);
					out[sc[v]]++;
				}
			}
		}
		for (v = 1; v < numSCC; v++)
			if (out[v]==0)
				for (u = 0; u < SCC[v].size(); u++)
					sol.insert (SCC[v][u]);

		for (set <int>::const_iterator it = sol.begin(); it != sol.end();)
		{
			printf ("%d", *it);
			if (++it != sol.end()) putchar (' ');
			else putchar ('\n');
		}

		/*******************************
		 *  	- sort the dag -
		 *******************************/
/*		fill (visited.begin(), visited.end(), 0);
		for (v = 1; v < numSCC; v++)
			if (visited[v]==0)
				invTS(v);

		puts ("topological sort:");
		for (v = ts.size() - 1; v >= 0; v++)
			printf ("%d ", ts[v]);
*/
	}

	return 0;
}

