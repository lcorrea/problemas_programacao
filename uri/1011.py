#!/usr/bin/env python3

r = float(input())

print("VOLUME = %.03lf" % (4/3.0 * 3.14159 * r ** 3))
