#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		int v[n];

		while (scanf ("%d", &v[0]), v[0])
		{
			bool valid = true;
			stack <int> estacao;
			queue <int> trem;

			for (int i=1; i < n; i++) trem.push(i), scanf ("%d", &v[i]);
			trem.push(n);

			for (int k = 0; k < n and valid; k++)
			{
				if (!trem.empty())
				{
					if (!estacao.empty() and estacao.top() == v[k])
					{
						estacao.pop();
						continue;
					}

					while (!trem.empty() and v[k] != trem.front())
					{
							estacao.push(trem.front());
							trem.pop();
					}
							
					if (!trem.empty()) trem.pop();
					else valid = false;
				}
				else
				{
					while (!estacao.empty() and k < n and valid)
						valid = (estacao.top() == v[k++]), estacao.pop();
				}
			}

			puts (valid ? "Yes" : "No");
		}

		putchar ('\n');
	}

	return 0;
}

