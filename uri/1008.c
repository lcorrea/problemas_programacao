/*
	problem 1008
	salary
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	int n, h;
	float salary;

	while(scanf("%d%*c", &n) != EOF)
	{
		scanf("%d%*c%f%*c", &h, &salary);
		printf("NUMBER = %d\nSALARY = U$ %.2f\n", n, h*salary);
	}
	return 0;
}
