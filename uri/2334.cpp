#include <cstring>
#include <cstdio>

using namespace std;

int main (void)
{
    char str[20];
    int len;

    for (;;)
    {
        scanf ("%s", str);

        len = strlen(str);

        if (str[0] == '-')
            break;
        else if (len == 1 and (str[0] == '0' or str[0] == '1'))
            puts ("0");
        else
        {
            int i = len - 1;

            while (str[i] == '0')
                str[i--] = '9';

            str[i] -= 1;

            if(str[i] == '0' and i == 0)
                printf ("%s\n", str + 1);
            else
                printf ("%s\n", str);
        }
    }

    return 0;
}

