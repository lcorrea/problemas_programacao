#include <bits/stdc++.h>

using namespace std;

int lab[10][10];

void dfs (int i, int j)
{
    const int di[4] = {0, 0, -1, 1};
    const int dj[4] = {-1, 1, 0, 0};

    lab[i][j] = 1;

    for (int k = 0; k < 4; k++)
    {
        int newi = i + di[k];
        int newj = j + dj[k];
        
        if (newi >= 0 and newi < 5)
            if (newj >= 0 and newj < 5)
                if (lab[newi][newj] == 0)
                    dfs (newi, newj);
    }
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
                scanf ("%d", &lab[i][j]);

        dfs (0, 0);

        puts (lab[4][4] == 0? "ROBBERS" : "COPS");
    }

    return 0;
}

