#include <bits/stdc++.h>

#define MAX 1000000000

using namespace std;

typedef struct {
    int min;
    int max;
}Node;

class ST
{
    private:
        vector <Node> st, lazy;
        vector <int> v;
        int n;

        void build(int p, int l, int r)
        {
            if (l == r)
            {
                st[p].max = st[p].min = v[l];
                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            build (left, l, mid);
            build (right, mid+1, r);

            Node vl = st[left];
            Node vr = st[right];

            st[p].min = min(vl.min, vr.min);
            st[p].max = max(vl.max, vr.max);
        }

        Node query(int p, int l, int r, int i, int j)
        {
            if (i > r or j < l)
            {
                Node rtr;

                rtr.min = MAX;
                rtr.max = 0;

                return rtr;
            }

            if (i <= l and j >= r)
                return st[p];

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            Node vl = query(left, l, mid, i, j);
            Node vr = query(right, mid+1, r, i, j);

            Node rtr;
            rtr.max = max(vl.max, vr.max);
            rtr.min = min(vl.min, vr.min);

            return rtr;
        }

        void update(int p, int l, int r, int i, int j, int val)
        {
            if (i > r or j < l) return;

            if (i <= l and j >= r)
            {
                v[l] = val;
                st[p].min = val;
                st[p].max = val;
                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid+1, r, i, j, val);

            Node vl = st[left];
            Node vr = st[right];

            st[p].min = min(vl.min, vr.min);
            st[p].max = max(vl.max, vr.max);
        }

    public:

        void show()
        {
            for (int i = 0; i < st.size(); i++)
                printf ("[%d, %d] ", st[i].min, st[i].max);
            putchar ('\n');
        }

        ST(int _n, vector <int> &_v)
        {
            n = _n;
            v = _v;
            
            st.resize(n << 2);
            //lazy.assign(n << 2, x);

            build (1, 0, n-1);
        }

        Node query (int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }

        void update (int i, int val)
        {
            update(1, 0, n-1, i, i, val);
        }
};

int main (void)
{

    int n;

    while (scanf ("%d", &n) != EOF)
    {
        int q;
        vector <int> v(n);

        for (int i = 0; i < n; i++)
            scanf ("%d", &v[i]);

        ST sTree(n, v);

        scanf ("%d", &q);

        while (q--)
        {
            int cmd, a, b;

            scanf ("%d %d %d", &cmd, &a, &b);

            if (cmd == 1)
                sTree.update(a-1, b);
            else
            {
                Node x = sTree.query(a-1, b-1);

                printf ("%d\n", x.max - x.min);
            }
        }
    }

    return 0;
}

