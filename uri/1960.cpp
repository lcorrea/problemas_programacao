#include <cstdio>
#include <string>
#include <map>

using namespace std;

int main (void)
{
	int a;
	map<int, string> a2r;
	map<int, string>::reverse_iterator r;

	a2r[1000] = "M"; a2r[900] = "CM"; a2r[500] = "D"; a2r[400] = "CD";
	a2r[100] = "C"; a2r[90] = "XC"; a2r[50] = "L"; a2r[40] = "XL";
	a2r[10] = "X"; a2r[9] = "IX"; a2r[5] = "V"; a2r[4] = "IV"; a2r[1] = "I";

	scanf ("%d", &a);

	for (r = a2r.rbegin(); r != a2r.rend(); r++)
	{
		while (a >= r->first)
		{
			printf ("%s", r->second.c_str());
			a -= r->first;
		}
	}
	putchar ('\n');
	return 0;
}

