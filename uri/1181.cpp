#include <iostream>
#include <cstdio>

using namespace std;

int main (void)
{
	char op;
	float v[12][12], result = 0;
	unsigned int line;

	cin >> line;
	cin >> op;

	for (unsigned int a = 0; a < 12; a++)
	{
		for (unsigned int b = 0; b < 12; b++)
		{
			cin >> v[a][b];

			if (a == line)
				result += v[a][b];
		
		}
	}
	
	if (op == 'M')
		result = result/12.0;

	printf ("%.1f\n", result);

	return 0;
}

