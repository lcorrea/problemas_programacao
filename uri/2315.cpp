#include <bits/stdc++.h>

using namespace std;

int main()
{
    int m[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int ad, am, bd, bm;

    scanf ("%d %d", &ad, &am);
    scanf ("%d %d", &bd, &bm);

    int d = 0, d2 = 0, f = 1;

    if (bm < am)
        d = 365, swap(bm, am), swap(ad, bd), f = -1;

    while (am < bm)
        d2 += m[am-1], am++;
    
    printf ("%d\n", (d2 + bd - ad)*f + d);
    
    return 0;
}
