#include <cstdio>

using namespace std;

int main(void)
{
	float score = 0, tmp = -1;
	unsigned char a = 0;

	for(; a < 2; a++)
	{
		while(1)
		{
			scanf("%f%*c", &tmp);

			if(tmp < 0 || tmp > 10)
				puts("nota invalida");
			else
				break;
		}

		score += tmp;
	}

	printf("media = %.2f\n", score/2.0);

	return 0;
}

