#include <bits/stdc++.h>

using namespace std;

int toys[100], weight[100], w, t, n, p;
int mem[1000][1000];

int knapsack (int kg, int i)
{
    if (mem[kg][i] >= 0)
        return mem[kg][i];

    if (i == n)
        return mem[kg][i] = 0;

    int nao_leva = knapsack (kg, i + 1);

    if (weight[i] <= kg)
    {
        int leva = toys[i] + knapsack (kg - weight[i], i + 1);

        return mem[kg][i] = max (leva, nao_leva);
    }

    return mem[kg][i] = nao_leva;
}

int main (void)
{
    int Case;

    scanf ("%d", &Case);

    while (Case--)
    {
        scanf ("%d", &n);

        for (int i = 0; i < n; i++)
            scanf ("%d%d", toys + i, weight + i);

        memset (mem, -1, sizeof (mem));

        int ans = knapsack (50, 0);

        int w = 50;
        int item = 0;
        int bag = 0;
        int total_w = 0;
        int p = n;

        while (mem[w][item] > 0)
        {
            if (mem[w][item + 1] != mem[w][item])
            {
                w -= weight[item];
                bag += toys[item];
                total_w += weight[item];
                p--;
            }

            item++;
        }

        printf ("%d brinquedos\n", ans);
        printf ("Peso: %d kg\n", total_w);
        printf ("sobra(m) %d pacote(s)\n", p);

       // if (Case)
            putchar ('\n');
    }

    return 0;
}

