#!/usr/bin/python3

from sys import stdin, stdout

lines = stdin.readlines()
total = len(lines)
line = 0

while(line < total):

    n = int(lines[line])

    line += 1
    students = []

    for i in range(n):
        student = lines[line].split()
        students.append(student)
        line += 1

    students.sort(key=lambda x: (int(x[2]), x[1], x[0]))

    for i in range(n):
        print(students[i][0])

