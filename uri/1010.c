/*
	problem 1010
	Simple Calculate
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	unsigned int i = 0;
	unsigned int unit;
	float prices, sum = 0;

	for(; i < 2; i++)
	{
		scanf("%*u %u %f%*c", &unit, &prices);
		sum += unit*prices;
	}

	printf("VALOR A PAGAR: R$ %.2f\n", sum);

	return 0;
}
