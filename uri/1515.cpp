#include <bits/stdc++.h>

using namespace std;

typedef struct {
    char planet[100];
    int sent_time;
}msg_t;

bool cmp (msg_t a, msg_t b)
{
    return a.sent_time < b.sent_time;
}

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {
        msg_t msg[100];

        int year, time;
        
        for (int i = 0; i < n; i++)
            scanf ("%s %d %d", msg[i].planet,
                    &year, &time),
                  msg[i].sent_time = year - time;

        sort (msg, msg + n, cmp);

        printf ("%s\n", msg[0].planet);
    }

    return 0;
}

