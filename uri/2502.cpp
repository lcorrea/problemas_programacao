#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int c, n;

    while (scanf ("%d %d%*c", &c, &n) != EOF)
    {
        char tb[256], p1[30], p2[30];

        for (int i = 0; i < 256; i++)
            tb[i] = i;

        for (int i = 0; i < c; i++)
            p1[i] = getchar();
        p1[c] = 0; getchar();

        for (int i = 0; i < c; i++)
            p2[i] = getchar();
        p2[c] = 0; getchar();

        for (int i = 0; p1[i]; i++)
        {
            if (toupper(p1[i]) != p1[i])
            {
                tb[p1[i]] = p2[i];
                tb[p2[i]] = p1[i];
            }
            else
            {
                tb[toupper(p1[i])] = toupper(p2[i]);
                tb[toupper(p2[i])] = toupper(p1[i]);
                tb[tolower(p1[i])] = tolower(p2[i]);
                tb[tolower(p2[i])] = tolower(p1[i]);
            }
        }

        while (n--)
        {
            string line;

            getline (cin, line);

            for (int i = 0; line[i]; i++)
                putchar (tb[line[i]]);
            putchar ('\n');
        }

        putchar ('\n');
    }

    return 0;
}

