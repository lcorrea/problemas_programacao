/*
	problem 1534
	array 123
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	unsigned int k, j;

	unsigned int col, lin;

	while(scanf("%u%*c", &k) != EOF)
	{
		j = --k;

		for(lin = 0; lin <= k; lin++, j--)
		{
			for(col = 0; col <= k; col++)
			{
				if(col == j)
					putchar('2');
				else if(col == lin)
					putchar('1');
				else
					putchar('3');
			}
			putchar('\n');
		}
	}

	return 0;
}
