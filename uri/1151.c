/*
	problem 1151
	easy fibonacci
	by lucas correa
	time: 31:14
*/

#include <stdio.h>

int main(void)
{
	
	unsigned int n = 0, temp = 0, fib[2] = { 0, 1};
	
	scanf("%u%*c", &n);
	if ( n == 1)
	{
		putchar('0');	
	}
	else if( n > 1)
	{

		printf("0 1");
		while(n-- - 2)
		{
			printf(" %u", temp = fib[0] + fib[1]);
			fib[0] = fib[1];
			fib[1] = temp;
		}
	}
	putchar('\n');

	return 0;
}

