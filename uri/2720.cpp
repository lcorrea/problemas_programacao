#include <bits/stdc++.h>

using namespace std;

struct p {
    int id, h, w, l;
};

bool cmp(struct p a, struct p b)
{
    int va = a.h * a.w * a.l;
    int vb = b.h * b.w * b.l;

    if (va == vb)
        return a.id < b.id;

    return va > vb;
}

bool cmp2(struct p a, struct p b)
{
    return cmp({a.id, 0, 0, 0}, {b.id, 0, 0, 0});
}

int main (void)
{
    int t;

    scanf("%d", &t);

    while (t--) {

        int n, k;

        scanf("%d %d", &n, &k);

        struct p list[n];


        for (int i = 0; i < n; i++) {

            scanf ("%d %d %d %d", 
                    &list[i].id,
                    &list[i].h,
                    &list[i].w,
                    &list[i].l);
        }

        sort(list, list + n, cmp);
        sort(list, list + k, cmp2);

        for (int i = 0; i < k; i++)
            printf("%d%c", list[i].id, i == k-1 ? '\n' : ' ');
    }

    return 0;
}

