#include <bits/stdc++.h>

using namespace std;

typedef struct {
    int numero;
    char naipe;
}Carta;

class Jogador {

    protected:
        int id;
        int mao[4][13];
        int cartas = 0;

    public:

        Jogador(int id, vector <Carta> &baralho, int pos, int n)
        {
            this->id = id+1;
            cartas = 0;
            memset(mao, 0, sizeof(mao));

            for (int i = pos, sz = pos + n; i < sz; i++)
                compraCarta(baralho[i]);
        }

        void compraCarta (Carta c)
        {
//            printf ("jogador %d comprou %d %c\n",
//                    id, c.numero, c.naipe);
            mao[naipe(c)][c.numero-1]++;
            cartas++;
        }

        void printMao()
        {
            printf ("mao jogador %d\n", id);
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 13; j++)
                    printf ("%d ", mao[i][j]);
                putchar ('\n');
            }
        }

        int naipe(Carta carta)
        {
            int n = 0, c = carta.naipe;

            switch (c)
            {
                case ('C'): n++;
                case ('D'): n++;
                case ('H'): n++;
                default:;
            }

            return n;
        }

        int naipe(int n)
        {
            if (n == 1)
                return 'H';
            else if (n == 2)
                return 'D';
            else if (n == 3)
                return 'C';
            else
                return 'S';
        }

        Carta descarta(int i, int j)
        {
            Carta c;

            //printf ("%d -> ", mao[i][j]);
            mao[i][j]--; 
            //printf ("%d\n", mao[i][j]);
            cartas--;

//            printf ("jogador %d tem %d cartas\n",
//                    id, cartas);

            c.naipe = naipe(i);
            c.numero = j + 1;

            return c;
        }

        Carta joga(Carta topDescarte)
        {
            int n = naipe(topDescarte);
            int m = topDescarte.numero - 1;

//            printf ("jogador %d vai jogar (%d %c=%d)\n", id,
//                    m+1, naipe(n), n);

            for (int i = 12; i > m; i--)
                if (mao[n][i]){
//                    printf ("jogador %d descartou %d %c - %d %c\n",
//                            id, i+1, naipe(n),
//                            m+1, naipe(n));
                    return descarta(n, i);
                }

            for (int i = 0; i < 4; i++)
                if (mao[i][m]){
//                    printf ("jogador %d descartou %d %c - %d %c\n",
//                            id, m+1, naipe(i),
//                            m+1, naipe(n));
                    return descarta(i, m);
                }

            for (int i = m; i >= 0; i--)
                if (mao[n][i]){
//                    printf ("jogador %d descartou %d %c - %d %c\n",
//                            id, i+1, naipe(n),
//                            m+1, naipe(n));
                    return descarta(n, i);
                }

//            printf ("jogador %d nao tem esta carta %d %c\n",
//                    id, topDescarte.numero, topDescarte.naipe);

            return (Carta) { .numero = -1, .naipe = -1};
        }

        int numeroDeCartas()
        {
            return cartas;
        }
};

int main (void)
{
    int p, m, n;
    int C = 1;

    while (scanf ("%d %d %d%*c", &p, &m, &n), p or m or n)
    {
//        printf ("\ncaso %d\n", C++);
//        if (C == 18) {
//            printf ("%d %d %d\n", p, m, n);
//            return 0;
//        }
        vector <Jogador*> jogadores(p);
        vector <Carta> baralho(n);

        for (int i = 0; i < n; i++)
        {
            Carta c;

            scanf ("%d %c%*c", &c.numero, &c.naipe);
            baralho[i] = c;
        }

        for (int i = 0; i < p; i++)
            jogadores[i] = new Jogador (i, baralho, i*m, m);

        Carta topDescarte = baralho[p*m];
        int dir = topDescarte.numero == 12 ? -1 : 1;
        int vencedor = 1;

        for (int i = p*m + 1, j = 0; ; j += dir)
        {
//            printf ("\n(%d) topDescarte %d %c\n\n", i,
//                    topDescarte.numero, topDescarte.naipe);

            if (j >= p) j = (j % p);
            else if (j < 0) j = p + j;
            //else { puts ("egua"); return -1;}

            switch (topDescarte.numero) {
                case 7:
                    jogadores[j]->compraCarta(baralho[i++]);
                case 1:
                    jogadores[j]->compraCarta(baralho[i++]);
                    //topDescarte.numero *= -1;
                    //continue;
                case 11:
                    //     j += dir;
//                    printf ("jogador %d passou a vez\n", j + 1);
                    topDescarte.numero *= -1;
                    continue;
            }

            Carta x = topDescarte;

            if (x.numero < 0)
                x.numero *= -1;

            //jogadores[j]->printMao();

            Carta d = jogadores[j]->joga(x);

            if (d.numero == -1)
            {
                jogadores[j]->compraCarta(baralho[i++]);

                d = jogadores[j]->joga(x);
            }

            if (jogadores[j]->numeroDeCartas() == 0)
            {
                vencedor += j;
                break;
            }

            if (d.numero != -1)
            {
                topDescarte = d;

                if (d.numero == 12)
                {
                    dir = dir*-1;
//                    printf ("\ninverteu!! %d -> %d\n", j, j+dir);
                }
            }

//            scanf ("%*c");
        }

        printf ("%d\n", vencedor);
    }

    return 0;
}

