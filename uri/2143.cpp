#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  int p;

  while (scanf ("%d", &n), n)
  {
    for (int i = 0; i < n; i++)
    {
      scanf ("%d", &p);

      if (p % 2)
        printf ("%d\n", p*2 - 1);
      else
        printf ("%d\n", p*2 - 2);
    }
  }

  return 0;
}

