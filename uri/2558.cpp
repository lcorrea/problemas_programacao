#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[1001];

    while (scanf ("%s", str) != EOF)
    {
        int f[27] = {0}, d = 0, len = 0;

        for (int i = 0; str[i]; i++, len++)
            d += ++f[tolower(str[i]) - 'a'] == 1;

        int e = 0, o = 0;

        for (int i = 0; i < 27; i++)
            if (f[i])
                if (f[i] & 1)
                    o++;
                else
                    e++;

        if (d == 1 or ((d == 1) and o == 1) or (o == 0))
            puts ("0");
        else if (o > 1 and o == e)
            puts ("1");
        else
            printf ("%d\n", o - 1);
    }

    return 0;
}

