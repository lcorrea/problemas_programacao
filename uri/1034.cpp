#include <bits/stdc++.h>

using namespace std;

int change (int P, vector <int>& coins)
{
	vector <int> dpTable(P+1, P+100);

	//assumindo que o vetor coins esteja ordenado
	dpTable[0] = 0;
	for (int v = 1; v <= P; v++)
		for (int i = 0, sz = coins.size(); i < sz and coins[i] <= v; i++)
			dpTable[v] = min (dpTable[v], dpTable[v - coins[i]] + 1);

	return dpTable[P];
}

int main (void)
{
	int t;

	cin >> t;

	while (t--)
	{
		int n, m;

		cin >> n >> m;

		vector <int> blocks(n);
		for (int i = 0; i < n; i++)
			cin >> blocks[i];

		cout << change(m, blocks) << endl;
	}

	return 0;
}

