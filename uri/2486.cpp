#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    map <string, int> tb;

    tb["suco de laranja"] = 120;
    tb["morango fresco"] = 85;
    tb["mamao"] = 85;
    tb["goiaba vermelha"] = 70;
    tb["manga"] = 56;
    tb["laranja"] = 50;
    tb["brocolis"] = 34;

    while (scanf ("%d%*c", &t), t)
    {
        int total = 0;

        for (int i = 0; i < t; i++)
        {
            int qnt;
            string alimento;

            scanf ("%d%*c", &qnt);
            getline(cin, alimento);

            total += qnt * tb[alimento];
        }

        if (total > 130)
            printf ("Menos %d mg\n", total - 130);
        else if (total < 110)
            printf ("Mais %d mg\n", 110 - total);
        else
            printf ("%d mg\n", total);
    }

    return 0;
}

