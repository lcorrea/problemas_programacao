#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	
	for (;;)
	{
		char text[200][100], c;
		int i = 0, j = 0;
		while ((c = getchar()), c != '\n' and c != EOF)
			if (isalpha(c)) text[i][j++] = tolower(c);
			else text[i++][j] = '\0', j = 0;
		text[i++][j] = '\0';

		if (c==EOF) break;

		int ali = 0;

		c = *text[0];

		for (int k = 1; k < i; k++)
		{
			int cnt = 0;

			while (k < i and c == *text[k]) 
				k++, cnt++;

			if (cnt) ali++;

			if (k < i) c = *text[k];
		}
		printf ("%d\n", ali);
	}
	
	return 0;
}

