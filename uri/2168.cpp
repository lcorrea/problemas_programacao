#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  int blocks[102][102];

  scanf ("%d", &n);

  for (int i = 0; i < n + 1; i++)
    for (int j = 0; j < n + 1; j++)
      scanf ("%d", &blocks[i][j]);

  for (int i = 0; i < n; i++)
  {
    for (int j = 0, cnt; j < n; j++)
    {
      cnt = blocks[i][j];
      cnt += blocks[i][j+1];
      cnt += blocks[i+1][j];
      cnt += blocks[i+1][j+1];

      putchar (cnt < 2 ? 'U' : 'S');
    }

    putchar ('\n');
  }

  return 0;
}

