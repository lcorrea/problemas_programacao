#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  int spd[101];

  scanf ("%d", &n);

  for (int i = 0; i < n; i++)
    scanf ("%d", spd + i);

  for (int i = 1; i < n; i++)
    if (spd[i] < spd[i-1])
    {
      printf ("%d\n", i + 1);

      return 0;
    }

  puts ("0");

  return 0;
}

