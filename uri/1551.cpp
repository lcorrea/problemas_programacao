#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    cin >> n; cin.ignore();

    while (n--)
    {
        string line;
        set<char> letters;

        getline(cin, line);

        for (int i = 0, sz = line.length(); i < sz; i++)
            if (isalpha(line[i]))
                letters.insert(line[i]);

        if (letters.size() == 26) cout << "frase completa";
        else if (letters.size() >= 13) cout << "frase quase completa";
        else cout << "frase mal elaborada";

        cout << endl;
    }

	return 0;
}

