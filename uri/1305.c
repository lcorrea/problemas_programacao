#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define min(a, b) (a <= b ? a : b)

int main (void)
{
	//int intnum;
	char decnum[20], deccutoff[20], intnum[20];

	while (1)
	{
		char ch;
		int i = 0, down = 1;
		decnum[0] = intnum[0] = '0'; intnum[1] = decnum[1] = '\0';

		while ((ch = getchar()) != EOF && ch != '.' && ch != '\n') intnum[i++] = ch;
		intnum[ i ? i : 1 ] = '\0';

		if (ch == EOF) break;

		if (ch == '.')
		{
			i = 0;
			while ((ch = getchar()) != '\n') decnum[i++] = ch;

			for (; i < 8; i++)
				decnum[i] = '0';
			decnum[8] = '\0';
		}

		getchar(); getchar();

		i = 0;
		while ( (ch = getchar()) != '\n' ) deccutoff[i++] = ch;

		for (; i < 8; i++)
			deccutoff[i] = '0';
		deccutoff[8] = '\0';

		int numint; sscanf (intnum, "%d", &numint);

		for (i = 0; i < 8; i++)
		{
			if (decnum[i] > deccutoff[i])
			{
				down = 0;
				break;
			}

			if (decnum[i] < deccutoff[i]) break;
		}

		printf ("%d\n", down ? numint : numint + 1);

	}

	return 0;
}
