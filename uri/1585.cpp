#include <iostream>

using namespace std;

int main (void)
{
	int l, L, m;

	cin >> m;

	while (m--)
	{
		cin >> l >> L;

		cout << l*L/2 << " cm2" << endl;
	}

	return 0;
}

