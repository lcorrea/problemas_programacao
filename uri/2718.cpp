#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    unsigned long long int b;

    scanf("%d", &n);

    while (n--) {

        int cnt = 0, ans = 0;
        scanf("%llu", &b);

        for (auto i = 0ull; i <= 50; i++) {

            if (b & (1ull << i)) {

                cnt += 1;
                ans = max(cnt, ans);
            }
            else {

                cnt = 0;
            }
        }

        printf("%d\n", max(ans, cnt));
    }

    return 0;
}

