#include <stdio.h>

int main (void)
{

	int t, n, i, j;
	int jogadores[12];

	scanf ("%d", &t);

	for (i = 1; i <= t; i++)
	{
		scanf ("%d", &n);
		for (j = 0; j < n; j++)
			scanf ("%d", jogadores + j);

		printf ("Case %d: %d\n", i, jogadores[n/2]);
	}

	return 0;
}

