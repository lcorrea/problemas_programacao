#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {
        int cnt = 0;
        bool ok = true;

        ok = false;

        while (!ok)
        {
            ok = true;

            for (int i = 1, ai; i <= n; i++)
            {
                scanf ("%d", &ai);

                if (i != ai)
                    ok = false;
            }

            cnt++;
        }

        printf ("%d\n", cnt);
        
    }

    return 0;
}

