#include <bits/stdc++.h>

using namespace std;

char board[100][100];

typedef struct {
    int food;
    int maxFood;
    int x, y;
    int mov;
}Player;

void run (Player *pac, int n)
{
    for (;pac->x >= 0 and pac->x < n; pac->x += pac->mov)
        switch (board[pac->y][pac->x])
        {
            case 'A':
                pac->maxFood = max(pac->food, pac->maxFood);
                pac->food = 0;
                break;
            case 'o':
                pac->food++;
                break;
        }
    
    if (pac->x < 0)
        pac->x = 0;
    else
        pac->x = n - 1;

    pac->y++;
    pac->mov *= -1;
}

int main (void)
{
    int n;
    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%s", board[i]);

    Player pacMan = { .food = 0, .maxFood = 0, .x = 0, .y = 0, .mov = 1};
    
    for (int i = 0; i < n; i++)
        run(&pacMan, n);

    printf ("%d\n", max(pacMan.maxFood, pacMan.food));

    return 0;
}

