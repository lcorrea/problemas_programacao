#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int cards[55];

	for (int i = 0, j = 1; i < 26; i++, j+=2)
		cards[i] = j;

	while (scanf ("%d", &n), n)
	{
		int desc = 2;
		int t = n, l = 1, i;
		deque <int> par;

		for (int i = 2; i <= n; i += 2)
			par.push_back(i);

		if (n%2) par.push_back(2), par.pop_front();

		if (n == 1)
		{
			puts ("Discarded cards:");
			puts ("Remaining card: 1");
			continue;
		}

		printf ("Discarded cards: 1");

		for (int i = 1; cards[i] <= n; i++)
			printf (", %d", cards[i]);
		
		while (par.size() > 1)
		{
			printf (", %d", par.front()); par.pop_front();
			par.push_back(par.front()); par.pop_front();
		}

		printf ("\nRemaining card: %d\n", par.front());
	}

	return 0;
}

