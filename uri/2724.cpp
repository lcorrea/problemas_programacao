#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf("%d", &t);

    while (t--) {

        int n, c;

        scanf("%d", &n);

        char patterns[n][52];

        for (auto p : patterns)
            scanf("%s", p);

        scanf("%d", &c);

        while (c--) {

            int ok = 1;
            char formula[52];

            scanf("%s", formula);

            for (int i = 0; i < n and ok; i++) {

                char *p = patterns[i];
                char *chk = strstr(formula, p);

                if (chk != NULL) {

                    char nextch = *(chk + strlen(p));

                    if (!nextch or (!isdigit(nextch) and nextch >= 'A' and nextch <= 'Z') ) {
                        ok = 0;
                    }
                }
            }

            puts( ok ? "Prossiga" : "Abortar" );
        }

        if (t)
            putchar('\n');
    }

    return 0;
}

