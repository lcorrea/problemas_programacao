/*
	problem 1471
	dangerous dive
	by lucas correa
*/
#include <stdio.h>
#include <stdlib.h>

int comp(const void *a, const void *b)
{
	return *(unsigned int *)a - *(unsigned int *)b;
}

int main(void)
{
	int N, R;
	unsigned int returned[10000];

	while(scanf("%d %d%*c", &N, &R) != EOF)
	{
		unsigned int i = 0, r = 0, aux = 0;
		
		for(r = 0; r < R; r++)
			scanf("%u%*c", returned + r);

		if(N == R)
			putchar('*');
		else 
		{
			qsort(returned, R, sizeof(unsigned int), comp);
			
			for(r = 0, aux = 0; r < R; r++)
			{
				while(++aux < returned[r])
					printf("%d ", aux);
			}

			while(N > aux)
				printf("%d ", ++aux);
		}

		putchar('\n');
	}

	return 0;
}
