#include <iostream>

using namespace std;

int main (void)
{
	unsigned int n, x, sum;

	cin >> n;

	while (n--)
	{
		cin >> x;

		sum = 0;

		for (unsigned int a = 1; a < x; a++)
		{
			if (!(x % a))
			{
				sum += a;
			}
		}

		cout << x;
		if (sum == x)
			cout << " eh perfeito";
		else
			cout << " nao eh perfeito";
		cout << endl;

	}

	return 0;
}

