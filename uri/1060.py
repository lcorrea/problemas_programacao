#!/usr/bin/env python3

cnt = 0

for i in range(6):
    n = float(input())
    cnt += 1 if n > 0 else 0

print(cnt, "valores positivos")
