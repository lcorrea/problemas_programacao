#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string input;
    int n;

    cin >> n;

    while (n--)
    {
        long long hash = 0;
        int l;

        cin >> l;

        for (int i = 0; i < l; i++)
        {
            cin >> input;
            for (int j = 0; input[j] != '\0'; j++)
                hash += (input[j] - 'A') + i + j;
        }

        cout << hash << endl;
    }

	return 0;
}

