#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;

int p[1000001] = {0};

int main (void)
{
    int n, l;
    priority_queue <ii, vector <ii> , greater<ii> > pq;
    queue<int> t;

    scanf ("%d %d", &n, &l);

    for (int i = 1; i <= n; i++)
        pq.push({0, i});

    for (int i = 0, a; i < l; i++)
    {
        scanf ("%d", &a);

        t.push(a);
    }

    while (!t.empty())
    {
        ii person = pq.top(); pq.pop();
        int time = t.front(); t.pop();

        p[person.second]++;
        person.first += time;

        pq.push(person);
    }

    for (int i = 1; i <= n; i++)
        printf ("%d %d\n", i, p[i]);

    return 0;
}

