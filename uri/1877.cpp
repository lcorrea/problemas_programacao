#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    scanf("%d %d", &n, &k);

    int v[n];

    for (int i = 0; i < n; i++) {
        scanf("%d", v + i);
    }

    int p[n], ini=n, fim=0;

    p[0] = 0;
    for (int i = 1; i <= n-2; i++) {
        if (v[i-1] < v[i] and v[i+1] < v[i]) {
            p[i] = p[i-1] + 1;
            ini = min(ini, i);
            fim = max(fim, i);
        } else {
            p[i] = p[i-1];
        }
    }
    p[n-1] = n > 1 ? p[n-2] : p[0];
    
    if (ini == n) {
        puts ("ugly");
    } else {

        bool beautiful = true;

        for (int i = 1; beautiful and i < ini; i++)
            beautiful = p[i] == 0 and (v[i] > v[i-1]);

        for (int i = fim+1; beautiful and i < n; ++i)
            beautiful = v[i] < v[i-1];

        beautiful = (p[fim] == k);

        puts (beautiful ? "beautiful" : "ugly");
    }

    return 0;
}

