#include <bits/stdc++.h>

using namespace std;

int calc (pair <int, int> exp, char op)
{
    switch (op)
    {
        case '+':
            return exp.first + exp.second;
        case '-':
            return exp.first - exp.second;
        case '*':
            return exp.first * exp.second;
    }

    return (1 << 31) - 1;
}

int main (void)
{
    int t;

    while (scanf ("%d%*c", &t) != EOF)
    {
        pair <int, int> exp[t];
        int resp[t];
        vector <string> ok;

        for (int i = 0; i < t; i++)
            scanf ("%d %d=%d%*c",
                    &exp[i].first,
                    &exp[i].second,
                    &resp[i]);

        for (int i = 0; i < t; i++)
        {
            int id;
            char op, nome[51];

            scanf ("%s %d %c%*c", nome, &id, &op); id--;

            if (op == 'I')
            {
                if (calc(exp[id], '+') == resp[id] or
                    calc(exp[id], '-') == resp[id] or
                    calc(exp[id], '*') == resp[id])
                    ok.push_back(nome);
                continue;
            }

            if (resp[id] != calc (exp[id], op))
                ok.push_back(nome);
        }
        
        if (ok.size() > 0 and ok.size() < t)
        {
            sort (ok.begin(), ok.end());

            printf ("%s", ok[0].c_str());
            for (int j = 1; j < (int) ok.size(); j++)
                printf (" %s", ok[j].c_str());
            putchar ('\n');
        }
        else if (ok.size() == t)
            puts ("None Shall Pass!");
        else
            puts ("You Shall All Pass!");
    }

    return 0;
}

