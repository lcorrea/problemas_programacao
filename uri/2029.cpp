#include <bits/stdc++.h>
#define PI 3.14

using namespace std;

int main (void)
{
	double v, d;

	while (scanf ("%lf %lf", &v, &d) != EOF)
	{
		double a = PI*d*d/4.0;
		printf ("ALTURA = %.02lf\nAREA = %.02lf\n", v/a, a);
	}

	return 0;
}

