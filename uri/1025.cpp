#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, q, t = 1;

    while (cin >> n >> q, n or q)
    {
        vector <int> marbles(n);
        vector <int>::const_iterator it;

        cout << "CASE# " << t << ":\n";

        for (int i = 0; i < n; i++)
            cin >> marbles[i];

        sort (marbles.begin(), marbles.end());

        for (int i = 0, a; i < q; i++)
        {
            cin >> a;

            if (binary_search(marbles.begin(), marbles.end(), a))
            {
                it = lower_bound (marbles.begin(), marbles.end(), a);

                cout << a << " found at " << (it - marbles.begin()) + 1 << endl;
            }
            else
                cout << a << " not found\n";
        }

        t++;
    }

    return 0;
}

