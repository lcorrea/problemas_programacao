#include <stdio.h>

int main(void)
{
	int d,h,m,s;
	int D, H, M, S;

	while(scanf("%*s %d\n%2d %*c %2d %*c %2d\n%*s %d\n%2d %*c %2d %*c %2d", &d, &h, &m, &s, &D, &H, &M, &S) != EOF)
	{
		S = M*60 + H*3600 + D*86400 + S;

		s = m*60 + h*3600 + d*86400 + s;

		S -= s;

		D = S/86400;
		H = (S/3600)%24;
		M = (S/60)%60;
		S = S%60;

		printf("%d dia(s)\n%d hora(s)\n%d minuto(s)\n%d segundo(s)\n", D, H, M, S);

	}
	return 0;
}
