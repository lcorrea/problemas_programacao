#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    int h[231] = {0};
    int n, d = 0;

    scanf ("%d", &t);

    while (t--)
    {
        memset (h, 0, sizeof(h));

        scanf ("%d", &n);

        for (int i = 0, a; i < n; i++)
        {
            scanf ("%d", &a);
            h[a]++;
        }

        for (int i = 20; i <= 230; i++)
        {
            for (int j = 1; j <= h[i]; j++)
            {
                printf ("%d", i);
                if (j < h[i] - 1 or n > 1) putchar (' ');
                n--;
            }
        }
        putchar ('\n');

    }

    return 0;
}

