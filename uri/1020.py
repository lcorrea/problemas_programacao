#!/usr/bin/env python3

dias = int(input())
anos, dias = dias / 365, dias % 365
meses, dias = dias / 30, dias % 30

print("%d ano(s)\n%d mes(es)\n%d dia(s)" % (anos, meses, dias))
