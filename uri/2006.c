#include <stdio.h>

int main (void)
{
	int t, i, c, n=0;

	scanf ("%d", &t);

	for (i=0; i < 5; i++)
		if (scanf ("%d", &c), c == t) n++;
	printf ("%d\n", n);
	return 0;
}
