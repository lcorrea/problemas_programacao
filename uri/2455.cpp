#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int p1, p2, c1, c2;

    scanf("%d %d %d %d", &p1, &c1, &p2, &c2);

    p1 *= c1;
    p2 *= c2;

    if (p1 == p2)
        puts ("0");
    else if (p1 > p2)
        puts ("-1");
    else
        puts ("1");

    return 0;
}

