#include <stdio.h>

int main (void)
{
  char c;
  int len = 0;

  while ((c = getchar()) != '\n' && c != EOF)
    len++;

  puts (len <= 140 ? "TWEET" : "MUTE");

  return 0;
}

