#include <bits/stdc++.h>

using namespace std;

int diffMostfreq(int mat[3][3])
{
    int arcans = -1, ans = -1;
    map<int, int> freq;

    for (int i = 0, diff; i < 3; i++) {
        
        diff = abs(mat[i][0] - mat[i][1]);

        if (++freq[diff] > ans) {

            arcans = diff;
            ans = freq[diff];
        }

        for (int j = 1; j < 3; j++) {

            diff = abs(mat[i][j-1] - mat[i][j]);

            if (++freq[diff] > ans) {

                arcans = diff;
                ans = freq[diff];
            }
        }
    }

    return arcans;
}

void solve(int mat[3][3], vector<int> &sol)
{
    int diffmostfreq = diffMostfreq(mat);

    for (int i = 0; i < 3; i++) {

        if ((diffmostfreq != abs(mat[i][0] - mat[i][1])) and (diffmostfreq != abs(mat[i][0] - mat[i][2])))
            sol.push_back(mat[i][0]);

        if ((diffmostfreq != abs(mat[i][1] - mat[i][0])) and (diffmostfreq != abs(mat[i][1] - mat[i][2])))
            sol.push_back(mat[i][1]);
        
        if ((diffmostfreq != abs(mat[i][2] - mat[i][0])) and (diffmostfreq != abs(mat[i][2] - mat[i][1])))
            sol.push_back(mat[i][2]);
    }
}

int main (void)
{
    int p;

    scanf("%d", &p);

    while (p--) {

        vector<int> sol;
        int mat[3][3];

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                scanf("%d", &mat[i][j]);

        solve(mat, sol);

        printf("Possiveis maletas: ");
        for(int i = 0, b = (int) sol.size(); i < b; i++)
            printf("%d%s", sol[i], (i+1) == b ? ";\n" : ", ");

    }

    return 0;
}

