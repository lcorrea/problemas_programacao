#include <bits/stdc++.h>

using namespace std;

class MyDate {
    int day;
    int month;
    int year;
    int daysOnMonth[12] = {
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public:

    MyDate (int d, int m, int y)
    {
        day = d;
        month = m;
        year = y;
        daysOnMonth[1] += MyDate::leapYear(year);
    }

    static bool leapYear(int y)
    {
        if (y % 400 == 0)
            return true;

        if (y % 4 == 0 and y % 100)
            return true;

        return false;
    }

    MyDate nextDay ()
    {
        int d = day, m = month, y = year;

        d = d < daysOnMonth[m-1] ? d + 1 : 1;

        if (d == 1)
        {
            m = m < 12 ? m + 1 : 1;

            if(m == 1)
                y += 1;
        }

        return MyDate(d, m, y);
    }

    int getDay()
    {
        return day;
    }

    int getMonth()
    {
        return month;
    }

    int getYear()
    {
        return year;
    }

    bool isEqual(MyDate b)
    {
        if (day == b.getDay())
            if (month == b.getMonth())
                if (year == b.getYear())
                    return true;
        return false;
    }

};

class Note {
    int power;
    MyDate date;

    public:
    Note(int d, int m, int y, int p):
        date(d, m, y)
    {
        power = p;
    }

    const MyDate getDate ()
    {
        return date;
    }

    const int getPower()
    {
        return power;
    }
};

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {
        vector <Note*> notes(n);

        for (int i = 0; i < n; i++)
        {
            int d, m, y, p;

            scanf ("%d %d %d %d", &d, &m, &y, &p);
            notes[i] = new Note(d, m, y, p);
        }

        int consecutive = 0;
        int totalPower = 0;

        for (int i = 0; i < n - 1; i++)
        {
            MyDate date = notes[i]->getDate();
            MyDate nextDate = notes[i+1]->getDate();
            int power = notes[i]->getPower();
            int nextPower = notes[i+1]->getPower();

            if (date.nextDay().isEqual(nextDate))
            {
                consecutive++;
                totalPower += nextPower - power;
            }
        }

        printf ("%d %d\n", consecutive, totalPower);
    }

    return 0;
}

