#include <stdio.h>
#include <stdlib.h>

int main (void)
{
	char str[120];
	char *pend;
	scanf ("%s", str);
	printf ("%+.4E\n", strtod(str, &pend));

	return 0;
}
