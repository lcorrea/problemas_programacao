#include <cstdio>

using namespace std;

int main(void)
{
	int x, y;
	unsigned int n;

	scanf("%u%*c", &n);

	while(n--)
	{
		scanf("%d %d%*c", &x, &y);

		if(y)
			printf("%.1f\n", (float) x/y);
		else
			printf("divisao impossivel\n");
	}

	return 0;
}

