#include <iostream>
#include <cmath>

using namespace std;

int main (void)
{
	int n, qt, s, w_guess, w_number, guess;

	cin >> n;

	while (n--)
	{
		cin >> qt >> s;

		w_number = 1;
		
		cin >> guess;

		w_guess = abs (s-guess);
		
		for (unsigned int i = 2; i <= qt; i++)
		{
			cin >> guess;

			guess = abs (s - guess);

			if (guess < w_guess)
			{
				w_number = i;
				w_guess = guess;
			}
		}

		cout << w_number << endl;
	}

	return 0;
}

