#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int h, c, m, block, cnt;

	while (scanf ("%d %d", &h, &c), h and c)
	{	
		cnt = 0;
		m = h;
		for (int i = 0; i < c; i++)
		{
			scanf ("%d", &block);

			if (m > block)
				cnt += m - block;

			m = block;
		}

		printf ("%d\n", cnt);
	}

	return 0;
}

