#!/usr/bin/env python3

n = int(input())

out = 0

for i in range(n):
    x = int(input())
    if (x < 10 or x > 20):
        out += 1

print(n - out, 'in')
print(out, 'out')
