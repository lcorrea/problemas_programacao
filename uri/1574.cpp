#include <iostream>
#include <vector>

using namespace std;

int main (void)
{
	int t, n;
	int p = 0, i = 0;
	string I;
	vector <int> pos;

	cin >> t;

	while (t--)
	{
		p = 0;
		i = 0;
		cin >> n;
		
		pos.assign (n+1, 0);

		for (int j = 1; j <= n; j++)
		{
			cin >> I;

			if (I[0] == 'S')
			{
				cin >> I >> i;

				p += pos[i];
				pos[j] = pos[i];
			}
			else if (I[0] == 'L')
			{
				pos[j] = -1;
				p += -1;
			}
			else
			{
				pos[j] = 1;
				p += 1;
			}
		}

		cout << p << endl;
	}

	return 0;
}

