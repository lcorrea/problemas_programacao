#include <bits/stdc++.h>

using namespace std;

int m;

bool cmp (int a, int b)
{
	int ma = a%m;
	int mb = b%m;

	if (ma == mb)
	{
		if (a%2 == 0)
		{
			if (b%2 == 0)
				return a < b;
			else
				return false;
		}
		else
		{
			if (b%2 == 0)
				return true;
			else
				return a > b;
		}
	}

	return ma <= mb;
}

int main (void)
{
	int n;

	while (scanf ("%d %d",&n, &m), n or m)
	{
		int v[n];
		for (int i = 0; i < n; i++)
			scanf ("%d", &v[i]);
		sort (v, v+n, cmp);

		printf ("%d %d\n", n, m);
		for (int i = 0; i < n; i++)
			printf ("%d\n", v[i]);
	}

	puts ("0 0");

	return 0;
}

