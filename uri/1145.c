/*
	problem 1145
	Logical Sequence 2
	by lucas correa
	time: 00:11:38
*/

#include <stdio.h>

int main(void)
{
	unsigned int x = 1, y = 1, i = 0, sum = 1;

	scanf("%u %u%*c", &x, &y);

	for(; sum <= y; )
	{
		for(i = 0; i < x - 1 && sum <= y - 1; i++)
			printf("%u ", sum++);
		printf("%u\n", sum++);
	}

	return 0;
}
