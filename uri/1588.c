/*
	problem 1588
	help the federation
	by Lucas Correa
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//#define DEBUG
typedef struct team {
	char name[21];
	unsigned int points;
	unsigned int wins;
	unsigned int goals;
	char position;
}team_t;

int classificator(const void *p1, const void *p2);

int main(void)
{
	unsigned short int T = 0, N = 0, M = 0;
	team_t *teams = NULL;
	
	//--- read team numbers ---
	scanf("%hd%*c", &T);

	while(T)
	{
		
		unsigned short int n = 0;
		char t = 0;
		
		scanf("%hd %hd%*c", &N, &M);

		teams = (team_t *) malloc(sizeof(team_t)*N);
		memset(teams, 0, sizeof(team_t)*N);
		n = N; // memorize

		//--- read team names ---
		do
		{
			N--;
			scanf("%s%*c", teams[N].name);
			teams[N].position = N;
		}while(N > 0);
	
		// get matches information ---
		while(M--)
		{
			char A[21], B[21], line[60];
			unsigned short int goalsA, goalsB;
			unsigned short int i = 0, a = 0, b = 0;

			//--- read match information ---
			scanf("%hd %20s %hd %20s%*c", &goalsA, A, &goalsB, B);

			//--- look for the teams and add information ---
			for(i = 0; i < n; i++)
			{
				if(!strcmp(teams[i].name, A))
				{
					a = i;
					teams[i].goals += goalsA;
					
				}else if(!strcmp(teams[i].name, B))
				{
					b = i;
					teams[i].goals += goalsB;
				}
			}
			
			if(goalsA > goalsB)
			{
				teams[a].wins += 1;
				teams[a].points += 3;
			}
			else if ( goalsA == goalsB)
			{
				teams[a].points += 1;
				teams[b].points += 1;
			}
			else
			{
				teams[b].wins += 1;
				teams[b].points += 3;
			}
			
		}


		//--- set classification ---
		//using qsort
		qsort(teams, n, sizeof(team_t), classificator);
		
		for(t = 0; t < n; t++)
			printf("%s\n", teams[t].name);
		
		free(teams);
		T--;
	}
	return 0;
}

int classificator(const void *p1, const void *p2)
{
	const team_t *teamA = p1;
	const team_t *teamB = p2;
	int class = 0;
	
	if(class = teamB->points - teamA->points);
	else if (class = teamB->wins - teamA->wins);
	else if (class = teamB->goals - teamA->goals);
	else
		class = teamB->position - teamA->position;
	
	return class;
}
