#include <stdio.h>

int main (void)
{
	int h, m;
	while (scanf ("%d:%d", &h, &m) != EOF)
	{

		m += h*60 + 60;
		int const met = 480;

		printf ("Atraso maximo: ");
		if (m > met)
			printf ("%d\n", m - met);
		else
			puts ("0");
	}

	return 0;
}

