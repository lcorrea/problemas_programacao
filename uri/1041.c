/*
	problem 1041
	coordinates of a point
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	
	float x, y;
	unsigned char quadrante = 48;

	while(scanf("%f %f%*c", &x, &y) != EOF)
	{
		if(x > 0)
		{
			quadrante++;
			if(y < 0)
				quadrante += 3;
		}
		else if(x < 0)
		{
			quadrante += 2;
			if(y < 0)
				quadrante++;
		}
		
		if(x == 0 && y)
			quadrante = 5;
		else if(y == 0 && x)
			quadrante = 6;
		
		switch(quadrante)
		{
			case('0'):
				puts("Origem");
			break;
			case(5):
				puts("Eixo Y");
			break;
			case(6):
				puts("Eixo X");
			break;
			default:
				printf("Q%c\n", quadrante);
		}
		quadrante = 48;		
	}
	return 0;
}

