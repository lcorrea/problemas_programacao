#include <bits/stdc++.h>

using namespace std;

class ST
{
    private:
        vector <unsigned int> lazy, st;
        vector <unsigned int> v;
        int n;

        void check (int p, int l, int r)
        {
            if (lazy[p] != 0)
            {
                if (l == r) st[p] = v[l] = lazy[p];
                else
                {
                    st[p] = (r - l + 1) & 1 ? lazy[p] : 0;
                    lazy[p << 1] = lazy[p];
                    lazy[(p << 1) | 1] = lazy[p];
                }

                lazy[p] = 0;
            }
        }

        void build(int p, int l, int r)
        {
            if (l == r) st[p] = v[l];
            else
            {
                int mid = (l+r) >> 1;
                int left = p << 1;
                int right = left | 1;

                build (left, l, mid);
                build (right, mid+1, r);

                st[p] = st[left] ^ st[right];
            }
        }
        
        unsigned int query (int p, int l, int r, int i, int j)
        {
            check(p, l, r);

            if (i > r or j < l) return 0;

            if (i <= l and j >= r) return st[p];

            int mid = (l + r) >> 1;
            int left = p << 1;
            int right = left | 1;

            unsigned int vl = query(left, l, mid, i, j);
            unsigned int vr = query(right, mid+1, r, i, j);

            return vl ^ vr;
        }

        void update(int p, int l, int r, int i, int j, int val)
        {
            check (p, l, r);

            if (i > r or j < l) return;

            if (i <= l and j >= r)
            {
                if (l == r) st[p] = v[l] = val;
                else
                {
                    st[p] = (r - l + 1) & 1 ? val : 0;
                    lazy[p << 1] = val;
                    lazy[(p << 1) | 1] = val;
                }

                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid+1, r, i, j, val);

            st[p] = st[left] ^ st[right];
        }

    public:
        ST(int _n, vector <unsigned int> &_v)
        {
            n = _n;
            v = _v;

            st.assign (n << 2, 0);
            lazy.assign(n << 2, 0);

            build(1, 0, n-1);
        }

        void update(int i, int j, int val)
        {
            update(1, 0, n-1, i, j, val);
        }

        unsigned int query(int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }
};

int main (void)
{
    int n, m;
    vector <unsigned int> v;

    scanf ("%d %d", &n, &m);

    v.assign(n, 0);
    for (int i = 0; i < n; i++)
        scanf ("%u", &v[i]);

    ST sTree(n, v);

    for (int i = 0; i < m; i++)
    {
        int x, y, q;

        scanf ("%d %d %d", &x, &y, &q);

        sTree.update(x-1, y-1, q);

        if (sTree.query(0, n-1))
            puts ("Possivel");
        else
            puts ("Impossivel");
    }

    return 0;
}

