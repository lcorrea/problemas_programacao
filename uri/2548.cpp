#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    while (EOF != scanf ("%d%d", &n, &m))
    {
        int v[n];

        for (int i = 0; i < n; i++)
            scanf ("%d", v+i);

        sort (v, v+n);

        int ans = 0;
        for (int i = n - 1; m; --m, --i)
            ans += v[i];

        printf ("%d\n", ans);
    }

    return 0;
}

