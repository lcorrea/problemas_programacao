#include <algorithm>
#include <utility>
#include <cstdio>

using namespace std;

typedef pair <int, char> event;

int main (void)
{
	int n;

	while (scanf ("%d%*c", &n), n)
	{
		int e = n/2;

		event events[n];

		for (int i = 0; i < n; i++)
		{
			int h, m, s;
			char reg;

			scanf ("%d:%d:%d %c%*c", &h, &m, &s, &reg);

			events[i] = event(h*3600 + m*60 + s, reg);

			if (reg == 'E') e--;
		}

		sort (events, events + n);

		int cnt = 0;
		int m_cnt = 0;

		for (int i = 0; i < n; i++)
		{
			char reg = events[i].second;

			if (reg == 'E') cnt++;
			else if (reg == '?' and e)
			{
				cnt ++;
				e--;
			}
			else
			{
				m_cnt = max (cnt, m_cnt);
				cnt--;
			}
		}

		printf ("%d\n", m_cnt);
	}

	return 0;
}

