#include <cstdio>
#include <cstring>

#define min(a, b) ((a < b) ? a : b)

using namespace std;

char str[2001];
int pal[2001][2001] = {0};
int n, cntP[2001] = {0};

int palindromo(int a, int b)
{
    if (a >= n or b < 0 or a >= b)
        return 1;

    if (pal[a][b] == -1)
        pal[a][b] = str[a] == str[b] and palindromo(a + 1, b - 1);

    return pal[a][b];
}

int main()
{
    int teste = 1;

    while (scanf ("%d", &n), n)
    {
        scanf ("%s", str);

        memset(pal, -1, sizeof(pal));

        for (int i = 0; i < n; i++)
        {
            cntP[i] = palindromo(0, i) ? 1 : 1000000000;
            for (int j = 1; j <= i; j++)
                if (palindromo(j, i))
                    cntP[i] = min(cntP[i], cntP[j-1] + 1);
        }

        printf ("Teste %d\n%d\n\n", teste++, cntP[n-1]);
    }

    return 0;
}

