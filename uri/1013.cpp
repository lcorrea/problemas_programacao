/*
	problem 1013
	the greatest
	by lucas correa
*/
#include <iostream>
#include <cmath>

using namespace std;

int main(void)
{
	int greater = 0, num;

	for(unsigned int i = 0; i < 2; i++)
	{
		cin >> num;
		greater = (greater+ num + abs(greater - num))/2;
	}

	cout << greater << " eh o maior" << endl;

	return 0;
}

