#include <cstdio>

using namespace std;

int main(void)
{
	float score = 0, tmp = -1;
	unsigned char a = 0;
	int k = 1;


	while(k == 1)
	{
		for(a = 0; a < 2; a++)
		{
			while(1)
			{
				scanf("%f%*c", &tmp);

				if(tmp < 0 || tmp > 10)
					puts("nota invalida");
				else
					break;
			}

			score += tmp;
		}

		printf("media = %.2f\n", score/2.0);

		score = 0;

		do
		{
			puts("novo calculo (1-sim 2-nao)");
			scanf("%d%*c", &k);
		}while(k > 2 || k < 1);
	}
	

	return 0;
}

