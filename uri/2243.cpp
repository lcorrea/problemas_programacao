#include <cstdio>

#define min(a, b) (a < b ? a : b)
#define max(a, b) (a > b ? a : b)

using namespace std;

int main (void)
{
    int n;
    int e[50001], d[50001], h[50001];

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", h + i);

    e[n-1] = d[0] = 1;

    //calc direita
    for (int i = 1; i < n; i++) {
        if (d[i-1] + 1 <= h[i])
            d[i] = d[i-1] + 1;
        else
            d[i] = h[i];
    }

    //calc esquerda
    for (int i = n - 2; i >= 0; i--) {
        if (e[i+1] + 1 <= h[i])
            e[i] = e[i+1] + 1;
        else
            e[i] = d[i];
    }

    //calc isosceles
    int maxH = 1;
    for (int i = 0; i < n; i++)
        maxH = max(maxH, min(e[i], d[i]));

    printf ("%d\n", maxH);

    return 0;
}

