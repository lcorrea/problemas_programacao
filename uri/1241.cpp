#include <iostream>

using namespace std;

int main (void)
{
	int n;
	string a, b;

	cin >> n;

	while (n--)
	{
		cin >> a >> b;

		int sz = b.length();

		if (a.length() < sz)
			cout << "nao encaixa";
		else if (a==b)
			cout << "encaixa";
		else if (b == a.substr(a.length() - sz))
			cout << "encaixa";
		else 
			cout << "nao encaixa";

		cout << endl;
	}

	return 0;
}

