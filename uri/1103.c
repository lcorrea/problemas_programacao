#include <stdio.h>

unsigned int time2min(unsigned int hour, unsigned int min)
{
	return min + hour*60;
}

int main(void)
{

	unsigned int minA, hourA, minB, hourB, minTotalA = 0, minTotalB = 0;

	scanf("%u %u %u %u%*c", &hourA, &minA, &hourB, &minB);
	while(hourA || hourB || minA || minB)
	{
		minTotalA = time2min(hourA, minA);
		minTotalB = time2min(hourB, minB);
		
		if(minTotalB < minTotalA)
			minTotalB = 24*60 + minTotalB;

		printf("%u\n", minTotalB - minTotalA);
		scanf("%u %u %u %u", &hourA, &minA, &hourB, &minB);
	}

	return 0;
}

