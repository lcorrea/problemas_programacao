#include <bits/stdc++.h>

using namespace std;

char ox, oy, dx, dy;
int chessboard[256];

int bfs (int x, int y)
{
  const int di[8] = { -1, -2, -1, -2, 1, 1, 2, 2};
  const int dj[8] = { -2, 1, 2, -1, -2, 2, -1, 1};
  const int ex = chessboard[dx], ey = chessboard[dy];
  int dist[10][10];
  queue < pair <int, int> > q;

  memset (dist, -1, sizeof (dist));

  dist[y][x] = 0;
  q.push (make_pair (x, y));

  while (!q.empty())
  {
    int i = q.front().second;
    int j = q.front().first;
    q.pop();

    if (i == ey and j == ex)
      return dist[ey][ex];

    for (int cell = 0; cell < 8; cell++)
    {
      int cellj = j + dj[cell];
      int celli = i + di[cell];

      if (cellj >= 0 and cellj < 8)
        if (celli >= 0 and celli < 8)
          if (dist[celli][cellj] == -1)
          {
            dist[celli][cellj] = 1 + dist[i][j];

            q.push (make_pair (cellj, celli));
          }
    }

  }

  return -1;
}

int main (void)
{

  for (char c = 'a'; c <= 'h'; c++)
    chessboard[c] = c - 'a';

  for (char c = '1'; c <= '8'; c++)
    chessboard[c] = c - '1';

  while (EOF != scanf ("%c%c %c%c%*c", &ox, &oy, &dx, &dy))
  {
    int moves = bfs (chessboard[ox], chessboard[oy]);

    printf ("To get from %c%c to %c%c takes %d knight moves.\n",
        ox, oy, dx, dy, moves);
  }

  return 0;
}

