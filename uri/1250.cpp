#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;

  scanf ("%d", &n);

  while (n--)
  {
    int s, h[51], hit = 0;
    char p[51];

    scanf ("%d", &s);

    for (int i = 0; i < s; i++)
      scanf ("%d", h + i);

    scanf ("%s", p);

    for (int i = 0; p[i]; i++)
      hit += p[i] == 'J' ? (h[i] > 2) ? 1 : 0 : (h[i] == 1 or h[i] == 2 ? 1 : 0);

    printf ("%d\n", hit);

  }

  return 0;
}

