#include <stdio.h>
#include <set>

int main (void)
{
	int n, estrela, pre;
	long long unsigned int carneiros = 0;
	std::set<int> atacados;
	int sitio[1000002];

	scanf ("%d", &n);

	for (estrela = 1; estrela <= n; estrela++)
		scanf ("%d", &sitio[estrela]), carneiros += sitio[estrela];
	estrela = 1;
	while (estrela >= 1 and estrela <= n)
	{
		if (sitio[estrela])
		{
			atacados.insert (estrela);
			pre = sitio[estrela]--;
			carneiros--;
			if (pre % 2) estrela++;
			else estrela--;
		}
		else atacados.insert(estrela--);
	}
	printf ("%d %llu\n", atacados.size(), carneiros);

	return 0;
}

