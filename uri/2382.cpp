#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int l, a, p, r;

    scanf ("%d%d%d%d", &l, &a, &p, &r);

    puts (r*2.0 >= sqrt(a*a + l*l + p*p) ? "S" : "N");

    return 0;
}

