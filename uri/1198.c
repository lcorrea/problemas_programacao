#include <stdio.h>

int main (void)
{
	long long int n, m;

	while (scanf ("%lld %lld", &n, &m) != EOF)
	{
		if (m > n)
		{
			long long int tmp = n;
			n = m;
			m = tmp;
		}

		printf ("%lld\n", n-m);
	}

	return 0;
}

