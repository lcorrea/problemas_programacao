#include <bits/stdc++.h>

using namespace std;
int tb[100][100];

int backtraking (int i, int n)
{
	if (i > n)
		return 0;

	if (tb[i][n]) return tb[i][n];

	if (i == n)
		return 1;

	tb[i][n] = backtraking(i+1, n) + backtraking(i+2, n);

	return tb[i][n];
}

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
		printf ("%d\n", backtraking(0, n));

	return 0;
}

