#include <bits/stdc++.h>

using namespace std;

int n1, n2;
vector < vector<int> > graph;

int count_changes (vector <int> &ts)
{
    int cnt = 2;
    int disc = ts[0];

    for (int i = 1; i < (int) ts.size(); disc = ts[i], i++)
        if ((disc <= n1 and ts[i] > n1) or (disc > n1 and ts[i] <= n1))
            cnt++;

    return cnt;
}

void install_dep (queue <int> &q, int out[100001], vector <int> &ts, queue <int> &next)
{
    while (!q.empty())
    {
        int u = q.front();

        out[u] = -1; //installed
        q.pop();
        ts.push_back(u);

        for (int i = 0; i < (int) graph[u].size(); i++)
        {
            int v = graph[u][i];

            if (out[v])
            {
                if (1 == out[v])
                    if ((u <= n1 and v <= n1) or (u > n1 and v > n1))
                        q.push(v);
                    else
                        next.push(v);
                out[v]--;
            }
        }
    }
}

queue <int> verify_dep (int out[100001], int disc)
{
    queue <int> q;

    int first = disc == 1 ? 1 : n1+1;
    int last = disc == 1 ? n1 : n1+n2;

    for (int i = first; i <= last; i++)
        if (out[i] == 0)
            q.push(i);

    return q;
}

int run_process (int first_disc, int out[100001])
{
    int x1, x2;
    int max_size = n1+n2;
    vector <int> ts;
    queue <int> q1, q2;

    q1 = verify_dep(out, first_disc);
    q2 = verify_dep(out, first_disc == 1 ? 2 : 1);

    while ((int) ts.size() < max_size)
    {
        install_dep (q1, out, ts, q2); //insert first disc
        install_dep (q2, out, ts, q1); //insert second disc
    }

    return count_changes (ts);
}


int main (void)
{
    int d;

    while (scanf ("%d %d %d", &n1, &n2, &d), n1 or n2 or d)
    {
        int out[100001] = {0}, out2[100001] = {0};

        graph.clear();
        graph.resize(n1+n2+1);

        for (int i = 0; i < d; i++)
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            graph[b].push_back(a);
            out[a]++;
            out2[a]++;
        }

        int x1 = run_process(1, out); 
        int x2 = run_process(2, out2);

        //printf ("%d %d\n", x1, x2);
        printf ("%d\n", min(x1, x2));
    }

    return 0;
}

