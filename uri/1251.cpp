#include <bits/stdc++.h>

using namespace std;

bool cmp (pair <int, char> a, pair <int, char> b)
{
    if (a.first == b.first)
        return a.second > b.second;
    return a.first < b.first;
}

int main (void)
{
    string line;

    getline (cin, line);

    while (!cin.eof())
    {
        char hashmap[255] = {0};
        set < char > letters;
        set < char >::const_iterator it;
        vector < pair <int, char> > freq;

        for (int i = 0; line[i] != '\0'; i++)
            hashmap[line[i]]++, letters.insert (line[i]);

        for (it = letters.begin(); it != letters.end(); it++)
            freq.push_back ( make_pair (hashmap[*it], *it) );

        sort (freq.begin(), freq.end(), cmp);

        for (int i = 0; i < (int) freq.size(); i++)
            cout << (int) freq[i].second << ' ' << freq[i].first << endl;

        getline (cin, line);

        if (!cin.eof()) cout << endl;
    }

	return 0;
}

