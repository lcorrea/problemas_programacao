#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (cin >> n, n and !cin.eof())
    {
        int p = 0, cnt = 0;
        char v[4];
        string panel[n+1];

        panel[0] = "FACE";

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < 4; j++)
                cin >> v[j];

            if (panel[p][0] == v[3] and panel[p][1] == v[2] and panel[p][2] == v[1] and panel[p][3] == v[0])
            {
                cnt++;

                if (p > 0)
                    p--;
            }
            else
            {
                p++; panel[p] = "";
                for (int j = 0; j < 4; j++)
                    panel[p] += v[j];
            }
        }

        cout << cnt << endl;
    }

    return 0;
}

