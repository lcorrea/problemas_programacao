#!/usr/bin/env python3

p1, p2, p3, p4 = map(float, input().split())

media = (p1*2.0 + p2*3.0 + p3*4.0 + p4) / 10.0

print("Media: %.01f" % media)

if (media < 5.0):

    print("Aluno reprovado.")

elif (media <= 6.9):
    
    print("Aluno em exame.")
    
    exame = float(input())
    
    print("Nota do exame: %.01f" % exame)
    
    media = (exame + media) / 2.0
    
    print("Aluno reprovado." if media < 5.0 else "Aluno aprovado.")
    print("Media final: %.01f" % media)

else:
    print("Aluno aprovado.")

