#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    vector <string> nomes(101);

    cin >> n >> k;

    for (int i = 0; i < n; i++)
        cin >> nomes[i];

    sort (nomes.begin(), nomes.begin() + n);

    cout << nomes[k-1] << endl;        

    return 0;
}

