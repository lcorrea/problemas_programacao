#include <bits/stdc++.h>

using namespace std;

int knapsack (int *q, int *w, int tb[101][1001], int n, int i, int aguenta)
{
    if (tb[i][aguenta] != -1)
        return tb[i][aguenta];

    if (i == n)
        return tb[i][aguenta] = 0;

    int naopega = knapsack(q, w, tb, n, i+1, aguenta);

    if (w[i] <= aguenta)
    {
        int pega = knapsack(q, w, tb, n, i+1, aguenta-w[i]) + q[i];

        tb[i][aguenta] = max(pega, naopega);

        return tb[i][aguenta];
    }

    return tb[i][aguenta] = naopega;
}

int main (void)
{
    int g;

    scanf ("%d", &g);

    for (int Case = 1; Case <= g; Case++)
    {
        int p, k;
        int q[110], w[110], tb[101][1001];

        scanf ("%d", &p);
        scanf ("%d", &k);

        for (int i = 0; i < p; i++)
            scanf ("%d %d", q + i, w + i);

        memset (tb, -1, sizeof(tb));

        printf ("Galho %d:\n"
                "Numero total de enfeites: %d\n\n",
                Case,
                knapsack(q, w, tb, p, 0, k));
    }


    return 0;
}

