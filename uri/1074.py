#!/usr/bin/env python3

n = int(input())

for i in range(n):
    x = int(input())
    if (x == 0):
        print("NULL")
    elif (x & 1):
        print("ODD", "NEGATIVE" if x < 0 else "POSITIVE")
    else:
        print("EVEN", "NEGATIVE" if x < 0 else "POSITIVE")

