#!/usr/bin/env python3

x = int(input())
y = int(input())

if (x > y):
    x, y = y, x

cnt = 0

for i in range(x+2 if (x & 1) else x + 1, y, 2):
    cnt += i

print(cnt)

