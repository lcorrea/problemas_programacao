// competitive programming - chapter 9
// algorithm - Shuting Yard - (Edsger Dijkistra)
// code by: lucas
#include <cstdio>
#include <cctype>
#include <stack>

#define isoperator(c) ((c == '-' or c == '+' or c == '*' or c == '^' or c == '/'))

using namespace std;

int main (void)
{
	stack <char> sol;
	int n;
	char c;

	scanf ("%d%*c", &n);
	while (n--)
	{
		while (c = getchar (), c != '\n')
		{
			if (isalnum (c)) putchar (c);
			else if (c == ')')
			{
				while (!sol.empty() and sol.top() != '(')
				{
					putchar (sol.top());
					sol.pop();
				}
				if (!sol.empty()) sol.pop();
			}
			else if (isoperator(c))
			{
				char top;
				switch (c)
				{
					case ('-'):
					case ('+'):
						while (!sol.empty() and sol.top() != '(')
						{
							putchar (sol.top());
							sol.pop();
						}
					break;
					case ('*'):
					case ('/'):
						while (!sol.empty())
						{
							top = sol.top();
							if (top == '-' or top == '+' or top == '(')
								break;
							putchar (top);
							sol.pop();
						}
					break;
					case ('^'):
						while (!sol.empty() and sol.top() == c)
						{
							putchar (sol.top());
							sol.pop();
						}
					break;
				}
				sol.push (c);
			}
			else
				sol.push (c);
		}
		while (!sol.empty())
		{
			putchar (sol.top());
			sol.pop();
		}
		putchar ('\n');
	}

	return 0;
}

