#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vi nivel;
vvi graph;
int cartas[50002][2];
int n;

unsigned int bfs (int s, int t)
{
	vi d(n+1, 1000000000); d[s] = 0;
	queue<int> q; q.push(s);

	while (!q.empty())
	{
		int u = q.front(); q.pop();
		for (int i = 0; i < graph[u].size(); i++)
		{
			int v = graph[u][i];
			if (d[v] == 1000000000)
			{
				d[v] = d[u]+1;
				//printf ("%d->%d\n", u, v);
				if (v == t) return d[v];
				q.push(v);
			}
		}
	}
}

int main ()
{
	int c, a, b;

	scanf ("%d", &n);
	graph.resize(n+1);

	for (int i = 1; i <= n; i++)
	{
		scanf ("%d", &c);
		if (cartas[c][0]) cartas[c][1] = i;
		else cartas[c][0] = i;
	}

	for (int i = 1; i < n; i++)
	{
		scanf ("%d %d", &a, &b);
		graph[a].push_back (b);
		graph[b].push_back (a);
	}


	long long unsigned int total = 0;
	for (int i = 1; i <= n/2; i++)
		total += bfs(cartas[i][0], cartas[i][1]);

	printf ("%llu\n", total);

	return 0;
}

