#include <bits/stdc++.h>

using namespace std;

int tb[1000][1000];
int d[200], w[200];
int n;

int loadCannon (int i, int l)
{
	if (tb[i][l] >= 0)
		return tb[i][l];

	if (i >= n or !l)
		return tb[i][l] = 0;

	int damage1 = loadCannon (i+1, l);

	if (w[i] <= l)
	{
		int damage2 = d[i] + loadCannon (i+1, l - w[i]);

		return tb[i][l] = max (damage1, damage2);
	}

	return tb[i][l] = damage1;
}

int main (void)
{
	int t, l, r;
	
	scanf ("%d", &t);

	while (t--)
	{
		memset(tb, -1, sizeof(tb));
		scanf("%d", &n);

		for (int i = 0; i < n; i++)
			scanf ("%d %d", &d[i], &w[i]);

		scanf ("%d%d", &l, &r);

		int maxDamage = loadCannon(0, l);

		puts (maxDamage >= r ? "Missao completada com sucesso" : 
				"Falha na missao");
	}

	return 0;
}

