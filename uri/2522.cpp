#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
    private:
        vector <int> h, p;
        int n;

    public:
        UnionFind (int _n)
        {
            n = _n;
            h.assign(n, 0);
            p.assign(n, 0);

            for (int i = 0; i < n; i++)
                p[i] = i;
        }

        int findSet (int i)
        {
            return i == p[i] ? i : p[i] = findSet(p[i]);
        }

        int isSameSet (int i, int j)
        {
            return findSet(i) == findSet(j);
        }

        void join (int i, int j)
        {
            if (isSameSet(i, j)) return;

            i = findSet(i);
            j = findSet(j);

            if (h[i] > h[j])
                p[j] = i;
            else
            {
                p[i] = j;
                h[j] += h[j] == h[i];
            }
        }
};

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        double d[n][n];
        pair <int, int> p[n];
        vector < pair <double, pair <int, int> > > edges;

        for (int i = 0; i < n; i++)
            scanf ("%d %d", &p[i].first, &p[i].second);

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            {
                d[i][j] = hypot(p[i].first - p[j].first, p[i].second - p[j].second);
                edges.push_back(make_pair(d[i][j], make_pair(i, j)));
            }

        sort (edges.begin(), edges.end());

        UnionFind ds(n);
        double cost = 0;

        for (int i = 0; i < (int) edges.size(); i++)
        {
            int a = edges[i].second.first;
            int b = edges[i].second.second;
            double w = edges[i].first;

            if (!ds.isSameSet(a, b))
                cost += w, ds.join(a,b);
        }

        printf ("%.02lf\n", cost);
    }

    return 0;
}

