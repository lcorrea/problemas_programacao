#include <bits/stdc++.h>

using namespace std;

map <string, string> nomes;

int ler (const char *num)
{
    string centena, dezena, unidade;

    if (nomes.find (num) != nomes.end())
    {
        if (nomes[num] != "000")
            return printf ("%s", nomes[num].c_str()), 1;
    }

    if (num[0] != '0')
    {
        centena = centena + num[0] + "00";
        if (num[0] == '1' and (num[1] != '0' or num[2] != '0'))
            printf ("cento");
        else
            printf ("%s", nomes[centena].c_str());
    }

    if (num[1] != '0')
    {
        if (num[0] != '0')
            printf (" e ");

        dezena = dezena + "0" + num[1] + num[2];

        if (nomes.find(dezena) != nomes.end())
        {
            printf ("%s", nomes[dezena].c_str());
            return 0;
        }
        
        dezena = ""; dezena = dezena + "0" + num[1] + "0";
        printf ("%s", nomes[dezena].c_str());
    }

    unidade = unidade + "00" + num[2];
    printf (" e %s", nomes[unidade].c_str());

    return 0;
}

int main (void)
{
    int number;
    char num[20];

    nomes["000"] = "zero"; nomes["001"] = "um";
    nomes["002"] = "dois"; nomes["003"] = "tres";
    nomes["004"] = "quatro"; nomes["005"] = "cinco";
    nomes["006"] = "seis"; nomes["007"] = "sete";
    nomes["008"] = "oito"; nomes["009"] = "nove";
    nomes["010"] = "dez"; nomes["011"] = "onze";
    nomes["012"] = "doze"; nomes["013"] = "treze";
    nomes["014"] = "quatorze"; nomes["015"] = "quinze";
    nomes["016"] =  "dezesseis"; nomes["017"] =  "dezessete";
    nomes["018"] =  "dezoito";  nomes["019"] =  "dezenove";
    nomes["020"] =  "vinte";    nomes["030"] =  "trinta";
    nomes["040"] =  "quarenta"; nomes["050"] =  "cinquenta";
    nomes["060"] =  "sessenta"; nomes["070"] =  "setenta";
    nomes["080"] =  "oitenta";   nomes["090"] =  "noventa";
    nomes["100"] = "cem";   nomes["200"] = "duzentos";
    nomes["300"] = "trezentos"; nomes["400"] = "quatrocentos";
    nomes["500"] = "quinhentos"; nomes["600"] = "seiscentos";
    nomes["700"] = "setecentos"; nomes["800"] = "oitocentos";
    nomes["900"] = "novecentos"; nomes["001000"] = "mil";

    while (cin >> number, !cin.eof())
    {
        sprintf (num, "%06d", number);
        int c = number - (number/1000)*1000;
        string str = num;
        string hi = str.substr(0, 3);
        string lo = str.substr(3);

        if (number==0) { puts ("zero"); continue;}

        if (number >= 2000)
            ler (hi.c_str()), printf (" mil");

        if (number >= 1000 and number < 2000)
            printf ("mil");

        if (c)
        {
            if (number > 999)
                putchar (' ');
            
            if ((c < 100 or c % 100 == 0) and number > 999)
                printf ("e ");

            ler (lo.c_str());
        }
        putchar ('\n');
    }

    return 0;
}

