/*
	problem 1307
	all you need is love
	by lucas correa
	time:
*/

#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>

using namespace std;

unsigned int bStr2Int(string &s1)
{
	unsigned int r = 0, i;

	reverse(s1.begin(), s1.end());

	for(i = 0; i < s1.size(); i++)
		if(s1[i] == '1')
			r += pow(2, i);

	return r;
}


unsigned int gcd(unsigned int a, unsigned int b)
{
	unsigned int r = 0;

	while(b != 0)
	{
		r = a%b;

		a = b;
		b = r;
	}

	return a;
}

int main(void)
{
	string s1;
	unsigned int b_s1, b_s2, n, pair = 0;

	cin >> n;

	while(n--)
	{
		pair++;

		cin >> s1;
		b_s1 = bStr2Int(s1);

		cin >> s1;
		b_s2 = bStr2Int(s1);

		cout << "Pair #" << pair << ": ";

		if(gcd(b_s1, b_s2) > 1)
			cout << "All you need is love!";
		else
			cout << "Love is not all you need!";

		cout << endl;
	}

	return 0;
}

