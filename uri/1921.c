#include <stdio.h>

int main (void)
{
	int n, i;
	long long unsigned int total = 1;

	scanf ("%d", &n);
	total = ((long long unsigned int) n*(n-1))/2ull - n;
	printf ("%llu\n", total);
	return 0;	
}

