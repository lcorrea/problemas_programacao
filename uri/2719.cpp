#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf("%d", &t);

    while (t--) {

        int n, w, wt = 0, travel = 0;

        scanf("%d %d", &n, &w);

        for (int i = 0, p; i < n; i++) {
            scanf("%d", &p);

            if (wt + p > w) {
                travel++;
                wt = 0;
            }

            wt += p;
        }

        printf("%d\n", travel + (wt > 0));
    }

    return 0;
}

