#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    float n[5], a = 4.0, b = 11.0, s = 0;

    for (int i = 0; i < 5; i++) {
        scanf("%f", n+i);
        
        if (n[i] > a)
            a = n[i];
        
        if (n[i] < b)
            b = n[i];
        
        s += n[i];
    }

    printf("%.1f\n", s - a - b);

    return 0;
}

