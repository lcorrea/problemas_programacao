/*
	problem 1047
	game time with minutes
	by lucas correa
	time: 00:27:29
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int s_min, s_hour, e_min, e_hour;

	cin >> s_hour >> s_min >> e_hour >> e_min;

	s_hour *= 60;
	s_hour += s_min;
	e_hour *= 60;
	e_hour += e_min;

	if(e_hour <= s_hour)
		e_hour += 24*60;

	e_hour -= s_hour;
	
	cout << "O JOGO DUROU ";
	cout << e_hour/60;
	cout << " HORA(S) E ";
	cout << e_hour%60;
	cout << " MINUTO(S)" << endl;

	return 0;
}

