#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    
    scanf("%d", &t);

    while (t--) {
        int n, m;

        scanf("%d %d", &n, &m);

        int d[m];

        for (int i = 0; i < m; i++)
            scanf("%d", d + i);

        sort(d,d+m);

        long long int ans = min(d[0], n), x = min(d[0], n);
        
        for (int i = 1; i < m and d[i] <= n; i++) {
            ans = max(ans, (long long int) d[i] - x);
            x += d[i] - x;
        }

        ans = max(ans, n - x);

        printf("%lld\n", ans);
    }

    return 0;
}

