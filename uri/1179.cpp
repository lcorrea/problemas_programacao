#include <iostream>
#include <string>

using namespace std;

void printvet (int *v, unsigned int i, unsigned int type)
{
	string name[2] = {"impar", "par"};

	for (unsigned int b = 0; b < i; b++)
		cout << name[type] << '[' << b << "] = " << v[b] << endl;
}

int main (void)
{
	int par[5], impar[5], n, p = 0, i = 0;

	for (unsigned int a = 0; a < 15; a++)
	{
		cin >> n;

		if (n % 2)
		{
			if (i == 5)
			{
				printvet (impar, 5, 0);
				i = 0;
			}

			impar[i++] = n;
		}
		else
		{
			if (p == 5)
			{
				printvet (par, 5, 1);
				p = 0;
			}

			par[p++] = n;
		}
	}

	printvet (impar, i, 0);
	printvet (par, p, 1);
	
	return 0;
}

