#!/usr/bin/env python3

def sum_odd(i):
    tb = [0]
    k = 1
    while True:
        if (i <= k):
            yield tb[i-1]
        tb.append(tb[-1] if not(k & 1) else k + tb[-1])
        k += 1

def main():
    
    from sys import stdin, stdout

    # --- #
    while(True):
        i = int(stdin.readline())
        print("i=", int(sum_odd(i)))

if __name__ == "__main__":
    main()

