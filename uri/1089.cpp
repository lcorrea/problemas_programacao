#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int m[10009];

	while(scanf ("%d", &n), n)
	{
		int cnt = 0;
		for (int i = 0; i < n; i++)
			scanf ("%d", &m[i]);

		for (int i = 0; i < n; i++)
		{
			if (m[i] < m[(i+1)%n] and m[(i+1)%n] > m[(i+2)%n])
				cnt++;

			if (m[i] > m[(i+1)%n] and m[(i+1)%n] < m[(i+2)%n])
				cnt++;
		}

        printf ("%d\n", cnt);
	}

	return 0;
}
