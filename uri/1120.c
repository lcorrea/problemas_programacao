/*
	problem 1120
	contract revision
	by lucas correa
	time: 2:47:00
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	char d, n[100];
	unsigned char i = 0, size = 0, j = 0;

	scanf("%c %[^\n]%*c", &d, n);

	while(d != '0' || n[0] != '0')
	{
		size = strlen(n);
		
		for(i = 0, j = 0; i < size; i++)
			if(n[i] != d && n[i] != '0')
				break;

		for(; i < size; i++)
		{
			if(*(n+i) != d)
				putchar(*(n + j++) = *(n+i));
		}

		if(j)
			putchar('\n');
		else
			puts("0");
		scanf("%c %[^\n]%*c", &d, n);
	}

	return 0;
}

