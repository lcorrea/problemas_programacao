#include <iostream>

using namespace std;

int main (void)
{
	int n, s;

	cin >> n;

	while (n--)
	{
		cin >> s;

		if (s % 2)
			cout << 1;
		else
			cout << 0;
		cout << endl;
	}

	return 0;
}

