#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int a, b, c;

    scanf ("%d %d %d", &a, &b, &c);

    int um = 2*b + 4*c;
    int dois = 2*a + 2*c;
    int tres = 4*a + 2*b;

    printf ("%d\n", min(um, min(dois, tres)));

    return 0;
}

