#!/usr/bin/env python3

x = int(input())

for i in range(x if (x & 1) else x + 1, x + 12 if (x & 1) else x + 13 , 2):
    print(i)
