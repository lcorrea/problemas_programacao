/*
	problem 1049
	animal
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	string nome1, nome2, nome3;

	cin >> nome1 >> nome2 >> nome3;

	if(nome1 == "vertebrado")
	{
		if(nome2 == "ave")
		{
			if(nome3 == "carnivoro")
				cout << "aguia";
			else
				cout << "pomba";
		}
		else
		{
			if(nome3 == "onivoro")
				cout << "homem";
			else
				cout << "vaca";
		}
	}
	else
	{
		if(nome2 == "inseto")
		{
			if(nome3 == "hematofago")
				cout << "pulga";
			else
				cout << "lagarta";
		}
		else
		{
			if(nome3 == "hematofago")
				cout << "sanguessuga";
			else
				cout << "minhoca";
		}
	}

	cout << endl;

	return 0;
}
