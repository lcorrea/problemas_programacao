#include <stdio.h>
#include <stdlib.h>

int main (void)
{
	long long unsigned int r;
	unsigned int n, m;

	while (scanf ("%u %u", &n, &m), n && m)
	{
		r = n + m;
		char result[50], *ptr;
		sprintf (result, "%llu", r);
		ptr = result;

		while (*ptr)
		{
			if (*ptr != '0')
				putchar (*ptr);
			ptr++;
		}

		putchar ('\n');
	}

	return 0;
}

