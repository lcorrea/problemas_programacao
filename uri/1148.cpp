#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

int main (void)
{
	int q, e, n;

	while (scanf ("%d %d", &n, &e), n or e)
	{
		int adjGraph[n+1][n+1];

		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				adjGraph[i][j] = i != j ? INF : 0;

		for (int i = 0, u, v, p; i < e; i++)
		{
			scanf ("%d %d %d", &u, &v, &p);
			adjGraph[u][v] = p;
			if (adjGraph[v][u] != INF) { adjGraph[v][u] = 0; adjGraph[u][v] = 0; }
		}

		for (int k = 1; k <= n; k++)
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= n; j++)
					adjGraph[i][j] = min (adjGraph[i][j], adjGraph[i][k] + adjGraph[k][j]);

		scanf ("%d", &q);

		for (int i = 0, u, v; i < q; i++)
		{
			scanf ("%d %d", &u, &v);
			if (adjGraph[u][v] != INF)
				printf ("%d\n", adjGraph[u][v]);
			else
				puts ("Nao e possivel entregar a carta");
		}
		putchar ('\n');
	}

	return 0;
}

