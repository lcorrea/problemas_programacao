#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int pecas[1001];

    scanf ("%d", &n);

    memset (pecas, 0, sizeof(pecas));

    for (int i = 1; i <= n - 1; i++)
    {
        int p;

        scanf ("%d", &p);

        pecas[p] = 1;
    }

    for (int i = 1; i <= n; i++)
        if (pecas[i] == 0)
        {
            printf ("%d\n", i);
            return 0;
        }


    return 0;
}

