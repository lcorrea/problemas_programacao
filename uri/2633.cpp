#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        vector < pair <int, string> > carnes;

        for (int i = 0; i < n; i++)
        {
            char nome[20];
            int val;

            scanf ("%s %d", nome, &val);

            carnes.push_back(make_pair(val, nome));
        }

        sort (carnes.begin(), carnes.end());

        for (int i = 0; i < n; i++)
            printf ("%s%c", 
                    carnes[i].second.c_str(), 
                    i == n-1 ? '\n' : ' ');
    }

    return 0;
}


