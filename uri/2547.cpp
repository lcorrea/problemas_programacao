#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, hmin, hmax;

    while (scanf ("%d %d %d", &n, &hmin, &hmax) != EOF)
    {
        int cnt = 0;

        for (int i = 0, h; i < n; i++)
            scanf ("%d", &h), cnt += h >= hmin and h <= hmax;

        printf ("%d\n", cnt);
    }

    return 0;
}

