#include <cstdio>
#include <map>
#include <cmath>

using namespace std;

#define digits(n) ((unsigned int) log10(n))

inline void printBlank(unsigned int blanks)
{
	while (blanks > 0)
	{
		putchar(' ');
		blanks--;
	}
}

int main (void)
{
	map <unsigned int, long long unsigned int> squares;
	unsigned int numbers[400];
	unsigned int n, m, cnt = 3, number;

	scanf ("%u%*c", &n);

	while (n)
	{
		scanf ("%u%*c", &m);
		printf("Quadrado da matriz #%u:\n", ++cnt);

		unsigned int maxDigit[20] = {0};

		for (unsigned int i = 0; i < m; i++)
		{
			for (unsigned int l = 0; l < m; l++)
			{
				scanf("%u%*c", numbers + i*m + l);
				number = numbers[i*m + l];

				if (squares[number] == 0 && number)
					squares[number] = (long long unsigned int) number*number;

				maxDigit[l] = max(maxDigit[l], digits(squares[number]));
			}
		}
		
		for (unsigned int i = 0; i < m; i++)
		{
			for (unsigned int j = 0; j < m - 1; j++)
			{	
				number = numbers[i*m + j];
				printBlank(maxDigit[j] - digits(squares[number]));
				printf("%llu ", squares[number]);
			}

			number = numbers[m * (i + 1) - 1];
			printBlank(maxDigit[m - 1] - digits(squares[number]));
			printf("%llu\n", squares[number]);
		}

		n--;

		if(n)
		{
			putchar('\n');
		}
	}

	return 0;
}

