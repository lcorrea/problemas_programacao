#include <iostream>

using namespace std;

int main (void)
{
	unsigned int t, i = 0;

	cin >> t;

	while (i < 1000)
	{
		for (unsigned int a = 0; a < t && i < 1000; a++, i++)
			cout << "N[" << i << "] = " << a << endl;
	}

	return 0;
}

