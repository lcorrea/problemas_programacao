#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	int n;
	const char jogadores[3][10] = {"Uilton", "Rita", "Ingred"};

	while (scanf ("%d", &n), n)
	{
		int pt[3] = {0}, *p;

		while (n--)
		{
			int num[3];

			for (int i = 0; i < 3; i++)
			{
				scanf ("%d", num + i);

				if ((num[i] & (num[i] - 1)) == 0)
					pt[i]++;
			}

			p = max_element (num, num+3);

			if ((*p & (*p - 1)) == 0)
				pt[p - num]++;
		}

		p = max_element (pt, pt+3);
		
		if (count (pt, pt+3, *p) > 1)
			puts ("URI");
		else
			puts (jogadores[p - pt]);
	}

	return 0;
}

