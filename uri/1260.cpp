/*
	problem 1260
	Hardwood Species
	by lucas correa
	time: 01:24:45
*/
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>
#include <utility>
#include <map>

using namespace std;

int main(void)
{
	string name;
	int count, casoteste;
	map<string, int> list;
	map<string, int>::iterator it;

	cin >> casoteste;

	while(casoteste--)
	{
		list.clear();
		count = 0;
		
		while(name.size() == 0)
			getline(cin, name);

		while(name.size())
		{
			count++;
			
			it = list.find(name);
			
			if(it != list.end())
				it->second++;
			else
				list.insert(pair<string, int>(name, 1));
			
			getline(cin, name);
		}

		//imprime lista de arvores
		for(it = list.begin(); it != list.end(); it++)
		{
			cout << it->first;
			printf(" %.4f\n", (float) (it->second)/count*100);
		}

		if(casoteste)
			cout << endl;
	}

	return 0;
}

