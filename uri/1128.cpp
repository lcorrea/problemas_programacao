#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;
typedef vector <int> vi;
typedef vector <vi> AdjList;

int tick, numSCC;
vi dfs_num, dfs_low, S, visited;
AdjList city;

bool tarjanSCC (int u)
{
  dfs_low[u] = dfs_num[u] = ++tick;

  S.push_back (u);
  visited[u] = 1;

  for (int i = 0; i < (int) city[u].size(); i++)
  {
    int v = city[u][i];

    if (dfs_num[v] == 0)
      tarjanSCC (v);

    if (visited[v])
      dfs_low[u] = min (dfs_low[u], dfs_low[v]);
  }

  if (dfs_low[u] == dfs_num[u])
  {
    numSCC++;

    while (1)
    {
      int v = S.back(); S.pop_back();
      visited[v] = 0;
      if (u == v) break;
    }
  }
}

int main (void)
{
    int n, m;

    while (scanf ("%d%d", &n, &m), n and m)
    {
        dfs_num.assign (n, 0); dfs_low.assign (n, 0);
        visited.assign (n, 0); tick = numSCC = 0;
        city.clear(); city.resize(n);

        for (int i = 0, v, w, p; i < m; i++)
        {
            scanf ("%d%d%d", &v, &w, &p);
            
            city[v-1].push_back (w-1);

            if (p==2)
                city[w-1].push_back (v-1);
        }

        for (int i = 0; i < n; i++)
          if (dfs_num[i] == 0)
            tarjanSCC (i);

        printf ("%d\n", numSCC == 1);
    }

    return 0;
}

