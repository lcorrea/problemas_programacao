#include <bits/stdc++.h>

using namespace std;

void fill (int i, int j, char graph[1026][1026])
{
    const int dx[4] = {1, -1, 0, 0};
    const int dy[4] = {0, 0, 1, -1};

    graph[i][j] = 'o';

    for (int k = 0; k < 4; k++)
        if(graph[dy[k]+i][dx[k]+j] == '.')
            fill(dy[k]+i, dx[k]+j, graph);
}

int main (void)
{
    int n, m;
    char graph[1026][1026] = {'o'};

    scanf ("%d %d", &n, &m);

    for (int i = 1; i <= n; i++)
        scanf ("%s", &graph[i][1]);

    int clicks = 0;

    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            if (graph[i][j] == '.')
                fill(i, j, graph), clicks++;

    printf ("%d\n", clicks);

    return 0;
}

