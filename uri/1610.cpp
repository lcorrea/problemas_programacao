#include <iostream>
#include <set>
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vi pre, pos;

//dfs modificada para encontrar ciclos em um grafo dirigido
//retorna true se existe um ciclo no grafo

bool dfs (int vtx, vvi& graph)
{
	pre[vtx] = 1;

	for (unsigned int i = 0; i < graph[vtx].size(); i++)
	{
		int v = graph[vtx][i];

		if (pre[v] == 0)
		{
			if (dfs (v, graph))
				return true;
		}
		else if (pos[v] == 0)
			return true;
	}
	pos[vtx] = 1;

	return false;
}

int main (void)
{
	int t, n, m, a, b;

	cin >> t;

	while (t--)
	{
		cin >> n >> m;

		vvi graph (n);
		bool loop = false;

		pre.assign (n, 0);
		pos.assign (n, 0);

		for (int i = 0; i < m; i++)
		{
			cin >> a >> b;

			graph[a-1].push_back (b-1);
		}

		for (int i = 0; i < n; i++)
		{
			if (!loop)
				loop = dfs (i, graph);
			else
				break;
		}
		
		if (!loop)
			cout << "NAO";
		else
			cout << "SIM";

		cout << endl;
	}

	return 0;
}

