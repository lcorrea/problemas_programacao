#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    while (scanf ("%d %d", &n, &k), n and k)
    {
        int p[101] = {0};
        int add = 0;

        for (int i = 0, id; i < n; i++)
        {
            scanf ("%d", &id);

            add += (++p[id] == k);
        }

        printf ("%d\n", add);
    }

    return 0;
}

