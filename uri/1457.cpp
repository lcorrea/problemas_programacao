#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		char line[1000], ch;
		int k=1, i = 0, n;
		long long int f = 1;

		while ((ch = getchar()) != '!') line[i++] = ch;
		line[i] = '\0';

		sscanf (line, "%d", &n);
		
		while ((ch = getchar()) == '!') k++;

		i = 0;
		while (n - i*k >= 1)
		{
			f *= n - i*k;
			i++;
		}

		printf ("%lld\n", f);
	}

	return 0;
}

