#include <bits/stdc++.h>

using namespace std;

#define INF 1000000000

typedef vector < vector <pair <int, int> > > Graph;
typedef pair <int, pair <int, int>> Node;

int dijkstra(int u, int destiny, Graph &g)
{
    int d, lineA, lineB;
    vector <int> dist(g.size(), INF);
    priority_queue <Node, vector<Node>, greater<Node>> pq;

    dist[u] = 0;
    pq.push({0, {0, u}});

    while (!pq.empty())
    {
        d = pq.top().first;
        lineA = pq.top().second.first;
        u = pq.top().second.second;

        pq.pop();

        if (d > dist[u]) continue;

        for (auto v : g[u])
        {
            lineB = v.second;

            if (lineB == lineA and d < dist[v.first])
            {
                dist[v.first] = d;
                pq.push({d, {lineA, v.first}});
            }
            else if (d + 1 < dist[v.first])
            {
                dist[v.first] = d + 1;
                pq.push({d+1, {lineB, v.first}});
            }
        }
    }

    return dist[destiny];
}

int main (void)
{
    int n, k;

    scanf ("%d %d", &n, &k);

    Graph g(n);

    for (int line = 1; line <= k; line++)
    {
        int l, u, v;

        scanf ("%d", &l);

        int route[l];

        for (int i = 0; i < l; i++)
            scanf ("%d", route+i);

        for (int i = 0; i < l; i++)
            for (int j = 0; j < l; j++)
                if (i != j)
                    g[route[i]-1].push_back({route[j]-1, line});
    }

    printf ("%d\n", dijkstra(0, n-1, g));

    return 0;
}

