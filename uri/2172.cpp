#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  register unsigned long long m;
  register unsigned int x;

  while (scanf ("%u%llu", &x, &m), x and m)
    printf ("%llu\n", m * x);

  return 0;
}

