#include <bits/stdc++.h>

using namespace std;

int gcd(long long int a, long long int b)
{
	return b == 0 ? a : gcd(b, a % b);
}

int lcm(long long int a, long long int b)
{
	return a * (b / gcd(a, b));
}

int main (void)
{
    long long int a, b, n;

    while (scanf ("%lld %lld %lld", &n, &a, &b), n and a and b)
        printf ("%lld\n", n/a + n/b - n/lcm(a, b));

    return 0;
}

