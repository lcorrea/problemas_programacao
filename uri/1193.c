#include <stdio.h>
#include <string.h>
#include <math.h>

int main (void)
{
	int n, teste = 1;
	char str[110], base[5];
	scanf ("%d", &n);

	while (n--)
	{	
		unsigned int number = 0;
		int i = 0;
		char *ptr = str;

		printf ("Case %d:\n", teste++);

		scanf ("%s %s", str, base);
		if (base[0] == 'b')
		{
			i = strlen (str) - 1;
			while (*ptr)
			{
				number |= (*ptr=='1')?(1 << i):(0);
				ptr++;
				i--;
			}
			printf ("%u dec\n%x hex\n\n", number, number);
		}
		else if (base[0] == 'h')
		{
			i = strlen (str) - 1;
			number = strtoul (str, NULL, 16);
			printf ("%u dec\n", number);
			i = 31;
			while (!(number & (1 << i))) i--;
			while (i >= 0)
			{
				putchar ((number & (1 << i)) ? ('1') : ('0'));
				i--;
			}
			puts (" bin\n");
		}
		else
		{
			unsigned long ul = strtoul (str, NULL, 0);
			i = 31;
			printf ("%lx hex\n", ul);
			while (!(ul & (1 << i))) i--;
			while (i >= 0)
			{
				putchar ((ul & (1 << i)) ? ('1') : ('0'));
				i--;
			}
			puts (" bin\n");
		}
	}

	return 0;
}

