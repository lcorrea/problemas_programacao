#include <stdio.h>

int main (void)
{
	char id[5];
	int n, q;
	double total = 0;
	
	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%s %d%*c", id, &q);
		total += ((double) (id[3]-'0') + .5) * q;
	}

	printf ("%.2lf\n", total);

	return 0;
}

