#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    while (scanf ("%d %d", &n, &m) != EOF)
    {
        const int dx[4] = {0, 0, 1, -1};
        const int dy[4] = {1, -1, 0, 0};
        int tb[102][102];

        memset (tb, 0, sizeof(tb));

        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= m; j++)
                scanf ("%d", &tb[i][j]);

        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
                if (tb[i][j])
                    printf ("9");
                else
                {
                    int cnt = 0;
                    for (int k = 0; k < 4; k++)
                        cnt += tb[i+dy[k]][j+dx[k]];
                    printf ("%d", cnt);
                }
            
            putchar ('\n');
        }
    }

    return 0;
}

