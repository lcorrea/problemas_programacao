#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char alphabeto[27];

    while (scanf ("%s", alphabeto) != EOF)
    {
        int n;

        scanf ("%d", &n);

        for (int i = 0, id; i < n; i++)
        {
            scanf ("%d", &id);
            putchar (alphabeto[id-1]);
        }
        putchar('\n');
    }

    return 0;
}

