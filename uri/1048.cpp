/*
	problem 1048
	salary increase
	by lucas correa
	time: 00:13:42
*/
#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{	
	float salary, increase = 0.15;

	cin >> salary;
	
	if(salary > 2000.00)
		increase = 0.04;
	else if(salary > 1200.00)
		increase = 0.07;
	else if(salary > 800.00)
		increase = 0.1;
	else if(salary > 400.00)
		increase = 0.12;

	printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: %.0f \%%\n", salary + salary*increase, salary*increase, increase*100);

	return 0;
}
