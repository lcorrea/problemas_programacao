#include <bits/stdc++.h>

using namespace std;

class BST;

class BST
{
	private:
		class BST *left;
		class BST *right;
		int value;
	public:
		BST()
		{
			left = right = NULL;
			value = -1;
		}

		void insert (int child)	
		{
			if (value == -1) { value = child; }
			else if (child > value)
			{
				if (left == NULL) left = new BST();
				left->insert(child);
			}
			else
			{
				if (right == NULL) right = new BST();
				right->insert(child);
			}
		}

		int getValue () { return value; }
		
		BST toRight() { return *right; }
		
		BST toLeft() { return *left; }
		
		bool goToRight() { return right != NULL; }
		
		bool goToLeft() { return left != NULL; }
};

void pre (BST parent)
{
	int value = parent.getValue();

	if (value == -1) return;

	printf (" %d", value);

	if (parent.goToRight()) pre (parent.toRight());
	if (parent.goToLeft()) pre (parent.toLeft());
}

void in (BST parent)
{
	int value = parent.getValue();

	if (value == -1) return;

	if (parent.goToRight())
		in(parent.toRight());
	printf (" %d", value);
	if (parent.goToLeft())
		in (parent.toLeft());
	
}

void pos (BST parent)
{
	int value = parent.getValue();

	if (value == -1) return;


	if (parent.goToRight()) pos (parent.toRight());
	if (parent.goToLeft()) pos (parent.toLeft());

	printf (" %d", value);
}

int main (void)
{
	int n;

	scanf ("%d", &n);

	for (int i = 1; i <= n; i++)
	{
		int nodes;
		BST bst;

		scanf ("%d", &nodes);

		for (int j = 0, node; j < nodes; j++)
			scanf ("%d", &node), bst.insert(node);

		printf ("Case %d:\n", i);
		printf ("Pre.:"); pre (bst); putchar('\n');
		printf ("In..:"); in (bst); putchar ('\n');
		printf ("Post:"); pos (bst); putchar ('\n');
		putchar ('\n');
	}
	
	return 0;
}

