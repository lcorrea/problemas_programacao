#include <cstdio>

using namespace std;

int P[100001];

class BST;

class BST
{
	private:
		class BST *left;
		class BST *right;
		int value;
	public:
		BST()
		{
			left = right = NULL;
			value = -1;
		}

		void insert (int child, int o)	
		{
			if (value == -1) { value = child; }
			else if (child < value)
			{
				if (left == NULL) left = new BST(), P[o] = value;
				left->insert(child, o);
			}
			else
			{
				if (right == NULL) right = new BST(), P[o] = value;
				right->insert(child, o);
			}
		}
};

int main (void)
{
	BST p;
	int alunos[100003];
	int n;

	scanf ("%d", &n);

	for (int i = 0, a; i < n; i++)
		scanf ("%d", &alunos[i]), p.insert(alunos[i], i);

	int q;

	scanf ("%d", &q);

	for (int i = 0, a, j = 0; i < q; i++)
	{
		scanf("%d", &a);

		printf ("%d", P[a-1]);

		if (i < q - 1) 	putchar (' ');
	}
	putchar ('\n');

	return 0;
}

