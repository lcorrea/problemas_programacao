#include <bits/stdc++.h>

using namespace std;

bitset<1000000> bs;
vector <int> primes;

void sieve (void)
{
	bs.set();
	bs[0] = bs[1] = 0;

	for (int i = 2; i <= 1010; i++)
	{
		if (bs[i])
		{
			for (int j = i*i; j <= 1010; j += i)
				bs[j] = 0;
			primes.push_back (i);
		}
	}
}

int main (void)
{
	sieve();

	int n, m;

	cin >> n >> m;

	while (bs[n] == false) n--;

	while (bs[m] == false) m--;

	printf ("%d\n", n*m);

	return 0;
}

