#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>

using namespace std;

int main (void)
{
	int jn, jf;
	int n, f, i;
	vector < pair <int, int> > notas(3);

	cin >> jn >> jf;
	while (!cin.eof())
	{
		for (i = 0; i < 3; i++)
		{
			cin >> n >> f;
			notas[i] = pair <int, int> (-n, f);
		}
		sort (notas.begin(), notas.begin()+i);
		n = -notas[0].first;
		f = notas[0].second;
		
		if (jn > n and jf < f) cout << "MELHOR ALUNO";
		else if (jf < f) cout << "MAIS PRESENTE";
		else if (jn > n) cout << "MELHOR NOTA";
		else cout << "PRECISA MELHORAR";
		cout << endl;

		cin >> jn >> jf;
	}
	
	return 0;
}

