#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int d, i, x, f;

    scanf ("%d %d %d %d", &d, &i, &x, &f);

    int v = i;

    for (int a = d+1; a <= d + f; a++)
    {
        if (a % 2)
            v -= x;
        else
            v += x;
    }

    printf ("%d\n", v);

    return 0;
}

