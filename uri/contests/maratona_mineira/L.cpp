#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int l, c, x, y;

    scanf ("%d %d %d %d", &l, &c, &x, &y);

    long long int cnt = 1LL + x*c + y;

    printf ("%s\n", cnt % 2ll ? "Direita" : "Esquerda");

    return 0;
}

