#include <bits/stdc++.h>

using namespace std;

double p, j;
long long int x, m;

double calc (int k)
{
    return pow((1ll - p/100.0), k)*(j/100.0)*m;
}

int main (void)
{
    scanf ("%lld %lf %lf %lld",
            &m, &p, &j, &x);

    unsigned int middle, left, right, cnt = 1;

    left = 0;
    right = 4000000000;

    while (left <= right and cnt <= 33)
    {
        middle = (right + left) >> 1;

        double y = calc(middle);
        double z = calc(middle+1);

        if (y >= x and z <= x)
            break;

        else if (y >= x and z >= x)
            left = middle;
        else
            right = middle;

        cnt++;
    }

    printf ("%d\n", middle);

    return 0;
}

