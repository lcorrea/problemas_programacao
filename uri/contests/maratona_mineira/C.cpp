#include <bits/stdc++.h>

using namespace std;

int ans[4] = {0, 0, 0, 0};
int n, m;
char grid[1000][1000];

pair <char, int> resl[1010], resc[1010];

int ki = 0;

void dC (int x, char c)
{
    //for (int i = 0; i < n; i++)
    //    grid[i][x] = c;
    resc[x] = make_pair(c, ki);
}

void dL (int x, char c)
{
    resl[x] = make_pair(c, ki);
    // for (int i = 0; i < m; i++)
    //     grid[x][i] = c;
}

void calc ()
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            switch(grid[i][j])
            {
                case ('R'): ans[0]++; break;
                case ('H'): ans[1]++; break;
                case ('C'): ans[2]++; break;
                case ('P'): ans[3]++; break;
            }
        }
    }
}


int main (void)
{
    int k;
    char name[4] = {'R', 'H', 'C', 'P'};

    scanf ("%d %d %d", &n, &m, &k);

    for (ki = 0; ki < k; ki++)
    {
        char c[30];
        int num;
        //char c;

        getchar();
        scanf ("%s %u", c, &num);

        if (c[0] == 'C')
            dC(num-1, name[ki%4u]);
        else
            dL(num-1, name[ki%4u]);
    }

    for (int i = 0; i < n; i++)
        switch(resl[i].first)
        {
            case ('R'): ans[0] += m; break;
            case ('H'): ans[1] += m; break;
            case ('C'): ans[2] += m; break;
            case ('P'): ans[3] += m; break;
        }

    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (resc[i].second > resl[j].second)
            {
                switch(resl[j].first)
                {
                    case ('R'): ans[0] -= 1; break;
                    case ('H'): ans[1] -= 1; break;
                    case ('C'): ans[2] -= 1; break;
                    case ('P'): ans[3] -= 1; break;
                }
            }
        }
            switch(resc[i].first)
            {
                case ('R'): ans[0] += n; break;
                case ('H'): ans[1] += n; break;
                case ('C'): ans[2] += n; break;
                case ('P'): ans[3] += n; break;
            }


    }


    printf ("R%d H%d C%d P%d\n",
            ans[0],
            ans[1],
            ans[2],
            ans[3]);

    return 0;
}

