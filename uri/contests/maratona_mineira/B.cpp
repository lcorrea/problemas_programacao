#include <bits/stdc++.h>

using namespace std;

void del(char num[20])
{
    for (int i = 0; num[i]; i++)
        if (num[i] == '7')
            num[i] = '0';
}

int main (void)
{
    char numa[20], numb[20], op;

    scanf ("%s %c %s", numa, &op, numb);

    del(numa); del(numb);

    unsigned int a, b, c;

    sscanf (numa, "%u", &a);
    sscanf (numb, "%u", &b);

    char numc[20];

    sprintf (numc, "%d", op == '+' ? a+b : a*b);

    del(numc);

    sscanf (numc, "%u", &c);

    printf ("%u\n", c);

    return 0;
}

