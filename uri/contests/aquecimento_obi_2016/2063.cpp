#include <bits/stdc++.h>

using namespace std;

int gcd (int a, int b) { return b == 0 ? a : gcd(b, a % b); }
int lcm (int a, int b) { return a * (b / gcd(a, b)); }

int main (void)
{
	int n, sec = 1;
	int p[102], t[102];
	bool ok = false;

	cin >> n;

	for (int i = 1; i <= n; i++)
		cin >> p[i];

	int mmc = 1;
	for (int i = 1; i <= n; i++)
	{
		int d = 1, e = p[i];
		while (e != i)
			e = p[e], d++;
		mmc = lcm (mmc, d);
	}

	cout << mmc << endl;

	return 0;
}

