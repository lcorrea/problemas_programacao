#include <bits/stdc++.h>

using namespace std;

int md (int a)
{
	int r = a % 24;

	return r < 0 ? r + 24 : r;
}

int main (void)
{
	int s, t, f;

	cin >> s >> t >> f;

	cout << md (s + t + f) << endl;

	return 0;
}

