#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	string s;

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> s;

		if (s.length() == 3)
		{
			if (s.substr(0, 2) == "OB")
				cout << "OBI";
			else if (s.substr(0, 2) == "UR")
				cout << "URI";
			else 
				cout << s;
		}
		else
			cout << s;

		if (i+1 < n) cout << ' ';
	}
	cout << endl;

	return 0;
}

