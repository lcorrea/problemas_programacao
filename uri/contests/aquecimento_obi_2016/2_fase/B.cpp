#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  pair <float, string> p[3];

  for (int i = 0; i < 3; i++)
      cin >> p[i].first;
  p[0].second = "Otavio";
  p[1].second = "Bruno";
  p[2].second = "Ian";
  

  sort (p, p + 3);

  if (p[0].first == p[1].first)
    puts ("Empate");
  else
    printf ("%s\n", p[0].second.c_str());

  return 0;
}

