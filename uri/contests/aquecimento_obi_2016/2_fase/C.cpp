#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  string msg;
  int cnt =0;

  cin >> msg;

  for (int i = 0; i < msg.length(); i++)
    cnt += msg[i] == '1';

  cout << msg << (cnt % 2 ? '1' : '0') << endl;

  return 0;
}

