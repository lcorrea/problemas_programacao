#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int x, y, n;
  vector <int> ok;

  cin >> x >> y >> n;

  for (int i = 1; i <= n; i++)
  {
    int nx, ny, nt;

    cin >> nx >> ny >> nt;

    if (hypot(x-nx, y-ny) < nt)
      ok.push_back(i);
  }

  if (ok.size())
  {
    cout << ok[0];
    for (int i = 1; i < (int)ok.size(); i++)
      cout << ' ' << ok[i];
    cout << '\n';
  }
  else
    puts ("-1");

  return 0;
}

