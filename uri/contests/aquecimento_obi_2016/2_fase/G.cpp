#include <bits/stdc++.h>

using namespace std;

bitset<90000> bs;
vector <int> primes;

void sieve (void)
{
  bs.set();

  bs[0] = bs[1] = 0;
  for (long long i = 2; i <= 90000; i++)
  {
    if (bs[i])
    {
      for (long long j = i*i; j <= 90000; j+=i)
        bs[j] = 0;

      primes.push_back(i);
    }
  }
}

bool isPrime(int i)
{
  return bs[i];
}

int main (void)
{
  sieve();
  int kg;
  int v = 0;
  long long d = 60000000;

  cin >> kg;

  for (int i = kg, cnt = 0; cnt < 10; i++)
    if (isPrime(i))
      v += i, cnt++;

  cout << v << " km/h\n";

  double h = d / (double) v;
  double dias = h / 24.0;

  cout << (long long) h << " h / " << (long long) dias << " d\n";

  return 0;
}

