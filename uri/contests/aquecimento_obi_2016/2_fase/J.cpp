#include <bits/stdc++.h>

using namespace std;

#define INF 1000000000

set <char> itens;
char tb[200][200];
int zumbis[200][200];
int health = 0;
int n, k, bx, by, potion = 0;

int process (int i, int j)
{
  if (i < 0 or j < 0 or tb[i][j] == 'I')
    return 0;

  if (zumbis[i][j] != -1)
    return zumbis[i][j];

  int dano = 0;
  int ch = tb[i][j];

  if (isdigit(ch) and potion <= 0)
    dano = tb[i][j] - '0';
  else
    potion--;

  if (isalpha(ch) and ch != 'I' and ch != 'F')
  {
    itens.insert(ch);

    if (itens.size() == 5)
    {
      potion = k;
      itens.clear();
    }
  }

  int newx1 = j - 1, newy1 = i, dano1 = INF;
  int newx2 = j, newy2 = i - 1, dano2 = INF;

  if (newx1 >= 0 and newy1 >= 0 and tb[newy1][newx1] != '#')
    dano1 = process(newy1, newx1);

  if (newx2 >= 0 and newy2 >= 0 and tb[newy2][newx2] != '#')
    dano2 = process(newy2, newx2);

  if (dano1 == INF and dano2 == INF)
    return INF;
  else if (dano1 == INF)
    return dano2;
  else if (dano2 == INF)
    return dano1;
  else
    return zumbis[i][j] = dano + min(dano1, dano2);
}

int main (void)
{

  scanf ("%d %d%*c", &n, &k);

  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < n; j++)
    {
      scanf ("%c%*c", &tb[i][j]);

      if (tb[i][j] == 'F')
        by = i, bx = j;
    }
  }
  memset (zumbis, -1, sizeof(zumbis));
  printf ("%d\n", process(by, bx));

  return 0;
}

