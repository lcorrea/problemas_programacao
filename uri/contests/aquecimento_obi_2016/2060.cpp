#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int v[4] = {2, 3, 4, 5};
	int q[4] = {0};

	cin >> n;

	for (int i = 0, a; i < n; i++)
	{
		cin >> a;

		for (int j = 0; j < 4; j++)
			q[j] += ((a%v[j])==0);
	}

	for (int i = 0; i < 4; i++)
		cout << q[i] << " Multiplo(s) de " << v[i] << endl;

	return 0;
}

