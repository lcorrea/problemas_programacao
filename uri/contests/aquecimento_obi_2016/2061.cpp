#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;
	string s;

	cin >> n >> m;

	for (int i = 0; i < m; i++)
	{
		cin >> s;

		if (s[0] == 'f')
			n++;
		else
			n--;
	}

	cout << n << endl;

	return 0;
}

