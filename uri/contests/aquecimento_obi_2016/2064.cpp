#include <bits/stdc++.h>

using namespace std;


int main (void)
{
	int k, m, n;
	int fav[256] = { 0 };
	int qnt[256] = { 0 };
	char alpha[256] = { 0 };
	char sol[256] = { 0 };
	string str, name;

	cin >> k >> m >> n;

	cin >> str;

	for (int i = 0; i < k; i++)
		fav[str[i]] = 1;

	cin >> str; name = str;

	for (int i = 0; i < m; i++)
		qnt[name[i]]++;

	for (char c = 'a'; c <= 'z'; c++)
		sol[c] = alpha[c] = c;

	int v = 0;

	for (char c = 'a'; c <= 'z'; c++)
		v += qnt[c]*fav[alpha[c]];

	for (int i = 0; i < n; i++)
	{
		char l1, l2;

		cin >> l1 >> l2;

		for (char c = 'a'; c <= 'z'; c++)
		{
			if (alpha[c] == l1)
				alpha[c] = l2;
			else
			{
				if (alpha[c]==l2)
					alpha[c] = l1;
			}
		}

		int w = 0;

		for (char c = 'a'; c <= 'z'; c++)
			w += qnt[c]*fav[alpha[c]];

		if (w > v)
		{
			v = w;
			for (char c = 'a'; c <= 'z'; c++)
				sol[c] = alpha[c];
		}
	}

	cout << v << endl;

	for (int i = 0; i < m; i++) cout << sol[name[i]];
	cout << endl;

	return 0;
}

