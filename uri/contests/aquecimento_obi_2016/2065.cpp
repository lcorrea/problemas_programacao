#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;
typedef priority_queue < ii, vector <ii>, greater <ii> > min_heap;

int main (void)
{
	int n, m;
	int cashier[10001];
	queue <int> clients;
	int time = 0;
	min_heap pq;

	scanf ("%d%d", &n, &m);

	for (int i = 0; i < n; i++)
		scanf ("%d", cashier + i);
	for (int i = 0, a; i < m; i++)
	{
		scanf ("%d", &a);
		clients.push(a);
	}

	for (int i = 0; i < n; i++)
	{
		if (!clients.empty())
		{
			int itens = clients.front();

			clients.pop();
			pq.push (ii(time + itens*cashier[i], i));
		}
		else break;
	}

	while (!clients.empty())
	{
		int itens = clients.front();
		int free_cashier = pq.top().second;
		time = pq.top().first;

		pq.pop();
		clients.pop();
		pq.push (ii(time + itens*cashier[free_cashier], free_cashier));
	}

	while (pq.size() > 1) pq.pop();

	printf ("%d\n", pq.top().first);

	return 0;
}

