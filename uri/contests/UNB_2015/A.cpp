#include <bits/stdc++.h>

using namespace std;

typedef struct {
    string jogada;
    string texto;
}Player;

map <string, string> mat;

Player run(Player A, Player B, Player C)
{
    Player empate;

    empate.texto = "Putz vei, o Leo ta demorando muito pra jogar...";
    
    if (mat[A.jogada] == B.jogada and mat[A.jogada] == C.jogada)
        return A;
    else if (mat[B.jogada] == A.jogada and mat[B.jogada] == C.jogada)
        return B;
    else if (mat[C.jogada] == A.jogada and mat[C.jogada] == B.jogada)
        return C;
    else
        return empate;
}

int main (void)
{
    char a[20], b[20], c[20];
    Player A, B, C;
    
    mat["pedra"] = "tesoura";
    mat["papel"] = "pedra";
    mat["tesoura"] = "papel";

    A.texto = "Os atributos dos monstros vao ser inteligencia, sabedoria...";
    B.texto = "Iron Maiden's gonna get you, no matter how far!";
    C.texto = "Urano perdeu algo muito precioso...";

    while (scanf ("%s %s %s", a, b, c) != EOF)
    {
        A.jogada = a; B.jogada = b; C.jogada = c;

        printf ("%s\n", run(A,B,C).texto.c_str());
    }

    return 0;
}

