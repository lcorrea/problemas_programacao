#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long int n,k;

    while (scanf ("%lld %lld", &n, &k) != EOF)
    {
        char str[100010];

        scanf ("%s", str);

        vector <char> pilha;
        int pos[100010], pai[100010];

        memset(pos, 0, sizeof(pos));
        memset(pai, 0, sizeof(pai));

        for (int i = 0; i < n; i++)
        {
            auto it = upper_bound(pilha.begin(), pilha.end(), str[i]);

            int p = it-pilha.begin();

            if (it == pilha.end()) pilha.push_back(str[i]);
            else *it = str[i];

            pos[p] = i;
            if (p==0) pai[i] = -1;
            else pai[i] = pos[p-1];
        }

        int p = pos[pilha.size()-1];
        long long int total = 0;

//        puts (str);
        while(p>=0)
        {
//            putchar (str[p]);
            total += 26 - (str[p] - 'a');
            p=pai[p];
        }
//        putchar ('\n');

        if (total >= k)
            puts ("Aceita");
        else
            puts ("Rejeita");
   }

    return 0;
}

