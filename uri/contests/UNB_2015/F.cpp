#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
    private:
        vector <int> r, p;

    public:
        UnionFind(int n)
        {
            r.assign(n, 0);
            p.assign(n, 0);

            for (int i = 0; i < n; i++)
                p[i] = i;
        }

        int findSet(int i)
        {
            return i == p[i] ? i : p[i] = findSet(p[i]);
        }

        void join(int i, int j)
        {
            i = findSet(i);
            j = findSet(j);

            if (i == j) return;

            if (r[i] > r[j])
                p[j] = i;
            else
            {
                r[j] += r[i] == r[j];
                p[i] = j;
            }
        }
};


int main (void)
{
    int n=0, m=0, q=0;
    int cnt = 0;

    if (scanf ("%d %d %d", &n, &m, &q) == EOF)
        return 0;
 
    while(1)
    {
        UnionFind g(n);

        for (int i = 0; i < m; i++)
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            g.join(a-1, b-1);
        }

        for (int i = 0; i < q; i++)
        {
            int a, b;
            
            scanf ("%d %d", &a, &b);

            if (g.findSet(a-1) == g.findSet(b-1))
                puts ("S");
            else
                puts ("N");
        }

        if (scanf ("%d%d%d", &n, &m, &q) != EOF)
            putchar ('\n');
        else
            break;
    }

    return 0;
}

