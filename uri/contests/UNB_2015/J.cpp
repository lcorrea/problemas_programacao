#include <bits/stdc++.h>

using namespace std;

typedef struct _Trie_ Trie;

struct _Trie_ {
    int words;
    int pre;
    int mSize;
    Trie *child[27];
    bool end;
};

Trie * init()
{
    Trie *ptr = NULL;

    ptr = (Trie *) malloc(sizeof(Trie));

    if (ptr)
    {
        ptr->end = false;
        ptr->words = 1;
        
        for (int i = 0; i < 27; i++)
            ptr->child[i] = NULL;
    }

    return ptr;
}

void insert (Trie *root, const char *str)
{
    int len = strlen(str);
    int index;

    Trie *ptr = root;

    for (int level = 0; level < len; level++)
    {
        index = str[level] - 'a';

        if (!ptr->child[index])
        {
            ptr->child[index] = init();
            ptr->child[index]->mSize = len;
        }

        ptr->words++;
        ptr->mSize = max(ptr->mSize, len);
        /*printf ("level %d [%c] = %d (%d)\n",
                level,
                str[level],
                ptr->child[index]->words,
                ptr->mSize);*/
        ptr = ptr->child[index];
    }

    ptr->end = true;
}

int search(Trie *root, const char *str, int *sz)
{
   int len = strlen (str);
   int index, cnt = -1;
   Trie *ptr = root;

   for (int level = 0; level < len; level++)
   {
       index = str[level] - 'a';

       if (!ptr->child[index])
           break;

/*    printf ("level %d [%c] = %d (%d)\n",
                level,
                str[level],
                ptr->words,
                ptr->mSize);
*/
       cnt = 1;
       ptr = ptr->child[index];
   }

   if (cnt == -1)
       puts ("-1");
   else
       printf ("%d %d\n", ptr->end ? ptr->words : ptr->words - 1, ptr->mSize);

   return (ptr != NULL && ptr->end);
}

int main (void)
{
    int n;

    if (scanf ("%d", &n) == EOF) return 0;
    while (1)
    {
        Trie *root = init();

        root->words = 0;

        while (n--)
        {
            char str[200];

            scanf ("%s", str);

            insert(root, str);
        }

        int q;

        scanf ("%d", &q);

        while (q--)
        {
            char str[200];

            scanf ("%s", str);

            search(root, str, &q);
        }

        if (scanf ("%d", &n) != EOF)
            putchar('\n');
        else
            break;
    }
    
    return 0;
}

