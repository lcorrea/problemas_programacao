#include <bits/stdc++.h>

using namespace std;

bool primes[1000];

void sieve()
{
    memeset(primes, 1, sizeof(primes));

    for (int i = 2; i*i <= n; i++)
        if(primes[i] == true)
            for (int j=i*2; j<=n; j+=i)
                prime[j] = false;
}

int main (void)
{
    int n, m;

    sieve();

    while (scanf ("%d %d", &n, &m) != EOF)
    {
        int area[n];

        for (int i = 0; i < n; i++)
            scanf ("%d", area + i);

        int mat[n][n];

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                mat[i][j] = 1000000007;

        for (int i = 0; i < m; i++)
        {
            int a, b, c;

            scanf ("%d %d %d", &a, &b, &c);

            mat[b-1][a-1] = mat[a-1][b-1] = c;
        }
    }

    return 0;
}

