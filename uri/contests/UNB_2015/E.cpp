#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    for (int T = 1; T <= t; T++)
    {
        char str[50];
        int r, g, b;

        scanf ("%s", str);
        scanf ("%d %d %d", &r, &g, &b);

        printf ("Caso #%d: ", T);
        if (str[0] == 'e')
            printf ("%d\n", (int) (.3*r + .59*g + .11*b));
        else if (str[2] == 'x')
            printf ("%d\n", max(r, max(g, b)));
        else if (str[2] == 'n')
            printf ("%d\n", min(r, min(g, b)));
        else
            printf ("%d\n", (int) (r + g + b)/3);
    }

    return 0;
}

