#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        priority_queue < pair <int, string> > seq;
        char str[50];
        int d;

        for (int i = 0; i < n; i++)
        {
            scanf ("%s %d", str, &d);
            seq.push(make_pair(-d, str));
        }

        printf ("%s", seq.top().second.c_str()); seq.pop();
        while (!seq.empty())
            printf (" %s", seq.top().second.c_str()), seq.pop();
        putchar ('\n');
    }

    return 0;
}

