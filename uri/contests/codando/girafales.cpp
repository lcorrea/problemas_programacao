#include <iostream>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <map>

using namespace std;


int main (void)
{

	int n, m, falses = 0;
	map <string, string> alunos;
	string aluno, assinatura;

	cin >> n;

	while (n)
	{
		falses = 0;
		alunos.clear();

		while (n--)
		{
			cin >> aluno >> assinatura;
			
			alunos[aluno] = assinatura;
		}

		cin >> m;

		while (m--)
		{
			cin >> aluno >> assinatura;
			
			string orig = alunos[aluno];
			int diff = abs(orig.size() - assinatura.size());
		
			for (int i = 0; i < assinatura.size() and i < orig.size() and diff < 2; i++)
			{
				if (orig[i] != assinatura[i])
					diff++;
			}

			if (diff > 1)
				falses++;
		}

		cout << falses << endl;
		cin >> n;
	}

	return 0;
}

