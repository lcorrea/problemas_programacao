#include <iostream>
#include <queue>
#include <cstdio>

using namespace std;

int main (void)
{
	int n, a, sum, t, h;

	cin >> n >> a;

	while (n and a)
	{
		priority_queue <int> cuts;

		sum = 0;

		while (n--)
		{
			cin >> t;
			sum += t;
			cuts.push (t);
		}

		if (a == sum)
			cout << ":D" << endl;
		else if (a > sum)
			cout << "-.-" << endl;
		else
		{
			int result = 0, f = 1;

			h = cuts.top();
			cuts.pop();

			cout << h << endl;

			while (result != a and (sum - result) != a and !cuts.empty() and h > 0)
			{
				cout << "top " << cuts.top() << endl;
				if (cuts.top() == h)
				{
					result += (h - cuts.top())*f;
					h = cuts.top();
					cuts.pop();
					f++;
					cout << "new top " << cuts.top() << endl;
				}
				h--;

				cout << "cutting " << h << endl;
			}

			printf ("%.4f\n", (float) h);
		}

		cin >> n >> a;
	}

	return 0;
}
