#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int main (void)
{
	int qt, n;
	int he, me, hs, ms, he2, me2, hs2, ms2;
	int mt = 0;

	const int tolE = 8*60;
	const int tolE2 = 14*60;
	const int tolS = 12*60;
	const int tolS2 = 18*60;

	scanf ("%d%*c", &qt);

	while (qt--)
	{
		scanf ("%d%*c", &n);

		mt = 0;

		while (n--)
		{
			scanf ("%d%*c%d%*c%d%*c%d%*c%*c%*c%d%*c%d%*c%d%*c%d%*c",
					&he, &me,
					&hs, &ms, 
					&he2, &me2,
					&hs2, &ms2);
			me += he*60;
			ms += hs*60;
			me2 += he2*60;
			ms2 += hs2*60;

			if (abs (tolE - me) <= 5)
				me = tolE;
			if (abs (tolS - ms) <= 5)
				ms = tolS;
			if (abs (tolE2 - me2) <= 5)
				me2 = tolE2;
			if (abs (tolS2 - ms2) <= 5)
				ms2 = tolS2;

			mt += 480 - (ms - me) - (ms2 - me2);
		}

		if (mt > 0)
			putchar ('-');
		else 
			putchar ('+');

		mt = abs (mt);

		printf (" %02d:%02d\n", mt/60, mt%60);
	}
		
	return 0;
}

