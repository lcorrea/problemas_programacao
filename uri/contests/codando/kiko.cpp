#include <iostream>
#include <vector>

using namespace std;

unsigned int gcd_1(unsigned int a, unsigned int b)
{
	unsigned int r = 0;
	while(b != 0)
	{
		r = a%b;
		a = b;
		b = r;
	}

	return a;
}

int main (void)
{
	int n, t;
	int sum = 0, ti;
	vector <int> v;

	cin >> n >> t;

	while (n and t)
	{
		v.assign (n, 1);

		sum = 0;
		while (n--)
		{
			cin >> ti;

			v[ti-1] = 0;

			sum = (sum%t +  ti%t)%t;
		}

		sum = gcd_1 (t - sum, t);

		if (sum > 0 && sum < n && v[sum-1])
			cout << sum;
		else
			cout << "impossivel" << endl;
		cin >> n >> t;
	}
}
