#include <iostream>
#include <cmath>

using namespace std;

int main (void)
{
	int r1, r2, n, cx, cy;
	int px, py;
	int cD, cB, pD, pB;
	int *ptrB, *ptrD;

	cin >> n;

	while (n)
	{
		cin >> cx >> cy >> r1 >> r2;

		n = n*2;

		pD = 0; pB = 0;
		cD = 0; cB = 0;

		for (int i = 0; i < n; i++)
		{
			cin >> px >> py;

			float dist = hypot (px - cx, py - cy);
			
			if (i % 2)
			{
				ptrD = &pD;
				ptrB = &pB;
			}
			else
			{
				ptrD = &cD;
				ptrB = &cB;
			}

			if (dist < r1)
			{
				(*ptrD)++;
			}
			else
			{
				if(dist >= r1 and dist <= r2)
					(*ptrB)++;
			}
		}

		if (cD > pD)
			cout << "C > P";
		else if (pD > cD)
			cout << "P > C";
		else if (cB > pB)
			cout << "C > P";
		else if (pB > cB)
			cout << "P > C";
		else
			cout << "C = P";

		cout << endl;

		cin >> n;
	}

	return  0;
}

