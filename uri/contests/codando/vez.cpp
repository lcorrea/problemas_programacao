#include <iostream>
#include <map>

using namespace std;

int main (void)
{
	string jogador1, jogador2, escolha1, escolha2;
	string v1, v2;
	int n;
	long long unsigned int n1, n2;
	
	cin >> n;

	while (n--)
	{
		cin >> jogador1 >> escolha1 >> jogador2 >> escolha2;

		if (escolha1 == "IMPAR")
		{
			v2 = jogador1;
			v1 = jogador2;
		}
		else
		{
			v2 = jogador2;
			v1 = jogador1;
		}

		cin >> n1 >> n2;

		n1 += n2;

		if (n1 % 2)
			cout << v2;
		else
			cout << v1;

		cout << endl;
		
	}

	return 0;
}

