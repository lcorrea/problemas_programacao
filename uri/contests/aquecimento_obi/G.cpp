#include <iostream>
#include <set>

using namespace std;

int main(void)
{
	set<unsigned int> v;
	unsigned int q, e, si;

	cin >> q >> e;

	while(e--)
	{
		cin >> si;
		v.insert(si);
	}

	while(q--)
	{
		cin >> si;
		if(v.find(si) != v.end())
			cout << '0' << endl;
		else
		{
			cout << '1' << endl;
			v.insert(si);
		}
	}

	return 0;
}

