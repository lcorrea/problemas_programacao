#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	unsigned int a, b;
	register long long unsigned int sum = 0;

	cin >> a >> b;

	if(b < a)
		swap(a, b);
	
	for(register unsigned int i = a; i <= b; i++)
		sum += i;

	cout << sum << endl;

	return 0;
}

