#include <iostream>

using namespace std;

int main(void)
{
	float percent;
	unsigned int q, v, count = 0;

	cin >> q;
	for(unsigned int i = 0; i < q; i++)
	{
		cin >> v;
		if(v == 0)
			count++;
	}
	
	percent = (float) count/q;

	if(percent > 0.500000000)
		cout  << 'Y' << endl;
	else
		cout << 'N' << endl;

	return 0;
}

