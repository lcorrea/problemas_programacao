#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;


int main(void)
{
	vector<unsigned int> livros[5];
	vector<unsigned int>::const_iterator p, m, f, q, b;
	vector<unsigned int> k;

	unsigned int n, v, sum = 0;

	for(unsigned int i = 0; i < 5; i++)
	{
		cin >> n;
		
		while(n--)
		{
			cin >> v;
			livros[i].push_back(v);
		}

		//sort(livros[i].begin(), livros[i].end());
	}

	cin >> n;

	for(p = livros[0].begin(); p != livros[0].end(); p++)
	{
		for(m = livros[1].begin(); m != livros[1].end(); m++)
		{
			for(f = livros[2].begin(); f != livros[2].end(); f++)
			{
				for(q = livros[3].begin(); q != livros[3].end(); q++)
				{
					for(b = livros[4].begin(); b != livros[4].end(); b++)
					{
						k.push_back(*p + *m + *f + *q + *b);	
					}
				}
			}
		}
	}

	sort(k.begin(), k.end());

	vector<unsigned int>::reverse_iterator rit;
	unsigned int i = 0;

	for(rit = k.rbegin(); rit != k.rend() && i < n; ++rit, i++)
		sum += *rit;

	cout << sum << endl;

	return 0;
}

