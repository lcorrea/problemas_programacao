#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main(void)
{
	unsigned int F = 0, L = 0, j;
	unsigned int i = 0, pot = 3;
	string line;

	cin >> line;
	
	vector<unsigned int> M(line.length() - 2, 0);

	F += (line[0] - '0')*pow(10, pot-i);
		
	L += (line[line.length()-1] - '0')*pow(10, pot - i);
	
	for(j = 0; j < M.size(); j++)
		M[j] += (line[j+1] - '0')*pow(10, pot - i);

	for(i = 1; i < 4; i++)
	{
		cin >> line;

		F += (line[0] - '0')*pow(10, pot-i);
		
		L += (line[line.length()-1] - '0')*pow(10, pot - i);

		for(j = 0; j < M.size(); j++)
			M[j] += (line[j+1] - '0')*pow(10, pot - i);
	}

	for(j = 0; j < M.size(); j++)
		cout << (char) ((M[j]*F + L)%257);
	
	cout << endl;

	return 0;
}

