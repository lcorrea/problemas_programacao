#include <bits/stdc++.h>

using namespace std;

int n, X, maxS = -1;
int s[101], v[101];
unsigned int tb[110][1010];
vector <int> sol;

int get(int i, int j, int qnt)
{
    int maxcnt1 = qnt, maxcnt2 = qnt, maxcnt3 = qnt;

    if (i <= 0 or j <= 0)
        return qnt;

    if (tb[i][j] == tb[i-1][j])
        maxcnt1 = get(i-1, j, qnt);

    if (tb[i][j] == v[i] + tb[i-1][j-s[i]])
        maxcnt2 = get(i-1, j-s[i], qnt+1);

    if (maxcnt2 > maxcnt1)// or maxcnt3 > maxcnt1)
        sol.push_back(i);

    //printf ("%d %d\t%d %d\n", i, j, maxcnt1, maxcnt2);

    return max(maxcnt1, maxcnt2);//max(maxcnt2, maxcnt3));
}

int main (void)
{
    set <int> ans;
    memset(tb, 0, sizeof(tb));

    scanf ("%d %d", &n, &X);

    for (int i = 1; i<=n; i++)
    {
        scanf ("%d %d", v+i, s+i);
        if (!s[i])
            ans.insert(i);
    }

    for (int i = 1; i <= n; i++)
        for (int j = 0; j <= X; j++)
            if (j >= s[i])
                tb[i][j] = max(v[i] + tb[i-1][j-s[i]], tb[i-1][j]);
            else
                tb[i][j] = tb[i-1][j];

    //for (int i = 0; i <= n; i++)
    //    for (int j = 0; j <= X; j++)
    //        printf ("%5d%c", tb[i][j], j == X ? '\n' : ' ');

    //printf ("%d\n", get(n, X, 0));

    //for (auto i : sol)
    //    printf ("%d ", i);
    //putchar('\n');

    get(n, X, 0);

    if (!sol.empty())
    {
        X -= s[sol.back()];
        ans.insert(sol.back()); sol.pop_back();
    }

    while (!sol.empty())
    {
        if (s[sol.back()] <= X and ans.find(sol.back()) == ans.end())
            //printf (" %d", sol.back()),
            ans.insert(sol.back()),
                X -= s[sol.back()],
                sol.pop_back();
        else if (s[sol.back()] <= X)
            sol.pop_back();
        else
            break;
    }

    auto it = ans.begin();
    for (int i = 0; i < (int) ans.size(); i++)
        printf ("%d%c", *it++, (i+1)==ans.size() ? '\n' : ' '); 

    return 0;
}

