#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int a, b;

	cin >> a >> b;

	if(b < a)
		swap(a,b);

	while(a <= b)
	{
		cout << a << ' ';
		a++;
	}

	cout << endl;

	return 0;
}

