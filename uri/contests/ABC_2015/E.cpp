#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{

	unsigned int n, caset = 0, c, max;
	char e;

	cin >> n;

	
	while(n--)
	{
		caset++;
		max = 0;
		e = ' ';

		while(e == ' ')
		{
			scanf("%u%c", &c, &e);

			if(c > max)
				max = c;
		}
		
		cout << "Case " << caset << ": ";
	
		cout << max << endl;
	}

	return 0;
}

