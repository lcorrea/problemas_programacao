#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>

using namespace std;

int main(void)
{
	unsigned int x, y;

	cin >> x >> y;
	
	cout << x;
	for(unsigned int j = 1; j < y; j++)
	{
		cout << ' ';
		cout << x;
		for(unsigned int i = 0; i < j; i++)
			cout << '0';
	}

	cout << endl;
	
	return 0;
}
