#include <iostream>

using namespace std;

int main(void)
{
	unsigned int x, y, cnt = 1;
	float sum = 0, l = 0;
	cin >> x >> y;

	sum += x;

	while(sum < y)
	{
		cnt++;
		sum = x + sum*0.9;
	}

	cout << cnt << endl;

	return 0;
}
