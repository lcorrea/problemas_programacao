#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(void)
{
	int price = 0, n;
	vector<int> prices;
	vector<int>::const_iterator it;
	vector<int> profit;

	cin >> n;

	while(n--)
	{
		cin >> price;
		prices.push_back(price);
	}

	for(unsigned int i = 1; i < prices.size()-1; i++)
	{
		it = max_element(prices.begin()+i, prices.end());

		profit.push_back(*it - prices[i-1]);
	}

	
	price = *max_element(profit.begin(), profit.end());

	if(price < 0)
		price = 0;
	
	cout << price << endl;

	return 0;
}

