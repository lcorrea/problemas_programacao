#include <iostream>

using namespace std;

int main(void)
{
	unsigned int x, y, cnt = 0;
	float sum = 0, l = 0;
	cin >> x >> y;

	sum += x/2;

	while(sum < y)
	{
		cnt++;
		sum += x/2.0;
	}

	cout << cnt << endl;

	return 0;
}
