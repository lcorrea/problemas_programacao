#include <iostream>
#include <algorithm>
#include <deque>
#include <utility>

using namespace std;

int main(void)
{
	unsigned int instancia = 0, m, l, i;
	char c;
	deque<char> list, listB;

	for(i = 0; i < 26; i++)
		listB.push_back('A' + i);

	cin >> m;

	while(m)
	{
		instancia++;

		list = listB;

		cout << "Instancia " << instancia << '\n';
		while(m--)
		{
			cin >> l;

			c = list[l-1];

			cout << c;

			list.erase(list.begin()+(l-1));
			list.push_front(c);

		}

		cout << '\n' << endl;

		cin >> m;
	}

	return 0;
}

