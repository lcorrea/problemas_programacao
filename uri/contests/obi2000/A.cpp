#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int v, Case = 1;
  const int notes[4] = {50, 10, 5, 1};

  while (scanf ("%d", &v), v)
  {
    int used[4] = {0};
    int i = 0;

    while (v)
    {
      if (v >= notes[i])
      {
        used[i] = v / notes[i];
        v = v % notes[i];
      }

      i++;
    }

    printf ("Teste %d\n", Case++);
    printf ("%d", used[0]);
    for (i = 1; i < 4; i++)
      printf (" %d", used[i]);
    puts ("\n");

  }

  return 0;
}

