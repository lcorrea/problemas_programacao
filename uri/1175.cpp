#include <iostream>
#include <algorithm>

using namespace std;

int main (void)
{
	int v[20];

	for (unsigned int i = 0; i < 20; i++)
		cin >> v[i];

	reverse(v, v + 20);

	for (unsigned int i = 0; i < 20; i++)
		cout << "N[" << i << "] = " << v[i] << endl;

	return 0;
}

