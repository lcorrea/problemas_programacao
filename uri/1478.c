/*
	problem 1478
	square matrix II
	by lucas correa
	time: 01:10:21

*/
#include <stdio.h>

int main(void)
{
	unsigned int n, i = 0, j = 0;
	int value = 1, inc = -1;
	
	scanf("%u%*c", &n);
	while(n)
	{
		printf("%3u", 1);		//----------
		for(j = 1; j < n ; j++)		// linha 1
			printf(" %3u", j + 1);	//
		putchar('\n');			//-----------	

		for(i=1; i < n; i++)
		{	
			printf("%3u", i+1);

			for(j = 1, value = i; value >= 1; value--, j++)
				printf(" %3u", value);

			for(value=2; j < n; j++)
				printf(" %3u", value++);

			putchar('\n');
		}
			
		putchar('\n');		
		scanf("%u%*c", &n);
	}

	return 0;
}
