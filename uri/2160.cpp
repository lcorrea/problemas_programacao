#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  char str[1000], c;
  int i = 0;

  while ((c = getchar()) != '\n' and c != EOF)
    str[i++] = c;
  str[i] = '\0';

  puts (i <= 80 ? "YES" : "NO");

  return 0;
}

