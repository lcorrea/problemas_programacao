#include <iostream>
#include <cmath>
using namespace std;

int main (void)
{
	unsigned int n, m;

	cin >> n;

	while (n--)
	{
		cin >> m;
		m++;

		cout << m/2 << endl;
	}

	return 0;

}

