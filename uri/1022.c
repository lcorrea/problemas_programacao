#include <stdio.h>
#include <stdlib.h>

int gcd(int a, int b) { return b == 0 ? a : gcd (b, a % b); }

int main (void)
{
	int n1, n2, d1, d2, N1, D1;
	char op;
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		scanf ("%d %*c %d %c %d %*c %d%*c", &n1, &d1, &op, &n2, &d2);
		switch (op)
		{
			case ('+'):
				N1 = n1*d2 + n2*d1;
				D1 = d1*d2;
				break;
			case ('-'):
				N1 = n1*d2 - n2*d1;
				D1 = d1*d2;
				break;
			case ('*'):
				N1 = n1*n2;
				D1 = d1*d2;
				break;
			case ('/'):
				N1 = n1*d2;
				D1 = n2*d1;
				break;
		}
		int aux = gcd (N1, D1);

		if (aux<0)
			printf ("%d/%d = -%d/%d\n", N1, D1, N1/aux, -D1/aux);
		else
			printf ("%d/%d = %d/%d\n", N1, D1, N1/aux, D1/aux);
	}

	return 0;
}

