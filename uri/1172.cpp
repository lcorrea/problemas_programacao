#include <iostream>

using namespace std;

int main (void)
{
	int n;

	for (unsigned int i = 0; i < 10; i++)
	{
		cin >> n;

		cout << "X[" << i << "] = ";
		if(n > 0)
			cout << n;
		else
			cout << 1;
		cout << endl;
	}

	return 0;
}

