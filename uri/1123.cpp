#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

int main (void)
{
	int n, m, c, K;

	while (scanf ("%d %d %d %d", &n, &m, &c, &K), n and m and c and K)
	{
		vector < vector < pair <int, int> > > graph(n);
		vector <int> costRoute(n, 0);
		int mincost = INF;
		int adj[251][251] = {0};
		int adj_aux[251][251] = {0};

		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				adj_aux[i][j] = adj[i][j] = INF;

		for (int i = 0; i < m; i++)
		{
			int u, v, p;

			scanf ("%d %d %d", &u, &v, &p);

			adj_aux[u][v] = adj_aux[v][u] = p;
			
			if (u < c and v < c) continue;

			if (u >= c and v >= c)
				adj[u][v] = adj[v][u] = p;
			else
				graph[u].push_back (make_pair(v, p)), graph[v].push_back (make_pair(u, p));
		}

		for (int i = 1; i < c; i++)
			costRoute[i] = costRoute[i-1] + adj_aux[i][i-1];

		adj[K][K] = 0;

		for (int k = c; k < n; k++)
			for (int i = c; i < n; i++)
				for (int j = c; j < n; j++)
					adj[i][j] = min (adj[i][j], adj[i][k] + adj[k][j]);

		for (int i = 0; i < c; i++)
		{
			for (int j = 0, sz = graph[i].size(); j < sz; j++)
			{
				int u = graph[i][j].first;
				int p = adj[K][u] + graph[i][j].second + costRoute[c-1] - costRoute[i];

				mincost = min (mincost, p);
			}
		}

		printf ("%d\n", mincost);
	}

	return 0;
}

