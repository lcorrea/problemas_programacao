/*
	problem 1080
	highest and position
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	unsigned int num = 0, maxnum = 0, pos = 0, i = 0;
	
	
	for(;i < 100; i++)
	{
		scanf("%d%*c", &num);

		if(num > maxnum)
		{
			pos = i + 1;
			maxnum = num;
		}
	}

	printf("%d\n%d\n", maxnum, pos);

	return 0;
}
