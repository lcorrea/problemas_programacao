#include <bits/stdc++.h>

using namespace std;

typedef struct node
{
    int notas[9];
    int maxfreq;
}Node;

class ST
{
    private:
        vector <Node> st;
        vector <int> v, lazy;
        int n;

        Node add(Node a, Node b)
        {
            Node r;

            r.notas[0] = a.notas[0] + b.notas[0];
            r.maxfreq = 0;
            for (int i = 1; i < 9; i++)
            {
                r.notas[i] = a.notas[i] + b.notas[i];

                if (r.notas[r.maxfreq] <= r.notas[i])
                    r.maxfreq = i;
            }

            return r;
        }

        Node change(Node a, int f)
        {
            Node r;
            
            r.maxfreq = (a.maxfreq + f)%9;
            for (int i = 0; i < 9; i++)
                r.notas[(i+f)%9] = a.notas[i];
            
            return r;
        }

        void build (int p, int l, int r)
        {
            if (l == r)
            {
                for (int i = 0; i < 9; i++) st[p].notas[i] = 0;
                
                st[p].notas[v[l]] = 1;
                st[p].maxfreq = v[l];

                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            build (left, l, mid);
            build (right, mid+1, r);

            st[p] = add(st[left], st[right]);
        }

        void check (int p, int l, int r)
        {
            if (lazy[p])
            {
                st[p] = change(st[p], lazy[p]);

                if (l != r)
                {
                    int left = p << 1;
                    int right = left | 1;

                    lazy[left] += lazy[p];
                    lazy[right] += lazy[p];
                }
                else
                    v[l] = (v[l] + lazy[p]) % 9;

                lazy[p] = 0;
            }
        }

        Node query (int p, int l, int r, int i, int j)
        {
            check(p, l, r);

            if (i > r or j < l)
            {
                Node r;
                memset (&r, 0, sizeof(Node));
                return r;
            }

            if (i <= l and j >= r) return st[p];

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            Node a = query (left, l, mid, i, j);
            Node b = query (right, mid+1, r, i, j);

            return add(a, b);
        }

        void update(int p, int l, int r, int i, int j, int val)
        {
            check(p, l, r);

            if (i > r or j < l) return;

            if (i <= l and j >= r)
            {
                st[p] = change(st[p], val);

                if (l != r)
                {
                    int left = p << 1;
                    int right = left | 1;

                    lazy[left] += val;
                    lazy[right] += val;
                }
                else
                    v[l] = (v[l] + val) % 9;

                return;
            }
            
            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid+1, r, i, j, val);

            st[p] = add(st[left], st[right]);
        }

    public:
        ST(int _n)
        {
            n = _n;

            st.resize(n << 2);
            lazy.assign(n << 2, 0);
            v.assign(n, 1);

            build (1, 0, n-1);
        }

        void update(int i, int j, int val)
        {
            update(1, 0, n-1, i, j, val);
        }

        int query (int i, int j)
        {
            Node interval = query(1, 0, n-1, i, j);

            return interval.maxfreq;
        }

        void show()
        {
            for (int i = 0; i < n; i++)
                printf ("%d\n", query(i, i));
        }

};

int main (void)
{
    int n, q;

    scanf ("%d %d", &n, &q);

    ST tree(n);
    for (int i = 0, a, b, f; i < q; i++)
    {
        scanf ("%d %d", &a, &b);

        f = tree.query(a, b);
        
        tree.update(a, b, f);
    }
    
    tree.show();

    return 0;
}

