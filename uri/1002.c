#include <stdio.h>
#define PI 3.14159
int main(void)
{
	double r;
	while(scanf("%lf", &r) != EOF)
		printf("A=%.4f\n", PI*r*r);
	return 0;
}

