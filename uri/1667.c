#include <stdio.h>
#include <string.h>

int main (void)
{
	char word[100];
	int chline = 0;

	while (scanf ("%s", word) != EOF)
	{
		int len = strlen (word);

		if (word[0] == '<')
		{
			if (word[1] == 'b')
				putchar ('\n');
			else
			{
				int i;

				if (chline != 0)
					putchar('\n');
				
				for (i = 0; i < 80; i++)
					putchar ('-');
				putchar ('\n');
			}

			chline = 0;
		}
		else if (len + chline <= 80 - (chline!=0))
			printf (chline ? " %s" : "%s", word), chline = chline ? chline + len + 1 : len;
		else
			printf ("\n%s", word), chline = len;
	}

	if (word[0] != '<')
		putchar('\n');

	return 0;
}

