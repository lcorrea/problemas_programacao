#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, q;

    while (scanf ("%d %d", &n, &q) != EOF)
    {
        int v[n];

        for (int i = 0; i < n; i++)
            scanf ("%d", v + i);

        sort(v, v + n);

        for (int i = 0, a; i < q; i++)
        {
            scanf ("%d", &a);

            printf ("%d\n", v[n-a]);
        }
    }

    return 0;
}

