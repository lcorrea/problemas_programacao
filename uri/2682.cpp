#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int x, last;

    scanf ("%d", &last);

    while (EOF != scanf ("%d", &x))
        if (x > last)
            last = x;
        else
        {
            while (EOF != scanf ("%*d"));
            break;
        }

    printf ("%d\n", ++last);

    return 0;
}

