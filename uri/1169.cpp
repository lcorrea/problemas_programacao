/*
	problem 1169
	grains in a chess board
	by lucas correa
	time: 00:21:55
*/
#include <iostream>
#include <cstdio>

using namespace std;

#define GRAINS_IN_ONE_KG 12000ULL

int main(void)
{
	unsigned short int n, x;
	unsigned long long int grains = 0;

	cin >> n;
	
	while(n--)
	{
		cin >> x;
		
		for(unsigned short int i = 0; i < x; i++)
			grains += (1ULL << i);

		grains /= GRAINS_IN_ONE_KG;

		printf("%llu kg\n", grains);

		grains = 0;

	}

	return 0;
}

