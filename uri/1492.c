#include <stdio.h>

int main (void)
{
	unsigned long long int uns, total;
	unsigned long long int a, b, c;

	while (EOF != scanf ("%llu %llu", &a, &b))
	{
		total = 0;
		while (a <= b)
		{
			c = a;
			uns = 1;
			while ((c = c & (c-1))) uns++;
			a++;
			total += uns;
		}
		printf ("%llu\n", total);
	}

	return 0;
}
