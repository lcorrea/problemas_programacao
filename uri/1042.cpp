#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int a[3], b[3], i = 0, *m, j;

	cin >> a[0] >> a[1] >> a[2];

	b[0] = a[0];
	b[1] = a[1];
	b[2] = a[2];

	//selection sort
	for(i = 0; i < 2; i++)
	{
		m = a + i;
	
		for(j = i+1; j < 3;j++)
		{
			if(a[j] < *m)
				m = a + j;
		}

		swap(a[i], *m);
	}

	/*	insertion sort
	for(i = 1; i < 3; i++)
	{
		min = a + i;
		j = i - 1;
		while(j && a[j] > *min)
		{
			a[j] = a[j+1];
			j--;
		}

		min = a + j + 1;
	}
	*/

	cout << a[0] << '\n' << a[1] << '\n' << a[2] << '\n' << endl;
	cout << b[0] << '\n' << b[1] << '\n' << b[2] << endl;
			
	return 0;
}

