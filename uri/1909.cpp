// pode melhorar utilizando set
//
#include <cstdio>
#include <algorithm>

using namespace std;

int gcd (int a, int b) { return b == 0 ? a : gcd (b, a % b); }
int lcm (int a, int b) { return a * (b / gcd (a, b)); }

int main (void)
{
	int n, t;
	int tempos[110];

	while (scanf ("%d %d", &n, &t), n or t)
	{
		bool div = true;
		int mmc = 1;

		for (int i = 0; i < n; i++)
			scanf ("%d", tempos + i), div = (t % tempos[i] == 0);

		if (!div)
		{
			puts("impossivel");
			continue;
		}

		sort (tempos, tempos + n);

		mmc = tempos[0];

		for (int i = 1; i < n; i++)
			mmc = lcm (mmc, tempos[i]);

		int resp = 2;

		while ((binary_search(tempos, tempos + n, resp) or lcm (resp, mmc) != t) and resp <= t)
			resp++;

		if (resp > t)
			puts("impossivel");
		else
			printf ("%d\n", resp);
	}

	return 0;
}

