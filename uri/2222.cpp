#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        bool x[61][10001] = {0};

        int n, q;

        scanf ("%d", &n);

        for (int i = 1; i <= n; i++)
        {
            int tn;

            scanf ("%d", &tn);

            for (int j = 0, e; j < tn; j++)
                scanf ("%d", &e), x[e][i] = true;
        }

        scanf ("%d", &q);

        for (int i = 0, op, a, b; i < q; i++)
        {
            scanf ("%d %d %d", &op, &a, &b);

            int cnt = 0;

            if (op == 1)
                for (int j = 1; j <= 60; j++)
                    cnt += x[j][a] and x[j][b];
            else
                for (int j = 1; j <= 60; j++)
                    cnt += x[j][a] or x[j][b];

            printf ("%d\n", cnt);
        }
    }

    return 0;
}

