#include <iostream>

using namespace std;

int main (void)
{
	int x, sum = 0;
	
	cin >> x;

	while (x)
	{
		if (x % 2)
			x++;

		for (int a = 0; a < 5; a++, x += 2)
			sum += x;

		cout << sum << endl;

		sum = 0;

		cin >> x;
	}

	return 0;
}

