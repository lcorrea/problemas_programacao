#include <bits/stdc++.h>

using namespace std;

bool cmp (pair <int, string> a, pair <int, string> b)
{
    return a.first < b.first;
}

int main (void)
{
    int n, t;
    vector < pair <int, string> > alunos;
    vector < vector <string> > times;

    cin >> n >> t;

    alunos.resize(n);
    times.resize(t);

    for (int i = 0; i < n; i++)
        cin >> alunos[i].second >> alunos[i].first;

    sort (alunos.begin(), alunos.end(), cmp);

    while (!alunos.empty())
        for (int i = 0; !alunos.empty() and i < t; i++)
            times[i].push_back (alunos.back().second), alunos.pop_back();

    for (int i = 0; i < t; i++)
    {
        cout << "Time " << (i + 1) << endl;

        sort (times[i].begin(), times[i].end());

        for (int a = 0; a < (int) times[i].size(); a++)
            cout << times[i][a] << endl;

        cout << endl;
    }

    return 0;
}

