/*
	problem 1038
	snack
	by lucas correa

*/
#include <stdio.h>

int main(void)
{
	float price[5] = { 4, 4.5, 5, 2, 1.5};
	unsigned int qnt = 0, i = 0;

	scanf("%u %u%*c", &i ,&qnt);

	printf("Total: R$ %.2f\n", qnt*price[i-1]);

	return 0;
}
