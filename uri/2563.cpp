#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    while (scanf ("%d %d", &n, &k) != EOF)
    {
        int p[n+1], d[n+1], tb[n+1][k+1];

        for (int i = 1; i <= n; i++)
            scanf ("%d", d + i);

        for (int i = 1; i <= n; i++)
            scanf ("%d", p + i);

        memset(tb, 0, sizeof(tb));
        
        //run knapsack
        for (int i = 1; i <= n; i++)
            for (int j = 0; j <= k; j++)
                if (p[i] <= j)
                    tb[i][j] = max(tb[i-1][j], tb[i-1][j-p[i]] + d[i]);
                else
                    tb[i][j] = tb[i-1][j];

        printf ("%d\n", tb[n][k]);
    }

    return 0;
}

