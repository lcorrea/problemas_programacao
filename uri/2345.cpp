#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int v[4];
    int diff = INT_MAX;
    scanf ("%d %d %d %d", v, v+1, v+2, v+3);

    do
    {
        diff = min(diff, abs(v[0]+v[1] - (v[2]+v[3])));
    }while (next_permutation(v, v+4));

    printf ("%d\n", diff);

    return 0;
}

