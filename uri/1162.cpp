#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d", &n);

	while (n--)
	{
		int l, swp = 0;
		int train[100];

		scanf ("%d", &l);

		for (int i = 1, p; i <= l; i++)
			scanf ("%d", &p), train[p] = i;

		for (int i = 1; i <= l; i++)
		{
			if (train[i] == i) continue;

			int p = train[i];
			train[i] = i;

			for (int j = i+1; j <= l; j++)
				if (train[j] < p) train[j]++, swp++;
		}

		printf ("Optimal train swapping takes %d swaps.\n", swp);
	}

	return 0;
}

