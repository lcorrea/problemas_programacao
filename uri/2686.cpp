#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    double n;
    const char horario[][25] = {
        "Bom Dia!!",
        "Boa Tarde!!",
        "Boa Noite!!",
        "De Madrugada!!"};

    while (scanf("%lf", &n) != EOF) {

        int qd = (((int)n) / 90) % 4;

        if (n <= 360) {

            printf("%s\n",horario[qd]);

            double distance = n >= 270.0 ? n - 270.0 : n + 90.0;
            double total = 240.0 * distance;

            int hr = total / 3600;
            int min = (total - hr*3600) / 60;
            int sec = total - hr*3600 - min*60;

            printf("%02d:", hr);
            printf("%02d:", min);
            printf("%02d\n", sec);
        }
    }

    return 0;
}

