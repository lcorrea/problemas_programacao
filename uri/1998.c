#include <stdio.h>

int total = 0;
int boxes[510];

void distribui (int i)
{
	total--;
	boxes[i] = 0;
	for (--i; i >= 1; i--)
	{
		boxes[i]++;
		if (i == boxes[i]) distribui (i);
	}
}

int main (void)
{
	int n, i;

	while (scanf ("%d", &n), n > 0)
	{
		total = 0;
		
		for (i=1; i <= n; i++)
		{
			scanf ("%d", boxes + i);
			total += boxes[i];

			if (i == boxes[i]) distribui (i);
		}

		if (total) puts ("N");
		else puts ("S");
	}

	return 0;
}

