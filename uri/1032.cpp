#include <bits/stdc++.h>

using namespace std;

bitset <1000000> bs;
vector <int> primes;

void sieve (void)
{
	bs.set();

	bs[0] = bs[1] = 0;

	for (long long i = 2; i <= 100000; i++)
	{
		if (bs[i])
		{
			for (long long j = i*i; j <= 100000; j += i)
				bs[j] = 0;
			primes.push_back((int) i);
		}
	}
}

int survivor(int n, int k)
{
	if (n == 1)
		return 0;

	return (survivor(n-1, k+1) + primes[k])%n;
}

int main (void)
{
	int n;

	sieve();

	while (scanf ("%d", &n), n)
		printf ("%d\n", survivor(n, 0) + 1);

	return 0;
}

