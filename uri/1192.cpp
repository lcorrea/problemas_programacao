#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    while (n--)
    {
        char line[4];
        int d1, d2;

        scanf ("%s", line);

        d1 = line[0] - '0';
        d2 = line[2] - '0';

        if (line[0] == line[2])
            printf ("%d\n", d1*d2);
        else if (line[1] >= 'a')
            printf ("%d\n", d1 + d2);
        else 
            printf ("%d\n", d2 - d1);
    }

	return 0;
}

