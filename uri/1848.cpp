#include <iostream>

using namespace std;

int main (void)
{
	unsigned int number = 0, k = 3;
	string line;

	while (k--)
	{
		number = 0;

		while (1)
		{
			getline(cin, line);

			if (line[0] == '-' or line[0] == '*')
			{
				int sum = 0;

				for (unsigned int bit = 0; bit < 3; bit++)
					sum |= (line[2 - bit] == '*')?((1 << bit)):(0);

				number += sum;
			}
			else if (line[0] == 'c')
			{
				break;
			}

		}

		cout << number << endl;
	}

	return 0;
}

