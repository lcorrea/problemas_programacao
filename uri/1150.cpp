#include <iostream>

using namespace std;

int main (void)
{
	int x, z, d = 1;

	cin >> x >> z;

	while (z <= x)
		cin >> z;

	for(int c = x+1; x <= z; c++, d++)
		x += c;
	
	cout << d << endl;

	return 0;
}

