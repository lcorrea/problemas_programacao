#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        int cnt = 0;

        for (int i = 0, a ; i < n; i++)
            scanf ("%d", &a), cnt += a;

        if (2/3.0 <= cnt/(double)n)
            puts ("impeachment");
        else
            puts ("acusacao arquivada");
    }

    return 0;
}

