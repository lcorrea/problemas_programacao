#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    for line in stdin.readlines():
        ch_open = 0
        ok = True
        for ch in line:
            if (ch == ')'):
                if (ch_open):
                    ch_open -= 1
                    continue
                ok = False
                break
            elif (ch == '('):
                ch_open += 1

        ok = True if (ok and ch_open == 0) else False

        stdout.write("correct\n" if ok else "incorrect\n")

if __name__ == "__main__":
    main()

