#include <cstdio>
#include <cctype>

using namespace std;

int main (void)
{
	bool print = true;
	char c, lastc = '?';
	const char F = 'F', f = 'f';

	while (scanf("%c", &c) != EOF)
	{
		switch (c)
		{
			case ('S'):
			case ('V'):
			case ('J'):
			case ('P'):
			case ('B'):
			case ('X'):
			case ('Z'):
			case ('F'):
				if (toupper(lastc) == F)
					print = false;
				else
				{
					c = F;
					lastc = c;
				}
			break;
			case ('s'):
			case ('v'):
			case ('j'):
			case ('p'):
			case ('b'):
			case ('x'):
			case ('f'):
			case ('z'):
				if (toupper(lastc) == F)
					print = false;
				else
				{
					c = f;
					lastc = c;
				}
			break;
			default:
				print = true;
				lastc = c;
			break;
		}

		if (print)
			putchar(c);

	}

	return 0;
}

