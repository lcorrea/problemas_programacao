#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int t, m;

  while (scanf ("%d%d", &t, &m), t)
  {
    int d = m * 3;
    int total = 0;

    for (int i = 0, p; i < t; i++)
    {
      scanf ("%*s %d", &p);

      total += p;
    }

    printf ("%d\n", d - total);
  }

  return 0;
}

