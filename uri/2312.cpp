#include <bits/stdc++.h>

using namespace std;

typedef struct 
{
    int ouro, prata, bronze;
    string nome;
}Pais;

bool cmp (Pais a, Pais b)
{
    if (a.ouro == b.ouro)
        if (a.prata == b.prata)
            if (a.bronze == b.bronze)
                return a.nome < b.nome;
            else
                return a.bronze > b.bronze;
        else
            return a.prata > b.prata;

    return a.ouro > b.ouro;
}

int main ()
{
    int n;

    scanf ("%d", &n);

    Pais Rank[n];

    for (int i = 0; i < n; i++)
    {
        char nome[110];

        scanf ("%s %d %d %d", nome, &Rank[i].ouro,
                &Rank[i].prata, &Rank[i].bronze);
        Rank[i].nome = nome;
    }

    sort (Rank, Rank+n, cmp);

    for (int i = 0; i < n; i++)
        printf ("%s %d %d %d\n",
                Rank[i].nome.c_str(),
                Rank[i].ouro,
                Rank[i].prata,
                Rank[i].bronze);

    return 0;
}

