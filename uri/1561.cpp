#include <bits/stdc++.h>

using namespace std;

#define isbitset(a, n) (n & (1 << a))
#define printbit(a, n) ((isbitset(a, n)) ? ('o') : (' '))

int main (void)
{
	int h, m;

	while (scanf ("%d:%d", &h, &m) != EOF)
	{

		printf (" ____________________________________________\n"
			"|                                            |\n"	
			"|    ____________________________________    |_\n"
			"|   |                                    |   |_)\n"
			"|   |   8         4         2         1  |   |\n"
			"|   |                                    |   |\n"
			"|   |   %c         %c         %c         %c  |   |\n"
		        "|   |                                    |   |\n"
			"|   |                                    |   |\n"
			"|   |   %c     %c     %c     %c     %c     %c  |   |\n"
			"|   |                                    |   |\n"
			"|   |   32    16    8     4     2     1  |   |_\n"
			"|   |____________________________________|   |_)\n"
			"|                                            |\n"
			"|____________________________________________|\n"
			 , printbit(3, h), printbit(2, h), printbit(1, h), printbit(0, h)
			 , printbit(5, m), printbit(4, m), printbit(3, m), printbit(2, m)
			 , printbit(1, m), printbit(0, m));

		putchar ('\n');
	}

	return 0;
}

