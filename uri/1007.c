/*
	problem 1007
	diference
	by lucas correa
*/
#include <stdio.h>

int main(void)
{

	int a = 0, b = 0, dif = 0;

	while(scanf("%d%*c", &a) != EOF)
	{
		dif = a;
		scanf("%d%*c", &a);
		dif *= a;
		scanf("%d%*c", &a);
		scanf("%d%*c", &b);
		dif -= a*b;
		printf("DIFERENCA = %d\n", dif);
	}

	return 0;
}
