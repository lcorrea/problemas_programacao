/*
	problem 1070
	six odd numbers
	by lucas correa
*/

#include <iostream>

using namespace std;

int main(void)
{
	unsigned int x;

	cin >> x;

	if(x % 2 == 0)
		x++;

	for (unsigned int i = 0; i < 6; i++, x += 2)
		cout << x << endl;
	

	return 0;
}

