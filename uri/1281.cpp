#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    while (n--)
    {
        map <string, double> price;
        double v = 0;
        int itens, list;

        scanf ("%d", &itens);

        for (int i = 0; i < itens; i++)
        {
            char item[51];
            double cost;

            scanf ("%s %lf", item, &cost);
            price[item] = cost;
        }

        scanf ("%d", &list);

        for (int i = 0; i < list; i++)
        {
            char item[51];
            int qnt;

            scanf ("%s %d", item, &qnt);

            v += price[item] * qnt;
        }

        printf ("R$ %.02lf\n", v);
    }

    return 0;
}

