#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;

  while (scanf ("%d", &n), n)
  {
    int total = 0;
    int t = 0;
    int start;

    scanf ("%d", &start);
    t = start + 10;

    for (int i = 1, now; i < n; i++)
    {
      scanf ("%d", &now);

      if (now >= t)
      {
        total += t - start;
        start = now;
      }

      t = now + 10;
    }

    total += t - start;

    printf ("%d\n", total);
  }

  return 0;
}

