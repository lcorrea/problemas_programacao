#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout
    
    n = int(stdin.readline())
    fmt_out = lambda i: str(i) + '\n'
    ans = (i for i in range(2, 10000) if (i % n == 2))
    ans = map(fmt_out, numbers)

    for i in ans:
        stdout.write(i)

if __name__ == "__main__":
    main()

