#include <stdio.h>
#include <stdlib.h>

int main (void)
{
	int n, t, l, c, i;
	int scoreA=0, scoreB=0;
	scanf ("%d %d %d", &n, &t, &l);

	for (i=1; i < n; i++)
	{
		scanf ("%d", &c);
		
		int score = abs(t-c);
		int *vez = (i%2)?(&scoreA):(&scoreB);

		if (score <= l) *vez += score, t = c;
	}

	printf ("%d %d\n", scoreA, scoreB);

	return 0;
}

