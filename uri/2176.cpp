#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string S;
    int cnt = 0;

    cin >> S;

    for (int i=0; S[i]; i++)
        cnt += S[i] == '1';

    printf ("%s%c\n", S.c_str(), cnt % 2 ? '1' : '0');

    return 0;
}

