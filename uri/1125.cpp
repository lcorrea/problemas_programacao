#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int g, p;

	while (cin >> g >> p, g and p)
	{
		int vp[g][100];
		
		for (int i = 0; i < g; i++)
		{
			int ps;
			for (int j = 0; j < p; j++)
				cin >> ps, vp[i][ps-1] = j;
		}

		int sys;

		cin >> sys;

		for (int i = 0; i < sys; i++)
		{
			int qnt;
			cin >> qnt;

			int sys_s[qnt];

			for (int j = 0; j < qnt; j++)
				cin >> sys_s[j];

			int score[100] = {0};
			int wc = 1;

			for (int j = 0; j < g; j++)
				for (int k = 0; k < qnt; k++)
					score[vp[j][k]] -= sys_s[k], wc = score[vp[j][k]] <  wc ? score[vp[j][k]] : wc;

			int j = 0;

			while (score[j] != wc) j++;
			printf ("%d", j+1);
			j++;

			while (j < p)
			{
				if (score[j] == wc) printf (" %d", j+1);
				j++;
			}

			putchar ('\n');
		}
	}

	return 0;
}

