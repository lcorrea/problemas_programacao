#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int c = 1;
 
	scanf ("%d%*c", &n);
	while (1)
	{
		int f = 0, m = 0;
		int num;
		char t, ch;

		do
		{
			scanf ("%d%*c%c%c", &num, &t, &ch);

			if (n == num)
				m += t == 'M', f += t == 'F';

		}while (ch != '\n' and ch != EOF);

		printf ("Caso %d:\nPares Iguais: %d\nF: %d\nM: %d\n", c++, f+m, f, m);

		if(scanf ("%d%*c", &n) != EOF) putchar ('\n');
		else break;
	}

	return 0;
}

