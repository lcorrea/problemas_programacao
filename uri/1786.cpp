#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[20];

    while (EOF != scanf("%s", str)) {
        int sum = 0;
        for (int i = 0; i < 9; i++) {
            sum += (1+i)*(str[i]-'0');
        }
        int digit = sum%11 == 10 ? 0 : sum%11;

        sum = 0;
        for (int i = 8; i >= 0; --i) {
            sum += (i+1)*(str[8-i] -'0');
        }
        int digit2 = sum%11 == 10 ? 0 : sum%11;

        for (int i = 0; i < 9; i++) {
            if (i % 3 == 0 and i)
                putchar('.');
            putchar(str[i]);
        }
        printf("-%d%d\n", digit, digit2);
    }

    return 0;
}

