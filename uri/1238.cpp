#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string a,b;
	int n;

	cin >> n;

	while (n--)
	{
		cin >> a >> b;
		int ia, ib;
		ia = ib = 0;

		while (ia < a.length() and ib < b.length())
			cout << a[ia] << b[ib], ia++, ib++;
		cout << a.substr(ia) << b.substr(ib) << endl;
	}

	return 0;
}

