#!/usr/bin/env python3

km = int(input())
litros = float(input())

print("%.03f km/l" % (km / litros))
