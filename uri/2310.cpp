#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);
   
    int ts, tb, ta;
    int s, b, a;

    ts = tb = ta = s = b = a = 0;

    for (int i = 0; i < n; i++)
    {
        int t, u, v, x, y, z;

        scanf ("%*s%*c%d %d %d%*c%d %d %d%*c", &t, &u, &v, &x, &y, &z);

        ts += t; s += x;
        tb += u; b += y;
        ta += v; a += z;
    }

    printf ("Pontos de Saque: %.02lf %%.\n", 100.0*(s / (double) ts));
    printf ("Pontos de Bloqueio: %.02lf %%.\n", 100.0*(b / (double) tb));
    printf ("Pontos de Ataque: %.02lf %%.\n", 100.0*(a / (double) ta));

    return 0;
}

