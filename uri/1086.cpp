#include <bits/stdc++.h>

using namespace std;

int solve(int l, int n, int x, int y, int tb[10001])
{
    int parts = y * 100 / l;
    int target = x;
    int total = n;

    while (parts and target)
    {
        if (x == target and tb[target])
        {
            int use = min(tb[target], parts);

            //printf ("used %d (%d)\n", target, use);

            n -= use;

            parts -= use;
        }
        else if (tb[target] and tb[x-target] and target == x-target)
        {
            int use = min(parts, tb[target] >> 1);

            n -= 2*use;

            parts -= use;
        }
        else if (tb[target] and tb[x-target])
        {
            //printf ("used %d %d (%d) \n", target, x-target, use);
            int use = min(tb[target], tb[x-target]);

            use = min(parts, use);
            n -= 2*use;

            parts -= use;
        }

        target--;

        if (target + (x>>1) < x) break;
    }

    //printf ("sobrou %d\n", parts);

    return parts ? INT_MAX : total - n;
}

int main (void)
{
    int n, m, l;

    while (scanf ("%d %d", &n, &m), n and m)
    {
        scanf ("%d", &l);
        int tabuas;

        scanf ("%d", &tabuas);
        int tb[10001] = {0};

        for (int i = 0, v; i < tabuas; i++)
            scanf ("%d", &v), tb[v]++;

        int a = INT_MAX;
        int b = INT_MAX;

        if (!((m*100) % l))
            a = solve(l, tabuas, n, m, tb);

        //puts ("---");

        if (!((n*100) % l))
            b = solve(l, tabuas, m, n, tb);

        int ans = min(a, b);

        if (ans == INT_MAX)
            puts ("impossivel");
        else
            printf ("%d\n", ans);
    }

    return 0;
}

