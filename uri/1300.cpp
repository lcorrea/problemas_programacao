#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int a;

	while (scanf ("%d", &a) != EOF)
		puts (!(a%6) ? "Y" : "N");

	return 0;
}

