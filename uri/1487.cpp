#include <bits/stdc++.h>

using namespace std;

bool cmp (pair <int, int> a, pair <int, int> b)
{
    return (a.second / (double) a.first) >= (b.second / (double) b.first);
}

int main (void)
{
    int n, t, Case = 1;

    while (scanf ("%d %d", &n, &t), n and t)
    {
        int total = 0;
        pair <int, int> opt[n];
        
        for (int i = 0, d, p; i < n; i++)
            scanf ("%d %d", &d, &p), opt[i] = make_pair(d, p);

        sort (opt, opt + n, cmp);

        for (int i = 0, qnt, pts; i < n and t >= 0; i++)
        {
            qnt = t / opt[i].first;

            total += qnt * opt[i].second;

            t -= qnt * opt[i].first;
        }

        printf ("Instancia %d\n%d\n\n", Case++, total);

    }

    return 0;
}

