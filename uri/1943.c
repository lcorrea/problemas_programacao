#include <stdio.h>

int main (void)
{
	int p;
	scanf ("%d", &p);

	printf ("Top ");
	if (p==1) puts ("1");
	else if (p <= 3) puts ("3");
	else if (p <= 5) puts ("5");
	else if (p <= 10) puts ("10");
	else if (p <= 25) puts ("25");
	else if (p <= 50) puts ("50");
	else puts ("100");

	return 0;
}

