#include <cstdio>

using namespace std;

int main (void)
{
	int t,q;
	double a,b;

	while (scanf ("%d", &t), t)
	{
		for (int i = 1; i <= t; i++)
		{
			scanf ("%d", &q);
			scanf ("%lf %lf", &a, &b);
			printf ("Size #%d:\nIce Cream Used: ", i);
			printf ("%.2lf cm2\n", (a!=b) ? ((a+b)*2.5*q) : (a*5.0*q));
		}
		putchar ('\n');
	}

	return 0;
}

