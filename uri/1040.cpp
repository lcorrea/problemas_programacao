/*
	problem 1040
	avarege 3
	by lucas correa
*/

#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	float notes = 0, exame = 0, note;
	unsigned int weight[4] = {2, 3, 4, 1};

	for(unsigned int i = 0; i < 4; i++)
	{
		cin >> note;
		notes += note*weight[i];
	}

	printf("Media: %.1f\n", notes /= 10.0);
	
	if(notes >= 7.0)
		cout << "Aluno aprovado." << endl;
	else if(notes >= 5.0)
	{
		puts("Aluno em exame.");
		cin >> exame;

		printf("Nota do exame: %.1f\n", exame);
		
		notes += exame;

		if((notes /= 2) >= 5.0)
			puts("Aluno aprovado.");
		else
			puts("Aluno reprovado.");

		printf("Media final: %.1f\n", notes);

	}
	else
		puts("Aluno reprovado.");


	return 0;
}

