#!/usr/bin/env python3

cnt = 0

for i in range(5):
    n = int(input())
    if (not (n & 1)):
        cnt += 1

print(cnt, "valores pares")
