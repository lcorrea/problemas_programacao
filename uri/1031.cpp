#include <bits/stdc++.h>

using namespace std;

int calc (int *x, int m, int n)
{
    int i = 1;
    int size = n;
    int y = m;

    while (n and !x[13])
    {

        while (x[i])
            if (++i > size)
                i = 1;

        x[i] = 1;
        y = m;
        n--;

        while (y and n)
        {
            i++;
            if (i > size ) i = 1;
            if (!x[i]) --y;
        }

    }

    if (n == 0)
        return m;
    else
        return -1;
}

int main (void)
{
    int n;
    int sol[102] = {0};
    int x[101];

    while (scanf ("%d", &n), n)
    {
        if (sol[n] == 0)
        {
            for (int i = 1; i <= 100000; i++)
            {
                memset(x, 0, sizeof(x));

                if (calc(x, i, n) >= 1)
                {
                    sol[n] = i;
                    break;
                }
            }
        }

        printf ("%d\n", sol[n]);
    }

    return 0;
}

