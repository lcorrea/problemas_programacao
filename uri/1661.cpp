#include <iostream>
#include <cstdlib>
#include <utility>
#include <vector>

using namespace std;

typedef pair <int, int> ii;

int main (void)
{
	int n;
	vector <ii> c(100005), v(100005);

	while (cin >> n, n)
	{
		long long int w = 0;
		int j=1, i=1, k, d;
		
		for (k=1;k<=n;k++)
		{
			cin >> d;
			if (d>=0) c[i++] = ii(d, k);
			else v[j++] = ii(-d, k);
		}

		int pc=1, pv=1;
		while (pc < i and pv < j)
		{
			if (c[pc].first >= v[pv].first)
			{
				w += v[pv].first * (abs(c[pc].second - v[pv].second));
				c[pc].first -= v[pv].first;
				v[pv].first = 0;
			}
			else
			{
				w += c[pc].first * (abs(c[pc].second - v[pv].second));
				v[pv].first -= c[pc].first;
				c[pc].first = 0;
			}

			pc += c[pc].first == 0;
			pv += v[pv].first == 0;
		}

		cout << w << endl;		
	}

	return 0;
}

