#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int a, b;
	
	while (cin >> a >> b, a and b)
	{
		int c;
		set <int> ca, cb;
		set <int>::const_iterator it;

		for (int i = 0; i < a; i++)
			cin >> c, ca.insert(c);

		for (int i = 0; i < b; i++)
			cin >> c, cb.insert(c);

		int cnt = 0;
		for (it = ca.begin(); it != ca.end(); it++)
			cnt += cb.find(*it) != cb.end();
		
		printf ("%lu\n", min (ca.size() - cnt, cb.size() - cnt));
	}

	return 0;
}

