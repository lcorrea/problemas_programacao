#!/usr/bin/env python3

from math import sqrt

a, b, c = map(float, input().split())

delta = (b * b) - 4.0 * a * c

if(delta < 0 or a == 0):
    print("Impossivel calcular")
else:
    R1 = (-b + sqrt(delta)) / (2 * a)
    R2 = (-b - sqrt(delta)) / (2 * a)
    print("R1 = %.05f\nR2 = %.05f" % (R1, R2))
