#include <iostream>

using namespace std;

int main(void)
{
	int n;

	cin >> n;

	while(n)
	{
		for(int a = 1; a < n; a++)
			cout << a << ' ';
		cout << n << endl;

		cin >> n;
	}

	return 0;
}

