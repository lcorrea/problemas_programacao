#include <iostream>

using namespace std;

int main (void)
{
	const string digno = "Thor";
	string naodigno;
	int N, t;

	cin >> t;

	while (t--)
	{

		cin >> naodigno >> N;

		if (naodigno == digno && N)
			cout << 'Y';
		else
			cout << 'N';

		cout << endl;
	}

	return 0;
}

