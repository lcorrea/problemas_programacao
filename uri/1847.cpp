#include <iostream>

using namespace std;

int main (void)
{
	int a, b, c;

	cin >> a >> b >> c;

	if (a > b && b <= c)
	{
		cout << ":)";
	}
	else if (b > a && b >= c)
	{
		cout << ":(";
	}
	else if (b > a && b < c && (b - a) > (c - b))
	{
		cout << ":(";
	}
	else if (b > a && b < c && (b - a) <= (c - b))
	{
		cout << ":)";
	}
	else if (a > b && b > c && (a - b) > (b - c))
	{
		cout << ":)";
	}
	else if (a > b && b > c && (a - b) <= (b - c))
	{
		cout << ":(";
	}
	else if (a - b == 0 && b < c)
	{
		cout << ":)";
	}
	else
	{
		cout << ":(";
	}

	cout << endl;

	return 0;
}

