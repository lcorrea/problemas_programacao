#include <bits/stdc++.h>

using namespace std;

class Node
{
    protected:
        int h, e, r;

    public:
        Node(int a = 1, int b = 0, int c = 0)
        {
            h = a;
            e = b;
            r = c;
        }

        Node operator+(const Node& b)
        {
            Node c;

            c.h = this->h + b.h;
            c.e = this->e + b.e;
            c.r = this->r + b.r;

            return c;
        }

        Node operator++()
        {
            int tmp = this->r;

            r = e;
            e = h;
            h = tmp;

            return *this;
        }

        Node operator<<(int x)
        {
            Node y = *this;

            x = x % 3;

            while (x--)
                ++y;

            return y;
        }

        void show()
        {
            printf ("%d %d %d\n", h, e, r);
        }
};

class ST
{
    private:
        vector <Node> st;
        vector <int> lazy;
        int n;

        void check(int p, int l, int r)
        {
            if (lazy[p] == 0) return;

            st[p] = st[p] << lazy[p];

            if (l != r)
            {
                lazy[p<<1] += lazy[p];
                lazy[(p<<1)|1] += lazy[p];
            }

            lazy[p] = 0;
        }

        void build(int p, int l, int r)
        {
            if (l == r) { st[p] = Node(); return; }

            int mid = (l+r) >> 1;
            int left = p<<1;
            int right = left | 1;

            build(left, l, mid);
            build(right, mid+1, r);

            st[p] = st[left] + st[right];
        }

        void update(int p, int l, int r, int i, int j)
        {
            check(p, l, r);

            if (i > r or j < l) return;

            if (i <= l and j >= r)
            {
                ++st[p];

                if (l != r)
                {
                    lazy[p<<1]++;
                    lazy[(p<<1)|1]++;
                }

                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j);
            update(right, mid+1, r, i, j);

            st[p] = st[left] + st[right];
        }

        Node query(int p, int l, int r, int i, int j)
        {
            check(p, l, r);

            if (i > r or j < l){ Node rtr(0); return rtr;}

            if (i <= l and j >= r)
                return st[p];

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            Node a = query(left, l, mid, i, j);
            Node b = query(right, mid+1, r, i, j);

            return a + b;
        }

    public:
        ST(int _n)
        {
            n = _n;
            st.assign(n << 2, Node(0));
            lazy.assign(n << 2, 0);

            build(1, 0, n-1);
        }

        void update(int i, int j)
        {
            update(1, 0, n-1, i, j);
        }

        Node query(int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }
};

int main (void)
{
    int n, m;

    while (scanf ("%d %d", &n, &m) != EOF)
    {
        ST sTree(n);

        while (m--)
        {
            char cmd;
            int a, b;

            scanf ("%*c%c %d %d", &cmd, &a, &b);

            if (cmd == 'C')
                sTree.query(a-1, b-1).show();
            else
                sTree.update(a-1, b-1);
        }
        putchar ('\n');
    }

    return 0;
}

