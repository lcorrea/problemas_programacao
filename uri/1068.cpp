#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	while(1)
	{
		string exp;
		stack <char> stck;
		bool ok = true;
		char c;

		while ((c = getchar()) != '\n' and c != EOF)
			exp += c;
		if (c == EOF) break;

		for (int i = 0, sz = exp.length(); ok and i < sz; i++)
		{
			if (exp[i] == '(')
				stck.push ('(');

			if (exp[i] == ')')
			{
				if (!stck.empty())
					stck.pop();
				else
					ok = false;
			}
		}

		cout << (ok and stck.empty() ? "correct" : "incorrect") << endl;
	}

	return 0;
}

