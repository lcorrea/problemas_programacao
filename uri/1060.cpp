/*
	problem 1060
	Positive Numbers
	by: Lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	float n;
	unsigned int cnt = 0;


	for(unsigned int i = 0; i < 6; i++)
	{
		cin >> n;

		if(n > 0.0000)
			cnt++;
	}


	cout << cnt << " valores positivos" << endl;

	return 0;
}

