#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		int n, cnt = 0;
		int grade[10010] = {0};
		vector <int> q;
		
		scanf ("%d", &n);

		for (int i = 0; i < n; i++)
			scanf ("%d", grade + i), q.push_back(grade[i]);
		
		sort (q.rbegin(), q.rend());

		for (int i = 0; i < n; i++)
			cnt += q[i] == grade[i];

		printf ("%d\n", cnt);
	}

	return 0;
}

