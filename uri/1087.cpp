/*
	problem 1087
	queen
	by lucas correa
	time: 00:37:49
*/

#include <iostream>
#include <cmath>

using namespace std;

int main(void)
{
	short int x1,y1,x2,y2, dx, dy, mov;

	cin >> x1 >> y1 >> x2 >> y2;

	while(x1 || x2 || y1 || y2)
	{
		dx = abs(x2 - x1);
		dy = abs(y2 - y1);

		if(dx == 0 && dy == 0)
			mov = 0;
		else if( dx == dy || dx == 0 || dy == 0)
			mov = 1;
		else
			mov = 2;

		cout << mov << endl;
	
		cin >> x1 >> y1 >> x2 >> y2;
	}

	return 0;
}
