#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  vector <int> tickets;
  int n, m;

  while(scanf ("%d%d", &n, &m), n and m)
  {
    int cnt = 0;
    tickets.assign(n+1, 0);

    for (int i = 0, t; i < m; i++)
    {
      scanf ("%d", &t);

      tickets[t]++;

      if (tickets[t] == 2)
        cnt++;
    }

    printf ("%d\n", cnt);
  }

  return 0;
}

