#include <bits/stdc++.h>

using namespace std;

long long int cubo[10][10][10];

long long int calc (int c, int i, int j)
{
    int dx[] = {0, 0, 0, 1, -1, 1, -1, -1, 1};
    int dy[] = {0, 1, -1, 0, 0, 1, -1, 1, -1};

    long long int sum = 0;

    for (int x = c - 1; x <= c + 1; x++)
    {
        for (int k = 0; k < 9; k++)
        {
            int ii = i + dy[k];
            int jj = j + dx[k];

            sum += cubo[x][ii][jj];
        }
    }

    return sum;
}

void run() {

    long long int cubo2[10][10][10];

    memset (cubo2, 0, sizeof(cubo2));
    
    for (int c = 1; c <= 3; c++)
        for (int i = 1; i <= 3; i++)
            for (int j = 1; j <= 3; j++)
                cubo2[c][i][j] = calc(c, i, j);
    
    memcpy(cubo, cubo2, sizeof(cubo));
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;

        scanf ("%d", &n);

        memset (cubo, 0, sizeof(cubo));

        for (int c = 1; c <= 3; c++)
            for (int i = 1; i <= 3; i++)
                for (int j = 1; j <= 3; j++)
                    scanf ("%lld", &cubo[c][i][j]);

        while (n--)
            run();

        for (int c = 1; c <= 3; c++)
            for (int i = 1; i <= 3; i++)
                for (int j = 1; j <= 3; j++)
                    printf ("%lld%c", cubo[c][i][j], j > 2 ? '\n' : ' ');
    }

    return 0;
}

