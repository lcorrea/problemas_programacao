#include <bits/stdc++.h>

using namespace std;

void printsums(int *ptr[101], int n)
{
    for (int j = 0; j < n; j++) {
        for (int k = 0; k < n; k++) {
            fprintf(stderr, "%4d ",ptr[j][k]);
        }
        fprintf(stderr, "\n");
    }
}

int main (void)
{
    int sum[101][101], m[101][101], *ptr[101];
    int n, ans = 0;

    scanf("%d", &n);
 
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            scanf("%d", &m[i][j]);

    for (int j = 0; j < n; j++)
        for (int k = 0; k < n; k++)
            for (int i = k; i < n; i++) 
                sum[j][k] += m[i][j];

    for (int i = 0; i < n; i++)
        ptr[i] = sum[i];

    printsums(ptr, n);

    for (int i = 0; i < n; i++)
        ans += ptr[i][i];
    
    fprintf(stderr, "ans %d\n", ans);

    int ok = false;

    printf("%d\n", ans);

    return 0;
}

