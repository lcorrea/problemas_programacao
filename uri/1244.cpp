#include <bits/stdc++.h>

using namespace std;

int getline (char line[10000])
{
    char ch;
    int i = 0;

    while ((ch = getchar ()) != '\n')
        line[i++] = ch;
    line[i] = '\0';

    return i;
}

bool cmp (pair <string, int> a, pair <string, int> b)
{
    if (a.first.size() == b.first.size())
        return a.second < b.second;
    return a.first.size() > b.first.size();
}

int main (void)
{
    int n;

    scanf ("%d%*c", &n);

    while (n--)
    {
        char line[10000], *str;
        pair <string, int> input[51];
        int sz = 0;

        getline (line);

        str = strtok (line, " ");

        while (str != NULL)
        {
            input[sz].first = str;
            input[sz].second = sz;
            sz++;

            str = strtok (NULL, " ");
        }

        sort (input, input + sz, cmp);

        printf ("%s", input[0].first.c_str());
        for (int i = 1; i < sz; i++)
            printf (" %s", input[i].first.c_str());
        putchar ('\n');

    }

    return 0;
}

