#include <stdio.h>
#include <string.h>

int main (void)
{
	int n;
	char num[11];

	while (scanf ("%d %s", &n, num) != EOF)
	{
		int s=0, i;
		n = strlen (num);
		for (i=0; i < n; i++)
			s += num[i] - '0';

		printf ("%d %s\n", s, (s%3) ? ("nao") : ("sim"));
	}

	return 0;
}

