#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d", &n);
	while (n)
	{
		int m = 0;
		string words[n];

		for (int i = 0; i < n; i++)
			cin >> words[i], m = max((int)words[i].length(), m);

		for (int i = 0; i < n; i++)
			cout << string(m - words[i].length(), ' ') + words[i] << endl;
		scanf ("%d", &n);
		if (n) putchar ('\n');
	}

	return 0;
}

