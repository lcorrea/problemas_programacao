#include <bits/stdc++.h>

using namespace std;

vector < vector <int> > graph;

int visit[100], v, e;

void dfsR (int vtx, int lvl)
{
  int mark[200] = {0};
  visit[vtx] = 1;

  sort (graph[vtx].begin(), graph[vtx].end());

  for (int i = 0; i < graph[vtx].size(); i++)
  {
    if (!mark[graph[vtx][i]])
    {
      printf ("%*c%d-%d", lvl, ' ', vtx, graph[vtx][i]);
      if (!visit[graph[vtx][i]])
      {
        printf (" pathR(G,%d)\n", graph[vtx][i]);
        dfsR (graph[vtx][i], lvl + 2);
      }
      else
        putchar ('\n');

      mark[graph[vtx][i]] = 1;
    }
  }
}

int main (void)
{
  int n;

  scanf ("%d", &n);

  for (int i = 1; i <= n; i++)
  {
    scanf ("%d%d", &v, &e);

    memset (visit, 0, sizeof (visit));
    graph.clear();
    graph.resize(v+1);

    for (int j = 0, a, b; j < e; j++)
    {
      scanf ("%d%d", &a, &b);

      graph[a].push_back(b);
    }

    printf ("Caso %d:", i);
    for (int j = 0; j < v; j++)
    {
      if (!visit[j] and graph[j].size())
      {
        putchar ('\n');
        dfsR (j, 2);
      }
    }

    putchar ('\n');
  }

  return 0;
}

