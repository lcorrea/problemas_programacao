#include <iostream>
#include <cmath>

using namespace std;

int main (void)
{
	int x, y;
	int t;
	int pos[4][2];

	cin >> t;

	while (t--)
	{

		for (int i = 0; i < 4; i++)
			cin >> pos[i][0] >> pos[i][1];

		cin >> x >> y;

		if ((x >= pos[0][0] and x <= pos[1][0] or (x >= pos[3][0] and x <= pos[2][0])) and ((y >= pos[0][1] and y <= pos[3][1]) or (y >= pos[1][1] and y <= pos[2][1])))
			cout << 1;
		else
			cout << 0;
		cout << endl;
	}

	return 0;
}

