#!/usr/bin/env python3
from functools import reduce

while (1):
    x = input()

    if (x == '*'):
        break

    check = lambda x,y: x[0].lower() if x[0].lower() == y[0].lower() else '*'
    ok = reduce(check, x.split()) != '*'

    print('Y' if ok else 'N')
