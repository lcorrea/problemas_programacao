#!/usr/bin/env python3

cntpar = 0
cntimp = 0
cntp = 0
cntn = 0

for i in range(5):
    n = int(input())
    if (n & 1):
        cntimp += 1
    else:
        cntpar += 1

    if (n < 0):
        cntn += 1
    elif(n > 0):
        cntp += 1

print(cntpar, "valor(es) par(es)")
print(cntimp, "valor(es) impar(es)")
print(cntp, "valor(es) positivo(s)")
print(cntn, "valor(es) negativo(s)")

