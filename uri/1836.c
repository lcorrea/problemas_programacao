#include <stdio.h>
#include <math.h>

#define hp(a, b, c, l) ((c + 50 + a + b)*l/50.0 + 10.0)
#define s(a, b, c, l) ((c + a + b)*l/50.0 + 5.0)

int main (void)
{
	int t=1, n;

	scanf ("%d",&n);

	while (n--)
	{
		char name[101];
		int level, i;
		int bs, iv, ev;

		scanf ("%s %d", name, &level);

		printf ("Caso #%d: %s nivel %d\n", t++, name, level);

		double cnt;

		scanf ("%d %d %d", &bs, &iv, &ev);
	       	cnt = sqrt(ev)/8.0;
		printf ("HP: %lld\n", (long long int) hp(bs, iv, cnt, level));
		
		scanf ("%d %d %d", &bs, &iv, &ev);
	       	cnt = sqrt(ev)/8.0;
		printf ("AT: %lld\n", (long long int) s(bs, iv, cnt, level));
		
		scanf ("%d %d %d", &bs, &iv, &ev);
	       	cnt = sqrt(ev)/8.0;
		printf ("DF: %lld\n", (long long int) s(bs, iv, cnt, level));
		
		scanf ("%d %d %d", &bs, &iv, &ev);
	       	cnt = sqrt(ev)/8.0;
		printf ("SP: %lld\n", (long long int) s(bs, iv, cnt, level));

	}

	return 0;
}

