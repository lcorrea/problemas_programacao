#include <bits/stdc++.h>

using namespace std;

double calc (int n)
{
  if (n == 0)
    return 0;
  
  return 1 / (2 + calc (n - 1));
}

int main (void)
{
  int n;

  scanf ("%d", &n);
  printf ("%.10lf\n", 1 + calc(n));

  return 0;
}

