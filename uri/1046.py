#!/usr/bin/env python3

inicio, fim = map(int, input().split())

print("O JOGO DUROU %d HORA(S)" % (fim - inicio if inicio < fim else 24 - inicio + fim))
