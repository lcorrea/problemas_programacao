#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    const char list[11][50] = {
        "PROXYCITY",
        "P.Y.N.G.",
        "DNSUEY!",
        "SERVERS",
        "HOST!",
        "CRIPTONIZE",
        "OFFLINE DAY",
        "SALT",
        "ANSWER!",
        "RAR?",
        "WIFI ANTENNAS"
    };

    scanf ("%d", &n);

    while (n--)
    {
        int a, b;
        
        scanf ("%d %d", &a, &b);

        printf ("%s\n", list[a+b]);
    }

    return 0;
}

