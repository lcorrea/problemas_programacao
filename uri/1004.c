/*
	problem 1004
	simple product
	by lucas correa
*/
#include <stdio.h>

int main(void)
{
	int a = 0, prod = 0;
	while(scanf("%d%*c", &a) != EOF)
	{
		prod = a;
		scanf("%d%*c", &a);
		printf("PROD = %d\n", a*prod);
	}

	return 1;
}
