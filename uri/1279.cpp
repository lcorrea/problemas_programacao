#include <bits/stdc++.h>

using namespace std;

bool check_leap (char num[1001])
{
	int len = strlen (num);
	int n;

	sscanf (num + (len-2), "%d", &n);

	if (n % 4 == 0)
	{
		if (n == 0)
		{
			char num2[4] = {num[len-4], num[len-3]};

			sscanf (num2, "%d", &n);

			if (n % 4 == 0)
				return true;
			else
				return false;
		}
		else
			return true;
	}

	return false;
}

bool check_3 (char num[1001])
{
	int sum = 0;

	for (int i = 0, len = strlen (num); i < len; i++)
		sum += num[i] - '0';

	return (sum % 3) == 0;
}	

bool check_5 (char num[1001])
{
	char last_digit = num[strlen (num) - 1];

	return ('5' == last_digit or '0' == last_digit);
}

bool check_11 (char num[1001])
{
	int sig[2] = {1, -1};
	int diff_sum = 0;

	for (int i = 0, len = strlen(num); i < len; i++)
		diff_sum += sig[i%2]*(num[i] - '0');

	return (diff_sum % 11) == 0;
}

bool check_15 (char num[1001])
{
	return (check_3(num) and check_5(num));
}

bool check_55 (char num[1001])
{
	return (check_5(num) and check_11(num));
}


int main (void)
{
	char number[1001];

	scanf ("%s", number);
	while(1)
	{

		bool leap = check_leap (number);
		bool huluculu = check_15 (number);
		bool bulukulu = check_55 (number) and leap;

		if (leap or huluculu or bulukulu)
		{
			if (leap)
				puts ("This is leap year.");

			if (huluculu)
				puts ("This is huluculu festival year.");

			if (bulukulu)
				puts ("This is bulukulu festival year.");
		}
		else
			puts ("This is an ordinary year.");

		if (scanf ("%s", number) == EOF) break;
		else putchar ('\n');
	}

	return 0;
}

