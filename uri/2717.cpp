#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, a, b;

    scanf("%d\n%d %d", &n, &a, &b);

    puts((a + b) <= n ? "Farei hoje!" : "Deixa para amanha!");

    return 0;
}

