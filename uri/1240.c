#include <stdio.h>

int getNumber (char *str)
{
	int len = 0;
	char c;

	while (1)
	{
		c = getchar ();
		if (c == ' ' || c == '\n') break;
		*str = c;
		len++;
		str++;
	}
	*str = '\0';

	return len;
}

int main (void)
{
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		int sizeA=0, sizeB=0;
		char a[25], b[25];
		
		sizeA = getNumber (a);
		sizeB = getNumber (b);

		if (sizeA < sizeB)
		{
			puts ("nao encaixa");
			continue;
		}

		sizeA--;
		sizeB--;
		while (sizeB >= 0)
		{
			if (a[sizeA] == b[sizeB]) sizeB--, sizeA--;
			else break;
		}

		if (sizeB != -1) puts ("nao encaixa");
		else puts ("encaixa");
	}

	return 0;
}

