#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char str[3000000], *ptr, ch;
	set <string> dic;
	set <string>::const_iterator it;
	string ignore;
	
	for (char i = 1; i < 127; i++)
		if (!isalpha(i)) ignore += i;

	ptr = str;
	while ((ch = getchar()) != EOF)
		*ptr = ch, ptr++;
	*ptr = '\0';

	ptr = strtok (str, ignore.c_str());
	while (ptr != NULL)
	{
		string s;

		for (int i=0, sz = strlen(ptr); i < sz; i++)
		{

			if (isalpha(ptr[i]))
				s += tolower (ptr[i]);
			else
				if (s.size())
					dic.insert(s), s.clear();
		}
		if (s.size()) dic.insert(s);

		ptr = strtok (NULL, ignore.c_str());
	}

	for (it = dic.begin(); it != dic.end(); it++)
		puts (it->c_str());

	return 0;
}

