#include <bits/stdc++.h>

using namespace std;

long long _sieve_size;
bitset<10000010> bs;
vector <long long> primes;

void sieve (long long upperbound)
{
	_sieve_size = upperbound + 1;

	bs.set();

	bs[0]= bs[1] = 0;

	for (long long i = 2; i <= _sieve_size; i++)
	{
		if (bs[i])
		{
			for (long long j = i*i; j <= _sieve_size; j+= i)
				bs[j] = 0;
			primes.push_back (i);
		}
	}
}

bool isprime (long long n)
{
	if (n <= _sieve_size) return bs[n];

	for (int i = 0; i < primes.size(); i++)
		if (n % primes[i] == 0) return false;
	return true;
}


int main (void)
{
	int n;
	
	sieve(1000);
	scanf ("%d", &n);

	while (n--)
	{
		long long i;

		scanf ("%lld", &i);
		
		if (isprime(i)) puts ("Prime");
		else puts ("Not Prime");
	}

	return 0;
}

