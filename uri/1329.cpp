/*
	problem 1329
	head or tail
	by lucas correa
	time: 00:10:11
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int mary = 0, jhon = 0, n, result;

	cin >> n;

	while(n)
	{
		mary = 0;
		jhon = 0;

		while(n--)
		{
			cin >> result;
			if(result)
				jhon++;
			else
				mary++;
		}

		cout << "Mary won " << mary << " times and John won " << jhon << " times" << endl;

		cin >> n;
	}

	return 0;
}

