#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, d;

    while (scanf ("%d %d", &n, &d) != EOF)
    {
        char datas[d][20], *pmim = NULL;
        int presenca[n];

        for (int i = 0; i < d; i++)
        {
            scanf ("%s", datas[i]);

            int cnt = 0;

             for (int j = 0, p; j < n; j++)
                 scanf ("%d", &p), cnt += p;

             if (cnt == n and pmim == NULL)
                 pmim = datas[i];
        }

        puts ((pmim == NULL) ? "Pizza antes de FdI" : pmim);
    }

    return 0;
}

