/*
	problem 1037
	interval
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	float num;

	cin >> num;

	if(num < 0 || num > 100)
		cout << "Fora de intervalo";
	else if(num > 75)
		cout << "Intervalo (75,100]";
	else if(num > 50)
		cout << "Intervalo (50,75]";
	else if(num > 25)
		cout << "Intervalo (25,50]";
	else
		cout << "Intervalo [0,25]";
	
	cout << endl;

	return 0;
}
