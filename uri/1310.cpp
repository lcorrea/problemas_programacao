#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (scanf ("%d", &n) != EOF)
	{
		int cost;
		int A[n], revenue = 0, ai;
		int maxProfit = 0;

		scanf ("%d", &cost);

		for (int i = 0; i < n; i++)
		{
			scanf ("%d", &ai);
			
			revenue += ai - cost;
			maxProfit = max (revenue, maxProfit);

			if (revenue < 0)
				revenue = 0;
		}

		printf ("%d\n", maxProfit);
	}

	return 0;
}

