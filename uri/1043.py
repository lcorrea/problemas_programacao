#!/usr/bin/env python3

a, b, c = map(float, input().split())

if (a > abs(b-c) and a < b+c):
    if (b > abs(a-c) and b < a+c):
        if (c > abs(a-b) and c < a+b):
            print("Perimetro = %.1f" % (a + b + c))
            exit(0)

print("Area = %.1f" % ((a+b)*c/2.0))

