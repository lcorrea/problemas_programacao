#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int Cases;

    scanf("%d", &Cases);

    while (Cases--) {
        int d, n, x;
        double change = 0, p;

        scanf("%d%d", &d, &n);

        for (int i = 0; i < n; i++) {

            scanf("%lf", &p);

            if (p > d) continue;

            x = d / p;

            change = max(change, (double) d - (p * x));
        }

        printf("%.02lf\n", change);
    }

    return 0;
}

