#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;
    int max_h = 0;

    while (EOF != scanf ("%d%d", &n, &m))
    {
        int cnt = 0;
        deque<int> rats;
        deque<int>::iterator it;

        for (int i = 0, h; i < n; i++)
            scanf ("%d", &h), rats.push_back(h);

        sort(rats.begin(), rats.end());

        for (int i = 0, f; i < m; i++)
        {
            scanf ("%d", &f);

            if (!rats.size())
                continue;

            if (*rats.begin() > f)
            {
                it = lower_bound (rats.begin(), rats.end(), *rats.rbegin() - f);
                rats.insert(it, *rats.rbegin() - f);

                rats.pop_back();

                continue;
            }

            it = lower_bound(rats.begin(), rats.end(), f);

            if (it != rats.end())
                if (*it == f)
                    rats.erase(it);
                else
                    rats.erase(it - 1);
            else
                rats.pop_back();

            cnt++;
        }

        printf ("%d\n", cnt);
    }

    return 0;
}

