#include <bits/stdc++.h>

using namespace std;

class ST {
    private:
        vector <int> st;
        vector <int> v;
        int n;
        
        void build(int p, int l, int r)
        {
            if (l == r)
            {
                st[p] = v[l];
                return;
            }

            int mid = (l + r) >> 1;
            int left = p << 1;
            int right = p << 1 | 1;

            build (left, l, mid);
            build (right, mid + 1, r);

            int vl = st[left];
            int vr = st[right];

            st[p] = vl*vr;
        }

        void update(int p, int l, int r, int i, int j, int val)
        {
            if (j < l or i > r) return;

            if (i <= l and j >= r) { st[p] = v[i] = val; /*printf ("*%d\n", st[p]);*/ return; }

            int mid = (l + r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid + 1, r, i, j, val);

            int vl = st[left]; //printf ("l: %d\n", st[left]);
            int vr = st[right]; //printf ("r: %d\n", st[right]);

            st[p] = vl*vr;
        }

        int query(int p, int l, int r, int i, int j)
        {
            //printf ("[%d, %d] (%d, %d)\n", l, r, i, j);
            if (j < l or i > r) return 1;

            if (i <= l and j >= r) return st[p];

            int mid = (l + r) >> 1;
            int left = p << 1;
            int right = left | 1;

            int vl = query(left, l, mid, i, j);
            int vr = query(right, mid+1, r, i, j);

            return vl*vr;
        }

    public:
        ST(int _n, vector <int> &_v)
        {
            n = _n;
            st.assign(4*n, 0);
            v = _v;

            build(1, 0, n-1);
        }

        void update(int i, int val)
        {
            if(val == v[i]) return;

            update(1, 0, n-1, i, i, val);
        }

        int query(int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }

        void show()
        {
            for (int i = 0; i < st.size(); i++)
                printf ("%d ", st[i]);
            putchar('\n');
        }
};

int main (void)
{
    int n, k;

    while (scanf ("%d %d", &n, &k) != EOF)
    {
        vector <int> v(n);

        for (int i = 0, a; i < n; i++)
        {
            scanf ("%d", &a);

            if (a > 0) a = 1;
            else if (a < 0) a = -1;

            v[i] = a;
        }

        ST sTree(n, v);

        while (k--)
        {
            char cmd;
            int a, b;

            scanf ("%*c%c %d %d", &cmd, &a, &b);

            if (cmd == 'C')
            {
                if (b < 0) b = -1;
                else if (b > 0) b = 1;

                sTree.update(a-1, b);
            }
            else
            {
                int q = sTree.query(a-1, b-1);
                putchar(q == 0 ? '0' : q > 0 ? '+' : '-');
            }

 //           sTree.show();
        }
        putchar ('\n');
    }
    
    return 0;
}

