#include <iostream>
#include <cmath>

using namespace std;

int main (void)
{

	int a, b, r, q, d;

	cin >> a >> b;
	
	if ((a / b > 0 || (b < a)) && ((a < 0) || (b < 0)))
		q = ceil ( (float) a/b);
	else
		q = floor ( (float) a/b);

	if (b*q > a)
		r = b*q - a;
	else
		r = a - b*q;

	cout << q << ' ' << r << endl;

	return 0;
}

