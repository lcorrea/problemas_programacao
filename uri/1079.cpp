/*
	problem 1079
	weighted Avarage
	by lucas correa
*/

#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	unsigned int n;
	float a,b,c;

	cin >> n;

	while(n--)
	{
		cin >> a >> b >> c;

		printf("%.1f\n", (a*2.0 + b*3.0 + c*5.0)/10.0);
	}

	return 0;
}

