#include <bits/stdc++.h>

using namespace std;

int r, k, indegree[1001];
int mem[102][10002], ok = 0;

bool ispossible(int i, int cnt)
{
  if (mem[i][cnt] >= 0)
    return mem[i][cnt];

  if (i > r or cnt > k)
    return mem[i][cnt] = false;

  if (cnt == k)
    return mem[i][cnt] = true;

  int result1 = ispossible(i + 1, cnt);
  
  if (cnt + indegree[i] <= k and !result1)
  {
    int result2 = ispossible (i + 1, cnt + indegree[i]);

    return mem[i][cnt] = result2;
  }
  
  return mem[i][cnt] = result1;
}

int main (void)
{

  while (scanf ("%d%d", &r, &k) != EOF)
  {
    memset (indegree, 0, sizeof(indegree));
    memset (mem, -1, sizeof(mem));

    for (int i = 0, a, b; i < k; i++)
    {
      scanf ("%d%d", &a, &b);
      indegree[a]++;
      indegree[b]++;
    }

    puts (ispossible(1, 0) ? "S" : "N");
  }

  return 0;
}

