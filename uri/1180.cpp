#include <iostream>

using namespace std;

int main (void)
{
	unsigned int n, pos = 0, mpos = 0;
	int v, m = 0;

	cin >> n >> m;

	n--;
	pos++;

	while (n--)
	{
		cin >> v;

		if (v < m)
		{
			m = v;
			mpos = pos;
		}

		pos++;
	}

	cout << "Menor valor: " << m << "\nPosicao: " << mpos << endl;

	return 0;
}

