#include <stdio.h>
#include <math.h>

int main (void)
{
	int n, t;
	double d;

	scanf ("%d %lf", &n, &d);

	while (scanf ("%d%*c", &t) != EOF)
	{
		int i, disqualified = 0;
		double time = 0;
		
		for (i=0; i < n; i++)
		{
			char h1, m1, m2, s1, s2;

			scanf ("%c:%c%c:%c%c%*c", &h1, &m1, &m2, &s1, &s2);
			
			if (h1 != '-' && !disqualified)
			{
				int s, m, h;
				h = h1 - '0';
				m = (m1 - '0')*10 + (m2 - '0');
				s = h*3600 + m*60 + (s1 - '0')*10 + (s2 - '0');
				time += s;
			}
			else { disqualified = 1; }

		}
		time = round(time / d);

		printf ("%3d: ", t);
		
		if (disqualified)
			puts ("-");
		else
			printf ("%d:%02d min/km\n", (int) time/60, (int) time % 60);
	}

	return 0;
}

