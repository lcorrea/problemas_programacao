#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout

    n = int(stdin.readline())

    numbers = map(float, stdin.read().split())

    for i in range(n):
        a, b, c = next(numbers), next(numbers), next(numbers)
        stdout.write("{0:.1f}\n".format((2*a + 3*b + 5*c) / 10))

    # --- #

if __name__ == "__main__":
    main()

