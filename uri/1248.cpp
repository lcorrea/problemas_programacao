#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		int p;
		char c, lunch[30], dinner[30];
		bool cheater = false;
		map <char, int> diet;

		while ((c = getchar()) != '\n')
			diet[c] = 1;

		p  = 0;
		while ((c = getchar()) != '\n')
			lunch[p++] = c;
		lunch[p] = '\0';

		for (int i = 0; i < p and !cheater; i++)
			if (diet.find(lunch[i]) != diet.end() and diet[lunch[i]])
				diet[lunch[i]] = 0;
			else
				cheater = true;

		p = 0;
		while ((c = getchar()) != '\n')
			dinner[p++] = c;
		dinner[p] = '\0';

		for (int i = 0; i < p and !cheater; i++)
			if (diet.find(dinner[i]) != diet.end() and diet[dinner[i]])
				diet[dinner[i]] = 0;
			else
				cheater = true;

		if (cheater)
			puts ("CHEATER");
		else
		{
			map <char, int>::const_iterator it;

			for (it = diet.begin(); it != diet.end(); it++)
				if (it->second)
					putchar (it->first);

			putchar ('\n');
		}
	}

	return 0;
}

