#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int fact[6] = {1, 2, 6, 24, 120, 720};
	string number;

	while (cin >> number, number[0] != '0')
	{
		int len = number.size();
		int decimal = 0;

		for (int i = len-1, j = 0; i >= 0; i--, j++)
			decimal += (number[i] - '0')*fact[j];

		printf ("%d\n", decimal);
	}

	return 0;
}

