/*
	problem 1016
	distance
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	int km;

	cin >> km;

	cout << km*2 << " minutos" << endl;
	return 0;
}
