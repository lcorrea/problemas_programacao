#!/usr/bin/env python3

a, b = map(int, input().split())

if (a > b):
    b, a = a, b

print ("Sao Multiplos" if (b % a == 0) else "Nao sao Multiplos")

