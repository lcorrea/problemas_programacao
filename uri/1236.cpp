#include <bits/stdc++.h>

using namespace std;

int count_ch (char *p, char ch)
{
	int cnt = 0;
	while (*p == ch) p++, cnt++;
	return cnt;
}

int compress (int i, char text[2500], char ch)
{
	int cnt = count_ch (text + i, ch);
	int inc = cnt;

	if (cnt <= 2) ch == '0' ? printf ("%0*d", cnt, 0) : printf ("%*c", cnt, ch);
	else
	{
		while (cnt > 255)
		{
			printf ("%c%c", ch == '0' ? '#' : '$', (char) 0xff);
			cnt -= 255;
		}

		if (cnt)
			printf ("%c%c", ch == '0' ? '#' : '$', (char) cnt);
	}

	return inc;
}

int main (void)
{
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		char text[2500] = {0};
		int len = 0;
		char ch;

		while ((ch = getchar()) != '\n' and ch != EOF)
			text[len++] = ch;
		text[len] = 0;

		for (int i = 0; i < len; i++)
		{
			if (text[i] == '0' or text[i] == ' ')
				i += compress (i, text, text[i]) - 1;
			else
				putchar (text[i]);
		}

		putchar ('\n');
	}

	return 0;
}

