#include <iostream>
#include <map>

using namespace std;

int main (void)
{
	string p1, p2;
	map <string, unsigned int> opt;
	unsigned int g[5][2] = {2, 3, 0, 4, 1, 3, 1, 4, 0, 2};
	unsigned int t;
	
	opt["pedra"] = 0;
	opt["papel"] = 1;
	opt["tesoura"] = 2;
	opt["lagarto"] = 3;
	opt["Spock"] = 4;

	cin >> t;

	for (unsigned int a = 1; a <= t; a++)
	{
		cin >> p1 >> p2;

		cout << "Caso #" << a << ": ";

		if (opt[p1] == opt[p2])
		{
			cout << "De novo!";
		}
		else if (g[opt[p1]][0] == opt[p2] || g[opt[p1]][1] == opt[p2])
		{
			cout << "Bazinga!";
		}
		else
		{
			cout << "Raj trapaceou!";
		}

		cout << endl;
	}

	return 0;
}

