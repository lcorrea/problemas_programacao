#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
    private:
        int num;
        vector <int> p, h;

    public:
        UnionFind(int n)
        {
            h.assign(n, 0);
            num = n;

            for (int i = 0; i < n; i++)
                p.push_back(i);
        }

        int find(int a)
        {
            return a == p[a] ? a : p[a] = find(p[a]);
        }

        int join(int a, int b)
        {
            a = find(a);
            b = find(b);

            if (a != b)
            {
                --num;

                if (h[a] > h[b])
                    p[b] = a;
                else
                {
                    p[a] = b;
                    h[b] += h[a] == h[b];
                }
            }
        }

        int sets()
        {
            return num;
        }

};

int main (void)
{
    int n, m;
    pair <int, pair<int, int>> edges[500010];

    while (EOF != scanf ("%d %d", &n, &m))
    {
        UnionFind uf(n);
        int cost = 0;
        int a, b, c;

        for (int i = 0; i < m; i++)
        {
            scanf ("%d %d %d", &a, &b, &c);

            edges[i] = {c, {a-1, b-1}};
        }

        sort (edges, edges+m);

        for (int i = 0; i < m; i++)
        {
            c = edges[i].first;
            a = edges[i].second.first;
            b = edges[i].second.second;
            
            if (uf.find(a) != uf.find(b))
            {
                uf.join(a, b);
                cost += c;
            }
        }

        if (uf.sets() > 1)
            puts ("impossivel");
        else
            printf ("%d\n", cost);
    }

    return 0;
}

