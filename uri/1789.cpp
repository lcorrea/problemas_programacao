/*
	problem 1789
	race of slugs
	by lucas correa
	time:00:10:34
*/
#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int l, v, maxL, level;

	cin >> l;

	while(!cin.eof())
	{
		maxL = 0;

		while(l--)
		{
			cin >> v;

			if(v < 10)
				level = 1;
			else if(v < 20)
				level = 2;
			else
				level = 3;

			maxL = max(level, maxL);
		}

		cout << maxL << endl;

		cin >> l;
	}


	return 0;
}
