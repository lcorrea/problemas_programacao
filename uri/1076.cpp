#include <bits/stdc++.h>

using namespace std;

vector < vector <int> > grid;
vector < int > v;

void dfs (int u, int *cnt)
{
  v[u] = 1;
  *cnt = *cnt + 1;

  for (int i = 0; i < grid[u].size(); i++)
  {
    if (!v[grid[u][i]])
    {
       dfs (grid[u][i], cnt);
      *cnt = *cnt + 1;
    }
  }
}

int main (void)
{
  int t;

  scanf ("%d", &t);

  while (t--)
  {
    int src, x, e, cnt = 0;

    scanf ("%d", &src);
    scanf ("%d%d", &x, &e);

    grid.clear();
    grid.resize (x);
    v.assign (x, 0);

    for (int i = 0, u, v; i < e; i++)
    {
      scanf ("%d%d", &u, &v);

      grid[u].push_back(v);
      grid[v].push_back(u);
    }

    dfs (src, &cnt);

    printf ("%d\n", cnt-1);
  }

  return 0;
}

