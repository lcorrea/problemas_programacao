#include <iostream>

using namespace std;

int main (void)
{
	unsigned int n;
	int  x, y, sum = 0;

	cin >> n;

	while (n--)
	{
		cin >> x >> y;

		if (!(x % 2))
			x++;

		for (; y; y--, x += 2)
			sum += x;
		
		cout << sum << endl;

		sum = 0;
	}

	return 0;
}

