#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string last, lastname;
	char ch;

	last = lastname = 'a';

	while (1)
	{
		string child, childname;

		while ((ch = getchar()) != '\n' and ch != EOF)
		{
			child += tolower(ch);
			childname += ch;
		}

		if (ch == EOF) break;

		if (child > last)
			last = child, lastname = childname;
	}

	cout << lastname << endl;

	return 0;
}

