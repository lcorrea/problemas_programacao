#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string line;
    double ascii_map[255] = {0};

    ascii_map['W'] = 1.0;
    ascii_map['H'] = 1.0 / 2.0;
    ascii_map['Q'] = 1.0 / 4.0;
    ascii_map['E'] = 1.0 / 8.0;
    ascii_map['S'] = 1.0 / 16.0;
    ascii_map['T'] = 1.0 / 32.0;
    ascii_map['X'] = 1.0 / 64.0;

    while (getline(cin, line), line[0] != '*')
    {
        char composition[300], *measure;
        int corrects = 0;

        strcpy (composition, line.c_str());

        measure = strtok (composition, "/");

        while (measure != NULL)
        {
            double period = 0;

            for(int i = 0; measure[i] != '\0'; i++)
                period += ascii_map[measure[i]];
            
            if (period == 1.0)
                corrects++;

            measure = strtok (NULL, "/");
        }

        cout << corrects << endl; 
    }

    return 0;
}

