#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (scanf ("%d", &n) != EOF)
	{
		bool ok = true;

		for (int i = 0; i < 100 and ok; i++)
		{
			for (int j = 0; j < 100 and ok; j++)
			{
				if (i*i + j*j == n)
					puts ("YES"), ok = false;

				if (i*i + j*j > n)
					break;
			}
		}

		if (ok) puts ("NO");
	}

	return 0;
}

