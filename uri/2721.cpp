#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int ans = 0;
    char names[9][30] = {
        "Dasher", "Dancer", "Prancer",
        "Vixen", "Comet", "Cupid",
        "Donner", "Blitzen", "Rudolph"};

    for (int i = 0, j; i < 9; i++) {

        scanf("%d", &j);
        ans = (j + ans) % 9;
    }

    printf("%s\n", names[ans ? ans - 1 : 8]);

    return 0;
}

