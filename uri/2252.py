#!/usr/bin/python3

from sys import stdin, stdout

lines = stdin.readlines()
caso = 1

for line in range(0, len(lines), 2):
    n = int(lines[line])
    digit = -1
    keys = []

    def create(x):
        global digit
        digit += 1
        return (float(x), -digit)

    keys[:] = map(create, lines[line+1].split())
    
    keys.sort(reverse=True, key=lambda x: x)

    print("Caso %d: " % caso, end='')
    for ans in keys[0:n]:
        print(-ans[1], end='')
    print()
    caso += 1

