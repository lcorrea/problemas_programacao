from sys import stdin, stdout

teste = 0
itens = [0 for i in range(101)]
tb = [[False for j in range(10001)] for i in range(101)]
tb[0][0] = True

while True:

    total = 0
    teste += 1
    x, y, n = map(int, stdin.readline().split())

    if (x == 0 and y == 0 and n == 0):
        break

    for i in range(n):
        itens[i] = int(stdin.readline())
        total += itens[i]

    value = min(x, y) + total - max(x, y)

    if (value < 0 or value % 2):
        stdout.write ("Teste %d\nN\n\n" % teste)
        continue
    else:
        value >>= 1

    for i in range(1, n+1):
        for j in range(value+1):

            tb[i][j] = tb[i-1][j]

            if (j >= itens[i-1]):
                tb[i][j] = tb[i][j] or tb[i-1][j - itens[i-1]]

    stdout.write("Teste %d\n%s\n\n" % (teste, "S" if tb[n][value] else "N"))

