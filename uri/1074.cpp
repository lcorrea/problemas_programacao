/*
	problem 1074
	even or odd
*/
#include <iostream>
#include <cmath>

using namespace std;

int main(void)
{
	unsigned int n;
	long int x;

	cin >> n;

	while(n--)
	{
		cin >> x;

		if(x == 0)
		{
			cout << "NULL" << endl;
			continue;
		}
		else if (fmod(x, 2) == 0)
			cout << "EVEN ";
		else
			cout << "ODD ";

		if(x > 0)
			cout << "POSITIVE";
		else
			cout << "NEGATIVE";

		cout << endl;
	}

	return 0;
}

