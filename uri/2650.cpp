#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, w;

    scanf ("%d %d%*c", &n, &w);

    while (n--)
    {
        char nome[110];
        int h;

        scanf ("%[^0-9] %d%*c", nome, &h);

        if (h > w)
            printf ("%.*s\n", (int) strlen(nome) - 1, nome);
    }

    return 0;
}

