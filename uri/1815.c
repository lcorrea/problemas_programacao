#include <stdio.h>
#include <string.h>

double T[110][110];
double init[110];
double result=0;

void markov(int n)
{
	int i, j;
	double aux[110];

	for (i = 1; i <= n; i++)
	{
		aux[i] = 0.0;
		for (j = 1; j <= n; j++)
			aux[i] += init[j]*T[j][i];
	}

	memcpy (init, aux, sizeof (double) * (n+1));
}

int main (void)
{
	int n, t, k, m;
	int inst = 1;

	while (scanf ("%d %d %d %d", &n, &t, &k, &m), n)
	{
		int i, j;
		result = 0;

		for (i = 1; i <= n; i++)
		{
			init[i] = 0.0;
			for (j = 1; j <= n; j++)
				scanf ("%lf", &T[i][j]);
		}

		init[t] = 1.0;

		while (m--)
		{
			markov (n);
			result += init[k];
			init[k]=0.0;
		}

		printf ("Instancia %d\n%lf\n\n", inst++, 1.0 - result);
	}

	return 0;
}

