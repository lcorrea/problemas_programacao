#include <cstdio>
#include <vector>
#include <utility>
#include <algorithm>
#include <stack>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vvi digraph, dag;
vi visited, in, out, ts;
vi dfs_low, dfs_num, S, sc;
int dfsNumberCounter, numSCC;

void invTS (int u) // inverse topological sort
{
	visited[u] = 1;

	for (int v = 0; v < dag[u].size(); v++)
		if (visited[dag[u][v]] == 0)
			invTS (dag[u][v]);
	ts.push_back(u);
}

void tarjanSCC (int u)
{
	dfs_low[u] = dfs_num[u] = dfsNumberCounter++;
	S.push_back(u);
	visited[u] = 1;
	for (int j = 0; j < digraph[u].size(); j++)
	{
		if (dfs_num[digraph[u][j]] == 0) tarjanSCC(digraph[u][j]);

		if (visited[digraph[u][j]])
			dfs_low[u] = min (dfs_low[u], dfs_low[digraph[u][j]]);
	}

	if (dfs_num[u] == dfs_low[u])
	{
		while (1)
		{
			int v = S.back(); S.pop_back();
			sc[v] = numSCC;
			visited[v] = 0;
			if (u == v) break;
		}
		numSCC++;
	}
}

int main (void)
{
	int n, m, v, u, i;
	bool bolada = true;

	scanf ("%d %d", &n, &m);

	digraph.resize(n+1);
	//in.resize(n+1, 0); out.resize(n+1, 0);
	visited.resize(n+1, 0);
	dfs_num.assign (n+1, 0); dfs_low.assign(n+1, 0);
	sc.assign (n+1, -1);
	dfsNumberCounter = numSCC = 1;

	while (m--)
	{
		scanf ("%d %d", &u, &v);
	//	in[v]++;
	//	out[u]++;
		digraph[u].push_back(v);
	}

	/****************************
	 * - tarjan's algorithm
	 ****************************/
	for (i = 1; i <= n; i++) {
		if (dfs_num[i] == 0)
			tarjanSCC(i);
	}

	/*****************************
	 * 	- mount dag -
	 *****************************/
	dag.resize(numSCC);
	for (v = 1; v <= n; v++)
	{
		for (u = 0; u < digraph[v].size(); u++)
			if (sc[v] != sc[digraph[v][u]])
				dag[sc[v]].push_back (sc[digraph[v][u]]);
	}

	/**************************
	 * 	- print dag
	 **************************/
	/*for (v = 1; v < numSCC; v++)
		for (u = 0; u < dag[v].size(); u++)
			printf ("%d->%d\n", v, dag[v][u]);*/


	/************************************
	 * 	topological sort dag
	 ************************************/
	fill (visited.begin(), visited.end(), 0);
	for (v = 1; v < numSCC; v++)
		if (visited[v]==0)
			invTS(v);

	/*************************************
	 * 	- check if is sf dag -
	 *************************************/
	for (v = ts.size()-1, u = v - 1; v > 0 and bolada; v--, u--)
	{
		bolada = false;
		for (i = 0; i < dag[ts[v]].size() and !bolada; i++)
			bolada = ts[u] == dag[ts[v]][i];
	}

	if (bolada) puts ("Bolada");
	else puts ("Nao Bolada");

	return 0;
}

