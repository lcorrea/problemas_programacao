/**
\brief Problema para maratona de programacao
*/
#include <stdio.h>
 
int main(void)
{
    int jogador1, jogador2, jogador3;
    //char Vencedor;
    //scanf("%d", jogadaAnterior);
    while((scanf("%d %d %d", &jogador1 , &jogador2, &jogador3) != EOF))
    {
     
        if(jogador1 == jogador2 && jogador3 == jogador1 && jogador2 == jogador3)
        {
            printf("*\n");
        }
        else
        {
            if(jogador1 != jogador2 && jogador3 != jogador1){
                printf("A\n");
            }
            else if(jogador2 != jogador1 && jogador3 != jogador2)
            {
                printf("B\n");
            }
            else
            {
                printf("C\n");
            }
        }
    }
     
    return 0;
}

