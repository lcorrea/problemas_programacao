#!/usr/bin/env python3

dds = {
        61: 'Brasilia',
        71: 'Salvador',
        11: 'Sao Paulo',
        21: 'Rio de Janeiro',
        32: 'Juiz de Fora',
        19: 'Campinas',
        27: 'Vitoria',
        31: 'Belo Horizonte'
        }

cod = int(input())

print(dds[cod] if cod in dds else "DDD nao cadastrado")

