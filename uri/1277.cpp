#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    cin >> t;

    while (t--)
    {
        int n;
        pair <string, string> students[101];
        int cnt = 0;

        cin >> n;

        for (int i = 0; i < n; i++)
            cin >> students[i].first;

        for (int i = 0; i < n; i++)
        {
            int p = 0, f = 0;

            cin >> students[i].second;

            for (int j = 0; students[i].second[j] != '\0'; j++)
            {
                switch (students[i].second[j])
                {
                    case ('P'): p++; break;
                    case ('A'): f++; break;
                }
            }

            if (p / (double) ( p + f) < .75)
            {
                if (cnt > 0)
                    cout << ' ';
                cout << students[i].first;
                cnt++;
            }
        }
        cout << endl;
    }

	return 0;
}

