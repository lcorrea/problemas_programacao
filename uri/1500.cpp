#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int uLL;

class ST
{
    private:
        vector <uLL> st, lazy;
        int n;

        void check(int p, int l, int r)
        {
            if (lazy[p] == 0)
                return;

            st[p] += (1ull + r - l)*lazy[p];

            if (l != r)
            {
                lazy[p<<1] += lazy[p];
                lazy[(p<<1)|1] += lazy[p];
            }

            lazy[p] = 0;
        }

        uLL query(int p, int l, int r, int i, int j)
        {
            check(p, l, r); // check if node must be update!

            if (i > r or j < l) return 0;

            if (i <= l and j >= r) return st[p];

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            uLL a = query(left, l, mid, i, j);
            uLL b = query(right, mid+1, r, i, j);

            return a+b;
        }

        void update (int p, int l, int r, int i, int j, uLL val)
        {
            //printf ("[%d, %d] (%d, %d)\n", l, r, i, j);
            check(p, l, r);

            if (i > r or j < l) return;

            if (i <= l and j >= r)
            {
                //printf ("hi (%d, %d)\n", l, r);

                st[p] += (1ull + r - l)*val;

                if (l != r)
                {
                    lazy[p<<1] += val;
                    lazy[(p<<1)|1] += val;
                }

                return;
            }

            int mid = (l+r)>>1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid+1, r, i, j, val);

            st[p] = st[left] + st[right];
        }

    public:
        ST(int _n)
        {
            n = _n;
            st.assign(n << 2, 0);
            lazy.assign(n << 2, 0);
        }

        uLL query(int i, int j)
        {
            return query(1, 0, n-1, i, j);
        }

        void update(int i, int j, uLL val)
        {
            update(1, 0, n-1, i, j, val);
        }

        void show()
        {
            for (int i = 0; i < st.size(); i++)
                printf ("(%llu, %llu)  ", st[i], lazy[i]);
            putchar ('\n');
        }
};


int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n, c;

        scanf ("%d %d", &n, &c);

        ST sTree(n);
        for (int i = 0; i < c; i++)
        {
            unsigned int cmd, a, b, v;

            scanf ("%u", &cmd);
            if (cmd)
            {
                scanf ("%u %u", &a, &b);

                printf ("%llu\n", sTree.query(a-1, b-1));
            }
            else
            {
                scanf ("%u %u %u", &a, &b, &v);

                sTree.update(a-1, b-1, v);
            }
        }
    }

    return 0;
}

