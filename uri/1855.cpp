#include <cstdio>

using namespace std;

int main ()
{
	char map[100][100], dir;
	int x, y, x_pos = 0, y_pos = 0, incr_x, incr_y;

	scanf("%d%d", &x, &y);

	for (int i = 0; i < y; i++)
		scanf("%s", map[i]);

	dir = map[0][0];

	while (dir != '!' and dir != '*')
	{
		switch (dir)
		{
			case ('>'):
				incr_x = 1;
				incr_y = 0;
			break;
			case ('<'):
				incr_x = -1;
				incr_y = 0;
			break;
			case ('^'):
				incr_y = -1;
				incr_x = 0;
			break;
			case ('v'):
				incr_y = 1;
				incr_x = 0;
			break;
		}

		y_pos += incr_y;
		x_pos += incr_x;

		if (x_pos >= x or x_pos < 0 or y_pos >= y or y_pos < 0)
		{
			dir = '!';
		}
		else
		{
			dir = map[y_pos][x_pos];
			map[y_pos][x_pos] = '!';
		}
	}

	putchar(dir);
	putchar('\n');
	
	return 0;
}

