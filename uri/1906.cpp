#include <bits/stdc++.h>

using namespace std;

int stat[10000001];
int n;

int dfs(int x, int k)
{
    if (stat[x] == 1)
        return true;

    if (k)
    {
        int next;

        if (x & 1)
        {
            next = 3*x + 1;

            if (next > n)
            {
                stat[x] = -1;
                return false;
            }
        }
        else
            next = x >> 1;

        return dfs(next,k-1);
    }

    return true;
}

int main (void)
{
    int k, cnt = 0;
    scanf ("%d %d", &n, &k);

    for (int i = 1; i <= n; i++)
        if (stat[i] != -1)
            if (dfs(i, k-1))
                cnt++, stat[i] = 1;

    printf ("%d\n", cnt);

    return 0;
}

