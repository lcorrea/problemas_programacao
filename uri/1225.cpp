#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;

  while (EOF != scanf ("%d", &n))
  {
    int s = 0;
    int notas[n+1];
    int max_ni = 0;

    for (int i = 0, ni; i < n; i++)
      scanf ("%d", &notas[i]), s += notas[i];

    if (s % n == 0)
    {
      int ans = 0;
      
      s = s / n;
      for (int i = 0; i < n; i++)
        if (notas[i] > s)
          ans += notas[i] - s;

      printf ("%d\n", ans + 1);
    }
    else
      puts ("-1");
  }

	return 0;
}

