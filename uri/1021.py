#!/usr/bin/env python3

notas = [100, 50, 20, 10, 5, 2]
moedas = [1, .50, .25, .10, .05, .01]

calc = lambda valor,nota: (valor/nota, valor % nota)

valor = float(input()) * 1.0000001

print("NOTAS:")
for nota in notas:
    qnt, valor = calc(valor, nota)
    print("%d nota(s) de R$ %0.02f" % (qnt, nota))

print("MOEDAS:")
for moeda in moedas:
    qnt, valor = calc(valor, moeda)
    print("%d moeda(s) de R$ %0.02f" % (qnt, moeda))

