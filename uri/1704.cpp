#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>

using namespace std;

typedef pair <int, int> ii;

int main (void)
{
	int n, h, v, a, t;
	cin >> n >> h;
	while (!cin.eof())
	{
		vector <int> hour(h+1, 0);
		priority_queue <ii> q;
		unsigned int spend = 0;
		int i;

		while (n--)
		{
			cin >> v >> a;
			q.push (ii(v, a));
			spend += v;
		}
		
		ii p = q.top(); q.pop();
		i = 1;
		hour[p.second] = p.first;
		spend -= p.first;
		while (i <= h and !q.empty())
		{
			p = q.top(); q.pop();

			while (hour[p.second] >= p.first and p.second >=1)
				p.second--;

			if (p.second)
			{
				spend -= p.first;
		       		hour[p.second] = p.first;
				i++;
			}
		}

		cout << spend << endl;

		cin >> n >> h;
	}
	
	return 0;
}

