#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    while (cin >> n, n)
    {
        map <string, string> original;
        int cnt = 0;

        for (int i = 0; i < n; i++)
        {
            string name, signature;

            cin >> name >> signature;
            original[name] = signature;
        }

        cin >> m;

        for (int i = 0; i < m; i++)
        {
            string name, signature;

            cin >> name >> signature;

            if (original[name] != signature)
            {
                if (abs((int)original[name].size() - (int)signature.size()) > 1)
                    cnt++;
                if (original[name].size() == signature.size())
                {
                    int diff = 0;

                    name = original[name];
                    for (int i = 0; name[i] != '\0'; i++)
                        diff += name[i] != signature[i];

                    cnt += diff > 1;
                }
            }
        }

        cout << cnt << endl;
    }

    return 0;
}

