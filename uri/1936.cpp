#include <iostream>
using namespace std;

int main (void)
{
	int k = 0, n;
	unsigned int fat = 1, last;

	cin >> n;

	while (n > 0)
	{
		k++;
		for (fat = 1, last = 2; fat <= n; last++)
			fat = fat*last;
		n -= fat/(last-1);
	}

	cout << k << endl;

	return 0;
}

