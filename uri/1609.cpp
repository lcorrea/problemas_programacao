#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        set<int> c;
        int n, total = 0;

        scanf ("%d", &n);

        for (int i = 0, id; i < n; i++)
        {
            scanf ("%d", &id);

            c.insert(id);
        }

        printf ("%d\n", (int) c.size());
    }

    return 0;
}

