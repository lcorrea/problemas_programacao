/*
	problem 1247
	Coast guard
*/

#include <iostream>
#include <cmath>

using namespace std;

int main(void)
{
	float h, tp, tb;
	unsigned int d, vb, vp;

	cin >> d >> vb >> vp;
	
	while(!cin.eof())
	{

		h = sqrt(144 + d*d);

		tb = 12.0/vb;
		tp = h/vp;

		if(tp <= tb)
			cout << 'S' << endl;
		else
			cout << 'N' << endl;

		cin >> d >> vb >> vp;	
	}

	return 0;
}

