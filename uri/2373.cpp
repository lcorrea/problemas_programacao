#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, total = 0;

    scanf ("%d", &n);

    for (int i = 0, l, c; i < n; i++)
    {
        scanf ("%d %d", &l, &c);

        if (l > c)
            total += c;
    }

    printf ("%d\n", total);

    return 0;
}

