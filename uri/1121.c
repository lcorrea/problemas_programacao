/*
	problem 1121
	sticker collector robot
	by lucas correa

*/
#include <stdio.h>

int main(void)
{
    unsigned int robotPos[2], nextPos[2], n = 0, m = 0, s = 0, i = 0, j = 0;
    unsigned int stickers = 0;
    char orientacao = 'X';
    char instrucao = 'P';

    scanf("%u %u %u%*c", &n, &m, &s);
    
    while(n)
    {
     	char arena[n][m];

        orientacao = 'X';
        stickers = 0;
        
        for(i = 0; i < n; i++)
        {
            scanf("%[^\n]%*c", arena[i]);
            
            for(j = 0; j < m && orientacao == 'X'; j++)
            {
                if(isalpha(arena[i][j]))
                {
                    orientacao = arena[i][j];
		    arena[i][j] = '.';
                    robotPos[0] = i;
                    robotPos[1] = j;
                    break;
                }
            }
        }

        
	for(i = 0; i < s; i++)
        {	
		scanf("%c", &instrucao);

		nextPos[0] = robotPos[0];
		nextPos[1] = robotPos[1];

		switch(instrucao)
		{
			case('F'):
				if(orientacao == 'N')
				{
					if(nextPos[0])
						nextPos[0]--;
				}
				else if(orientacao == 'L')
				{
					if(nextPos[1] < m - 1)
						nextPos[1]++;
				}
				else if(orientacao == 'S')
				{
					if(nextPos[0] < n - 1)
						nextPos[0]++;
				}
				else
				{
					if(nextPos[1])
						nextPos[1]--;
				}

				if(nextPos[0] < n && nextPos[1] < m)
				{
					if(arena[nextPos[0]][nextPos[1]] == '*')
					{
						robotPos[0] = nextPos[0];
						robotPos[1] = nextPos[1];
						
						stickers += 1;

						arena[robotPos[0]][robotPos[1]] = '.';
					}
					else if(arena[nextPos[0]][nextPos[1]] == '.')
					{
						robotPos[0] = nextPos[0];
						robotPos[1] = nextPos[1];
					}
				}
			break;
			case('D'):
				if(orientacao == 'N') orientacao = 'L';

				else if(orientacao == 'L') orientacao = 'S';

				else if(orientacao == 'S') orientacao = 'O';

				else orientacao = 'N';
			break;
			default:
				if(orientacao == 'N') orientacao = 'O';

				else if(orientacao == 'L') orientacao = 'N';

				else if(orientacao == 'S') orientacao = 'L';
				
				else orientacao = 'S';
		}

        }
        
	printf("%u\n", stickers);
	scanf("%*c%u %u %u%*c", &n, &m, &s);
    }
    
    return 0;
}

