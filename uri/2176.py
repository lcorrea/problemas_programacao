number = input()
cnt = 0

for x in number:
    cnt += 1 if x == '1' else 0

ans = number + ('1' if cnt % 2 else '0')

print (ans)

