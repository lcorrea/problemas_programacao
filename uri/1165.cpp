#include <iostream>
#include <cmath>

using namespace std;

int main (void)
{
	unsigned int n, x, b, a;

	cin >> n;

	while (n--)
	{
		cin >> x;

		cout << x;

		for (a = 2, b = 0; !b && a <= sqrt(x) ; a++)
		{
			if (!(x % a))
			{
				b++;
				cout << " nao eh primo" << endl;
				break;
			}
		}

		if (!b)
			cout << " eh primo" << endl;

	}

	return 0;
}

