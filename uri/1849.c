#include <stdio.h>

#define min(a,b) ((a<b)?(a):(b))
#define max(a,b) ((a>b)?(a):(b))

int main (void)
{
	int a, b, c, d;
	unsigned int m = 0;

	scanf ("%d %d %d %d", &a, &b, &c, &d);

	m = max(m, min(a+c, min(d, b)));
	m = max(m, min(d+a, min(b, c)));
	m = max(m, min(b+d, min(a, c)));
	m = max(m, min(b+c, min(d, a)));

	printf ("%d\n", m*m);

	return 0;
}

