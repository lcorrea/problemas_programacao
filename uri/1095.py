#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout
    
    for i, j in zip(range(1, 10000, 3), range(60, -1, -5)):
        stdout.write("I=%d J=%d\n" % (i, j))

if __name__ == "__main__":
    main()

