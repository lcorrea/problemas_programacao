#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf("%d", &n);

    while (n--) {
        int a, b, c, d;

        scanf("%d %d %d %d", &a, &b, &c, &d);

        int t = c - a;
        int w = d - b;
        double ans = w/(double)t;
        int ansI = ans;
        int ansD = ans*100.0 - ansI*100.0;
        printf("%d,%02d\n", ansI, ansD);
    }
                

    return 0;
}

