#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;

	while (cin >> n >> m, m and n)
	{
		int mat[m][n];

		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				cin >> mat[i][j];
		
		bool ok = false;
		for (int j = 0; j < n and !ok; j++)
		{
			int cnt = 0;
			
			for (int i = 0; i < m; i++)
				cnt += mat[i][j];

			if (cnt == m)
				ok = true;
		}

		puts (ok ? "yes" : "no");
	}


	return 0;
}

