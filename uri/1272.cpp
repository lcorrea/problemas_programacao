#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		bool w = true;
		char ch;

		while ((ch = getchar()) != '\n')
		{
			if (isblank(ch)) w = true;
			else
			{
				if (w) putchar (ch);
				w = false;
			}
		}
		putchar('\n');
	}

	return 0;
}

