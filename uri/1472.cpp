/*
	problem 1472
	triangulos
	by lucas correa
*/
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	unsigned int i, npoints, p, cnt, c;
	vector<unsigned int> arcos;
	cin >> npoints;

	while(!cin.eof())
	{
		cnt = 0;

		arcos.clear();

		cin >> p;
		arcos.push_back(p);

		for(i = 1; i < npoints; i++)
		{
			cin >> p;
			arcos.push_back(p + arcos.back());
		}

		c = arcos.back()/3;

		for(i = 0; i < npoints; i++)
		{
			if(binary_search(arcos.begin(), arcos.end(), arcos[i] + c))
				if(binary_search(arcos.begin(), arcos.end(), arcos[i] + 2*c))
					cnt++;				
		}

		cout << cnt << endl;

		cin >> npoints;
	}

	return 0;
}

