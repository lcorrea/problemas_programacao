#!/usr/bin/python3
from sys import stdin

for line in stdin:
    n = int(line)

    for x in range(n):
        l = stdin.readline().split(' ')
        x = len(l)
        print(chr(96 + x*3 - (3 - len(l[0]) - (-1 if x == 1 else 0))))

