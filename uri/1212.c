#include <stdio.h>
#include <string.h>

int main (void)
{
	char n1[13], n2[13];

	while (scanf ("%s %s", n1, n2), (*n1-'0') || (*n2-'0'))
	{
		int len1 = strlen (n1) - 1;
		int len2 = strlen (n2) - 1;
		int carry = 0, ncarry = 0;

		while (len1 >= 0 && len2 >= 0)
		{
			int a = n1[len1] - '0';
			int b = n2[len2] - '0';

			if (a + b + carry > 9)
			{
				carry = 1;
				ncarry++;
			}
			else	carry = 0;
			
			len1--; len2--;
		}

		int a = n1[len1] - '0';
		if (len1 >= 0 && carry + a > 9)
			ncarry++;

		a = n2[len2] - '0';
		if (len2 >= 0 && carry + a > 9)
			ncarry++;

		if (!ncarry) puts ("No carry operation.");
		else if (ncarry == 1) puts ("1 carry operation.");
		else printf ("%d carry operations.\n", ncarry);

	}

	return 0;
}

