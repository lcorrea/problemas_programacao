#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, i = 1;
	vector <int> s, t(300);

	s.push_back (0);
	t[0] = 1;
	for (int k = 1; k <= 200; k++)
	{
		int tmp = k;
		while (tmp--) s.push_back (k);
		t[k] = t[k-1] + k;
	}

	while (scanf ("%d", &n) != EOF)
	{
		printf ("Caso %d: %d %s\n", i, t[n], t[n]>1 ? "numeros" : "numero");

		for (int k = 0; k < t[n]; k++)
			printf ("%d%c", s[k], k+1==t[n] ? '\n' : ' ');

		i++;
		putchar ('\n');
	}

	return 0;
}

