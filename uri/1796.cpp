#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, ok_cnt = 0;

    scanf ("%d", &n);

    for (int i = 0, q; i < n; i++)
    {
        scanf ("%d", &q);

        ok_cnt += !q;
    }

    if (ok_cnt > n/2)
        puts ("Y");
    else
        puts ("N");

    return 0;
}

