#include <bits/stdc++.h>

using namespace std;

const int dx[8] = {0, 0, 1, -1, 1, -1, -1, 1};
const int dy[8] = {1, -1, 0, 0, -1, -1, 1, 1};

int dfs(char grid[20][20], int n, int i, int j, char key)
{
    int ans = 1;
    grid[i][j] = '*';

    for (int d = 0; d < (key == '1' ? 8 : 4); d++) {

        int newi = dy[d] + i;
        int newj = dx[d] + j;

        if (newi < n and newi > -1)
            if (newj < n and newj > -1)
                if (grid[newi][newj] == key)
                    ans += dfs(grid, n, newi, newj, key);
    }

    return ans;
}

int main (void)
{
    int t;

    scanf("%d%*c", &t);

    while (t--) {

        int n, perimetro, area;

        scanf("%d%*c", &n);

        char grid[20][20];

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                scanf("%c%*c", &grid[i][j]);

        for (int i[2] = {0, n-1}, cnt = 0, ok = 0; cnt < 2; cnt++)
            for (int j = 0; j < n; j++)
                if (grid[i[cnt]][j] == '0') 
                    dfs(grid, n, i[cnt], j, '0');

        for (int j[2] = {0, n-1}, cnt = 0; cnt < 2; cnt++)
            for (int i = 0; i < n; i++)
                if (grid[i][j[cnt]] == '0')
                    dfs(grid, n, i, j[cnt], '0');

        for (int i = 0, ok = true; i < n and ok; i++)
            for (int j = 0; j < n and ok; j++)
                if (grid[i][j] == '1') {
                    perimetro = dfs(grid, n, i, j, '1');
                    ok = false;
                }

        for (int i = 0, ok = true; i < n and ok; i++)
            for (int j = 0; j < n and ok; j++)
                if (grid[i][j] == '0') {
                    area = dfs(grid, n, i, j, '0');
                    ok = false;
                }

        printf("%.02lf\n", (perimetro + area) / 2.0);
    }

    return 0;
}

