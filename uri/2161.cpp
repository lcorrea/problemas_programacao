#include <bits/stdc++.h>

using namespace std;

double calc (int n, double v)
{
  if (n == 0)
    return 0;

  return 1/(v + calc (n - 1, v));
}


int main (void)
{
  int n;
  double root = 3.0;
  
  scanf ("%d", &n);

  printf ("%.10lf\n", root + calc (n, 6));

  return 0;
}

