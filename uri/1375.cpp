#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		int grid[10000] = {0};
		int c, p, cars = 0;
		bool valid = true;
		
		for (int i = 1; i <= n; i++)
		{
			int ipos;

			scanf ("%d %d", &c, &p);
			ipos = i + p;

			if (ipos <= 0 or ipos > n or grid[ipos] != 0) valid = false;
			else grid[ipos] = c, cars++;
		}

		if (valid and cars == n)
			for (int i = 1; i <= n; i++)
				printf ("%d%c", grid[i], i < n ? ' ' : '\n');
		else
			puts ("-1");
	}

	return 0;
}

