#include <iostream>
#include <algorithm>
using namespace std;

int main(void)
{
	unsigned int x, y, n, sum;

	cin >> n;

	while(n--)
	{
		cin >> x >> y;

		sum = 0;

		if(y < x)
			swap(x,y);

		if(x % 2)
			x += 2;
		else
			x++;
		
		while(x < y)
		{
		
			sum += x;
			x += 2;
		}

		cout << sum << endl;
	}

	return 0;
}

