#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int r1, r2, x1, x2, y1, y2;

	while (scanf ("%d %d %d %d %d %d", &r1, &x1, &y1, &r2, &x2, &y2) != EOF)
	{
		if (hypot(x1 - x2, y1 - y2) + r2 > r1)
			puts ("MORTO");
		else
			puts ("RICO");
	}

	return 0;
}

