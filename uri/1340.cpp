#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        int op, x, cnt = 7;
        stack <int> pilha;
        queue <int> fila;
        priority_queue <int> pq;

        while (n--)
        {
            scanf ("%d %d", &op, &x);

            if (op == 1)
            {
                if (cnt & 1) pilha.push(x);

                if (cnt & 2) fila.push(x);

                if (cnt & 4) pq.push(x);
            }
            else 
            {
                int x1, x2, x3;

                if (cnt & 1)
                {
                    bool empty = pilha.empty();

                    if (!empty)
                    {
                        x1 = pilha.top();
                        pilha.pop();
                    }

                    if (empty or x != x1)
                        cnt &= ~1;
                }

                if (cnt & 2)
                {
                    bool empty = fila.empty();

                    if (!empty)
                    {
                        x2 = fila.front();
                        fila.pop();
                    }

                    if (empty or x != x2)
                        cnt &= ~2;
                }

                if (cnt & 4)
                {
                    bool empty = pq.empty();

                    if (!empty)
                    {
                        x3 = pq.top();
                        pq.pop();
                    }

                    if (empty or x != x3)
                        cnt &= ~4;
                }
            }
        }

        if (cnt == 0) puts ("impossible");
        else if (cnt >= 5 or cnt == 3) puts ("not sure");
        else if (cnt == 1) puts ("stack");
        else if (cnt == 2) puts ("queue");
        else puts ("priority queue");
    }

    return 0;
}

