#include <iostream>
#include <map>

using namespace std;

int main (void)
{
	int n, k, i;

	string resp[4] = {"Rolien", "Naej", "Elehcim", "Odranoel"};

	cin >> n;

	while (n--)
	{
		cin >> k;

		while (k--)
		{
			cin >> i;

			cout << resp[i-1] << endl;
		}
	}

	return 0;
}

