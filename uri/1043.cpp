/*
	problem 1043
	triangle
	by lucas correa
	time 00:19:28
*/
#include <cstdio>
#include <cmath>

using namespace std;

int main(void)
{
	float a, b, c;
	
	scanf("%f %f %f%*c", &a, &b, &c);

	if(fabs(b-c) < a && a < b+c)
	{
		if(fabs(a-c) < b && b < a+c)
		{
			if(fabs(a-b) < c && c < a+b)
			{
				printf("Perimetro = %.1f\n", a + b + c);
				return 0;
			}
		}
	}
	
	printf("Area = %.1f\n", (a+b)*c/2);
	
	return 0;
}

