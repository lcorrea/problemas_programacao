/*
	problem 1245
	losts boots
	time: 01:11:11
*/

#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <utility>

using namespace std;

int main(void)
{
	map<unsigned int, vector<unsigned int> > boots;
	map<unsigned int, vector<unsigned int> >::iterator it;
	vector<unsigned int> aux(2);
	unsigned int n, numero, soma;
	char peh;
	
	cin >> n;

	while(!cin.eof())
	{
		boots.clear();
		soma = 0;
		while(n--)
		{
			cin >> numero >> peh;

			if(boots.find(numero)!= boots.end())
			{
				aux = boots[numero];
				aux[peh - 'D']++;
				boots[numero] = aux;
			}
			else
			{
				aux[0] = 0;
				aux[1] = 0;
				aux[peh - 'D'] = 1;
				boots[numero] = aux;
			}
		}

		for( it = boots.begin(); it != boots.end(); it++)
		{
			pair< unsigned int, vector<unsigned int> > p =  *it;

			soma += min(p.second[0], p.second[1]);
		}

		cout << soma << endl;

		cin >> n;
	}

	return 0;
}
