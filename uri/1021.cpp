/*
	problem 1021
	banknotes and coins
	by lucas correa
	time: 01:21:00
*/
#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	unsigned int r = 0, n, notes, cents;

	scanf("%u.%u", &notes, &cents);

	n = notes;

	cout << "NOTAS:\n";

	if(r = n/100)
		n -= (r*100);

	cout << r << " nota(s) de R$ 100.00\n";

	if(r = n/50)
		n = n - r*50;
	cout << r << " nota(s) de R$ 50.00\n";
	
	if(r = n/20)
		n = n - r*20;
	cout << r << " nota(s) de R$ 20.00\n";
	
	if(r = n/10)
		n = n - r*10;
	cout << r << " nota(s) de R$ 10.00\n";

	if(r = n/5)
		n = n - r*5;
	cout << r << " nota(s) de R$ 5.00\n";
	
	if(r = n/2)
		n = n - r*2;
	cout << r << " nota(s) de R$ 2.00\n";

	cout << "MOEDAS:\n";

	cout << n << " moeda(s) de R$ 1.00\n";

	n = cents;

	if(r = n/50)
		n = n - r*50;
	cout << r << " moeda(s) de R$ 0.50\n";

	if(r = n/25)
		n = n - r*25;
	cout << r << " moeda(s) de R$ 0.25\n";

	if(r = n/10)
		n = n - r*10;
	cout << r << " moeda(s) de R$ 0.10\n";
	
	if(r = n/5)
		n = n - r*5;
	cout << r << " moeda(s) de R$ 0.05\n";

	cout << n << " moeda(s) de R$ 0.01\n";

	return 0;
}

