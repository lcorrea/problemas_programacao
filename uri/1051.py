#!/usr/bin/env python3

salario = float(input())

ans = 0.0
calculo = salario

if (salario > 4500.0):
    ans += (calculo - 4500.0) * .28
    calculo -= calculo - 4500.0

if (salario > 3000.0):
    ans += (calculo - 3000.0) * .18
    calculo -= calculo - 3000.0

if (salario > 2000.0):
    ans += (calculo - 2000.0) * .08

print(("R$ %.2f" % (ans)) if ans > 0.0 else "Isento")
