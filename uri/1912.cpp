#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	double fitas[100001];
	int total = 0, n, a;

	while (scanf ("%d%d", &n, &a), n or a)
	{
		total = 0;

		for (int i = 0; i < n; i++)
		{
			scanf ("%lf", fitas + i);
			total += fitas[i];
		}

		if (total < a)
			puts ("-.-");
		else if (total == a)
			puts (":D");
		else
		{
			double lo, hi, middle;
			double area = total;

			sort (fitas, fitas + n);

			hi = fitas[n-1];
			lo = 0;

			while (lo <= hi and abs(area - a) >= 0.00009999)
			{
				middle = (lo + hi) * 0.5;

				int i = n - 1;
				area = 0;

				while (i >= 0 and middle < fitas[i])
				{
					area += fitas[i] - middle;
					i--;
				}

				if (area > a)
					lo = middle;
				else
					hi = middle;
			}

			if (abs(area - a) <= 0.0000999)
				printf ("%.04lf\n", middle);
			else
				puts ("-.-");
		}
	}

	return 0;
}

