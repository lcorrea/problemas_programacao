#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf("%d%*c", &n);

    while (n--) {

        char ch, line[10000] = {0}, *ptr;
        int t, ok, error = 0, open = 0, close = 0;

        scanf("%d%*c", &t);

        ptr = line;
        while ((ch = getchar()) != '\n' and ch != EOF) {

            *ptr = ch;
            ptr++;
        }
        *ptr = '\0';

        ptr = line;

        stack<char> q;
        stack<string> tags;

        while (*ptr and !error) {

            if (t == 1) {

                if (*ptr == '<' or *ptr == '>' or *ptr == '/')
                    q.push(*ptr);
                else;
            }
            else
                q.push(*ptr);

            if (*ptr == '>') {

                error = 1;
                string tag;

                while (!q.empty()) {

                    ch = q.top(); q.pop();
                    tag += ch;

                    if (ch == '<') {

                        reverse(tag.begin(), tag.end());
                        
                        if (tag[1] == '/' and t)
                            break;

                        if (!t) {

                            //printf ("x: %s \n", tag.c_str());
                            if (tag[1] == '/')
                            {
                                string temp = tags.top();
                                string opentag, closetag = "<";// = tags.top();

                                for (auto ss : temp)
                                    if (ss == ' ' or ss == '>')
                                        break;
                                    else
                                        opentag += ss;

                                temp = tag.substr(2);

                                for (auto ss : temp)
                                    if(ss == ' ' or ss == '>')
                                        break;
                                    else
                                        closetag += ss;

                                if (opentag != closetag) {
                                    //printf ("eita: %s != %s\n", opentag.c_str(), closetag.c_str());
                                    break;
                                }
                                else
                                    tags.pop();
                                //printf("%s == %s\n", opentag.c_str(), closetag.c_str());
                            }
                            else
                                tags.push(tag);
                        }
                        
                        error = 0;
                        break;
                    }
                }
            }

            ptr++;
        }

        while (!q.empty() and !error) {
            char ttt = q.top(); q.pop();
            error = ttt == '>' or ttt == '<';
        }

        if (error or (t==0 and !tags.empty())) {
        //if (error or (t==0 and !tags.empty()) or (t==1 and !q.empty())) {
            puts("error");
            //printf("%d %d\n", (int) q.size(), (int) tags.size());
            /*
            puts("q: ");
            while(!q.empty())
                putchar(q.top()), q.pop();
            putchar('\n');
            puts("tags: ");
            while(!tags.empty())
                puts (tags.top().c_str()), tags.pop();
            puts("---");*/
        }
        else
            puts ("Successful !!");
    }       

    return 0;
}

