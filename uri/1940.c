#include <stdio.h>

int jogadores[510];

int main (void)
{
	int j, r, i, ii, p;

	scanf ("%d %d", &j, &r);

	for (i = 0; i < r; i++)
	{
		for (ii = 0; ii < j; ii++)
		{
			scanf ("%d", &p);
			jogadores[ii] += p;
		}
	}
	
	int maxid = 0;
	for (i = 0; i < j; i++)
	{
		if (jogadores[maxid] <= jogadores[i])
			maxid = i;
	}
	printf ("%d\n", maxid+1);

	return 0;
}

