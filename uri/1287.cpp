#include <bits/stdc++.h>

using namespace std;

int read_line (char str[55])
{
	int i = 0;
	char ch;

	while ((ch = getchar()) != '\n' and ch != EOF)
	{
		if (!isdigit(ch))
		{
			if (ch == 'o' or ch == 'O')
				ch = '0';
			else if (ch == 'l')
				ch = '1';
			else if (ch == ',' or isblank(ch)) continue;
			else { i = 0; while ((ch = getchar()) != '\n' and ch != EOF); break;}
		}

		str[i++] = ch;
	}
	str[i] = '\0';

	return ch == EOF ? -1 : i;
}

int main (void)
{

	int len;
	long long num;
	char str[55];

	while ((len = read_line(str)) != -1)
	{

		if (!len)
			puts ("error");
		else
		{
			sscanf (str, "%lld", &num);

			if (num <= 2147483647)
				printf ("%lld\n", num);
			else
				puts ("error");
		}
	}

	return 0;
}

