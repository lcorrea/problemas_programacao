#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        int notas[n];
        int c[n];
        int ch = 0, acc = 0;

        for (int i = 0, a, b; i < n; i++)
        {
            scanf ("%d %d", &a, &b);

            acc += a*b;
            ch += b;
        }

        printf ("%.04lf\n", acc / (100.00 * ch));
    }
            
    return 0;
}

