#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	unsigned int t, v;
	float fuel;

	cin >> t >> v;

	printf("%.3f\n", t*v/12.0);	

	return 0;
}

