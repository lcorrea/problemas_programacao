#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int mod (int a)
{
	int r = a%26;

	return r < 0 ? r + 26 : r; 
}

int main (void)
{
	int i, k;
	char alph[26], str[100], *ch;

	for (i = 0; i < 26; i++)
		alph[i] = 'A' + i;

	scanf ("%d", &i);

	while (i--)
	{
		scanf ("%s %d", str, &k);

		ch = str;

		while (*ch)
		{
			putchar (alph[mod(*ch - 'A' - k)]);
			ch++;
		}

		putchar ('\n');
	}

	return 0;
}

