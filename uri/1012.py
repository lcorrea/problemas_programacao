#!/usr/bin/env python3

a, b, c = map(float, input().split())

print("TRIANGULO: %.03f" % (a * c / 2))
print("CIRCULO: %.03f" % (3.14159 * c * c))
print("TRAPEZIO: %.03f" % ((a + b)*c/2))
print("QUADRADO: %.03f" % (b*b))
print("RETANGULO: %.03f" % (a*b))

