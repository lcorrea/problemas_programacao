#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string s1, s2;

    while (getline(cin, s1), getline(cin, s2), !cin.eof())
    {
        int mseq = 0;

        for (int i = 0; s1[i] != '\0'; i++)
        {
            int cnt = 0;

            for (int j = 0; s2[j] != '\0'; j++)
            {
                if (s1[i] == s2[j])
                {
                    int a = i;
                    int b = j;

                    while (s1[a] and s2[b] and s1[a] == s2[b])
                    {
                        cnt++;
                        a++;
                        b++;
                    }

                    mseq = max (cnt, mseq);
                    cnt = 0;
                }
            }
        }
        
        cout << mseq << endl;
    }

	return 0;
}

