/*
	problem 1094
	Experiments
	by lucas correa
*/

#include <iostream>
#include <cstdio>

using namespace std;

int main(void)
{
	unsigned int total = 0, n = 0, amount;
	unsigned total_c = 0, total_r = 0, total_s = 0;
	char animal;

	cin >> n;

	while(n--)
	{
		cin >> amount >> animal;

		total += amount;

		switch(animal)
		{
			case('C'):
				total_c += amount;
			break;
			case('R'):
				total_r += amount;
			break;
			case('S'):
				total_s += amount;
			break;
		}
	}

	printf("Total: %u cobaias\n", total);
	printf("Total de coelhos: %u\n", total_c);
	printf("Total de ratos: %u\n", total_r);
	printf("Total de sapos: %u\n", total_s);
	printf("Percentual de coelhos: %.2f %%\n", total_c*100.0/total);
	printf("Percentual de ratos: %.2f %%\n", total_r*100.0/total);
	printf("Percentual de sapos: %.2f %%\n", total_s*100.0/total);

	return 0;
}

