#include <iostream>
#include <cstdio>

using namespace std;

int main (void)
{
	double n;

	cin >> n;

	for (unsigned int i = 0; i < 100; i++, n /= 2)
	{
		printf("N[%u] = %.4lf\n", i, n);
	}

	return 0;
}

