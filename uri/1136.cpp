#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, b;

	while (scanf ("%d %d", &n, &b), n and b)
	{
		vector <int> calls (n+1, 0), balls(b);

		for (int i = 0; i < b; i++)
			scanf ("%d", &balls[i]);

		for (int i = 0; i < b; i++)
			for (int j = 0; j < b; j++)
				calls[abs(balls[i] - balls[j])] = 1;

		bool ok = true;

		for (int i = 0; ok and i <= n; i++)
			ok = calls[i];

		puts ( ok ? "Y" : "N");
	}

	return 0;
}

