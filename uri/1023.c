#include <stdio.h>
#include <math.h>
#include <string.h>

#define max(a, b) ((a>b)?(a):(b))

int main (void)
{
	int n, i;
	long long unsigned int medias[201], cidade = 1;

	scanf ("%d", &n);
	while (n)
	{
		
		printf ("Cidade# %llu:\n", cidade++);
		long long unsigned int totalpop = 0;
		long long unsigned int totalconsumo = 0;
		unsigned int pop, consumo, maxC = 0;
		memset (medias, 0, sizeof(long long unsigned int)*201);
		for (i = 0; i < n; i++)
		{
			scanf ("%u %u", &pop, &consumo);
			totalpop += pop;
			totalconsumo += consumo;
			consumo /= pop;
			medias[consumo] += pop;
			maxC = max(consumo, maxC);
		}
		
		for (i = 0; i <= maxC; i++)
		{
			if (medias[i])
			{
				printf ("%llu-%d", medias[i], i);
				if (i != maxC) putchar (' ');
			}
		}
		putchar ('\n');
		double media = totalconsumo/(double)totalpop;
		totalconsumo /= totalpop;
		long long unsigned int m = (long long unsigned int) media;
		printf ("Consumo medio: %llu.%02llu m3.\n", m, (long long unsigned int)((media-totalconsumo)*100.00));
		
		scanf ("%d", &n);
		if (n) putchar ('\n');
	}

	return 0;
}

