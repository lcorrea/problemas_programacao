#include <stdio.h>
#include <string.h>

#define max(a, b) ((a>b)?(a):(b))
#define min(a, b) ((a<b)?(a):(b))

int main (void)
{
	int t;
	
	scanf ("%d", &t);

	while (t--)
	{
		int m, foods[102], total = 0;

		scanf ("%d", &m);

		memset (foods, 0, sizeof(int)*102);

		while (m--)
		{
			int type, qnt;

			scanf ("%d %d", &type, &qnt);

			//esta dentro da faixa
			if (qnt >= 10 && qnt <= 100)
			{
				total -= foods[type];

				//se o valor atual da comida esta dentro da faixa
				if (foods[type] >= 10 && foods[type] <= 100) foods[type] = max (foods[type], qnt);
				else foods[type] = qnt; //valor atual fora da faixa

				total += foods[type];
			}
			else
			{
				//valor da comida fora da faixa
				if (foods[type] < 10 || foods[type] > 100)
				{
					total -= foods[type];
					foods[type] = max (foods[type], qnt);
					total += foods[type];
				}
			}

		}

		printf ("%d\n", total);
	}

	return 0;
}

