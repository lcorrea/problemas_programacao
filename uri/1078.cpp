/*
	problem 1078
	multiplication table
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, prod;
	
	cin >> n;
	prod = n;

	for(unsigned int i = 1; i <= 10; i++, prod += n)
		cout << i << " x " << n << " = " << prod << endl;

	return 0;
}

