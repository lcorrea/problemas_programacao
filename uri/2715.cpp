#include <bits/stdc++.h>

using namespace std;

long long int prefix[1000000], suffix[1000000], arr[1000000];

int main (void)
{
    int n;

    while (EOF != scanf("%d", &n))
    {
        for(int i = 0; i < n; i++)
            scanf ("%lld", arr+i);

        prefix[0] = arr[0];
        suffix[n-1] = arr[n-1];

        for (int i = 1; i < n; i++)
        {
            prefix[i] = arr[i] + prefix[i-1];
            suffix[n-i-1] = arr[n-i-1] + suffix[n-i];
        }

        long long int ans = arr[0];

        if (n > 1)
            ans = abs(prefix[0] - suffix[1]);

        for (int i = 1; i < n-1; i++)
            ans = min(ans, abs(prefix[i] - suffix[i+1]));

        printf("%lld\n", ans);
    }

    return 0;
}

