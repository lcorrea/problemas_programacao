#include <bits/stdc++.h>

#define INF 1000000000
#define check(a) ((a) >= 1 and (a) <= 100000 and !ban[(a)])

using namespace std;

bitset<100005> ban;
int dist[100005];

int next_channel (int key, int ch)
{
  int next = -1;

  switch (key)
  {
    case 0:
      next = ch * 3;
      if (check(next))
        return next;
    break;

    case 1:
      next = ch << 1;
      if (check(next))
        return next;
    break;

    case 2:
      if (ch >= 2 and (ch % 2) == 0)
      {
        next = ch >> 1;
        
        if (check(next))
          return next;
      }
    break;

    case 3:
      next = ch + 1;
      if (check(next))
        return next;
    break;

    case 4:
      next = ch - 1;
      if (check(next))
        return next;
    break;
  }

  return -1;
}

int bfs (int o, int d)
{
  queue <int> q;

  for (int i = 0; i <= 100000; i++)
    dist[i] = INF;

  dist[o] = 0;
  q.push (o);

  while (!q.empty())
  {
    int u = q.front(); q.pop();

    if (u == d)
      return dist[u];

    for (int i = 0; i < 5; i++)
    {
      int v = next_channel (i, u);

      if (v != -1 and dist[v] == INF)
      {
        dist[v] = dist[u] + 1;

        q.push (v);
      }
    }
  }

  return -1;
}

int main (void)
{
  int o, d, k;

  while (scanf ("%d%d%d", &o, &d, &k), o and d)
  {
    ban.reset();

    for (int i = 0, a; i < k; i++)
      scanf ("%d", &a), ban[a] = true;

    printf ("%d\n", bfs(o, d));
  }

  return 0;
}

