#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    cin >> n;

    map <string, set<string>> list;
    string name, gift;
    
    for (int i = 0; i < n; i++) {

        cin >> name;

        for (int j = 0; j < 3; j++) {
            cin >> gift;

            list[gift].insert(name);
        }
    }

    while (cin >> name >> gift) {
        if (list[gift].find(name) != list[gift].end())
            cout << "Uhul! Seu amigo secreto vai adorar o/\n";
        else
            cout << "Tente Novamente!\n";
    }

    return 0;
}

