#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  const double cnst = 0.00000000675;
  double f;

  while (EOF != scanf ("%lf", &f))
  {
    double n = f / 0.0000001;
    printf ("%.10lf\n", f + cnst * n);
  }

  return 0;
}

