#include <bits/stdc++.h>

using namespace std;

int n, m, coef[4];
map <int, int> score;

void calc (int pts, int contest)
{
    if (contest >= n)
    {
        ++score[pts];
        return;
    }

    for (int i = 1; i <= m; i++)
        calc (pts + coef[contest]*i, contest + 1);
}

int main (void)
{
    while (scanf ("%d %d", &n, &m) != EOF)
    {
        score.clear();

        for (int i = 0; i < n; i++)
            scanf ("%d", coef + i);

        calc (0, 0);

        map <int, int>::const_iterator it = score.begin();
        bool ok = true;

        for (it = score.begin(); ok and it != score.end(); it++)
            ok = it->second <= 1;

        puts (ok ? "Lucky Denis!" : "Try again later, Denis...");
    }

    return 0;
}

