#include <stdio.h>
#include <stdlib.h>

#define max(a,b) ((a>b)?(a):(b))
#define min(a,b) ((a<b)?(a):(b))

int main (void)
{
	int n, p, pa, pi, H;

	scanf ("%d %d", &p, &n);

	scanf ("%d", &pa);
	H=0;
	n--;
	while (n--)
	{
		scanf ("%d", &pi);
		H = max (H, abs(pa - pi));
		pa = pi;
	}
	if (H > p) puts ("GAME OVER");
	else puts ("YOU WIN");

	return 0; 
}

