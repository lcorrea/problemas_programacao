#include <stdio.h>
#include <math.h>

int main(void)
{
	unsigned char i, j, dist = 0;
	unsigned int N;
	scanf("%u%*c", &N);
	while(N)
	{
		for(i = 0; i < N; i++)
		{
			for( j = 0; j < N-1; j++)
			{
				dist =  fmin( fmin(i, j),  fmin(N-i-1, N-j-1));
				printf("%3u ", dist+1);
			}
			puts("  1");
		}
		putchar('\n');
		scanf("%u%*c", &N);
	}
	return 0;
}

