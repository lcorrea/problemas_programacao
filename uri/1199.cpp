#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	char num[100];

	while (scanf ("%s", num) != EOF)
	{
		if (strncmp(num, "0x", 2) == 0)
		{
			sscanf(num, "%x", &n);
			printf ("%d\n", n);
		}
		else if (strcmp(num, "-1") == 0) 
		{
			break;
		}
		else
		{
			sscanf (num, "%d", &n);
			printf ("0x%X\n", n);
		}
	}

	return 0;
}

