/*
	problem 1096
	Sequence IJ 2
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int i = 1, j=7;

	while(i < 10)
	{
		while(j >= 5)
		{
			cout << "I=" << i << " J=" << j << endl;
			j--;
		}

		j = 7;
		i += 2;
	}

	return 0;
}

