#include <bits/stdc++.h>

using namespace std;

#define calc(a) ((((a)*(1LL + (a))) >> 1LL))

long long int myBinarySearch(long long int k)
{
    long long int low = 1, hi = 100011, mid, a, b, c;

    for (int i = 0; i < 32; i++)
    {
        mid = (hi + low) >> 1LL;

        a = calc(mid);
        b = calc(mid + 1);

        if (k > a and k < b)
            break;
        else if (k <= a)
            hi = mid;
        else 
            low = mid + 1;
    }

    long long int res = calc(mid);

    return res == k ? mid : k - res;
}

int main (void)
{
    int n, k;
    char str[100000][20];

    while (scanf ("%d %d", &n, &k), n and k)
    {
        for (int i = 0; i < n; i++)
            scanf ("%s", str[i]);

        long long int i = myBinarySearch(k);

        printf ("%s\n", str[i-1]);
    }

    return 0;
}

