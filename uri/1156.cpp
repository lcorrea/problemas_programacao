#include <cstdio>

using namespace std;

int main (void)
{
	float s = 1;
	unsigned int n = 1, d = 1;


	while (n < 39 )
	{	
		d *= 2;
		n += 2;

		s += (float) n / d;
	}
	
	printf ("%.2f\n", s);

	return 0;
}

