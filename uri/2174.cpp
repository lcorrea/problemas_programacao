#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    cin >> n;

    set <string> data;

    while (n--)
    {
        string name;

        cin >> name;

        data.insert(name);
    }

    cout << "Falta(m) " << (151 - (int) data.size()) << " pomekon(s).";
    cout << endl;

    return 0;
}

