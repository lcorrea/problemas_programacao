/*
	problem 1006
	average 2
	by lucas correa
*/
#include <stdio.h>

int main(void)
{

	float a = 0, media = 0;

	while(scanf("%f%*c", &a) != EOF)
	{
		media = a*2;
		scanf("%f%*c", &a);
		media += a*3;
		scanf("%f%*c", &a);
		media += a*5;

		printf("MEDIA = %.1f\n", media/10);
	}

	return 0;
}
