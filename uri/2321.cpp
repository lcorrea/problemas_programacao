#include <bits/stdc++.h>

using namespace std;

int ax1[2], ay1[2], ax2[2], ay2[2];

bool get(int x, int y, int *xx, int *yy)
{
    if (x >= xx[0] and x <= xx[1])
        if (y >= yy[0] and y <= yy[1])
            return true;

    return false;
}

int main (void)
{
    scanf ("%d %d %d %d", &ax1[0], &ay1[0], &ax1[1], &ay1[1]);
    scanf ("%d %d %d %d", &ax2[0], &ay2[0], &ax2[1], &ay2[1]);

    int ok = 0;

    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++)
            if (get(ax2[i], ay2[j], ax1, ay2) or get(ax1[i], ay1[j], ax2, ay2))
                ok = 1;

    printf ("%d\n", ok);

    return 0;
}

