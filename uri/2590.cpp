#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    const int last[] = {7, 9, 3, 1};

    scanf ("%d", &t);

    while (t--)
    {
        int n;
        
        scanf ("%d", &n);

        printf ("%d\n", n ? last[(n-1)%4] : 0);
    }

    return 0;
}

