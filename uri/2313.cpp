#include <bits/stdc++.h>

using namespace std;

typedef long long int ll;

inline bool check (ll a, ll b, ll c)
{
    return (a + b > c and abs (a - b ) < c);
}

int main (void)
{
    long long int a, b, c;

    scanf ("%lld %lld %lld", &a, &b, &c);

    if (check(a, b, c) or
            check(b, a, c) or
            check(c, a, b) or
            check(c, b, a) or
            check(b, c, a) or
            check (a, c, b))
    {
        if ((a == b and a != c) or
                (b == c and b != a ) or
                (a == c and b != a))
            puts ("Valido-Isoceles");
        else if (a != b and b != c and c != a)
            puts ("Valido-Escaleno");
        else
            puts ("Valido-Equilatero");

        printf ("Retangulo: %c\n", (a*a + b*b == c*c) or
                (a*a + c*c == b*b) or 
                (c*c + b*b == a*a) ? 'S' : 'N');
    }
    else
        puts ("Invalido");

    return 0;
}

