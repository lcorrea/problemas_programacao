#!/usr/bin/env python3

cod1, qnt1, valor1 = input().split()
cod2, qnt2, valor2 = input().split()

qnt1, qnt2 = int(qnt1), int(qnt2)
valor1, valor2 = float(valor1), float(valor2)

print("VALOR A PAGAR: R$ %.02lf" % (valor1*qnt1 + valor2*qnt2))

