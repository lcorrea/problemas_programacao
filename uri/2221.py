t = int(input())

for instance in range(t):
    
    bonus = int(input())

    a1, d1, l1 = map(int, input().split())
    a2, d2, l2 = map(int, input().split())

    poke1 = 0 if l1 % 2 else bonus
    poke2 = 0 if l2 % 2 else bonus

    poke1 += (a1 + d1)*0.5
    poke2 += (a2 + d2)*0.5

    if (poke1 > poke2):
        print ("Dabriel")
    elif (poke1 == poke2):
        print ("Empate")
    else:
        print ("Guarte")

