#include <iostream>
#include <set>
#include <utility>

using namespace std;

typedef pair <int, int> ii;

int main ()
{
	int n, t;

	set <ii> people;

	cin >> n;

	for (unsigned int i = 1; i <= n; i++)
	{
		cin >> t;

		people.insert(ii(t,i));
	}

	cout << people.begin()->second << endl;

	return 0;
}

