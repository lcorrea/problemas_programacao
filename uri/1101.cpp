/*
	problem 1101
	Sequence of numbers and sum
	by lucas correa
*/
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	int a,b;

	cin >> a >> b;

	while(a > 0 && b > 0)
	{
		if(a > b)
			swap(a,b);

		for(int i = a; i <= b; i++)
			cout << i << ' ';

		cout << "Sum=" << ((b - a + 1)*(a + b))/2 << endl;
	
		cin >> a >> b;
	}

	return 0;
}

