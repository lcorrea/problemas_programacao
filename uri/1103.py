#!/usr/bin/env python3

def main():

    from sys import stdin, stdout

    for line in stdin:

        hnow, mnow, hafter, mafter = map(int, line.split())

        now = hnow*60 + mnow
        after = hafter*60 + mafter

        if not (now | after):
            return
        
        ans = after - now if now < after else 24*60 + after - now

        stdout.write("%d\n" % ans)

if __name__ == "__main__":
    main()

