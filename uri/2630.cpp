#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    for (int i = 1; i <= n; i++)
    {
        char type[20];
        int r, g, b, result;

        scanf ("%s", type);
        scanf ("%d %d %d", &r, &g, &b);

        if (type[0] == 'e')
                result = (int) (0.30*r + 0.59*g + 0.11*b);
        else
        {
            if (type[0] == 'm' and type[1] == 'e')
                result = (int) ((r+g+b)/3.0);
            else if (type[0]== 'm' and type[1] == 'a')
                result = max(r, max(g, b));
            else
                result = min(r, min(g, b));
        }

        printf ("Caso #%d: %d\n", i, result);
    }

    return 0;
}

