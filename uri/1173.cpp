#include <iostream>

using namespace std;

int main (void)
{
	long long int n;
	
	cin >> n;

	for (unsigned int i = 0; i < 10; i++, n *= 2)
	{

		cout << "N[" << i << "] = ";
		cout << n << endl;
	}

	return 0;
}

