#include <bits/stdc++.h>

using namespace std;

typedef struct
{
    long long int sum;
    map<string, int> var;
}LineRow;

int numVars, L, C;
map <string, bool> ok;
map <string, long long int> vars;
LineRow lin[101], col[101];

void search(LineRow lineRow[101], int n)
{
    for (int i = 0; i < n and numVars; i++)
    {
        auto &v = lineRow[i].var;
        long long int &sum = lineRow[i].sum;

        for (auto it = v.begin(); it != v.end(); it++)
        {
            auto key = it->first;

            if (ok[key])
            {
                long long int x = (vars[key])*(it->second);
                sum += -x;
                v.erase(key);
            }
        }

        if (v.size() == 1)
        {
            auto key = v.begin()->first;
            auto val = v.begin()->second;

            vars[key] = sum / val;
            ok[key] = true;

            v.erase(key);
            numVars--;
        }
    }
}

int main (void)
{
    char str[3];

    scanf ("%d %d%*c", &L, &C);

    for (int i = 0; i < L; i++)
    {
        for (int j = 0; j < C; j++)
        {
            scanf ("%s%*c", str);
            vars[str] = ok[str] = 0;
            lin[i].var[str]++;
            col[j].var[str]++;
        }

        scanf ("%lld%*c", &lin[i].sum);
    }

    for (int j = 0; j < C; j++)
        scanf ("%lld%*c", &col[j].sum);

    numVars = vars.size();

    while (numVars)
    {
        search(col, C);
        search(lin, L);
    }

    for (auto it = vars.begin(); it != vars.end(); it++)
        printf ("%s %lld\n", it->first.c_str(), it->second);

    return 0;
}

