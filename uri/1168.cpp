#include <iostream>
#include <cstring>

using namespace std;

int main (void)
{
	int n;
	const int leds[] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};

	cin >> n;
		
	while (n--)
	{
		string number;
		int total = 0;
		
		cin >> number;

		for (int i = 0; i < number.size(); i++)
			total += leds[number[i] - '0'];

		cout << total << " leds" << endl;
	}

	return 0;
}

