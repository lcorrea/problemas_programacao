#include <iostream>
#include <algorithm>
#include <cmath>
#include <utility>

using namespace std;

int main (void)
{
	int xb, yb;
	int x, y, n, i;
	pair <double, int> dists[50];

	cin >> n;
	while (n--)
	{
		cin >> i;
		cin >> xb >> yb;

		for (int b = 0; b < i; b++)
		{
			cin >> x >> y;
			dists[b].second = b;
			dists[b].first = hypot (xb - x, yb - y);
		}

		sort (dists, dists+i);
		
		cout << dists[0].second + 1 << endl;
	}

	return 0;
}

