#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    scanf("%d %d", &n, &m);

    int p[n][4];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < 4; j++)
            scanf("%d", &p[i][j]);

    int ans = 0;
    for (int i = 0; i < m; i++) {
        int cnt = 0;
        int planet[3];
        
        for (int j = 0; j < 3; j++)
            scanf("%d", &planet[j]);

        for (int j = 0; j < n; j++) {
            int x = planet[0]*p[j][0] + p[j][1]*planet[1] + p[j][2]*planet[2] - p[j][3];

            printf("%d\n", x);
            cnt += x == 0;
            break;
        }

        ans = max(ans, cnt);
        puts("---");
    }

    printf("%d\n", ans);

    return 0;
}

