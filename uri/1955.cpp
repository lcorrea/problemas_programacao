#include <bits/stdc++.h>

using namespace std;

bool bicolor(int u, int n, bool seeworld[1001][1001])
{
    int vertices = 1;
    int color[1001];
    queue <int> q;
   
    memset(color, -1, sizeof(color));

    color[u] = 0;
    q.push(u);

    while (!q.empty())
    {
        u = q.front(); q.pop();

        for (int v = 0; v < n; v++)
            if (seeworld[u][v])
            {
                if (color[v] == -1)
                {
                    vertices++;
                    color[v] = !color[u];
                    q.push(v);
                }
                else if (color[v] == color[u])
                    return false;
            }
    }

    return true;
}

int main (void)
{
    int n;
    bool seeworld[1001][1001];
    
    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        for (int j = 0, a; j < n; j++)
            scanf ("%d", &a), seeworld[i][j] = !a;

    puts (((!bicolor(0, n, seeworld))?"Fail!":"Bazinga!"));

    return 0;
}

