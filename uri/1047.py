#!/usr/bin/env python3

inicioh, iniciom, fimh, fimm = map(int, input().split())

inicio = inicioh*60 + iniciom
fim = fimh*60 + fimm

if (inicio >= fim):
    fim = 24*60 + fim

duracao = fim - inicio

print("O JOGO DUROU %d HORA(S) E %d MINUTO(S)" % (duracao / 60, duracao % 60))
