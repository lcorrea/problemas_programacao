#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int m, n;
    queue <char> times;

    for (char t = 'A'; t <= 'P'; t++)
        times.push(t);

    for (int i = 0; i < 15; i++)
    {
        char a = times.front(); times.pop();
        char b = times.front(); times.pop();

        scanf ("%d %d", &n, &m);

        if (n > m)
            times.push(a);
        else
            times.push(b);
    }

    printf ("%c\n", times.front());

    return 0;
}

