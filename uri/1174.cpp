#include <cstdio>

using namespace std;

int main (void)
{
	float n;

	for (unsigned int i = 0; i < 100; i++)
	{
		scanf("%f%*c", &n);

		if(n <= 10)
			printf ("A[%u] = %.1f\n", i, n);
	}

	return 0;
}

