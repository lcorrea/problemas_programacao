/*
	problem 1075
	remaining 2
	by lucas correa
*/

#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n;

	cin >> n;

	for(unsigned int i = 2; i < 10000; i++)
		if(i % n == 2)
			cout << i << '\n';


	return 0;
}
