/*
	problem 1066
	even, odd, positive and negative
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	int num;
	unsigned int cnt_even = 0, cnt_odd = 0, cnt_p = 0, cnt_n = 0;


	for(unsigned int i = 0; i < 5; i++)
	{
		cin >> num;

		if(num > 0)
			cnt_p++;
		else if(num < 0)
		{
			cnt_n++;
			num *= -1;
		}
		
		if(num % 2 == 0)
			cnt_even++;
		else
			cnt_odd++;

	}

	cout << cnt_even << " valor(es) par(es)" << endl;
	cout << cnt_odd << " valor(es) impar(es)" << endl;
	cout << cnt_p << " valor(es) positivo(s)" << endl;
	cout << cnt_n << " valor(es) negativo(s)" << endl;

	return 0;
}

