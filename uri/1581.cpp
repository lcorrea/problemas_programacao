#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    cin >> t;

    while (t--)
    {
        int n;
        bool ok = true;
        string person, lang;

        cin >> n;

        cin >> lang;

        for (int i = 1; i < n; i++)
        {
            cin >> person;

            if (person != lang) ok = false;
        }

        cout << (ok ? lang : "ingles") << endl;
    }
	return 0;
}

