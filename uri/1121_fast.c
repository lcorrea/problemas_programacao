/*
    problem 1121
    sticker Collector Robot
    by lucas correa
 
*/
#include <stdio.h>
#include <stdlib.h>
 
unsigned int m;
 
char mov(char *orient, const char instru, unsigned int pos[2],
     char arena[][m], const unsigned int n, const unsigned int m)
{   
    unsigned int i = pos[0];
    unsigned int j = pos[1];
     
    switch(instru)
    {
        case('F'):
            switch(*orient)
            {
                case('S'):
                    if(i + 1 < n)
                        i++;
                break;
                case('N'):
                    if(i)
                        i--;
                break;
                case('O'):
                    if(j)
                        j--;
                break;
                case('L'):
                    if(j + 1 < m)
                        j++;
                break;
            }
        break;
        case('D'):
            switch(*orient)
            {
                case('S'):
                    *orient = 'O';
                break;
                case('N'):
                    *orient = 'L';
                break;
                case('O'):
                    *orient = 'N';
                break;
                case('L'):
                    *orient = 'S';
                break;
            }
        break;
        case('E'):
            switch(*orient)
            {
                case('S'):
                    *orient = 'L';
                break;
                case('N'):
                    *orient = 'O';
                break;
                case('O'):
                    *orient = 'S';
                break;
                case('L'):
                    *orient = 'N';
                break;
            }
        break;
    }
    if(arena[i][j] == '#')
        return '#';
 
    pos[0] = i;
    pos[1] = j;
 
    return arena[i][j];
}
 
int main(void)
{
    unsigned int n, s, pos[2], i, j, stickers = 0;
    char cell = '.', orient = 'N';
 
    scanf("%u %u %u%*c", &n, &m, &s);
 
    while(n)
    {
        char instru[s];
        char arena[n][m];
        orient = 'A';
        stickers = 0;
 
        for(i = 0; i < n; i++)
        {
            scanf("%[^\n]%*c", arena[i]);
            for(j = 0; j < m && orient == 'A'; j++)
            {
                if(isalpha(arena[i][j]))
                {
                    pos[0] = i;
                    pos[1] = j;
                    orient = arena[i][j];
                    arena[i][j] = '.';
                    //printf("\no=%c\n", orient);
                }
            }
        }
         
        scanf("%[^\n]%*c", instru);
         
        for(i = 0; i < s; i++)
        {
            cell = mov(&orient, instru[i], pos, arena, n, m);
             
            if(cell == '*')
            {
                stickers++;
                arena[pos[0]][pos[1]] = '.';
            }
        }
        printf("%u\n", stickers);
 
        scanf("%u %u %u%*c", &n, &m, &s);
    }
     
    return 0;
}

