#!/usr/bin/env python3

cnt = 0
media = 0.0

for i in range(6):
    n = float(input())
    
    if (n > 0):
        cnt += 1
        media += n

print(cnt, "valores positivos")
print("%.1f" % (media/cnt))
