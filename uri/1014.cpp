/*
	problem 1040
	consumption
	by lucas correa
	time: 00:07:02
*/
#include <cstdio>

int main(void)
{
	unsigned int km;
	float consumption;

	scanf("%u%*c%f%*c", &km, &consumption);

	printf("%.3f km/l\n", km/consumption);

	return 0;
}

