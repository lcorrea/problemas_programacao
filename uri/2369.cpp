#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int faixa[3] = {100, 30, 10};
    int tarifa[3] = {5, 2, 1};
    int valor = 7;
    int consumo;

    scanf ("%d", &consumo);

    for (int i = 0; i < 3; i++)
        if (consumo > faixa[i])
            valor += (consumo - faixa[i]) * tarifa[i], consumo = faixa[i];
    
    printf ("%d\n", valor);

    return 0;
}

