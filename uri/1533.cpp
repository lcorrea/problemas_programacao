#include <bits/stdc++.h>

using namespace std;

struct susp
{
	int id;
	int s;
};

bool cmp (struct susp a, struct susp b)
{
	return a.s > b.s;
}

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		struct susp ss[n];
		int m = 0;

		for (int i = 0; i < n; i++)
			scanf ("%d", &ss[i].s), ss[i].id = i, m = max (ss[i].s, m);

		sort (ss, ss+n, cmp);

		int i = 0;
		while (ss[i].s == m) i++;
		printf ("%d\n", ss[i].id+1);

	}
	
	return 0;
}


