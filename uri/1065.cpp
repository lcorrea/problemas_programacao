/*
	problem 1065
	Even between five numbers
	by lucas correa
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int val, count = 0;

	for(unsigned int i = 0; i < 5; i++)
	{
		cin >> val;
		if(val % 2 == 0)
			count++;
	}

	cout << count << " valores pares" << endl;

	return 0;
}

