#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, m;
  const int bills[6] = { 2, 5, 10, 20, 50, 100};

  while (scanf ("%d%d", &n, &m), n and m)
  {
    int change = m - n;
    bool ok = false;

    for (int i = 0; !ok and i < 6; i++)
      for (int j = 0; !ok and j < 6; j++)
        if (i != j)
          ok = change == (bills[i] + bills[j]);

    puts (ok ? "possible" : "impossible");
  }

  return 0;
}

