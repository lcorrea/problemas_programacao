#include <iostream>

using namespace std;

int main (void)
{
	int n;
	string number;

	cin >> n;

	while (n--)
	{
		cin >> number;

		if (number.length() > 3)
			cout << 3 << endl;
		else if (number == "one")
			cout << 1 << endl;
		else if (number == "two")
			cout << 2 << endl;
		else
		{
			const char one[] = "one";
			int swaps = 0;

			for (int i = 0; i < 3; i++)
				if (number[i] != one[i])
					swaps++;

			if (swaps == 1)
				cout << 1 << endl;
			else
				cout << 2 << endl;
		}
	}

	return 0;
}

