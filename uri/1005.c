/*
	problem 1005
	average
	by lucas correa
*/
#include <stdio.h>

int main(void)
{

	float a = 0, media = 0;

	while(scanf("%f%*c", &a) != EOF)
	{
		media = a*3.5;
		scanf("%f%*c", &a);
		media += a*7.5;
		printf("MEDIA = %.5f\n", media/11);
	}

	return 0;
}
