#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, x;
	map <int, int> rep;
	map <int, int>::const_iterator it;

	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
		scanf ("%d", &x), rep[x]++;

	for (it = rep.begin(); it != rep.end(); it++)
		printf ("%d aparece %d vez(es)\n", it->first, it->second);

	return 0;
}

