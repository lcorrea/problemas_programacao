/*
	problem 1035
	selection test 1
	by lucas correa
	time: 00:08:00
*/
#include <iostream>

using namespace std;

int main(void)
{
	int a,b,c,d;

	cin >>  a >> b >> c >> d;

	if( b > c )
	{
		if(d > a)
		{
			if(c + d > a + b)
			{
				if(c >= 0)
				{
					if(d >= 0)
					{
						if(a%2 == 0)
						{
							cout << "Valores aceitos" << endl;
							return 0;
						}
					}
				}
			}
		}
	}
	
	cout << "Valores nao aceitos" << endl;

	return 0;
}
