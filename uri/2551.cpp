#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        int t, d;
        double vm;

        scanf ("%d %d", &t, &d);

        vm = d / (double) t;

        puts ("1");
        for (int i = 2; i <= n; i++)
        {
            scanf ("%d %d", &t, &d);

            double v = d / (double) t;

            if (vm < v)
            {
                vm = v;
                printf ("%d\n", i);
            }
        }

    }

    return 0;
}

