#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int maxs = 0;
    int mat[3][3], sol[3][3], dpsum = 0, dssum = 0;

    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            scanf("%d", &mat[i][j]), sol[i][j] = mat[i][j];

    for (int i = 0; i < 3; i++) {
        int linsum = 0, colsum = 0;

        for (int j = 0; j < 3; j++)
            linsum += mat[i][j];

        for (int j = 0; j < 3; j++)
            colsum += mat[j][i];

        dpsum += mat[i][i];
        dssum += mat[i][2-i];

        maxs = max(maxs, max(colsum, linsum));
    }

    memcpy(sol, mat, sizeof(sol));
    maxs = max(maxs, max(dpsum, dssum));

    if (!dpsum) {
        sol[0][0] = (mat[1][2] + mat[2][1]) >> 1;
        sol[1][1] = (mat[0][2] + mat[2][0]) >> 1;
        sol[2][2] = (mat[0][1] + mat[1][0]) >> 1;
    } else if (!dssum) {
        sol[0][2] = (mat[1][0] + mat[2][1]) >> 1;
        sol[1][1] = (mat[0][1] + mat[2][1]) >> 1;
        sol[2][0] = (mat[1][2] + mat[0][1]) >> 1;
    } else {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!mat[i][j]) {
                    int count;

                    if (i == j) {
                        count = (!mat[0][0]) + (!mat[1][1]) + (!mat[2][2]);
                        if (count == 1) {
                            sol[i][j] = maxs - dpsum;
                            continue;
                        }
                    }

                    if (2 - i == j) {
                        count = (!mat[0][2]) + (!mat[1][1]) + (!mat[2][0]);
                        if (count == 1) {
                            sol[i][j] = maxs - dssum;
                            continue;
                        }
                    }

                    count = (!mat[i][0]) + (!mat[i][1]) + (!mat[i][2]);

                    if (count == 1) {
                        sol[i][j] = maxs - mat[i][0] - mat[i][1] - mat[i][2];
                        continue;
                    }

                    sol[i][j] = maxs - mat[0][j] - mat[1][j] - mat[2][j];
                } else {
                    sol[i][j] = mat[i][j];
                }
            }
        }
    }

    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            printf("%d%c", sol[i][j], j == 2 ? '\n' : ' ');

    return 0;
}

