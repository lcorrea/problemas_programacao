#include <stdio.h>
#include <iostream>
#include <ctype.h>
#include <string.h>
#include <algorithm>
#include <stack>

using namespace std;

bool divBy0 = false;
bool exprValid = true;

int calc(int a, int b, char op)
{
	switch (op)
	{
		case ('-'): return a - b; break;
		case ('+'): return a + b; break;
		case ('*'): return a * b; break;
		default:
			if (b != 0) return a / b;
			else divBy0 = true;
		break;
	}

	return 0;
}

int main (void)
{
	for (; ;)
	{
		divBy0 = false;
		exprValid = true;
		char line[2000010];
		cin.getline (line, 2000009);

		if (cin.eof()) break;
		int len = strlen (line);
		
		if (len == 0) {puts ("Invalid expression."); continue; }

		stack <int> nums;
		int num = 0;

		reverse (line, line + len);

		char *digit = strtok (line, " ");

		while (digit != NULL and exprValid)
		{
			num++;
			if (isdigit (*digit)) nums.push (*digit - '0');
			else
			{
				if (nums.size() > 1)
				{
					int a = nums.top(); nums.pop();
					int b = nums.top(); nums.pop();
					int result = calc (b, a, *digit);
					nums.push (result);
				}
				else { exprValid = false;}
			}

			digit = strtok (NULL, " ");
		}
		if ((exprValid == false) or (nums.size() > 1 or nums.size() == 0) or (num % 2 == 0)) { puts ("Invalid expression."); exprValid = true; }
		else if (divBy0 and exprValid and num % 2) { puts ("Division by zero."); divBy0 = false; }
		else
			printf ("The answer is %d.\n", nums.top());
	}

	return 0;
}

