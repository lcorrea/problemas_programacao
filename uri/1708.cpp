#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int a, b;
    double time;

    scanf ("%d %d", &a, &b);

    time = a*b / (double) (b-a);

    a = ceil(time/a);

    printf ("%d\n", a);

    return 0;
}

