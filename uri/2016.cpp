#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    const int cartoes[5] = {1, 10, 100, 1000, 10000};
    int n, m, daedalus = 0, maxP = 0;

    scanf ("%d %d", &n, &m);

    while (m--)
    {
        int b, d, s = 0;

        scanf ("%d %d", &b, &d);

        for (int i = 1, j; i < n; i++)
            scanf ("%d", &j), s += j;

        if (d + s <= b)
            daedalus += d;

        d = 0;
        for (int i = 0; i < 5; i++)
            if (s + cartoes[i] <= b)
                d = cartoes[i];
        
        maxP += d;
    }

    int extra = maxP - daedalus;

    printf ("%d\n", extra);

    return 0;
}

