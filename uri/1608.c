/*
	problem 1608
	Maria's cakes
	by Lucas Correa
*/
#include <stdio.h>

int main(void)
{
	unsigned int D = 0;
	unsigned int cake_price = 0;
	unsigned int max = 0;

	unsigned short int T = 0, I = 0, B = 0;
	unsigned short int preco_ingr[1000], ingr_nece[1000];
	unsigned short int i = 0;
	unsigned short int ingr_dist = 0;
	
	unsigned short int c = 0;

	scanf("%hd%*c", &T);
	while(T--)
	{
		scanf("%d %hd %hd%*c", &D, &I, &B);

		//---ingredientes
		for(i = 0; i < I; i++)
			scanf("%hd%*c", preco_ingr + i);

		while(B--)
		{
			scanf("%hd%*c", &ingr_dist);
			
			while(ingr_dist--)
			{
				scanf("%hd%*c", &c);
				scanf("%hd%*c", ingr_nece + c);
				
				cake_price += preco_ingr[c]*ingr_nece[c];
			}
			
			for(i = 1; (cake_price*i) <= D;i++)
				if(i > max)
					max = i;
			
			cake_price = 0;
		}
		printf("%d\n", max);
		max = 0;
	}

	return 0;
}
