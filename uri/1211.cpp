#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(void)
{
	unsigned int n, count, i, j; 
	string num, last;
	vector<string> phonebook;
	vector<string>::const_iterator it;
	
	cin >> n;

	while(!cin.eof())
	{
		phonebook.clear();
		count = 0;

		for(i = 0; i < n; i++)
		{
			cin >> num;
			phonebook.push_back(num);
		}

		sort(phonebook.begin(), phonebook.end());
		it = phonebook.begin();

		last = *it;

		for(j = 1; j < n; j++)
		{
			num = *(++it);
			i = 0;

			while(i < num.length())
			{
				if(num[i] != last[i])
					break;

				count++;
				i++;
			}

			last = num;
		}

		cout << count << endl;

		cin >> n;
	}

	return 0;
}

