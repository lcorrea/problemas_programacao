#include <bits/stdc++.h>

using namespace std;

int tempo[100], pizzas[100], tab[100][100];
int n;

int knapsack (int pedido, int faltando)
{
	if (tab[pedido][faltando] >= 0)
		return tab[pedido][faltando];

	if(pedido > n or !faltando)
		return tab[pedido][faltando] = 0;
	
	int nao_atende = knapsack(pedido+1, faltando);

	if (pizzas[pedido] <= faltando)
	{
		int atende = tempo[pedido] + knapsack(pedido+1, faltando - pizzas[pedido]);

		return tab[pedido][faltando] = max(atende, nao_atende);
	}

	return tab[pedido][faltando] = nao_atende;
}

int main (void)
{
	while (cin >> n, n)
	{
		int p;

		memset (tab, -1, sizeof(tab));

		cin >> p;

		for (int i = 1; i <= n; i++)
			cin >> tempo[i] >> pizzas[i];

		cout << knapsack(1, p) << " min." << endl;
	}

	return 0;
}

