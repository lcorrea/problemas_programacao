#include <stdio.h>

int main (void)
{
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		char line[110], c;
		int len = 0;

		while ((c = getchar()) != '\n') line[len++] = c;
		line[len] = '\0';

		int midlle = len/2 - 1;
		while (midlle >= 0)
			putchar (line[midlle--]);

		int end = len/2;

		midlle = len-1;
		while (midlle >= end)
			putchar (line[midlle--]);

		putchar ('\n');
	}

	return 0;
}

