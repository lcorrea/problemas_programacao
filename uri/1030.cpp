#include <bits/stdc++.h>

using namespace std;

int compute (int n, int k)
{
    if (n == 1) return 0;

    return (compute (n - 1, k) + k)%n;
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    for (int i = 1; i <= t; i++)
    {
        int n, k;
        scanf ("%d%d", &n, &k);
        printf ("Case %d: %d\n", i, compute (n, k) + 1);
    }
    
	return 0;
}

