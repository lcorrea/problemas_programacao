#include <bits/stdc++.h>
#include <cmath>

using namespace std;

int main (void)
{
  int n;

  scanf ("%d", &n);

  double ln = log (n);
  double a = n / ln;

  printf ("%.1lf %.1lf\n", a, 1.25506 * a);

  return 0;
}

