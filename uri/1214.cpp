#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int c;

	cin >> c;

	while (c--)
	{
		int n;
		double average = 0;
		int above = 0;
		int grade[2000];

		cin >> n;

		for (int i = 0; i < n; i++)
			cin >> grade[i], average += grade[i];

		average = average / (double) n;

		for (int i = 0; i < n; i++)
			above += grade[i] > average;

		printf ("%.03lf%%\n", (above/(double) n)*100);
	}

	return 0;
}

