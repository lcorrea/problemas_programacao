#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

#define FINALIZED 0b100
#define REACHABLE 0b010
#define VISITED 0b001
#define CYCLE -1

int dfs(int u, int I, int N, Graph &graph, vector <int> &stat)
{
    stat[u] |= VISITED;

    if (u >= (I+N+1))
    {
        stat[u] |= FINALIZED | REACHABLE;
        
        return stat[u];
    }

    for (auto v : graph[u])
    {
        if (!(stat[v] & VISITED))
        {
            int s = dfs(v, I, N, graph, stat);

            if (s == CYCLE)
                return CYCLE;

            stat[u] |= (s & REACHABLE);
        }
        else if (stat[v] & FINALIZED)
        {
            if (stat[v] & REACHABLE)
                stat[u] |= REACHABLE;
        }
        else
            return CYCLE;
    }

    stat[u] |= FINALIZED;

    return stat[u];
}

int main (void)
{
    int I, N, O;

    scanf ("%d %d %d", &I, &N, &O);

    Graph graph(I+N+O+1);

    for (int i = I+1, len = N+I; i <= len; i++)
    {
        int k;

        scanf ("%d", &k);

        for (int j = 0; j < k; j++)
        {
            int input;

            scanf ("%d", &input);
            graph[input].push_back(i);
        }
    }

    for (int o = N+I+1, len = N+I+O; o <= len; o++)
    {
        int input;

        scanf ("%d", &input);

        graph[input].push_back(o);
    }

    vector <int> stat(I+N+O+1, 0);
    for (int i = 1; i <= I; i++)
        if (CYCLE == dfs(i, I, N, graph, stat))
        {
            puts ("u.u");
            return 0;
        }

    for (int i = 1, len = N+I+O; i <= len; i++)
        if (!(stat[i] & (REACHABLE)))
        {
            puts ("u.u");
            return 0;
        }

    puts ("o.o");

    return 0;
}

