#include <stdio.h>

float alunos[1000001];

int main (void)
{
	int n, id, maxid = 0;
	float nota;

	scanf ("%d", &n);
	
	while (n--)
	{
		scanf ("%d", &id);
		scanf ("%f", &alunos[id]);

		if (alunos[maxid] < alunos[id])
			maxid = id;
	}

	if(alunos[maxid] >= 8) printf ("%d\n", maxid);
	else puts ("Minimum note not reached");

	return 0;
}

