/*
	problem 1044
	multiples
	by lucas correa
	time: 00:07:05
*/
#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main(void)
{
	int a, b;

	cin >> a >> b;

	if(b > a)
		swap(a,b);

	a = fabs(a);
	b = fabs(b);

	if(a%b)
		cout << "Nao sao Multiplos";
	else
		cout << "Sao Multiplos";
	
	cout << endl;

	return 0;
}

