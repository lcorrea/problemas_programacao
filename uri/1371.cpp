#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	int bs[10001];

	for (int i = 1, j = 1; j <= 10000; j++, i++)
		bs[j] = i*i;
	
	while(scanf ("%d", &n), n)
	{
		int i = 2;
		printf ("1");

		while (bs[i] <= n)
			printf (" %d", bs[i++]);

		putchar ('\n');
	}

	return 0;
}

