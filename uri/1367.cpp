#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (cin >> n, n)
	{
		int s = 0, total = 0;
		int incorrect[256] = {0};

		while (n--)
		{
			int t;
			char p;
			string veredict;

			cin >> p >> t >> veredict;

			if (veredict[0] == 'c')
				total += t + 20*(incorrect[p]), s++;
			else
				incorrect[p]++;
		}

		cout << s << ' ' << total << endl;
	}

	return 0;
}

