#include <iostream>
#include <algorithm>

using namespace std;

int main (void)
{
	string cpf;

	cin >> cpf;

	while (!cin.eof ())
	{
		int sum = 0;
		cout << "CPF ";
		for (int i = 0, j = 1; i < 11; i++)
		{
			if (cpf[i] == '.') continue;

			sum += (cpf[i] - '0')*j, j++;
		}
		
		int digit = cpf[12] - '0';
		sum = sum % 11;

		if ((sum == 10 and digit == 0) or digit == sum)
		{
			sum = 0;
			for (int i = 10, j = 1; i > -1; i--)
			{
				if (cpf[i] == '.') continue;
				sum += (cpf[i] - '0')*j, j++;
			}

			digit = cpf[13] - '0';
			sum = sum % 11;

			if ((sum == 10 and digit == 0) or digit == sum)
				cout << "valido";
			else
				cout << "invalido";
		}
		else
			cout << "invalido";
		cout << endl;

		cin >> cpf;
	}

	return 0;
}

