#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int a, d, h;
	int cardsA[20], cardsD[20], cardsH[20];
	bool found = false;

	scanf ("%d%d%d%d", &n, &a, &d, &h);

	for (int i = 0; i < n; i++)
	{
		scanf ("%d%d%d", cardsA + i,
				cardsD + i,
				cardsH + i);
	}

	for (int i = 0, limit = (1 << n); i < limit; i++)
	{
		int sumA = 0, sumD = 0, sumH = 0;
		int cnt = 0;

		for (int j = 0; j < n; j++)
		{
			if (i  & (1 << j))
			{
				sumA += cardsA[j];
				sumD += cardsD[j];
				sumH += cardsH[j];
				cnt++;

				if (sumA > a) break;
				if (sumD > d) break;
				if (sumH > h) break;
			}

			if(sumA == a and sumD == d and sumH == h and cnt > 1)
			{
				found = true;
				break;
			}
		}
	}

	puts (found ? "Y" : "N");

	return 0;
}

