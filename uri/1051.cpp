/*
	problem 1051
	taxes
	by lucas correa
*/
#include <iostream>
#include <cstdio>

using namespace std;

float desconto(float *salary, const float teto, const float taxe)
{
	float desc = 0;

	if(*salary > teto)
	{
		desc = (*salary) - teto;
	
		*salary -= desc;

		desc = taxe*desc;
	}
	
	return desc;
}

int main(void)
{
	float salary, taxe=0.00;
	const float tetos[3] = {4500.00, 3000.00, 2000.00};
	const float taxes[3] = {0.28, 0.18, 0.08};

	cin >> salary;

	if(salary <= 2000.00)
	{
		cout << "Isento" << endl;
		return 0;
	}

	for(unsigned int i = 0; i < 3; i++)
		taxe += desconto(&salary, tetos[i], taxes[i]);

	printf("R$ %.2f\n", taxe);

	return 0;
}

