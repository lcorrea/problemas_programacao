#include <iostream>

using namespace std;

int main (void)
{
	double g1, g2;
	unsigned long long int pa, pb, t, y = 0;

	cin >> t;

	while (t--)
	{
	
		cin >> pa >> pb >> g1 >> g2;

		while (pa <= pb && y <= 100)
		{
			pa += ((double) (g1 / 100.0)) * pa;
			pb += ((double) (g2 / 100.0)) * pb;

			y++;
		}

		if ( y > 100)
			cout << "Mais de 1 seculo." << endl;
		else 
			cout << y << " anos." << endl;

		y = 0;
	}

	return 0;
}

