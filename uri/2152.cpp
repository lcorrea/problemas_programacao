#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;

  scanf ("%d", &n);

  while (n--)
  {
    int h, m, p;

    scanf ("%d %d %d", &h, &m, &p);
    printf ("%02d:%02d - A porta %s!\n", h, m, p ? "abriu" : "fechou");
  }

  return 0;
}

