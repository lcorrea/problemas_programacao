#include <stdio.h>

int win (char a, char b)
{
	if (a == 'u')
	{
		if (b == 'u')
			return 4;

		return 0;
	}

	if (a == 'a')
	{
		if (b == 'u')
			return 1;

		if (b == 'a')
			return 3;

		return 0;
	}

	if (a == 'l')
	{
		if (b == 'l')
			return 2;

		return 1;
	}
}

int main (void)
{
	int n;
	char p1[20], p2[20];
	const char message[5][20] = { "Jogador 1 venceu",
					"Jogador 2 venceu",
					"Ambos venceram",
					"Sem ganhador",
					"Aniquilacao mutua" };

	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%s %s", p1, p2);

		puts (message[win(p1[4], p2[4])]);
	}

	return 0;
}

