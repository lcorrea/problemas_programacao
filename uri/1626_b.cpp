#include<stdio.h>
#include<math.h>

#define MAX 112345
#define P 1000000007
#define insira(pp, mm, f) {f.p[f.k] = pp; f.m[f.k] = mm; f.k++; }

typedef struct { int k, p[MAX], m[MAX]; } fat;
typedef unsigned long long ull;

int PI[MAX], pi;

ull expbin(ull a, ull b) {
	ull y;
	if (b == 0) return 1;
	if (b & 1) return a * expbin(a, b - 1) % P;
	y = expbin(a, b >> 1);
	return y * y % P;
}

void crivo(int n) {
	int i, j, primo, raiz;
	PI[0] = 2; pi = 1;
	for (i = 3; i <= n; i++) {
		primo = 1; raiz = sqrt(i);
		for (j = 0; primo && j < pi && PI[j] <= raiz; j++)
			if (i % PI[j] == 0) primo = 0;
		if (primo) PI[pi++] = i;
	}
}

ull fatorial(int n) {
	if (n == 0) return 1;
	return n * fatorial(n - 1) % P;
}

int main(void) {
	int N, n, i, m;
	ull Nfatorial, prod;
	fat F;
	crivo(MAX);
	while (scanf("%d", &N) != EOF) {
		Nfatorial = fatorial(N);
		F.k = 0;
		for (i = 0; i < pi && PI[i] <= N; i++) {
			for (m = 0, n = N; n; m += n /= PI[i]);
			insira(PI[i], m, F);
		}
		prod = 1;
		for (i = 0; i < F.k; i++)
			prod *= ((expbin(F.p[i], F.m[i] + 1) + P - 1) % P) *
				expbin(F.p[i] - 1, P - 2) % P;
		printf("%llu %llu\n", (prod + P - Nfatorial) % P, Nfatorial);
	}
	return 0;
}
