#include <iostream>
#include <cstring>

using namespace std;

unsigned long long int fib[1000];

unsigned long long int fibonacci (unsigned int x)
{
	if (x)
	{
		if (fib[x])
		{
			return fib[x];
		}
		else 
		{
			fib[x] = fibonacci (x - 1) + fibonacci (x - 2);

			return fib[x];
		}
	}

	return 0;
}

int main (void)
{
	unsigned int n, f;

	memset(fib, 0, 1000 * sizeof (unsigned long long int));
	fib[0] = 0;
	fib[1] = 1;

	cin >> n;

	while (n--)
	{
		cin >> f;
		cout << "Fib(" << f << ") = " << fibonacci (f) << endl;
	}

	return 0;
}

