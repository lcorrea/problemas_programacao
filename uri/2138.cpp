#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[10001];

    while (EOF != scanf ("%s", str))
    {
        int most = 0;
        int freq[10] = {0};

        for (int i = 0; str[i]; i++)
        {
            int digit = str[i]-'0';
            
            ++freq[digit];
        }

        for (int i = 0; i <= 9; i++)
            if (freq[most] <= freq[i])
                most = i;

        printf ("%d\n", most);
    }

    return 0;
}

