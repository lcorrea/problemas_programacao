#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char c = 'a';

	while (c != EOF)
	{
		set <char> dic;

		while ((c = getchar ()), c != EOF and c != '\n')
			if (isalpha(c)) dic.insert (c);

		if (c == EOF) break;

		set <char>::const_iterator it = dic.begin();

		if (dic.size())
		{
			while (it != dic.end())
			{
				c = *it;
				printf ("%c:", c);
				it++;

				while (it != dic.end() and *it == c + 1) c = *it, it++;
				printf ("%c", c);

				if (it != dic.end()) printf (", ");
			}

		}

		putchar ('\n');
	}


	return 0;
}

