/*
	problem 1426
	add bricks in the wall
	by lucas correa
	time: 08:16:00
*/

#include <stdio.h>

int main(void)
{
	unsigned int wall[9][9], n = 0;
	char i,j, offset = 0;

	scanf("%u%*c", &n);
	while(n--)
	{
		for(i = 0; i < 9; i += 2)
		{
			for(j = 0; j <= i; j+=2)
			{
				scanf("%u%*c", &wall[i][j]);
				
				if(i==8 && j)
					wall[i][j-1] = (wall[i-2][j-2] - wall[i][j-2] - wall[i][j])/2;
			}
		}

		for(i=7; i >= 0; i--)
		{
			for(j = 0; j <= i; j++)
			{
				if(j%2 || i%2)
					wall[i][j] = wall[i+1][j] + wall[i+1][j+1];
			}
				
		}

		for(i=0; i < 9; i++)
		{
			for(j=0; j <= i - 1; j++)
			{
				printf("%u ", wall[i][j]);
			}
			printf("%u\n", wall[i][j]);
		}

	}
	return 0;
}
