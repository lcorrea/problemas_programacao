#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int x;

    scanf ("%d", &x);

    if (x & 1)
        printf ("%d\n", x + 1);
    else
        printf ("%d\n", x + 2);

    return 0;
}

