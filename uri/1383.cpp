#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d", &n);

	for (int c = 1; c <= n; c++)
	{
		int tab[9][9] = {0};
		bool check_col = true, check_lin = true, check_subset = true;

		printf ("Instancia %d\n", c);

		for (int i = 0; i < 9; i++)
		{
			int lin[10] = {0};
			for (int j = 0; j < 9; j++)
			{
				scanf ("%d", &tab[i][j]);

				if (lin[tab[i][j]])
					check_lin = false;
				lin[tab[i][j]]++;
			}
		}

		for (int j = 0; j < 9; j++)
		{
			int col[10] = {0};
			for (int i = 0; i < 9; i++)
			{
				if (col[tab[i][j]])
					check_col = false;

				col[tab[i][j]]++;
			}
		}

		for (int i = 0; i < 9; i += 3)
		{
			for (int j = 0; j < 9; j += 3)
			{
				int subset[10] = {0};
				for (int k = i; k < i + 3; k++)
				{
					for (int l = j; l < j + 3; l++)
					{
						if (subset[tab[k][l]])
							check_subset = false;
						subset[tab[k][l]]++;
					}
				}
			}
		}

		if (check_col and check_lin and check_subset) puts ("SIM");
		else puts ("NAO");

		putchar ('\n');
	}

	return 0;
}

