#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    map <int, int> houses;

    int n, m;

    scanf ("%d%d", &n, &m);

    for (int i = 0, num; i < n; i++)
    {
        scanf ("%d", &num);

        houses[num] = i;
    }

    int time = 0;
    int start = 0;

    for (int i = 0, num; i < m; i++)
    {
        scanf ("%d", &num);

        time += abs(houses[num] - houses[start]);

        start = num;
    }

    printf ("%d\n", time);

    return 0;
}

