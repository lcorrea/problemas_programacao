#include <bits/stdc++.h>

using namespace std;

vector <int> path;
int x, y, m[1000][1000], len = 0, total = 0;

bool walkx (int cnt, int inc)
{
    while (cnt--)
    {
        len++;

        if (m[y][x])
            path.push_back(m[y][x]), total--;

        if (total == 0)
            return false;

        x += inc;
    }

    return true;
}

bool walky (int cnt, int inc)
{
    while (cnt--)
    {
        len++;

        if (m[y][x])
            path.push_back(m[y][x]), total--;

        if (total == 0)
            return false;

        y += inc;
    }

    return true;
}

int main (void)
{
    int offset = 500;
    int n;

    scanf ("%d %d %d", &n, &y, &x); getchar();
    
    total = n*n;

    for (int i = offset, end = offset + n, cnt = 1; i < end; i++)
        for (int j = offset; j < end; j++)
            m[i][j] = cnt++;

    bool run = true;
    int cnt = 1;

    x += offset;
    y += offset;

    while (run)
    {
        run = walkx(cnt, 1);

        if (run)
            run = walky(cnt++, 1);

        if (run)
            run = walkx(cnt, -1);

        if (run)
            run = walky(cnt++, -1);
    }

    printf ("%d", path[0]);
    for (int i = 1, sz = (int) path.size(); i < sz; i++)
        printf (" %d", path[i]);
    putchar ('\n');

    printf ("%d\n", len);

    return 0;
}

