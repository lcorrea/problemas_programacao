#include <stdio.h>
#include <math.h>

int main(void)
{
	int col=1, lin=1, mat;
	
	while(scanf("%d", &mat))
	{
		if(mat == 0)
			break;
		if(mat == 1)
		{
			printf("  1\n\n");
			continue;
		}

		char x=1;
		printf("  1");
		for(col = 2; col <= mat; col++)	//
			printf("   1");		//
		printf("\n");			//
		
		for(lin = 2;lin <= (mat-1);lin++)
		{
			printf("  1");
			for(col = 2; col <= mat-1; col++)
			{
				

				x = fmin(fmin(fabs(col-1), fabs(col-mat)), fmin(fabs(lin-mat),fabs(lin-1))) + 1;

				printf(" %3d", x);

			}
			printf("   1\n");

	
		}
		
		printf("  1");
		for(col = 2; col <=mat; col++)	//
			printf("   1");		//
		printf("\n\n");			//
	}
	
	return 0;
}
