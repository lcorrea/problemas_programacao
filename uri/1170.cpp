/*
	problem 1170
	blobs
	by lucas correa
	time: 00:10:23
*/
#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, dia;
	float supply = 0;

	cin >> n;

	while(n--)
	{
		dia = 0;
		
		cin >> supply;
		
		while(supply > 1.0)
		{
			dia++;
			supply /= 2;
		}

		cout << dia << " dias" << endl;
	}

	return 0;
}
