#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	bool a,b;
	int n, cnt1 = 0, cnt2 = 0;
	int f1[5001], f2[5001];

	while (scanf ("%d", &n), n)
	{
		cnt1 = cnt2 = 0;
		a=true, b=false;

		for (int i = 0; i < n; i++)
		{
			scanf ("%d", &f1[i]), f2[i] = f1[i];

			if (f1[i]) a = true, b = true;

			if (!f1[i] and a) cnt1++, f1[i] = a;
			if (!f2[i] and b) cnt2++, f2[i] = b;

			a = !a; b = !b;
		}

		if (!f1[0] and !f1[n-1]) cnt1++;
		if (!f2[0] and !f2[n-1]) cnt2++;

		printf ("%d\n", min (cnt1, cnt2));
	}

	return 0;
}

