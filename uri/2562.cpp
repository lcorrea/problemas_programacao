#include <bits/stdc++.h>

using namespace std;

int dfs (int u, vector <int> &seen, vector <vector <int> > &graph)
{
    int cnt = 1;
    seen[u] = 1;

    for (int i = 0; i < (int) graph[u].size(); i++)
    {
        int v = graph[u][i];

        if (!seen[v])
            cnt += dfs(v, seen, graph);
    }

    return cnt;
}

int main (void)
{
    int n, m;

    while (scanf ("%d %d", &n, &m) != EOF)
    {
        vector <vector <int> > graph(n);

        for (int i = 0; i < m; i++)
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            graph[a-1].push_back(b-1);
            graph[b-1].push_back(a-1);
        }

        int s;

        scanf ("%d", &s);

        vector <int> seen(n, 0);

        printf ("%d\n", dfs(s-1, seen, graph));
    }

    return 0;
}

