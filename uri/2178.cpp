#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, p;

    scanf("%d %d", &n, &p);

    int ans = 1;

    for (int i = 0; i < n; i++) {
        
        int k, s, v = 1;
        
        scanf("%d %d", &k, &s);
        
        for (int i = 1; i < k; i++) {
            
            int a;
            
            scanf("%d", &a);
            
            if (a <= s)
                v++;
            
            s = a;
        }

        ans = max(v - (s == 0), ans);
    }

    printf("%d\n", ans);

    return 0;
}

