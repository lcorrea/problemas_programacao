#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    set <string> friends, notfriends;
    set <string>::const_iterator it;
    string name, op;
    string chosen;

    while (cin >> name, name != "FIM")
    {
        cin >> op;

        if (op == "YES")
        {
            chosen = name.size() > chosen.size() ? name : chosen;
            friends.insert (name);
        }
        else
            notfriends.insert(name);

    }

    for (it = friends.begin(); it != friends.end(); it++)
        printf ("%s\n", it->c_str());

    for (it = notfriends.begin(); it != notfriends.end(); it++)
        printf ("%s\n", it->c_str());
    
    printf ("\nAmigo do Habay:\n%s\n", chosen.c_str());

    return 0;
}

