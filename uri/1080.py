#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout
    
    ans = (0, 0)
    
    for n in zip(map(int, stdin.readlines()), range(1, 101)):
        if (ans < n):
            ans = n

    n, p = ans
    
    stdout.write("{:d}\n{:d}\n".format(n, p))
    # --- #

if __name__ == "__main__":
    main()

