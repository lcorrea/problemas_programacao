#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int main(void)
{
	float a,b,c, r1,r2, d;

	cin >> a >> b >> c;

	if(a)
	{
		if((d = b*b - 4.0*a*c) >= 0.000000000)
		{
			r1 = ((-b + sqrt(d))/(2.0*a));
			r2 = ((-b - sqrt(d))/(2.0*a));

			printf("R1 = %.5f\nR2 = %.5f\n", r1, r2);
			return 0;
		}

	}

	cout << "Impossivel calcular" << endl;

	return 0;
}

