/*
	problem 1228
	start grid
	by lucas correa
	time: 01:13:51
*/
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	vector<unsigned short int> start(24), finish(24);
	vector<unsigned short int>::iterator i, p;
	unsigned short int n = 0, overtakes = 0, tmp = 0, tmp2 = 0;

	cin >> n;

	while(!cin.eof())
	{
		for(i = start.begin(); i - start.begin() < n; i++)
			cin >> *i;
		
		for(i = finish.begin(); i - finish.begin() < n; i++)
			cin >> *i;

		for(i = finish.begin(); i - finish.begin() < n; i++)
		{
			p = find(start.begin(), start.end(), *i);

			if(p - start.begin() > i - finish.begin())
			{
				while(i - finish.begin() != p - start.begin())
				{
					overtakes++;
					/*
					tmp = *p;
					*p = *(p-1);
					*(p-1) = tmp;*/
					swap_ranges(p-1, p, p);
					--p;
				}

			}
			else if(p - start.begin() < i - finish.begin())
			{
				while(i - finish.begin() != p - start.begin())
				{
					overtakes++;
					/*tmp = *p;
					*p = *(p+1);
					*(p+1) = tmp;*/
					swap_ranges(p, p+1, p);
					++p;
				}
			}
		}
		
		cout << overtakes << endl;
					
		cin >> n;

		overtakes = 0;
	}

	return 0;
}
