#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int a, b, c;

    while (scanf("%d %d %d", &a, &b, &c), a and b and c) {
        int x = pow(10, log10(a*b*c) / 3.0);
        printf("%d\n", x);
    }

    return 0;
}

