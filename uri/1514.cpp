#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    while (scanf("%d%d", &n, &m), n and m)
    {
        int suml[101] = {0}, sumc[101] = {0};
        int contest[101][101];
        int ans = 0;

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                scanf ("%d", &contest[i][j]), suml[i] += contest[i][j];

        for (int j = 0; j < m; j++)
            for (int i = 0; i < n; i++)
                sumc[j] += contest[i][j];

        bool cnd1 = true;
        bool cnd4 = true;
        for (int i = 0; i < n; i++)
        {
            if (suml[i] == 0) cnd4 = false;
            if (suml[i] == m) cnd1 = false;
        }

        bool cnd2 = true;
        bool cnd3 = true;
        for (int j = 0; j < m; j++)
        {
            if (sumc[j] == 0) cnd2 = false;
            if (sumc[j] == n) cnd3 = false;
        }

        printf ("%d\n", cnd1 + cnd2 + cnd3 + cnd4);
    }

	return 0;
}

