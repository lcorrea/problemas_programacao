#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf ("%d", &n) != EOF)
    {
        int m, l;

        scanf ("%d %d", &m, &l);

        int attm[m][n], attl[l][n];

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                scanf ("%d", &attm[i][j]);

        for (int i = 0; i < l; i++)
            for (int j = 0; j < n; j++)
                scanf ("%d", &attl[i][j]);

        int cm, cl;

        scanf ("%d %d", &cm, &cl);

        int a;

        scanf ("%d", &a);

        int vm = attm[cm-1][a-1];
        int vl = attl[cl-1][a-1];

        if (vm == vl)
            puts ("Empate");
        else if (vm > vl)
            puts ("Marcos");
        else
            puts ("Leonardo");
    }

    return 0;
}

