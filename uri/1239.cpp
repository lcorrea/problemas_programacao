#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char ch;
	stack <char> tag;
	string text;

	while ((ch = getchar()) != EOF)
	{
		if (ch == '*' or ch == '_')
		{ 
			if (!tag.empty() and tag.top() == ch)
			{
				if (ch == '*')
					cout  << "</b>";
				else
					cout << "</i>";
				tag.pop();
			}
			else
			{
				tag.push(ch);

				if (ch == '*')
					cout << "<b>";
				else
					cout << "<i>";
			}
		}
		else cout << ch;
	}

	return 0;
}

