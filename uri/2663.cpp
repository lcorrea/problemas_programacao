#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    scanf ("%d %d", &n, &k);

    int v[n], cnt[1001] = {0};

    for (int i = 0; i < n; i++)
        scanf ("%d", &v[i]), v[i] = -v[i];
    sort (v, v + n);

    int r = k;
    for (int i = k; i < n; i++)
        if(v[k-1] == v[i])
            r++;

    printf ("%d\n", r);

    return 0;
}

