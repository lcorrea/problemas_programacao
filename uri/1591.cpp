#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>

using namespace std;

int main (void)
{
	int n, l, c, p;

	cin >> n;

	while (n--)
	{
		cin >> l >> c;

		vector <string> palavras(l+c);
		
		for (int i = 0; i < l; i++)
			cin >> palavras[i];

		for (int i = l, j = 0; j < c; j++, i++)
			for (int k = 0; k < l; k++)
				palavras[i] += palavras[k][j];
		cin >> p;

		while (p--)
		{
			string pergunta;
			int cnt = 0, sz = l + c;

			cin >> pergunta;

			if (pergunta.length() == 1)
				sz = l;

			for (int i = 0; i < sz; i++)
			{
				char *word = strdup (palavras[i].c_str());
				const char *substr = strstr (word, pergunta.c_str());
				
				while (substr != NULL)
				{
					cnt++;
					substr = strstr (substr+1, pergunta.c_str());
				}
			}

			cout << cnt << endl;
		}
	}

	return 0;
}

