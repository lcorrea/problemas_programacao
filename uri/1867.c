#include <stdio.h>
#include <string.h>

int sum (char *ptr)
{
	int s=0;
	while (*ptr)
		s += (*ptr) - '0', ptr++;
	return s;
}

int calc (int n)
{
	if (n < 10) return n;
	char num[110];
	sprintf (num, "%d", n);
	return calc(sum(num));
}

int main (void)
{
	char n[110], m[110];

	while (scanf ("%s %s", n, m), n[0] != '0' || m[0] != '0')
	{
		int a = calc(sum(n));
		int b = calc(sum(m));
		if (a > b) puts ("1");
		else if (a < b) puts ("2");
		else puts ("0");
	}

	return 0;
}

