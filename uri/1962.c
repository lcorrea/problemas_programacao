#include <stdio.h>

#define calc(a) ((a>=2015ULL)?(a - 2014ULL):(2015ULL - a))

int main (void)
{
	unsigned long long int t;
	int n;

	scanf ("%d",&n);

	while (n--)
	{
		scanf ("%llu", &t);
		printf ("%llu ", calc (t));
		if (t < 2015) puts ("D.C.");
		else puts ("A.C.");
	}
	return 0;
}

