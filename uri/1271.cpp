#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, g = 1;

	while (cin >> n, n)
	{
		int r, q;

		cin >> r;

		pair <int, int> op[r];

		for (int i = 0; i < r; i++) 
			cin >> op[i].first >> op[i].second;

		cin >> q;

		printf ("Genome %d\n", g++);

		for (int i = 0; i < q; i++)
		{
			int qq, p;

			cin >> qq; p = qq;

			for (int j = 0; j < r; j++)
				if (p >= op[j].first and p <= op[j].second)
					p = op[j].second - (p - op[j].first);

			printf ("%d\n", p);
		}
	}

	return 0;
}

