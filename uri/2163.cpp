#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  const int dx[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
  const int dy[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
  int p[1001][1001], n, m;

  scanf ("%d%d", &n, &m);

  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      scanf ("%d", &p[i][j]);

  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      if (p[i][j] == 42)
      {
        int cnt = 0;

        for (int k = 0, x, y; k < 8; k++)
        {
          x = j + dx[k];
          y = i + dy[k];

          if (x >= 0 and x < m and y >= 0 and y < n)
            if (p[y][x] == 7)
              cnt++;
            else
              break;
          else
            break;
        }

        if (cnt == 8)
        {
          printf ("%d %d\n", i + 1, j + 1);

          return 0;
        }
      }

  puts ("0 0");

  return 0;
}

