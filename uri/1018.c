/*
	problem 1018
	banknotes
	by lucas correa
	time: 0:23:20

*/
#include <stdio.h>

int main(void)
{
	unsigned int n = 0;
	unsigned int i = 0, banknotes[] = { 100, 50, 20, 10, 5, 2, 1};
	scanf("%u%*c", &n);

	printf("%u\n", n);

	while(i < 7)
	{
		printf("%u nota(s) de R$ %u,00\n", n/banknotes[i], banknotes[i]);
		n -= (n/banknotes[i])*banknotes[i];
		
		i++;
	}
	
}
