#include <bits/stdc++.h>

using namespace std;

#define MOD 1000000007ULL

typedef vector <int> Vi;
typedef vector < Vi > Graph;

int n;
Graph s;

bool isSolution (Vi &sol)
{
    Vi reached(n, 0);
    int cnt = 0;

    for (int i = 0; i < (int) sol.size(); i++)
    {
        int v = sol[i];

        if (reached[v] == 0)
            reached[v] = 1, cnt += 1;

        for (int j = 0; j < (int) s[v].size(); j++)
            if (reached[s[v][j]] == 0)
                reached[s[v][j]] = 1, cnt += 1;
    }

    //printf ("isSolution: %s\n", cnt == n ? "yes" : "no"); 

    return cnt == n;
}

int modpow2 (int x)
{
    if (x == 0)
        return 1;

    if (x%2)
        return (modpow2 (x-1) << 1) % MOD;

    int b = modpow2(x >> 1);

    return (b*b) % MOD;
}

int backtracking (int u, Vi &sol)
{
    if (u == n)
        return 0;

    if (isSolution(sol))
        return modpow2(n - u - 1);

    sol.push_back(u);

    printf ("u = %d\nsol = ", u+1);
    for (int i = 0; i < (int) sol.size(); i++)
        printf ("%d ", sol[i]+1);
    putchar('\n');

    int cnt = 0;

    for (int i = u + 1; i < n; i++)
        cnt = (cnt % MOD + (backtracking(i, sol) % MOD)) % MOD;

    sol.pop_back();

    return cnt;
}

int main ()
{
    while (scanf ("%d", &n) != EOF)
    {
        s.clear();
        s.resize(n);

        for (int i = 0; i < n - 1; i++)
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            s[a-1].push_back(b-1);
            s[b-1].push_back(a-1);
        }

        int ans = 0;
        Vi sol;

        for (int i = 0 ; i < n; i++)
            ans = (ans + (backtracking(i, sol) % MOD)) % MOD;

        printf ("%d\n", ans);
    }

    return 0;
}
