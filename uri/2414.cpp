#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int m = 0, n;

    while (scanf("%d", &n), n) {
        m = max(n, m);
    }

    printf("%d\n", m);

    return 0;
}

