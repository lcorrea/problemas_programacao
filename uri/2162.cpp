#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  int l[100];
  bool ok = true;

  scanf ("%d", &n);

  for (int i = 0; i < n; i++)
    scanf ("%d", l + i);

  int p = l[1] - l[0];
  
  if (p==0)
    ok = false;

  p = p < 0 ? -1 : 1;

  for (int i = 2; ok and i < n; i++)
  {
    if (l[i] == l[i-1])
    {
      ok = false;
      break;
    }

    if (p == -1 and l[i] < l[i-1])
      ok = false;
    if (p == 1 and l[i] > l[i-1])
      ok = false;

    p = l[i] > l[i-1] ? 1 : -1;
  }

  printf ("%d\n", ok);

  return 0;
}

