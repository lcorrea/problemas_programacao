#include <cstdio>

using namespace std;

int main (void)
{

	int a, b;
	int pta, ptb, n;

	while (scanf ("%d", &n), n)
	{
		pta=0;
		ptb=0;
		while (n--)
		{
			scanf ("%d %d", &a, &b);

			(a != b)?(pta+=a>b, ptb+=a<b):(a=b);
		}

		printf ("%d %d\n", pta, ptb);
	}

	return 0;
}

