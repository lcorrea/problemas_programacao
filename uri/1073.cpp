/*
	problem 1073
	even square
	by lucas correa
*/
#include <cstdio>

using namespace std;

int main(void)
{
	unsigned int n;
	
	scanf("%u%*c", &n);

	for(unsigned int i = 2; i <= n; i += 2)
		printf("%u^2 = %u\n", i, i*i);

	return 0;
}

