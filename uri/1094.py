#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout
    
    n = int(stdin.readline())

    r, s, c = 0, 0, 0

    for i in range(n):
        qnt, animal = stdin.readline().split()

        qnt = int(qnt)

        if (animal == 'R'):
            r += qnt
        elif (animal == 'S'):
            s += qnt
        else:
            c += qnt

    total = r + c + s
    stdout.write("Total: %d cobaias\n" % (total))
    stdout.write("Total de coelhos: %d\n" % (c))
    stdout.write("Total de ratos: %d\n" % (r))
    stdout.write("Total de sapos: %d\n" % (s))
    stdout.write("Percentual de coelhos: %.02f %%\n" % (c / total * 100.0))
    stdout.write("Percentual de ratos: %.02f %%\n" % (r / total * 100.0))
    stdout.write("Percentual de sapos: %.02f %%\n" % (s / total * 100.0))
        # --- #

if __name__ == "__main__":
    main()

