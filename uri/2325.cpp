#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int c, n;

    scanf ("%d %d", &c, &n);

    map <int, int> rep;

    for (int i = 0; i < c; i++)
    {
        int a, b;

        scanf ("%d %d", &a, &b);

        rep[a] = b;
    }

    set <int> p;

    for (int i = 0; i < n; i++)
    {
        int a, b;

        scanf ("%d %d", &a, &b);

        if (rep[a] < b)
        {
            p.insert(a);
            rep[a] = b;
        }
    }

    for (auto it = p.begin(); it != p.end(); it++)
        printf ("%d %d\n", *it, rep[*it]);

    return 0;
}

