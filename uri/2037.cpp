#include <bits/stdc++.h>

using namespace std;

void solve(int n, char str[11][101])
{
    char *ptr[11];
    bool ok;

    for (int i = 1; i <= n; i++)
        ptr[i] = str[i];

    do
    {
        ok = false;
        for (int i = 2; i <= n; i++)
        {
            char play2 = *ptr[i];

            if (!play2) continue;
            else if (play2 == '1')
            {
                ptr[i]++;
                *ptr[1] = (i+'0'); ptr[1]++;
                ok = true;
                break;
            }
            else if (i+'0' == *ptr[play2-'0'])
            {
                ptr[i]++;
                ptr[play2-'0']++;
                ok = true;
                break;
            }
        }

    }while(ok);

    *ptr[1] = '\0';
}

int main (void)
{
    int n;

    while (scanf ("%d", &n), n != -1)
    {
        char str[11][101] = {0};

        for (int i = 2; i <= n; i++)
            scanf ("%s", str[i]);

        solve(n, str);
        
        puts (str[1]);
    }

    return 0;
}

