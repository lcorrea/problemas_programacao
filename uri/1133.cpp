#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int a,b,r;

	cin >> a >> b;

	if(a > b)
		swap(a,b);
	a++;

	while(a < b)
	{
		r = a%5;
		if(r>1&&r<4)
			cout  << a << endl;
		a++;
	}

	return 0;
}
