#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int v[1001];

    while (EOF != scanf ("%d", &n) && n)
    {
        for (int i = 0 ; i < n; i++)
            scanf ("%d", v + i);

        sort(v, v+n);

        int ans = 0;
        for (int i = 0, j = n-1; i < j; --j, ++i)
            ans += (v[j] - v[i]);

        printf ("%d\n", ans);
    }

    return 0;
}

