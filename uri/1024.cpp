/*
	problem 1024
	encryptation
	by lucas correa
*/
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <string>
#include <cctype>

using namespace std;

int main(void)
{
	string word;
	unsigned int n, i, len;

	scanf("%u%*c", &n);

	while(n--)
	{
		getline(cin, word);

		len = word.length();

		for(i = 0; i < len; i++)
			if(isalpha(word[i]))
				word[i] += 3;
			
		reverse(word.begin(), word.end());

		for(i = len/2; i < len; i++)
			word[i] -= 1;
		
		cout << word << '\n';
	}

	return 0;
}

