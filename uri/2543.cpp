#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, i;

    while (scanf ("%d %d", &n, &i) != EOF)
    {
        int cnt = 0;

        for (int j = 0, a, b; j < n; j++)
        {
            scanf ("%d %d", &a, &b);

            cnt += (a == i and !b);
        }
        
        printf ("%d\n", cnt);
    }

    return 0;
}

