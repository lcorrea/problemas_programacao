#include <iostream>

using namespace std;

int main (void)
{
	long long int fact = 1;
	int n;

	cin >> n;

	while(n)
		fact *= n--;

	cout << fact << endl;

	return 0;
}

