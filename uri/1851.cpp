#include <cstdio>
#include <queue>

using namespace std;

int main (void)
{
    int day = 1, t, f;
    unsigned long long int ans = 0, dayT = 0;
    pair <int , int> v[100010];
    priority_queue < pair <double, int > > pq;

    scanf ("%d %d", &t, &f);

    dayT = t + day++;

    while (scanf ("%d %d", &t, &f) != EOF)
    {
        v[day] = {t, f};

        pq.push({-t / (double) f, day});

        if (dayT == day)
        {
            int d = pq.top().second; pq.pop();
            unsigned long long int multa = 0;

            multa += (dayT - d)*v[d].second;

            ans += multa;
            dayT += v[d].first;

        }

        day++;
    }

    while (!pq.empty())
    {
        int d = pq.top().second; pq.pop();

        unsigned long long int multa = 0;

        multa += (dayT - d)*v[d].second;

        ans += multa;
        dayT += v[d].first;
    }

    printf ("%llu\n", ans);

    return 0;
}

