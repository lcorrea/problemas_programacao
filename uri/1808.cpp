/*
	problem 1808
	the return of the king
	by lucas correa
	time: 00:05:04
*/
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>

using namespace std;

int main(void)
{
	unsigned int sum = 0, n;
	string notes;

	cin >> notes;

	n = notes.length();

	reverse(notes.begin(), notes.end());

	for(unsigned int i = 0; i < notes.length(); i++)
	{
		if(notes[i] == '0')
		{
			sum += 10;
			i++;
			n--;
		}
		else
			sum += notes[i] - '0';
	}

	printf("%.2f\n", (float) sum/n);

	return 0;
}

