#include <stdio.h>

int main (void)
{
	double x, L;

	while (scanf ("%lf", &L), L)
	{
		x = 2.0*L - 3.0;
		x = (x - L) / L;
		printf ("%.06lf\n", x);
	}

	return 0;
}

