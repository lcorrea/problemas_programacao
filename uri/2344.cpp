#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf("%d", &n);

    if (n >= 86)
        puts("A");
    else if (n >= 61)
        puts("B");
    else if (n >= 36)
        puts("C");
    else if (n >= 1)
        puts("D");
    else
        puts("E");

    return 0;
}

