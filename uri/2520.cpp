#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;
    int ax, ay, px, py;

    while (scanf ("%d %d", &n, &m) != EOF)
    {
        for (int i = 0, a; i < n; i++)
            for (int j = 0; j < m; j++)
            {
                scanf ("%d", &a);

                if (a == 1)
                    px = j, py = i;
                if (a == 2)
                    ax = j, ay = i;
            }

        printf ("%d\n", abs(ax - px) + abs(ay - py));
    }

    return 0;
}

