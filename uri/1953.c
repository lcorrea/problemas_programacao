#include <stdio.h>
#include <string.h>

int main (void)
{
	int n;
	char course[5000];

	while (scanf ("%d", &n) != EOF)
	{
		int epr = 0, ehd = 0, intru = 0;
		while (n--)
		{
			scanf ("%*s %s", course);
			if (strlen (course) == 3)
			{
				if (course[1] == 'H') ehd++;
				else if (course[1] == 'P') epr++;
				else intru++;
			}
			else intru++;
		}
		printf ("EPR: %d\nEHD: %d\nINTRUSOS: %d\n", epr, ehd, intru);
	}

	return 0;
}

