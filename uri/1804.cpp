#include <bits/stdc++.h>

using namespace std;

class BIT
{
    private:
        vector <int> bit, v;
        int n;

    public:
        BIT(int _n, vector <int> &_v)
        {
            v = _v;
            n = _n;

            bit.assign(n+1, 0);

            for (int i = 0; i < n; i++)
                update(i+1, v[i]);
        }

        void update(int i, int val)
        {
            while (i <= n)
            {
                bit[i] += val;
                i += i & -i;
            }
        }

        int query(int i)
        {
            int sum = 0;

            while (i > 0)
            {
                sum += bit[i];

                i -= i & -i;
            }
            
            return sum;
        }
};

int main (void)
{
    int n, q;
    
    scanf ("%d%*c", &n);

    vector <int> v(n);

    for (int i = 0; i < n; i++)
        scanf ("%d%*c", &v[i]);

    BIT bit(n, v);

    char op; int i;
    while(scanf ("%c %d%*c", &op, &i) != EOF)
    {
        if (op=='?')
            printf ("%d\n", bit.query(i-1));
        else
            bit.update(i, -v[i-1]);
    }


    return 0;
}

