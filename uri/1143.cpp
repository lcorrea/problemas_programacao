#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, m = 1;

	cin >> n;

	while(n--)
	{
		cout << m << ' ' << m*m << ' ' << m*m*m << endl;

		m++;
	}

	return 0;
}

