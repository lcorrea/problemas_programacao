#include <bits/stdc++.h>

using namespace std;

class UnionFind {
    private:
        vector <int> p, h;

    public:
        UnionFind (int n) {
            h.assign(n+1, 0);
            p.resize(n+1);
            
            for (int i = 0; i <= n; i++)
                p[i] = i;
        }

        int findSet (int i) {
            return p[i] = (p[i] == i) ? i : findSet(p[i]);
        }

        void join (int i, int j) {
            int p_i = findSet (i);
            int p_j = findSet (j);

            if (p_i == p_j)
                return;

            if (h[p_i] > h[p_j])
                p[p_j] = p_i;
            else
            {
                h[p_j] += h[p_i] == h[p_j];

                p[p_i] = p_j;
            }
        }
};

int main (void)
{
    int n, c;

    scanf ("%d %d", &n, &c);

    UnionFind bancos(n);

    for (int i = 0; i < c; i++)
    {
        int a, b;
        char op;

        scanf ("%*c%c %d %d", &op, &a, &b);

        if (op == 'C')
            puts (bancos.findSet(a) == bancos.findSet(b) ? "S" : "N");
        else
            bancos.join(a, b);
    }

    return 0;
}

