#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;

	while (scanf ("%d %d", &n, &m) != EOF)
	{
		map <int, vector <int> > positions;

		for (int p = 1, i; p <= n; p++)
		{
			scanf ("%d", &i);
			positions[i].push_back (p);
		}

		for (int i = 0, k, v; i < m; i++)
		{
			scanf ("%d %d", &k, &v);
			
			if (positions.find(v) != positions.end())
				if (k <= positions[v].size())
					printf ("%d\n", positions[v][k-1]);
				else puts ("0");
			else puts ("0");
		}
	}

	return 0;
}

