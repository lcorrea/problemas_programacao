#!/usr/bin/python3

def main():

    from sys import stdin, stdout

    input_data = map(int, stdin.read().split())

    prefix = [0] * 1000000

    for n in input_data:

        idx = 0
        for i in range(n):
            if idx == 0:
                prefix[idx] = next(input_data)
            else:
                prefix[idx] = prefix[idx-1] + next(input_data)
            idx += 1
        
        ans = prefix[n-1]
        
        for i in range(0, n-1):
            ans = min(ans, abs(prefix[i] - (prefix[n-1] - prefix[i])))

        stdout.write("{:d}\n".format(ans))

if __name__ == "__main__":
    main()
