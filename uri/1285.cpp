#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, cnt = 0;
	char str[10];
	int freq[5010] = {0};

	for (int i = 1; i <= 5000; i++)
	{
		int v[11] = {0};

		sprintf (str, "%d", i);

		int len = strlen (str);
		bool igual = false;

		for (int k = 0; k < len and !igual; k++)
			v[str[k] - '0']++, igual = v[str[k] - '0']>1 ? true : igual;

		cnt += igual ? (0) : (1);

		freq[i] = cnt;
	}

	while (scanf ("%d %d", &n, &m) != EOF)
		printf ("%d\n", freq[m] - freq[n-1]);

	return 0;
}

