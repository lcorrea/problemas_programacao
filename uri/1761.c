#include <stdio.h>
#include <math.h>

#define PI 3.141592654

int main (void)
{
	double angle, distance, height;

	while (scanf ("%lf %lf %lf", &angle, &distance, &height) != EOF)
	{
		double length = (tan (angle*PI/180.0)*distance + height) * 5.0;

		printf ("%.02lf\n", length);
	}

	return 0;
}

