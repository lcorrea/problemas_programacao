//solucao quase levou TLE no URI, mas pegou AC
#include <iostream>

using namespace std;

int main (void)
{
	int s, b, l, r;
	ios_base::sync_with_stdio(false);
	while (cin >> s >> b, s and b)
	{
		int be[s+1], bd[s+1];

		be[0] = 0, bd[0] = 1, bd[s] = 0, be[s] = s-1;
		for (int i = 1; i <= s; i++) be[i] = i-1, bd[i] = i+1;
		
		while (b--)
		{
			cin >> l >> r;

			if (!be[l] and bd[r] > s) cout << "* *";
			else if (!be[l] and bd[r]<=s) cout << "* " << bd[r];
			else if (be[l] and bd[r]>s) cout << be[l] << " *";
			else cout << be[l] << ' ' << bd[r];
			cout << endl;
			
			bd[be[l]] = bd[r];
			be[bd[r]] = be[l];
		}

		cout << '-' << endl;
	}

	return 0;
}

