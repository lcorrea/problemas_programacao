#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  char str[10001];

  scanf ("%d", &n);

  while (n--)
  {
    scanf ("%s", str);

    printf ("%.02lf\n", 0.01 * strlen (str));
  }

  return 0;
}

