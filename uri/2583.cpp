#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;
        set <string> budget;

        scanf ("%d", &n);

        while (n--)
        {
            char stra[10000], strb[10000];
            
            scanf ("%s %s", stra, strb);
            
            const string in = "chirrin";
            const string out = "chirrion";
            const string s = string(strb);

            if (s == in and budget.find(strb) == budget.end())
                budget.insert(stra);
            
            if (s == out)
                budget.erase(stra);
        }

        puts ("TOTAL");
        for (auto str = budget.begin(); str != budget.end(); str++)
            printf ("%s\n", str->c_str());

    }

    return 0;
}

