/*
	problem 1216
	getline one
	by lucas correa
	time: 00:17:36
*/
#include <cstdio>
#include <cmath>

using namespace std;

int main(void)
{
	unsigned long int cnt = 0;
	double avarage = 0;
	long long int distance = 0;

	while(scanf("%*[^\n]%*c%lld%*c", &distance) != EOF)
	{
		avarage += distance;
		cnt++;
	}

	printf("%.1lf\n", avarage/cnt);

	return 0;
}
