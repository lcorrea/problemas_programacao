#include <cstdio>

using namespace std;

int main (void)
{
	float s = 1;
	unsigned int n = 2;

	while (n < 101)
		s += 1.0 / n++;

	printf ("%.2f\n", s);

	return 0;
}

