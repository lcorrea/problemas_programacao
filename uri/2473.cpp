#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int f[6], g, cnt = 0;
    const char ans[5][10] = {"azar", "terno", "quadra", "quina", "sena"};

    for (int i = 0; i < 6; i++)
        scanf("%d", f + i);

    for (int i = 0; i < 6; i++) {
        scanf("%d", &g);

        for (int j = 0; j < 6; j++)
            cnt += f[j] == g;
    }

    if ((cnt - 2) > 0)
        puts (ans[cnt-2]);
    else
        puts(ans[0]);

    return 0;
}

