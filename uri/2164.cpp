#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  const double sqrt5 = sqrt(5);
  const double a = (1 + sqrt5) * .5;
  const double b = (1 - sqrt5) * .5;

  scanf ("%d", &n);

  printf ("%.1lf\n", (pow (a, n) - pow(b, n)) / sqrt5);

  return 0;
}

