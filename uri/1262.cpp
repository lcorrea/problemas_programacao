#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char str[100];

	while (scanf ("%s", str) != EOF)
	{
		int c = 0, p;
		int i = 0, sz = strlen (str);

		scanf ("%d", &p);

		while (i < sz)
		{
			while (i < sz and str[i] == 'W') c++, i++;
			int cnt = 0;
			while (i < sz and str[i] == 'R')
			{
				i++;
				cnt++;
				if (cnt == p) c++, cnt = 0;
			}
			if (cnt) c++;
		}

		printf ("%d\n", c);
	}

	return 0;
}

