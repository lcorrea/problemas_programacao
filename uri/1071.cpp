/*
	problem 1072
	Sum of Consecutive Odd Numbers I
	by lucas correa
*/

#include <iostream>
#include <algorithm>

using namespace std;

int main(void)
{
	int x, y, sum = 0;

	cin >> x >> y;

	if(x > y)
		swap(x, y);

	if(abs(x) % 2 == 0)
		x++;
	else
		x += 2;

	while(x < y)
	{
		sum += x;
		x += 2;
	}

	cout << sum << endl;

	return 0;
}

