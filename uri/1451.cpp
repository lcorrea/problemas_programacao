#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[100010];

    while (scanf ("%s", str) != EOF)
    {
        deque <char> text;
        bool insert = true;
        int cnt = 0;

        for (char *ch = str; *ch; ch++)
        {
            if (isalpha(*ch) or *ch == '_')
            {
                if(insert)
                    text.push_back(*ch);
                else
                    text.insert(text.begin() + cnt++, *ch);
            }
            else if (*ch == '[')
                insert = false, cnt = 0;
            else
                insert = true;
        }

        while (!text.empty())
            putchar (text.front()), text.pop_front();
        putchar('\n');
    }

    return 0;
}

