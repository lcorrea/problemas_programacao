#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	unsigned int sz, p;

	while (cin >> sz >> p, sz and p)
	{
		int inf = sqrt(p);
		int sup;

		if (inf%2) sup = inf + 2;
		else sup = inf + 1, inf -= 1;

		int pSup = sup*sup;
		int pInf = inf*inf;

		if (pSup - p <= p - pInf)
		{
			int x = sz/2 + sup/2 + 1;
			int y = x, tmp = sup;
			int n = pSup;

			while (tmp > 1 and n > p)
				y -= 1, n -=1, tmp -= 1;
			tmp = sup;
			while (tmp > 1 and n > p)
				x -= 1, n -= 1, tmp -= 1;
			tmp = sup;
			while (tmp > 1 and n > p)
				y += 1, n -= 1, tmp -= 1;

			cout << "Line = " << y << ", column = " << x << '.' << endl;
		}
		else
		{
			int x = sz/2 + inf/2 + 1;
			int y = x, tmp = inf + 1;
			int n = pInf;

			if (p != n)
			{
				y++, n++;
				while (tmp > 1 and n < p)
					x -= 1, n += 1, tmp -= 1;
				tmp = sup;
				while (tmp > 1 and n < p)
					y -= 1, n += 1, tmp -= 1;
				tmp = sup;
				while (tmp > 1 and n < p)
					x += 1, n += 1, tmp -= 1;
			}
			cout << "Line = " << y << ", column = " << x << '.' << endl;
		}
	}

	return 0;
}

