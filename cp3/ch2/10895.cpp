#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;

	while (scanf ("%d %d", &m, &n) != EOF)
	{
		vector < vector < pair <int, int> > > t(n+1);

		for (int i = 1; i <= m; i++)
		{
			int c, u, v;
			vector <int> rows;
			scanf ("%d", &c);

			for (int j = 0; j < c; j++)
			{
				scanf ("%d", &u);
				rows.push_back(u);
			}
			
			for (int j = 0; j < c; j++)
			{
				scanf ("%d", &v);
				t[rows[j]].push_back(make_pair(i, v));
			}
		}

		printf ("%d %d\n", n, m);
		for (int i = 1; i <= n; i++)
		{
			int sz = t[i].size();

			printf ("%d", sz);

			if(!sz) putchar ('\n');

			for (int j = 0; j < sz; j++)
				printf (" %d", t[i][j].first);
			putchar ('\n');

			for (int j = 0; j < sz; j++)
				printf ("%d%c", t[i][j].second, j+1 == sz ? '\n' : ' ');
		}
	}

	return 0;
}

