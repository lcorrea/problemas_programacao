#include <bits/stdc++.h>

using namespace std;

struct team
{
	int id;
	int score;
	int problems;
	int time;
	int pn[11];
};

bool cmp (struct team a, struct team b)
{
	if(a.score == b.score)
	{
		if (a.time == b.time)
			return a.id < b.id;

		return a.time < b.time;
	}

	return a.score > b.score;
}

int main (void)
{
	int n;
	char str[100];

	scanf ("%d%*c", &n);
	getchar();

	while(n)
	{
		struct team teams[110] = {0};
		int p, t, c;
		char r, ch;

		while (1)
		{
			int i = 0;
			while ((ch = getchar()) != '\n' and ch != EOF)
				str[i++] = ch;
			str[i] = '\0';

			if (strlen(str) == 0) {break;}

			sscanf (str, "%d %d %d %c%*c", &c, &p, &t, &r);

			teams[c].id = c;
			if (r == 'I' and !(teams[c].problems & (1 << p))) teams[c].pn[p] += 20;
			if (r == 'C' and !(teams[c].problems & (1 << p)))
			{
				teams[c].score++;
				teams[c].time += t + teams[c].pn[p];
				teams[c].problems |= (1 << p);
			}
		}

		sort(teams, teams+110, cmp);

		for (int i = 0; i < 110; i++)
			if (teams[i].id)
				printf ("%d %d %d\n", teams[i].id, teams[i].score, teams[i].time);
		if(n > 1) putchar('\n');
		n--;
	}

	return 0;
}

