#include <bits/stdc++.h>

using namespace std;

typedef union integer
{
	char byte[4];
	int full;
}Integer;

int main (void)
{
	Integer a, b;
	
	while (scanf ("%d", &a.full) != EOF)
	{
		for (int i = 3, j = 0; i >= 0; i--, j++)
			b.byte[j] = a.byte[i];

		printf("%d converts to %d\n", a.full, b.full);
	}
	
	return 0;
}

