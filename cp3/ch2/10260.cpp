#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int table[500] = {0};
	string w;

	table['B'] = table['F'] = table['P'] = table['V'] = 1;
	table['C'] = table['G'] = table['J'] = table['K'] = table['Q'] = table['S'] = table['X'] = table['Z'] = 2;
	table['D'] = table['T'] = 3;
	table['L'] = 4;
	table['M'] = table['N'] = 5;
	table['R'] = 6;

	while(cin >> w)
	{
		int c = -1;

		for (int i = 0; i < w.size(); i++)
		{
			if (table[w[i]] != 0 and table[w[i]] != c)
				printf ("%d", table[w[i]]);
			c = table[w[i]];
		}

		putchar ('\n');
	}

	return 0;
}

