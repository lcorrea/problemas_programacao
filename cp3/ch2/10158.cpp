#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
	private:
		vector <int> p; //parent - country
		vector <int> r; //rank
		
	public:
		unionFind(int n)
		{
			p.assign (n, 0);
			r.assign (n, 0);

			for (int i = 0; i < n; i++)
				p[i] = i;
		}

		int findSet (int i)
		{
			return p[i] == i ? i : (p[i] = findSet(p[i]));
		}

		bool isSameSet(int i, int j)
		{
			return findSet(i) == findSet(j);
		}

		bool unionSet(int i, int j)
		{
			if (isSameSet(i, j))
				return;

			int x = findSet(i);
			int y = findSet(j);

			if (r[x] > r[y])
				p[y] = x;
			else
			{
				p[x] = y;

				if (r[x] == r[y])
					r[y]++;
			}
		}
};

int main (void)
{
	int n;
	int c, x, y;
	UnionFind uf(10000);

	scanf ("%d", &n);

	while (scanf ("%d%d%d", &c, &x, &y), c or x or y)
	{
		if (c == 1 and uf.findSet(x) )
	}

    return 0;
}

