#include <iostream>
#include <cstring>
#include <cstdlib>
#include <bitset>

using namespace std;

int main (void)
{
	int n, a, b;

	cin >> n;
	while (!cin.eof())
	{
		if (n == 1) cout << "Jolly", cin >> a;
		else if (n == 0) cout << "Jolly";
		else
		{
			cin >> a;
			bitset<3001> numbers(1);

			for (int i = 2; i <= n; i++)
			{
				cin >> b;
				numbers.set(abs (a-b));
				a = b;
			}
			for (a = 1; a < n; a++) { if (!numbers[a]) break; }
			if (a != n) cout << "Not jolly";
			else cout << "Jolly";
		}
		cout << endl;

		cin >> n;
	}

	return 0;
}

