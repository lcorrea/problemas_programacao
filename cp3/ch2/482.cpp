#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d%*c%*c", &n);

	while (n--)
	{
		char c;
		vector <int> p;
		int pi;
		
		while (scanf ("%d%c", &pi, &c), c != '\n')
			p.push_back(pi);
		p.push_back(pi);
		
		vector <string> x(p.size());
		char str[1000];

		for (int i = 0; i < p.size(); i++)
			scanf ("%s%*c", str), x[p[i]-1] = str;

		for (int i = 0; i < x.size(); i++)
			printf ("%s\n", x[i].c_str());

		if (n) putchar('\n');
	}

	return 0;
}

