#include <bits/stdc++.h>

using namespace std;

void rotate (vector <string> &m)
{
	int n = m.size();
	vector <string> tmp(n);

	for (int i  = 0; i < n; i++)
		for (int k = 0; k < n; k++)
			tmp[i] += m[n-k-1][i];
	m = tmp;
}

int main (void)
{
	int N, n;

	while (cin >> N >> n, n and N)
	{

		int cnts[4] = {0};
		vector <string> Nsquare(N);
		vector <string> nsquare(n);

		for (int i = 0; i < N; i++)
			cin >> Nsquare[i];

		for (int i = 0; i < n; i++)
			cin >> nsquare[i];

		for (int i = 0; i < 4; i++)
		{
			for (int l = 0; l < N; l++)
			{
				for (int s = 0; s < N; s++)
				{
					int j;
					bool match = true;

					for (j = 0; j < n and j+l < N and match; j++)
						match = Nsquare[j+l].substr (s, n) == nsquare[j];

					cnts[i] += (match and j == n) ? (1) : (0);
				}
			}

			rotate(nsquare);
		}

		cout << cnts[0];
		for (int i = 1; i < 4; i++)
			cout << ' ' << cnts[i];
		cout << endl;
	}

	return 0;
}

