#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t;
	int ptd[101];

	scanf ("%d", &t);

	while (t--)
	{
		int n, p, cnt = 0;

		scanf ("%d", &n);
		scanf ("%d", &p);

		for (int i = 0; i < p; i++)
			scanf ("%d", &ptd[i]);

		for (int i = 1; i <= n; i++)
		{
			int x = i%7;
			if (x != 6 and x != 0)
			{
				for (int j = 0; j < p; j++)
				{
					if (i%ptd[j]==0)
					{
						cnt++;
						break;
					}
				}
			}
		}

		printf ("%d\n",cnt);
	}

	return 0;
}

