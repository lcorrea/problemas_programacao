#include <bits/stdc++.h>

using namespace std;

class unionFind 
{
	vector <int> h, p;

	public:

	unionFind (int n)
	{
		h.assign (n+1, 0); p.assign (n+1, 0);

		for (int i = 1; i <= n; i++) p[i] = i;
	}

	int findSet (int i) { return (i == p[i]) ? i : (p[i] = findSet (p[i])); }

	bool isSameSet (int i, int j) { return findSet (i) == findSet (j); }

	void join (int i, int j)
	{
		if (!isSameSet (i, j))
		{
			int pi = p[i], pj = p[j];

			if (h[pi] > h[pj]) p[pj] = pi;
			else 
			{
				p[pi] = pj;
				if (h[pi] == h[pj]) h[pj]++;
			}
		}
	}
};

int main (void)
{
	int n;

	scanf ("%d%*c%*c", &n);

	while (n--)
	{
		char c;
		int v, c1, c2;
		int t = 0, f = 0;

		scanf ("%d%*c", &v);

		unionFind uf(v);

		while ((c = getchar()), c != '\n' and c != EOF)
		{
			scanf ("%d %d%*c", &c1, &c2);
			if (c == 'c') uf.join (c1, c2);
			else if (uf.isSameSet (c1, c2)) t++;
			else f++;
		}

		printf ("%d,%d\n", t, f);

		if (n) putchar ('\n');
	}

	return 0;
}

