#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d%*c", &n);
	while (n--)
	{
		int grid[3][3] = {0}, tmp[3][3] = {0};
		int sum = 0, cnt = 0;
		char c;

		for (int i = 0; i < 3; i++)
		{
			getchar();
			for (int j = 0; j < 3; j++)
				scanf ("%c", &c), grid[i][j] = c - '0', sum += grid[i][j];
		}
		getchar();

		while (sum)
		{
			sum = 0;
			cnt++;
			tmp[0][0] = (grid[1][0] + grid[0][1]) % 2;
			tmp[0][1] = (grid[0][0] + grid[0][2] + grid[1][1]) % 2;
			tmp[0][2] = (grid[0][1] + grid[1][2]) % 2;
			tmp[1][0] = (grid[0][0] + grid[1][1] + grid[2][0]) % 2;
			tmp[1][1] = (grid[0][1] + grid[1][0] + grid[1][2] + grid[2][1]) % 2;
			tmp[1][2] = (grid[0][2] + grid[1][1] + grid[2][2]) % 2;
			tmp[2][0] = (grid[1][0] + grid[2][1]) % 2;
			tmp[2][1] = (grid[1][1] + grid[2][0] + grid[2][2]) % 2;
			tmp[2][2] = (grid[1][2] + grid[2][1]) % 2;

			memcpy (grid, tmp, sizeof(tmp));
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					sum += grid[i][j];
		}

		cout << cnt - 1 << endl;
	}

	return  0;
}

