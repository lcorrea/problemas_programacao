#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int g, d;
	string w;

	while (cin >> g, g)
	{
		cin >> w;

		for (int i = 0, n = w.size()/g; i < w.size(); i += n)
		{
			string tmp = w.substr (i, n);
			reverse (tmp.begin(), tmp.end());
			cout << tmp;
		}

		cout << endl;

	}

	return 0;
}

