#include <bits/stdc++.h>

using namespace std;

class UnionFind
{
	private:
		vector <int> p;
		vector <int> r;
		vector <int> members;

	public:
		UnionFind (int n)
		{
			p.assign(n, 0);
			r.assign(n, 0);
			members.assign (n, 1);

			for (int i = 0; i < n; i++)
				p[i] = i;
		}

		int findSet (int v)
		{
			return (v == p[v]) ? v : (p[v] = findSet (p[v]));
		}

		bool isSameSet (int i, int j)
		{
			return findSet(i) == findSet(j);
		}

		void unionSet(int i, int j)
		{
			if (isSameSet(i, j))
				return;

			int x = findSet(i);
			int y = findSet(j);

			if (r[x] > r[y])
			{
				p[y] = x;
				members[x] += members[y];
			}
			else
			{
				p[x] = y;
				members[y] += members[x];

				if (r[x] == r[y])
					r[y]++;
			}
		}

		int getMembers(int v)
		{
			return members[findSet(v)];
		}
};

int main (void)
{
	int t;

	scanf ("%d", &t);

	for (int c = 0; c < t; c++)
	{
		map <string, int> names;
		UnionFind uf(100001);
		int cnt = 0;
		int e;

		scanf ("%d", &e);

		for (int i = 0; i < e; i++)
		{
			char a[21], b[21];

			scanf ("%s %s", a, b);

			if (names.find(a) == names.end())
				names[a] = cnt++;
			if (names.find(b) == names.end())
				names[b] = cnt++;

			uf.unionSet(names[a], names[b]);
			printf ("%d\n", uf.getMembers(names[a]));

		}
	}

	return 0;
}

