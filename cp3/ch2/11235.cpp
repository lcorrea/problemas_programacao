#include <bits/stdc++.h>

using namespace std;

#define MAX 200001
#define OFFSET 100000

class SegmentTree
{
    private:
        vector<int> v, tree, freq, start;
        int n;

        int left_son (int parent) { return (parent << 1); }
        int right_son (int parent) { return (parent << 1) + 1; }

        int process_interval (int number, int l, int r)
        {
            int begin = start[number];
            int end = start[number] + freq[number] - 1;

            if (begin >= l and end <= r)
                return freq[number];

            if (begin < l and end >= l and end <= r)
                return end - l + 1;

            if (begin >= l and end > r and begin <= r)
                return r - begin + 1;

            if (begin < l and end > r)
                return r - l + 1;

            return 0; 
        }
        
        void build (int node, int l, int r)
        {
            if (l == r) { tree[node] = l; return; }
            
            int middle = (l + r)/2;

            int left = left_son (node);
            int right = right_son (node);

            build (left, l, middle);
            build (right, middle + 1, r);

            int freq1 = process_interval(v[tree[left]], l, r);
            int freq2 = process_interval(v[tree[right]], l, r);

            tree[node] = (freq1 >= freq2) ? tree[left] : tree[right];

            //corner case
            if (v[middle] == v[middle+1])
            {
                int freq3 = process_interval(v[middle], l, r);
                int mfreq = process_interval(v[tree[node]], l, r);

                if (mfreq < freq3)
                    tree[node] = middle;
            }

        }

        int rfq (int node, int l, int r, int i, int j)
        {
            if (i > r or j < l) return -1;

            if (i <= l and j >= r)
                return tree[node];

            int middle = (l + r) / 2;
            int left = left_son (node);
            int right = right_son (node);

            int result1 = rfq (left, l, middle, i, j);
            int result2 = rfq (right, middle + 1, r, i, j);

            int freq1 = result1 == -1 ? -1 : process_interval (v[result1], i, j);
            int freq2 = result2 == -1 ? -1 : process_interval (v[result2], i, j);

            //corner cases
            if (v[middle] == v[middle+1])
            {
                int result3 = middle;
                int freq3  = process_interval (v[result3], i, j);

                if (freq1 > freq2)
                {
                    swap (freq1, freq2);
                    swap (result1, result2);
                }

                if (freq2 > freq3)
                    swap (result2, result3);

                return result3;
            }

            if (result1 == -1) return result2;
            if (result2 == -1) return result1;

            if (freq1 >= freq2)
                return result1;
            else
                return result2;
        }

    public:
        SegmentTree (vector <int> &_A) : freq(MAX, 0), start(MAX, 0)
        {
            v = _A;
            n = v.size();

            for (int i = 0; i < n; i++)
            {
                freq[v[i]]++;

                if (freq[v[i]] == 1)
                    start[v[i]] = i;
            }

            tree.assign (4 * n, 0);

            build (1, 0, n - 1);
        }

        int rfq (int i, int j)
        {
            return rfq (1, 0, n - 1, i, j);
        }

        //just to clean the code in main
        int max_freq (int i, int j)
        {
            return process_interval (v[rfq(i, j)], i, j);
        }
};

int main ()
{
    int n;

    while (scanf ("%d", &n), n)
    {
        int q;
        vector <int> A(n);

        scanf ("%d", &q);

        for (int i = 0, a; i < n; i++)
            scanf ("%d", &a), A[i] = a + OFFSET;

        SegmentTree st (A);

        while (q--)
        {
            int i, j;

            scanf ("%d %d", &i, &j);
            printf ("%d\n", st.max_freq(i-1, j-1));
        }
    }

    return 0;
}

