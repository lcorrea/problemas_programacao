#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;
typedef queue <ii> qii;

int main (void)
{
	int c;

	scanf ("%d", &c);

	while (c--)
	{
		int n, t, m;
		priority_queue <ii, vector <ii>, greater<ii> > sol;
		qii r, l;

		scanf ("%d %d %d", &n, &t, &m);

		for (int i = 0; i < m; i++)
		{
			int time;
			char str[10];

			scanf ("%d %s", &time, str);

			if (*str == 'l') l.push (make_pair (i, time));
			else r.push (make_pair (i, time));
		}

		char side = 'l';
		int tick = 0;

		while (!r.empty() or !l.empty())
		{
			if (r.empty())
			{
				if (tick == 0) tick = l.front().second;
				while (!l.empty())
				{
					int cars = 0;
					if (tick < l.front().second)
						tick = l.front().second;
					if (side == 'r') tick += t, side = 'l';
					while (cars < n and !l.empty() and l.front().second <= tick)
					{
						sol.push (make_pair (l.front().first, tick+t));
						cars++;
						l.pop();
					}
					tick += t;
					side = 'r';
					if (!l.empty() and tick >= l.front().second)
						tick += t, side = 'l';
				}
			}
			else if (l.empty())
			{
				if (tick == 0) tick = r.front().second;
				while (!r.empty())
				{
					int cars = 0;
					if (tick < r.front().second)
						tick = r.front().second;
					if (side == 'l') tick += t, side = 'r';
					while (cars < n and !r.empty() and r.front().second <= tick)
					{
						sol.push (make_pair (r.front().first, tick+t));
						cars++;
						r.pop();
					}
					tick += t; side = 'l';
					if (!r.empty() and tick >= r.front().second)
						tick += t, side = 'r';
				}
			}
			else
			{
				if (side == 'r')
				{
					if (tick >= r.front().second)
					{
						int cars = 0;
						while (cars < n and !r.empty() and tick >= r.front().second)
						{
							cars++;
							sol.push (make_pair (r.front().first, tick+t));
							r.pop();
						}
						tick += t;
						side = 'l';
						continue;
					}

					if (tick >= l.front().second)
					{
						tick += t, side = 'l';
						continue;
					}

					if (r.front().second <= l.front().second)
						tick = r.front().second;
					else
						side = 'l', tick = l.front().second + t;
				}
				else
				{
					if (tick >= l.front().second)
					{
						int cars = 0;
						while (cars < n and !l.empty() and tick >= l.front().second)
						{
							cars++;
							sol.push (make_pair (l.front().first, tick+t));
							l.pop();
						}
						tick += t;
						side = 'r';
						continue;
					}

					if (tick >= r.front().second)
					{
						tick += t, side = 'r';
						continue;
					}

					if (l.front().second <= r.front().second)
						tick = l.front().second;
					else 
						side = 'r', tick = r.front().second + t;
				}
			}
		}
		while (!sol.empty())
		{
			printf ("%d\n", sol.top().second);
			sol.pop();
		}

		if (c) putchar ('\n');
	}

	return 0;
}

