#include <bits/stdc++.h>

using namespace std;

class unionFind
{
	vector <int> high, p, friends;
	int maxfriends;

	public:
	
	unionFind (int n)
	{
		maxfriends = 0;
		friends.assign(n, 1);
		high.assign(n, 0);
		p.assign(n, 0);

		for (int i = 0; i < n; i++)
			p[i] = i;
	}

	int findSet (int a)
	{
		return (p[a] == a) ? a : (p[a] = findSet(p[a]));
	}
	
	bool isSameSet (int a, int b)
	{
		return findSet(a) == findSet(b);
	}

	void join(int a, int b)
	{
		if (isSameSet(a, b)) return;

		int i = findSet(a), j = findSet(b);

		if (high[i] > high[j])
		{
			friends[i] += friends[j];
			maxfriends = max (maxfriends, friends[i]);
			p[j] = i;
		}
		else
		{
			friends[j] += friends[i];
			maxfriends = max (maxfriends, friends[j]);
			p[i] = j;
			if (high[j] == high[i])
				high[j]++;
		}
	}

	int getMaxFriends()
	{
		return maxfriends;
	}
};

int main (void)
{
	int n;

	scanf ("%d", &n);

	while(n--)
	{
		int c, p;

		scanf ("%d %d", &c, &p);

		unionFind groups(c);

		for (int i = 0, a, b; i < p; i++)
		{
			scanf ("%d %d", &a, &b);
			groups.join(a-1, b-1);
		}

		printf ("%d\n", groups.getMaxFriends());
	}
	
	return 0;
}

