#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, set = 1;

	scanf ("%d", &n);
	while (n)
	{
		int moves = 0;
		int cnt = 0, wall[n], h;

		for (int i = 0; i < n; i++)
			scanf ("%d", &wall[i]), cnt+=wall[i];

		h = cnt/n;

		for (int i = 0; i < n;i++)
			moves += wall[i] > h ? wall[i] - h : 0;

		printf ("Set #%d\nThe minimum number of moves is %d.\n\n", set++, moves);

		scanf ("%d", &n);
	}

	return 0;
}

