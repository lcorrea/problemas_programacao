#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char line[200];

	while (scanf ("%s%*c", line), *line != '#')
	{
		if (next_permutation(line, line + strlen(line)))
			cout << line << endl;
		else
			cout << "No Successor" << endl;
	}

	return 0;
}

