#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	priority_queue <unsigned long long int, vector <unsigned long long int>, greater<unsigned long long int> > l;
	priority_queue <unsigned long long int> h;
	unsigned long long int x;

	scanf ("%llu", &x);
	l.push(x);
	printf ("%llu\n", x);

	while (scanf ("%llu", &x) != EOF)
	{
		if (x > l.top()) l.push(x);
		else h.push(x);

		if (h.size() > l.size() + 1) l.push(h.top()), h.pop();
		if (l.size() > h.size() + 1) h.push(l.top()), l.pop();
		
		if(h.size() > l.size()) printf ("%llu\n", h.top());
		else if (h.size() < l.size()) printf ("%llu\n", l.top());
		else printf ("%llu\n", (h.top() + l.top())/2ull);
	}

	return 0;
}

