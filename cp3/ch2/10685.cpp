#include <bits/stdc++.h>

using namespace std;

class unionFind
{
	vector <int> p, h, f;
	int maxfriends;

	public:
		unionFind(int n)
		{
			maxfriends = 1;
			p.assign(n, 0);
			h.assign(n, 0);
			f.assign(n, 1);

			for (int i = 0; i < n; i++)
				p[i] = i;
		}

		int findSet(int i)
		{
			return p[i] == i ? i : p[i] = findSet(p[i]);
		}

		bool isSameSet(int i, int j)
		{
			return findSet(i) == findSet(j);
		}

		void join(int i, int j)
		{
			if (isSameSet(i, j)) return;

			int pi = p[i], pj = p[j];

			if (h[pi] > h[pj])
			{
				f[pi] += f[pj];
				maxfriends = max(maxfriends, f[pi]);

				p[pj] = pi;

			}
			else
			{
				f[pj] += f[pi];
				maxfriends = max(maxfriends, f[pj]);
				p[pi] = pj;

				if (h[pi] == h[pj]) h[pj]++;
			}
		}

		int getMaxFriends()
		{
			return maxfriends;
		}
};


int main (void)
{
	int c, r;

	while (scanf ("%d %d%*c", &c, &r), c or r)
	{
		unionFind chains(c);
		map<string, int> animals;

		for (int i = 0; i < c; i++)
		{
			char animal[100];

			scanf ("%s%*c", animal);
			animals[animal] = i;
		}

		for (int i = 0; i < r; i++)
		{
			char a[100], b[100];

			scanf ("%s %s%*c", a, b);
			chains.join(animals[a], animals[b]);
		}
		getchar();

		printf ("%d\n", chains.getMaxFriends());
	}
	
	return 0;
}

