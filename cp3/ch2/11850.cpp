#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	while (scanf ("%d", &n), n)
	{
		int h[n+1];

		for (int i = 0; i < n; i++)
			scanf ("%d", h + i);
		h[n] = 1422;
		sort (h, h+n);

		bool ok = true;
		
		for (int i = 1; i < n and ok; i++)
			ok = h[i] - h[i-1] <= 200;

		ok = (h[n] - h[n-1])*2 <= 200;

		reverse(h, h+n);

		for (int i = 1; i < n and ok; i++)
			ok = h[i-1] - h[i] <= 200;

		puts (ok ? "POSSIBLE" : "IMPOSSIBLE");

	}

	return 0;
}

