#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, v, k;
	map <int, vector <int> > g;

	while (scanf ("%d %d", &n, &m) != EOF)
	{
		g.clear();

		for (int i = 1; i <= n; i++)
			scanf ("%d", &v), g[v].push_back (i);

		for (int i = 1; i <= m; i++)
			scanf ("%d %d", &k, &v), printf ("%d\n", k <= g[v].size() ? g[v][k-1] : 0);
	}

	return 0;
}

