#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (void)
{
	int n, k, m, v, i, j;
	int values[256];
	long long int total;
	unsigned char c;

	scanf ("%d%*c", &n);
	for (i = 0; i < n; i++)
	{
		memset (values, 0, sizeof(int)*256);

		scanf ("%d%*c", &k);

		for (j = 0; j < k; j++)
		{
			scanf ("%c %d%*c", &c, &v);
			values[c] = v;
		}

		scanf ("%d%*c", &m);
		total = 0;
		for (j = 0; j < m; j++)
		{
			while ((c = (unsigned char) getchar ()) != '\n' && c != '\r')
				total += values[c];
		}

		printf ("%lld.%02lld$\n", total/100, total%100);
	}

	return 0;
}

