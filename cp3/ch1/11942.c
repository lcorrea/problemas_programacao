#include <stdio.h>

int main (void)
{
	int n;

	scanf ("%d", &n);
	puts ("Lumberjacks:");

	while (n--)
	{
		int i = 8, r;
		int la, lb;
		scanf ("%d %d", &lb, &la);
		
		r = (lb > la) ? (-1):(1);
		while (i--)
		{
			scanf ("%d", &lb);
			r = (r==((la > lb) ? (-1):(1)) ? (r):(0));
			la = lb;
		}
		puts (r?"Ordered":"Unordered");
	}

	return 0;
}

