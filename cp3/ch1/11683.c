#include <stdio.h>

int main (void)
{
	int a, c;

	while (scanf ("%d %d", &a, &c), a && c)
	{
		int cols[c+1];
		int i;
		
		for (i=1; i <= c; i++)
			scanf ("%d", cols + i);

		unsigned int cnt = 0, last = a;

		for (i=1; i <= c; i++)
		{
			if (last >= cols[i])
				cnt += last - cols[i];
			last = cols[i];
		}
		
		printf ("%u\n", cnt);

	}

	return 0;
}

