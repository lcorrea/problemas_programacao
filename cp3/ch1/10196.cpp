#include <bits/stdc++.h>

using namespace std;

typedef vector < pair <char,  pair <int, int> > > vpp;

char board[8][8];

bool check_king (vpp& my, vpp& atack, pair <int, int> king)
{
	bool atack_type = atack[0].first < 'a'; //white = 1, black = 0
	int sz = atack.size();
	int check = false;

	for (int i = 0; i < sz and !check; i++)
	{
		char piece = tolower(atack[i].first);
		int posi = atack[i].second.first, posj = atack[i].second.second;

		switch (piece)
		{
			case 'p':
			{
				int next = atack_type ? -1 : 1;

				check = king == make_pair (posi + next, posj + next);
				check = king == make_pair (posi + next, posj - next) ? true : check;
			}
			break;
			case 'k':
			{
				int nexti[] = {1, 1, -1, -1, 2, 2, -2, -2};
				int nextj[] = {2, -2, 2, -2, 1, -1, 1, -1};

				for (int k = 0; k < 8 and !check; k++)
					check = king == make_pair (posi + nexti[k], posj + nextj[k]);
			}
			break;
			case 'r':
				check = king.first == posi;
				check = king.second == posj ? true : check;
			break;
			case 'b':
			{
				bool move = true;
				int end, begin;

				if(move)
					check = (king.first - posi) == (king.second - posj);
			}
			break;
			default:;
		}
	}

	return check;
}

int main (void)
{
	char ch;
	int game = 1;

	for(;;)
	{
		pair <int, int> wk, bk;
		vpp b, w;

		printf ("Game %d: ", game++);

		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				scanf ("%c", &ch);
				board[i][j] = ch;

				if (isalpha(ch))
				{
					if (ch >= 'a') b.push_back( make_pair(ch, make_pair(i, j)) );
					else w.push_back ( make_pair (ch, make_pair (i, j)) );
				}

				if (ch == 'K') wk = make_pair (i, j);
				if (ch == 'k') bk = make_pair (i, j);
			}
			getchar();
		}
		getchar();

		if (!b.size() and !w.size()) break;

		//if (check_king (b, wk, 1)) puts ("black king is in check.");
		//else if (check_king (w, bk, 0)) puts ("white king is in check.");
		//else puts ("no king is in check.");
	}

	return 0;
}

