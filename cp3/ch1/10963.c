#include <stdio.h>

int main (void)
{
	int n, w;
	
	scanf ("%d%*c", &n);
	while (n--)
	{
		char flag = 1;
		int gap,y1,y2;
		getchar();
		scanf ("%d", &w);
		scanf ("%d %d", &y1, &y2);
		gap = y1 - y2;
		while (--w)
		{
			scanf ("%d %d", &y1, &y2);
			if (gap != y1 - y2) flag=0;
		}
		puts ((flag ? "yes":"no"));
		if (n) putchar('\n');
	}

	return 0;
}

