//compilar sem flag ansi
//com g++
#include <stdio.h>

int main (void)
{
	int l;
	while (scanf ("%d%*c", &l), l)
	{
		char lastsign = '+', lastdir = 'x';
		char instrsign, instrdir;
		for (int i = 1; i < l; i++)
		{
			scanf ("%c%c%*c", &instrsign, &instrdir);
			if (lastdir == 'x' and instrsign != 'N')
			{
				lastdir = instrdir;
			       if (lastsign == '-')
				       lastsign = (instrsign == '-') ? ('+') : ('-');
			       else
				       lastsign = instrsign;
			}
			else if (lastdir == 'y' and instrdir == 'y')
			{
				lastdir = 'x';
				if (lastsign == '+') lastsign = (instrsign == '-') ? ('+') : ('-');
				else lastsign = instrsign;
			}
			else
			{
				if (lastdir == 'z' and instrdir == 'z')
				{
					lastdir = 'x';
					if (lastsign == '-') lastsign = instrsign;
					else lastsign = (instrsign == '-') ? ('+') : ('-');
				}
			}
		}

		printf ("%c%c\n", lastsign, lastdir);
	}

	return 0;
}

