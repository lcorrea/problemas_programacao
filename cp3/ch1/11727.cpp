#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	int salary[3];
	int t;

	scanf ("%d", &t);

	for (int i = 1; i <= t; i++)
	{
		scanf ("%d %d %d", &salary[0], &salary[1], &salary[2]);
		sort (salary, salary+3);
		printf ("Case %d: ", i);
		printf ("%d\n", salary[1]);
	}

	return 0;
}

