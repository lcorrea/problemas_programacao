#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int h, m;

	while (scanf ("%d:%d", &h, &m), h or m)
	{
		double m1 = m*6.0;
		double h1 = (h*60.0 + m)*.5;
		double angle = abs(h1-m1);

		printf ("%.3lf\n", angle>180 ? 360. - angle : angle);
	}

	return 0;
}

