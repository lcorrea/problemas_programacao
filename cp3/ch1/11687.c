#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main (void)
{
	char xo[1000001];

	while (scanf ("%s", xo), isdigit(xo[0]))
	{
		int len = strlen (xo);

		if (len == 1 && xo[0] == '1') puts ("1");
		else if (len == 1 && xo[0] != '1') puts ("2");
		else if (len <= 9) puts ("3");
		else puts ("4");
	}

	return 0;
}

