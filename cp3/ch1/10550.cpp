#include <stdio.h>

inline int mod40 (int a) { return (a < 0) ? ((a % 40) + 40) : (a % 40); }

int main (void)
{
	int init, pos1, pos2, pos3;
	int degrees;

	while (scanf ("%d %d %d %d", &init, &pos1, &pos2, &pos3), init or pos1 or pos2 or pos3)
	{
		degrees = 1080;
		degrees += mod40 (init - pos1) * 9;
		degrees += mod40 (pos2 - pos1) * 9;
		degrees += mod40 (pos2 - pos3) * 9;
		printf ("%d\n", degrees);				
	}

	return 0;
}

