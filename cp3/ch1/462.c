#include <stdio.h>
#include <string.h>
#include <ctype.h>

int cards[5][14];
int suits[5];
int points;

void parser (char suit, char rank)
{
	int i = 0, j = 0, pt = 0;
	
	switch (suit)
	{
		case 'C': i++;
		case 'D': i++;
		case 'H': i++;
		case 'S': i++;
	}

	if (isdigit (rank))  j = rank - '0';
	else
	{
		j = 9;
		switch (rank)
		{
			case 'K': j++; pt++;
			case 'Q': j++; pt++;
			case 'J': j++; pt++;
			case 'T': j++;
			break;
			case 'A': j = 1; pt = 4;
		}
	}

	cards[i][j]++;
	suits[i]++;
	points += pt;
}

char check (void)
{
	char rules = 0;
	int i;

	for (i=1; i <= 4; i++)
		if (cards[i][13] && suits[i] == 1) 
			points--;
	
	for (i=1; i <= 4; i++)
		if (cards[i][12] && suits[i] <= 2)
			points--;
	
	for (i=1; i <= 4; i++)
		if (cards[i][11] && suits[i] <= 3)
			points--;

	if (points >= 16)
		rules |= 1;

	for (i=1; i <= 4; i++)
	{
		if (suits[i]==2) rules |= (1<<5), points++;
		if (suits[i]==1) rules |= (1<<6), points += 2;
		if (suits[i]==0) rules |= (1<<7), points += 2;
	}

	for (i=1; i <= 4; i++)
	{
		if ((cards[i][1]==1) || (cards[i][13] && suits[i] > 1)
				|| (cards[i][12] && suits[i] > 2))
			rules |= (1<<i); 
	}

	return rules;
}

int main (void)
{
	char suit, rank;
	int i;

	while (scanf ("%c%c%*c", &rank, &suit) != EOF)
	{
		parser (suit, rank);

		for (i=2; i <= 13; i++)
		{
			scanf ("%c%c%*c", &rank, &suit);
			parser (suit, rank);
		}

		char rules = check();

		if (points < 14) puts ("PASS");
		else if (points >= 16 && (rules & 0b11111) == 0b11111) puts ("BID NO-TRUMP");
		else
		{
			printf ("BID ");
			int m = 1;

			for (i=1; i<=4; i++)
				if (suits[m] < suits[i])
					m = i;
			
			if (m==1) puts ("S");
			else if (m==2) puts ("H");
			else if (m==3) puts ("D");
			else puts ("C");
		}

		memset (cards, 0, sizeof cards);
		memset (suits, 0, sizeof suits);
		points = 0;
				
	}

	return 0;
}

