#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	int b, n, h, w;
	int p, a;

	while (scanf ("%d %d %d %d", &n, &b, &h, &w) != EOF)
	{
		bool stayHome = true;
		int cost = 1000555;
		for (int i = 0; i < h; i++)
		{
			scanf ("%d", &p);

			for (int j = 0; j < w; j++)
			{
				scanf ("%d", &a);

				if (n <= a)
				{
					stayHome = false;
					cost = min (cost, p*n);
				}
			}
		}

		if (stayHome or cost > b) puts ("stay home");
		else printf ("%d\n", cost);
	}

	return 0;
}

