#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int calc (int x)
{
	if (x < 10) { return x; }
	
	char str[20], i, len;
	int sum = 0;

	sprintf (str, "%d", x);
	for (i=0, len = strlen (str); i < len; i++)
		sum += str[i] - '0';

	return calc (sum);
}


int main (void)
{
	int n;
	while (scanf ("%d", &n), n) { printf ("%d\n", calc(n)); }
	return 0;
}

