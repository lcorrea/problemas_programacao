#include <stdio.h>

int main (void)
{
	int n, b;

	while (scanf("%d %d", &b, &n), n && b)
	{
		int banks[22];
		int i, r, d, c;
		for (i=1; i <= b; i++) scanf ("%d", &r), banks[i]=r;
		for (i=0; i < n; i++) 
		{
			scanf ("%d %d %d", &d, &c, &r);
			banks[c] += r;
			banks[d] -= r;
		}
		for (i=1; i <= b && banks[i]>=0; i++);
		puts ((i>b)?"S":"N");
	}
	return 0;
}

