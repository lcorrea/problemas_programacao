#include <stdio.h>
#include <stdlib.h>
#define min(a,b) ((a<b)?(a):(b))
int main (void)
{
	int a, b;

	while (scanf ("%d %d", &a, &b), a!=-1)
	{
		if (a > b)
		{
			int t = a;
			a=b;
			b = t;
		}
		printf ("%d\n", min(b-a, a+100-b));
	}

	return 0;
}

