#include <stdio.h>
#include <string.h>

char str[1000002];
int ones[1000002];
int zeros[1000002];

void process (void)
{
	int len = strlen (str);
	int i = len-1;

	while (i >= 0)
	{
		char pos = str[i];
		int endPos = i;
		while (i >= 0 && pos == str[i])
		{
			zeros[i] = (pos=='0')?(endPos):(-1);
			ones[i] = (pos=='1')?(endPos):(-1);
			i--;
		}
	}

	/*for (i=0;i<len;i++)
		printf ("%d | %d\n", zeros[i], ones[i]);*/
}

int main (void)
{
	int cases=1;

	while (1) 
	{
		char c;
		int p = 0;

		while ((c = getchar()) != EOF && c != '\n')
			str[p++] = c;
		str[p] = '\0';

		if (p == 0 || c == EOF) break;

		int queries;
		scanf ("%d%*c", &queries);
		process();
		printf ("Case %d:\n", cases++);
		while (queries--)
		{
			int i, j;
			scanf ("%d %d%*c", &i, &j);
			if (j < i)
			{
				int tmp = i;
				i =  j;
				j = tmp;
			}

			if (j == i || zeros[i] >= j || ones[i] >= j)
				puts ("Yes");
			else
				puts ("No");

			/*	for (; i <= j; i++) putchar (str[i]), printf (" (%d %d)\n", zeros[i], ones[i]);
			putchar ('\n');*/
		}
	}

	return 0;
}

