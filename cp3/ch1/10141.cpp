#include <string>
#include <algorithm>
#include <stdio.h>
#include <iostream>

using namespace std;

struct proposal
{
	string name;
	double compilance;
	double price;
};

bool comp (struct proposal a, struct proposal b)
{
	if (a.compilance == b.compilance) return a.price > b.price;
	return a.compilance < b.compilance;
}

int main (void)
{
	struct proposal p, pc;
	int n, np, teste = 1, nc;
	string req;

	scanf ("%d %d%*c", &n, &np);
	while (n and np)
	{
		for (int i = 0; i < n; i++) getline (cin, req);
		
		getline (cin, pc.name);
		scanf ("%lf %d%*c", &pc.price, &nc);
		pc.compilance = (double) nc / n;
		for (int i = 0; i < nc; i++) getline (cin, req);

		for (int i = 1; i < np; i++)
		{
			getline (cin, p.name);
			scanf ("%lf %d%*c", &p.price, &nc);
			p.compilance = (double) nc / n;
			for (int j = 0; j < nc; j++) getline (cin, req);
			
			pc = max (pc, p, comp);
		}

		printf ("RFP #%d\n", teste++);
		printf ("%s\n", pc.name.c_str());

		scanf ("%d %d%*c", &n, &np);
		if (n and np) putchar ('\n');
	}

	return 0;
}

