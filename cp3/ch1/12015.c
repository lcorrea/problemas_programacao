#include <stdio.h>

int main (void)
{
	char sites[10][105];
       	int relevance[10];
	int max, n, c = 1;

	scanf ("%d", &n);

	for (; c <= n; c++)
	{
		int i = 0, r;
		max = 0;
		printf ("Case #%d:\n", c);
		for (; i < 10; i++)
		{
			scanf ("%s %d", sites[i], &relevance[i]);
			max = (relevance[i] > max)?(relevance[i]):(max);
		}
		for (i=0; i < 10; i++) if (relevance[i]==max) puts (sites[i]);
	}
	return 0;
}

