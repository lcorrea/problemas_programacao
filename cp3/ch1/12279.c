#include <stdio.h>

int main (void)
{
	int c = 1;
	int n, i;

	while (scanf ("%d", &n), n)
	{
		int cnt = 0, m = n;
		while (m--) { scanf ("%d", &i); cnt += i>0; }
		printf ("Case %d: %d\n", c++, 2*cnt-n);
	}

	return 0;
}

