#include <stdio.h>
#include <string.h>

int main (void)
{
	char str[300];
	int n;

	scanf ("%d", &n);

	while (n--)
	{
		scanf ("%s", str);
		int len = strlen (str);
		if ((len == 1 && (str[0] == '1' || str[0] == '4')) ||
			(len == 2 && str[0] == '7' && str[1] == '8')) puts ("+");
		else if (str[len-2] == '3' && str[len-1] == '5') puts("-");
		else if (str[0] == '9' && str[len-1] == '4') puts ("*");
		else if (len > 3 && str[0] == '1' && str[1] == '9' && str[2] == '0') puts ("?");
		else;
	}

	return 0;
}

