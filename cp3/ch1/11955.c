#include <stdio.h>
#include <string.h>

int main (void)
{
	unsigned char memory[100];
	int n, t=1;

	scanf ("%d%*c", &n);

	while (n--)
	{
		memset (memory, 0, 100);
		unsigned char *ptr = memory, cmd;

		while ((cmd = getchar()) != '\n')
		{
			switch (cmd)
			{
				case ('>'):
					if (ptr < &memory[99])
						ptr++;
					else
						ptr = memory;
					break;
				case ('<'):
					if (ptr > memory)
						ptr--;
					else
						ptr = &memory[99];
					break;
				case ('+'):
					(*ptr)++;
					break;
				case ('-'):
					(*ptr)--;
					break;
			}
		}
		printf ("Case %d:", t++);
		int i;
		for (i = 0; i < 100; i++)
			printf (" %02X", memory[i]);
		putchar ('\n');
	}

	return 0;
}

