#include <stdio.h>

int main (void)
{
	int c = 1, n, t, h, l, past, now;

	scanf("%d", &t);

	for (; c <= t; c++)
	{
		scanf ("%d", &n);
		scanf ("%d", &past);
		h = l = 0;
		while (--n)
		{
			scanf ("%d", &now);
			if (now > past) h++;
			if (now < past) l++;
			past = now;
		}

		printf ("Case %d: %d %d\n", c, h, l);

	}

	return 0;
}

