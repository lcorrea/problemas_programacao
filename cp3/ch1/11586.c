#include <stdio.h>
#include <string.h>

int main (void)
{
	int n;
	char c;

	scanf ("%d%*c", &n);
	while (n--)
	{
		int m1 = 0, f1 = 0, m2 = 0, f2=0, cnt=0;
		while ((c = getchar()) != '\n')
		{
			if (c == ' ') continue;
			if (c == 'M') m1++;
			else f1++;
			if (getchar() == 'M') m2++;
			else f2++;
			cnt++;
		}

		if (m1 == f2 && f1 == m2 && cnt > 1) puts ("LOOP");
		else puts ("NO LOOP");
	}

	return 0;
}

