#include <stdio.h>
#define max(a,b) ((a>b)?(a):(b))
int main (void)
{
	int n, m, c,seq=1;
	int a[21];
	while (scanf ("%d %d %d", &n, &m, &c), n && m && c)
	{
		int i,fuse=0, exceeds=0, maxPower = 0;

		for (i = 1; i <= n; i++)
			scanf ("%d", &a[i]);

		for (i = 0; i < m; i++)
		{
			int device;

			scanf ("%d", &device);
			fuse += a[device];
			a[device] = -a[device];
			
			if (fuse > c) exceeds=1;
			
			maxPower = max(fuse, maxPower);
		}

		printf ("Sequence %d\n", seq++);
		if (exceeds) puts ("Fuse was blown.");
		else printf ("Fuse was not blown.\nMaximal power consumption was %d amperes.\n", maxPower);
		putchar ('\n');
	}

	return 0;
}

