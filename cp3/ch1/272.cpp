#include <stdio.h>

int main (void)
{
	bool endQuote = false;
	char c;

	while (scanf ("%c", &c) != EOF)
	{
		if (c == '"')
		{
			if (endQuote)
				putchar ('\''), putchar ('\'');
			else
				putchar ('`'), putchar ('`');
			
			endQuote = !endQuote;
		}
		else
			putchar (c);
	}

	return 0;
}

