#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	int p, u, t, n, x;

	scanf ("%d", &t);

	while (t--)
	{
		p = 100;
		u = 0;
		scanf ("%d",&n);

		while (n--)
		{
			scanf ("%d", &x);
			
			p = min (x, p);
			u = max (x, u);
		}

		printf ("%d\n", 2*(u - p));
	}
	return 0;
}

