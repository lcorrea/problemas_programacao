#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	double height = 0;
	int h, u, d, g;
	while (scanf ("%d %d %d %d", &h, &u, &d, &g), h)
	{
		int day = 0;
		double f = g*u/100.0;
		double alcance = u;
		height = 0;

		while (height >= 0 and height <= h)
		{
			day++;
			if (alcance > 0) height += alcance;
			alcance -= f;

			if (height >= 0 and height <= h)
				height -= d;
		}

		if (height > h) printf ("success ");
		else printf ("failure ");
		
		printf ("on day %d\n", day);
	}

	return 0;
}

