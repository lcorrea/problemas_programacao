#include <stdio.h>

int main (void)
{
	int t, c = 1, n;

	scanf ("%d",&t);

	for (; c <= t; c++)
	{
		printf ("Case %d: ", c);
		scanf ("%d", &n);
		int mile = 0, juice = 0, d;
		while (n--)
		{
			scanf ("%d", &d);
			mile += 10 + 10*((d>29)?(d/30):(0));
			juice += 15 + 15*((d>59)?(d/60):(0));
		}
		if (mile == juice) printf ("Mile Juice %d\n", mile);
		if (mile > juice) printf ("Juice %d\n", juice);
		if (mile < juice) printf ("Mile %d\n", mile);
	}

	return 0;
}

