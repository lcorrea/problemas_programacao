#include <stdio.h>
#include <ctype.h>
const char aa = 'a' - 1;

int calc (int a)
{
	if (a<10) return a;
	char str[50];
	int sum=0;
	sprintf (str, "%d", a);
	char *pstr = str;
	while (*pstr) sum += tolower(*pstr++) - '0';
	return calc (sum);
}

int main (void)
{
	char a[26], b[26], c;
	while ((c = getchar()) != EOF)
	{
		int sa = 0, sb = 0;
		if (isalpha(c))
			sa = tolower(c) - aa;
		while (c != '\n' && (c=getchar()) != '\n')
			if (isalpha(c))
				sa += tolower(c) - aa;
		while ((c=getchar()) != '\n')
			if (isalpha(c))
				sb += tolower(c) - aa;
		if ((sa = calc(sa)) >= (sb = calc(sb)) && sa)
			printf ("%.2lf %%\n", (double) sb/sa*100.0);
		else if (sb)
			printf ("%.2lf %%\n", (double) sa/sb*100.0);
		else
			putchar ('\n');
	}
	return 0;
}

