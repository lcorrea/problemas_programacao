#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int main(void)
{
	unsigned int n, m, f = 0;
	char c;

	cin >> n >> m;

	while(n && m)
	{
		long int ms[n][m];

		memset(ms, 0, m*n);

		for(int i = 0; i < n; i++)
		{
			scanf("%*c");
			for( int j = 0; j < m; j++)
			{
				scanf("%c", &c);

				if(c == '*')
				{
					ms[i][j] = -1;

					if(i - 1 >= 0 && j - 1 >= 0)
						if(ms[i - 1][j - 1] != -1)
							ms[i-1][j-1]++;

					if(i - 1 >= 0)
						if(ms[i - 1][j] != -1)
							ms[i-1][j]++;

					if(i - 1 >= 0 && j + 1 < m)
						if(ms[i - 1][j + 1] != -1)
							ms[i - 1][j + 1]++;

					if(j - 1 >= 0)
						if(ms[i][j-1] != -1)
							ms[i][j-1]++;

					if(j+1 < m)
						if(ms[i][j+1] != -1)
							ms[i][j+1]++;

					if(i + 1 < n && j - 1 >= 0)
						if(ms[i+1][j-1] != -1)
							ms[i+1][j-1]++;

					if(i + 1 < n)
						if(ms[i+1][j] != -1)
							ms[i+1][j]++;

					if(i+1 < n && j+1 < m)
						if(ms[i+1][j+1]!=-1)
							ms[i+1][j+1]++;


				//	for(int ii = -1; ii < 2; ii++)
				//	{
				//		for(int jj = -1; jj < 2; jj++)
				//		{
				//			if(ii == 0 && jj == 0)
				//			{
				//				;
				//			}
				//			else if(i + ii >= 0 && i + ii < n)
				//			{
				//				if(j + jj >= 0 && j + jj < m)
				//				{
				//					if(ms[i+ii][j+jj] != -1)
				//						ms[i+ii][j+jj]++;
				//				}
				//			}
				//		}
				//	}
				}
			}
		}
		f++;
		cout << "Field #" << f << ":\n";
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < m; j++)
			{
				if(ms[i][j] == -1)
					cout << '*';
				else
					cout << ms[i][j];
			}

			cout << endl;
		}

		cin >> n >> m;

		if(n&&m)
			cout << endl;	

	}

	return 0;
}
/*
#include <cstdio>

int main(void){
    int nDeColunas,
        nDeLinhas;

    int nDeCasosDeTeste = 0;

    //Fazemos um for sem argumentos
    for(;;){
        //Pegamos o numero de linhas e o numero de colunas a serem avaliadas
        scanf( "%d %d", &nDeColunas , &nDeLinhas );

        //Caso tenhamos alcançado a condição de parada
        if( nDeColunas == 0 && nDeLinhas == 0){
            break;//Saimos do laço for sem condição de parada
        }

        //Identificamos qual caso de teste estamos tratando
        nDeCasosDeTeste++;
        //Este é o provável motivo da tua reposta estar errada. Não se dá \n na última linhas
        if( nDeCasosDeTeste > 1){
            printf("\n");
        }

        //Uma matriz bidimencional de caracteres para manter nosso campo
        char field[nDeLinhas][nDeColunas];

        //Leio caracter a caracter da minha entrada e preencho minha matriz field
        for( int c = 0 ; c < nDeColunas ; c++ ){           
            //Toda vez que uma coluna acabar, haverá um caracter de quebra de linha '\n'
            //Vamos descartar ele. Lembrando também que o primeiro caractere que será lido será um '\n'
            getchar();
            for( int l = 0 ; l < nDeLinhas ; l++ ){
                field[l][c] = getchar();//Vamos ler cada elemento de uma coluna
            }
        }

        //Uma matriz para guardar nossa resposta
        char mineAns[nDeLinhas][nDeColunas];

        //Le em todos os campos não mina '.'(ponto) e identifica quantas minas existem próximo
        for( int c = 0 ; c < nDeColunas ; c++ ){
            for( int l = 0 ; l < nDeLinhas ; l++ ){
                //Se for uma mina nem verificamos
                if( field[l][c] == '*' ){
                    mineAns[l][c] = '*';
                    continue;
                }

                //Uma variável temporária para guardar o número de determinado espaço
                int tempNum = 0;

                //São 8 verificações de visinhos para calcular o número a ser mostrado
                // Também precisamos verificar se não estamos lendo fora do vetor
                //Topo e a esquerda.
                if( l - 1 >= 0 && c - 1 >= 0) if( field[l-1][c-1] == '*') tempNum++;
                //Topo e a cima.
                if( l - 1 >= 0 ) if( field[l-1][c] == '*') tempNum++;
                //Topo e a direita.
                if( l - 1 >= 0 && c + 1 < nDeColunas) if( field[l-1][c+1] == '*') tempNum++;
                //Esquerda
                if( c - 1 >= 0 ) if( field[l][c-1] == '*') tempNum++;
                //Direita
                if( c + 1 < nDeColunas ) if( field[l][c+1] == '*') tempNum++;
                //Esquerda e abaixo
                if( l + 1 < nDeLinhas && c - 1 >= 0 ) if( field[l+1][c-1] == '*') tempNum++;
                //abaixo
                if( l + 1 < nDeLinhas ) if( field[l+1][c] == '*') tempNum++;
                //Direita e abaixo
                if( l + 1 < nDeLinhas && c + 1 < nDeColunas ) if( field[l+1][c+1] == '*') tempNum++;

                //Grava na matriz de resposta
                mineAns[l][c] = tempNum + 48;//Estou fazendo uma conversão "na marra" de int para char. (char)tempNum não funcionou.
            }
        }

        printf("Field #%d:\n", nDeCasosDeTeste);
        //Exibe a resposta
        for( int c = 0 ; c < nDeColunas ; c++ ){
            for( int l = 0 ; l < nDeLinhas ; l++ ){
                printf("%c" , mineAns[l][c]);
            }
            printf("\n");
        }
    }

    return 0;
}*/
