#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	int months, deprec;
	double downpay, loan;

	while (scanf ("%d %lf %lf %d", &months, &downpay, &loan, &deprec),
			months >= 0)
	{
		double d[110] = {0};
		double worth = loan + downpay;
		double p = loan, f = loan/months; //o segredo eh entender o valor da parcela
		int ma, m, r = 0;

		scanf ("%d %lf", &ma, d);

		for (int i = 1; i < deprec; i++)
			scanf ("%d", &m), scanf("%lf", d + m);

		worth -= d[r]*worth; r++;

		while (worth < p)
		{
			p -= f;

			if (d[r] == 0) d[r] = d[r-1];

			worth -= d[r]*worth; r++;
		}

		r--;
		printf ("%d ", r);
		puts ((r != 1) ? ("months") : ("month"));
	}

	return 0;
}

