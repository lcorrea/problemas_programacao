// refazer utilizando map
#include <stdio.h>
#include <string.h>

int main (void)
{
	
	int n = 0;
	char word[15];

	while (scanf ("%s", word), word[0] != '#')
	{
		printf ("Case %d: ", ++n);

		if (strcmp ("HELLO", word)==0) puts ("ENGLISH");
		else if (strcmp("HALLO", word) == 0) puts ("GERMAN");
		else if (strcmp("HOLA", word) == 0) puts ("SPANISH");
		else if (strcmp ("BONJOUR", word) == 0) puts ("FRENCH");
		else if (strcmp ("CIAO", word) == 0) puts ("ITALIAN");
		else if (strcmp ("ZDRAVSTVUJTE", word) == 0) puts ("RUSSIAN");
		else puts ("UNKNOWN");
	}

	return 0;
}

