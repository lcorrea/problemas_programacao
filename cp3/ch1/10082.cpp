#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char ch;
	char rows[][40] = {"`1234567890-=", "QWERTYUIOP[]\\", "ASDFGHJKL;'", "ZXCVBNM,./"};
	map <char, char> kb;

	for (int i = 0; i < 4; i++)
		for (int j = 1, len = strlen(rows[i]); j < len; j++)
			kb[rows[i][j]] = rows[i][j-1];

	kb[' '] = ' ';
	kb['\n'] = '\n';

	while ((ch = getchar()) != EOF)
		putchar(kb[ch]);

	return 0;
}

