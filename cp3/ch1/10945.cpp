#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char str[10000];
	char c;
	
	for (;;)
	{
		int i = 0;
		while ((c = getchar()), c != '\n')
			if (isalpha(c)) str[i++] = tolower(c);
		str[i] = '\0';

		if (!strcmp (str, "done")) break;

		bool p = true;

		for (int j = 0, k = i - 1; j < k and p; j++, k--)
			p = str[j] == str[k];

		puts (p ? "You won't be eaten!" : "Uh oh..");
	}

	return 0;
}

