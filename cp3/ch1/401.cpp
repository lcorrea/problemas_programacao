#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char str[30];
	char rev[300] = {0};
	
	rev['A'] = 'A'; rev['M'] = 'M'; rev['W'] = 'W'; rev['3'] = 'E';
	rev['E'] = '3'; rev['O'] = 'O'; rev['X'] = 'X'; rev['5'] = 'Z';
	rev['H'] = 'H'; rev['S'] = '2'; rev['Y'] = 'Y'; rev['8'] = '8';
	rev['I'] = 'I'; rev['T'] = 'T'; rev['Z'] = '5';
	rev['J'] = 'L'; rev['U'] = 'U'; rev['1'] = '1';
	rev['L'] = 'J'; rev['V'] = 'V'; rev['2'] = 'S';

	while (scanf("%s", str) != EOF)
	{
		int len = strlen (str);
		int i = 0, j = len - 1;
		bool p = true;

		while (j > i)
		{
			if (str[i++] != str[j--])
			{
				p = false;
				break;
			}
		}

		j = len - 1;
		i = 0;
		bool m = true;

		while (j >= i)
		{
			if (str[i++] != rev[str[j--]])
			{
				m = false;
				break;
			}
		}

		printf ("%s -- ", str);
		
		if (p)
			if (m)
				puts ("is a mirrored palindrome.\n");
			else
				puts ("is a regular palindrome.\n");
		else
			if (m)
				puts ("is a mirrored string.\n");
			else
				puts ("is not a palindrome.\n");
	}

	return 0;
}

