#include <stdio.h>

int main (void)
{
	char str[6];
	int c = 1;

	while (scanf ("%s", str), str[0] != '*')
	{
		printf ("Case %d: ", c++);
		if (str[0] != 'H') puts ("Hajj-e-Asghar");
		else puts ("Hajj-e-Akbar");
	}

	return 0;
}

