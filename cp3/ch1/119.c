#include <stdio.h>
#include <string.h>

struct people {
	char name[13];
	int money;
};

int main (void)
{
	struct people group[10];
	int n;
	int r = scanf ("%d", &n);
	while (r != EOF)
	{
		int i;
		for (i = 0;i < n; i++)
			scanf ("%s", group[i].name), group[i].money = 0;
		for (i = 0; i < n; i++)
		{
			char name[13];
			int j = 0, money = 0, friends = 0, gift = 0;
			scanf ("%s", name);
			while (strcmp (name, group[j].name)) j++;
			scanf ("%d %d", &money, &friends);
			if (friends)
				gift = money/friends;
			group[j].money -= gift*friends;
			for (j = 0; j < friends; j++)
			{
				int f = 0;
				scanf ("%s", name);
				while (strcmp (group[f].name, name)) f++;
				group[f].money += gift;
			}
		}
		
		for (i = 0; i < n; i++)
			printf ("%s %d\n", group[i].name, group[i].money);
		if ((r = scanf ("%d", &n)) != EOF) putchar ('\n');
	}

	return 0;
}

