#include <stdio.h>

int main (void)
{
	int t, c = 1;

	scanf ("%d", &t);

	while (t--)
	{
		int  x, y, z;
		scanf ("%d %d %d", &x, &y, &z);
		printf ("Case %d: ", c++);
		if (x <= 20 && y <= 20 && z <= 20) puts ("good");
		else puts ("bad");
	}

	return 0;
}

