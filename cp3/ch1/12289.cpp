#include <iostream>
#include <string>

using namespace std;

int main (void)
{

	string word;
	int n;

	cin >> n;

	while (n--)
	{
		cin >> word;
		if (word.length() > 3) cout << 3;
		else if ((word[0] == 'o' and word[2] == 'e') or  (word[1] == 'n' and word[2] == 'e') or (word[0] == 'o' and word[1]=='n'))
			cout << 1;
		else
			cout << 2;
		cout << endl;
	}

	return 0;
}

