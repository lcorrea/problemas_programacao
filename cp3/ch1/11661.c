#include <stdio.h>
#include <stdlib.h>
#define min(a,b) ((a<b)?(a):(b))
int main (void)
{
	int l;

	while (scanf ("%d%*c", &l), l)
	{
		int lastR = 0;
		int lastD = lastR;
		int minL = 2000002;
		int dist = 1;

		while (l--)
		{
			char c = getchar();
			if (c=='Z') minL = 0;
			if (c=='R') lastR = dist;
			if (c=='D') lastD = dist;
			if (lastD && lastR)
				minL = min(minL, abs(lastR-lastD));
			dist++;
		}
		printf ("%d\n", minL);
	}

	return 0;
}

