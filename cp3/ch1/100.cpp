#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

map<unsigned int, unsigned int> memo;

int main(void)
{
	unsigned int i, j, ii, jj;
	unsigned int maxC, cycles;

	memo[1] = 1;

	while(cin >> i >> j && i && j)
	{
		ii = i;
		jj = j;

		maxC = 1;
		if(j < i)
			swap(j, i);

		if(i == 1)
			i++;

		for(; i <= j; i++)
		{
			
			cycles = memo[i];

			if(cycles == 0)
			{
				unsigned int a = i;
				cycles++;

				while(a > 1)
				{
					if(a%2)
						a = a*3 + 1;
					else
						a /= 2;

					cycles++;
				}

				memo[i] = cycles;
			}

			maxC = max(maxC, cycles);

		}

		cout << ii << ' ' << jj << ' ' << maxC << endl;
	}

	return 0;
}

