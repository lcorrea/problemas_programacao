#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	cin >> n;

	while (n--)
	{
		unsigned long s;
		cin >> s;

		bool is_p = false;
		int it = 0;

		for (; !is_p; it++)
		{
			char new_s[100], *ptr;
			
			sprintf (new_s, "%lu", s);
			reverse (new_s, new_s + strlen (new_s));
			ptr = new_s;

			while (*ptr == '0' and ptr - new_s < strlen(new_s) - 1) ptr++;

			s += strtoul (ptr, NULL, 0);

			sprintf (new_s, "%lu", s);

			is_p = true;

			for (int i = 0, sz = strlen (new_s) - 1; i < sz and is_p; i++, sz--)
				is_p = new_s[i] == new_s[sz];
		}

		printf ("%d %lu\n", it, s);
	}

	return 0;
}

