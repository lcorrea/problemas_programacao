#include <stdio.h>

typedef long long int ll;

int main (void)
{
	ll a, b;
	int t;

	scanf ("%d", &t);

	while (t--)
	{
		scanf ("%lld %lld", &a, &b);

		if (a < b) putchar ('<');
		else if (a > b) putchar ('>');
		else putchar ('=');
		putchar ('\n');
	}

	return 0;
}

