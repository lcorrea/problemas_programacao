#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int t = 1;
	int n;

	scanf ("%d%*c", &n);

	while (n--)
	{
		char str[20000], c;
		int i = 0;

		while ((c = getchar()), c != '\n')
			if (isalpha(c)) str[i++] = tolower (c);
		str[i] = '\0';

		printf ("Case #%d:\n", t++);

		int k = sqrt(i);
		if (k*k == i)
		{
			char square[k][k+1];

			for (int j = 0, l = 0; j < i; j += k, l++)
				memcpy (square[l], str + j, k), square[l][k] = '\0';

			int ok = 0;
			string s;

			for (int j = 0; j < k; j++)
				s += square[j];

			ok = !strcmp(s.c_str(), str);
			s.clear();

			for (int j = 0; j < k; j++)
				for (int l = 0; l < k; l++)
					s += square[l][j];

			ok |= ((!strcmp(s.c_str(), str)) << 1);
			s.clear();

			for (int j = k-1; j >= 0; j--)
				for (int l = k-1; l >= 0; l--)
					s += square[j][l];
			
			ok |= ((!strcmp(s.c_str(), str)) << 2);
			s.clear();

			for (int j = k-1; j >= 0; j--)
				for (int l = k-1; l >= 0; l--)
					s += square[l][j];

			ok |= ((!strcmp(s.c_str(), str)) << 3);

			if (ok == 0b1111) { printf ("%d\n", k); continue;}
		}

		puts ("No magic :(");

	}

	return 0;
}

