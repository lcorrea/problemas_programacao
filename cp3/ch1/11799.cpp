#include <stdio.h>
#include <algorithm>

using namespace std;

int main (void)
{
	int minS = -1, speed;
	int n;
	
	scanf ("%d", &n);
	for (int i = 1; i <= n; i++)
	{
		int creatures = 0;
		minS = -1;
		scanf ("%d", &creatures);
		while (creatures--)
			scanf ("%d", &speed), minS = max (speed, minS);
		printf ("Case %d: %d\n", i, minS);
	}

	return 0;
}

