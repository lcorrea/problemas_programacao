#include <stdio.h>

int main (void)
{
	int t, m = 0, k;
	char str[7];

	scanf ("%d", &t);

	while (t--)
	{
		scanf ("%s", str);
		if (str[0] == 'd') scanf ("%d", &k), m+=k;
		else printf ("%d\n", m);
	}

	return 0;
}

