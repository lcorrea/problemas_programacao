#include <cstdio>
#include <cstdlib>

using namespace std;

int main (void)
{
	int t;
	long long int n;
	scanf ("%d",&t);

	while (t--)
	{
		scanf ("%lld", &n);
		n = abs((((n*63 + 7492)*5 - 498)/10) % 10);
		printf ("%lld\n", n);
	}

	return 0;
}

