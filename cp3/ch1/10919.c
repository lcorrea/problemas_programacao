#include <stdio.h>
#include <string.h>

int main (void)
{
	int k, m;
	while (scanf ("%d", &k), k)
	{
		int i, c, ok = 1;
		int courses [100000];
		memset (courses, 0, 100000);
		scanf ("%d", &m);
		for (i = 0; i < k; i++)
			scanf ("%d", &c), courses[c] = 1;
		while (m--)
		{
			int t, p, fufil=0;
			scanf ("%d %d", &t, &p);
			for (i=0; i < t; i++)
			{
				scanf ("%d", &c);
				if (courses[c])
					fufil++;
			}
			if (fufil < p) ok = 0;
		}
		if (ok) puts ("yes");
		else puts ("no");
	}

	return 0;
}

