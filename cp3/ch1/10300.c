#include <stdio.h>

int main (void)
{
	int n;

	scanf ("%d", &n);

	while (n--)
	{
		unsigned long long int budget = 0;
		int farmers;
		scanf ("%d", &farmers);
		while (farmers--)
		{
			int farmyard, env;
			scanf ("%d %*d %d", &farmyard, &env);
			budget += farmyard*env;
		}
		printf ("%llu\n", budget);
	}

	return 0;
}

