#include <bits/stdc++.h>

using namespace std;

class unionFind
{
	vector <int> p, h, f;

	public:
		unionFind (int n)
		{
			p.assign(n, 0);
			h.assign(n, 0);
			f.assign(n, 1);

			for (int i = 0; i < n; i++)
				p[i] = i;
		}

		int findSet (int i)
		{
			return p[i] == i ? i : p[i] = findSet(p[i]);
		}

		bool isSameSet(int i, int j)
		{
			return findSet(i) == findSet(j);
		}

		void join(int i, int j)
		{
			if (isSameSet(i, j)) return;

			int a = p[i], b = p[j];

			if (h[a] > h[b])
			{
				f[a] += f[b];
				p[b] = a;
			}
			else
			{
				f[b] += f[a];
				p[a] = b;

				if (h[a] == h[b]) h[b]++;
			}
		}

		int getSizeGroup(int x)
		{
			return f[findSet(x)];
		}
};


int main (void)
{
	int n, m;

	while (scanf ("%d%d", &n, &m), n or m)
	{
		int members;
		int first;
		unionFind groups(n);

		while (m--)
		{
			scanf ("%d%d", &members, &first);

			for (int i = 1, second; i < members; i++)
			{
				scanf ("%d", &second);
				groups.join(first, second);
				first = second;
			}
		}

		printf ("%d\n", groups.getSizeGroup(0));
	}

	return 0;
}

