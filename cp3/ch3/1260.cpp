#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int teste;

    scanf ("%d", &teste);

    while (teste--)
    {
        int n;

        scanf ("%d", &n);

        int A[n], cnt = 0;

        for (int i = 0; i < n; i++)
            scanf ("%d", A + i);

        for (int i = 1; i < n; i++)
            for (int j = 0; j < i; j++)
                cnt += A[j] <= A[i];

        printf ("%d\n", cnt);
    }

    return 0;
}

