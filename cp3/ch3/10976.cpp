#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int k;

    while (scanf ("%d", &k) != EOF)
    {
        vector < pair <int, int> > xy;

        for (int x = k + 1; x <= 2*k; x++)
        {
            long long int y = 1LL*k*x/(x - k);

            if (y*x/(x+y) == k)
                xy.push_back ( make_pair (y, x) );
        }

        int sz = (int) xy.size();

        printf ("%d\n", sz);
        for (int i = 0; i < sz; i++)
            printf ("1/%d = 1/%d + 1/%d\n", k, xy[i].first, xy[i].second);
    }

    return 0;
}

