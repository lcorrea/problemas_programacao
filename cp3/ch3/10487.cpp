#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, m, Case = 1;
  int v[1000];

  while(scanf ("%d", &n), n)
  {
    vector <int> sum;

    for (int i = 0; i < n; i++)
      scanf ("%d", v + i);

    scanf ("%d", &m);

    for (int i = 0; i < n; i++)
      for (int j = i + 1; j < n; j++)
        sum.push_back (v[i] + v[j]);

    sort (sum.begin(), sum.end());

    printf ("Case %d:\n", Case++);

    while (m--)
    {
      int q, closest;

      scanf ("%d", &q);

      closest = sum[0];

      for (int i = 1; i < (int) sum.size(); i++)
        if (abs (sum[i] - q) <= abs (closest - q))
          closest = sum[i];
        else
          break;

      printf ("Closest sum to %d is %d.\n", q, closest);
    }
  }

  return 0;
}

