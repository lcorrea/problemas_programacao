#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int c;

	scanf ("%d", &c);

	while (c--)
	{
		int n;
		int median = 0;
		int dist = 0;
		vector <int> relatives;

		scanf ("%d", &n);

		for (int i = 0, r; i < n; i++)
			scanf ("%d", &r), relatives.push_back (r);

		sort (relatives.begin(), relatives.end());

		median = n % 2 ? relatives[n/2] : (relatives[n/2] + relatives[n/2 - 1])/2;

		for (int i = 0; i < n; i++)
			dist += abs (median - relatives[i]);

		printf ("%d\n", dist);
	}

	return 0;
}

