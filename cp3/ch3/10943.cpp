#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int tb[101][101] = {0};

  for (int i = 0; i <= 100; i++)
    tb[i][1] = 1;

  for (int numbers = 2; numbers <= 100; numbers++)
    for (int number = 0; number <= 100; number++)
      for (int i = 0; i <= number; i++)
        tb[number][numbers] = (tb[number][numbers]%1000000 + tb[number - i][numbers-1]) % 1000000;

  int n, k;

  while (scanf ("%d%d", &n, &k), n and k)
    printf ("%d\n", tb[n][k]);

  return 0;
}

