import math

def calc(v, v0):

    if (v <= v0):
        return 0

    return .3 * math.sqrt(v - v0)


#----------------- solution ----------------------


v, v0 = map(int, input().split())

while v and v0:

    if (v <= v0):

        print(0)

    else:

        sol = {}
        ele = {}
        max_d = 0
        x = 1

        while v/x > v0:

            d = round(calc(v/x, v0) * x, 10)

            if (d in sol):
                sol[d] += 1
            else:
                sol[d] = 1
                ele[d] = x

            max_d = max(max_d, d)

            x += 1

        print (0 if sol[max_d] > 1 else ele[max_d])

    v, v0 = map(int, input().split())

