#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int l, h, r;
	int city[10010] = {0};
	int city_end = 0, city_begin = 0;

	while (scanf ("%d %d %d", &l, &h, &r) != EOF)
	{
		city_begin = min (l, city_begin);
		city_end = max(r, city_end);

		for (int i = l; i < r; i++)
			city[i] = max(h, city[i]);
	}

	h = 0;
	for (int i = city_begin; i <= city_end;)
	{
		while (i <= city_end and h == city[i]) i++;

		printf ("%d %d", i, city[i]);
		h = city[i++];

		if (i <= city_end) putchar (' ');
	}

	putchar ('\n');

	return 0;
}

