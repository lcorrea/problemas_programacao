#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int f, r;

    while (scanf ("%d", &f), f)
    {
        scanf ("%d", &r);

        int F[f], R[r];

        double d[f*r];

        for (int i = 0; i < f; i++)
            scanf ("%d", F + i);
        
        for (int i = 0; i < r; i++)
            scanf ("%d", R + i);

        for (int i = 0, x = 0; i < f; i++)
            for (int j = 0; j < r; j++, x++)
                d[x] = R[j]/(double)F[i];

        sort (d, d + f*r);

        double maxcnt = d[1]/d[0];

        for (int i = 1, len = f*r; i < len; i++)
                maxcnt = max(maxcnt, d[i]/d[i-1]);

        printf ("%0.02lf\n", maxcnt);
    }

    return 0;
}

