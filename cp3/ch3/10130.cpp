#include <bits/stdc++.h>

using namespace std;

int n, price[1001], weight[1001], mem[31][1001];

int knapsack (int w, int i)
{
  int pega, npega;

  if (mem[w][i] != -1)
    return mem[w][i];

  if (w <= 0 or i >= n)
    return mem[w][i] = 0;

  npega = knapsack (w, i + 1);

  if (w >= weight[i])
  {
    pega = price[i] + knapsack (w - weight[i], i + 1);

    return mem[w][i] = max (pega, npega);
  }

  return mem[w][i] = npega;
}

int main (void)
{
  int t, g, ans;

  scanf ("%d", &t);

  while (t--)
  {
    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
      scanf ("%d %d", price + i, weight + i);

    scanf ("%d", &g);

    ans = 0;

    while (g--)
    {
      int w;
      
      memset (mem, -1, sizeof (mem));

      scanf ("%d", &w);

      ans += knapsack (w, 0);
    }

    printf ("%d\n", ans);
  }

  return 0;
}

