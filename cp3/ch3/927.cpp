#include <bits/stdc++.h>

using namespace std;


int main (void)
{
    int t, i, d, k, tmp;
    int coef[30];
    int seq[1000000];
    int n;
    unsigned long long ans = 0;

    for (i = 0, tmp = 1; i < 1000000; i += tmp, tmp++)
        for (k = i; k < 1000000 and k < i + tmp; k++)
            seq[k] = tmp;

    scanf ("%d", &t);

    for (int j = 0; j < t; j++)
    {
        scanf ("%d", &i);

        for (int c = 0; c <= i; c++)
            scanf ("%d", coef + c);

        scanf ("%d", &d);
        scanf ("%d", &k);

        n = seq[(k - 1)/d];
        ans = 0;

        for (int c = 0; c <= i; c++)
            if (coef[c])
                ans += coef[c] * pow (n, c);

        printf ("%llu\n", ans);
    }

    return 0;
}

