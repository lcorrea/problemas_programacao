#include <bits/stdc++.h>

using namespace std;

vector < vector <int> > graph;
vector <int> dfs_num, dfs_low, dfs_parent, articulation_point;
int root_children, dfs_counter, dfs_root, articulation_point_counter;

void detect_articulation_point(int u)
{
  dfs_low[u] = dfs_num[u] = ++dfs_counter;

  for (int i = 0; i < (int) graph[u].size(); i++)
  {
    int v = graph[u][i];

    if (!dfs_num[v])
    {
      dfs_parent[v] = u;

      if (u == dfs_root)
        root_children++;

      detect_articulation_point (v);

      if (dfs_low[v] >= dfs_num[u])
        articulation_point[u] = 1;

      dfs_low[u] = min (dfs_low[u], dfs_low[v]);
    }
    else if (v != dfs_parent[u])
      dfs_low[u] = min (dfs_low[u], dfs_num[v]);
  }
}

int main (void)
{
  int n;

  while (scanf ("%d%*c", &n), n)
  {
    int u;

    graph.clear();
    graph.resize(n+1);

    while (scanf ("%d%*c", &u), u)
    {
      char *ptr, str[1000], c;
      int i = 0;

      while ((c = getchar()) != '\n' and c != EOF)
        str[i++] = c;
      str[i] = '\0';

      ptr = strtok (str, " ");

      while (ptr != NULL)
      {
        int v;

        sscanf (ptr, "%d", &v);
        graph[u].push_back(v);
        graph[v].push_back (u);

        ptr = strtok (NULL, " ");
      }
    }

    dfs_low.assign (n+1, 0);
    dfs_num.assign (n+1, 0);
    dfs_parent.assign (n+1, 0);
    articulation_point.assign(n+1, 0);
    dfs_counter = 0;
    articulation_point_counter = 0;

    for (int i = 1; i <= n; i++)
    {
      if (!dfs_num[i])
      {
        dfs_root = i; root_children = 0;
        detect_articulation_point(i);
        articulation_point[dfs_root] = (root_children > 1);
      }

      articulation_point_counter += articulation_point[i];
    }

    printf ("%d\n", articulation_point_counter);
  }

  return 0;
}

