t = int(input())

for case in range(t):
    
    n = int(input())

    grade = []

    for i in range(n):
        grade.append (int(input()))

    max_senior = grade[0]
    min_junior = grade[1]
    ans = max_senior - min_junior

    for i in range (2, n):

        if (grade[i - 1] > max_senior):

            max_senior = grade[i - 1]
            min_junior = grade[i]

        else:

            min_junior = min (min_junior, grade[i])

        ans = max (ans, max_senior - min_junior)

    print (ans)

