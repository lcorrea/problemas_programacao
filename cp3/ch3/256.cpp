#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    vector <int> numbers;
    vector <string> numbers_str;
    int n;

    for (int i = 0; i <= 32767; i++)
    {
        int num = i*i;
        char num_str[30];

        numbers.push_back (num);

        sprintf (num_str, "%012d", num);
        numbers_str.push_back(num_str);
    }

    while (scanf ("%d", &n) != EOF)
    {
        int limit = pow(10, n) - 1;

        for (int i = 0; numbers[i] <= limit; i++)
        {
            string s1, s2;
            int n1, n2;

            s1 = numbers_str[i].substr(12 - n, n/2);
            s2 = numbers_str[i].substr(12 - n + n/2);

            sscanf (s1.c_str(), "%d", &n1);
            sscanf (s2.c_str(), "%d", &n2);

            if (numbers[i] == pow(n1+n2, 2))
                printf ("%s\n", numbers_str[i].substr(12 - n).c_str());
        }
    }

    return 0;
}

