#include <bits/stdc++.h>

using namespace std;

typedef pair < pair <int, int>, string > Maker;

bool cmp (Maker a, Maker b)
{
    if (a.first == b.first)
        return true;
    return a < b;
}

int main (void)
{
    int t;
    Maker companies[10000];

    scanf ("%d", &t);

    while (t--)
    {
        int d, q, p;
        int lo = 1000000, hi = 0;
        char name[25];

        scanf ("%d", &d);

        for (int i = 0; i < d; i++)
        {
            scanf ("%s %d %d", name, &companies[i].first.first,
                    &companies[i].first.second);

            companies[i].second = name;
            lo = min (lo, companies[i].first.first);
            hi = max (hi, companies[i].first.second);
        }

        sort (companies, companies + d, cmp);

        scanf ("%d", &q);

        while (q--)
        {
            scanf ("%d", &p);

            if (p < lo or p > hi)
                puts ("UNDETERMINED");
            else
            {
                int cnt = 0;
                string ans;

                for (int i = 0; i < d and p >= companies[i].first.first; i++)
                {
                    int plo = companies[i].first.first;
                    int phi = companies[i].first.second;

                    if (p >= plo and p <= phi)
                        cnt++, ans = companies[i].second;

                    if (cnt > 1)
                        break;
                }

                puts ( (cnt > 1 or !cnt) ? "UNDETERMINED" : ans.c_str());
            }
        }

        if (t >= 1)
            putchar ('\n');
    }

    return 0;
}

