#include <bits/stdc++.h>

using namespace std;

char warfield[101][101];
bool visited[101][101];
const int fwd_x[4] = {1, 1, -1, -1};
const int fwd_y[4] = {1, -1, 1, -1};
int cnt_even;
int cnt_odd;
int n, m, r, c;

void dfs (int x, int y)
{
    int cnt = 0;
    int k[101][101] = {0};

    visited[y][x] = 1;

    for (int i = 0; i < 4; i++)
    {
        int new_x1 = x + n*fwd_x[i], new_x2 = x + m*fwd_x[i];
        int new_y1 = y + m*fwd_y[i], new_y2 = y + n*fwd_y[i];

        if (new_x1 >= 0 and new_x1 < c)
            if (new_y1 >= 0 and new_y1 < r)
                if (!k[new_y1][new_x1] and warfield[new_y1][new_x1] != '*')
                {
                    k[new_y1][new_x1] = 1;
                    cnt++;

                    if (!visited[new_y1][new_x1])
                        dfs(new_x1, new_y1);
                }

        if (new_x2 >= 0 and new_x2 < c)
            if (new_y2 >= 0 and new_y2 < r)
                if (!k[new_y2][new_x2] and warfield[new_y2][new_x2] != '*')
                {
                    k[new_y2][new_x2] = 1;
                    cnt++;

                    if (!visited[new_y2][new_x2])
                        dfs(new_x2, new_y2);
                }
    }
    
    if (cnt % 2) cnt_odd++;
    else cnt_even++;
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    for (int i = 1, w; i <= t; i++)
    {
        scanf ("%d%d%d%d", &c, &r, &m, &n); //for this code c is collum and r is row
        scanf ("%d", &w);

        for (int x, y, j = 0; j < w; j++)
        {
            scanf ("%d%d", &x, &y);
            warfield[y][x] = '*';
        }

        dfs(0, 0);

        printf ("Case %d: %d %d\n", i, cnt_even, cnt_odd);

        memset(warfield, 0, sizeof(warfield));
        memset(visited, 0, sizeof(visited));
        cnt_even = cnt_odd = 0;
    }

    return 0;
}

