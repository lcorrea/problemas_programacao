#include <bits/stdc++.h>

using namespace std;

char picture[51][51];
int w, h;

void dfs (int i, int j, int *cnt);
void dfsx (int i, int j);

void dfs (int i, int j, int *cnt)
{
  const int dx[4] = {-1, 0, 1, 0};
  const int dy[4] = {0, -1, 0, 1};

  if (picture[i][j] == 'X')
  {
    *cnt = *cnt + 1;
    dfsx (i, j);
  }

  picture[i][j] = '.'; //visited

  for (int k = 0, x, y; k < 4; k++)
  {
    x = j + dx[k];
    y = i + dy[k];

    if (x >= 0 and x < w)
      if (y >= 0 and y < h)
        if (picture[y][x] != '.')
          dfs (y, x, cnt);
  }
}

void dfsx (int i, int j)
{
  const int dx[4] = {-1, 0, 1, 0};
  const int dy[4] = {0, -1, 0, 1};

  picture[i][j] = '*';

  for (int k = 0, x, y; k < 4; k++)
  {
    x = j + dx[k];
    y = i + dy[k];

    if (x >= 0 and x < w)
      if (y >= 0 and y < h)
        if (picture[y][x] == 'X')
          dfsx (y, x);
  }
}

int main (void)
{
  int throw_cnt = 1;

  while (scanf ("%d%d%*c", &w, &h), w and h)
  {
    vector <int> dice;

    for (int i = 0; i < h; i++)
      scanf ("%s", picture[i]);

   for (int i = 0; i < h; i++)
      for (int j = 0; j < w; j++)
        if (picture[i][j] == '*')
        {
          int cnt = 0;

          dfs(i, j, &cnt);

          if (cnt)
            dice.push_back (cnt);
        }

    sort (dice.begin(), dice.end());

    printf ("Throw %d\n", throw_cnt++);
    if (dice.size())
      printf ("%d", dice[0]);
    for (int i = 1; i < (int) dice.size(); i++)
      printf (" %d", dice[i]);
    puts("\n");
  }

  return 0;
}

