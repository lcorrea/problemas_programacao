#include <bits/stdc++.h>

using namespace std;

int new_dir (char o, int dir)
{
    if (o == 'E') dir++;
    else dir--;

    if (dir < 0) dir = 3;
    if (dir > 3) dir = 0;

    return dir;
}

int main (void)
{
    int n, m, s;
    char arena[101][101], instr[50001];
    const int fwd_x[4] = {0, -1, 0, 1};
    const int fwd_y[4] = {-1, 0, 1, 0};

    while (scanf ("%d %d %d%*c", &n, &m, &s), n and m and s)
    {
        int stick = 0;
        int dir;
        int x, y;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                arena[i][j] = getchar();

                if (isalpha (arena[i][j]))
                {
                    char o = arena[i][j];

                    if ('N' == o) dir = 0;
                    else if ('O' == o) dir = 1;
                    else if ('S' == o) dir = 2;
                    else dir = 3;

                    arena[i][j] = '.';
                    x = j;
                    y = i;
                }
            }
            getchar ();
        }

        scanf ("%s%*c", instr);

        for (int i = 0; instr[i] != '\0'; i++)
        {
            if ('F' == instr[i])
            {
                int new_x = x + fwd_x[dir];
                int new_y = y + fwd_y[dir];

                if (new_x >= 0 and new_x < m)
                    if (new_y >= 0 and new_y < n)
                        if (arena[new_y][new_x] != '#')
                            x = new_x, y = new_y;

                if (arena[y][x] == '*')
                    stick++, arena[y][x] = '.';
            }
            else
                dir = new_dir (instr[i], dir);
        }

        printf ("%d\n", stick);
    }

    return 0;
}

