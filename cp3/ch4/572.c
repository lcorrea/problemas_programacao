#include <stdio.h>

int n, m;
char land[101][101];

void search (int i, int j)
{
	int y[8] = {-1,1,0,0,-1,-1,1,1};
	int x[8] = {0,0,1,-1,-1,1,-1,1};
	int ii;

	land[i][j] = '*';
	
	for (ii = 0; ii < 8; ii++)
	{
		int cordy = i + y[ii];
		int cordx = j + x[ii];
		if (cordy >= 0 && cordy < n && cordx >= 0 && cordx < m)
		{
			if (land[cordy][cordx] == '@')
				search (cordy, cordx);
		}
	}
}

int main (void)
{
	char c;
	int i, j;
	while (scanf ("%d %d%*c",&n, &m), n && m)
	{
		for (i = 0; i < n; i++)
		{
			for (j = 0; j < m; j++)
				land[i][j] = getchar();
			getchar();
		}

		int deposits = 0;

		for (i = 0; i < n; i++)
		{
			for (j = 0; j < m; j++)
			{
				if (land[i][j] == '@')
				{
					deposits++;
					search (i, j);
				}
			}
		}
		
		printf ("%d\n", deposits);
	}
	return 0;
}

