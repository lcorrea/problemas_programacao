#include <bits/stdc++.h>

using namespace std;

int n;
char grid[101][101];

bool dfs (int i, int j)
{
  const int di[4] = {0, 0, 1, -1};
  const int dj[4] = {1, -1, 0, 0};
  bool alive = grid[i][j] == 'x';

  grid[i][j] = '.';

  for (int cell = 0; cell < 4; cell++)
  {
    int newi = i + di[cell];
    int newj = j + dj[cell];

    if (newi < n and newi >= 0)
      if (newj < n and newj >= 0)
        if (grid[newi][newj] != '.')
          return alive | dfs (newi, newj);
  }

  return alive;
}

int main (void)
{
  int Case;

  scanf ("%d", &Case);

  for (int c = 1; c <= Case; c++)
  {
    int ans = 0;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
      scanf ("%s", grid[i]);

    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        if (grid[i][j] != '.')
          ans += dfs (i, j);

    printf ("Case %d: %d\n", c, ans);
  }

  return 0;
}

