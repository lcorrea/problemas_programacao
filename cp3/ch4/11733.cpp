#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;
typedef vector < int > vi;
typedef vector < ii > vii;
typedef vector < vii > AdjList;
typedef priority_queue < ii, vii, greater < ii > > minPQ;

vi visited;
AdjList country;
minPQ pq;

void process (int u)
{
	visited[u] = 1;

	for (int i = 0, sz = country[u].size(); i < sz; i++)
		if (!visited[country[u][i].first])
			pq.push(make_pair(country[u][i].second, country[u][i].first));
}

int main (void)
{
	int tests;

	scanf ("%d", &tests);

	for (int i = 1, n, m, p; i <= tests; i++)
	{
		scanf ("%d %d %d", &n, &m, &p);

		visited.assign (n+1, 0);
		country.clear();
		country.resize(n+1);

		for (int k = 0, a, b, c; k < m; k++)
		{
			scanf ("%d %d %d", &a, &b, &c);

			country[a].push_back (make_pair(b, c));
			country[b].push_back (make_pair(a, c));
		}

		int roadCost = 0;
		int airports = 0;

		for (int k = 1; k <= n; k++)
		{
			if (visited[k]) continue;
			
			airports++;

			process (k);
			while (!pq.empty())
			{
				ii city = pq.top(); 

				pq.pop();

				if (!visited[city.second])
				{
					if (city.first >= p)
						airports++;
					else
						roadCost += city.first;

					process (city.second);
				}
			}
		}

		printf ("Case #%d: %d %d\n", i, roadCost + p*airports, airports);
	}

	return 0;
}

