#include <bits/stdc++.h>

using namespace std;

void dfs2 (int u, vector < vector <int> > &graph, vector <int> &dfs_num, vector <int> &order)
{
  dfs_num[u] = 1;
  for (int j = 0; j < (int)graph[u].size(); j++)
  {
    int v = graph[u][j];

    if (!dfs_num[v])
      dfs2(v, graph, dfs_num, order);
  }

  order.push_back (u + 1);
}

int main (void)
{
  int n, m;

  while (scanf ("%d %d", &n, &m), n)
  {
    vector <int> indegree(n, 0);
    vector < vector <int> > tasks_graph(n);
    vector <int> order;

    for (int i = 0, u, v; i < m; i++)
    {
      scanf ("%d %d", &u, &v);

      indegree[v-1]++;
      tasks_graph[u-1].push_back(v-1);
    }

    vector <int> dfs_num(n, 0);
    for (int i = 0; i < n; i++)
      if (!dfs_num[i])
        dfs2(i, tasks_graph, dfs_num, order);

    /*
    //topological sort - kahn's algorithm
    queue <int> q;
    for (int i = 0; i < n; i++)
      if (!indegree[i])
        q.push(i);

    while (!q.empty())
    {
      int u = q.front(); q.pop();
      order.push_back (u+1);

      for (int i = 0; i < tasks_graph[u].size(); i++)
      {
        int v = tasks_graph[u][i];

        if (--indegree[v] == 0)
          q.push(v);
      }
    }*/

    printf ("%d", order.back()); order.pop_back();
    while (order.size())
    {
      printf (" %d", order.back());
      order.pop_back();
    }
    putchar ('\n');

  }

  return 0;
}

