#include <bits/stdc++.h>

using namespace std;

char world[30][30], land;
int n, m;

int dfs (int x, int y)
{
  const int dx[4] = {0, 0, 1, -1};
  const int dy[4] = {1, -1, 0, 0};
  int regions = 1;

  world[y][x] = 0; //conquest region

  for (int i = 0; i < 4; i++)
  {
    int newx = x + dx[i]; newx = newx < 0 ? m - 1 : newx >= m ? 0 : newx;
    int newy = y + dy[i]; //newy = newy < 0 ? n - 1 : newy >= n ? 0 : newy;

      if (newy >= 0 and newy < n and world[newy][newx] == land)
        regions += dfs (newx, newy);
  }

  return regions;
}

int main (void)
{
  while (EOF != scanf ("%d%d", &n, &m))
  {
    int x, y, max_regions = 0;

    for (int i = 0; i < n; i++)
      scanf ("%s", world[i]);

    scanf ("%d%d", &y, &x);
    land = world[y][x];

    //fill world with my region
    dfs (x, y);

    //search other regions
    for (int i = 0; i < n; i++)
      for (int j = 0; j < m; j++)
        if (world[i][j] == land)
          max_regions = max (max_regions, dfs (j, i));

    printf ("%d\n", max_regions);
  }

  return 0;
}

