#include <bits/stdc++.h>

using namespace std;

int new_dir(char c, int dir)
{
    if (c == 'L') dir++;
    else dir--;

    if (dir < 0) dir = 3;
    if (dir > 3) dir = 0;

    return dir;
}

int main (void)
{
    int n, m;
    int x, y, dir;
    int grid[51][51] = {0};
    char instr[55], o;
    const int fwd_x[4] = {0, -1, 0, 1};
    const int fwd_y[4] = {1, 0, -1, 0};

    scanf ("%d %d%*c", &n, &m);

    while (EOF != scanf ("%d %d %c%*c", &x, &y, &o))
    {
        bool lost = false;
        scanf ("%s%*c", instr);

        if (o == 'N') dir = 0;
        else if (o == 'W') dir = 1;
        else if (o == 'S') dir = 2;
        else dir = 3;
        
        for (int i = 0; instr[i] != '\0' and !lost; i++)
        {
            if (instr[i] == 'F')
            {
                int new_x = fwd_x[dir] + x;
                int new_y = fwd_y[dir] + y;

                if (new_x < 0 or new_x > n or new_y < 0 or new_y > m)
                {
                    if (!grid[y][x])
                        lost = grid[y][x] = 1;
                }
                else
                {
                    x = new_x;
                    y = new_y;
                }
            }
            else
                dir = new_dir(instr[i], dir);
        }

        if (dir == 0) o = 'N';
        else if (dir == 1) o = 'W';
        else if (dir == 2) o = 'S';
        else o = 'E';

        printf ("%d %d %c%s", x, y, o, lost ? " LOST\n" : "\n");
    }

    return 0;
}

