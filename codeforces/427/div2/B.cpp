#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int k, len;
    char str[100010];

    scanf ("%d", &k);
    scanf ("%s", str);

    len = strlen(str);

    sort (str, str+len);
    
    int sum = 0, diff = 0, cnt = 0;
    for (int i = 0; str[i]; i++)
        sum += str[i] - '0';

    if (sum < k)
    {
        diff = k - sum;

        for (int i = 0; diff > 0 and str[i]; i++)
        {
            int v = 9 - (str[i] - '0');
            if (v <= diff)
            {
                diff -= v;
                cnt++;
            }
            else
            {
                cnt++;
                break;
            }
        }
    }

    printf ("%d\n", cnt);

    return 0;
}

