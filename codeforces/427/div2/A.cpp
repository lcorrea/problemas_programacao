#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int s, v1, t1, v2, t2;

    scanf ("%d %d %d %d %d", &s, &v1, &v2, &t1, &t2);

    double ta = t1 + s*v1 + t1;
    double tb = t2 + s*v2 + t2;

    if (ta == tb)
        puts ("Friendship");
    else
        puts (ta < tb ? "First" : "Second");

    return 0;
}

