#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int a, b, c, d;

	scanf ("%d %d %d %d", &a, &b, &c, &d);

	int m = max(3*a/10, a-((a/250)*c));
	int v = max(3*b/10, b-((b/250)*d));

	if (m > v)
		puts ("Misha");
	else if (m < v)
		puts ("Vasya");
	else
		puts ("Tie");
	
	return 0;
}

