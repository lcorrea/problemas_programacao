#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int v[101] = {0};
	char s[101][101];
	int n, m;

	scanf ("%d %d%*c", &n, &m);

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			s[i][j] = getchar();
			v[j] = max((int) s[i][j], v[j]);
		}
		getchar();
	}

	int cnt = 0;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (v[j] == s[i][j])
			{
				cnt++;
				break;
			}
		}
	}

	printf ("%d\n", cnt);

	return 0;
}

