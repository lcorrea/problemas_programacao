#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, h, p;
	long long w = 0;

	cin >> n >> h;

	for (int i = 0; i < n; i++)
	{
		scanf ("%d", &p);

		w += 1 + (p > h);
	}

	cout << w << endl;

	return 0;
}

