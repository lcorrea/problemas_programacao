#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	long long int n, h, k;
	long long int a, p = 0, sec = 0;

	cin >> n >> h >> k;

	for (int i = 0; i < n; i++)
	{
		cin >> a;

		if (p + a <= h)
			p += a;
		else 
		{
			int inc = ceil((p + a - h)/(double)k);
			sec += inc;
			p -= k*inc;
			if (p<0) p = 0;
			p+=a;
		}
	}

	sec += ceil (p/(double)k);

	cout << sec << endl;

	return 0;
}

