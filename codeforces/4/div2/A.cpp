#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d", &n);

	puts ((n%2==0 and n!=2) ? ("YES") : ("NO"));
	
	return 0;
}
