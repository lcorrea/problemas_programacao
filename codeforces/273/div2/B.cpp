#include <bits/stdc++.h>

using namespace std;

long long int cmb (long long int x)
{
	if (x < 2) return 0;
	return  x*(x-1)/2LL;
}

int main (void)
{
	long long int m, n;
	long long int y;

	cin >> m >> n;

	y = cmb (1LL + m - n);
	
	long long int p = m/n;
	long long int x = n*cmb(p) + (m%n)*p;
	
	cout << x << ' ' << y << endl;

	return 0;
}

