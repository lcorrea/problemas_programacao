#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char stones[100], inst[100];
	char *s = stones, *i = inst;

	scanf ("%s %s", stones, inst);

	while (*s and *i)
	{
		if (*s == *i)
			s++;
		i++;
	}

	printf ("%d\n", s - stones + 1);

	return 0;
}

