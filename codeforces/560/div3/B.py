#!/usr/bin/env python3

def main():
    ans, n = 0, int(input())
    arr = list(sorted(map(int, input().split())))
    day = 1
    for i in arr:
        if (i >= day):
            day += 1

    print(day)

if __name__ == "__main__":
    main()

