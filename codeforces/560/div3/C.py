#!/usr/bin/env python3

def main():
    n = int(input())
    good = ''

    for i in input():
        if not len(good) & 1 or good[-1] != i:
            good = good + i
    
    if len(good) & 1:
        good = good[:-1]

    print(n - len(good), good, sep='\n')

if __name__ == "__main__":
    main()

