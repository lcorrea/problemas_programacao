#!/usr/bin/env python3

def div(guess, n, primes):
    r = 1

    for p in primes:
        
        if (guess < p):
            break
        
        count = 0   
        
        while (guess % p == 0):
            count += 1
            guess = guess // p
        
        r = r * (count + 1)

    return r

def main():
    import math
    
    t = int(input())
    
    aux_primes = [True] * (10**6+1)
    aux_primes[0] = aux_primes[1] = False
    for i in range(2, int(math.sqrt(10**6+1))):
        if aux_primes[i]:
            for j in range(i*2, 10**6+1, i):
                aux_primes[j] = False

    primes = [i for i in range(10**6+1) if aux_primes[i]]
    
    while t:
        t -= 1
        n = int(input())
        arr = list(map(int, input().split()))
        guess = min(arr)*max(arr)
        ok = not any(guess % i for i in arr)

        if ok and n+2 == div(guess, n, primes):
            print(guess)
        else:
            print(-1)

if __name__ == "__main__":
    main()

