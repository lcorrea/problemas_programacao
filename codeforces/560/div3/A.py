#!/usr/bin/env python3

def main():
    n, x, y = map(int, input().split())
    s = input()[::-1]
    
    ans = s[:x].count('1') - (s[y] == '1') + (s[y]=='0')
    
    print(ans)

if __name__ == "__main__":
    main()

