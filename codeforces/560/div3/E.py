#!/usr/bin/env python3

def f(l, r, a, b):
    ans = 0
    for i in range(l, r+1):
        ans += arr[i]*b[i]
    return ans


def main():
    from itertools import accumulate
    n = input()
    a = sorted(map(int, input().split()))
    b = sorted(map(int, input().split()))

    ans, x = 0, 0
    for i,j in zip(a, b):
        ans += i*j + x
        x = ans

    print(ans % 998244353)

if __name__ == "__main__":
    main()

