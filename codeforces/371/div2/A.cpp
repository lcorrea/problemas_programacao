#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long int r1, r2, l1, l2, k;

    cin >> l1 >> r1 >> l2 >> r2 >> k;

    if (l2 > r1 or r2 < l1)
        cout << 0 << endl;
    else
    {
        long long int upper = min (r1, r2);
        long long int lower = max (l1, l2);
        long long int ans = upper - lower + 1ll;

        if (k >= lower and k <= upper)
            ans -= 1ll;

        cout << ans << endl;
    }

    return 0;
}

