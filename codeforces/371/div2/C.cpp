#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    map <string, int> pattern;
    map <string, int> multset;

    scanf ("%d%*c", &t);

    char c, s[20];

    for (int i = 0; i < t; i++)
    {
        scanf ("%c %s%*c", &c, s);
        string p;

        for (int j = 0, sz = 20 - strlen(s); j < sz; j++)
            p += '0';
        for (int j = 0, sz = strlen (s); j < sz; j++)
            p += '0' + ((s[j] - '0') % 2);

        switch (c)
        {
            case '-':
                multset[s] -= 1;
                pattern[p] -= 1;
                break;
            case '+':
                multset[s] += 1;
                pattern[p] += 1;
                break;
            case '?':
                printf ("%d\n", pattern[p]);
                break;
        }

    }

    return 0;
}

