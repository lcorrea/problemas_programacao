#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int cnt[4], d;
    set <int> dragons;

    for (int i = 0; i < 4; i++)
        cin >> cnt[i];
    
    cin >> d;

    for (int i = 0; i < 4; i++)
        for (int j = 1; cnt[i]*j <= d; j++)
            dragons.insert(cnt[i]*j);

    cout << dragons.size() << endl;

    return 0;
}

