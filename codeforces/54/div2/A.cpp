#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string s;
	int state = -1;

	cin >> s;

	for (int i = 0; i < s.length(); i++)
	{
		switch (s[i])
		{
			case ('h'):
				if (state == -1)
					state = 0;
				break;
			case('e'):
				if (state == 0)
					state = 1;
				break;
			case ('l'):
				if (state == 1 or state == 2)
					state++;
				break;
			case ('o'):
				if (state == 3)
					state = 4;
				break;
		}
	}

	if (state == 4)
		puts ("YES");
	else
		puts ("NO");

	return 0;
}

