#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	map <int, int> numbers;
	int n;

	cin >> n;

	for (int i = 0, j; i < n; i++)
		cin >> j, numbers[j]++;

	for (map<int,int>::const_iterator x = numbers.begin(); x != numbers.end(); x++)
	{
		if (x->second > (n+1)/2)
		{
			cout << "NO" << endl;
			return 0;
		}
	}

	cout << "YES" << endl;

	return 0;
}

