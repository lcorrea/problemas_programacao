#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    pair <int, int> seg[100001];
    int mini = 1000000003, maxj = 0;

    scanf ("%d",&n);

    for (int i = 0; i < n; i++)
    {
        scanf ("%d %d", &seg[i].first, &seg[i].second);

        mini = min(mini, seg[i].first);
        maxj = max(maxj, seg[i].second);
    }

    int cnt = 0;

    for (int i = 0; i < n and !cnt; i++)
        if (seg[i].first <= mini and
                seg[i].second >= maxj)
            cnt = i + 1;

    printf ("%d\n", cnt ? cnt : -1);

    return 0;
}

