#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string word, upper, lower;
    int up = 0;
    int lo = 0;

    cin >> word;

    for (int i = 0; word[i] != '\0'; i++)
    {
        if (word[i] == toupper(word[i]))
            up++;
        else
            lo++;
        upper += toupper (word[i]);
        lower += tolower (word[i]);
    }

    if (lo >= up)
        cout << lower << endl;
    else
        cout << upper << endl;

    return 0;
}

