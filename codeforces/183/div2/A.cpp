#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, ans = 0;

    scanf("%d", &n);

    int a = 1, b = 1, c;

    while (a <= n) {

        while (b <= a) {

            c = sqrt(a*a + b*b);
            if (c <= n) {
                ans += (c*c == (a*a + b*b));
            }
            b++;
        }
        b = 1;
        a++;
    }

    printf("%d\n", ans);
    
    return 0;
}

