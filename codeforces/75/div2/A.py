#!/usr/bin/env python3

def main():
    n, m = map(int, input().split())
    tot = (1+n)*n // 2
    res = m % tot
    i = 1

    while res >= i:
        res -= i
        i += 1

    print(res)

if __name__ == "__main__":
    main()

