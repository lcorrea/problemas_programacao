#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int letters[500] = {0};
	int cnt = 0;
	char c;
	
	while ((c = getchar()) != '}')
	{
		if (isalpha(c) and !letters[c])
			cnt += 1, letters[c] = 1;
	}

	cout << cnt << endl;

	return 0;
}

