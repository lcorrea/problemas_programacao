#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    scanf ("%d %d", &n, &k);

    int sum = 0;

    for (int i = 0, x; i < n; i++)
        scanf ("%d", &x), sum += x;

    int n1 = 0;
    while (round((sum + n1*k)/(double)(n+n1)) < k)
        n1++;

    printf ("%d\n", n1);

    return 0;
}

