#include <bits/stdc++.h>

using namespace std;

int n, f;
long long int k[100010], l[100010];
long long int tb[100010][2];

long long calc (int i, int j)
{
    if (i < 0)
        return 0;

    if (j == 0)
        return tb[i][0];//min(k[i], l[i]) + calc(i-1, j);

    if (i == j)
        return tb[i][1];//calc (i - 1, j - 1) + min(k[i]*2ll, l[i]);

    return max (calc (i-1, j) + min(k[i], l[i]),
            calc(i-1, j-1) + min(k[i] << 1, l[i]));
}

int main (void)
{
    int n, f;

    cin >> n >> f;

    cin >> k[0] >> l[0];

    tb[0][0] = min(k[0], l[0]);
    tb[0][1] = min(k[0] << 1, l[0]);

    for (int i = 1; i < n; i++)
    {
        cin >> k[i] >> l[i];

        tb[i][0] = tb[i-1][0] + min(k[i], l[i]);
        tb[i][1] = tb[i-1][1] + min(k[i] << 1, l[i]);
    }

    cout << calc(n-1, f) << endl;

    return 0;
}

