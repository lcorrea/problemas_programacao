#!/usr/bin/env python3

def main():
    l, r = map(int, input().split())

    print("YES")
    for i in range(l, r+1, 2):
        print(i, i+1)

if __name__ == "__main__":
    main()

