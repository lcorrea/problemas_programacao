def main():
    l, r = map(int, raw_input().split())
    ans = ("%d %d" % (i, i+1) for i in xrange(l, r+1, 2))
    print "YES\n" + "\n".join(ans)

if __name__ == "__main__":
    main()
