#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, q;
  int shop[100000];

  scanf ("%d", &n);

  for (int i = 0; i < n; i++)
    scanf ("%d", shop + i);

  sort (shop, shop + n);

  scanf ("%d", &q);
  while (q--)
  {
    int m;

    scanf ("%d", &m);

    if (m < shop[0]) { puts ("0"); continue; } 
    if (m >= shop[n-1]){ printf ("%d\n", n); continue; }

    int *h = upper_bound (shop, shop + n, m);

    int qnt = h - shop;

    printf ("%d\n", qnt);
  }
  
  return 0;
}

