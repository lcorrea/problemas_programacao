#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  map <int, int> mset;
  int n;

  mset[0] = 1;

  scanf ("%d%*c", &n);

  for (int i = 0; i < n; i++)
  {
    int x;
    char op;

    scanf ("%c %d%*c", &op, &x);

    if (op == '+')
      mset[x]++;

    if (op == '-')
    {
      if (x and mset[x] <= 1)
        mset.erase(x);
      else
        mset[x]--;
    }

    if (op == '?')
    {
      map <int, int>::reverse_iterator it;
      int m_t = 0;

      for (it = mset.rbegin(); it != mset.rend(); it++)
        m_t = max (m_t, it->first ^ x);

      printf ("%d\n", m_t);
    }

  }
  
  return 0;
}

