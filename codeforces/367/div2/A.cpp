#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int a, b;
  int x, y, v;
  int n;

  scanf ("%d%d", &a, &b);
  scanf ("%d", &n);
  scanf ("%d %d %d", &x, &y, &v);

  double min_t = hypot (a - x, b - y) / (double) v;

  for (int i = 1; i < n; i++)
  {
    scanf ("%d %d %d", &x, &y, &v);

    min_t = min (min_t, hypot (a - x, b - y) / (double) v);
  }

  printf ("%lf\n", min_t);

  return 0;
}

