#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	unsigned long long int a, b, c, cnt = 0;

	cin >> a >> b;

	c = a;

	while (a < b)
	{
		a *= c;
		cnt++;
	}

	if (a==b) cout << "YES\n" << cnt << endl;
	else cout << "NO" << endl;

	return 0;
}

