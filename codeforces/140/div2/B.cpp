#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int v[100001], p[100001];
  int n, m;
  long long int petya = 0, vasya = 0;

  cin >> n;
  for (int i = 1, a; i <= n; i++)
  {
    cin >> a;

    if (!v[a])
      v[a] = i;

    p[a] = n - i + 1;
  }
  
  cin >> m;
  for (int i = 1, b; i <= m; i++)
  {
    cin >> b;

    petya += p[b];
    vasya += v[b];
  }

  cout << vasya << " " << petya << endl;

  return 0;
}

