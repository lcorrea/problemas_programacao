#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    vector <vector<int>> strmap(27);

    int n, k;
    char str[400001];

    scanf("%d%d", &n, &k);
    scanf("%s", str);

    for (int i = 0 ; i < n; i++) 
        strmap[str[i]-'a'].push_back(i);

    for (char i = 'a'; k and i <= 'z'; i++) {
        int cnt = strmap[i-'a'].size();
        if (cnt) {
            for (int x = 0; x < min(cnt, k); x++) {
                str[strmap[i-'a'][x]] = 0;
            }
            k -= min(cnt, k);
        }
    }

    for (int i = 0; i < n; i++)
        if (str[i])
            putchar(str[i]);
    putchar('\n');

    return 0;
}

