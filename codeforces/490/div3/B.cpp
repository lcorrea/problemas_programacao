#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    char str[101];

    scanf("%d", &n);
    scanf("%s", str);

    for (int i = 1; i < n; i++) {
        if ( (n % (i+1)) == 0) {
            reverse(str, str+(i+1));
        }
    }

    puts(str);

    return 0;
}

