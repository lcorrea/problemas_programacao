#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout, stderr

    n, k = map(int, input().split())
    arr = map(int, input().split())
    f = map(int, input().split())
    h = map(lambda pair: (pair[0], int(pair[1])), enumerate(input().split(), 1))

    freq = {}

    for ai in arr:
        if ai in freq:
            freq[ai] += 1
        else:
            freq[ai] = 1

    h = sorted(h, key=lambda x: (x[1] / x[0], x[0]), reverse=True)

    print(h)
    ans = 0

    for i in f:
        for j in h:
            if freq[i] >= j[0]:
                ans += j[1]
                freq[i] -= j[0]
                print(j[1])
                break

    print(ans)

if __name__ == "__main__":
    main()

