#!/usr/bin/env python3

def main():
    
    n, m = map(int, input().split())
    arr = [int(ai) for ai in input().split()]
    freq = [[] for i in range(m)]
    aux = []
    count, c = 0, n // m

    for i, ai in enumerate(arr):
        freq[ai % m].append(i)

    for _ in range(2):

        for i in range(m):
            if len(freq[i]) > c:
                aux += freq[i][c:]
                freq[i] = freq[i][:c]
                continue
           
            while len(freq[i]) < c and len(aux):
                idx = aux.pop(-1)
                remainder = arr[idx] % m
                moves = i - remainder if i >= remainder else m - remainder + i
                count += moves
                arr[idx] += moves
                freq[i].append(idx)
    
    print("%d\n%s" % (count, ' '.join(map(str, arr))))
                
if __name__ == "__main__":
    main()

