#include <bits/stdc++.h>

using namespace std;

int persist = 0;
vector < vector <int> > graph(5001);
vector <int> r(5001, 0);

int dfs(int x) {
    int cnt = 1;
    
    r[x] = persist;

    for (auto i : graph[x])
        if (r[i] < persist)
            cnt += dfs(i);
    
    return cnt;
}

int main (void)
{
    int n, m, s, mincnt = 0;

    scanf("%d %d %d", &n, &m, &s);

    for (int i = 0; i < m; i++) {
        int a, b;

        scanf("%d %d", &a, &b);

        graph[a].push_back(b);
    }
    
    persist = 1000000000;
    int visited = dfs(s);
    persist = 1;

    while (visited < n) {
        int maxcnt = 0, choice = 0;

        for (int i = 1; i <= n; i++) {
            if (r[i] < persist) {
                int cnt = dfs(i);

                if (cnt > maxcnt) {
                    choice = i;
                    maxcnt = cnt;
                }
            }

            persist++;
        }
        
        int old = persist;
        
        persist = 1000000000;
        visited += dfs(choice);
        persist = old;
        
        mincnt++;
    }

    printf("%d\n", mincnt);

    return 0;
}

