#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;
    int v[100];

    scanf("%d%d", &n, &k);

    for (int i = 0; i < n; i++)
        scanf("%d", v + i);

    int ans = 0, i = 0, j = n-1;

    while (i <= j) {
        if (v[i] <= k)
            ans++, i++;
        else if (v[j] <= k)
            ans++, j--;
        else
            break;
    }

    printf("%d\n", ans);

    return 0;
}

