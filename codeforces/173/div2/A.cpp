#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int x = 0, n;
	string statement;

	cin >> n;

	while (n--)
	{
		int incr = 1;
		cin >> statement;

		if (statement[0] == 'X')
			if (statement[1]=='-')
				incr = -1;

		if (statement[0] == '-')
			incr = -1;

		x += incr;
	}

	cout << x << endl;

	return 0;
}

