#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int64_t n, k, cnt[40] = {0};
    char ci;

    cin >> n >> k;
    cin.ignore();

    for (int i = 0; i < n; i++) {
        cin >> ci;
        cnt[ci-'A']--;
        cerr << " - " << ci << endl;
    }

    sort(cnt, cnt + 40);

    unsigned long long ans = 0;
    int64_t *cnt2 = cnt;
    while (k > 0) {
        cerr << k << " = n " << " cnt " << -(*cnt2) << endl;
        int64_t x = min(-(*cnt2), k);
        
        ans += x*x;

        k -= x;
        cnt2++;
    }

    cout << ans << endl;

    return 0;
}

