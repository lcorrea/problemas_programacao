#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	char board[110][110];

	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
		scanf ("%s", &board[i][0]);

	int dir[4][2] = { {-1, 0}, {1, 0}, {0, -1}, {0, 1}};

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			int cnt = 0;
			for (int k = 0; k < 4; k++)
				if (j+dir[k][0] < n and j+dir[k][0] >= 0 and i+dir[k][1] < n and i+dir[k] >= 0)
					if(board[i+dir[k][1]][j+dir[k][0]] == 'o')
						cnt++;
			if (cnt%2)
			{
				puts("NO");
				return 0;
			}
		}
	}

	puts ("YES");

	return 0;
}

