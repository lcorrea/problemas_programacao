#!/usr/bin/env python3

def main():
    a, b, n = map(int, input().split())

    for d in (a*10 + i for i in range(10)):
        if not (d % b):
            print(str(d) + "0"*(n-1))
            return
    
    print(-1)

if __name__ == "__main__":
    main()

