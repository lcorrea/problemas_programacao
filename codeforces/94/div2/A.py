n = int(input())

arr = [int(ai) for ai in input().split()]

summ = sum(arr) & 1

print(sum(ai & 1 == summ for ai in arr))
