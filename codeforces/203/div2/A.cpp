#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;

	scanf ("%d %d", &n, &m);

	int v[n];
	int tl1=0, tl2=200, mv = 200;

	for (int i = 0; i < n; i++)
	{
		scanf ("%d", v + i);
		tl1 = max (tl1, v[i]);
		mv = min (mv, v[i]);
	}

	tl1 = mv == tl1 ? mv*2 : tl1;
	mv *= 2;

	for (int i = 0; i < m; i++)
	{
		int c;
		scanf ("%d", &c);
		tl2 = min (tl2, c);
	}

	if (tl1 < tl2) 
	{
		tl1 = min (tl1, tl2 - 1);
		printf ("%d\n", mv <= tl1 ? tl1 : mv < tl2 ? mv : -1);
	}
	else puts ("-1");

	return 0;
}


