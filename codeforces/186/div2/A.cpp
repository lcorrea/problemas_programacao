#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string total;

	cin >> total;

	int len = total.size();
	if (total[0] == '-')
	{
		if (len >= 3)
			total = total.substr(0, len - 2) + ((char) min (total[len-1], total[len-2]));
		else
			total = "0";

		if (total == "-0")
			total = "0";
	}

	cout << total << endl;

	return 0;
}

