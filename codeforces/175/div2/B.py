#!/usr/bin/env python3

def main():
    n, s, t = map(int, input().split())
    p = [0] + list(map(int, input().split()))
    ans, visited = 0, [0]*(n+1)
    
    while not visited[s] and s != t:
        visited[s] = 1
        s = p[s]
        ans += 1
    
    print(ans if s == t else -1)

if __name__ == "__main__":
    main()

