#!/usr/bin/env python3

def main():
    
    n = int(input())
    ans = 0
    for i,j in zip(sorted(map(int, input().split())), range(1,n+1)):
        ans += abs(j-i)

    print(ans)


if __name__ == "__main__":
    main()

