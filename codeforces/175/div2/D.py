#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout, stderr
    from itertools import permutations
    
    cnt = 0
    n = int(input())

    for ai in permutations(range(1, n+1)):
        for bi in permutations(range(1, n+1)):
            s = (((aa + bb - 2 ) % n) + 1 for aa, bb in zip(ai, bi))
            cnt += len(set(s)) == n 

    print(cnt)
    # --- #

if __name__ == "__main__":
    main()

