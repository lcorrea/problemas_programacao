#!/usr/bin/env python3

def main():
    n, k = map(int, input().split())
    arr = [*range(k+1, 0, -1), *range(k+2, n+1)]
    
    print(*arr)

if __name__ == "__main__":
    main()

