#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int k;
	bool possible = true;
	map <char, int> times;

	scanf ("%d%*c", &k);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			char ch = getchar();

			if (isdigit(ch))
			{
				if (times[ch] < k*2) times[ch]++;
				else possible = false;
			}
		}

		getchar();
	}

	puts (possible ? "YES" : "NO");

	return 0;
}

