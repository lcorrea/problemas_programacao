#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int h[200];

	scanf ("%d", &n);

	for (int i = 1; i <= n; i++)
		scanf ("%d", &h[i]);

	int d = h[3] - h[1];

	for (int i = 4; i <= n; i++) d = max (d, h[i] - h[i-1]);

	int dmin = d;
	int r = 3;
	while (r < n)
	{
		int a1 = 1, a2 = 2;
		d = 0;
		while (a2 <= n)
		{
			if (a2 == r) a2++;
			if (a1 == r) a1++;
			d = max(d, h[a2] - h[a1]);
			a1++; a2++;
		}

		dmin = min (d, dmin);
		r++;
	}

	printf ("%d\n", dmin);

	return 0;
}

