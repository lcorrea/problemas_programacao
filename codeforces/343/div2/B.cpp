#include <stdio.h>
#include <algorithm>

using namespace std;

int list[368][2];// = {0};
int main (void)
{
	int n, f, e;
	char c;

	scanf ("%d%*c", &n);

	for (int i = 0; i < n; i++)
	{
		scanf ("%c %d %d%*c", &c, &f, &e);
		c = c == 'M';
		while (f <= e)
			list[f++][c] += 1;
	}

	int m = 0;
	for (int i = 1; i <= 366; i++)
		m = max(m, 2*min(list[i][0], list[i][1]));
	printf ("%d\n", m);

	return 0;
}

