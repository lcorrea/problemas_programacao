#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	priority_queue < pair <double, int> > cake;
	
	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
	{
		double r, h;
		scanf ("%lf %lf", &r, &h);
		cake.push(make_pair(r*r*h, i));
	}

	double v = 0.0;
	while (!cake.empty())
	{
		int top = cake.top().second;
		double vc = cake.top().first; cake.pop();

		while (!cake.empty() and top > cake.top().second)
			vc += cake.top().first, top = cake.top().second;
		
		vc *= M_PI;

		v = max (vc, v);
	}

	printf ("%lf\n", v);


	return 0;
}

