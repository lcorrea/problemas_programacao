#include <bits/stdc++.h>

using namespace std;

int cmb (int t)
{
	if (t == 2) return 1;
	if (t < 2) return 0;
	return (t*(t-1))/2;
}


int main (void)
{
	char cake[200][200];
	int n;
	int col[200] = {0}, row[200] = {0};

	scanf ("%d%*c", &n);
	
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			char ch = getchar();

			if (ch == 'C')
				row[i]++, col[j]++;
		}
		getchar();
	}

	int cnt = 0;
	for (int i = 0; i < n; i++)
		cnt += cmb(row[i]) + cmb(col[i]);
	printf ("%d\n", cnt);
	return 0;
}

