#include <bits/stdc++.h>

#define MODULUS 1000000007

using namespace std;

long long md (long long a)
{
	long long r = a%MODULUS;

	return r < 0 ? MODULUS + r : r;
}

int main (void)
{
	long long x, y, n;

	cin >> x >> y >> n;

	long long f[6] = {md(x-y), md(x), md(y), md(y - x), md(-x), md(-y)};

	cout << f[n%6] << endl;

	return 0;
}

