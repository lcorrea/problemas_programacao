#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, m;

  scanf ("%d%d", &n, &m);

  deque <int> line(n);
  vector <int> want(n);

  for (int i = 0; i < n; i++)
    scanf ("%d", &want[i]), line[i] = i;

  while (line.size() > 1)
  {
    int child = line.front(); line.pop_front();

    want[child] -= m;

    if (want[child] > 0)
      line.push_back (child);
  }

  printf ("%d\n", line.front() + 1);

  return 0;
}

