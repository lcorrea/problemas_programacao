#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, a = 0, b = 0;
	pair <int, int> d[101];

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> d[i].first >> d[i].second;

		a += d[i].first;
		b += d[i].second;
	}

	if (a%2==0 and b%2==0) cout << "0";
	else if (a%2 != b%2) cout << "-1";
	else
	{
		bool ok = false;
		for (int i = 0; i < n and !ok; i++)
			ok = d[i].first%2 != d[i].second%2;

		if (ok) cout << "1";
		else cout << "-1";
	}

	cout << endl;

	return 0;
}

