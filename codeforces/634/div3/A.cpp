#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;
    
    cin >> t;

    while (t--) {
        int n;

        cin >> n;

        if (n == 1)
            cout << 0 << endl;
        else 
            cout << (n >> 1) - (!(n & 1)) << endl;
    }

    return 0;
}

