#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	vector <int> v;

	scanf ("%d", &n);

	int i = 30;

	while (i >= 0)
	{
		if (n & (1 << i))
			v.push_back (i+1);

		i--;
	}

	cout << v[0];
	for (int i = 1; i < v.size(); i++)
		cout << ' ' << v[i];
	cout << endl;

	return 0;
}

