#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    scanf ("%d %d", &n, &k);

    int bc = 0, ac = 0;
    int v[n+2];

    for (int i = 1; i <= n; i++)
        scanf ("%d", v + i);

    for (int i = k-1; i >= 0 and v[i] == v[k]; i--)
        bc++;

    for (int i = k+1; i <= n and !ac; i++)
        ac += v[i] != v[k];

    if (ac)
        puts ("-1");
    else
        printf ("%d\n", k - bc - 1);

    return 0;
}

