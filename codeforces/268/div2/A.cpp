#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int levels[200] = {0};
	int pass = 0;
	int n, x, y, p;

	cin >> n;

	cin >> x;
	while (x--)
	{
		cin >> p;
		if (!levels[p])
			pass +=1, levels[p] = 1;
	}

	cin >> y;
	while (y--)
	{
		cin >> p;
		if (!levels[p])
			pass += 1, levels[p] = 1;
	}


	if (n - pass)
		cout << "Oh, my keyboard!" << endl;
	else
		cout << "I become the guy." << endl;

	return 0;
}

