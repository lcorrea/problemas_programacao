#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m, d, impossible = 0;

    scanf ("%d %d %d", &n, &m, &d);
    
    int v[n*m];
    
    for (int i = 0, l = n*m, a; i < l; i++) {
        scanf("%d", &a);
        v[i] = a;
    }

    sort(v, v + n*m);
    
    for (int a = 0, b = 1, l = n*m; b < l; ++b, ++a) {
        int distance = v[b] - v[a];
        impossible |= !!(distance % d);
    }

    if (impossible) {
        puts("-1");
        return 0;
    }
    
    int choose = (n*m) >> 1;
    int moves = 0;
    
    for (auto a : v) {
        int qnt = abs(a - v[choose])/d;
        moves += qnt;
    }

    printf("%d\n", moves);

    return 0;
}

