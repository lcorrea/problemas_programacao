#include  <bits/stdc++.h>

using namespace  std;

int main (void)
{
	int x, m = 0;;

	cin >> x;

	while (x)
	{
		if (x>=5)
			x -= 5;
		else if (x>=4)
			x -= 4;
		else if (x >= 3)
			x -= 3;
		else if (x >= 2)
			x -= 2;
		else
			x--;
		m++;
	}

	cout << m << endl;

	return 0;
}
