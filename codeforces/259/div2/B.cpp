#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int v[100005];
	int n, i, cnt = 0;
	bool asc = true;

	scanf ("%d", &n);
	scanf ("%d", v);

	for (i = 1; i < n; i++)
	{
		scanf ("%d", v + i);

		if (v[i] < v[i-1])
		{
			asc = false;
			break;
		}
	}

	if (!asc)
	{
		asc = v[i] <= v[0];
		cnt = asc;

		for (i+=1;asc and i < n; i++)
		{
			scanf ("%d", &v[i]);
			asc = v[i] <= v[0] and v[i] >= v[i-1];
			cnt++;
		}

		for (; i < n; i++) scanf ("%d", &v[i]);
	}

	printf ("%d\n", asc ? cnt : -1);

	return 0;
}

