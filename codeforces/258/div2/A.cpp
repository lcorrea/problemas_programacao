#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, i;
	int total;

	cin >> n >> m;
	total = n*m;

	for(i=0; total; i++)
	{
		total -= n + m - 1;
		--n;
		--m;
	}

	puts ((i%2) ? ("Akshat") : ("Malvika"));

	return 0;
}

