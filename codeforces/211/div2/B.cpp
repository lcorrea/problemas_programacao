#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;

    scanf ("%d%d", &n, &k);

    int v[n];

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i);

    int j = 0;
    unsigned int mins = 0, sum = 0;

    for (int i = 0; i < k; i++)
        mins += v[i];

    sum = mins;
    for (int i = 1; i <= n - k; i++)
    {
        sum += v[i+k-1] - v[i-1];

        if (sum < mins)
            mins = sum, j = i;
    }

    printf ("%d\n", j+1);

    return 0;
}

