#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char n[20];

	scanf ("%s", n);

	for (int i = strlen (n) - 1, cnt = 0; i >= 0; --i, cnt = 0)
	{
		if (n[i] >= '5') printf ("-O|"), n[i] -= 5;
		else	printf ("O-|");

		while (n[i]>='1') putchar ('O'), --n[i], ++cnt;
		putchar ('-');
		while (cnt < 4) putchar ('O'), cnt++;
		putchar ('\n');
	}

	return 0;
}

