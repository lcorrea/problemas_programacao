#include <bits/stdc++.h>

#define MAXVAL 1000000001

using namespace std;

typedef pair <int, int> ii;
typedef vector < ii > Vii;
typedef vector < Vii > AdjList;

int n, m, k;
int ans = MAXVAL;
bitset <100010> b;
AdjList graph(100010);

int main (void)
{
    b.reset();

    scanf ("%d %d %d", &n, &m, &k);

    for (int i = 0, u, v, l; i < m; i++)
    {
        scanf ("%d %d %d", &u, &v, &l);

        graph[u-1].push_back(make_pair(v-1, l));
        graph[v-1].push_back(make_pair(u-1, l));
    }

    if (k)
    {
        int bakery[k];

        for (int i = 0, u; i < k; i++)
            scanf ("%d", bakery + i), b.set(bakery[i]-1);

        for (int i=0; i < k; i++)
        {
            int u = bakery[i] - 1;

            for (int j = 0; j < (int) graph[u].size(); j++)
            {
                int d1 = graph[u][j].second;
                int v = graph[u][j].first;

                if (b[v] == 0)
                    ans = min (ans, d1);
            }
        }
    }

    cout << (ans == MAXVAL ? -1 : ans) << endl;

    return 0;
}

