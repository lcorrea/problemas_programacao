#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;
    char c;

    scanf ("%d %d%*c", &n, &m);

    bool bw = true;

    for (int i = 0; i < n; i ++)
        for (int j = 0; j < m; j++)
        {
            scanf ("%c%*c", &c);

            if (c == 'C' or c == 'M' or c == 'Y')
                bw = false;
        }

    puts (bw ? "#Black&White" : "#Color");

    return 0;
}

