#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n,ball;
	set <int> balls;
	set <int>::iterator it;

	scanf ("%d",&n);

	for (int i = 0; i < n; i++)
		scanf ("%d", &ball), balls.insert(ball);

	for (it = balls.begin(); it != balls.end(); it++)
	{
		int a = (*it)+1, b = *it+2;

		if (balls.find(a)!=balls.end() and balls.find(b) != balls.end())
		{
			puts ("YES");
			return 0;
		}
	}

	puts ("NO");

	return 0;
}

