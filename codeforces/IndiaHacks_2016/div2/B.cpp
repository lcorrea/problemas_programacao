#include <bits/stdc++.h>

using namespace std;

map <char, vector <string> > m;

int d (int n, char ai)
{
	int s = 0;
	if (n==1)
		return m[ai].size();
	
	for (int i = 0, sz = m[ai].size(); i < sz; i++)
		s += d (n - 1, m[ai][i][0]);

	return s;
}

int main (void)
{

	int n, p;

	scanf ("%d %d%*c", &n, &p);

	for (int i = 0; i < p; i++)
	{
		char bi[3], ai[3];

		scanf ("%s %s%*c", bi, ai);
		m[*ai].push_back(bi);
		
	}

	printf ("%d\n", d(n-1, 'a'));
	return 0;
}

