#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int lin[20] = {0}, col[20] = {0};
	int r, c, tl = 0, tc = 0;

	scanf ("%d %d%*c", &r, &c);

	for (int i = 0; i < r; i++)
	{
		for (int j = 0; j < c; j++)
			if (getchar() == 'S')
				tl += (lin[i] ? (0):(1)), tc += (col[j] ? (0):(1)), lin[i] = col[j] = 1;
		getchar();
	}

	printf ("%d\n", r*c - tl*tc);
			
	return 0;
}

