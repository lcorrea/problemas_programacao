#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char c;
	string soma;

	while ((c = getchar()) != '\n')
	{
		if (isdigit(c))
			soma += c;
	}

	sort (soma.begin(), soma.end());

	cout << soma[0];
	for (int i = 1, len = soma.length(); i < len; i++)
		cout << '+' << soma[i];
	cout << endl;

	return 0;
}

