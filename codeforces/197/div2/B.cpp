#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	long long int n, t;

	cin >> n >> t;

	long long int f0 = 1;
	long long int d = 0;
	for (int i = 1; i <= t; i++)
	{
		int c;
		scanf ("%d", &c);

		if (c < f0) d += n - f0 + c;
		else d += c - f0;

		f0 = c;
	}

	cout << d << endl;

	return 0;
}

