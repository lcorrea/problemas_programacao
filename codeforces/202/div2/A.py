#!/usr/bin/env python3
def main():
    
    from sys import stdin, stdout, stderr

    ans = True
    v25, v50 = 0, 0
    
    next(stdin)
    
    for bill in map(int, stdin.readline().split()):
        if bill == 25:
            v25 += 1
        elif bill == 50 and v25:
            v25 -= 1
            v50 += 1
        elif bill == 100 and v25 and v50:
            v25 -= 1
            v50 -= 1
        elif bill == 100 and v25 > 2:
            v25 -= 3
        else:
            ans = False

    stdout.write("YES\n" if ans else "NO\n")

if __name__ == "__main__":
    main()

