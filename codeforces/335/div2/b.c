#include <stdio.h>
#include <string.h>

char cmd[100005];
int ks[100005];
int field[501][501];
int y, x, rx, ry;
int bx, by;

void run()
{
		int cnt = 0;
		int px = rx, py = ry;
		while (*cmd)
		{
				if (field[py][px] == -1) field[py][px] = cnt;
				cnt++;
				switch (*cmd)
				{
						case ('U'):
								if (px>1)px--;
								break;
						case ('D'):
								if (px < x) px++;
								break;
						case ('L'):
								if (py>1)py--;
								break;
						case ('R'):
								if (py < y)py++;
								break;
				}
				cmd++;
		}
}

int main (void)
{
		scanf ("%d %d %d %d", &x, &y, &rx, &ry);
		scanf ("%s", cmd);
		memset (field, -1, 501*501);
		run();
		
		int len = strlen (cmd);
		for (bx=1; bx <= x;bx++)
		{
				for (by=1; by <= y; by++)
				{
						if (field[by][bx] != -1) ks[field[by][bx]]++;
						else ks[len]++;
				}
		}

		int i;
		printf ("%d", ks[0]);
		for (i=1; i <= len; i++)
				printf (" %d", ks[i]);
		putchar ('\n');

		return 0;
}

