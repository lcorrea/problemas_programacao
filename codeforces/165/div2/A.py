#!/usr/bin/env python3

def main():
    t = int(input())
    ans = ("NO" if 360 % (180 - int(input())) else "YES" for i in range(t))
    
    print("\n".join(ans))

if __name__ == "__main__":
    main()

