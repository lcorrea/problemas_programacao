#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    vector <long long int> fib;

    fib.push_back (0);
    fib.push_back (1);

    for (int n = 2; fib.back() < 1000000000ll; n++)
        fib.push_back(fib[n-1] + fib[n-2]);

    int n;

    scanf ("%d", &n);

    if (n == 1)
        puts ("0 0 1");
    else if (n == 0)
        puts ("0 0 0");
    else
    {
        vector <long long int>::iterator it2, it3, it = lower_bound (fib.begin(), fib.end(), n);
        
        it--; it--;
        it2 = it--;
        it3 = it;
        printf ("%I64d %I64d %I64d\n", *it2, *it2, *it3);
    }

    return 0;
}

