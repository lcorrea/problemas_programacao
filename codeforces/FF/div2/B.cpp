#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int k, maxValue = 0;
	string str;
	vector <int> v(255, 0);

	cin >> str;

	cin >> k;

	for (int i = 'a'; i <= 'z'; i++)
		cin >> v[i], maxValue = max (v[i], maxValue);

	int total  = 0;
	for (int i = 1; i <= str.size(); i++)
		total += v[str[i-1]]*i;

	for (int i = 0, j = str.size() + 1; i < k; i++, j++)
		total += j*maxValue;

	cout << total << endl;

	return 0;
}

