#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, p, fail = 0;
	vector <int> bucket;

	scanf ("%d %d", &p, &n);
	
	bucket.assign(p, 0);

	for (int i = 0, e; i < n; i++)
	{
		scanf ("%d", &e);
		if (!bucket[e%p])
			bucket[e%p]++;
		else if (!fail)
			fail = i;
		else;
	}

	printf ("%d\n", fail ? fail + 1: -1);
	return 0;
}

