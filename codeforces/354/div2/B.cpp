#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	double levels[12][12];
	
	int l, s, cnt = 0;

	cin >> l >> s;

	if (s)
	{
		cnt = 1;

		for (int i = 1, k = 1; k < l; i+=2, k++)
			for (int j = 0; j <= k; j++)
				if (j == 0 or j == k)
					cnt += s / (1 << i) >= 1;
				else
					cnt += s / (1 << (i - 2)) >= 1;
	}


	cout << cnt << endl;

	return 0;
}

