#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	cin >> n;

	int arr[n+1];

	for (int i = 1, a; i <= n; i++)
	{
		cin >> a;
		arr[a] = i;
	}
	
	int dist = max (n - arr[1], arr[1] - 1);
	dist = max(dist, max (n - arr[n], arr[n] - 1));

	cout << dist << endl;

	return 0;
}

