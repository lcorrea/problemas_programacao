#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	pair <int, int> laptops[100005], last;
	bool happy = false;
	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
		scanf ("%d %d", &laptops[i].first, &laptops[i].second);

	sort (laptops, laptops+n);

	last = laptops[0];
	for (int i = 1; i < n and !happy; i++)
	{
		if (laptops[i].first > last.first and laptops[i].second < last.second)
			happy = true;
		last =  laptops[i];
	}

	puts (happy ? "Happy Alex" : "Poor Alex");

	return 0;
}

