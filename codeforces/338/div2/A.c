#include <stdio.h>
#include <string.h>

int main (void)
{
	char bulbs[110];

	memset (bulbs, 1, sizeof bulbs);

	int n, m;

	scanf ("%d %d", &n, &m);

	while (n--)
	{
		int q, i, l;

		scanf ("%d", &q);

		while (q--)
		{
			scanf ("%d", &l);
			m -= bulbs[l];
			bulbs[l] = 0;
		}
	}

	puts ( m ? "NO" : "YES" );

	return 0;
}

