#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	map <char, pair <int, int> > kb;
	char num[10];

	kb['1'] = make_pair (0, 0); kb['2'] = make_pair (0, 1); kb['3'] = make_pair (0, 2);
	kb['4'] = make_pair (1, 0); kb['5'] = make_pair (1, 1);	kb['6'] = make_pair (1, 2);
	kb['7'] = make_pair (2, 0); kb['8'] = make_pair (2, 1); kb['9'] = make_pair (2, 2);
		kb['0'] = make_pair (3, 1);

	int n;

	scanf ("%d", &n);
	scanf ("%s", num);

	pair <int, int> path[n];

	if (n == 1) {puts ("NO"); return 0;}


	for (int i = 1; i < n; i++)
	{
		int diffx = kb[num[i]].first - kb[num[i-1]].first;
		int diffy = kb[num[i]].second - kb[num[i-1]].second;

		path[i-1] = make_pair (diffx, diffy);
	}

	int cnt = 0;

	for (char i = '0'; i <= '9'; i++)
	{
		pair <int, int> key = kb[i];
		bool ok = true;

		for (int j = 0; j < n-1 and ok; j++)
		{
			pair <int, int> newkey = make_pair (key.first + path[j].first,
					key.second + path[j].second);

			if (newkey == kb['0'])
			{ key = newkey; continue; }

			if (newkey.first < 0 or newkey.first > 2) ok = false;
			if (newkey.second < 0 or newkey.second > 2) ok = false;

			key = newkey;
		}

		if (ok) cnt++;
	}

	if (cnt > 1) puts ("NO");
	else puts ("YES");

	return 0;
}

