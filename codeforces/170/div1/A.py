#!/usr/bin/env python3

def main():
    n, m = map(int, input().split())
    lang = [[] for i in range(m)]
    visited = [0] * n
    graph = [set() for i in range(n)]
    newbies = 0

    def dfs(u):
        visited[u] = 1
        for i in graph[u]:
            if not visited[i]:
                dfs(i)
    
    for i in range(n):
        l, *arr = map(int, input().split())

        newbies += l == 0

        for y in arr:
            lang[y-1].append(i)

    for l in lang:
        for u in l:
            for v in l:
                graph[u].add(v)

    components = 0
    for u in range(n):
        if not visited[u]:
            dfs(u)
            components += 1

    print (components - 1 if newbies != components else components)


if __name__ == "__main__":
    main()

