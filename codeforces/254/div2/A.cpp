#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	char pieces[2] = {'B', 'W'}, ch;
	int n, m;

	scanf ("%d %d%*c", &n, &m);

	for (int i = 0; i < n; i++)
	{
		int b = i%2;
		for (int j = 0; j < m; j++)
		{
			ch = getchar ();
			
			putchar ( ch == '.' ? pieces[b%2] : '-' );
			b++;
		}
		getchar();
		putchar ('\n');
	}

	return 0;
}

