#include <bits/stdc++.h>

using namespace std;

map<unsigned long long, set<int>> listn;
vector < vector <int> > graph;
vector <int> ts, visited;
int n;

void dfs(int i)
{
    visited[i] = 1;

    for (auto x : graph[i]) {
        if (!visited[x]){
            dfs(x);
        }
    }

    ts.push_back(i);
}

int main (void)
{
    unsigned long long int a[101];

    cin >> n;

    graph.resize(n);

    for (int i = 0; i < n; i++) {
        cin >> a[i];

        listn[a[i]].insert(i);
    }

    for (int i = 0; i < n; i++) {
        unsigned long long int sa = a[i] << 1ULL;

        for (auto x : listn[sa])
            graph[i].push_back(x);

        if (a[i] % 3ULL) continue;

        sa = a[i] / 3ULL;

        for (auto x : listn[sa])
            graph[i].push_back(x);
    }

    for (int i = 0; i < n; i++)
    {
        ts.clear();
        visited.assign(n, 0);

        dfs(i);

        if(n == (int) ts.size()) {
            reverse(ts.begin(), ts.end());
            break;
        }
    }

    for (int i = 0, len = (int) ts.size(); i < len; i++)
        cout << a[ts[i]] << (i == len-1 ? '\n' : ' ');

    return 0;
}

