#include <bits/stdc++.h>

using namespace std;

int n, m, first = 0, cycle = 0, t = 0;
vector <int> visited;
vector < vector <int> > graph;

int dfs(int i, int j) {
    visited[i] = ++t;

    for (auto x : graph[i]) {
        if (!visited[x])
            fprintf(stderr, "%d -> %d\n", i+1, x+1), dfs(x, i);
        else if (x != j and x == first)
            cycle++, fprintf(stderr, "cycle %d -> %d\n", i+1, x+1);
        else if (x != j and visited[i] > visited[x])
            cycle = -1000000, fprintf(stderr, "# %d -> %d\n", i+1, x+1);
    }

    if (graph[i].size() == 1)
        cycle = -10000000;

    return 0;
}


int main (void)
{
    int ans = 0;
    
    scanf("%d %d", &n, &m);

    visited.assign(n, 0);
    graph.resize(n);

    for (int i = 0, a , b; i < m; i++) {
        scanf("%d %d", &a, &b);
        graph[a-1].push_back(b-1);
        graph[b-1].push_back(a-1);
    }

    for (int i = 0; i < n; i++) {
        if (!visited[i]) {
            cycle = 0;
            first = i;
            dfs(i, i);
            ans += cycle == 1;
        }
    }

    printf("%d\n", ans);

    return 0;
}

