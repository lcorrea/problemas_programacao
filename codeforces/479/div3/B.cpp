#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[110];
    int n;

    scanf("%d", &n);
    scanf("%s", str);

    map<string, int> count;

    for (int i = 0; i <= n-2 ; i++) {
        string x;
        x += str[i];
        x += str[i+1];
        count[x]++;
    }

    int ans = 0;
    string anss;
    for (auto i : count){
        if (i.second > ans) 
            anss = i.first, ans = i.second;
    }

    printf("%s\n", anss.c_str());

    return 0;
}

