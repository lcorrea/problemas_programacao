#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;
    int v[200001];

    scanf("%d%d", &n, &k);

    for (int i = 0; i < n; i++)
        scanf("%d", v + i);

    sort(v, v + n);

    int l = 1;
    int r = 1000000000;
    int m;

    for (int it = 0; l <= r and it < 50; it++) {
         m = l + (r-l)/2;

        auto i = upper_bound(v, v+n, m);

        if ((i-v) > k)
            r = m;
        else if ((i-v) == k)
            break;
        else
            l = m+1;
    }

    auto i = upper_bound(v, v+n, m);

    if ((i-v) == k and (m > 0 and m <= 1000000000))
        printf("%d\n", m);
    else
        printf("-1\n");

    return 0;
}

