//tutorial solution

#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf("%d", &n);
    int v[n], ans = 0, last;
    map<int,int> h;

    for (int i = 0; i < n; i++) {
        scanf("%d", v + i);
        h[v[i]] = h[v[i]-1] + 1;
        if (ans < h[v[i]]) {
            last = v[i];
            ans = h[v[i]];
        }
    }

    vector<int> sol;
    for (int i = n - 1; i >= 0; i--) {
        if (last == v[i]) {
            sol.push_back(i+1);
            last--;
        }
    }

    reverse(sol.begin(), sol.end());

    printf("%d\n", ans);
    for (auto i : sol)
        printf("%d ", i);
    putchar('\n');

    return 0;
}

