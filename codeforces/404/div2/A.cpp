#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    map<string,int> M = {
        {"Tetrahedron", 4},
        {"Cube", 6},
        {"Octahedron", 8},
        {"Dodecahedron", 12},
        {"Icosahedron", 20}
    };

    int n, ans = 0;

    cin >> n;

    while (n--) {
        string poly;

        cin >> poly;

        ans += M[poly];
    }

    cout << ans << endl;
    return 0;
}

