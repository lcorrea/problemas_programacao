#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int a, b, c;

	cin >> a >> b >> c;

	int m = max(a + b + c, max (a + b*c, max (a*b + c, max (a*b*c, max ((a+b)*c, a*(b+c))))));

	cout << m << endl;

	return 0;
}

