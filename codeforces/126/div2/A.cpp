#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int l;
    long double v = 0;

    cin >> l;

    for (int i = 0, p; i < l; i++)
    {
        cin >> p;

        v += p/100.0;
    }

    cout << (v / (long double) l)*100.0 << endl;

	return 0;
}

