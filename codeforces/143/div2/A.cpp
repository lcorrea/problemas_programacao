#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	scanf ("%d", &n);

	int cnt = 0;
	for (int i = 0; i < n; i++)
	{
		int acc = 0, s;

		for (int p = 0; p < 3; p++)
			scanf ("%d", &s), acc += s;

		cnt += acc >= 2;
	}

	printf ("%d\n", cnt);

	return 0;
}

