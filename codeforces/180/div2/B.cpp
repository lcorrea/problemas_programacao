#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int sx, sy, ex, ey, t;

    scanf ("%d %d %d %d %d", &t, &sx, &sy, &ex, &ey);
    
    int dx = ex - sx;
    int dy = ey - sy;
    int ans = -1;

    getchar();
    for (int i = 1; i <= t; i++)
    {
        char ch = getchar();

        switch (ch)
        {
            case 'S':
                if (dy < 0)
                    dy += 1;
                break;
            case 'N':
                if (dy > 0)
                    dy -= 1;
                break;
            case 'E':
                if (dx > 0)
                    dx -= 1;
                break;
            case 'W':
                if (dx < 0)
                    dx += 1;
                break;
        }

        if (!dx and !dy and ans == -1)
            ans = i;
    }

    printf ("%d\n", ans);

    return 0;
}

