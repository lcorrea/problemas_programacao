#include <bits/stdc++.h>

using namespace std;

int tb[5][5];

long long int calc (int sol[5], int n)
{
    long long int cnt = 0;

    if (n == 5)
        return 0;

    for (int i = n; i < 4; i += 2)
            cnt = cnt + tb[sol[i]][sol[i+1]] + tb[sol[i+1]][sol[i]];

    return cnt + calc (sol, n+1);
}

int main (void)
{
    int sol[] = {0, 1, 2, 3, 4};

    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            scanf ("%d", &tb[i][j]);

    long long int maxs = 0;

    do {
        maxs = max (maxs, calc(sol, 0));
    }while (next_permutation(sol, sol+5));

    cout << maxs << endl;

    return 0;
}

