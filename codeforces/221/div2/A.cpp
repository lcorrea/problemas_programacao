#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int i = 0, p, a;
	long long int la = 0, lb = 0;
	stack < pair <int, int> > force;
	string lever;

	cin >> lever;

	while (lever[i] != '^')
	{
		if (isdigit(lever[i]))
			force.push(make_pair(lever[i]-'0', i));
		i++;
	}

	a = i;

	while (!force.empty())
	{
		la += force.top().first * (i - force.top().second);
		force.pop();
	}

	i++;

	while (i < lever.length())
	{
		if (isdigit(lever[i]))
			lb += (lever[i]-'0')*(i - a);
		i++;
	}

	puts (la == lb ? "balance" : la > lb ? "left" : "right");

	return 0;
}

