#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	set <int> v;

	cin >> n;

	for (int i = 0, x; i < n; i++)
		cin >> x, v.insert(x);

	while (v.size() > 1)
	{
		set<int>::reverse_iterator a, b;
		
		a = b = v.rbegin();
		a++;

		v.insert(*b - *a);
		v.erase(*b);
	}

	cout << n * (*v.begin()) << endl;

	return 0;
}

