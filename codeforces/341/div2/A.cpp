#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	vector <long long int> v;
	long long int soma = 0;
	long long int c;

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> c;
		soma += c;
		v.push_back (c);
	}

	sort (v.begin(), v.end());

	for (int i = 0; i < n; i++)
	{
		if (soma % 2 == 0)
			break;
		if (v[i] % 2)
			soma -= v[i];
	}

	cout << soma << endl;

	return 0;
}
