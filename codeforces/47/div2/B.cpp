#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string s;
    map <char, int> m;
    map <char, int>::const_iterator it;
    long long int cnt = 0;

    cin >> s;

    for (int i = 0; s[i] != '\0'; i++)
        m[s[i]]++;

    for (it = m.begin(); it != m.end(); it++)
        cnt += (long long int) it->second * it->second;

    cout << cnt << endl;

    return 0;
}

