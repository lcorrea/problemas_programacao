#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;

int cnt = 0;
int n;

int solve(int i, int j, string num) {
    int cnt = 0;
    
    i -= 1; j -= 1;

    if (j < i) i -= 1;

    while (j+1 <= n-1) {
        swap(num[j], num[j+1]);
        j++;
        cnt++;
    }

    while (i+1 <= n-2) {
        swap(num[i], num[i+1]);
        i++;
        cnt++;
    }

    if (num[0] == '0') {
       if (n > 3) {
           int k = 0;
           
           while(num[k] == '0') cnt++, k++;
           
           return cnt;
       }
       else
           return INF;
    }

    return cnt;
}

int main (void)
{
    string num;
    map<char, int> x;

    cin >> num;
    int last0 = -1, ans = INF;

    n = num.length();

    if (n == 1) {
        puts("-1");
        return 0;
    }

    for (int i = 0; num[i]; i++) {
        
        if (num[i] == '0' and x['0'])
            last0 = x['0'];
        
        x[num[i]] = i+1;
    }

    //25
    if (x['2'] and x['5'])
        ans = min(ans, solve(x['2'], x['5'], num));
    
    //50
    if (x['5'] and x['0'])
        ans = min(ans, solve(x['5'], x['0'], num));

    //75
    if (x['7'] and x['5'])
        ans = min(ans, solve(x['7'], x['5'], num));
    
    //100
    if (last0 != -1) {
        ans = min(ans, solve(last0, x['0'], num));
    }

    if (ans == INF) puts("-1");
    else printf("%d\n", ans);

    return 0;
}

