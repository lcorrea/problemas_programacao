#include <bits/stdc++.h>

using namespace std;

bool ok = true;

bool cmp(string a, string b) {
    const char *aa = a.c_str();
    const char *bb = b.c_str();

    if (strstr(aa, bb) != NULL)
        return false;
    else if (strstr(bb, aa) != NULL)
        return true;
    else {
        ok = false;
        return false;
    }

    return false;
}

int main (void)
{
    int n;

    cin >> n;
    string v[n];

    for (int i = 0; i < n; i++) {
        cin >> v[i];
    }

    sort (v, v+n, cmp);

    cout << (ok ? "YES" : "NO") << "\n";

    if (ok) {
        for (int i = 0; i < n; i++) {
            cout << v[i] << "\n";
        }
    }

    return 0;
}

