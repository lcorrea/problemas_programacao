#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    map<int, map<int,int>> tb;
    int n;
    bool ok = false;
    
    scanf("%d", &n);

    struct {
        pair<int,int> x;
        pair<int,int> y;
    } ans;

    for (int i = 0 ; i < n; i++) { 
        
        int k, sum = 0;

        scanf("%d", &k);
        
        int seq[k];
        
        for (int j = 0; j < k; j++) {
            scanf("%d", &seq[j]);
            sum += seq[j];
        }

        for (int j = 0; !ok and j < k; j++) {

            tb[sum-seq[j]][i] = j;

            if (!ok and tb[sum-seq[j]].size() > 1) {
                auto it = tb[sum-seq[j]].begin();
                ans.x = *it++;
                ans.y = *it;
                ok = true;
            }
        }
    }

    puts( ok ? "YES" : "NO" );

    if (ok) {
        printf("%d %d\n%d %d\n",
                ans.x.first + 1, ans.x.second + 1,
                ans.y.first + 1, ans.y.second + 1);
    }
    
    return 0;
}

