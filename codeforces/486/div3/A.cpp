#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k;
    map <int,int> team;
    scanf("%d%d", &n, &k);

    for (int i = 0, a; i < n; i++) {
        scanf("%d", &a);
        team[a] = i+1;
    }

    if ((int)team.size() >= k) {
        int cnt = 0;
        printf("YES\n");
        for (auto i : team) {
            cnt++;
            printf("%d%c", i.second, cnt == k ? '\n' : ' ');
            if (cnt == k)
                break;
        }
    } else 
        puts("NO");


    return 0;
}

