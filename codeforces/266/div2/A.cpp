#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, a, b;

	cin >> n >> m >> a >> b;

	int totalA = n*a;
	int totalB = ceil(n/(double)m)*b;
	int AB = (n/m)*b + (n - (n/m)*m)*a;

	cout << min (totalA, min (totalB, AB)) << endl;

	return 0;
}

