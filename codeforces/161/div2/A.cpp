#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int px, py, c;

	for (int i = 1; i <= 5; i++)
		for (int j = 1; j <= 5; j++)
			cin >> c, px = (c) ? (j) : (px), py = (c) ? (i) : (py);

	cout << abs(px - 3) + abs(py-3) << endl;

	return 0;
}

