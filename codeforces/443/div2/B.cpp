#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    unsigned long long int n, k;
    deque <int> pwr;

    scanf ("%llu %llu", &n, &k);

    for (int i = 0, p; i < n; i++)
        scanf ("%d", &p), pwr.push_back(p);

    if (k >= n)
        cout << n << endl;
    else
    {
        unsigned long long int win[501] = {0};
        
        while(win[pwr.front()] < k)
        {
            int a = pwr.front(); pwr.pop_front();
            int b = pwr.front(); pwr.pop_front();
            
            if (a > b)
            {
                win[a]++;
                win[b] = 0;
                pwr.push_front(a);
                pwr.push_back(b);
            }
            else
            {
                win[b]++;
                win[a] = 0;
                pwr.push_front(b);
                pwr.push_back(a);
            }
        }

        printf ("%d\n", pwr.front());
    }

    return 0;
}

