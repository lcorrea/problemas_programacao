#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, s, d, par;
    long long int last;

    cin >> n;
    cin >> last >> d; last += 1;
    for (int i = 1; i < n; i++)
    {
        cin >> s >> d;

        if (s >= last)
            last = s+1;
        else
        {
            par = ceil((last - s)/(double)d);
            last = s + d*par + 1;
        }
    }

    cout <<  last-1 << endl;

    return 0;
}

