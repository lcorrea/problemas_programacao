#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	map <string, int>  finalScoreBoard, score;
	pair <string, int> rounds[1001];
	string player;
	int n, p;

	cin >> n;

	for (int i=0; i < n; i++)
	{
		cin >> player >> p;

		rounds[i] = make_pair (player, p);
		finalScoreBoard[player] += p;
	}

	map <string, int>::const_iterator it, m;
	m = finalScoreBoard.begin();

	for (it = finalScoreBoard.begin(); it != finalScoreBoard.end(); it++)
		if (it->second > m->second)
			m = it;

	for (int i=0; i < n; i++)
	{
		player = rounds[i].first;
		score[player] += rounds[i].second;

		if (score[player] >= m->second and finalScoreBoard[player] == m->second)
			break;
	}

	cout << player << endl;

	return 0;
}

