#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	vector <int> coins(110, 0);
	int n;

	cin >> n;

	int total = 0;
	for (int i = 0; i < n; i++)
		cin >> coins[i], total += coins[i];

	sort (coins.rbegin(), coins.rend());

	int v = 0;
	int ncoins = 0;
	for (int i = 0; v <= total and i < n; i++)
	{
		int c = coins[i];

		v += c;
		total -= c;
		ncoins++;
	}

	cout << ncoins << endl;

	return 0;
}

