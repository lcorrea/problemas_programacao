#!/usr/bin/python3

n = int(input())
ticket = input()

fh = sorted(ticket[0:n])
sh = sorted(ticket[n:])
pair = zip(fh, sh)

if (fh[0] < sh[0]):
    ans = [1 for x,y in pair if x >= y]
else:
    ans = [1 for x,y in pair if x <= y]

print("YES" if len(ans) == 0 else "NO")

#ans = "YES" if len([1 for x,y in pair if x >= y] if fh[0] < sh[0] else [1 for x,y in pair if x <= y]) == 0 else "NO"
#print(ans)

