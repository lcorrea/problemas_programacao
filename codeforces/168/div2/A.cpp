#include <bits/stdc++.h>

using namespace std;

int main (void)
{   
    int grid[3][3];

    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            grid[i][j] = 1;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0, a; j < 3; j++)
        {
            scanf ("%d", &a);

            if (a % 2)
            {
                int newPosx[4] = {0, 0, 1, -1};
                int newPosy[4] = {1, -1, 0, 0};

                grid[i][j] = !grid[i][j];

                for (int k = 0; k < 4; k++)
                {
                    int x = j + newPosx[k];
                    int y = i + newPosy[k];

                    if (x >= 0 and x < 3)
                        if (y >= 0 and y < 3)
                            grid[y][x] = !grid[y][x];
                }
            }
        }
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
            printf ("%d", grid[i][j]);
        putchar ('\n');
    }

    return 0;
}

