#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    int maxS = 1;
    set <int> table;

    for (long long int i = 0, s; i < 2ll*n; i++)
    {
        scanf ("%d", &s);

        if(table.find(s) != table.end())
            table.erase(s);
        else
            table.insert(s);

        maxS = max (maxS, (int)table.size());
    }

    printf ("%d\n", maxS);

    return 0;
}

