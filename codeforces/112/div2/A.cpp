#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	pair <int, int> xy[1009];

	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
	{
		int x,y;
		scanf ("%d %d", &x, &y);
		xy[i] = make_pair(x, y);
	}

	int sc = 0;
	for (int i = 0; i < n; i++)
	{
		int a=0,b=0,c=0,d=0;
		for (int j = 0; j < n; j++)
		{
			if (xy[j].first > xy[i].first and xy[j].second == xy[i].second) a++;
			if (xy[j].first < xy[i].first and xy[j].second == xy[i].second) b++;
			if (xy[j].first == xy[i].first and xy[j].second > xy[i].second) c++;
			if (xy[j].first == xy[i].first and xy[j].second < xy[i].second) d++;
		}

		sc += a and b and c and d;
	}

	printf ("%d\n", sc);

	return 0;
}

