#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string n1, n2;

    cin >> n1 >> n2;

    for (int i = 0; n1[i] != '\0'; i++)
        cout << (n1[i] == n2[i] ? '0' : '1');
    cout << endl;

	return 0;
}

