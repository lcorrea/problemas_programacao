#!/usr/bin/env python3

def main():
    x, y = map(int, input().split())
    xx, yy = map(abs, (x, y))
    a = 0, (xx + yy) * y // yy
    c = (xx + yy) * x // xx, 0

    if (a[0] > c[0]):
        a, c = c, a
    
    print(*a, *c)

if __name__ == "__main__":
    main()

