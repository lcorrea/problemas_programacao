#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    map <int, long long> flowers;

    for (int i = 0; i < n; i++)
    {
        int b;

        scanf ("%d", &b);

        flowers[b]++;
    }

    auto minf = flowers.begin();
    auto maxf = flowers.rbegin();
    long long int ways;

    if (minf->first == maxf->first)
        ways = (maxf->second*(maxf->second-1)) >> 1;
    else
        ways = minf->second * maxf->second;

    printf ("%d %I64d\n", maxf->first - minf->first, ways);

    return 0;
}

