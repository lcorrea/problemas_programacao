#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string desc[4];
	int shorter[4], longer[4], cnt = 0;
	char greater;

	for (int i = 0; i < 4; i++)
		cin >> desc[i];

	for (int i = 0; i < 4; i++)
	{
		shorter[i] = desc[(i+1) % 4].length() - 2;
		longer[i] = desc[(i + 1) % 4].length() - 2;

		for (int j = 0; j < 4; j++)
		{
			if (i==j) continue;

			shorter[i] = min ((int) shorter[i], (int) desc[j].length() - 2);
			longer[i] = max (longer[i], (int) desc[j].length() - 2);
		}

		int len = desc[i].length() - 2;

		if (len <= shorter[i]/2.0 or len/2.0 >= longer[i])
		{
			greater = desc[i][0];
			cnt++;
		}
	}

	if (cnt == 1) cout << greater;
	else cout << 'C';

	cout << endl;

	return 0;
}

