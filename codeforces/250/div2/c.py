#!/usr/bin/env python3

def main():
    n, m = map(int, input().split())
    v, ans = list(map(int, input().split())), 0

    for i in range(m):
        ans += min(map(lambda x: v[int(x)-1], input().split()))

    print(ans)

if __name__ == "__main__":
    main()

