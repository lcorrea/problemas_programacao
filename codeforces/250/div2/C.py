#!/usr/bin/env python3

def main():
    n, m = map(int, input().split())
    v, g = list(map(int, input().split())), [0] * n
    graph, visited = [[] for i in range(n)], [0] * n
    stack = []

    for i in range(m):
        s, t = map(lambda x: int(x)-1, input().split())
        graph[s].append(t)
        graph[t].append(s)
        g[s] += 1
        g[t] += 1

    def process(u):
        if not g[u]:
            return 0
        
        ans = g[u]*v[u]
        
        for t in graph[u]:
            g[t] -= 1
            g[u] -= 1
        
        return ans

    def dfs(u):
        visited[u] = 1
        for v in graph[u]:
            if not visited[v]:
                dfs(v)
        stack.append(u)
    
    ans = 0
    for i in range(n):
        if not visited[i]:
            dfs(i)
            ans += sum(map(process, sorted(stack, key=lambda x: v[x])))
            stack.clear()

    print(ans)


if __name__ == "__main__":
    main()

