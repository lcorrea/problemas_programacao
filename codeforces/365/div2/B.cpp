#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int c[100001] = {0};
  int C[100001] = {0};
  long long int ans = 0;
  long long int sum = 0;
  long long int sumc = 0;
  int n, k;

  scanf ("%d %d", &n, &k);

  for (int i = 1; i <= n; i++)
    scanf ("%d", c + i), sum += c[i];

  for (int i = 0, id; i < k; i++)
  {
    scanf ("%d", &id);
    C[id] = 1;
    sumc += c[id];
    sum -= c[id];
  }

  for (int i = 2; i <= n; i++)
  {
    if (!C[i] and !C[i-1])
      ans += c[i]*c[i-1];
  }
  if (!C[n] and !C[1])
    ans += c[n]*c[1];

  for (int i = 1; i <= n; i++)
  {
    if (C[i])
    {
      ans += sum*c[i];
      ans += (sumc - c[i]) * c[i];
      sumc -= c[i];
    }
  }
  
  cout << ans << endl;

  return 0;
}

