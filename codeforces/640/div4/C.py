#!/usr/bin/env python3

def main():
    t = int(input())
    while(t):
        t -= 1

        n, k = map(int, input().split())
        cnt = k // n;
        
        while (cnt < (k+cnt) // n):
            cnt = (k + cnt) // n
        
        print(cnt+k)
if __name__ == "__main__":
    main()

