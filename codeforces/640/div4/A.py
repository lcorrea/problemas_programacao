#!/usr/bin/env python3

def solve(n):
    l, ans = len(n), []
    for i in range(l):
        if (n[i] != '0'):
            ans.append(n[i] + '0' * (l-i-1))
    return ans

def main():
    t = int(input())
    while (t):
        ans = solve(input())
        print(len(ans), ' '.join(ans), sep='\n')
        t -= 1

if __name__ == "__main__":
    main()

