#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, d=0, md=0;
	int jars[2000001] = {0};
	int *m = jars+1;

	cin >> n;

	for (int i=1; i<=n; i++)
	{
		cin >> jars[i];
		if (jars[i] <= *m)
		{
			m = jars + i;
		}
	}

	for (int i=1; i<=n; i++)
	{
		if (jars[i] != *m) d++;
		else md = max (md, d), d=0;
	}

	md = max (md, d);

	unsigned long long int cnt = (*m)*1ULL*n;

	int i = (m - jars) + 1;
	
	d=0;
	if (i>n) i = 1;
	while (jars[i] != *m)
	{
		i++;
	       	d++;
		if (i > n) i = 1;
	}

	cnt += max (md, d);

	cout << cnt << endl;

	return 0;
}

