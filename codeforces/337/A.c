#include <stdio.h>

int main (void)
{
	unsigned int n;

	scanf ("%u%*c", &n);

	if (n <= 4 || n % 2)
	{
		puts ("0");
		return 0;
	}

	printf ("%u\n", (n/2 - 1)/2);

	return 0;
}

