#!/usr/bin/env python3

def main():
       
    from sys import stdin, stdout, stderr

    n, k = map(int, stdin.readline().split())
    arr = [tuple((int(line.split()[0]), -int(line.split()[1]))) for line in stdin]
    
    arr.sort(reverse=True)

    k -= 1
    ans = 0

    for ai in arr:
        if(ai == arr[k]):
            ans += 1

    stdout.write(str(ans) + '\n')

if __name__ == "__main__":
    main()

