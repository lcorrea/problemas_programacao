#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int openr = 0;
    int openl = 0;

    cin >> n;

    for (int i = 0, l, r; i < n; i++)
    {
        cin >> l >> r;
        openr += r;
        openl += l;
    }

    cout << min (openr, n - openr) + min (openl, n - openl) << endl;

    return 0;
}

