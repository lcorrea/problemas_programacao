#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    bool solve = true;
    char number[10];

    cin >> n;

    do
    {
        int cnt[10] = {0};

        n++;
        sprintf (number, "%04d", n);
        
        solve = true;
        for (int i = 0; number[i] != '\0' and solve; i++)
        {
            int a = number[i] - '0';
            
            cnt[a]++;
            if (cnt[a] == 2)
                solve = false;
        }

    }while (!solve);

    printf("%d\n", n);

	return 0;
}

