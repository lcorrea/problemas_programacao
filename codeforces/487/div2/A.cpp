#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[200] = {0};

    scanf("%s", str);

    bool ok = false;

    for (int i = 1; !ok and str[i+1]; i++)
        if (str[i] != str[i-1] and str[i] != str[i+1] and str[i-1] != str[i+1]) {
        ok = isalpha(str[i]) and isalpha(str[i+1]) and isalpha(str[i-1]);
        }
    puts(ok ? "Yes" : "No");

    return 0;
}

