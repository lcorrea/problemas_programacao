#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int cc[4] = {0};

    for (int i = 0; i < 4; i++)
        scanf("%d", cc + i);

    char board[50][51] = {0};

    for (int inc = 0, letter = 'A'; inc < 48; inc += 12, letter++)
        for (int i = inc; i < min(48, (inc + 12)); i++)
           for (int j = 0; j < 50; j++)
               board[i][j] = letter;

    for (int inc = 0, letter = 'A'; inc < 48; inc += 12, letter++) {
        for (int i = inc; i < min(48, (inc + 12)); i+=2) {
           char next = (letter == 'D' ? 'A' : letter + 1) - 'A';
           for (int j = 1; j < 50 and (cc[next]-1); j += 2,--cc[next]) {
               board[i][j] = next + 'A';
           }
        }
    }

    puts ("48 50");
    for (int i = 0; i < 48; i++)
        puts (board[i]);

    return 0;
}

