#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, p;
    char str[3000] = {0};
    bool ok = false;

    scanf("%d %d", &n, &p);

    scanf("%s", str);

    for (int i = 0; !ok and i < n; i++) {
        if (str[i] == '.') {
            if (i+p < n and str[i+p] == '.') {
                str[i] = '0'; str[i+p] = '1';
                ok = true;
            } else if (i+p < n) {
                str[i] = str[i+p] == '1' ? '0' : '1';
                ok = true;
            }
        } else {
            if (i+p < n and str[i+p] == '.') {
                str[i+p] = str[i] == '1' ? '0' : '1';
                ok = true;
            } else if (i+p < n and str[i] != str[i+p])
                ok = true;
        }
    }

    for (int i = 0; ok and i < n; i++)
        if (str[i] == '.')
            str[i] = '0';

    puts(ok ? str : "No");

    return 0;
}

