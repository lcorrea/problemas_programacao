#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int m, n, ans = 0;

    scanf ("%d %d", &n, &m);

    for (int a = 0; a*a <= n; a++)
        for (int b = 0; b*b <= m; b++)
            if (a*a + b == n and b*b + a == m)
                ans++;

    printf ("%d\n", ans);


    return 0;
}

