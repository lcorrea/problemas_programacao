#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int i = 2;
	int n;

	scanf ("%d", &n);

	if (n%2) puts ("-1");
	else
	{
		do
		{
			printf ("%d %d ", i, i-1);
			i+=2;
		}while (i <= n);
		putchar ('\n');
	}

	return 0;
}

