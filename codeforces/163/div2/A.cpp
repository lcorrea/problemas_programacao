#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int cnt = 0;
    string line;

    scanf ("%d%*c", &n);

    getline (cin, line);

    if (n > 1)
        for (int i = 1; i < n; i++)
            if (line[i] == line[i-1])
                cnt++;

    printf ("%d\n", cnt);

	return 0;
}

