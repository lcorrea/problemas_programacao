#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	long long int l, r, b;

	scanf ("%I64d %I64d", &l, &r);

	l += l%2==1 ? 1 : 0;

	if (r - l < 2)
		puts ("-1");
	else
		printf ("%I64d %I64d %I64d\n", l, l+1u, l+2u);

	return 0;
}

