#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, cnt = 26;
    char alpha[256] = {0};

    cin >> n;

    getchar();

    while(n--) {
        char ch = tolower(getchar());

        cnt -= alpha[ch] == 0;

        alpha[ch] = 1;
    }

    puts(cnt ? "NO" : "YES");

    return 0;
}

