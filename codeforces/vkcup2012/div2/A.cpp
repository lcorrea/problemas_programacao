#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, k;

	scanf ("%d %d", &n, &k);

	int f[n], cnt = 0;

	for (int i = 0; i < n; i++) scanf ("%d", f + i);

	for (int i = 0; i < n; i++) cnt += f[i] > 0 and f[i] >= f[k-1];

	printf ("%d\n", cnt);

	return 0;
}


