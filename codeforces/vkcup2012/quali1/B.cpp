#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int cars = 1, lot = 0, s;
	int group[5] = {0};

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> s;
		group[s]++;
	}

	group[1] -= min (group[3], group[1]);

	int b = group[2]/2;
	group[2] = group[2]%2;
	group[1] -= group[2] ? group[1] >= 2 ? 2 : group[1] ? 1 : 0 : 0;

	int c = group[1]/4; group[1] = group[1]%4;
	int d = group[1]/3; group[1] = group[1]%3;
	int e = group[1]/2; group[1] = group[1]%2;
	
	cout << group[4] + group[3] + b + group[2] + c + d + e + group[1] << endl;

	return 0;
}

