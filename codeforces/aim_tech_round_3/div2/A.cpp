#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, b, d, ans = 0;

  scanf ("%d%d%d", &n, &b, &d);

  for (int i = 0, sum = 0, orange; i < n; i++)
  {
    scanf ("%d", &orange);

    if (orange <= b)
    {
      sum += orange;

      if (sum > d)
        ans++, sum  = 0;
    }
  }

  printf ("%d\n", ans);

  return 0;
}

