#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, c;
	int l, p, t = 0;

	scanf ("%d %d", &n, &c);

	scanf ("%d", &l);
	for (int i = 1; i < n; i++)
	{
		scanf ("%d", &p);

		t = max(l-p-c, t);
		l = p;
	}

	printf ("%d\n", t);

	return 0;
}
