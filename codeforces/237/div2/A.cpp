#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	char ch;

	scanf ("%d%*c", &n);
	set <char> d1, d2, other;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			ch = getchar ();

			if (j==i) d1.insert(ch);
			else if (j+i == n-1) d2.insert (ch);
			else other.insert(ch);
		}
		getchar();	
	}

	set <char>::const_iterator a = d1.begin(), b = d2.begin(), c = other.begin();

	if (other.size() == 1 and d1.size() == 1 and d2.size() == 1)
		puts (*a == *b and *a != *c ? ("YES") : ("NO"));
	else
		puts ("NO");

	return 0;
}

