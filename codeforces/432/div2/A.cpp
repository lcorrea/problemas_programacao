#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k, t;

    scanf ("%d %d %d", &n, &k, &t);

    if (t >= k and t <= n)
        printf ("%d\n", k);
    else if (t < k)
        printf ("%d\n", t);
    else
        printf ("%d\n", n - (t - k));

    return 0;
}

