#include <bits/stdc++.h>

using namespace std;

class Trie
{
    public:
        int prefixes;
        Trie  *zero, *um;

        Trie () : zero(NULL), um(NULL)
        {
            prefixes = 0;
        }

        int add(string x, int i)
        {
            if (x[i] == 0)
                return 0;

            prefixes++;

            if (x[i] == '0')
            {
                if(zero == NULL)
                    zero = new Trie();

                zero->add(x, i + 1);
            }
            else
            {
                if (um == NULL)
                    um = new Trie();

                um->add(x, i + 1);
            }
        }

        int count_queries()
        {
            int cnt = (zero != NULL) + (um != NULL);

            if (zero != NULL)
                if (zero->prefixes > 1)
                    cnt += zero->count_queries();

            if (um != NULL)
                if (um->prefixes > 1)
                    cnt += um->count_queries();

            return cnt;
        }
};

int main (void)
{
    int n;
    char str[21];
    Trie pre;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf ("%s", str);

        pre.add(str, 0);
    }

    printf ("words %d\n", pre.prefixes);

    if (pre.zero != NULL)
        printf ("0x %d\n", pre.zero->prefixes);
    if (pre.um != NULL)
        printf ("1x %d\n", pre.um->prefixes);

    printf ("%d\n", pre.count_queries() + 1);

    return 0;
}

