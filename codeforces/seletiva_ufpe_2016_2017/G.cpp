#include <bits/stdc++.h>

using namespace std;

/*map <string, int> id;
typedef vector <int> vi;

class Set
{

	private:
        vi p, h, len;

    public:

        Set (int n)
        {
            h.assign(n, 0);
            p.assign(n, 0);
            len.assign(n, 1);

            for (int i = 0; i < n; i++)
                p[i] = i;
        }

        int get_set (int i)
        {
            return (p[i] == i) ? i : (p[i] = get_set(p[i]));
        }

        bool is_same_set (int i, int j)
        {
            return get_set (i) == get_set (j);
        }

        void join_sets(int i, int j)
        {
            if (is_same_set (i, j) == false)
            {
                int set_i = get_set(i);
                int set_j = get_set(j);

                if (h[set_i] > h[set_j])
                {
                    p[set_j] = set_i;
                    len[set_i] += len[set_j];
                }
                else
                {
                    p[set_i] = set_j;
                    len[set_j] += len[set_i];

                    if (h[set_i] == h[set_j])
                        h[set_j]++;
                }
            }
        }

        int len_set(int i)
        {
            return len[p[i]];
        }
};


int main (void)
{
	int n, q, cnt = 0;
	char a[20], b[20];
    Set words(300000);

	scanf ("%d%*c", &n);

	for (int i = 0; i < n; i++)
	{
		scanf ("%s %s%*c", a, b);

        if (id[a] == 0)
            id[a] = ++cnt;

        if (id[b] == 0)
            id[b] = ++cnt;

        words.join_sets (id[a], id[b]);
	}

	scanf ("%d%*c", &q);

	while (q--)
	{
		scanf ("%s%*c", a);

		puts (words.len_set(id[a]) % 2 ? "N" : "S");
	}

	return 0;
}
*/

map <string, int> id;
vector < vector <int> > g(200010);
vector <int> v;

void dfs (int s, int &cnt)
{
    v[s] = true;
    cnt++;

    for (int i = 0; i < (int) g[s].size(); i++)
    {
        int word = g[s][i];

        if (!v[word])
            dfs(word, cnt);
    }
}

int main (void)
{
    int n, q, cnt = 1;
    char a[20], b[20];

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf ("%s %s", a, b);

        if (id[a] == 0)
            id[a] = cnt++;
        if (id[b] == 0)
            id[b] = cnt++;

        g[id[a]].push_back(id[b]);
        g[id[b]].push_back(id[a]);
    }

    scanf ("%d", &q);

    while (q--)
    {
        scanf ("%s", a);

        v.resize(200002, 0);

        int ans = 0;

        dfs(id[a], ans);

        puts (ans % 2 ? "N" : "S");
    }

    return 0;
}

