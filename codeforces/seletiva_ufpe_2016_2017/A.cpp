#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char words[5][5][20];
    char ascii[255], lin[10];
    vector < vector < string > > ans(4);

    ascii['V'] = 0;
    ascii['A'] = 1;
    ascii['N'] = 2;
    ascii['E'] = 3;

    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            scanf ("%s%*c", words[i][j]);

    for (int i = 0; i < 5; i++)
    {
        scanf ("%s", lin);

        for (int j = 0; j < 5; j++)
            ans[ascii[lin[j]]].push_back(words[i][j]);
    }

    for (int i = 0; i < 4; i++)
    {
        sort (ans[i].begin(), ans[i].end());

        for (int j = 0, sz = (int) ans[i].size(); j < sz; j++)
            if (j + 1 < sz)
                printf ("%s ", ans[i][j].c_str());
            else
                printf ("%s", ans[i][j].c_str());

        putchar ('\n');
    }


    return 0;
}

