#!/usr/bin/env python3

def main():
    n = int(input())
    ans = 1
    
    for i in map(int, sorted(map(int, input().split()))):
        if (ans < i):
            break
        ans += 1
    
    print(ans)

if __name__ == "__main__":
    main()

