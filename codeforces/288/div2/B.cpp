#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string n, s, m = "0";
	vector <int> even;
	bool f = false;

	cin >> n;
	for (int i = 0, sz = n.size(); i < sz; i++)
		if ((n[i]%2==0))
			even.push_back (i);

	for (int i = 0, sz = even.size(); i < sz and !f; i++)
	{
		if (n[even[i]] <= n[n.size()-1])
		{
			swap(n[even[i]], n[n.size()-1]);
			f = true;
		}
	}

	for (int i = even.size() - 1; i >= 0 and !f; i--)
	{
		if (n[even[i]] >= n[n.size()-1])
		{
			swap(n[even[i]], n[n.size()-1]);
			f = true;
		}
	}

	if (!f)
		cout << -1 << endl;
	else
		cout << n << endl;
	
	return 0;
}

