#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, groups = 1;

    cin >> n;

    char current[3], last[3];

    cin >> last;
    while (--n) {
        cin >> current;

        if (current[0] == last[0] &&
                current[1] == last[1])
            continue;

        last[0] = current[0];
        last[1] = current[1];
        groups++;
    }

    cout << groups << endl;

    return 0;
}

