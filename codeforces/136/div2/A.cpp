#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    printf ("%d", n);
    for (int i = 1; i < n; i++)
        printf (" %d", i);
    putchar ('\n');

    return 0;
}

