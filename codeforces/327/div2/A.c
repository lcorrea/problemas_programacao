/*
 * wizards' Duel
 */
#include <stdio.h>

int main (void)
{
	int l, q, p;
	scanf ("%d %d %d", &l, &q, &p);
	double s = q*l/(double)(q+p);
	printf ("%lf\n", s);
	return 0;
}

