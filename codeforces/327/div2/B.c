#include <stdio.h>

int main (void)
{
	int n, m;
	char str[200009];
	char alphabet[256];
	char alphabet2[256];

	for (n='a'; n <= 'z'; n++)
		alphabet[n] = n, alphabet2[n] = n;

	scanf ("%d %d", &n, &m);
	scanf ("%s", str);

	while (m--)
	{
		char x, y;
		scanf ("%*c%c %c", &x, &y);
		alphabet[x] = y;
	}

	char *ptr = str;

	while (*ptr)
	{
		while (alphabet[*ptr] != *ptr)
			*ptr = alphabet[*ptr];
		putchar (alphabet[*ptr++]);
	}

	putchar ('\n');
}

