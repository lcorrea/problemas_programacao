#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout, stderr

    n, m = map(int, stdin.readline().split())
    arr = [int(x) for x in stdin.readline().split()]
    queries = map(int, stdin)
    distincts = set()
    suff = []
    
    for i in reversed(arr):
        distincts.add(i)
        suff.append(len(distincts))
    
    ans = (str(suff[n-q]) for q in queries)

    stdout.write('\n'.join(ans) + '\n')

if __name__ == "__main__":
    main()

