#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  long long int a, b, c;

  scanf ("%I64d%I64d%I64d", &a, &b, &c);

  if (!c) { puts (a == b ? "YES" : "NO"); return 0; }

  long long int n = (b - a) / c;

  if (n < 0)
    puts ("NO");
  else if (c*n + a == b)
    puts ("YES");
  else
    puts ("NO");

  return 0;
}

