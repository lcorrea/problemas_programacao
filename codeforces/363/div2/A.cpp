#include <bits/stdc++.h>
#include <cstring>

using namespace std;

int main (void)
{
    int n;
    char desc[200001];
    int pos[200001];

    scanf ("%d", &n);
    scanf ("%s", desc);

    for (int i = 0; i < n; i++)
        scanf ("%d", &pos[i]);

    char * p = strstr (desc, "RL");

    if (p == NULL)
        puts ("-1");
    else
    {
        int min_time = 1000000000;

        do
        {
            min_time = min (min_time, (pos[p - desc + 1] - pos[p - desc])/2);
        }while ((p = strstr(p+1, "RL")) != NULL);

        printf ("%d\n", min_time);
    }

	return 0;
}

