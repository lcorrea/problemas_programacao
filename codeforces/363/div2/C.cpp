#include <bits/stdc++.h>

using namespace std;

int bk (int last, int i, int n, int task[102])
{
    if (i >= n)
        return 0;

    if (!task[i])
        return 1 + bk(0, i + 1, n, task);

    if (task[i] == 3 and last == 0)
        return min (bk(1, i + 1, n, task), min(1 + bk(0, i + 1, n, task), bk(2, i + 1, n, task)));

    if (task[i] == 3 and last == 2)
        return min (1 + bk(0, i + 1, n, task), bk(1, i + 1, n, task));

    if (task[i] == 3 and last == 1)
        return min (1 + bk (0, i + 1, n, task), bk(2, i + 1, n, task));
    
    if (task[i] == last)
        return 1 + bk(task[i], i+1, n, task);
    else
        return min (1 + bk (0, i + 1, n, task), bk(task[i], i + 1, n, task));
}


int main (void)
{
    int n;
    int cnt = 0;
    int task[102] = {0};

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", &task[i]);

    printf ("%d\n", bk(0, 0, n, task));

	return 0;
}

