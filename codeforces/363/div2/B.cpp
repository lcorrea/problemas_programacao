#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;
    char depot[1001][1001] = {0};
    int lin[1001] = {0};
    int col[1001] = {0};
    int cnt = 0;

    scanf ("%d %d%*c", &n, &m);

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            scanf ("%c",  &depot[i][j]);

            if (depot[i][j] == '*')
            {
                lin[i]++;
                col[j]++;
                cnt++;
            }
        }
        getchar();
    }

    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            if (lin[i] + col[j] - (depot[i][j] == '*') == cnt)
                return printf ("YES\n%d %d\n", i+1, j+1), 0;

    puts ("NO");

	return 0;
}

