#include <bits/stdc++.h>

using namespace std;

bitset<100000> bs;
vector <int> primes;

void sieve()
{
    bs.set();
    bs[0] = bs[1] = 0;

    for (int i = 2; i <= 100; i++)
    {
        if (bs[i])
        {
            for (int j = i*i; j <= 100; j += i)
                bs[j] = 0;
            primes.push_back (i);
        }
    }
}

int main (void)
{
    int n, m, i;

    cin >> n >> m;

    sieve();

    for (i = n + 1; !bs[i] and i <= m; i++);

    if (i == m)
        puts ("YES");
    else
        puts ("NO");

	return 0;
}

