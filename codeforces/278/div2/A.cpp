#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	bool luck = false;
	long long int n, first;
	char floor[50];

	cin >> n;
	first = n;

	while (!luck)
	{
		n++;
		sprintf (floor, "%I64d", n);

		for (int i = strlen(floor) - 1; !luck and i >= 0; i--)
			luck = floor[i]=='8';
	}

	cout << n - first << endl;

	return 0;
}

