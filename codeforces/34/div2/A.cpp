#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  int unit[200];

  scanf ("%d", &n);

  for (int i = 0; i < n; i++)
    scanf ("%d", &unit[i]);

  int m = 1000000, p1, p2;
  for (int i = 0; i < n; i++)
  {
    int next = (i + 1) % n;

    if (abs (unit[i] - unit[next]) < m)
      p1 = i, p2 = next, m = abs (unit[i] - unit[next]);
  }

  printf ("%d %d\n", p1 + 1, p2 + 1);

  return 0;
}

