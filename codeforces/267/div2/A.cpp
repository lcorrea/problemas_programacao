#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, cnt = 0;

  scanf ("%d", &n);

  for (int i = 0, p, q; i < n; i++)
  {
    scanf ("%d%d", &p, &q);

    cnt += (p + 2) <= q;
  }

  printf ("%d\n", cnt);

  return 0;
}

