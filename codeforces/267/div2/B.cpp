#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, k;
	int cnt = 0;
	cin >> n >> m >> k;

	int v[m+1];
	for (int i = 0; i <= m; i++)
		cin >> v[i];

	for (int i = 0; i < m; i++)
	{
		int r = v[m] ^ v[i];
		int dif = 0;

		for (int j = 0; j < n and dif <= k; j++)
			dif += (r & (1 << j)) != 0;
		cnt += dif <= k;
	}

	printf ("%d\n", cnt);

	return 0;
}

