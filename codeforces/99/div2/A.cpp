#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, k = 0;
  int week[7];

  scanf ("%d", &n);

  for (int i = 0; i < 7; i++)
    scanf ("%d", week + i), k += week[i];

  int ans = -1;
  int left = n % k ? n % k : k;

  for (int i = 0; i < 7 and ans == -1; i++)
    if (week[i] and left <= week[i])
      ans = i;
    else
      left -= week[i];

  printf ("%d\n", ans + 1);

  return 0;
}

