#!/usr/bin/env python3

def main():
    n = int(input())
    arr = list(sorted(map(int, input().split())))
    arr[0], arr[-1] = arr[-1], arr[0]

    print(*arr)

if __name__ == "__main__":
    main()

