#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, v, k, p, t=0;
	bool s, sl[100] = {0};

	scanf("%d %d", &n, &v);

	for (int i = 1; i <= n; i++)
	{
		s = false;
		scanf ("%d", &k);
		for (int j = 1; j <= k; j++)
			scanf ("%d", &p), s = v>p?(true):(s);
		
		if(s) t+= 1, sl[i] = true;
	}

	printf ("%d\n", t);
	for (int i = 1; i <= n; i++)
		if (sl[i]) printf ("%d ", i);
	putchar('\n');

	return 0;
}

