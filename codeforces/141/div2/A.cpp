#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	set <int> colors;
	int color;

	for (int i = 0; i < 4; i++)
		scanf ("%d", &color), colors.insert(color);

	printf ("%d\n", 4 - colors.size());

    return 0;
}

