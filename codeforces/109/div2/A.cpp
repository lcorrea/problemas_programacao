#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int amazing = -1;
    int max_p = 0, min_p = 10001;

    cin >> n;

    for (int i = 0, p; i < n; i++)
    {
        cin >> p;

        if (p > max_p or p < min_p)
            max_p = max (p, max_p), min_p = min (p, min_p), amazing++;
    }

    cout << amazing << endl;

    return 0;
}

