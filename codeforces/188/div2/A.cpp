#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	unsigned long long int n, k, szO, szE;

	cin >> n >> k;

	szO = ceil (n/((double)2.00));
	szE = n - szO;

	if (k <= szO)
		k = (k << 1) - 1ull;
	else 
		k = ((k - szO) << 1);

	cout << k << endl;

	return 0;
}

