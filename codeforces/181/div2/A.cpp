#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	cin >> n;

	vector <int> neg;
	vector <int> pos;
	
	for (int i = 0, j; i < n; i++)
	{
		cin >> j;

		if (j>0) pos.push_back (j);
		if (j<0) neg.push_back (j);
	}

	printf ("1 %d\n", neg.back());
	neg.pop_back();
	
	printf ("%d",neg.size()+pos.size() - neg.size()%2);

	for (int i = 0, sz = neg.size() - neg.size()%2; i < sz; i++)
		printf (" %d", neg[i]);

	for (int i = 0, sz = pos.size(); i < sz; i++)
		printf (" %d", pos[i]);

	printf ("\n%d 0", neg.size()%2 + 1);

	if (neg.size()%2) printf (" %d", neg[neg.size()-1]);

	putchar ('\n');

	return 0;
}

