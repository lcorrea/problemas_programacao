#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	pair <int, int> dragons[1009];
	int s, n;

	cin >> s >> n;

	for (int i = 0; i < n; i++)
		cin >> dragons[i].first >> dragons[i].second;

	sort (dragons, dragons + n);

	bool won = true;
	for (int i = 0; i < n and won; i++)
	{
		if (s <= dragons[i].first)
			won = false;
		s += dragons[i].second;
	}

	puts ((won) ? "YES" : "NO");

	return 0;
}

