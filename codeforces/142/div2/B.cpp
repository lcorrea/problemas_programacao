#include <bits/stdc++.h>

using namespace std;

bool tb[1000001];
set <long long> tp;

int main (void)
{
	int n;
	long long p;
	
	for (long long i = 2ll; i*i < 1000001; i++)
		if (!tb[i])
			for (long long j = i*i; j < 1000001; j += i)
				tb[j] = true;

	for (long long i = 2; i < 1000001; i++)
		if (!tb[i])
			tp.insert (i*i);

	cin >> n;

	while (n--)
	{
		cin >> p;

		if (tp.find(p) != tp.end())
			cout << "YES" << endl;
		else
			cout << "NO" << endl;
	}

	return 0;
}

