#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int c, v0, v1, a, l;

    cin >> c >> v0 >> v1 >> a >> l;

    c -= v0;

    if (c <= 0)
        puts ("1");
    else
    {
        v0 += a;

        int t = 2;
        int newpages = (v0 + l) > v1 ? v1 - l : v0 - l;

        while (c >= newpages)
        {
            c -= newpages;

            if (c == 0) break;

            t++;
            v0 += a;
            newpages = (v0 + l) > v1 ? v1 - l : v0 - l;
        }

        printf ("%d\n", t);
    }

    return 0;
}

