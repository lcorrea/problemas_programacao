#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int cnt[10] = {0};
    const int seq[3][3] = {1, 2, 4, 1, 2, 6, 1, 3, 6};

    scanf ("%d", &n);

    for(int i = 0, a; i < n; i++)
    {
        scanf ("%d", &a);
        cnt[a]++;
    }

    int ok = 0;
    int sol[n][3];

    for (int i = 0; i < 3; i++)
    {
        int *a = cnt + seq[i][0];
        int *b = cnt + seq[i][1];
        int *c = cnt + seq[i][2];
        
        if (*a and *b and *c)
        {
            int rep = min(*a, min(*b, *c));

            for (int j = 0; j < rep; j++, ok++)
            {
                sol[ok][0] = seq[i][0];
                sol[ok][1] = seq[i][1];
                sol[ok][2] = seq[i][2];
            }

            *a -= rep;
            *b -= rep;
            *c -= rep;
        }
    }

    if (ok)
        for (int i = 0; i < ok; i++)
            printf ("%d %d %d\n", sol[i][0], sol[i][1], sol[i][2]);
    else
        puts ("-1");

    return 0;
}

