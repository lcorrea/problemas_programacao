#include <stdio.h>

int main (void)
{
	int x, cnt=0, l;
	char op[20];


	scanf ("%d %*s %s", &x, op);

	if (op[0] == 'm')
	{
		cnt = 12;
		if (x > 29)
			cnt--;
		if (x > 30)
			cnt -= 4;
	}
	else
	{
		cnt = 52;
		if (x == 5 || x == 6) cnt++;
	}

	printf ("%d\n", cnt);

	return 0;
}

