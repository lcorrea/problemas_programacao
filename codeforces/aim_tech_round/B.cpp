#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	long long int total = 0;
	int map <int, int> present;
	int size, a;

	cin >> size;

	for (int i = 1; i <= size; i++)
	{
		cin >> a;

		if (present[a] == 0) present[a] = 1;
		else a = present[a];
		total += a;
	}

	cout << total << endl;

	return 0;
}

