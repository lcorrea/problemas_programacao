#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int v1, v2, l, d;

	scanf ("%d %d %d %d", &d, &l, &v1, &v2);

	printf ("%lf\n", (l - d) / ((double) (v1 + v2)));

	return 0;
}

