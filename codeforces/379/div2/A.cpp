#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char p;
    int n, a = 0, d = 0;
    
    cin >> n; getchar();

    while (n-- > 0) {
        p = getchar();
        a += p == 'A';
        d += p == 'D';
    }

    if (a == d)
        puts("Friendship");
    else if (a > d)
        puts("Anton");
    else
        puts("Danik");

    return 0;
}

