#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, g;
	int home[40] = {0};
	int guest[200] = {0};

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> home[i] >> g;

		guest[g]++;
	}

	int cnt = 0;

	for (int i = 0; i < n; i++)
		cnt += guest[home[i]];

	cout << cnt << endl;

	return 0;
}

