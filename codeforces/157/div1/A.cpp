#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int z = 0;
    bool ok = true;
    string number;

    cin >> number;
    
    for (int i = 0; number[i]; i++)
        z += number[i] == '0';

    if (z == 0)
        cout << number.substr(1) << endl;
    else if ( z == number.length() )
        cout << "0" << endl;
    else
    {
        int i = 0;

        do
        {
            if (number[i] != '0')
                break;
            ok = false;
            z--;
            i++;
        }while (i < number.length());

        if (z)
            while (i < number.length()
                    and number[i] != '0') cout << number[i++];

        cout << number.substr(i+ok) << endl;

    }

    return 0;
}

