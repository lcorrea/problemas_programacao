#!/usr/bin/env python3

def main():
    from sys import stdin
    
    n = int(stdin.readline())
    ans = -1
    ones, onesInterval, subarrsum = 0, 0, 0

    for ai in map(int, stdin.readline().split()):
        
        subarrsum += -1 if ai else 1
        ones += ai

        if subarrsum > ans:
            ans = subarrsum
            onesInterval += ai

        if subarrsum < 0:
            subarrsum = 0
            onesInterval = 0

    print(ans + ones - onesInterval)

if __name__ == "__main__":
    main()

