#!/usr/bin/env python3

def main():
    n = int(input())
    n_2 = n // 2
    nn = n*n

    print("\n".join(
        " ".join(
            " ".join(
                map(str,[i+j, nn - (i-1) - j])) for j in range(n_2)) 
            for i in range(1, nn//2 + 1, n_2))
        )

if __name__ == "__main__":
    main()

