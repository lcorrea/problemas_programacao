#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[1001];
    bool ok = true;
    int k;
    map <char, int> letters;
    map <char, int> :: const_iterator it;

    scanf ("%d", &k);
    scanf ("%s", str);

    for (char *ch = str; *ch; ch++)
        letters[*ch]++;

    for (it = letters.begin(); ok and it != letters.end(); it++)
        ok = (it->second % k) == 0;

    if (!ok)
        return printf ("-1\n"), 0;

    for (int len = strlen (str), cnt = strlen(str) / k; len; len -= cnt)
        for (it = letters.begin(); it != letters.end(); it++)
            for (int i = 0; i < it->second / k; i++)
                putchar (it->first);
    putchar ('\n');

    return 0;
}

