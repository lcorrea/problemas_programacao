#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n,m;

    cin >> n >> m;

    unsigned long long v[n];
    unsigned long long s = 0;

    for (int i = 0; i < n ; i++) {
        cin >> v[i];
    }

    for (int i = 0, j = 0; i < m; i++) {
        unsigned long long b;
        cin >> b;
        while (b > s + v[j]) {
            s += v[j++];
        }
        printf("%d %llu\n", j+1, b - s);
    }


    return 0;
}

