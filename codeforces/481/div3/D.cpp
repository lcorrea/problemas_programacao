#include <bits/stdc++.h>

#define INF 1000000000

using namespace std;
int dx[9] = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
int dy[9] = {-1, 0, 1, -1, 0, 1, -1, 0, 1};
int v[100001];

int solve(int k, int i, int c, int r, int n)
{
    int ans = INF;

    fprintf(stderr, "#%d %d (r:%d) ", i+1, k, r);

    if (i == n) {
        fprintf(stderr, " -- \n");
        return c;
    }

    fprintf(stderr, "v[%d] -> %d, %d, %d | r: %d\n", 
            i, v[i], v[i] - 1, v[i] + 1, r);

    if (v[i] - k == r)
        ans = c + solve(v[i], i+1, 0, r, n);

    if (v[i] - 1 - k == r)
        ans = min(ans, c + solve(v[i]-1, i+1, 1, r, n));

    if (v[i] + 1 - k == r)
        ans = min(ans, c + solve(v[i]+1, i+1, 1, r, n));
    
    if (ans >= INF)
        return INF;

    return ans;
}

int main (void)
{
    int n;

    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        scanf("%d", v + i);
    }

    if (n == 1 or n==2)
        puts("0");
    else {
        
        int ans = INF;

        for (int i = 0; i < 9; i++) {
            int a = v[0] + dx[i];
            int b = v[1] + dy[i];
            int r = b - a;
            ans = min(ans, solve(b, 2, (dx[i]!=0) + (dy[i]!=0), r, n));
        }

        printf("%d\n", ans >= INF ? -1 : ans);
    }

    return 0;
}

