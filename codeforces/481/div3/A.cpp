#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf("%d", &n);

    vector <int> count(1001, 0), v(n);

    for (int i = 0; i < n; i++) {
        scanf("%d", &v[i]);
    }

    vector<int> ans;
    for (auto i = v.rbegin(); i != v.rend(); i++) {
        if (++count[*i] == 1)
            ans.push_back(*i);
    }

    printf("%d\n", (int) ans.size());
    for (int i = ans.size() - 1; i >= 0; --i)
        printf("%d%c", ans[i], i ? ' ' : '\n');

    return 0;
}

