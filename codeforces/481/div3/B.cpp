#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int l;
    
    scanf("%d", &l);

    char str[l+1];

    scanf("%s", str);

    int count = 0;
    for (int i = 2; i < l; i++) {
        if (str[i-2] == 'x' and str[i-1] == 'x' and str[i] == 'x') {
            count++;
        }
    }

    printf("%d\n", count);

    return 0;
}

