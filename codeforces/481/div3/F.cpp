#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k, aux[200001], v[200001], c[200001] = {0};

    scanf("%d%d", &n, &k);

    for (int i = 0; i<n; i++)
        scanf("%d", v + i), aux[i] = v[i];

    for (int i = 0, a, b; i < k; i++) {
        scanf("%d %d", &a, &b);
        a--; b--;

        if (v[a] > v[b])
            c[a]++;

        if (v[b] > v[a])
            c[b]++;
    }
    sort(aux, aux+n);

    for (int i = 0; i < n; i++) {
        auto x = lower_bound(aux, aux+n, v[i]);
        printf("%d%c", (int) (x-aux) - c[i], i == n-1 ? '\n' : ' ');
    }



    return 0;
}

