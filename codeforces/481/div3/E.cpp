#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, w;

    scanf("%d %d", &n, &w);

    int maxk = 0;
    int mink = 0;

    for (int i = 0, d, k = 0; i < n; i++) {
        scanf("%d", &d);

        k += d;

        maxk = max(k, maxk);
        mink = min(k, mink);
    }

    int a = max(0, -mink);
    int b;

    if (maxk <= 0)
        b = w;
    else
        b = w - maxk;

    printf("%d\n", b >= a ? b - a + 1 : 0);

    return 0;
}

