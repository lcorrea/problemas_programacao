#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int cnt[2] = {0};
	char ch, last;
	bool danger = false;

	last = getchar();
	cnt[last-'0']++;

	while ((ch = getchar()) != '\n')
	{
		if (ch != last)
			cnt[0] = 0, cnt[1] = 0, last = ch;
		
		cnt[ch-'0']++;

		if (cnt[ch-'0'] >= 7)
			danger = true;
	}

	puts (danger ? "YES" : "NO");
	
	return 0;
}

