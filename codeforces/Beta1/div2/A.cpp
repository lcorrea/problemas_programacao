#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m, a;

	scanf ("%d %d %d", &n, &m, &a);

	long long int x = ceil (n/(double)a);
	long long int y = (m > a) ? ceil ((m - a)/(double)a)*x : 0ll;
	
	printf ("%I64d\n", x + y);

	return 0;
}

