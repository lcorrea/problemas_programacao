#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int x, y, z, n;

	cin >> n;
	cin >> x >> y >> z;

	for (int i = 1; i < n; i++)
	{
		int xx, yy, zz;
		cin >> xx >> yy >> zz;
		x += xx;
		y += yy;
		z += zz;
	}

	puts ((x or y or z) ? ("NO") : ("YES"));

	return 0;
}

