#!/usr/bin/env python3

def main():
    n = input()
    cnt = 0

    while len(n) > 1:
        n = str(sum(map(int, n)))
        cnt += 1

    print(cnt)

if __name__ == "__main__":
    main()

