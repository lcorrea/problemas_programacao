#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  char bus[1001][10], *p;
  bool ok = false;

  scanf ("%d", &n);

  for (int i = 0; i < n; i++)
  {
    scanf ("%s", bus[i]);

    if (!ok and (p = strstr (bus[i], "OO")) != NULL)
    {
      *p = '+'; *(p + 1) = '+';
      ok = true;
    }
  }

  puts (ok ? "YES" : "NO");
  if (ok)
    for (int i = 0; i < n; i++)
      printf ("%s\n", bus[i]);

  return 0;
}

