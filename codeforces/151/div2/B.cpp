#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, v[100010], mid = 0, sum1 = 0, sum2 = 0, cnt = 0;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i), mid += v[i];

    mid = mid / n;

    for (int i = 0; i < n; i++)
        if (v[i] > mid)
            sum1 += v[i] - mid;
        else
            sum2 += mid - v[i];

    if (sum1 == sum2)
        printf ("%d\n", n);
    else
        printf ("%d\n", n-1);

    return 0;
}

