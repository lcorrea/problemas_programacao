#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout, stderr

    n = int(stdin.readline())
    v = [int(i) for i in stdin.readline().split()]
    q = int(stdin.readline())

    presum = [0]
    presums = [0]
    for i, pair in enumerate(zip(v, sorted(v))):
        presum.append(presum[i]+pair[0])
        presums.append(presums[i]+pair[1])
    
    for line in stdin:
        t, l, r = map(int, line.split())
        if t == 1:
            stdout.write("%d\n" % (presum[r] - presum[l-1]))
        else:
            stdout.write("%d\n" % (presums[r] - presums[l-1]))

if __name__ == "__main__":
    main()

