#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int t = 0;
    int one = 0;
    int two = 0;
    bool ok = false;

    scanf ("%d", &n);

    for (int i = 0, w; i < n; i++)
    {
        scanf ("%d", &w);

        t += w;
        one += w == 100;
        two += w == 200;
    }

    for (int i = 0; i <= one and !ok; i++)
        for (int j = 0; j <= two and !ok; j++)
            ok = (i + j) == n/2 and (200*i + 400*j) == t;

    puts (ok ? "YES" : "NO");

    return 0;
}

