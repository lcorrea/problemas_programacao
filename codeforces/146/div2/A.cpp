#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  char alpha[255] = {0};
  int cnt = 0;
  char name[1001];

  scanf ("%s", name);

  for (int i = 0; name[i] != '\0'; i++)
    cnt += !alpha[name[i]], alpha[name[i]] = 1;

  puts (cnt % 2 ? "IGNORE HIM!" : "CHAT WITH HER!");

  return 0;
}

