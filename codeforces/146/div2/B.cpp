#include <bits/stdc++.h>

using namespace std;

#define MAXN 1000001

int tb[1000001] = {0};

int main (void)
{
    int a, b, c;

    scanf("%d %d %d", &a, &b, &c);

    tb[1] = 1;
    for (int i = 2; i < MAXN; i++) {
        tb[i]++;
        for (int j = i; j < MAXN; j+=i)
            tb[j]++;
    }

    unsigned long long ans = 0;
    for (int i = 1; i <= a; i++) {
        unsigned long long ansb = 0;
        for (int j = 1; j <= b; j++) {
            for (int k = 1; k <= c; k++) {
                ansb += tb[i*j*k];
            }
        }
        ans = (ans + (ansb%1073741824UL))%1073741824UL;
    }

    printf("%llu\n", ans);

    return 0;
}

