#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int q[101];
	priority_queue <int, vector <int>, greater<int> > pq;


	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
		scanf ("%d", q + i);

	for (int i = 0, total, itens; i < n; i++)
	{
		total = 0;
		for (int j = 0, sz = q[i]; j < sz; j++)
		{
			scanf ("%d", &itens);
			total += itens*5 + 15;
		}

		pq.push (total);
	}

	printf ("%d\n", pq.top());

	return 0;
}

