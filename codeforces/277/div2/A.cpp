#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long n;

    /*for (int i = 1, ans = 0; i < 10; i++) {
        ans += (i & 1 ? -1 : 1) * i;
        cout << i << "=>" << ans << endl;
    }*/

    cin >> n;

    int odd = n & 1;
    long long inc = odd ? 1 : 0;
    long long ans = ((n >> 1LL) + inc) * (odd ? -1LL : 1LL);

    cout << ans << endl;

    return 0;
}

