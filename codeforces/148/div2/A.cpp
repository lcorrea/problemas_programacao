#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int y, k, n, cnt = 0;

	cin >> y >> k >> n;

	for (int i = ceil(y/(double)k); i*k <= n; i++)
	{
		if (i*k - y == 0) continue;
		cout << i*k - y << ' ';
		cnt++;
	}

	if (!cnt) cout << -1;
	
	cout << endl;

	return 0;
}

