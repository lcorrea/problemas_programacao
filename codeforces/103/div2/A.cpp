#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int c, *pmin, *pmax;
	int line[110];

	pmin = line;
	pmax = line;

	cin >> c;

	for (int i = 0; i < c; i++)
	{
		cin >> line[i];
		if (*pmin >= line[i])
			pmin = line + i;

		if (*pmax < line[i])
			pmax = line + i;
	}

	int seconds = (&line[c-1] - pmin) + (pmax - line);

	if (pmin < pmax)
		seconds--;

	cout << seconds << endl;

	return 0;
}

