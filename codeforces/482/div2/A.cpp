#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    unsigned long long int n;

    scanf("%llu", &n);

    if ((n+1) & 1) {
        cout << n + 1;
    } else {
        cout << n - 1;
    }
    cout << "\n";

    return 0;
}

