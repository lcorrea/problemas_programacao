#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    bool ok = true;
    int grid[55][55];


    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            scanf ("%d", &grid[i][j]);

    for (int i = 0; i < n and ok; i++)
    {
        for (int j = 0; j < n and ok; j++)
        {
            if (grid[i][j] == 1) continue;

            ok = false;
            for (int k = 0; k < n and !ok; k++)
            {
                int x = grid[i][j] - grid[i][k];

                for (int l = 0; l < n and !ok; l++)
                    ok = x == grid[l][j];
            }
        }
    }

    puts (ok ? "Yes" : "No");

    return 0;
}

