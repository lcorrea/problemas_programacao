#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long int m, b;

    cin >> m >> b;

    unsigned long long int sumx, sumy;
    unsigned long long int sum = 0;
    unsigned long long int summax = 0;

    for (long long int i = 0; i <= b; i++)
    {
        long long int x, y;

        y = i;
        x = (b - y)*m;

        sumx = ((x+1ull)*x) >> 1;
        sumy = (y*(1ull+y)) >> 1;
        sum = sumx*(1ull + y) + (1ull + x)*sumy;

        summax = max (summax, sum);
    }

    cout << summax << endl;

    return 0;
}

