#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, total = 0, ans = 0;
    vector <int> stck;

    scanf ("%d%*c", &n);

    while(total < n)
    {
        char str[20];
        int a;

        scanf ("%s%*c", str);

        if (*str == 'a')
        {
            scanf ("%d%*c", &a);
            stck.push_back(a);
        }
        else
        {
            total += 1;

            if (stck.back() != total)
                stck.push_back(total+1), ans += 1;
            else
                stck.pop_back();
        }
    }

    printf ("%d\n", ans);

    return 0;
}

