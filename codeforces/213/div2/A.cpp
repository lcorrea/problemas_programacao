#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n, k, cnt = 0;

  scanf ("%d %d%*c", &n, &k);

  while (n--)
  {
    char c;
    bool ok = true;
    int t[10] = {0};

    while ((c = getchar ()) != '\n' and c != EOF)
      t[c - '0']++;

    for (int i = 0; i <= k; i++)
      if (t[i] == 0)
        ok = false;

    cnt += ok;
  }

  printf ("%d\n", cnt);

  return 0;
}

