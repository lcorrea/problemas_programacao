#!/usr/bin/env python3

def main():
    n = int(input())
    seq = list(map(int, input().split()))
    ans, i = 2, 2

    if n < 3:
        print(n)
        return

    while i < n:
        t, l = i, 2
        
        while t < n and (seq[t] == seq[t-1] + seq[t-2]):
            t += 1
            l += 1

        ans = max(ans, l)
        i = t if t > i else i + 1

    print(ans)

if __name__ == "__main__":
    main()

