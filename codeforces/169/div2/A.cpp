#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int m;
	int k, n, t, f;

	scanf ("%d %d", &n, &k);
	scanf ("%d %d", &f, &t);

	m = (t<=k) ? (f) : (f - (t - k));

	for (int i = 1; i < n; i++)
		scanf ("%d %d", &f, &t), m = max (m, (t<=k) ? (f) : (f - (t-k)));

	printf ("%d\n", m);

	return 0;
}

