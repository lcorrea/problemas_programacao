#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    map <int, int> int_gears;
    int a[51], b[51], n, m;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", a + i);

    scanf ("%d", &m);

    for (int j = 0; j < m; j++)
        scanf ("%d", b + j);

    for (int j = 0; j < m; j++)
        for (int i = 0; i < n; i++)
            if (b[j] % a[i] == 0)
                int_gears[b[j] / a[i]]++;

    printf ("%d\n", int_gears.rbegin()->second);

    return 0;
}

