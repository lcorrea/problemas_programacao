#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int m[100001], minv, cnt = 0;

    scanf("%d", &n);

    minv = n;
    for(int i = 0; i < n; i++) {
        scanf("%d", m+i);

        int x = n - m[i];
        minv = min(x, minv);

        printf("[%d] mark on %d => (x - minv = %d)\n", m[i], x, x - minv);

        cnt += x - minv;
    }

    printf("%d\n",cnt);

    return 0;
}

