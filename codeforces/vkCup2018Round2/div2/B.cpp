#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m, ok = 1;
    int r[50] = {0}, c[50] = {0};

    scanf("%d %d%*c", &n, &m);


    for (int i = 0; ok and i < n; i++) {

        for (int j = 0; ok and j < m; j++) {

            char ch;
            scanf("%c", &ch);

            if (ch == '#') {
                
                if (r[i] == 0 and c[j] == 0)
                    r[i] = c[j] = 1;
                else
                    ok = 0;
            }
        }
    }

    puts(ok ? "Yes" : "No");

    return 0;
}

