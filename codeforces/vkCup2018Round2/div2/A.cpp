#include <bits/stdc++.h>

using namespace std;

int dfs(int i, int n, char str[200])
{
    int cnt = 0;

    if(i == n)
        return 1;

    if (str[i] != '?')
        return dfs(i+1, n, str);

    char color[4] = "CMY";

    for (int j = 0; j < 3 and cnt < 2; j++) {

        if (i > 0 and (str[i-1] != color[j] and str[i+1] != color[j])) {
            str[i] = color[j];
            cnt += dfs(i+1, n, str);
            str[i] = '?';
        }
        
        else if (i == 0 and str[i+1] != color[j]) {

            str[i] = color[j];
            cnt += dfs(i+1, n, str);
            str[i] = '?';
        }
    }

    return cnt;

}

int main (void)
{
    int n;
    char str[200];

    scanf("%d", &n);

    scanf("%s", str);
    
    int ok = 1;

    for (int i = 0; i <= n-2 and ok; i++)
        if (str[i] == '?' or str[i+1] == '?') continue;
        else
          ok = str[i] != str[i+1];

    puts ((ok and (dfs(0, n, str) >= 2)) ? "Yes" : "No");

    return 0;
}

