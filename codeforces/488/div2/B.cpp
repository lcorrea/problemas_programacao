#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    pair <int, pair <int, int> > knights[100001];
    int n, k;

    scanf("%d %d", &n, &k);

    for (int i = 0 ; i < n; i++) {
        scanf("%d", &knights[i].first);
    }
    for (int i = 0; i < n; i++) {
        scanf("%d", &knights[i].second.first);
        knights[i].second.second = i;
    }

    sort (knights, knights+n);

    uint64_t ans[100001] = {0}, sum = 0;
    priority_queue<int> pq;

    for (int i = 0; i < n; i++) {
        int a = knights[i].second.first;
        sum += knights[i].second.first;
        ans[knights[i].second.second] = sum;
        pq.push(-a);

        if ((int) pq.size() == (k+1)) {
            int b = -pq.top(); pq.pop();
            sum -= b;
        }
    }

    for (int i = 0; i < n; i++)
        cout << ans[i] << (i==n-1?'\n':' ');

    return 0;
}

