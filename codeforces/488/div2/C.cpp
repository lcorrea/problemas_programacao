#include <bits/stdc++.h>

using namespace std;

inline double det(pair<int,int> a[2], pair<int,int>b[2])
{
    return (b[1].first - b[0].first) * (a[1].second - a[0].second) - (b[1].second - b[0].second) * (a[1].first - a[0].first);
}

int main (void)
{
    vector <pair<int,int>> a;
    vector <pair<int,int>> b;

    for (int i = 0, x, y; i < 4; i++) {
        scanf("%d %d", &x, &y);
        a.push_back({x, y});
    }
    for (int i = 0, x, y; i < 4; i++) {
        scanf("%d%d", &x, &y);
        b.push_back({x, y});
    }

    int hmaxb = 0, hminb = 200;
    int hmaxa = 0, hmina = 200;
    int lmaxb = 0, lminb = 200;
    int lmaxa = 0, lmina = 200;

    for (auto i : a) {
        hmaxa = max(i.second, hmaxa);
        hmina = min(i.second, hmina);
        lmaxa = max(i.first, lmaxa);
        lmina = min(i.first, lmina);
    }

    bool ans = false;

    for (auto i : b) {
        ans |= (i.first >= lmina and i.first <= lmaxa) and (i.second >= hmina and i.second <= hmaxa);
    }

    sort(b.begin(), b.end());

    for (auto i : a) {
        int cnt = 0;

        pair <int,int> sa[2] = {make_pair(i.first, i.second), make_pair(300, i.second)};
        pair <int, int> sb[2] = {b[0], b[2]};
        pair <int, int> sb2[2] = {b[2], b[3]};
        pair <int, int> sb3[2] = {b[0], b[1]};
        pair <int, int> sb4[2] = {b[1], b[3]};
        
        cnt += det(sa, sb);
        cnt += det(sa, sb2);
        cnt += det(sa, sb3);
        cnt += det(sa, sb4);

        ans |= (cnt == 1);
    }

    puts (ans ? "YES" : "NO");

    return 0;
}

