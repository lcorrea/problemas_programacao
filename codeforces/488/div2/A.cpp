#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m, a[11] = {0}, b[11] = {0};

    scanf("%d %d", &n, &m);

    for (int i = 0; i < n; i++)
        scanf("%d", a+i);

    for (int i = 0, k; i < m; i++)
        scanf("%d", &k), b[k]++;
    
    for (int i = 0; i < n; i++)
        if (b[a[i]]) {
            printf("%d ", a[i]);
            b[a[i]] = 0;
        }

    putchar('\n');

    return 0;
}

