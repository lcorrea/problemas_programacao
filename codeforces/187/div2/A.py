#!/usr/bin/env python3

def main():

    desc = set()
    brand = [0] * 1001
    proc = [0] * 1001
    tb = [False] * 1001

    n = int(input())
    total = n

    for i in range(n):
        a, b = map(int, input().split())
        desc.add(a)
        brand[a] += 1
        proc[b] += 1
        tb[b] |= True if a != b else False

    for a in desc:
        if (tb[a] or proc[a] > 1):
            total -= brand[a]
        elif proc[a]:
            total -= brand[a] - 1

    print(total)

        
if __name__ == "__main__":
    main()

