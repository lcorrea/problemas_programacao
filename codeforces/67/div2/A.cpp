#include <bits/stdc++.h>

using namespace std;

int without (int a)
{
	char b[30], bwithout[30];
	int p = 0;

	sprintf (b, "%d", a);

	for (int i = 0, sz = strlen(b); i < sz; i++)
		if (b[i] != '0')
			bwithout[p++] = b[i];
	bwithout[p] = '\0';

	return atoi (bwithout);
}


int main (void)
{
	int n1, n2, p;

	scanf ("%d%d", &n1, &n2);
	p = n1 + n2;

	if (without(p) == without(n1) + without(n2))
		puts ("YES");
	else
		puts ("NO");

	return 0;
}

