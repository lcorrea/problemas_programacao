#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int total = 0, min_remain;
	map <int, int> rep;

	for (int i = 0, a; i < 5; i++)
	{
		scanf ("%d", &a);
		total += a;
		rep[a]++;
	}

	min_remain = total;

	map <int, int>::iterator it;

	for (it = rep.begin(); it != rep.end(); it++)
	{
		if (it->second >= 3)
			min_remain = min (min_remain, total - 3*it->first);
		if (it->second == 2)
			min_remain = min (min_remain, total - 2*it->first);
	}

	printf ("%d\n", min_remain);

	return 0;
}

