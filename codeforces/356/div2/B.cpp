#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, a, total = 0;
	int c[1000];

	scanf ("%d%d", &n, &a);

	for (int i = 1; i <= n; i++)
		scanf ("%d", c + i);

	for (int max_d = max (abs(n-a), a - 1); max_d > 0; max_d--)
	{
		if (a + max_d <= n and a - max_d >= 1)
		{
			int posa = a + max_d;
			int posb = a - max_d;

			total += c[posa] and c[posb] ? 2 : 0;
		}
		else if (a + max_d <= n and c[a + max_d]) total++;
		else if (a - max_d >= 1 and c[a - max_d]) total++;
		else;
	}

	printf ("%d\n", total + c[a]);

	return 0;
}

