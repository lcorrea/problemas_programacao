#include <bits/stdc++.h>

using namespace std;

int check_next (int n)
{
	if (n > 10)
		return 0;

	char ans[10];

	printf ("%d\n", n);
	fflush (stdout);
	scanf ("%s", ans);

	return (*ans == 'y') + check_next (n + 1);
}

int main (void)
{
	int composite = 0, inc = 1;
	char ans[10];

	composite += check_next (2);

	printf ("%s\n", composite ? "composite" : "prime");
	fflush (stdout);

	return 0;
}

