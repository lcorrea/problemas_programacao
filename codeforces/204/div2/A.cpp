#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    vector <int> v(10000);
    int n, z = 0, sum = 0, m = -1;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf ("%d", &v[i]);

        sum += v[i];

        if (sum % 9 == 0)
            m = sum / 5;

        z += !v[i];
    }

    sort (v.rbegin(), v.rend());

    if (z and m > 0 and sum)
    {
        for (int i = 0; i < m; i++)
            printf ("%d", v[i]);
        printf ("%0*d", z, 0);
    }        
    else if (z)
        printf ("0");
    else
        printf ("-1");

    putchar ('\n');

    return 0;
}

