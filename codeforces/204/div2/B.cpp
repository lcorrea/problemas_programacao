#include <bits/stdc++.h>

using namespace std;

int v[100001], cnt[100001], last[100001];

int main (void)
{
    int n, m = 0;

    scanf ("%d", &n);

    for (int i = 0, a; i < n; i++)
    {
        scanf ("%d", v + i);


        a = v[i];
        m += cnt[a]==0;
        cnt[a]++;

        if (last[a] >= 0)
        {
            if (cnt[a] <= 2)
                last[a] = i - last[a];
            else
            {
                if (v[i] != v[i - last[a]])
                    last[a] = -1, m--;
            }
        }
    }

    printf ("%d\n", m);
    for (int i = 0; i <= 100000; i++)
        if (cnt[i])
            if (last[i] != -1)
                printf ("%d %d\n", i, cnt[i] == 1 ? 0 : last[i]);

    return 0;
}

