#include <bits/stdc++.h>

using namespace std;

typedef struct
{
    unsigned int v;
    unsigned int c;
    unsigned int t;
}Voucher;

bool cmp2 (Voucher a, Voucher b)
{
    if (a.t == b.t)
        return a.c < b.c;

    return a.t < b.t;
}

bool cmp (Voucher a, Voucher b)
{
    if (a.v == b.v)
        return cmp2(a, b);

    return a.v < b.v;
}

int main (void)
{
    int n, x;

    scanf ("%d %d", &n, &x);

    vector <Voucher> r(n), l(n);

    for (int i = 0; i < n; i++)
    {
        int a, b, c;

        scanf ("%d %d %d", &a, &b, &c);

        l[i].v = a; r[i].v = b;
        r[i].c = l[i].c = c;
        l[i].t = r[i].t = b - a + 1;
    }

    sort (r.begin(), r.end(), cmp);
    sort (l.begin(), l.end(), cmp);

    puts ("L:");
    for (auto it = l.begin(); it != l.end(); it++)
        printf ("%d %d %d\n", it->v, it->t, it->c);

    puts ("\nR: ");
    for (auto it = r.begin(); it != r.end(); it++)
        printf ("%d %d %d\n", it->v, it->t, it->c);

    unsigned int minC = UINT_MAX;

    for (int i = 0; i < n; i++)
    {
        if (l[i].t < x)
        {
            unsigned int z = x - l[i].t;
            Voucher a, b;

            a.v = l[i].v + l[i].t;
            a.t = b.t = z;
            a.c = b.c = l[i].c;
            b.v = a.v+z;

            printf ("searching by: %d (%d)\n", a.v, z);

            auto it = lower_bound(l.begin(), l.end(), a, cmp);
            auto it2 = upper_bound(l.begin(), l.end(), b, cmp);

            printf ("the answer is between %d and %d\n",
                    (int) (it - l.begin()), (int) (it2 - l.begin()));

            auto bounds = equal_range(it, l.end(), a, cmp2);
            it = lower_bound(it, it2, a, cmp2);

            printf ("guess %d %d (%d possible)\n",
                    (int) (bounds.first - l.begin()),
                    (int) (bounds.second - l.begin()),
                    (int) (it - l.begin()));
        }
    }

    return 0;
}

