#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    char s[n+1], t[m+1];

    scanf ("%s", s);
    scanf ("%s", t);

    int minCnt = n;
    int p = 0;
    
    for (int i = 0; i <= m - n; i++)
    {
        int pCnt = 0;
        
        for (int k = 0; k < n; k++)
            pCnt += t[i+k] != s[k];

        if (pCnt < minCnt)
        {
            p = i;
            minCnt = pCnt;
        }
    }

    printf ("%d\n", minCnt);
    for (int k = 0; k < n; k++)
        if (t[p+k] != s[k])
            printf ("%d%c", k+1, --minCnt ? ' ' : '\n');

    return 0;
}

