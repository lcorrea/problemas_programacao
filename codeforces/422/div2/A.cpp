#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int a, b;

    scanf ("%d %d", &a, &b);

    int c = min(a, b);
    unsigned long long int ans = 1;

    for (int i = 2; i <= c; i++)
        ans *= i;

    cout << ans << endl;

    return 0;
}

