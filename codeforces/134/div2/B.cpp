#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    scanf ("%d%d", &n, &m);

    priority_queue <int> pmax, pmin;

    for (int i = 0; i < m; i++)
    {
        int a;

        scanf ("%d", &a);

        pmax.push(a);
        pmin.push(-a);
    }

    int maxq, minq;

    maxq = minq = 0;

    while (n--)
    {
        while (!pmax.top()) pmax.pop();
        while (!pmin.top()) pmin.pop();
        
        int a = pmax.top(); pmax.pop(); pmax.push(a-1);
        int b = pmin.top(); pmin.pop(); pmin.push(b+1);

        maxq += a; minq += -b;
    }

    printf ("%d %d\n", maxq, minq);

    return 0;
}

