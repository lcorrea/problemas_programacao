#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    for (int i = 1; i <= n; i++)
    {    

        long long int pi = (4ll * (i + 1) * (i + 1) - 2ll * i) / i;

        cout << pi << endl;
    }

    return 0;
}

