#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int curtime = 0, c, word = 1, t;

    scanf ("%d%d", &n, &c);

    scanf ("%d", &t);

    curtime = t;

    for (int i = 1; i < n; i++)
    {
        scanf ("%d", &t);

        if (t - curtime > c)
            word = 1;
        else
            word++;

        curtime = t;
    }

    printf ("%d\n", word);
    
    
    return 0;
}

