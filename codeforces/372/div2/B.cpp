#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int f = 0;
    string s;

    cin >> s;

    for (int i = 0, sz = s.length(); sz >= 26 and i < sz; i++)
    {
        int alpha[27] = {0};
        int cnt = 0;
        bool find = true;
        //printf ("analysing... ");
        for (int l = 0; l < 26 and l + i < sz; l++)
        {
            //putchar (s[l+i]);
            if (s[l+i] == '?') { cnt++; continue; }

            if (alpha[s[l+i] - 'A'])
            {
                find = false;
                break;
            }
            else
                alpha[s[l+i] - 'A'] = 1, cnt++;
        }
        //printf ("\nfind: %d cnt: %d\n", find, cnt);

        if (find and cnt == 26)
        {
            for (int j = i; j < i + 26; j++)
            {
                if (s[j] == '?')
                    for (int k = 0; k < 26; k++)
                        if (alpha[k] == 0)
                        {
                            s[j] = k + 'A';
                            alpha[k] = 1;
                            break;
                        }
                        
            }

            for (int j = 0; j < sz; j++)
                putchar ( s[j] == '?' ? 'Z' : s[j]);
            putchar ('\n');
            return 0;
        }
    }

    puts ("-1");

    return 0;
}

