#include <bits/stdc++.h>

using namespace std;

int tb[200];

bool check(int p, char str[2][200]) {
    
    if (str[0][p] == '0')
        if (str[1][p] == '0')
            if  (str[1][p+1] == '0') {
                return true;
            }

    return false;
}

bool check90(int p, char str[2][200]) {
    if (str[0][p] == '0')
        if (str[0][p+1] == '0')
            if (str[1][p] == '0'){
                return true;
            }

    return false;
}

bool check180(int p, char str[2][200]) {
    if (str[0][p] =='0')
        if (str[0][p-1] == '0')
            if (str[1][p] == '0') {
                return true;
            }

    return false;
}

bool check270(int p, char str[2][200]) {
    if (str[0][p] == '0')
        if(str[1][p] == '0')
            if (str[1][p-1] == '0') {
                return true;
            }
    return false;
}

int put(int i, char str[2][200])
{
    if (str[0][i] == '\0')
        return tb[i] = 0;

    if (tb[i] != -1)
        return tb[i];

    int ans = 0;
    int p = i;
    
    if (check(i, str)) {
        str[0][p] = str[1][p] = str[1][p+1] = 'X';
        ans = max(ans, 1 + put(i+1, str));
        str[0][p] = str[1][p] = str[1][p+1] = '0';
    }

    if (check90(i, str)) {
        str[0][p] = str[0][p+1] = str[1][p] = 'X';
        ans = max(ans, 1 + put(i+1, str));
        str[0][p] = str[0][p+1] = str[1][p] = '0';
    }

    if (check180(i, str)) {
        str[0][p] = str[0][p-1] = str[1][p] = 'X';
        ans = max(ans, 1 + put(i+1, str));
        str[0][p] = str[0][p-1] = str[1][p] = '0';
    }

    if (check270(i, str)) {
        str[0][p] = str[1][p] = str[1][p-1] = 'X';
        ans = max(ans, 1 + put(i+1, str));
        str[0][p] = str[1][p] = str[1][p-1] = '0';
    }

    ans = max(ans, put(i+1, str));

    return tb[i] = ans;
}

int main (void)
{
    char str[2][200] = {0};

    scanf("%s", &str[0][1]);
    scanf("%s", &str[1][1]);

    memset(tb, -1, sizeof(tb));
    int ans = put(1, str);
    
    memset(tb, -1, sizeof(tb));
    ans = max(ans, put(2, str));
    
    printf("%d\n", ans);

    return 0;
}

