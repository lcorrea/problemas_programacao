#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int g[101] = {0};
    int s[101] = {0};

    scanf("%d", &n);

    for (int i = 0; i < n; i++)
        scanf("%d", g + i);

    sort(g, g+n);

    s[n-1] = g[n-1];
    for (int i = n-2; i >= 0; i--)
        s[i] = g[i] + s[i+1];

    int tg = round(s[0] / (double) n);
    int redo = 0;

    while (redo < n and tg < 5) {
        tg = round(((redo+1)*5.0 + s[redo+1]) / (double) n);
        redo++;
    }

    printf("%d\n", redo);

    return 0;
}

