#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string word;

    getline (cin, word);

    bool first_upper = word[0] == toupper (word[0]);
    bool other = true;

    for (int i = 1; i < (int)word.length(); i++)
        if (word[i] != toupper (word[i]))
            other = false;

    if ((first_upper and other) or other)
        for (int i = 0; i < (int)word.length(); i++)
            word[i] = word[i] == toupper(word[i]) ? tolower(word[i]) : toupper(word[i]);
    
    puts (word.c_str());

	return 0;
}

