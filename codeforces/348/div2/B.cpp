#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	long long int p = 1, j;
	bool infinite = false;
	int instructions[1000000];
	char last = 1;
	string strip;

	cin >> n;
	cin >> strip;

	for (int i = 1; i <= n; i++)
		cin >> instructions[i];

	while (p >= 1 and p <= n and !infinite)
	{
		if (strip[p-1] == '*')
			infinite = true;
		else
		{
			int inc = strip[p-1] == '<' ? -instructions[p] : instructions[p];
			strip[p-1] = '*';
			p += inc;
		}
	}

	puts (infinite ? "INFINITE" : "FINITE");
	
	return 0;
}

