#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, k, g, t;
    char str[101];

    scanf ("%d %d%*c", &n, &k);

    for (int i = 0; i < n; i++)
    {
        str[i] = getchar();

        if(str[i] == 'G')
            g = i;

        if (str[i] == 'T')
            t = i;
    }


    bool ok1 = false, ok2 = false;

    for (int i = g; !ok1 and i >= 0; i -= k)
    {
        if (str[i] == 'T')
            ok1 = true;

        if (str[i] == '#')
            break;
    }

    for (int i = g; !ok2 and i < n; i += k)
    {
        if (str[i] == 'T')
            ok2 = true;

        if (str[i] == '#')
            break;
    }

    puts ( ok1 or ok2 ? "YES" : "NO");

    return 0;
}

