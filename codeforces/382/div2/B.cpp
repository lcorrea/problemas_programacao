#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, o, p;
    int c[100001];

    scanf ("%d %d %d", &n, &o, &p);

    for (int i = 0, v; i < n; i++)
        scanf ("%d", &v), c[i] = -v;

    sort (c, c+n);

    double s1 = 0, s2 = 0;

    for (int i = 0; i < min(o, p); i++)
        s1 += -c[i];

    for (int i = min(o, p); i < min(o, p) + max(o, p); i++)
        s2 += -c[i];

    printf ("%lf\n", s1/min(o,p) + s2/max(o,p));

    return 0;
}

