#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
       	vector <int> v(3);

	scanf ("%d %d %d %d", &n, &v[0], &v[1], &v[2]);

	sort (v.rbegin(), v.rend());

	int m = 0;
	for (int i = 0; i*v[2] <= n; i++)
		for (int j = 0; j*v[1] <= n; j++)
			if (i*v[2]+j*v[1] <= n and (n - i*v[2] - j*v[1])%v[0]==0)
				m = max (m, (n - i*v[2] - j*v[1])/v[0] + i + j);
	printf ("%d\n", m);


	return 0;
}

