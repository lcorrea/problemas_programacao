#!/usr/bin/env python3

def main():
    
    with open('input.txt') as data_in, open('output.txt', 'w') as data_out:
        from collections import defaultdict

        n = int(data_in.readline())
        arr = defaultdict(list)
        
        for i, ai in enumerate(map(int, data_in.readline().split())):
            arr[ai].append(i+1)

        for k, v in arr.items():
            if (len(v) & 1):
                data_out.write('-1\n')
                return

        data_out.write(
                '\n'.join(
                    '\n'.join(
                        '%d %d' % (a, b) for a, b in zip(vec[::2], vec[1::2])
                        ) for vec in arr.values()
                    ) + '\n'
                )


if __name__ == "__main__":
    main()

