#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int k, a[12];
    int d = 0, ans = 0;

    scanf ("%d", &k);

    for (int i = 0; i < 12; i++)
        scanf ("%d", a + i), a[i] = -a[i];

    sort (a, a + 12);

    for (int i = 0; d < k and i < 12; i++)
        d += -a[i], ans++;

    if (d < k)
        puts ("-1");
    else
        printf ("%d\n", ans);

    return 0;
}

