#!/usr/bin/env python3
def main():

    def calculate(wolve):
        
        if wolve == len(wolves):
            return 0

        ans = 0
        positions = [(-1, 0), (0, -1), (0, 1), (1, 0)]
        y, x = wolves[wolve]

        for dy, dx in positions:
            ny, nx = y + dy, x + dx

            if ny < n and ny > -1:
                if nx < m and nx > -1:
                    if grid[ny][nx] == 'P':
                        grid[ny][nx] = '.'
                        return 1 + calculate(wolve+1)

        return calculate(wolve+1)

    n, m = map(int, input().split())
    grid = [list(input()) for i in range(n)]
    wolves = []

    for i in range(n):
        for j in range(m):
            if grid[i][j] == 'W':
                wolves.append((i, j))
    
    print(calculate(0))

if __name__ == "__main__":
    main()

