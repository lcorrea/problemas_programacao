#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, a, b;
    int capacity = 0;
    int people = 0;

    scanf ("%d", &n);

    while (n--)
    {
        scanf ("%d%d", &a, &b);

        capacity  = max (capacity, people);
        people -= a;
        people += b;
    }
    
    printf ("%d\n", capacity);
	return 0;
}

