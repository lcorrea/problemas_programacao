#include <bits/stdc++.h>

using namespace std;

vector < vector <int> > g;
int ans = 0;

void dfs (int u, int layer)
{
  ans = max (layer, ans);

  for (int i = 0; i < (int) g[u].size(); i++)
      dfs(g[u][i], layer + 1);
}

int main (void)
{
  int n;

  scanf ("%d", &n);

  visit.assign(n, 0);
  g.resize(n);

  for (int i = 0, a; i < n; i++)
  {
    scanf ("%d", &a);

    if (a > 0)
      g[a-1].push_back (i);
  }

  for (int i = 0; i < n; i++)
      dfs(i, 1);

  printf ("%d\n", ans);

  return 0;
}

