#include <bits/stdc++.h>

using namespace std;

inline bool isVowel(char c)
{
	c = tolower (c);
	return (c == 'a' or c == 'e' or c == 'i' or c == 'o' or c == 'u' or c == 'y');
}

int main (void)
{
	string word;

	cin >> word;

	for (int i = 0, len = word.length(); i < len; i++)
	{
		if (isVowel(word[i])) continue;

		putchar ('.');
		putchar ((isupper(word[i])) ? (tolower (word[i])) : word[i]);
	}

	cout << endl;

	return 0;
}

