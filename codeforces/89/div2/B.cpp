#include <bits/stdc++.h>

using namespace std;

string pattern[] = {
    "0",
    "0 1 0",
    "0 1 2 1 0",
    "0 1 2 3 2 1 0",
    "0 1 2 3 4 3 2 1 0",
    "0 1 2 3 4 5 4 3 2 1 0",
    "0 1 2 3 4 5 6 5 4 3 2 1 0",
    "0 1 2 3 4 5 6 7 6 5 4 3 2 1 0",
    "0 1 2 3 4 5 6 7 8 7 6 5 4 3 2 1 0",
    "0 1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1 0"
};

int main (void)
{
    int n;
    int i, space;

    scanf ("%d", &n);

    for (i = 0, space = 2*n + 1; i <= n; i++, space += 2)
        printf ("%*s\n", space, pattern[i].c_str());

    for (i = i - 2, space = space - 4; i >= 0; i -= 1, space -= 2)
        printf ("%*s\n", space, pattern[i].c_str());

    return 0;
}

