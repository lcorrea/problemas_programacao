#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;

int main (void)
{
	int n, k;
	priority_queue <ii , vector < ii >, greater< ii > > pq;

	scanf ("%d%d", &n, &k);

	for (int i = 0, ai; i < n; i++)
	{
		scanf ("%d", &ai);
		pq.push(ii(ai, i+1));
	}

	vector <int> instruments;

	while (!pq.empty() and k >= pq.top().first)
	{
		k -= pq.top().first;
		instruments.push_back(pq.top().second);
		pq.pop();
	}

	printf ("%d\n", (int)instruments.size());

	for (int i = 0, sz = (int) instruments.size(); i < sz; i++)
		printf ("%d ", instruments[i]);

	if (instruments.size())
		putchar ('\n');
	
	return 0;
}

