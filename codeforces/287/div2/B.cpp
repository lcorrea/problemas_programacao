#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int r, x, y, ex, ey;

	cin >> r >> x >> y >> ex >> ey;

	cout << ceil (hypot(ex-x, ey-y)/(2.0*r)) << endl;

	return 0;
}

