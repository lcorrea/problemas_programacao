#!/usr/bin/env python3

def main():
    
    def aux(ok, s, i):
        if i >= len(s):
            return 0

        if ok:
            s[i] = '4'
            return aux(True, s, i+1)

        if s[i] > '7':
            s[i] = '4'
            aux(True, s, i+1)
            return 1

        if s[i] > '4':
            ok = aux(s[i] < '7', s, i+1)
            s[i] = '4' if ok and s[i] == '7' else '7'
            return ok
        
        ok = aux(s[i] < '4', s, i+1)
        s[i] = '7' if ok and s[i] == '4' else '4'
        
        return 0

    def nextN(i):
        s = list(str(i))
        carryout = aux(False, s, 0)
        s = '4' + "".join(s) if carryout else "".join(s)
        return int(s)

    l, r = map(int, input().split())
    ans = 0
    nr = nextN(r)
    
    while l <= r:
        nl = nextN(l)
        
        if nl == nr:
            ans += nl * (r - l + 1)
            break

        ans += nl * (nl - l + 1)
        l = nl + 1

    print(ans)

if __name__ == "__main__":
    main()

