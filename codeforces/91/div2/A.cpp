#include <bits/stdc++.h>
#include <string>

using namespace std;

int main (void)
{
	int n;

	cin >> n;

	for (int i = 1; i <= n; i++)
	{
		string s = to_string (i);

		int t = 0;
		for (int j = 0; j < s.length() ; j++)
			t += (s[j] != '4') and (s[j] != '7');

		if (t == 0 and ((n % i) == 0))
		{
			puts ("YES");
			return 0;
		}
	}

	puts ("NO");

	return 0;
}

