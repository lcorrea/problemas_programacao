#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int k, d;

	cin >> k >> d;

	if (k > 1 and !d)
		puts ("No solution");
	else
		printf ("%d%0*d\n",d, k - 1, 0);

	return 0;
}

