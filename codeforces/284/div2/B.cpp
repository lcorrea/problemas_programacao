#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	map <string, string> dic;
	string word, sig;
	int a, b;

	cin >> a >> b;

	while (b--)
	{
		cin >> word >> sig;
		dic[word] = sig;
	}

	for (int i = 0; i < a; i++)
	{
		cin >> word;

		if (word.size() <= dic[word].size())
			cout << word << ' ';
		else
			cout << dic[word] << ' ';
	}
	cout << '\n';

	return 0;
}

