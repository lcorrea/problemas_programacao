#include <bits/stdc++.h>

using namespace std;

inline int skip (int a, int b, int x)
{
	return (b-a)%x;
}

int main (void)
{

	int n, x;
	int total = 0;
	int c = 1, l, r;

	scanf ("%d %d", &n, &x);

	scanf ("%d %d", &l, &r);

	total += skip (c, l, x) + r - l + 1;
	c = r+1;

	for (int i = 1; i < n; i++)
	{
		scanf ("%d %d", &l, &r);
		total += skip (c, l, x) + r - l + 1;
		c = r+1;
	}

	printf ("%d\n", total);
	
	return 0;
}

