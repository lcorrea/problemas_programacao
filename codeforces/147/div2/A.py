#!/usr/bin/env python3

def main():
    from sys import stdin
    from collections import Counter
    print(Counter(stdin.readlines()).most_common(1)[0][1])

if __name__ == "__main__":
    main()

