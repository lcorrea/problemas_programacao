#include <bits/stdc++.h>

using namespace std;

int solve(int i, int n, vector <int> &a) {

    if (i + 1 >= n) return 0;

    if (a[i] == a[i+1])
        return solve(i+2, n, a);

    vector <int> b(a.begin(), a.end());

    int j = i+2;
    int cnt = 0;
    while (j < n and b[j] != b[i]) j++;

    while (i + 1 < j) swap(b[j], b[j-1]), j--, cnt++;

    int cntb = cnt + solve(i+2, n, b);

    vector <int> c(a.begin(), a.end());

    j = i+2;
    int cnt2 = 0;
    while (j < n and b[j] != b[i+1]) j++;

    while (i < j) swap(b[j], b[j-1]), j--, cnt2++;

    int cntc = cnt2 + solve(i+2, n, c);

    return min(cntb, cntc);
}

int main (void)
{
    int n;

    scanf("%d", &n);
    vector <int> v(2*n);
    for (int i = 0; i < 2*n; i++)
        scanf("%d", &v[i]);
    printf("%d\n", solve(0, n, v) + 1);
    return 0;
}

