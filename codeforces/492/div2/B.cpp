#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int v[100001];

    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", v + i);
    }

    int minE = 2000000010;
    int mint = 2000000010;
    int ans = 1;

    for (int i = 0; i < n; i++) {
          int t = v[i] / n;
          int x = v[i] - (n*t + i);

          if (x > 0) {
              t++;
              x = v[i] - (n*t + i);
          }

          if (t < mint) {
              mint = t;
              minE = x;
              ans = i+1;
          } else if (t == mint) {
              if (x < minE and minE > 0) {
                  mint = t;
                  minE = x;
                  ans = i+1;
              }
          }
    }

    printf("%d\n", ans);

    return 0;
}

