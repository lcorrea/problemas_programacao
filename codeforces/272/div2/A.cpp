#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int x, y, n, m;

	cin >> n >> m;

	int bound = ceil(n/2.0);

	bool match = true;
	int i;
	for (i = bound; i <= n and match;i++)
		match = i%m;

	if (match)
		cout << "-1" << endl;
	else
		cout << i - 1<< endl;

	return 0;
}

