#!/usr/bin/env python3

def main():
    commands = list(input())
    wifi = list(input())
    possibilities = []

    def count(i, cnt, s):
        if len(s) == cnt:
            possibilities.append(i)
            return

        if s[cnt] == '+':
            count(i+1, cnt+1, s)
        elif s[cnt] == '-':
            count(i-1, cnt+1, s)
        else:
            count(i+1, cnt+1, s)
            count(i-1, cnt+1, s)

    endpoint = sum((1 if i == '+' else -1 for i in commands))
    count(0,0,wifi)
    
    total, chances = len(possibilities), sum((endpoint==i for i in possibilities))
    
    print(chances / total)

if __name__ == "__main__":
    main()
