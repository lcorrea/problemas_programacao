#include <bits/stdc++.h>

using namespace std;

bool check (char str[20])
{
    for (char *ch = str; *ch; ch++)
        if (*ch != '1' and *ch != '4')
            return false;

    if (str[0] == '4')
        return false;

    if (strstr(str, "444") != NULL)
        return false;

    return true;
}

int main (void)
{
    char str[20];

    scanf ("%s", str);

    puts (check(str) ? "YES" : "NO");

    return 0;
}

