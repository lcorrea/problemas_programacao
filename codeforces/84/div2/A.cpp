#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string number;
    int cnt = 0;

    cin >> number;

    for (int i = 0; number[i] != '\0'; i++)
        cnt += number[i] == '4' or number[i] == '7';
    
    cout << (cnt == 4 or cnt == 7 ? "YES" : "NO") << endl;

    return 0;
}

