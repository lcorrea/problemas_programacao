#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n,k,w;

    cin >> k >> n >> w;

    int sn = k*(((w + 1)*w) >> 1);

    cout << max(sn-n, 0) << endl;

    return 0;
}

