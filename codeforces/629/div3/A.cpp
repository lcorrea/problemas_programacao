#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    cin >> t;

    while (t--) {
        int a, b;

        cin >> a >> b;

        int d = a / b;
        int r = a % b;

        if (r)
            cout << (d + 1) * b - a << endl;
        else
            cout << 0 << endl;
    }

    return 0;
}

