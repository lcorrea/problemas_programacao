#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int alpha[255] = {0};
  int cnt = 0;
  char name[101];

  for (int i = 0; i < 2; i++)
  {
    scanf ("%s", name);

    for (int j = 0; name[j] != '\0'; j++)
      alpha[name[j]]++;

    cnt += strlen(name);
  }

  bool ok = true;
  scanf ("%s", name);

  for (int i = 0; ok and name[i] != '\0'; i++)
    if (alpha[name[i]] > 0)
      alpha[name[i]]--, cnt--;
    else
      ok = false;

  puts (ok and cnt == 0 ? "YES" : "NO");

  return 0;
}

