#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[200], str1[200];

    scanf ("%s\n%s", str, str1);

    if (strlen (str) != strlen (str1))
        puts ("NO");
    else
    {
        bool ok = true;

        for (int i = 0, sz = strlen(str1); ok and i < sz; i++)
            ok = str[i] == str1[sz - 1 - i];

        puts (ok ? "YES" : "NO");
    }

    return 0;
}

