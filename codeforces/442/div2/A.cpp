#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int cnt = 0;
    char str[101];
    char names[5][20] = { "Danil", "Olya", "Slava", "Ann", "Nikita"};

    scanf ("%s", str);

    for (int i = 0; i < 5; i++)
    {
        char *ptr = str;
        while ((ptr = strstr(ptr, names[i]))!=NULL)
            cnt++, ptr++;
    }

    puts (cnt == 1 ? "YES" : "NO");

    return 0;
}

