#include <bits/stdc++.h>

using namespace std;

#define INF 1000000000

int bfs(int i, int j, int fx, int fy, int k, int n, int m, char graph[1001][1001])
{
    const int dx[4] = {0, 0, 1, -1};
    const int dy[4] = {-1, 1, 0, 0};
    int dist[1001][1001];

    for (int ii = 0; ii < 1001; ii++)
        for (int jj = 0; jj < 1001; jj++)
            dist[ii][jj] = INF;

    queue <pair<int, int>> q;
    dist[i][j] = 0;
    q.push({i, j});
    while (!q.empty())
    {
        i = q.front().first;
        j = q.front().second; q.pop();

        if (i == fx and j == fy)
            break;

        for (int dir = 0; dir < 4; dir++)
        {
            for (int d = 1; d <= k; d++)
            {   
                int newx = j + dx[dir]*d;
                int newy = i + dy[dir]*d;

                if (newx >= 1 and newx <= m)
                    if (newy >= 1 and newy <= n)
                        if (graph[newy][newx] == '.')
                            if (dist[i][j] + 1 < dist[newy][newx])
                            {
                                dist[newy][newx] = dist[i][j] + 1;
                                q.push({newy, newx});
                            }
                        else if (graph[newy][newx] == '#')
                            break;
                        else;
                    else
                        break;
                else
                    break;

            }
        }
    }

    return dist[fx][fy];
}

int main (void)
{
    int n, m, k;
    int x, y, fx, fy, dist;
    char graph[1001][1001];

    scanf ("%d %d %d", &n, &m, &k);

    for (int i = 1; i <= n; i++)
        scanf ("%s", &graph[i][1]);

    scanf ("%d %d %d %d", &x, &y, &fx, &fy);

    dist = bfs(x, y, fx, fy, k, n, m, graph);
    
    printf ("%d\n", dist == INF ? -1 : dist);
    
    return 0;
}

