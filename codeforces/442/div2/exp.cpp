#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long int ull;

ull exp(ull base, int n)
{
    ull ans = 1ull;
    
    while (n)
    {
        if (n & 1)
            ans *= base;

        base *= base;
        
        n >>= 1;
    }

    return ans;
}

ull expmod (ull base, int n, int m)
{
    ull ans = 1ull;

    while (n)
    {
        if (n & 1)
            ans = (ans * base ) % m;

        base = (base * base) % m;

        n >>= 1;
    }

    return ans;
}

int main ()
{
   
    return 0;
}
