#!/usr/bin/env python3

def calc(a):
    d = max(a)
    return list(map(lambda i: d - i, a))

def main():
    t = int(input())
    for ti in range(t):
        n, k = map(int, input().split())
        a = [ai for ai in map(int, input().split())]
        a = calc(a)
        if k % 2 == 0:
            a = calc(a)
        print(" ".join(map(str, a)))

if __name__ == "__main__":
    main()

