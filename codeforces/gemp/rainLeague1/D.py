#!/usr/bin/env python3

def main():
    t = int(input())
    for ti in range(t):
        n = int(input())
        p = input().split()
        print(" ".join(p[-1::-1]))

if __name__ == "__main__":
    main()

