#!/usr/bin/env python3

import math

def calc(n, k):
    if k < n:
        return k
    r = k // n
    v = k + r
    while (r < (v // n)):
        r = v // n
        v = k + (v // n)
    return v

def main():
    t = int(input())
    for ti in range(t):
        n, k = map(int, input().split())
        print(calc(n, k))

if __name__ == "__main__":
    main()

