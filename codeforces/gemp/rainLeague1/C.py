#!/usr/bin/env python3

def main():
    n = int(input())
    G = [[] for i in range(n)]
    R = []

    def bfs(employee, level):
        maxLevel = 0
        queue = [(employee, level)]
        while queue:
            employee, level = queue.pop()
            maxLevel = max(maxLevel, level)
            for v in G[employee]:
                queue.append((v, level+1))
        return maxLevel

    for i in range(n):
        m = int(input())
        if m >= 1:
            G[m-1].append(i)
        else:
            R.append(i)

    ans = 1
    for i in R:
        ans = max(ans, bfs(i, 1))

    print(ans)

if __name__ == "__main__":
    main()

