#!/usr/bin/env python3

def main():
    n, k = map(int, input().split())
    road = input()
    block = '#' * k
    if -1 == road.find(block):
        print("YES")
    else:
        print("NO")

if __name__ == "__main__":
    main()

