#!/usr/bin/env python3
import math

def main():
    n = int(input())
    groups = list(map(int, input().split()))
    taxi, load = 0, 0
    count = [0,0,0,0]

    for group in groups:
        count[group-1] += 1

    aloc = min(count[2], count[0])
    count[0] -= aloc
    count[2] -= aloc
    
    taxi += aloc

    aloc = min(count[0] // 2, count[1])
    count[1] -= aloc
    count[0] -= aloc*2
    taxi += aloc

    if (count[0] and count[1]):
        count[1] -= 1
        count[0] -= 1
        taxi += 1

    taxi += math.ceil(count[0]/4) + math.ceil(count[1]/2) + count[2] + count[3]

    print(taxi)

if __name__ == "__main__":
    main()

