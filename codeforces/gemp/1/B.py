#!/usr/bin/env python3

def find(n, m):
    if (m == 0):
        return 0
    calc = lambda x: (x * (x-1)) >> 1
    lo, hi, mid = 0, n+1, 0
    for i in range(64):
        mid = lo + ((hi-lo) >> 1)
        v = calc(mid)
        if (v < m):
            lo = mid + 1
        elif v > m:
            hi = mid - 1
        else:
            break
    if (calc(mid) < m):
        mid += 1
    return mid
    

def main():
    n, m = map(int, input().split())
    print(n - min(n, m*2), n - find(n, m))

if __name__ == "__main__":
    main()

