#!/usr/bin/env python3

def main():
    n = int(input())
    inc = n//2
    p1, p2 = 1, n**2

    for i in range(n):
        brother = []
        for j in range(p1, p1+inc):
            brother.append(j)
        for j in range(p2, p2-inc, -1):
            brother.append(j)
        p1, p2 = p1+inc, p2-inc
        print(" ".join(map(str, brother)))

if __name__ == "__main__":
    main()

