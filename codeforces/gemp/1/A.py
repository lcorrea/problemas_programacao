#!/usr/bin/env python3

def main():
    ans = "YES"
    s, n = map(int, input().split())
    dragons = [tuple(map(int, input().split())) for _ in range(n)]
    dragons.sort()
    for dragon in dragons:
        if(dragon[0] < s):
            s += dragon[1]
        else:
            ans = "NO"
            break
    print(ans)

if __name__ == "__main__":
    main()

