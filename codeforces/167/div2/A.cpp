#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  int sum = 0;
  int ways = 0;

  scanf ("%d", &n);

  for (int i = 0, a; i < n; i++)
    scanf ("%d", &a), sum += a;

  for (int i = 1; i <= 5; i++)
    if (((sum + i - 1) % (n+1)))
      ways++;

  printf ("%d\n", ways);

  return 0;
}

