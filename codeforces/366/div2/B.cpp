#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  unsigned long long int s = 0;

  scanf ("%d", &n);

  for (int i = 0, b; i < n; i++)
  {
    scanf ("%d", &b);
    
    s = s + (b - 1);

    if (s % 2) puts ("1");
    else puts ("2");
  }

  return 0;
}

