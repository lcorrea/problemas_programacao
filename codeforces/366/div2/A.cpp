#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;
  const char layer[2][20] = { "hate", "love" };
  
  scanf ("%d", &n);

  for (int i = 0; i < n; i++)
    printf ("I %s %s", layer[i%2], i + 1 == n ? ("it\n") : "that ");

  return 0;
}

