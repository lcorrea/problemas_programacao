#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;
	int numbers[200];
	int nEven=0, nOdd=0, *even, *odd;

	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
	{
		scanf ("%d", &numbers[i]);

		if (numbers[i]%2)
			nOdd++, odd = numbers + i;
		else
			nEven++, even = numbers + i;
	}

	int solv = nOdd >= nEven ? (even - numbers + 1) : (odd - numbers + 1);

	printf ("%d\n", solv);

	return 0;
}

