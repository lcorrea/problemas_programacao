#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;

	cin >> n >> m;

	vector < vector < pair <int, string> > > reg (m);

	for (int i = 0; i < n; i++)
	{
		int r, p;
		string name;

		cin >> name >> r >> p;
	
		reg[r-1].push_back (make_pair (p, name));
	}

	for (int i = 0; i < m; i++)
	{
		sort (reg[i].rbegin(), reg[i].rend());

		if (reg[i].size() > 2 and (reg[i][2].first == reg[i][0].first or reg[i][2].first == reg[i][1].first))
			cout << "?" << endl;
		else
			cout << reg[i][0].second << ' ' << reg[i][1].second << endl;
	}

	return 0;

}

