#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	string str1, str2;

	cin >> str1 >> str2;
	
	for (int i = 0, len = str1.length(); i < len; i++)
		str1[i] = tolower (str1[i]);
	for (int i = 0, len = str2.length(); i < len; i++)
		str2[i] = tolower (str2[i]);

	cout << strcmp (str1.c_str(), str2.c_str()) << endl;

	return 0;
}

