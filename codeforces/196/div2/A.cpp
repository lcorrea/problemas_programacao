#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;
	
	cin >> n >> m;
	
	vector <int> pluzzes(m);
	
	for (int i = 0; i < m; i++)
			cin >> pluzzes[i];
	
	sort (pluzzes.begin(), pluzzes.end());
	vector <int>::const_iterator it = pluzzes.begin(),
								 it2 = pluzzes.begin() + (n-1);
	int minDiff = (*it2) - (*it);		 
	while (it2 != pluzzes.end())
	{
		minDiff = min ((*it2) - (*it), minDiff);
		it++;
		it2++;
	}
	
	cout << minDiff << endl;
	
	return 0;
}
