#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	map <string, int> teams;
	priority_queue < pair <int, string> > score;
	string team;
	int n;

	cin >> n;

	while (n--)
	{
		cin >> team;
		teams[team]++;
		score.push (make_pair(teams[team], team));
	}

	cout << score.top().second << endl;

	return 0;
}

