#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char ascii[256] = {0};
    string head, text;

    getline(cin, head);
    getline(cin, text);

    for (int i = 0; head[i]; i++)
        ascii[head[i]]++;

    bool ok = true;

    for (int i = 0; text[i] and ok; i++)
        if (isalpha(text[i]))
            if (ascii[text[i]])
                ascii[text[i]]--;
            else
                ok = false;

    puts (ok ? "YES" : "NO");

    return 0;
}

