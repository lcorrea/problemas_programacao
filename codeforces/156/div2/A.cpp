#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m[3] = {0};
	char str[3][20] = {"chest", "biceps", "back"};
	
	scanf ("%d", &n);

	int ai, *p = m, k;

	for (int i = 0; i < n; i++)
		scanf ("%d", &ai), k = i%3, m[k] += ai, p = m[k] > *p ? &m[k] : p;

	puts (str[p - m]);

	return 0;
}

