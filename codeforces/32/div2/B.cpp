#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    string line;

    getline (cin, line);

    for (int i = 0, sz = (int) line.size(); i < sz; i++)
    {
        if (line[i] == '-')
        {
            if (line[i+1] == '.')
                cout << 1;
            else
                cout << 2;
            i+=1;
        }
        else
            cout << 0;
    }
    cout << endl;

	return 0;
}

