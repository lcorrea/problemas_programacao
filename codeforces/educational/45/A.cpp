#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long int n, m, k;
    int a, b;

    cin >> n >> m >> a >> b;

    k = n / m;

    cout << min(a*((k+1)*m - n), b*(n - k*m)) << endl;

    return 0;
}

