#include <bits/stdc++.h>

using namespace std;

map<int, uint64_t> open, close;
uint64_t complete = 0;

uint64_t solve()
{
    uint64_t ans = 0;

    for (auto i : open) {
        ans += min((uint64_t)i.second, close[i.first]) * max((uint64_t)i.second, close[i.first]);
    }

    ans += complete * complete;

    return ans;
}

void check (char str[300001])
{
    queue <char> q;
    int qntclose = 0;
    
    for (int i = 0; str[i]; i++) {
        if (q.empty() and str[i] == ')')
            qntclose++;
        else if (str[i] == ')')
            q.pop();
        else
            q.push('(');
    }

    if (qntclose and !q.empty());
    else if (qntclose) {
        close[qntclose]++;
        //fprintf (stderr, "close %d = %d + 1\n", qntclose, close[qntclose]-1);
    }
    else if (!q.empty()) {
        open[(int)q.size()]++;
        //fprintf (stderr, "open %d = %d + 1\n", (int)q.size(), open[(int)q.size()]-1);
    }
    else {
        //fprintf(stderr, "complete %d + 1\n", complete+1);
        complete++;
    }
}

int main (void)
{
    char str[300001];
    int n;

    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        scanf("%s", str);
        check(str);
    }

    cout << solve() << endl;

    return 0;
}

