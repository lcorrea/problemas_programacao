#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    unordered_map<int, int> freq;
    vector <int> num;
    int n, k;

    scanf("%d%d", &n, &k);

    for (int i = 0, b; i < n; i++) {
        scanf("%d", &b);
        freq[b]++;
        if (freq[b] == 1)
            num.push_back(b);
    }

    sort(num.begin(), num.end());

    int ans = n;

    for (int i = 0, n_set = (int) num.size(); i <= n_set-2; i++)
        if (num[i] >= num[i+1] - k)
            ans -= freq[num[i]];

    printf("%d\n", ans);

    return 0;
}

