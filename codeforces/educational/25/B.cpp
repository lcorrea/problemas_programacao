#include <bits/stdc++.h>

using namespace std;

char mapa[100][100];

bool counter(int p, int i, int j, int dx, int dy)
{
    if (p == 0) return true;

    int cnt = 0;
    while (mapa[i+dy][j+dx] == 'X' and cnt <= 5)
    {
        i += dy; j += dx;
        cnt++;
    }

    return cnt >= p;
}

bool check (int p, int i, int j, int dir)
{
    int dy[8] = {1, -1, 0, 1, -1, 0, 1, -1};
    int dx[8] = {0, 0, 1, 1, 1, -1, -1, -1};

    bool a = counter(5-p, i, j, dx[dir], dy[dir]);
    bool b = counter(p-1, i, j, -dx[dir], -dy[dir]);

    //printf ("%d %d (%d, %d)\n", a, b, i, j);

    return a and b;
}

//up - 1
//right - 2
//down + right - 3
//up + right - 4

int main (void)
{
    for (int i = 10; i < 20; i++)
        scanf ("%s", &mapa[i][10]);

    bool ok = false;
    for (int i = 10; !ok and i < 20; i++)
        for (int j = 10; !ok and j < 20; j++)
            if (mapa[i][j] == '.')
                for (int k = 1; !ok and k <= 4; k++)
                    for (int h = 1; !ok and h <= 5; h++)
                        ok = check(h, i, j, k);

    puts (ok ? "YES" : "NO");

    return 0;
}

