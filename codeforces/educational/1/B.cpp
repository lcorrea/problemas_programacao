#include <bits/stdc++.h>

using namespace std;

void rotate (char *str, int l, int r, int k)
{
	int len = r - l + 1;
	string ss = str, s;
	
	s = ss.substr(l-1, len);

	for (int i = l-1, j = 0; j < len; i++, j++)
		s[(j+k)%len] = str[i];

	strncpy (str+l-1, s.c_str(), len);
}

int main (void)
{
	char str[10010];
	int m;

	scanf ("%s", str);
	scanf ("%d", &m);

	while (m--)
	{
		int l, r, k;

		scanf ("%d %d %d", &l, &r, &k);

		rotate (str, l, r, k);
	}

	puts (str);

	return 0;
}

