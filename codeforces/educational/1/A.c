#include <stdio.h>

int main (void)
{
	int t, i;
	long long int n;

	scanf ("%d", &t);

	while (t--)
	{
		long long int sum = 0;
		
		scanf ("%lld", &n);

		sum = (n + n*n) / 2ll;

		for (i = 1; i <= n; i <<= 1)
			sum -= 2ll*i;

		printf ("%lld\n", sum);
	}

	return 0;
}

