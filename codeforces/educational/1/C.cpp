#include <bits/stdc++.h>
#include <cmath>

using namespace std;

int main (void)
{
	int n, indx1=1, indx2=2, x, y;
	long double teta = 2e18;
	long double PI = M_PI;

	pair <long double, int> vec[100010];

	scanf ("%d", &n);

	for (int i = 0; i < n; i++)
	{
		scanf ("%d %d", &x, &y);
		long double ang = atan2 ((long double) x, (long double) y) * 180.0 / PI;
		if (ang < 0) ang += 360.0;
		vec[i] = make_pair (ang, i+1);
	}

	sort (vec, vec + n);
	
	for (int i = 0; i < n; i++)
	{
		long double minAng = vec[(i+1)%n].first - vec[i].first;

		if (minAng < 0) minAng += 360.0;

		if (minAng <= teta)
		{
			indx2 = vec[(i+1)%n].second;
			indx1 = vec[i].second;
			teta = minAng;
		}
	}

	printf ("%d %d\n", indx1, indx2);

	return 0;
}

