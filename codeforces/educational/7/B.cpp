#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int hh, mm, minutes;
	int a;

	scanf ("%d:%d", &hh, &mm);
	scanf ("%d", &a);

	minutes = hh*60 + mm + a;
	hh = minutes/60;
	mm = minutes - hh*60;

	printf ("%02d:%02d\n", hh%24, mm);

	return 0;
}

