#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;
	int a[200010] = {0};
	int b[200010] = {0};

	scanf ("%d %d", &n, &m);

	for (int i = 1; i <= n; i++)
	{	
		scanf ("%d", &a[i]);
		b[i] = a[i] == a[i-1] ? b[i-1] : i - 1;
	}

	int l, r, x;

	while (m--)
	{
		scanf ("%d %d %d", &l, &r, &x);
		printf ("%d\n", (a[r] == x) ? (b[r] >= l ? (b[r]) : (-1)) : (r));
	}
		
	return 0;
}

