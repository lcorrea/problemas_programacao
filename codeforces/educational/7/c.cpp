#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, m;
	int a[200010] = {0};
	int b[200010] = {0};
	int *p = a+1;

	ios_base::sync_with_stdio(false);

	cin >> n >> m;

	cin >> a[1];
	for (int i = 2; i <= n; i++)
	{	
		cin >> a[i];
		if (*p==a[i]) b[p - a]++;
		else
			p = &a[i];
	}

	for (int i = 1; i <= n;)
	{
		int d = b[i];
		if (d)
		{
			i++;
			while (i <= n and d)
				b[i++] = --d;
		}
		else i++;
	}

	int l, r, x;
	while (m--)
	{
		cin >> l >> r >> x;

		if (a[l] == x and b[l] >= r-l) {cout << "-1" << endl; continue;}

		if (a[l] == x) l+=b[l]+1;

		cout << l << endl;
	}

	return 0;
}

