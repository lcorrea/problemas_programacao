#include <bits/stdc++.h>

using namespace std;

void print_split (int start, int len, int piece, string word)
{
    for (int i = start; i < len; i += piece)
        printf ("%s\n", word.substr(i, piece).c_str());
}


int main (void)
{
    int n, p, q;
    string word;

    scanf ("%d %d %d%*c", &n, &p, &q);

    getline (cin, word);

    for (int a = 0; a*p <= n; a++)
    {
        for (int b = 0; a*p + b*q <= n; b++)
        {
            if (a*p + b*q == n)
            {
                printf ("%d\n", a+b);
                print_split(0, a*p, p, word);
                print_split(a*p, word.length(), q, word);
                return 0;
            }
        }
    }

    puts ("-1");

	return 0;
}

