#include <cstdio>
#include <iostream>
#include <algorithm>
#include <utility>

using namespace std;

int main (void)
{
	int n, p;
	int sectors[200009] = {0};

	scanf ("%d", &n);

	for (int i = 1; i <= n; i++)
	{
		scanf ("%d", &p);
		sectors[p] = i;
	}

	long long unsigned int time = 0;
	for (int i = 2; i <= n; i++)
		time += abs (sectors[i-1] - sectors[i]);

	cout << time << endl;

	return 0;
}

