#include <cstdio>
#include <stack>
#include <algorithm>

using namespace std;

char isOpenBracket (char a)
{
	if (a == '(' or a == '{' or a == '[' or a == '<')
	{
		if (a == '(')
			return ')';
		else
			return a + 2;
	}

	return '\0';
}

int main (void)
{
	stack <char> rbs;
	char c, top;
	int trocas = 0;
	bool possible = true;

	while ((c = getchar ()) != '\n')
	{

		if (top = isOpenBracket (c)) rbs.push (c);
		else if (!rbs.empty()) 
		{
			if (isOpenBracket(rbs.top()) != c)
				trocas++;

				rbs.pop();
		}
		else
		{
			rbs.push(c);
			possible = false;
		}
	}

	if (possible && rbs.empty())
		printf ("%d\n", trocas);
	else
		puts ("Impossible");
	return 0;
}

