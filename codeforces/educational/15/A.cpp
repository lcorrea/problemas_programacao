#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, arr[100001];
	int lis = 0;
	int cnt = 1;

	scanf ("%d", &n);

	scanf ("%d", arr);
	for (int i = 1; i < n; i++)
	{
		scanf ("%d", arr + i);

		if (arr[i-1] < arr[i])
			cnt++;
		else
			lis = max (lis, cnt), cnt = 1;
	}
	lis = max (lis, cnt);

	printf ("%d\n", lis);

	return 0;
}

