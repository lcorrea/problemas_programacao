#include <bits/stdc++.h>

using namespace std;

int main (void)
{
		int n, arr[100001];
		int cnt = 0;

		scanf ("%d", &n);

		for (int i = 0; i < n; i++)
				scanf ("%d", arr + i);

		for (int i = 0; i < n; i++)
				for (int j = i + 1; j < n; j++)
				{
						long long int s = arr[i] + arr[j];
						cnt += ((s & -s) == s);
				}

		printf ("%d\n", cnt);
		return 0;
}

