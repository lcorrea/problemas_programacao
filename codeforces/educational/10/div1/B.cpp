#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n;

	cin >> n;

	int v[n], s[n];

	for (int i = 0; i < n; i++)
		cin >> v[i];

	sort (v, v+n);

	int *pa = v, *pb = v + n - 1, i = 0;

	while (pa < pb)
		cout << *pa << ' ' << *pb << ' ', pa++, pb--;
	
	if (n%2) cout << *pa;

	cout << endl;

	return 0;
}

