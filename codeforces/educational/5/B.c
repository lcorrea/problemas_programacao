#include <stdio.h>

#define max(a, b) ((a>b)?(a):(b))
#define min(a, b) ((*a<*b)?(a):(b))

int main (void)
{
	int m, n, i, j;
	int *costMin, costMax;
	int munhattan[110][110];

	scanf ("%d %d", &n, &m);
	
	costMax = 0;

	for (i=0; i<n; i++)
	{
		costMin = &munhattan[i][0];

		for (j=0; j<m; j++)
		{
			scanf ("%d", &munhattan[i][j]);
			costMin = min (costMin, &munhattan[i][j]);
		}

		costMax = max(costMax, *costMin);
	}

	printf ("%d\n", costMax);

	return 0;
}

