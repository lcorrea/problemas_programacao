#include <stdio.h>
#include <string.h>

unsigned int readN (char *a)
{
	unsigned int len=0;
	char c;

	while ((c = getchar()) == '0');

	if (c == '\n') { a[len++] = '0'; return 1U; }
	else a[len++] = c;

	while ((c = getchar()) != '\n') a[len++] = c;
	a[len] = '\0';

	return len;
}

int main (void)
{
	char str1[1000010], str2[1000010], *a, *b;
	char c;
	unsigned int len1 = readN(str1);
	unsigned int len2 = readN(str2);

	if (len1 > len2) puts (">");
	else if (len1 < len2) puts ("<");
	else
	{
		int r = strcmp (str1, str2);

		if (r == 1) puts (">");
		else if (-1 == r) puts ("<");
		else puts ("=");
	}

	return 0;
}

