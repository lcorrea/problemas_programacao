#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int x[4] = {8, 4, 2, 6};

    int n;

    scanf ("%d", &n);

    if (n == 0)
        puts ("1");
    else
        printf ("%d\n", x[(n-1) % 4]);

    return 0;
}

