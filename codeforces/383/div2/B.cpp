#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, x, v[100001];
    long long cnt = 0;

    map <unsigned long long, int> ans;

    scanf ("%d %d", &n, &x);

    for (int i = 0; i < n; i++)
        scanf ("%d", v + i), ans[v[i]]++;

    for (int i = 0; i < n; i++)
        cnt += ans[v[i] ^ x] * ans[v[i]];

    printf ("%I64d\n", cnt >> 1);

    return 0;
}

