#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, s = 0, o;

    cin >> n;

    while (n--) {
        cin >> o;

        s += o;
    }

    cout << (s ? "HARD" : "EASY") << endl;

    return 0;
}

