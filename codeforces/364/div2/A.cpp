#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    int sum = 0;
    int v[101] = {0}, cnt[101] = {0};

    cin >> n;

    for (int i = 0; i < n; i++)
    {
        cin >> v[i];
        sum += v[i];
    }

    for (int i = 0; i < n; i++)
    {
        if (cnt[i]) continue;

        cnt[i] = 1;
        cout << i + 1 << ' ';
        for (int j = 0; j < n; j++)
        {
            if (!cnt[j] and v[i] + v[j] == sum/(n/2))
            {
                cnt[j] = 1;
                cout << j + 1 << endl;
                break;
            }
        }
    }

	return 0;
}

