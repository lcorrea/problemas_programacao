#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long int n, m;
    long long int ans;
    int col[100001] = {0}, lin[100001] = {0};
    long long int freecol, freeline;

    cin >> n >> m;

    ans = n * n;
    freecol = freeline = n;

    for (int i = 0, a, b; i < m; i++)
    {
        int rest = 0;

        cin >> a >> b;

        if (!col[b]) freecol--;
        if (!lin[a]) freeline--;

        col[b] = lin[a] = 1;

        cout << freecol*freeline;

        if (i < m - 1) cout << ' ';
    }

    cout << endl;


	return 0;
}

