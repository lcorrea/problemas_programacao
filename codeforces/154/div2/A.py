#!/usr/bin/python3

data_in = open("input.txt", 'r')
data_out = open("output.txt", 'w')

b, g = map(int, data_in.readline().split())

last = 0

while (b or g):
    if (b > g):
        data_out.write('B')
        b -= 1
        last = 0
    elif (g > b):
        data_out.write('G')
        g -= 1
        last = 1
    else:
        if last:
            data_out.write('B')
            last = 0
            b -= 1
        else:
            data_out.write('G')
            last = 1
            g -= 1

data_out.write('\n')

data_in.close()
data_out.close()

