#!/usr/bin/env python3

def main():
    n, k = map(int, input().split())
    inc, b = [], 0

    for i in map(int, input().split()):
        inc.append(i)
        b += i <= 0

    ans = sum(map(lambda x: -x, inc[:min(b, k)]))
    ans += sum(inc[min(b, k):])
    
    if k > b and ((k - b) & 1):
        if b == n:
            ans += 2*inc[b-1]
        elif b and b < n:
            if abs(inc[b-1]) >= inc[b]):
                ans += -2*inc[b]
            else:
                ans += 2*inc[b-1]
        else:
            ans += -2*abs(inc[0])

    print(ans)

if __name__ == "__main__":
    main()

