#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, k, s = 0;

	scanf ("%d %d", &n, &k);

	for (int i = 0; i < n; i++)
	{
		char str[30], cnt = 0;

		scanf ("%s", str);

		for (int j = 0, len = strlen (str); j < len; j++)
			cnt += str[j] == '4' or str[j] == '7' ? 1 : 0;

		if (cnt <= k) s++;
	}

	printf ("%d\n", s);

	return 0;
}

