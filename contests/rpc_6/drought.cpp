#include <bits/stdc++.h>

using namespace std;

bool clean(deque < pair<int, long long int> > &dq)
{
    if (2 == (int) dq.size())
        return true;

    bool ok = true;

    for (auto it = dq.begin() + 1; it < dq.end() - 1; it++)
    {
        int ha = (it-1)->first;
        int hb = it->first;
        int hc = (it+1)->first;

        if (hb <= ha and hb <= hc)
        {
            auto tmp = it;

            do {

                long long int x = tmp->second;

                tmp = dq.erase(tmp);
                tmp->second += x;

                if (tmp >= dq.end() - 1)
                    break;

                hb = tmp->first;
                hc = (tmp+1)->first;

            } while (hb <= ha and hb <= hc);

            it = tmp;
            ok = false;
        }
    }

    return ok;
}

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        int n;
        long long int a, total = 0;

        scanf ("%lld %d", &a, &n);

        deque < pair <int, long long int> > dq(n+1);

        dq[0].second = 0;
        for (int i = 1; i <= n; i++)
            scanf ("%lld", &dq[i].second);

        for (int i = 0; i <= n; i++)
            scanf ("%d", &dq[i].first);

        while (!clean(dq));

        for (auto it = dq.begin()+1; it < dq.end(); it++)
        {
            int ha = (it-1)->first;
            int hb = it->first;
            long long int x = it->second;
            long long int minh = min(ha, hb);

            total += x*a*minh;
        }

        printf ("%lld\n", total);
    }

    return 0;
}

