#include <bits/stdc++.h>

using namespace std;

//calcular a area de cada casa
//calcular o comprimento dos lados l1 c1
//calcular a area da cidade l1 * c1
//calcular a area do estacionamento = l1*c1 - area de cada casa

double sqrt3;

double areaS (int l)
{
    return 1.00 * l * l;
}

double areaT (int l)
{
    return .25*l*l*sqrt3;
}

int main (void)
{
    sqrt3 = sqrt (3.0);

    int t;

    scanf ("%d%*c", &t);

    while (t--)
    {
        int n;
        int c = 0;
        int totalL = 0;
        int totalL1 = 0;
        int totalL2 = 0;
        double totalA = 0;

        scanf ("%d%*c", &n);

        for (int i = 0; i < n; i++)
        {
            char type;
            int l;

            scanf ("%c %d%*c", &type, &l);
            
            totalL += l;

            if (type == 'S')
                totalA += areaS(l);
            else if (type == 'T')
                totalA += areaT(l);
            else
            {
                totalA += areaS(l);
             
                if (c == 1) totalL1 = totalL;
                if (c == 2) totalL2 = totalL;

                totalL = l;
                c++;
            }
        }

        printf ("%.04lf\n", 1.0 * totalL1 * totalL2 - totalA);
    }

    return 0;
}

