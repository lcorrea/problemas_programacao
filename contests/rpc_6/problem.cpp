#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    scanf ("%d", &t);

    while (t--)
    {
        char n[2000];

        scanf ("%s", n);

        int len = strlen(n);
        bool ok = true;
        int i = 0;

        while (i < len and ok)
            if (n[i] == '0')
                i++;
            else
                ok = false;

        if (ok)
            puts ("1");
        else if (len == 1 and n[len-1] == '1')
            puts ("66");
        else
        {
            int x = n[len-1] - '0';

            if (x == 0 or x == 5)
                puts ("76");
            else if (x == 1 or x == 6)
                puts ("16");
            else if (x == 7 or x == 2)
                puts ("56");
            else if (x == 8 or x == 3)
                puts ("96");
            else if (x == 9 or x == 4)
                puts ("36");
            else;
        }
    }

    return 0;
}

