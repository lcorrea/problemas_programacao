#include <bits/stdc++.h>

using namespace std;

unsigned long long mergeSort (int *v, int n)
{
    if (n==1)
        return 0;

    int na = n/2;
    int nb = n - na;
    int va[na], vb[nb];

    for (int i = 0; i < na; i++)
        va[i] = v[i];

    for (int i = na, j = 0; i < n; i++, j++)
        vb[j] = v[i];

    unsigned long long int inv = mergeSort(va, na);
    inv += mergeSort(vb, nb);

    int i, ia, ib;

    i = ia = ib = 0;

    while (ia < na and ib < nb)
    {
        if (va[ia] <= vb[ib])
            v[i++] = va[ia++];
        else
        {
            inv += na - ia;
            v[i++] = vb[ib++];
        }
    }

    while (ia < na)
        v[i++] = va[ia++];

    while (ib < nb)
        v[i++] = vb[ib++];

    return inv;
}

int main (void)
{
    int n;

    while (scanf ("%d", &n), n)
    {
        int v[n];

        for (int i = 0; i < n; i++)
            scanf ("%d", v + i);

        printf ("%llu\n", mergeSort(v, n));

    }

    return 0;
}

