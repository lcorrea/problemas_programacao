#include <bits/stdc++.h>

using namespace std;

int calc(int i, int j) {

    if (i >= j)
        return 1000000000;


int main (void)
{
    int n;

    while(scanf("%d", &n) != EOF) {
        int v[n], sumA = 0, sumB = 0;

        for (int i = 0; i < n; i++) {
            scanf("%d", v + i);
            if (i < n/2)
                sumA += v[i];
            else
                sumB += v[i];
        }

        int diff = abs(sumA - sumB);

        bool troca = 1;

        while (troca) {
            troca = 0;
            int minDiff = diff;
            int ii, jj;

            for (int i = 0; i < n/2; i++) {
                for (int j = n/2; j < n; j++) {
                    int x = v[i];
                    int y = v[j];
                    int ndiff = abs((sumA-x+y) - (sumB-y+x));

                    if (ndiff < minDiff) {
                       minDiff = ndiff;
                       troca = 1;
                       ii = i, jj = j;
                    }
                }
            }

            if (troca) {
                sumA = sumA - v[ii] + v[jj];
                sumB = sumB - v[jj] + v[ii];
                swap(v[ii], v[jj]);
                diff = abs(sumA - sumB);
            }
        }

        printf("%d\n", diff);

        /*bool dp[sumtotal+1];
        
        memset(dp, 0, sizeof(bool) * (sumtotal+1));

        dp[0] = 1;
        for (int i = 0; i < n;i++)
            for (int j = sumtotal; j >= v[i]; j--)
                dp[j] |= dp[j-v[i]];

        for (int i = sumtotal >> 1; i >= 0; i--) {
            if(dp[i]) {
                printf("%d\n", sumtotal - 2*i);
                break;
            }
        }*/
    }

    return 0;
}

