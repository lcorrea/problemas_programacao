#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    while(scanf("%d%d", &n, &m) != EOF) {
        int cnt[20] = {0};
        for (int i = 0, a; i < n; i++){
            scanf("%d", &a);
            cnt[a]++;
        }
        double ans = 1;
        for (int i = n, j = 0, a; j < m; --i, j++) {
            scanf("%d", &a);
            ans =  ans*(cnt[a]/(double)i);
            cnt[a]--;
        }
        printf("%.03lf\n", ans);
    }

    return 0;
}

