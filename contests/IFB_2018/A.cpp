#include <bits/stdc++.h>

using namespace std;

int player(pair<int, int> v[8], vector <pair<int,int>> &sol, int i, int n)
{
    /*printf("in sol: ");
    
    for (auto k : sol)
        printf("%d|%d ", k.first, k.second);
    putchar('\n');*/

    if (i == n)
        return (int) sol.size();

    vector<pair<int,int>> sol1(sol.begin(), sol.end()), sol2(sol.begin(), sol.end());

    int a = -1, b = -1;

    if (i == 0) {
        sol1.push_back(v[0]);
        
        a = player(v, sol1, i+1, n);
        
        sol2.push_back({v[0].second, v[0].first});

        b = player(v, sol2, i+1, n);

        if (a >= b)
            sol = sol1;
        else
            sol = sol2;

        return (int) sol.size();
    }


    if (v[i].first == sol.back().second) {

        sol1.push_back(v[i]);

        a = player(v, sol1, i+1, n);
    }

    if (v[i].second == sol.back().second) {
        sol2.push_back({v[i].second, v[i].first});

        b = player(v, sol2, i+1, n);
    }

    if (a == -1 and b == -1) 
        return (int) sol.size();

    if (a>=b)
        sol = sol1;
    else
        sol = sol2;

    return (int) sol.size();
}

int main (void)
{
    int n;

    while(scanf("%d", &n) != EOF) {
        pair<int,int> v[8];

        for(int i = 0; i < n; i++) {
            scanf("%d|%d%*c", &v[i].first, &v[i].second);
        }

        vector<pair<int,int>>ans;

        do {
            vector<pair<int,int>> sol;
            int s = player(v, sol, 0, n);
            if ((int) ans.size() < s)
                ans = sol;
        }while(next_permutation(v, v + n) != 0);

        printf("%d\n", (int) ans.size());
        for (int i = 0, len = (int) ans.size(); i < len; i++)
            printf("%d|%d%c", 
                    ans[i].first,
                    ans[i].second,
                    i == len-1 ? '\n' : ' ');
    }

    return 0;
}

