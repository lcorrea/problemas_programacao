#include <bits/stdc++.h>

using namespace std;

#define MOD 1000000007

unsigned long long calc (int n)
{
    unsigned long long f, l = 1;

    for (unsigned long long i = 2; i <= n; i++)
    {
        f = ((l % MOD) * (i%MOD)) % MOD;
        l = f;
    }

    return f;
}

int main (void)
{
    int n;
   
    scanf ("%d", &n);

    int c[n], p[n], minp = INT_MAX, minc = INT_MAX;

    for (int i = 0 ; i < n; i++)
    {
        scanf ("%d", p+i);
        minp = min (minp, p[i]);
    }

    for (int i = 0; i < n; i++)
    {
        scanf ("%d", c+i);
        minc = min(minc, c[i]);
    }

    if (minc >= minp)
        printf ("%d\n", calc(n));
    else
        puts ("0");

    return 0;
}

