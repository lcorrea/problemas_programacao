#include <bits/stdc++.h>

using namespace std;

typedef struct
{
    int t;
    int x;
}Node;

class ST
{
    private:
        vector <Node> st, lazy, v;
        int n;
    
        void check (int p, int l, int r)
        {
            if (lazy[p].x)
            {
                if (l != r)
                {
                    int left = p << 1;
                    int right = left | 1;

                    lazy[left].x = lazy[p];
                    lazy[right].x = lazy[p];
                }

                st[p].x = lazy[p].x;//*(r-l+1);
                lazy[p].x = 0;
                st[p].t = ++g;
            }
        }

        int query (int p, int l, int r, int i, int j)
        {
            printf ("%d %d %d\n", p, l, r);
            check(p, l, r);

            if (i > r or j < l) return 0;
            if (i <= l and j >= r) return st[p].x;

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            Node q1 = query (left, l, mid, i, j);
            Node q2 = query (right, mid + 1, r, i, j);

            if (q1.t >= q2.t) return q1.x;
            else return q2.x;
            
            if (q1 == q2)
                return q1;
            else
                return q1 + q2;
        }

        void update (int p, int l, int r, int i, int j, int val)
        {
            printf ("%d %d %d\n", p, l, r);
            check (p, l, r);

            if (i > r or j < l) return;
            if (i <= l and j >= r)
            {
                st[p] = val;

                if (l != r)
                {
                    int left = p << 1;
                    int right = left | 1;

                    lazy [left] = val;
                    lazy [right] = val;
                }

                return;
            }

            int mid = (l+r) >> 1;
            int left = p << 1;
            int right = left | 1;

            update(left, l, mid, i, j, val);
            update(right, mid+1, r, i, j, val);

            printf ("(%d, %d) = %d %d\n", l, r, st[left], st[right]);

            st[p] = st[left] == st[right] ? st[left] : st[left] + st[right];
        }
    
    public:
        ST(int _n)
        {
            n = _n;

            st.assign(n << 2, {0, 0});
            lazy.assign(n << 2, {0, 0});
        }

        int query (int i, int j)
        {
            return query(1, 0, n-1, i-1, j-1);
        }

        void update(int i, int j, int val)
        {
            update(1, 0, n-1, i-1, j-1, val);
        }
};


typedef struct
{
    int i,j;
    int v;
}Show;

int main (void)
{
    int Q;
    Show like[100001];
    int idx = 1;
    ST st(100000);

    scanf ("%d", &Q);
    for (int i = 0; i < Q; i++)
    {
        int t;

        scanf ("%d", &t);

        if (t == 1)
        {
            scanf ("%d %d %d", &like[idx].i, &like[idx].j, &like[idx].v);
            st.update(like[idx].i, like[idx].j, like[idx].v);
            idx++;
        }
        else if (t == 2)
        {
            //update
            int id;
            scanf ("%d", &id);
            st.update(like[id].i, like[id].j, -like[id].v);
        }
        else
        {
            int a, b;

            scanf ("%d %d", &a, &b);

            printf ("%d\n", st.query(a+1, b+1));
        }
    }

    return 0;
}

