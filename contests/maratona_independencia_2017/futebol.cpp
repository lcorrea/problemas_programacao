#include <bits/stdc++.h>

using namespace std;

bool cmp(pair <int, string> a, pair <int, string> b)
{
    if (a.first == b.first)
        return a.second < b.second;
    return a.first > b.first;
}

int main (void)
{
    int n, anos[100];

    scanf ("%d%*c", &n);

    map <string, int> times;

    for (int i = 0; i < n; i++)
    {
        scanf ("%d%*c", anos + i);

        for (int j = 0; j < 20; j++)
        {
            char time[100];
            char ch;
            int k = 0;
            
            while((ch = getchar()), ch != '-')
                time[k++] = ch;
            time[k-1] = '\0'; getchar();

            int pontos;
            scanf ("%d%*c", &pontos);
            //printf ("**%s %d\n", time, pontos);
            times[time] += pontos;
        }
    }

    vector < pair <int, string> > Ordem;
    map <string, int >::iterator it;

    for (it = times.begin(); it != times.end(); it++)
        Ordem.push_back(make_pair(it->second, it->first));

    sort(Ordem.begin(), Ordem.end(), cmp);

    printf ("Os 10 melhores do ano de ");
    for(int i = 0; i < n; i++)
    {
        printf ("%d", anos[i]);
        if (i + 1 < n - 1)
            printf (", ");
        else
        {
            if (n > 1)
                printf (" e %d:\n", anos[i+1]);
            else
                puts (":");
            break;
        }
    }

    for (int i = 1; i <= 10; i++)
        printf ("%d - %s - %d\n", i, Ordem[i-1].second.c_str(), Ordem[i-1].first);

    return 0;
}

