#include <bits/stdc++.h>

using namespace std;

int bfs(int y, int x, int fy, int fx, char board[20][20])
{
    int movx[8] = {1, 2, 2, 1, -1, -2, -2, -1};
    int movy[8] = {-2, -1, 1, 2, 2, 1, -1, -2};

    queue < pair <int, int> > q;
    int dist[20][20];
    
    for (int i = 0; i < 20; i++)
        for (int j = 0; j < 20; j++)
            dist[i][j] = INT_MAX;

    q.push({y, x});
    dist[y][x] = 0;

    while (!q.empty())
    {
        int uy = q.front().first;
        int ux = q.front().second; q.pop();

        if (board[uy][ux] == 'F')
            break;
        
        board[uy][ux] = 'X';

        for (int i = 0; i < 8; i++)
        {
            int newux = ux + movx[i];
            int newuy = uy + movy[i];
            char ch = board[newuy][newux];

            if (ch == '.' or ch == 'F')
            {
                q.push({newuy, newux});
                dist[newuy][newux] = dist[uy][ux] + 1;
            }
        }
    }

    return dist[fy][fx];
}

int main (void)
{
    char board[20][20];

    for (int i = 0; i < 20; i++)
        for (int j = 0; j < 20; j++)
            board[i][j] = 'X';

    int y, x, fy, fx;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            char ch;
            ch = board[10+i][10+j] = getchar();

            if (ch == 'I')
                y = i+10, x = j+10;
            else if (ch == 'F')
                fy = i+10, fx = j+10;
        }
        getchar();
    }

    int d = bfs(y, x, fy, fx, board);
    if (d == INT_MAX)
        puts ("Impossivel ir de I a F");
    else
        printf ("Numero minino de movimentos: %d\n", d);

    return 0;
}

