#include <bits/stdc++.h>

using namespace std;

bool cmp (pair <int, int> a, pair <int, int> b)
{
    return a.second > b.second;
}

int main (void)
{
    int n, d;
    unsigned long long max_f = 0;
    pair <unsigned long long int, unsigned long long int> friends[100001];

    cin >> n >> d;

    for (int i = 0; i < n; i++)
        cin >> friends[i].first >> friends[i].second;

    sort (friends, friends+n, cmp);

    int i = 0;
    while (i < n)
    {
        unsigned long long int min_m = friends[i].second;
        unsigned long long int s = friends[i].first;
        i++;

        while (i < n and abs (friends[i].second - min_m) < d)
        {
            s += friends[i].first;
            min_m = min (friends[i].second, min_m);
            i++;
        }

        max_f = max (max_f, s);
    }

    cout << max_f << endl;

    return 0;
}

