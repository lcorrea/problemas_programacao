#include <bits/stdc++.h>

using namespace std;

typedef pair <int, int> ii;
typedef vector <int> vi;
typedef vector < vector <ii> > graph;

vi taken;
graph cities;

void process (int vtx, priority_queue <ii> &pq)
{
    taken[vtx] = 1;

    for (int i = 0, sz = cities[vtx].size(); i < sz; i++)
    {
        ii v = cities[vtx][i];

        if (!taken[v.first])
            pq.push(ii(-v.second, -v.first));
    }
}

int main (void)
{
    int n, m;

    while (scanf ("%d%d", &m, &n), n and m)
    {
        priority_queue <ii> pq;
        cities.clear();
        cities.resize(m+1);
        taken.assign (m+1, 0);
        int cost = 0;

        for (int i = 0, u, v, p; i < n; i++)
        {
            scanf ("%d%d%d",&u, &v, &p);

            cities[u].push_back (ii(v, p));
            cities[v].push_back (ii(u, p));
        }

        process (0, pq);

        while (!pq.empty())
        {
            ii front = pq.top(); pq.pop();

            int u = -front.second, w = -front.first;

            if (!taken[u])
                cost += w, process(u, pq);
        }

        printf ("%d\n", cost);
    }

    return 0;
}

