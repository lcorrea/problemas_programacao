#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    pair <int, int> v[m];

    for (int i = 0; i < m; i++)
        scanf ("%d", &v[i].first), v[i].second = i+1;

    sort (v, v+m);

    vector <int> ladoa, ladob;
    int a = n, b = n, last = 1;

    for (int i = 0; i < m;  i++)
    {
        if (a >= v[i].first and last == 1)
        {
            ladoa.push_back(v[i].second);
            a -= v[i].first;
        }
        
        if (b >= v[i].first and last == 0)
        {
            ladob.push_back(v[i].second);
            b -= v[i].first;
        }

        last = last ^ 1;
    }

    printf ("%d\n", (int)ladoa.size() + (int)ladob.size());

    printf ("Lado A:");
    for (int i = 0; i < (int) ladoa.size(); i++)
        printf (" %d", ladoa[i]);
    putchar('\n');
    printf ("Lado B:");
    for (int i = 0; i < (int) ladob.size(); i++)
        printf (" %d", ladob[i]);
    putchar('\n');

    return 0;
}

