#include <bits/stdc++.h>

using namespace std;

int dp[50000][2], v[100001];

int calc(int i, int j, int r, int c, int n)
{
    if (n < r) return dp[r][c] = 0;
    if (dp[r][c] != -1) return dp[r][c];

    int direita = calc(i, j-1, r+1, 0, n) + v[j]; printf ("direita: %d\n", direita);
    int esquerda = calc(i+1, j, r+1, 1, n) + v[i];

    return dp[r][c] = max(direita, esquerda);
}

int main (void)
{
    int n;

    while (EOF != scanf ("%d", &n))
    {
        memset(dp, -1, sizeof(dp));

        for (int i = 0, a; i < n; i++)
            scanf ("%d", &a), v[i] = a + 100;

        printf ("%d\n", calc(0, n-1, 1, 0, n/2) - n/2*100);
    }

    return 0;
}

