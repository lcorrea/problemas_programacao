#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, caso = 1;

    set <unsigned long long int> perf;

    for (unsigned long long int i = 1; i <= 100000; i++)
        perf.insert(i*i);

    while (scanf ("%d", &n), n)
    {
        unsigned long long int a = 0; 
        for (auto it = perf.begin(); n >= *it; it++)
            if (perf.find(n-*it) != perf.end())
            { 
                a = *it;
                break;
            }

        printf ("Caso %d: ", caso++);
        if (a == 0)
            puts ("metragem invalida");
        else
            printf ("%llu %llu\n", (unsigned long long int)sqrt(a), (unsigned long long int) sqrt(n - a));
    }


    return 0;
}

