#include <bits/stdc++.h>

using namespace std;

int main()
{
    int t, caso = 1;
    scanf("%d", &t);

    while(t--)
    {
        double d, x;
        scanf("%lf", &d);
        double notas[7];

        for (int i = 0; i < 7; i++)
            scanf ("%lf", notas+i);
        sort(notas, notas+7);

        double sum = 0;

        for (int i = 1; i <= 5; i++)
            sum += notas[i];

        sum /= 5.0;

        printf ("Caso %d: %.1lf\n", caso++, sum*d);

    }

    return 0;
}
