#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n;
    char vogal[5] = {'a', 'e', 'i', 'o', 'u'};

    while (EOF != scanf ("%d%*c", &n))
    {
        int cnt = 0;
        string str;

        getline(cin, str);

        for (int i = 0; str[i]; i++)
        {
            putchar(str[i]);

            if (isalpha(str[i]))
            {
                int xx = 0;
                for (int j = 0; j < 5; j++)
                    xx += vogal[j] == str[i];

                if (xx == 0)
                    putchar(vogal[cnt]), cnt = (cnt + 1)%5;
            }
        }

        putchar('\n');
    }

    return 0;
}
