#include <bits/stdc++.h>

using namespace std;

set<int> v;

void backtrack (int i, int x, int n)
{
    if (x <= 0)return;
    if (x >= n) return;

    v.insert(x);

    backtrack(i+1, x + (2*i - 1), n);
    backtrack(i+1, x - (2*i - 1), n);
}

int main (void)
{
    int n;
    int last = 0;

    while (scanf ("%d", &n), n)
    {
        backtrack(2, 1, n);

        putchar('\n');
        for (auto k : v)
            printf ("%d\n", k);
        
        printf ("\n diff: %+d\n", (int) v.size() - last);
        last = (int)v.size();
        puts("\n------\n");
        v.clear();
    }

    return 0;
}

