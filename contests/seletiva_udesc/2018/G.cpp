#include <bits/stdc++.h>

using namespace std;

int teste(int a, int b, int c)
{
    if (c < a)
        swap(c, a);

    if (c < b)
        swap(c, b);

    return (a+b) > c;
}

int main (void)
{
    int palitos[5], ans = 0;

    for (int i = 0; i < 5; i++)
        scanf("%d", palitos+i);

    for (int i = 0; i < 5; i++)
        for (int j = i+1; j < 5; j++)
            for (int k = j+1; k < 5; k++)
                ans += teste(palitos[i], palitos[j], palitos[k]);

    printf("%d\n", ans);

    return 0;
}

