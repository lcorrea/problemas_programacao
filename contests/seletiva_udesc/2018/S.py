n = int(input())

for caso in range(n):
    n = n - 1
    a, b = map(int, input().split())
    ans = (a+b)*(abs(b-a)+1)//2
    print("Caso #" + str(caso + 1) + ": " + str(ans))


