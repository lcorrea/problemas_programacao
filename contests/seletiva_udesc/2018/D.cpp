#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    while (scanf("%d", &n), n) {
        map<int, set<int>> list;
        vector< vector<int> > adjlist(n);
        int v[n];

        for (int i = 0; i < n; i++) {
            scanf("%d", v + i);
            list[i-v[i]].insert(i);
        }

        for (int i = 0; i < n; i++) {
            
            int s = i + v[i];

            for (auto k : list[s])
                adjlist[i].push_back(k), adjlist[k].push_back(i);
        }

        queue<int> q;
        int dist[n];
        memset(dist, -1, sizeof(int) * n);

        dist[0] = 0;
        q.push(0);

        while(!q.empty()) {
            int u = q.front(); q.pop();

            for (auto w: adjlist[u]) {
                if (dist[w] == -1 or dist[w] > dist[u] + 1) {
                    dist[w] = dist[u] + 1;
                    q.push(w);
                }
            }
        }

        int ans = 0;

        for (int i = 0; i < n; i++)
            if (dist[i] != -1)
                ans = i;

        printf("%d\n", ans);
    }

    return 0;
}

