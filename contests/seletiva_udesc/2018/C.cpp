#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    long long int n;

    scanf("%lld", &n);

    printf("%lld\n", n > 1 ? n*(n-1) : 0ll);

    return 0;
}

