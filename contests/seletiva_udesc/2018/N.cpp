#include <bits/stdc++.h>

#define INF (1000000000)

using namespace std;
typedef pair < pair<int, int>, int> Peso;
typedef vector < vector < pair<int,int> > > Graph;
int n;
int tb[601][10001];

int calc(int x, int id, vector<int> &conf, Graph &graph, int tb[601][10001]) {
    printf("%d\n", id);

    if (id == n)
        return tb[x][id] = 0;

    if (tb[x][id] != -1)
        return tb[x][id];

    int ans = INF;

    for (auto v : graph[id]) {
        if (conf[id] and v.second <= 600) {
            ans = 1 + calc(600 - v.second, v.first, conf, graph, tb);
        }
       
        if (x >= v.second)
            ans = min(ans, calc(x-v.second, v.first, conf, graph, tb));
    }

    return tb[x][id] = ans;
}



int main (void)
{

    while (scanf("%d", &n), n) {
        vector<int> conf(n+1, 0);
        Graph graph(n+1);
        int h;
        scanf("%d", &h);

        for (int i = 0, x; i < h; i++)
            scanf("%d", &x), conf[x] = 1;

        int m;

        scanf("%d", &m);

        for (int i = 0,a,b,p; i < m; i++) {
            scanf("%d %d %d", &a, &b, &p);
            graph[a].push_back({b, p});
            graph[b].push_back({a, p});
        }

        /*memset(tb, -1, sizeof(tb));

        int ans = calc(600, 1, conf, graph, tb);

        printf("%d\n", ans == INF ? -1 : ans);
    }*/

        vector<pair<int, int>> dist(n+1, make_pair(INF, INF));
        priority_queue<Peso, vector<Peso>, greater <Peso> > pq;

        dist[1] = {0, 0};

        pq.push(make_pair(dist[1], 1));

        while (!pq.empty()) {
            int u = pq.top().second;
            pair<int,int> d = pq.top().first;
            pq.pop();

            //if (d > dist[u]) continue;

            dist[u] = min(d, dist[u]);

            for (auto v : graph[u]) {
                pair<int,int> dv = make_pair(INF, INF); //dist[v.first];
                int travel = v.second + dist[u].second;
                
                if (travel > 600 and conf[u]) {
                    if (v.second <= 600) {
                        dv.first = dist[u].first + 1;
                        dv.second = v.second;
                    }
                }
                
                if (travel <= 600) {
                    dv.first = dist[u].first;
                    dv.second = travel;
                    pq.push(make_pair(dv, v.first));

                    if (conf[u]) {
                        dv.first = dist[u].first + 1;
                        dv.second = v.second;

                        pq.push(make_pair(dv, v.first));
                    }

                }

                //if (dv < dist[v.first]) {
                    //dist[v.first] = dv;
                    pq.push(make_pair(dv, v.first));
                //}
            }
        }

        printf("%d\n", dist[n].first == INF ? -1 : dist[n].first);
    }

    return 0;
}

