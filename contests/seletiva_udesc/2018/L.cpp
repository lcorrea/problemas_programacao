#include <bits/stdc++.h>

using namespace std;

bool verifica(string dig, char d, int n)
{
    int s = 0;

    for (int i = n+1, j = 0; i >= 2; --i, j++) {
        s += (dig[j] - '0')*i;
    }

    int r = s % 11;
    int a = d - '0';

    if (r < 2)
        return a == 0;
    
    return (11 - r) == a;
}

int main (void)
{
    int n;
    set<string> cpf;

    scanf("%d", &n);

    while(n--) {
        char dig[20], ver[5];

        scanf("%s %s", dig, ver);
        
        string c = dig;
        
        c += ver;

        if (cpf.find(c) != cpf.end())
            puts("Clonado");
        else if (verifica(c, ver[0], 9) and verifica(c, ver[1], 10)) {
            cpf.insert(c);
            puts("Sucesso");
        }
        else
            puts("Invalido");
    }

    return 0;
}

