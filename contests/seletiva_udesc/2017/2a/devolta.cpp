#include <bits/stdc++.h>

using namespace std;

long long int calc(int n, int w[366])
{
    if (n==1)
        return 100;
    
    long long int budget = 100;
    int i = 0;

    while (i < n - 1)
    {
        while ((i < n - 1) and w[i] <= budget and (w[i+1] <= w[i]))
            i++;

        if (i == n-1)
            break;

        int qty = (int) min (budget/w[i], 100000LL);

        budget -= qty*w[i];
        i++;

        while ((i < n) and (w[i] >= w[i-1])) i++;

        budget += w[i-1]*qty;
    }

    return budget;
}

int main (void)
{
    int n, w[366];

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
        scanf ("%d", w + i);

    printf ("%lld\n", calc(n, w));

    return 0;
}

