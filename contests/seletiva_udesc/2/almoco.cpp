#include <iostream>

using namespace std;

int main(void)
{
	unsigned int n, t = 1;
	unsigned int d, pd, p, aux;
	string caminho;

	cin >> n;

	while(n--)
	{
		p = 0;
		cin >> pd >> d;

		cin >> caminho;

		for(unsigned int k = 0; k < pd-1; )
		{
			aux = d+1;

			while(caminho[k + aux] == 'X')
				aux--;
			if(aux == 0)
			{
				p = 0;
				break;
			}
			k += aux;

			p++;
		}

		cout << "Caso #" << t << ": " << p << endl;

		t++;

	}

	return 0;
}

