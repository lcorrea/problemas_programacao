#include <iostream>
#include <algorithm>
#include <utility>

using namespace std;

bool finished = false;
unsigned int max_ele = 0, maxS = 0;

pair<unsigned int, unsigned int> itens[500], sol[250];

bool comp(pair<unsigned int, unsigned int> a, pair<unsigned int, unsigned int> b)
{
	return a.second < b.second;
}

unsigned int select_candidates(unsigned int k, unsigned int pos, pair<unsigned int, unsigned int> c[])
{
	unsigned int nc = 0;

	for(unsigned int x = pos - 1; (x+1)/2 >= max_ele/2 - k; x--)
	{
		if(x == 2)
		{
			c[nc++] = *max_element(itens, itens + x + 1, comp);
			break;
		}
		else
			c[nc++] = itens[x];
	}

	return nc;
}

void backtracking(unsigned int k)
{
	pair<unsigned int, unsigned int> c[250];
	unsigned int nc;

	if(k == max_ele/2)
	{
		unsigned int m = 0;
		
		for(unsigned int i = 0; i < max_ele/2; i++)
			m += sol[i].second;

		maxS = max(maxS, m);
			
	}
	else
	{
		nc = select_candidates(k, sol[k-1].first, c);

		for(unsigned int i = 0; i < nc; i++)
		{
			sol[k] = c[i];
			backtracking(k+1);

			if(finished == true)
				return;
		}
	}
}

int main(void)
{
	unsigned int n, qnt, item;

	pair<unsigned int, unsigned int> c[250];

	cin >> n;
	
	for(unsigned int i = 1; i <= n; i++)
	{
		cin >> max_ele;

		maxS = 0;

		finished = false;

		for(unsigned int j = 0; j < max_ele; j++)
		{
			cin >> item;
			itens[j] = pair<unsigned int, unsigned int>(j, item);
		}

		if(max_ele < 4)
			maxS = max_element(itens, itens + max_ele, comp)->second;
		else
		{
			sol[0] = itens[max_ele-1];

			if(max_ele%2)
				if(itens[max_ele-2].second > sol[0].second)
					sol[0] = itens[max_ele-2];
										
			backtracking(1);
		}

		cout << "Maratona #" << i << ": " << maxS << endl;
			
	}

	return 0;
}


