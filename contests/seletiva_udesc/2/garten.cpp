#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	unsigned int n, maratona, qnt, item, pos;

	vector<unsigned int> sol(250, 0);
	
	cin >> n;

	for(maratona = 1; maratona <= n; maratona++)
	{
		cin >> qnt;

		fill(sol.begin(), sol.end(), 0);
		
		for(unsigned int i = 0; i < qnt; i++)
		{
			cin >> item;

			if(i < 3)
			{
				sol[0] = max(sol[0], item);
			}
			else
			{
				pos = i/2;
				pos -= ((i%2)?(0):(1));
				sol[pos] = max(sol[pos], item + sol[pos-1]);
				sol[0] = max(sol[0], item);
			}

			for(unsigned int j = 0; j < qnt/2; j++)
				cout << sol[j] << ' ' << endl;
			cout << endl;

		}

		cout << "Maratona #" << maratona << ": " << sol[qnt/2 -1] << endl;
	}

	return 0;
}

