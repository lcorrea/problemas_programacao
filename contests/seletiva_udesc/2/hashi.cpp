#include <iostream>
#include <algorithm>
#include <set>

using namespace std;

int main(void)
{
	unsigned int n, c, d, caso = 0;
	unsigned int tc[50];
	unsigned int aluno[200];
	unsigned int num = 0;
	set<unsigned int> minT;

	cin >> n;

	while(n--)
	{
		minT.clear();
		cin >> c;

		caso++;

		for(unsigned int k = 0; k < c; k++)
			cin >> tc[k];

		for(unsigned int k = 0; k < c*4; k++)
			cin >> aluno[k];

		sort(aluno, aluno+c*4);
		sort(tc, tc+c);

		num = 3;

		for(unsigned int k = 0; k < c; k++)
		{
			minT.insert(aluno[num] + tc[c-k-1]);
			num += 4;
		}

		cout  << "CASO #" << caso << ": " << *max_element(minT.begin(), minT.end());

		cout << endl;
	}
	
	return 0;
}

