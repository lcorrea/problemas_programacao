#include <iostream>

using namespace std;

int main(void)
{
	int n, times = 0, pontos, jogos;

	cin >> n;

	while(n--)
	{
		times++;

		cin >> jogos >> pontos;

		cout << "Time #" << times << '\n';
		cout << "Jogos: " << jogos << '\n';
		cout << "Pontos: " << pontos << '\n';
		cout << "Possiveis resultados:\n";

		int aux = pontos/3;
		int aux2 = pontos%3;
		
		while(aux + aux2 <= jogos && aux >= 0) 
		{
			cout << aux << '-' << aux2 << '-' << jogos - aux - aux2 << '\n';
			aux--;
			
			aux2 = pontos - aux*3;
		}

		cout << endl;
	}

	return 0;
}

