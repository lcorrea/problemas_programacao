#include <iostream>

using namespace std;

int main(void)
{
	float a, g, ra, rg;

	cin >> a >> g >> ra >> rg;

	while(!cin.eof())
	{
		if(a/ra < g/rg)
			cout << 'A';
		else
			cout << 'G';

		cout << endl;

		cin >> a >> g >> ra >> rg;
	}
	
	return 0;
}

