#include <iostream>
#include <algorithm>
#include <map>
#include <utility>

using namespace std;

bool comp(pair<unsigned int, unsigned int> a, pair<unsigned int, unsigned int> b)
{
	return a.second < b.second;
}

int main(void)
{
	unsigned int n, d;
	map<unsigned int, unsigned int> nums;

	cin >> n;

	while(n)
	{
		nums.clear();
		for(unsigned int i = 0; i < n; i++)
		{
			cin >> d;
	
			if(nums.find(d) != nums.end())
				nums[d] += 1;
			else
				nums[d] = 1;
		}	

		pair<unsigned int, unsigned int> a =  *max_element(nums.begin(), nums.end(), comp);
		
		cout << a.second <<  endl;

		cin >> n;
	}

	return 0;
}

