#include <bits/stdc++.h>

using namespace std;

int n, r1, r2, c1, c2;

int bfs (int a, int b)
{
  queue< pair <int, int> > q;
  int dist[30][30];

  memset (dist, -1, sizeof(dist));

  q.push (make_pair(a, b));
  dist[a][b] = 0;

  while (!q.empty())
  {
    int y = q.front().first;
    int x = q.front().second;

    q.pop();

    if (x == c2 and y == r2)
      return dist[y][x];

    const int dy[8] = {-2, -2, 2, 2, -1, -1, 1, 1};
    const int dx[8] = {-1, 1, -1, 1, -2, 2, -2, 2};

    for (int i = 0; i < 8; i++)
    {
      int newx = x + dx[i];
      int newy = y + dy[i];

      if (newx >= 1 and newx <= n)
        if (newy >= 1 and newy <= n)
          if (dist[newy][newx] == -1)
          {
            dist[newy][newx] = 1 + dist[y][x];
            q.push (make_pair(newy, newx));
          }
    }
  }

}

int main (void)
{
  int t;

  scanf ("%d", &t);

  for (int i = 1; i <= t; i++)
  {
    scanf ("%d%d%d%d%d", &n, &r1, &c1, &r2, &c2);

    printf ("Caso #%d: %d\n", i, bfs (r1, c1));
  }

  return 0;
}

