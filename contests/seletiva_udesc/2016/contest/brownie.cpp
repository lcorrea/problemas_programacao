#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int n;

  scanf ("%d", &n);

  for (int k = 1; k <= n; k++)
  {
    int alunos, pedacos, grupos;

    scanf ("%d%d", &alunos, &pedacos);
    scanf ("%d", &grupos);

    printf ("Seletiva #%d:\n", k);
    for (int i = 0, m; i < grupos; i++)
    {
      scanf ("%d", &m);

      while (pedacos <= m)
        pedacos <<= 1;
      pedacos -= m;

      printf ("%d\n", pedacos);
    }
    putchar ('\n');
  }

  return 0;
}

