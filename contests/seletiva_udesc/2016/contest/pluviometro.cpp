#include <bits/stdc++.h>

using namespace std;
#define PI 3.14159265358979
int main (void)
{
  int t;
  double l, r;

  scanf ("%d", &t);

  while (t--)
  {
    scanf ("%lf%lf", &l, &r);

    if (2.0*r <= l)
      printf ("%.02lf\n", PI*r*r);
    else if (l*sqrt(2.0) <= 2.0*r)
      printf ("%.02lf\n", l*l);
    else
    {
      double h = l/2.0; // altura do triangulo isosceles (setor)
      double x = sqrt ((double) r*r - h*h); // 0,5 * base do triangulo
      double teta = acos (h/(double)r) * 2.0; // angulo do setor
      double circle = PI*r*r; //area da circunferencia
      double diff = 4.0 * ((teta * r * r / 2.0) - x*h); //area fora do quadrado
      double ans = circle - diff; //area coberta

      printf ("%.02lf\n", ans);
    }
  }

  return 0;
}

