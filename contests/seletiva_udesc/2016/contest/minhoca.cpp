#include <bits/stdc++.h>

using namespace std;

char tab[16][16];
int m, n;
int print_path (int mode, int *x, int *y)
{
  //printf ("mode: %d x: %d y: %d\n", mode, *x, *y);
  int cnt = 0;
  int startx = *x;
  int starty = *y;
  const int incx[4] = {1, 0, -1, 0};
  const int incy[4] = {0, 1, 0, -1};

  while (*x >= 0 and *x < n and *y >= 0 and *y < m and tab[*y][*x])
  {
    cnt++;
    putchar(tab[*y][*x]);
    tab[*y][*x] = 0;

    *x += incx[mode];
    *y += incy[mode];
  }

  if (cnt)
  {
    *x -= incx[mode];
    *y -= incy[mode];

    *x += incx[(mode + 1) % 4];
    *y += incy[(mode + 1) % 4];
    putchar ('\n');

    //printf ("mode %d start with x: %d y: %d\n", (mode + 1) % 4, *x, *y);
  }
  return cnt;
} 

int main (void)
{
  int matriz = 1;

  while (scanf ("%d %d%*c", &m, &n), m and n)
  {

    for (int i = 0; i < m; i++)
    {
      for (int j = 0; j < n; j++)
        scanf ("%c%*c", &tab[i][j]);
    }

    printf ("Matriz #%d:\nOriginal:\n", matriz++);

    for (int i = 0; i < m; i++)
    {
      printf ("%c", tab[i][0]);
      for (int j = 1; j < n; j++)
        printf (" %c", tab[i][j]);
      putchar ('\n');
    }
    puts ("Minhoca:");

    int next = 0;
    int x = 0, y = 0;

    while (print_path(next % 4, &x, &y))
      next++;
    putchar ('\n');
  }

  return 0;
}

