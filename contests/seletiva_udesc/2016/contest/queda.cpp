#include <bits/stdc++.h>

using namespace std;

int process (char preco[4], int p, char c)
{
  int num;

  if (p == -1)
  {
    sscanf (preco, "%d", &num);
    return num;
  }

  preco[p] = c;
  sscanf (preco, "%d", &num);
  return num;
}

int main (void)
{
  int t;

  scanf ("%d%*c", &t);

  for (int o = 1; o <= t; o++)
  {
    char preco1[4];
    char preco2[4];
    char preco3[4];
    int m1 = -1, m2 = -1, m3 = -1, a, b, c;

    scanf ("%s", preco1);
    scanf ("%s", preco2);
    scanf ("%s", preco3);

    for (int i = 0; i < 3; i++)
    {
      if (preco1[i] == '-')
        m1 = i;
      if (preco2[i] == '-')
        m2 = i;
      if (preco3[i] == '-')
        m3 = i;
    }

    bool found = false;
    for (char i = '0'; i <= '9' and !found; i++)
    {
      if (i == '0' and m1 == 0)continue;
      if (i == '1' and m1 == 0) continue;

      a = process(preco1, m1, i);

      for (char k = '0'; k <= '9' and !found; k++)
      {
        if (k == '0' and m2 == 0)continue;
        if (k == '1' and m2 == 0) continue;

        b = process(preco2, m2, k);

        for (char l = '0'; l <= '9' and !found; l++)
        {
          if (l == '0' and m3 == 0)continue;
          if (l == '1' and m3 == 0) continue;

          c = process(preco3, m3, l);

          if (a < b and b < c)
            found = true;
        }
      }
    }
    if (m1 >= 0) preco1[m1] = '-';
    if (m2 >= 0) preco2[m2] = '-';
    if (m3 >= 0) preco3[m3] = '-';

    printf ("Posto de Gasolina #%d:\n", o);
    printf ("   Entrada: %s %s %s\n", preco1, preco2, preco3);
    printf ("   Saida: %d %d %d\n\n", a, b, c);

  }

  return 0;
}

