#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > AdjList;

vector <int> visit;
AdjList domino;

int n;

void dfs (int v)
{
  visit[v] = 1;
  n--;

  for (int i = 0; i < (int) domino[v].size(); i++)
  {
    if (visit[domino[v][i]] == 0)
      dfs (domino[v][i]);
  }
}

int main (void)
{

  int t;

  scanf ("%d", &t);

  while (t--)
  {

    int m;
    scanf ("%d%d", &n, &m);

    visit.assign(n, 0);
    domino.clear();
    domino.resize(n);

    for (int i = 0; i < m; i++)
    {
      int x, y;

      scanf ("%d%d", &x, &y);
      domino[x-1].push_back (y - 1);
    }

    int cnt = 0;

    for (int i  = 0, sz = n; i < sz; i++)
    {
      if (visit[i]==0 and domino[i].size())
      {
        cnt++;
        dfs (i);
      }
    }
    printf ("%d\n", cnt + n);
  }

  return 0;
}

