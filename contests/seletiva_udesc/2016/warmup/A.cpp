#include <bits/stdc++.h>

using namespace std;

int main (void)
{
  int h1, m1, h2, m2;

  while (scanf ("%d%d%d%d", &h1, &m1, &h2, &m2), h1 or m1 or h2 or m2)
  {
    m1 += h1*60;
    m2 += h2*60;

    if (m2 < m1)
      m2 += 24*60;

    printf ("%d\n", m2 - m1);
  }

  return 0;
}

