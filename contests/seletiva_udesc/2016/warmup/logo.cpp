#include <bits/stdc++.h>
#include <cmath>

using namespace std;

int main (void)
{
  int n;
  int Case;

  scanf ("%d", &Case);

  while (Case--)
  {
    double x = 0.0, y = 0.0, teta = 0.0;
    char cmd[3];
    int param;

    scanf ("%d", &n);

    while (n--)
    {
      scanf ("%s %d", cmd, &param);

      if (*cmd == 'f')
      {
        y = y + sin(teta)*param;
        x = x + cos(teta)*param;
      }

      if (*cmd == 'b')
      {
        teta += M_PI;
        y = y + sin (teta)*param;
        x = x + cos (teta)*param;
      }

      if (*cmd == 'r')
        teta += (-param)*M_PI/180.0;

      if (*cmd == 'l')
        teta += param*M_PI/180.0;
    }

    printf ("%d\n", (int) round (hypot(-x, -y)));

  }

  return 0;
}

