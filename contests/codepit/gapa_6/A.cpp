#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    char a[10001], b[10001];

    scanf ("%d", &n);

    while (n--)
    {
        int cnt = 0;
        scanf ("%s %s", a, b);

        for (int i = 0; a[i]; i++)
        {
            if (a[i] > b[i])
                cnt += 26 + b[i]-a[i];
            else
                cnt += b[i]-a[i];
        }

        printf ("%d\n", cnt);
    }

    return 0;
}

