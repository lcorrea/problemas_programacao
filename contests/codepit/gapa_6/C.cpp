#include <bits/stdc++.h>

using namespace std;

bitset<1000010> bs;
bool aux[1000010] = {0};
int sum[1000010] = {0};

void sieve()
{
    bs.set();
    bs[0] = false;
    bs[1] = false;

    for (int i = 2; i < 1000010; i++)
        if (bs[i])
            for (int j = i*2; j < 1000010; j += i)
                bs[j] = false;

    for (int i = 2; i < 1000005; i++)
        sum[i] = sum[i-1] + (bs[i] and (bs[i+2] or bs[i-2]));
}

int main (void)
{
    int n;

    sieve();
    scanf ("%d", &n);

    while (n--)
    {
        int a, b;

        scanf ("%d %d", &a, &b);

        if(a > b) swap(a, b);

        printf ("%d\n", sum[b] - sum[a-1]);
    }

    return 0;
}

