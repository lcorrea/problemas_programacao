#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int Case = 1;
	double in[3], ans[3];
	const double inv[3][3] = {
		{9.66667,   -0.33333,   -0.33333},
		{-11.00000,    3.00000,   -1.00000},
		{2.00000,   -2.00000,    2.00000}};

	while (EOF != scanf ("%lf %lf %lf", in, in+1, in+2) && in[0]+in[1]+in[3] > 0.000009)
	{
		ans[0] = ans[1] = ans[2] = 0.0;

		ans[0] = in[0]*inv[0][0] + in[1]*inv[0][1] + in[2]*inv[0][2] + .00009;
		ans[1] = in[0]*inv[1][0] + in[1]*inv[1][1] + in[2]*inv[1][2] + .00009;
		ans[2] = in[0]*inv[2][0] + in[1]*inv[2][1] + in[2]*inv[2][2] + .00009;

        printf ("Caso #%d: %d Especial, %d Predileta e %d Sortida\n", Case++,
                (int) floor(ans[0]), (int) floor(ans[1]), (int) floor(ans[2]));
	}


	return 0;
}

