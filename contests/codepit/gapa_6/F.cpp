#include <bits/stdc++.h>

using namespace std;

int calc_lps(char pat[100001], int n, int lps[100001])
{
    int i = 1, j = 0;
    lps[0] = 0;

    for (i = 1,  j = 0; i < n;)
        if (pat[i] == pat[j])
            lps[i] = j + 1, ++i, ++j;
        else
            if (j == 0)
                lps[i] = 0, ++i;
            else
                j = lps[j-1];

    for (i = 0, j = 0; i < n; i++)
        if (!lps[i])
            j = i;

    return j;
}

int main (void)
{
    int lps[100001];
    char str[100001];

    while (scanf ("%s", str), str[0] != '*')
    {
        int len = strlen(str);
        int end_pattern = calc_lps(str, len, lps) + 1;

        if (lps[len-1] and ((lps[len-1] % end_pattern) == 0))
            printf ("%d\n", len / end_pattern);
        else
            puts ("1");
    }

    return 0;
}

