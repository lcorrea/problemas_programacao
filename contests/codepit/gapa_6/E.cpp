#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, s;
    long long int a, b, c;

    while (EOF != scanf ("%d", &n) and n)
    {
        unordered_map<long long int, int> x;

        for (int i = 0; i < n; i++)
            scanf ("%lld", &a), x[a]++;

        s = 0;
        for (auto i : x)
            if (i.second & 1)
                if (s == 0)
                    b = i.first, s++;
                else if (s == 1)
                    c = i.first;

        printf ("%lld %lld\n", min(c, b), max(b, c));
    }

    return 0;
}

