#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[65];

    scanf ("%s", str);

    int len = strlen(str);
    int x = 1 << len;

    reverse(str, str+strlen(str));
    
    for (int i = 0; str[i]; i++)
        x |= ((str[i] == '7') << i);
    
    printf ("%d\n", x-1);

    return 0;
}

