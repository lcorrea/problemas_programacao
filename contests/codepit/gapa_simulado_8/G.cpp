#include <bits/stdc++.h>

using namespace std;

#define UNVISITED (-1)
#define VISITED 0
#define EXPLORED 1

typedef vector <vector <int>> AdjList;

AdjList dep(100001);
vector <int> ts, visited(100001, -1);
int courses[100001], n, k;

int dfs (int u)
{
    visited[u] = VISITED;

    for (int i = 0, len = dep[u].size(); i < len; i++) {

        int v = dep[u][i];

        if (visited[v] == VISITED)
            return 0;

        if (visited[v] == UNVISITED)
            if (!dfs(v))
                return 0;
    }

    visited[u] = EXPLORED;
    ts.push_back(u);

    return 1;
}

int main (void)
{
    scanf("%d%d", &n, &k);

    for (int i = 0, course; i < k; i++) {

        scanf("%d", &course);
        courses[i] = course - 1;
    }

    for (int i = 0, deps; i < n; i++) {

        scanf("%d", &deps);

        while (deps--) {

            int course;

            scanf("%d", &course);

            dep[i].push_back(course - 1);
        }
    }

    int notcycle = 1;
    for (int i = 0, len = k; notcycle and i < k; i++)
        if (visited[courses[i]] == UNVISITED)
            notcycle = dfs(courses[i]);

    if (notcycle) {

        printf("%d\n", (int) ts.size());

        for (int i = 0, len = ts.size(); i < len; i++)
            printf("%d%c", 1 + ts[i], i != len - 1 ? ' ' : '\n');
    }
    else
        puts("-1");

    return 0;
}

