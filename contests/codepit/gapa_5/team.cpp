#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    int cnt = 0;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        int x = 0;

        for (int a, b = 0; b < 3; b++)
            scanf ("%d", &a), x += a;

        cnt += x >= 2;
    }

    printf ("%d\n", cnt);

    return 0;
}

