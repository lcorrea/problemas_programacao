#include <bits/stdc++.h>

using namespace std;

#define MOD 303700049LL

int n;
unsigned long long int K;
long long int out[101][101] = {0};
long long int ans[101][101] = {0};
long long int base[101][101] = {0};

void solve()
{
    for (register int i = 0; i < n; i++)
        for (register int j = 0; j < n; j++)
            ans[i][j] = i==j;

    while (K > 0)
    {
        if (K & 1)
        {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    for (int k = out[i][j] = 0; k < n; k++)
                        out[i][j] = out[i][j] + ans[i][k]*base[k][j];
                    out[i][j] = out[i][j]%MOD;
                }

            memcpy(ans, out, sizeof(ans));
        }

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            {
                for (int k = out[i][j] = 0; k < n; k++)
                    out[i][j] = out[i][j] + base[i][k]*base[k][j];
                out[i][j] = out[i][j]%MOD;
            }

        memcpy(base, out, sizeof(base));

        K >>= 1;
    }
    
    memcpy(base, ans, sizeof(ans));
}

int main (void)
{
    while (EOF != scanf ("%d %llu", &n, &K))
    {
        unsigned long long int x;
        long long int f[101], fi, ans_fk = 0, ans_sk = 0;

        f[0] = 0;
        for (int i = n; i >= 1; i--)
        {
            scanf ("%lld", f+i);
            f[i] %= MOD;
            f[0] = (f[0] + f[i])%MOD;
        }

        memset(base, 0, sizeof(base));
        base[0][0] = 1;
        base[1][0] = 0;

        for (int i = 1; i <= n; i++)
            base[0][i] = base[1][i] = i;

        for (int i = 2; i <= n; i++)
            base[i][i-1] = 1;

        K -= n; n+=1;
        solve(); n -= 1;
        
        for (int i = 0; i <= n; i++)
        {
            ans_fk = (ans_fk + base[1][i] * f[i]);
            ans_sk = (ans_sk + base[0][i] * f[i]);
        }

        printf ("%lld %lld\n", ans_fk%MOD, ans_sk%MOD);
    }

    return 0;
}

