#include <bits/stdc++.h>

using namespace std;

typedef struct
{
    string nome;
    int poder;
    int morreu;
    int matou;
}Candidates;

bool cmp (Candidates a, Candidates b)
{
    if (a.poder == b.poder)
    {
        if (a.matou == b.matou)
        {
            if (a.morreu == b.morreu)
            {
                return a.nome < b.nome;
            }

            return a.morreu < b.morreu;
        }

        return a.matou > b.matou;
    }

    return a.poder > b.poder;
}

int main (void)
{
    int n;

    scanf ("%d", &n);

    Candidates jog[n];

    for (int i =0; i < n; i++)
    {
        char str[1000];
        
        scanf ("%s %d %d %d", str, &jog[i].poder, &jog[i].matou, &jog[i].morreu);

        jog[i].nome = str;
    }

    sort (jog, jog + n, cmp);

    printf ("%s\n", jog[0].nome.c_str());

    return 0;
}

