#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n,m,k;

    scanf ("%d %d %d", &n, &m, &k);

    int players[m+1];

    for (int i = 0; i < m+1; i++)
        scanf ("%d", players+i);

    const int fedor = players[m];

    int cnt = 0;

    for (int i = 0; i < m; i++)
    {
        int mask = fedor ^ players[i];
        
        int x = __builtin_popcount(mask);

        cnt += x <= k;
    }

    printf ("%d\n", cnt);

    return 0;
}

