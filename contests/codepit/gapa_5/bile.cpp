#include <bits/stdc++.h>

using namespace std;

#define MAXN 101
#define MOD 303700049ULL
#define product(a,b) ((((a)%MOD)*((b)%MOD))%MOD)
#define sum(a,b) ((((a)%MOD)+((b)%MOD))%MOD)

struct matrix
{
    int mat[MAXN][MAXN];
};

matrix matMult(matrix a, matrix b)
{
    matrix ans;
    int i, j, k;

    for (i = 0; i < MAXN; i++)
        for (j = 0; j < MAXN; j++)
            for (ans.mat[i][j] = k = 0; k < MAXN; k++)
                ans.mat[i][j] = (ans.mat[i][j] + (a.mat[i][k]*b.mat[k][j])%MOD) % MOD;

    return ans;
}

matrix matPow(matrix base, int n)
{
    matrix ans;
    for (int i = 0; i < MAXN; i++)
        for (int j = 0; j < MAXN; j++)
            ans.mat[i][j] = (i == j);

    while(n)
    {
        if (n & 1)
            ans = matMult(ans, base);
        base = matMult(base, base);
        n >>= 1;
    }

    return ans;
}


int main (void)
{
    unsigned long long int k;
    int n;

    while (EOF != scanf ("%d %llu", &n, &k))
    {
        long long int f[101], fi, ans_fk = 0, ans_sk = 0;
        matrix base;

        f[0] = 0;
        for (int i = n; i >= 1; i--)
            scanf ("%lld", f+i), f[0] = (f[0] + (f[i]%MOD))%MOD;

        memset(base.mat, 0, sizeof(base.mat));
        base.mat[0][0] = 1;
        base.mat[1][0] = 0;

        for (int i = 1; i <= n; i++)
            base.mat[0][i] = base.mat[1][i] = i;

        for (int i = 2; i <= n; i++)
            base.mat[i][i-1] = 1;

        matrix ans = matPow(base, k-n);

        for (int i = 0; i <= n; i++)
            ans_fk = (ans_fk + ((ans.mat[1][i] * f[i])%MOD))%MOD;

        printf ("%lld %lld\n", ans_fk, ans_sk);
    }

    return 0;
}

