#include <bits/stdc++.h>

using namespace std;

int bfs(int i, int j, int graph[102][102])
{
    int dias = 0;
    int dx[8] = {0, 0, 1, -1, 1, -1, -1, 1};
    int dy[8] = {1, -1, 0, 0, 1, -1, 1, -1};
    queue <pair <int, int>> q;

    q.push({i, j});

    while (!q.empty())
    {
        dias++;
        queue <pair <int, int>> aux;

        while (!q.empty())
        {
            pair <int, int> arvore = q.front(); q.pop();

            for (int k = 0; k < 8; k++)
            {
                int newx = dx[k] + arvore.second;
                int newy = dy[k] + arvore.first;

                if (graph[newy][newx])
                {
                    graph[newy][newx] = 0;
                    aux.push({newy, newx});
                }
            }
        }

        while (!aux.empty())
            q.push(aux.front()), aux.pop();
    }
    
    return dias;
}

int main (void)
{
    int k;

    scanf ("%d", &k);

    while (k--)
    {
        int b, a;

        scanf ("%d %d", &a, &b);

        int graph[102][102] = {0};

        for (int i = 1; i <= a; i++)
            for (int j = 1; j <= b; j++)
                scanf ("%d", &graph[i][j]);
        
        int x, y;

        scanf ("%d %d", &y, &x);
        graph[y][x] = 0;
        printf ("%d\n", bfs(y, x, graph) - 1);
    }

    return 0;
}

