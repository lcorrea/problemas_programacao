#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int x = 100;
    
    while (x--)
    {
        printf ("100 1000000000000000000\n");

        for (int i = 0; i < 100; i++)
            printf("10000000000%c", i == 99 ? '\n' : ' ');
    }

    return 0;
}

