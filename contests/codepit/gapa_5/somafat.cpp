#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int a, b;

    while (EOF != scanf ("%d %d", &a, &b))
    {
        unsigned long long int soma = 1;
        unsigned long long int somb = 1;

        for (int i = 2; i <= a; i++)
            soma = i * soma;

        for (int i = 2; i <= b; i++)
            somb = i * somb;

        printf ("%llu\n", soma + somb);
    }

    return 0;
}

