#include <bits/stdc++.h>

using namespace std;

void backtrack (int i, int x, int n, int v[51])
{
    if (x <= 0)return;
    if (x > n) return;

    v[x] = 1;

    backtrack(i+1, x + (2*i - 1), n, v);
    backtrack(i+1, x - (2*i - 1), n, v);
}

int main (void)
{
    int n, m;

    while (scanf ("%d %d", &n, &m), n and m)
    {
        if (n <= 49)
        {
            int v[51] = {0};
            backtrack(2, 1, n, v);

            puts (v[m] ? "Let me try!" : "Don't make fun of me!");
        }
        else
            puts ("Let me try!");
    }

    return 0;
}

