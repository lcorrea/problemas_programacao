#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef vector<int> vi;

typedef unordered_set<int> Node;

class SegmentTree
{
    private:
        vector<Node> st;
        vi A;
        int n;

        void build(int p, int L, int R)
        {
            if (L == R)
                st[p].insert(A[L]);
            else
            {
                int mid = (L+R) >> 1;
                int left = p << 1;
                int right = left | 1;

                build(left, L, mid);
                build(right, mid+1, R);

                st[p].insert(st[left].begin(), st[left].end());
                st[p].insert(st[right].begin(), st[right].end());
            }
        }

        void query(int p, int L, int R, int i, int j, Node *r)
        {
            //fprintf(stderr, "%d,%d\n", L, R);

            if (L > j or R < i)
                return;

            if (L >= i && R <= j) {

                r->insert(st[p].begin(), st[p].end());

                return;
            }

            int mid = (L + R) >> 1;
            int left = p << 1;
            int right = left | 1;

            query(left, L, mid, i, j, r);
            query(right, mid + 1, R, i, j, r);
        }

    public:
        SegmentTree(const vi &_A)
        {
            A = _A;
            n = (int) A.size();
            st.resize(4 * n);
            build(1, 0, n - 1);
        }

        int query(int i, int j)
        {
            Node ans;
            query(1, 0, n - 1, i, j, &ans);
            return (int) ans.size();
        }
};

void fastscan(int &number)
{
    bool negative = false;
    register int c;

    number = 0;

    c = getchar_unlocked();
    if (c=='-')
    {
        negative = true;

        c = getchar_unlocked();
    }

    for (; (c>47 && c<58); c=getchar_unlocked())
        number = number *10 + c - 48;

    if (negative)
        number *= -1;
}


int main()
{
    int n, q, x, y;

    //scanf("%d", &n);
    fastscan(n);
    vi v;

    for (int i = 0; i < n; i++)
    {
        fastscan(x);
        v.push_back(x);
    }

    SegmentTree ST(v);

    fastscan(q);
    //scanf("%d", &q);

    for (int i = 0; i < q; i++)
    {
        //scanf("%d %d", &x, &y);
        fastscan(x);
        fastscan(y);
        printf("%d\n", ST.query(x-1, y-1));
    }

    //cerr << "Time elapsed: " << 1.0 * clock() / CLOCKS_PER_SEC << " s.\n";
    return 0;
}

