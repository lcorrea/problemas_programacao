#include <bits/stdc++.h>

using namespace std;

const int dy[2] = {0, 1};
const int dx[2] = {1, 0};
int n, m, k;

int stamp(int prop[102][102], int cnt)
{
    int r = cnt;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (prop[i][j] == 0) {
                prop[i][j] = 2;
                if (prop[i][j+1] == 0) {
                    prop[i][j+1] = 2;
                    r = max(r, stamp(prop, cnt + 1));
                    prop[i][j+1] = 0;
                }
                if (prop[i+1][j] == 0) {
                    prop[i+1][j] = 2;
                    r = max(r, stamp(prop, cnt + 1));
                    prop[i+1][j] = 0;
                }

                prop[i][j] = 3;

                r = max(r, stamp(prop, cnt));

            }
        }
    }

    return r;
}

int main (void)
{
    while (scanf("%d %d", &n, &m), n and m) {
        int prop[102][102];

        memset(prop, -1, sizeof(prop));

        scanf("%d", &k);

        for (int i = 1; i <= n; i++)
            memset(&prop[i][1], 0, m*sizeof(int));

        for (int i = 0, y, x; i < k; i++)
            scanf("%d %d", &y, &x), prop[y][x] = 1;

        printf("%d\n", stamp(prop, 0));
    }

    return 0;
}

