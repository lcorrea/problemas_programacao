#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    int ans  = 0;

    while(n--)
    {
        int l, c;

        scanf ("%d %d", &l, &c);

        ans += (l > c) ? c : 0;
    }

    printf ("%d\n", ans);

    return 0;
}

