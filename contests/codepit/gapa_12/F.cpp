#include <cstdio>
#include <algorithm>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    while(n--)
    {
        int t;

        scanf ("%d", &t);

        int o[11];

        for (int i = 0; i < t; i++)
            scanf ("%d", &o[i]);

        sort(o, o+t);

        int cnt = 0;

        for (int i = 0; i < t - 1; i++)
            cnt += o[i] - 1;

        cnt += o[t-1];

        printf ("%d\n", cnt);
    }

    return 0;
}

