#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    bool deck[255][20] = {0};
    const char suits[5] = "PKHT";

    char str[1000] = {0};
    bool greska = false;
    scanf ("%s", str);

    for (int i = 0; str[i]; i+=3)
    {
        int num = (str[i+1] - '0')*10 + (str[i+2]-'0');

        if (deck[str[i]][num])
            greska = true;
        
        deck[str[i]][num] = true;
    }

    if (greska)
    {
        puts ("GRESKA");
        return 0;
    }

    for (int i = 0; suits[i]; i++)
    {
        int cnt = 13;
        for (int num = 1; num <= 13; num++)
            cnt -= deck[suits[i]][num];

        printf ("%d%c", cnt, suits[i] == 'T' ? '\n' : ' ');
    }

    return 0;
}

