#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    while(n--)
    {
        int e, f, c;

        scanf ("%d %d %d", &e, &f, &c);

        int ans = 0;
        int v = e+f;

        while (v >= c)
        {
            ans += v / c;

            v = (v/c + (v % c));
        }

        printf ("%d\n", ans);
    }

    return 0;
}

