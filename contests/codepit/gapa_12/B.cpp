#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    char str[1000] = {0};

    scanf ("%s", str);

    while(1)
    {
        for (int i = 0; str[i]; i++)
        {
            putchar(str[i]);
            switch(str[i])
            {
                case ('a'):
                case ('e'):
                case ('i'):
                case ('o'):
                case ('u'):
                    i+=2;
            }
        }

        memset(str, 0, sizeof(str));
        
        if (EOF == scanf ("%s", str))
        {
            putchar('\n');
            break;
        }
        else
            putchar(' ');
    }

    return 0;
}

