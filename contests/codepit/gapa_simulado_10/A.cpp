#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    map <int, pair<int, int>> out;
    int v[10], sum = 0;

    for (int i = 0; i < 9; i++)
        scanf("%d", v + i), sum += v[i];

    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9 and j != i; j++)
            out[v[i]+v[j]] = {i, j};

    int r = sum - 100;

    for(int i = 0; i < 9; i++)
        if (out[r].first != i and out[r].second != i)
            printf("%d\n", v[i]);

    return 0;
}

