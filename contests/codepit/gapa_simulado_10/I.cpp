#include <bits/stdc++.h>

using namespace std;

#define MOD 1000000000000ULL

unordered_map<uint64_t, uint64_t> tb;

uint64_t fastexp(uint64_t a, uint64_t b)
{
    if (tb.find(b) != tb.end())
        return tb[b];

    if (b == 0)
        return tb[b] = 1;

    if (b == 1)
        return tb[b] = a;

    if (b & 1)
        return tb[b] = (a * fastexp(a, b-1)) % MOD;

    uint64_t x = fastexp(a, b >> 1);

    return tb[b] = (x * x) % MOD;
}

int main (void)
{
    int k1;

    while (scanf("%d", &k1), k1) {
        map<uint64_t, bool> x;

        for (uint64_t i = 0; i < 10000000; i++) {
            uint64_t k  = fastexp(k1, i);

            if (x.find(k) != x.end()) {
                //printf("ciclou %d\n", i);
            } else {
                x[k] = true;
            }

        }
        printf("%d\n", x[308646916096LL]);
        puts("#");
    }
    
    return 0;
}

