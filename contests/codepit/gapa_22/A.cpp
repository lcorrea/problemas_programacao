#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf("%d", &n);

    while(n--) {
        int x;

        scanf ("%d", &x);

        if (x < 0)
        {
            x = -x;
            printf ("%d is %s\n", -x, x & 1 ? "odd" : "even");
        }
        else
            printf ("%d is %s\n", x, x & 1 ? "odd" : "even");
    }

    return 0;
}

