#include <bits/stdc++.h>

using namespace std;

typedef pair<int,int> Stop;
typedef vector<Stop> Stops;

bool cmp(Stop a, Stop b)
{
    return a.first < b.first;
}

int solve(Stops &stops, int l, int p)
{
    int ans = 0, f = p, x = l;
    priority_queue<pair<int, int>> pq;

    //pq.push({p, l});

    for (auto stop : stops) {

        //int f = pq.top().first;
        //int x = -pq.top().second;

        if (stop.first > x) continue;

        if ((x - stop.first) <= f) {

            //printf("I can reach (%d, %d) with %d of fuel from %d\n", 
                    //stop.first, stop.second, f, x);

            pq.push({stop.second, -stop.first});
        }
        else {

            //printf("I not can reach (%d, %d) from %d with %d of fuel\n", stop.first, stop.second, x, f);

            //pq.pop();

            while (!pq.empty() and (x - stop.first) > f) {

                f += pq.top().first;
                //f = f - (x + pq.top().second) + pq.top().first;
                //x = min(x, -pq.top().second);
                pq.pop();
                ans++;
                //printf("I stopped in %d, now I have %d of fuel\n", x, f);
            }

            if ((x - stop.first) > f) {

                ans = -1;
                break;
            }
            else {

                //pq.push({f, -x});
                pq.push({stop.second, -stop.first});
                //printf("Now I have %d of fuel at %d\n", f, x); 
            }
        }
    }

    while (!pq.empty() and x > f) {
        f += pq.top().first;//f - (x + pq.top().second) + pq.top().first;
        //x = min(x, -pq.top().second);
        pq.pop();
        ans++;
        //printf("*I stopped in %d, now I have %d of fuel\n", x, f);
    }

    //printf("Now I have %d of fuel at %d\n", f, x); 
    if (x > f)
        ans = -1;

    /*if (ans == 0 and !pq.empty())
      if (pq.top().second > pq.top().first)
      ans = -1;*/

    return ans;
}

int main (void)
{
    int t;

    scanf("%d", &t);

    while (t--) {

        int n, p, l;

        scanf("%d", &n);

        Stops stops(n);

        for (int i = 0; i < n; i++)
            scanf ("%d %d", &(stops[i].first), &(stops[i].second));

        scanf("%d %d", &l, &p);

        sort(stops.rbegin(), stops.rend(), cmp);

        printf("%d\n", solve(stops, l, p));
    }

    return 0;
}

