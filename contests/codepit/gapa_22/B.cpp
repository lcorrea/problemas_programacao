#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int l;

    while (scanf("%d%*c", &l), l) {

        char str[2000001] = {0};
        int ans = l+1, lastR = -1, lastD = -1;

        for (int i = 0; i < l; i++) {
            str[i] = getchar();
            
            if (str[i] == 'R') lastR = i;
            else if (str[i] == 'D') lastD = i;
            else if (str[i] == 'Z') ans = 0;

            if(str[i] == 'R' and lastD != -1)
                ans = min(ans, lastR - lastD);
            else if (str[i] == 'D' and lastR != -1)
                ans = min(ans, lastD - lastR);

        }
        getchar();
        printf ("%d\n", ans);
    }


    return 0;
}

