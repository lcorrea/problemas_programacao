#include <bits/stdc++.h>

using namespace std;

bool solve(int i, int aff, const pair<int, bool> stat[110], int tb[110])
{
    tb[i] = aff;

    int next_i = stat[i].first;
    int curr_aff = stat[i].second;

    if (tb[next_i] == -1) {
        return solve(next_i, aff ? curr_aff : !curr_aff, stat, tb);
    }

    return (aff ? curr_aff : !curr_aff) == tb[next_i];
}

int main (void)
{
    int n;

    while (scanf("%d", &n), n) {

        bool ans = true;
        pair<int, bool> stat[110];
        int tb[110];
        int tb2[110];

        memset(tb, -1, sizeof(tb));
        memset(tb2, -1, sizeof(tb2));

        for (int i = 0; i < n; i++) {
            
            int a;
            char str[10];

            scanf("%d %s", &a, str);

            stat[i] = {a-1, str[0] == 't'};
        }

        for (int i = 0; i < n; i++)
            for (int ini = 0; ini < 2; ini++) {

                memcpy(tb2, tb, sizeof(tb));

                if(tb2[i] == -1 and !solve(i, ini, stat, tb2)) {

                    if (ini == 1)
                        ans = false;
                    
                    continue;
                }
                else {

                    memcpy(tb, tb2, sizeof(tb2));
                    break;
                }
            }

        puts(ans ? "NOT PARADOX" : "PARADOX");

    }

    return 0;
}

