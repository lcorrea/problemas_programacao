#include <bits/stdc++.h>

using namespace std;

#define INF 1000000000

typedef pair <int,int> ii;
typedef vector < vector <ii> > AdjList;
typedef pair <int, ii > State;

int dijkstra (int n, AdjList graph, vector<int> prices, int T) {
    int ans = INF;
    vector <vector<int>> Cost(n, vector<int>(T+1, INF));//int Cost[n][T+1];
    priority_queue < State > pq;
    
    Cost[0][T] = 0;

    State initial = {0, {-T, 0}};

    pq.push(initial);

    while (!pq.empty()) {
        State now = pq.top(); pq.pop();
        int u = now.second.second;
        int d = -now.first;
        int g = -now.second.first;

        if (d > Cost[u][g]) continue;

        for (auto next_v : graph[u]) {
            int v = next_v.first;
            int c = next_v.second;

            for (int y = 0; (g+y) <= T; y++) {
                if ((g+y) >= c) {
                    int money = prices[u]*y;
                    if (d + money < Cost[v][g+y-c]) {
                        Cost[v][g+y-c] = d+money;
                        State fwd = {-(d+money), {-(g+y - c), v}};
                        pq.push(fwd);
                    }
                }
            }
        }
    }

    for (int i = 0; i <= T; i++)
        ans = min(ans, Cost[n-1][i]);

    return ans;
}

int main (void)
{
    int n, m, t;

    while (scanf("%d %d %d", &n, &m, &t), n and m and t) {
        AdjList graph(n);
        vector<int> prices(n);

        for (int i = 0, a, b, c; i < m; i++) {

            scanf("%d %d %d", &a, &b, &c);

            a--; b--;
            graph[a].push_back({b, c});
            graph[b].push_back({a, c});
        }

        for (int i = 0; i < n; i++)
            scanf("%d", &prices[i]);

        //puts("OK. Run Dijkstra!");
        int ans = dijkstra(n, graph, prices, t);

        printf("%d\n", ans == INF ? -1 : ans);
    }

    return 0;
}

