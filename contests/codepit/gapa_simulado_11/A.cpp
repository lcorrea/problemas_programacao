#include <bits/stdc++.h>

using namespace std;

struct car{
	unsigned int number;
	unsigned int last_sensor;
	unsigned int pts;
	unsigned int tempo;
}car_t;

bool comp(car a, car b)
{
	if(a.pts == b.pts)
		return (a.tempo < b.tempo);

	return (a.pts > b.pts);
}

int main(void)
{
	unsigned int k, n, m, v, i, p, j = 0;
	car carros[100];

	cin >> k >> n >> m;
	
	for(p = 0; p < n; p++)
	{
		carros[p].number = p+1;
		carros[p].last_sensor = 0;
		carros[p].pts = 0;
		carros[p].tempo = -1;
	}

	for(j = 0; j < m; j++)
	{
		cin >> i >> v;
		
		i--;

		if(v-1 == carros[i].last_sensor || ((v == 1) && carros[i].last_sensor == k))
		{
			carros[i].last_sensor = v;
			carros[i].pts++;
			carros[i].tempo = j;
		}
		
	}
	
	sort(carros, carros + n, comp); 
	
	cout << carros[0].number;

	for(j=1; j < n; j++)
		cout << ' ' << carros[j].number;
	
	cout << endl;
	
	return 0;
}

