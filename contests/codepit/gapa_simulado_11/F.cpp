#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;
    
    scanf("%d", &n);

    int va[n];
    int vb[n];

    for (int i = 0; i < n; i++)
        scanf("%d", va+i);
    for (int i = 0; i < n; i++)
        scanf("%d", vb+i);

    sort (vb, vb+n);
    sort (va, va+n);

    int ans = 0, pa = 0, pb = 0;
    while (pa < n and pb < n) {
        while ((pa < n and pb < n) and va[pa] >= vb[pb]) {
            pb++;
        }
        pa++;
        if (pb < n) {
            ans++;
            pb++;
        }
    }
    printf("%d\n", ans);

    return 0;
}

