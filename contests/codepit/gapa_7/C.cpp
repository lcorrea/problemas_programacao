#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        int a, b;

        scanf ("%d %d", &a, &b);

        puts (a >= b ? "MMM BRAINS" : "NO BRAINS");
    }

    return 0;
}

