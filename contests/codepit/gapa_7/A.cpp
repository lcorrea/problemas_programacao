#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    const string ovf = "2147483647";
    
    while (1)
    {
        int len = 0;
        char str[1000], ch;

        while ((ch = getchar()), ch != EOF and ch != '\n')
            str[len++] = ch;
        str[len] = '\0';

        if (ch == EOF) break;

        string a;

        bool error = 0, hasnumber = false;
        
        for (int i = 0; str[i]; i++)
            if (isdigit(str[i]))
                a += str[i];
            else if (toupper(str[i]) == 'O' or str[i] == 'l')
                a += str[i] == 'l' ? '1' : '0';
            else if(str[i] == ',' or str[i] == ' ');
            else
                error = true;
        
        reverse(a.begin(), a.end());
        while (a.length() > 1 and a.back() == '0' and a[a.length()-2] == '0')
            a.pop_back();
        
        reverse(a.begin(), a.end());

        if (a.length() > ovf.length())
            puts ("error");
        else if (a.length() == ovf.length() and a > ovf)
            puts ("error");
        else if (a.length() and !error)
        {
            unsigned long long int num;
            sscanf (a.c_str(), "%llu", &num);
            printf ("%llu\n", num);
        }
        else
            puts ("error");
    }

    return 0;
}

