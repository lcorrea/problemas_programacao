#include <bits/stdc++.h>

using namespace std;

vector <int> h; 
vector <int> p;

int find(int a)
{
    return p[a] == a ? a : p[a] = find(p[a]);
}

void join(int a, int b)
{
    int pa = find(a);
    int pb = find(b);

    if (pa == pb)
        return;

    if (h[pa] > h[pb]) {
        p[pb] = pa;
    }
    else
    {
        p[pa] = pb;

        if (h[pa] == h[pb])
            h[pb]++;
    }
}

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    h.resize(n+1, 0);
    p.resize(n+1);

    for (int i = 0; i <= n; i++)
        p[i] = i;

    for (int i = 0, a, b; i < m; i++)
    {
        scanf ("%d %d", &a, &b);

        join(a, b);
    }

    int x;

    scanf ("%d", &x);

    while (x--)
    {
        int a, b;

        scanf ("%d %d", &a, &b);

        puts (find(a) == find(b) ? "YES" : "NO");
    }

    return 0;
}

