#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int v[43] = {0}, n, ans = 0, it = 0;

    for (int i = 0; i < 10; i++)
    {
        scanf ("%d", &n);

        int x = n % 42;

        ans += v[x] == 0;

        v[x] = 1;
    }

    printf ("%d\n", ans);

    return 0;
}

