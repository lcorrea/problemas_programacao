#include <bits/stdc++.h>

using namespace std;

int main (void)
{

    int n;

    while (scanf ("%d", &n), n)
    {
        int ans = 0;
        int cor[1000001] ={0};

        for (int i = 0, c; i < n; i++)
            scanf ("%d", &c), ans = cor[ans] < (++cor[c]) ? c : ans;

        printf ("%d\n", ans);
    }
    

    return 0;
}

