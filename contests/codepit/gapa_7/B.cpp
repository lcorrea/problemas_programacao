#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int a[10];
    printf ("%d %d %d\n", a[0], a[1],  a[9]);
    int n;
    unsigned long long v[1000001];
    unsigned long long ans = 0;
    scanf ("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf ("%llu", &v[i]);
        ans ^= v[i];
    }

    unsigned long long x = 0;

    for (int i = 0; i < n; i++)
        x |= ans^v[i];

    printf ("%llu\n", x);

    return 0;
}

