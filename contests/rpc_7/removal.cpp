#include <bits/stdc++.h>

using namespace std;

map <deque <int>, int> x;

int calc (deque <int> v, int cost)
{
    if (x.find(v) != x.end())
        return x[v] + cost;

    int n = (int) v.size();

    if (n == 2)
    {
        int gcd = __gcd(v[0], v[1]);

        x[v] = gcd;

        return cost + gcd;
    }

    int a = *v.rbegin();
    int b = v[1];
    int c = v[0];

    v.erase(v.begin());
    
    int minCost = calc (v, cost + __gcd(a, b));
    
    v.insert(v.begin(), c);

    for (int i = 1; i < n; i++)
    {
        a = v[i-1];
        b = v[(i+1)%n];
        c = v[i];

        int gcd = __gcd(a,b);

        if (gcd + cost < minCost)
        {
            v.erase(v.begin() + i);
            minCost = min(minCost, calc(v, cost + gcd));
            v.insert (v.begin()+i, c);
        }
    }

    x[v] = minCost;

    return minCost;
}

int main (void)
{
    int n;
    
    while(scanf ("%d", &n), n)
    {
        x.clear();
        deque <int> v(n);
        
        for (int i = 0; i < n; i++)
            scanf ("%d", &v[i]);

        printf ("%d\n", calc(v, 0));
    }

    return 0;
}

