#include <iostream>
#include <algorithm>

using namespace std;

int main (void)
{
	int n, numero;
	unsigned int v;

	cin >> n;

	while (n)
	{
		v = 0;
		for (int i = 1; i <= n; i++)
		{
			cin >> numero;

			if (i != numero)
				v++;
		}

		if (v == 0)
			cout << "Carlos";
		else if (v % 2 == 0)
			cout << "Marcelo";
		else 
			cout << "Carlos";

		cout << endl;

		cin >> n;
	}

	return 0;
}

