#include <iostream>
#include <cstdio>
#include <algorithm>

#define MAX 100010

using namespace std;

int vet[MAX], cnt;

bool compare (int x, int y)
{
	cnt++;
	if (x > y)
	{
		return false;
	}
	return true;
}

int main()
{
	int n;
	while(scanf("%d", &n) != EOF && n)
	{
		cnt = 1;
		for (int i = 0; i < n; i++)
		{
			scanf("%d", &vet[i]);
		}
		sort(vet, vet+n, compare);
		printf("%d\n", cnt);
		for (int i = 0; i < n; i++)
		{
			printf("%d ", vet[i]);
		}
	}
	return 0;
}
