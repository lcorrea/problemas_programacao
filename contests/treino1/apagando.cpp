#include <iostream>
#include <cmath>
#include <algorithm>
#include <queue>

using namespace std;

int main (void)
{
	string numero;
	string::iterator it;
	unsigned int D, N, digits, digit;
	unsigned int max = 0;
	unsigned int p;

	cin >> N >> D;

	while (D && N)
	{
		cin >> numero;
		
		priority_queue <int> candidates;
		max = 0;
		digits = N - D;

		for (it = numero.begin(); it != numero.end() - digits+1; it++)
			candidates.push(*it - '0');

		p = pow (10, digits - 1);

		while (digits)
		{
			digit = candidates.top(); candidates.pop();

			max += digit*p;
			p /=10;

			candidates.push(*it - '0');
			it++;

			digits--;
		}

		cout << max << endl;

		cin >> N >> D;
	}	

	return 0;
}

