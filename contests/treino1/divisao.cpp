#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
	int k, n, m, x, y;
	while(scanf("%d", &k) != EOF && k)
	{
		scanf("%d %d", &n, &m);
		for (int i = 0; i < k; i++)
		{
			scanf("%d %d", &x, &y);
			if (x==n || y == m)
			{
				printf("divisa\n");
			}
			else if (x > n)
			{
				if (y > m)
				{
					printf("NE\n");
				}
				else
				{
					printf("SE\n");
				}
			}
			else
			{
				if (y > m)
				{
					printf("NO\n");
				}
				else
				{
					printf("SO\n");
				}
			}
		}
	}
	return 0;
}	
