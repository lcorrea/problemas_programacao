#include <bits/stdc++.h>

using namespace std;

char map1[1010][1010] = {0};
char tab[1010][1010] = {0};
int ans = 0;

int dfs(int i, int j)
{
    int dx[8] = {0, 1, 1, 1, 0, -1, -1, -1};
    int dy[8] = {-1, -1, 0, 1, 1, 1, 0, -1};
    int cnt = 0;

    if (tab[i][j] == '#')
        cnt++;

    tab[i][j] = '-';
    for (int k = 0; k < 8; k++)
    {
        int newx = j + dx[k];
        int newy = i + dy[k];

        if (tab[newy][newx] == '#' or tab[newy][newx] == '.')
            cnt += dfs(newy, newx);
    }
    
    return cnt;
}

void dfs2(int i, int j)
{
    int dx[8] = {0, 1, 1, 1, 0, -1, -1, -1};
    int dy[8] = {-1, -1, 0, 1, 1, 1, 0, -1};

    tab[i][j] = '-';

    for (int k = 0; k < 8; k++)
    {
        int newx = j + dx[k];
        int newy = i + dy[k];

        if (tab[newy][newx] == '#' or tab[newy][newx] == '*')
        {
            memcpy(map1, tab, sizeof(tab));
            ans = max(ans, dfs(newy, newx));
        }           
    }
}


int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

        int startx = 1, starty = 1;

    memset (tab, '.', sizeof(tab));

    for (int i = 1; i <= n; i++)
    {
        getchar();
        for (int j = 1; j <= m; j++)
            scanf ("%c", &tab[i][j]);
    }

    int maxCnt = 0, cnt = 0;
/*    
    for (int i = 1; i <= m; i++)
        if (map[1][i] == '#')
            cnt++, map[1][i] = '.';

    for (int i = 1; i <= n; i++)
        if (map[i][1] == '#')
            cnt++, map[i][1] = '.';

    for (int i = 1; i <= m; i++)
        if (map[n][i] == '#')
            cnt++, map[n][i] = '.';

    for (int i = 1; i <= n; i++)
        if (map[i][m] == '#')
            cnt++, map[i][m] = '.';
*/
    /*
    for (int i = 1; i <= m; i++)
        maxCnt = max(maxCnt, dfs(1, i, map, 1));

    for (int i = 1; i <= n; i++)
        maxCnt = max(maxCnt, dfs(i, 1, map, 1));

    for (int i = 1; i <= m; i++)
        maxCnt = max(maxCnt, dfs(n, i, map, 1));

    for (int i = 1; i <= n; i++)
        maxCnt = max(maxCnt, dfs(i, m, map, 1));
*/

    dfs2(0, 0);
    printf ("%d\n", ans); //maxCnt + cnt);

    return 0;
}

