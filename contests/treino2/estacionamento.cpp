#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int main (void)
{
	int n, c;

	cin >> c >> n;

	while (!cin.eof())
	{
		char op;
		int placa, carSize, faturamento = 0, vagas = c;
		vector <int> E(c, 0);
		map <int, int> placaSize, placaPos;

		while (n--)
		{
			cin >> op >> placa;	

			if (op == 'C')
			{
				cin >> carSize;

				if (vagas >= carSize)
				{
					for (int pos = 0, vaga = 0; pos < c; pos++)
					{
						if (E[pos])
						{
							vaga = 0;
							pos += placaSize[E[pos]] - 1;
						}
						else
						{
							vaga++;

							if (vaga == carSize)
							{
								faturamento += 10;
								placaSize[placa] = carSize;
								placaPos[placa] = pos+1-carSize;
								vagas -= carSize;
								fill (E.begin()+pos-carSize+1, E.begin()+pos+1, placa);
								break;
							}
						}
					}
				}
			}
			else
			{
				fill (E.begin()+placaPos[placa], E.begin()+placaPos[placa]+placaSize[placa], 0);
				vagas += placaSize[placa];
				placaSize.erase(placa);
				placaPos.erase(placa);
			}
		}

		cout << faturamento << endl;

		cin >> c >> n;
	}

	return 0;
}

