#include <iostream>
#include <map>
#include <utility>
#include <algorithm>

using namespace std;

typedef pair <int, int> ii;
typedef map <int, ii > caixa;

int main (void)
{
	int n, m, numero;
	char l;

	cin >> n;

	while (!cin.eof())
	{
		caixa botasRecebidas;

		for (int i = 0; i < n; i++)
		{

			cin >> numero  >> l;

			switch (l)
			{
				case ('D'):
					botasRecebidas[numero].first++;
					break;
				default:
					botasRecebidas[numero].second++;
					break;
			}
		}

		int pares = 0;

		for (caixa::const_iterator it = botasRecebidas.begin(); it != botasRecebidas.end(); it++)
		{
			ii par = it->second;

			if (par.first and par.second)
				pares += min(par.first, par.second);
		}

		cout << pares << endl;

		cin >> n;
	}
	return 0;
}

