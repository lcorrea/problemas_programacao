/*
	problem 5060
	arm wrestling tournament
	by lucas correa

*/
#include <iostream>
#include <vector>
#include <queue>
#include <cmath>

using namespace std;

typedef struct contestant 
{
	int n, o;
	int p;
	vector<int> beated;
}cntst_t;

cntst_t winner(cntst_t& c1, cntst_t& c2, int k)
{
	if(c1.p == c2.p)
	{
		if(c1.n < c2.n)
		{
			if(k <= c1.o)
				c1.p = k;

			c1.beated.push_back(c2.n);

			return c1;
		}
		else
		{
			if(k <= c2.o)
				c2.p = k;

			c2.beated.push_back(c1.n);

			return c2;
		}
	}
	else if(c1.p > c2.p)
	{
		c1.p = c1.p - c2.p + k;

		if(c1.p > c1.o)
			c1.p = c1.o;
		
		c1.beated.push_back(c2.n);
		return c1;
	}
	else
	{
		c2.p = c2.p - c1.p + k;

		if(c2.p > c2.o)
			c2.p = c2.o;

		c2.beated.push_back(c1.n);
		return c2;
	}
	
}

int main(void)
{
	int t = 0, n;
	int k, i;

	cntst_t c1, c2;

	queue<cntst_t> current_matches, next_matches;

	cin >> t;

	while(t--)
	{
		cin >> n >> k;
		
		n = pow(2, n);

		for(i = 1; i <= n; i++)
		{
			cin >> c1.o;
			c1.p = c1.o;
			c1.n = i;
			
			next_matches.emplace(c1);
		}

		while(next_matches.size() > 1)
		{
			current_matches = next_matches;
			next_matches = queue<cntst_t>();

			while(!current_matches.empty())
			{
				c1 = current_matches.front();
				current_matches.pop();

				c2 = current_matches.front();
				current_matches.pop();

				c1 = winner(c1, c2, k);

				next_matches.emplace(c1);
			}
		}

		c1 = next_matches.front();
		next_matches.pop();

		cout << c1.n << '\n';
		cout << c1.beated[0];

		for(vector<int>::const_iterator it = c1.beated.begin() + 1; it != c1.beated.end(); it++)
			cout << ' ' << *it;

		cout << endl;

		c1.beated.clear();
		c2.beated.clear();

	}
	return 0;
}

