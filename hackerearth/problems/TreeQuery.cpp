#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, m;

    scanf ("%d %d", &n, &m);

    vector <int> in(n+1,0), out(n+1, 0);

    int maxSink = 0, maxSource = 0, isolated = 0;

    for (int i = 0, a, b; i < m; i++)
    {
        scanf ("%d %d", &a, &b);

        ++in[b]; ++out[a];
    }

    for (int i = 1; i <= n; i++)
    {
        if (!out[i] and !in[i])
            isolated++;
        else if (!out[i])
            maxSink++;
        else if (!in[i])
            maxSource++;
    }

    printf ("%d\n", max(maxSink+isolated, maxSource+isolated));

    return 0;
}

