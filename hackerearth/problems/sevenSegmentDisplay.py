#!/usr/bin/env python3

def main():
    
    from sys import stdin, stdout, stderr
    
    disp_count = (6, 2, 5, 5, 4, 5, 6, 3, 7, 6)
    
    next(stdin)

    for number in stdin:
        
        number = number if number[-1] != '\n' else number[:-1]

        total_sticks = sum(map(lambda x: disp_count[int(x)], number))
        
        ans = '1' * (total_sticks >> 1)

        if (total_sticks & 1):
            stdout.write('7' + ans[1:] + '\n')
        else:
            stdout.write(ans + '\n')

if __name__ == "__main__":
    main()
