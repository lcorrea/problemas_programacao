#include <bits/stdc++.h>

using namespace std;

int height_tree = 0, sqrtn;
vector <int> parent, sqrt_parent, level;
vector < vector <int> > graph;

void pre_process_tree(int u, int lvl)
{
    level[u] = lvl;
    height_tree = max(lvl, height_tree);
    
    for (auto i = graph[u].begin(); i != graph[u].end(); i++)
    {
        int v = *i;
        
        if (parent[v] == -1)
        {
            parent[v] = u;
            pre_process_tree(v, lvl+1);
        }
    }
}

void pre_process_lca(int u)
{
    if (level[u] < sqrtn)
        sqrt_parent[u] = 1;
    else if ((level[u] % sqrtn) == 0)
        sqrt_parent[u] = parent[u];
    else
        sqrt_parent[u] = sqrt_parent[parent[u]];
    
    for (auto i = graph[u].begin(); i != graph[u].end(); i++)
    {
        int v = *i;
        
        if (sqrt_parent[v] == -1)
            pre_process_lca(v);
    }
}

int lca(int u, int v)
{
    while (sqrt_parent[u] != sqrt_parent[v])
    {
        if (level[u] > level[v])
            u = sqrt_parent[u];
        else
            v = sqrt_parent[v];
    }
    
    while (u != v)
    {
        if (level[u] > level[v])
            u = parent[u];
        else
            v = parent[v];
    }
    
    return u;
}

int main()
{
    int n;
    
    scanf ("%d", &n);
    
    graph.resize(n+1);
    parent.assign(n+1, -1);
    level.assign(n+1, 0);
    sqrt_parent.assign(n+1, -1);
    
    for (int i = 1, a, b; i < n; i++)
    {
        scanf ("%d %d", &a, &b);
        
        graph[a].push_back(b);
        graph[b].push_back(a);
    }
    
    parent[1] = 1;
    pre_process_tree(1, 0);
    sqrtn = sqrt(height_tree);
    pre_process_lca(1);
    
    int q;
    
    scanf ("%d", &q);
    
    while(q--)
    {
        int t, x, y;
        
        scanf ("%d %d %d", &t, &x, &y);
        
        if (t)
            puts (lca(y, x) == y ? "YES" : "NO");
        else
            puts (lca(y, x) == x ? "YES" : "NO");
    }
    
    return 0;
}

