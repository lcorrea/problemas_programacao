#include <bits/stdc++.h>

using namespace std;

typedef vector < vector <int> > Graph;

int dfs (int u, Graph &graph, vector <int> visited)
{
    int cnt = 1;

    visited[u] = 1;

    for (int i = 0; i < (int) graph[u].size(); ++i)
    {
        int v = graph[u][i];

        if (!visited[v])
            cnt += dfs(v, graph, visited);
    }

    return cnt;
}

int main (void)
{
    int n, d;

    scanf ("%d %d", &n, &d);

    Graph graph(n+1);

    for (int i = 0, a, b; i < d; i++)
    {
        scanf ("%d %d", &a, &b);

        graph[a].push_back(b);
    }

    int minCnt = n;

    for (int i = 1; i <= n; i++)
        minCnt = min(minCnt, dfs(i, graph, vector <int> (n+1, 0)));

    printf ("%d\n", minCnt);

    return 0;
}

