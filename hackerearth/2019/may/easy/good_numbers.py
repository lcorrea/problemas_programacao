#!/usr/bin/env python3

def main():
    t = int(input())

    while t:

        n = int(input())

        print(n // 2 - n // 4)
        
        t -= 1

if __name__ == "__main__":
    main()

