#!/usr/bin/env python3
#editorial's answer

def main():
    t = int(input())
    while t:
        x, y, a, b = map(int, input().split())
        
       print("Yes" if x*y == a+b else "No")

        t -= 1 

if __name__ == "__main__":
    main()

