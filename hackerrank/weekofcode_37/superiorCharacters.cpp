#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

bool check(long long s, long long *suffix)
{
    int i;
    for (i = 25; i >= 0 and s > suffix[i]; --i);

    if (i == -1) {printf("$"); return false;}
    long long sup = (i == 25) ? 0 : suffix[i+1];
    long long nonsup = suffix[0] - suffix[i];
    long long supEqual = (i == 25) ?  s : s - suffix[i+1] ;
    long long nonsupEqual = suffix[i] - s;
    
    /*fprintf(stderr, "total: %lld\nr: %lld\nl: %lld\nrs: %lld\nls: %lld\n",
            suffix[25],
            sup,
            nonsup,
            supEqual,
            nonsupEqual);*/

    if ((nonsup + nonsupEqual) > (sup + supEqual))
        if (nonsup > supEqual)
            return (sup + supEqual) >= s;

    return false;
}

// Complete the maximumSuperiorCharacters function below.
long long maximumSuperiorCharacters(vector<int> freq) {
    
    int n = 26;
    long long suffix[26];
    
    suffix[25] = freq[25];

    for (int i = 24; i >= 0; --i)
        suffix[i] = suffix[i+1] + freq[i];

    long long l = 0;
    long long r = suffix[0] + 1;
    long long mid = l + ((r-l) >> 1);
    long long ans = 0;

    for (int i = 0; l < r and i < 64; i++) {

        mid = l + ((r - l) >> 1);
        
        //fprintf(stderr, "mid: %lld ", mid);

        if (check(mid, suffix)) {
            ans = max(mid, ans);
            l = mid;
            //fprintf(stderr, ">>\n");
        } else {
            r = mid;
            //fprintf(stderr, "<<\n");
        }

    }

    //fprintf(stderr, "%lld\n", ans);
    
    return ans;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int t;
    cin >> t;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int t_itr = 0; t_itr < t; t_itr++) {
        string freq_temp_temp;
        getline(cin, freq_temp_temp);

        vector<string> freq_temp = split_string(freq_temp_temp);

        vector<int> freq(26);

        for (int freq_itr = 0; freq_itr < 26; freq_itr++) {
            int freq_item = stoi(freq_temp[freq_itr]);

            freq[freq_itr] = freq_item;
        }

        long long result = maximumSuperiorCharacters(freq);

        cout << result << "\n";
        //fout << result << "\n";
    }

    fout.close();

    return 0;
}

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}

