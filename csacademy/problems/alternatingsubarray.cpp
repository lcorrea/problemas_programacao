#include <bits/stdc++.h>

using namespace std;

int n, i, form;
int v[100001];

int las ()
{
    if (i == n)
        return 0;

    if (form == 0)
    {
        if (v[i] > v[i-1])
        {
            form = !form; i++;
            return 1 + las();
        }
    }
    else
    {
      if (v[i] < v[i-1])
      {
          form = !form; i++;
          return 1 + las();
      }
    }

    i++; form = !form;
    return 0;
}

int main()
{
    scanf ("%d", &n);

    for (int k = 0; k < n; k++)
        scanf ("%d", v+k);

    if (n == 1)
        puts ("1");
    else
    {
        int mcnt = 0;

        i = 1, form = 0;
        while (i < n)
        {
            int local;
            
            local = 1 + las();
            mcnt = max(mcnt, local);

            local = 1 + las();
            mcnt = max(mcnt, local);
        }

        i = 1, form = 1;
        while (i < n)
        {
            int local;
            
            local = 1 + las();
            mcnt = max(mcnt, local);
            
            local = 1 + las();
            mcnt = max(mcnt, local);
        }

        printf ("%d\n", mcnt);
    }

    return 0;
}

