#include <bits/stdc++.h>

using namespace std;

class SubGraph;

typedef pair <int, int> Edge;
typedef vector < pair <int, int> > EdgeList;
typedef vector < vector <int> > AdjList;
typedef priority_queue <SubGraph>  Components;

int edgeCount = 0;
vector <int> visited, parent;
AdjList graph;
Components subGraphs;

class SubGraph
{
    public:
    EdgeList graph;
    int id;

    void merge(SubGraph &a)
    {
        graph.insert (graph.end(), a.graph.begin(), a.graph.end());
    }

    bool operator < (const SubGraph &b) const
    {
        if (this->graph.size() < b.graph.size())
            return true;
        
        return false;
    }
};

void dfs (int u, SubGraph &subGraph)
{
    visited[u] = 1;

    for (int i = 0; i < (int) graph[u].size(); i++)
    {
        int v = graph[u][i];

        if (v == parent[u]) continue;

        if (visited[v])
        {
            subGraph.graph.push_back(make_pair(u, v));
            edgeCount++;
        }
        else
        {
            parent[v] = u;
            dfs(v, subGraph);
        }
    }
}

int main ()
{
    int n, m;

    scanf ("%d %d", &n, &m);

    graph.resize(n+1);
    visited.resize(n+1, 0);
    parent.resize(n+1, 0);

    for (int i = 0, a, b; i < m; i++)
    {
        scanf ("%d %d", &a, &b);

        graph[a].push_back(b);
        graph[b].push_back(a);
    }

    vector <int> components;

    for (int i = 1; i <= n; i++)
    {
        if (!visited[i])
        {
            SubGraph subg;
            subg.id = i;
            dfs(i, subg);
            subGraphs.push(subg);
            components.push_back(i);
        }
    }

    if (edgeCount >= ((int) components.size()) - 1)
    {
        printf ("%d\n", (int) components.size() - 1);

        while ((int) components.size() > 1)
        {
            SubGraph u = subGraphs.top();
            
            if (!u.graph.empty())
            {

                if (u.id == components.back())
                {
                    components.pop_back();
                    continue;
                }

                int u1 = u.graph.back().first;
                int u2 = u.graph.back().second; u.graph.pop_back();

                printf ("%d %d %d %d\n", u1, u2, u.id, components.back());
                components.pop_back();
            }
            else
                subGraphs.pop();
        }
    }
    else
        puts ("-1");

    return 0;
}

