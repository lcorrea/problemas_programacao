#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, x;

    cin >> n;

    while (n--)
    {
        cin >> x;

        string str[x];
        int eng = -1;

        for (int i = 0; i < x; i++)
        {
            cin >> str[i];

            if (isalpha(str[i][0]))
                eng = i;
        }

        if (eng == -1)
        {
            cout << str[0];

            for (int i = 1; i < x; i++)
                cout << ' ' << str[i];
        }
        else
        {
            for (int i = eng + 1; i < x; i++)
                    cout << str[i] << ' ';

            cout << str[eng];

            for (int i = 0; i < eng; i++)
                cout << ' ' << str[i];
        }

        cout << endl;
    }

    return 0;
}
