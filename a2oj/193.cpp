#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n;

    scanf ("%d", &n);

    while (n--)
    {
        char str[200], *max_qnt = str;
        char candies[256] = {0};

        scanf ("%s", str);

        sort (str, str + strlen(str));

        for (char *i = str; *i; i++)
            if (++candies[*i] > candies[*max_qnt])
                max_qnt = i;

        printf ("%d %c\n", candies[*max_qnt], *max_qnt);
    }

    return 0;
}

