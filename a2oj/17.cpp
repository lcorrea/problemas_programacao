#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    cin >> t;

    while (t--)
    {
        int n, m;
        int total = 0;

        cin >> n >> m;

        for (int i = 0, q; i < n; i++)
        {
            cin >> q;

            total += q / m;
        }

        cout << total << endl;
    }

    return 0;
}

