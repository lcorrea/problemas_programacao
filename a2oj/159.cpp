#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int v[10], t;

    scanf ("%d", &t);

    while (t--)
    {
        for (int i = 0; i < 10; i++)
            scanf ("%d", v + i);

        sort(v, v+10);

        printf ("%d\n", v[1]);
    }

    return 0;
}

