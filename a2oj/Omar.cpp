#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int x, y, n;

    cin >> n;

    while (n--)
    {
        cin >> x >> y;
        cout << x + y << endl;
    }
    
    return 0;
}

