#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int t;

    cin >> t;

    while (t--)
    {
        int n, x, y;
        int colors[101];

        cin >> n >> x >> y;

        for (int i = 0; i < n; i++)
            cin >> colors[i];

        if (x == colors[0] and y == colors[n-1])
            puts ("BOTH");
        else if (x == colors[0])
            puts ("EASY");
        else if (y == colors[n-1])
            puts ("HARD");
        else
            puts ("OKAY");
    }

    return 0;
}

