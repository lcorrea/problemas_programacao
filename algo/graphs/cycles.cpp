#include <iostream>
#include <set>
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vi pre, pos;

//dfs modificada para encontrar ciclos em um grafo dirigido
//retorna true se existe um ciclo no grafo

bool dfs (int vtx, vvi& graph)
{
	pre[vtx] = 1;

	for (unsigned int i = 0; i < graph[vtx].size(); i++)
	{
		int v = graph[vtx][i];

		if (pre[v] == 0)
		{
			if (dfs (v, graph))
				return true;
		}
		else if (pos[v] == 0)
			return true;
	}
	pos[vtx] = 1;

	return false;
}

