/*
 * Breadth-first-search - com lista de adjacencias
 * by: Carlos, Flavio, Lucas
 */
#include <vector>
#include <queue>

typedef vector <int> vi;
typedef vector <vi> vvi;

vvi graph;
vi visited;
queue <int> nextToVisit;

void BFS (int firstVtx)
{
	int  vtx;

	visited[firstVtx] = 1;
	nextToVisit.push(firstVtx);

	while (!nextToVisit.empty())
	{
		vtx = nextToVisit.front();
		nextToVisit.pop();

		for (int i = 0, newVtx; i < graph[visit].size(); i++)
		{
			newVtx = graph[vtx][i];

			if (visited[newVtx] == -1)
			{
				visited[newVtx] = 1;
				nextToVisit.push(newVtx);
			}
		}
	}
}

