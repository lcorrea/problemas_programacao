/*
 * Depth first search - Com lista de adjacencias
 * by: Carlos, Flávio, Lucas
 */
#include <vector>

using namespace std;

typedef vector <int> vi;
typedef vector <vi> vvi;

vvi graph;
vi visited;

void DFS (int vtx)
{
	visited[vtx] = 1;	//flood fill

	for (int i = 0, newVtx = 0; i < graph[vtx].size(); i++)
	{
		newVtx = graph[vtx].size();

		if (visited[newVtx] == -1)
		{
			DFS (newVtx);
		}
	}
}

