// by: flavio mendes

#include<iostream>
#include<vector>
#include<stdio.h>
#include<queue>

using namespace std;

typedef pair<int,int> ii;
typedef vector<ii> vii;

vector<vii> graph;
vector<int> dist;
priority_queue<ii, vector<ii>, greater<ii> > pq;
vector <int> parent;


//Função para imprimir o menor caminho

void printPath(int s, int u) {
	if ( u == s) { printf("%d", s); return; }

	printPath(s,parent[u]);
	printf("->%d", u); 
}


int main(){

	int nv, iv, fv, w,s=2;

	cin >> nv;

	graph.clear();
	graph.resize(nv);

	dist.clear();
	dist.resize(nv);
	dist.assign(dist.size(),40);

	parent.clear();
	parent.resize(nv);
	parent.assign(parent.size(),0);

	while(scanf("%d",&iv) != EOF){

		cin >> fv >> w;
		graph[iv].push_back(ii(fv,w));

	}


	//dijsktra algorithm starts here
	cout << "Running dijkstra algorithm... " << endl;
	dist[s] = 0;
	pq.push(ii(0,s));
	parent[s] = 2;
	while(!pq.empty()){
		ii front = pq.top();
		pq.pop();
		int d = front.first;
		int u = front.second;
		if (d > dist[u]) continue;
		for (int j = 0; j < (int)graph[u].size(); j++) {
			ii v = graph[u][j];
			if (dist[u] + v.second < dist[v.first]) {
				parent[v.first] = u;
				dist[v.first] = dist[u] + v.second;
				pq.push(ii(dist[v.first], v.first));
			}
		}

	}
	//The algorithm ends here.

	for (int i = 0; i < nv; i ++)
		cout << dist[i] << ' ';
	cout << endl;

	cout << "The vector of parents..." << endl;

	for (int i = 0; i < parent.size(); i++)
		cout << '|' << i << '|';
	cout << endl;

	for (int i = 0; i < parent.size(); i++)
		cout << '|'<< parent[i] << '|';
	cout << endl;

	printPath(2,0);		


	return 0;



}
