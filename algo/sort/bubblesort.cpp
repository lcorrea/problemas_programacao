#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

// bubble sort
void bubble (vector <int> v)
{
	bool flag = true;

	while (flag)
	{
		flag = false;

		for (unsigned int i = 0; i < v.size() - 1; i++)
		{
			if (v[i] > v[i+1])
			{
				flag = true;
				swap (v[i], v[i+1]);
			}
		}
	}
}

