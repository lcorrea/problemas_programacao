#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

//mergesort
void mergesort (int p, int r, int v[])
{
	if (p < r - 1)
	{
		int q = (p + r)/2;

		mergesort (p, q, v);
		mergesort (q, r, v);
		intercala2 (p, q, r, v);
	}
}

void intercala2 (int p, int q, int r, int v[])
{
	int i, j, k, *w;
	w = (int *) malloc ((r-p) * sizeof (int));

	for (i = 0, k = p; k < q; ++i, ++k) w[i] = v[k];
	for (j = r-p-1, k = q; k < r; --j, ++k) w[j] = v[k];
	i = 0; j = r-p-1;
	for (k = p; k < r; ++k)
	{
		if (w[i] < w[j])
		{
			v[k] = w[i++];
		}
		else
		{
			v[k] = w[j--];
		}
	}
	free (w);
}

// bubble sort
void bubble (vector <int> v)
{
	bool flag = true;

	while (flag)
	{
		flag = false;

		for (unsigned int i = 0; i < v.size() - 1; i++)
		{
			if (v[i] > v[i+1])
			{
				flag = true;
				swap (v[i], v[i+1]);
			}
		}
	}
}

