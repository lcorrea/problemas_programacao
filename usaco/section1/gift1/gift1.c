/*
ID: lucas.c2
PROG: gift1
LANG: C
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
	char name[20];
	int money;
};

int main (void)
{
	FILE *fin = fopen ("gift1.in", "r");
	FILE *fout = fopen ("gift1.out", "w");

	struct person people[11];

	int n;
	
	fscanf (fin, "%d", &n);

	int i;
	
	for (i=0; i < n; i++)
	{
		fscanf (fin, "%s", people[i].name);
		people[i].money = 0;
	}

	for (i=0; i<n; i++)
	{
		char name[20];

		fscanf (fin, "%s", name);
		
		int p = 0;

		while (strcmp (people[p].name, name)) p++;

		int total, qnt;

		fscanf (fin, "%d %d", &total, &qnt);

		int j, frac = 0;

		if (qnt) frac = total / qnt;

		people[p].money -= frac*qnt;

		for (j=0; j < qnt; j++)
		{
			fscanf (fin, "%s", name);
			p = 0;
			while (strcmp(people[p].name, name)) p++;
			people[p].money += frac;
		}
	}

	for (i=0; i < n; i++)
		fprintf (fout, "%s %d\n", people[i].name, people[i].money);

	exit (0);
}

