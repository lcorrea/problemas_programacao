/*
ID: lucas.c2
PROG: milk2
LANG: C++
*/

#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	FILE *in, *out;
	int n, idle=0, continuous=0, start, end;
	pair <int, int> times[5010];

	in = fopen ("milk2.in", "r");
	out = fopen ("milk2.out", "w");

	fscanf (in, "%d", &n);

	for (int i = 0; i < n; i++)
	{
		fscanf (in, "%d %d", &start, &end);
		times[i] = make_pair (start, end);
	}

	sort (times, times + n);

	start = times[0].first;
	end = times[0].second;
	continuous = end - start;

	for (int i = 1; i < n; i++)
	{
		if (times[i].first <= end)
		{
			start = min (start, times[i].first);
			end = max (end, times[i].second);
			continuous = max (continuous, end - start);
		}
		else
		{
			idle = max (idle, times[i].first - end);
			start = times[i].first;
			end = times[i].second;
		}
	}

	fprintf (out, "%d %d\n", continuous, idle);

	exit (0);
}

