/*
ID: lucas.c2
PROG: friday
LANG: C
*/
#include <stdio.h>
#include <stdlib.h>

#define BASE_YEAR 1900

int main (void)
{
	FILE *fin = fopen ("friday.in", "r");
	FILE *fout = fopen ("friday.out", "w");

	int n, cnt, i, j, day, year;
	unsigned int days[7] = {0, 0, 0, 0, 0, 0, 0};
	unsigned int month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	year = BASE_YEAR;

	for (i=0;i<12;i++)
		month[i] %= 7;

	fscanf (fin, "%d", &n);

	cnt = 13 % 7;

	for (i=0; i <= n - 1; i++)
	{

		if ((!(year % 2) && !(year % 4) && (year % 100)) || !(year % 400))
			month[1]++;

		for (j=0; j < 12; j++)
		{
			days[cnt]++;
			cnt = (cnt + month[j]) % 7;
		}
		
		if (month[1])
			month[1] = 0;

		year++;
	}

	fprintf (fout, "%d", days[6]);
	for (i=0; i < 6; i++)
		fprintf (fout, " %d", days[i]);
	fprintf (fout, "\n");

	exit (0);
}

