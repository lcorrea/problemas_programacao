/*
ID: lucas.c2
PROG: beads
LANG: C
*/
#include <stdio.h>
#include <string.h>

#define max(a, b) ((a>b)?(a):(b))

int main (void)
{
	FILE *fin, *fout;
	int n, i, fpos, len, mBeads = 0, cnt = 0;
	char ch, str[800], necklace[400];

	fin = fopen ("beads.in", "r");
	fout = fopen ("beads.out", "w");

	fscanf (fin, "%d", &n);
	fscanf (fin, "%s", necklace);
	
	strcpy (str, necklace);
	strcat (str, necklace);

	len = 2*n;

	for (i=0; i < n; i++)
	{
		fpos = i;

		while (fpos < len && str[fpos] == 'w')
		{ 
			fpos++;
			cnt++;
		}

		if (fpos < len) ch = str[fpos];

		while (i < len && (str[fpos] == ch || str[fpos] == 'w'))
		{
			fpos++;
			cnt++;
		}

		if (fpos < len) ch = str[fpos];

		while (i < len && (str[fpos] == ch || str[fpos] == 'w'))
		{
			fpos++;
			cnt++;
		}

		mBeads = max (mBeads, cnt);
		cnt = 0;
		if (mBeads >= n) { mBeads = n; break; }
	}

	fprintf (fout, "%d\n", mBeads);

	return 0;
}

