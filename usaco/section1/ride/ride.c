/*
 ID: lucas.c2
 PROG: ride
 LANG: C
*/
#include <stdio.h>
#include <stdlib.h>

int main (void)
{
	FILE *fin = fopen ("ride.in", "r");
	FILE *fout = fopen ("ride.out", "w");
	char ufo[8], group[8], *ptr;
	const char cnte = 'A' - 1;
	int nUfo=1, nGroup=1;

	fscanf (fin, "%s\n%s", ufo, group);

	ptr = ufo;
	while (*ptr) nUfo *= *ptr++ - cnte;
	nUfo %= 47;

	ptr = group;
	while (*ptr) nGroup *= *ptr++ - cnte;
	nGroup %= 47;

	fputs((nUfo==nGroup) ? ("GO\n") : ("STAY\n"), fout);

	exit (0);
}

