#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	int n, t;
	priority_queue < pair <int, string> > alunos;
	pair <string, int> aluno;

	cin >> n >> t;

	for (int i = 0; i < n; i++)
	{
		cin >> aluno.first >> aluno.second;
		alunos.push (make_pair(aluno.second, aluno.first));
	}

	vector < vector <string> > times (t);
	int i = 0;

	while (!alunos.empty())
	{
		times[i%t].push_back (alunos.top().second);
		i++;
		alunos.pop();
	}

	for (int i = 0; i < t; i++)
	{
		cout << "Time " << i+1 << endl;
		sort (times[i].begin(), times[i].end());

		for (int j = 0, sz = times[i].size(); j < sz; j++)
			cout << times[i][j] << endl;
		cout << '\n';
	}

	return 0;
}
// 100 pontos

