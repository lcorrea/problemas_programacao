#include <bits/stdc++.h>

using namespace std;

int main (void)
{
	queue <char> selec, classif;
	int n = 8;

	for (char i = 'A'; i <= 'P'; i++)
		selec.push (i);

	while (n)
	{
		for (int i = 0, a, b; i < n; i++)
		{
			cin >> a >> b;

			if (a > b)
				classif.push(selec.front());
			selec.pop();

			if (b > a)
				classif.push(selec.front());
			selec.pop();
		}

		while (!classif.empty())
			selec.push (classif.front()), classif.pop();

		n /= 2; // n >>= 2
	}

	cout << selec.front() << endl;

	return 0;
}
// 100 pontos

