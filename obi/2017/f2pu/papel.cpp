#include <bits/stdc++.h>

using namespace std;

int main (void)
{
    int n, last;
    vector <int> h;

    cin >> n;
    cin >> last;

    h.push_back(last);

    for (int i = 1, b; i < n; i++)
    {
        cin >> b;

        if (b != last)
            h.push_back(b), last = b;
    }

    vector <int> v;
    set <int> s;

    //procura os vales
    for (int i = 1, len = (int) h.size(); i < len; i++)
        if (i + 1 < len)
            if (h[i+1] >= h[i] and h[i-1] >= h[i])
                v.push_back(h[i]), s.insert(h[i]);

    sort (v.begin(), v.end());

    int maxc = 0;

    //se o corte for realizado na altura de cada vale, distinto
    //calcula quantos pedacos podem ser obitidos 
    for (auto i = s.begin(); i != s.end(); i++)
    {
        auto it = upper_bound(v.begin(), v.end(), *i);
        maxc = max (maxc, (int) (it - v.begin()));
        printf ("%d => %d\n", *i, (int) (it - v.begin()));
    }

    printf ("%d\n", (int) s.size() ? maxc + 2 : 2);

    return 0;
}

