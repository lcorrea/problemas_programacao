/*
	problem 2388
	who's the middle
	by lucas correa
*/
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(void)
{
	unsigned int cows;
	unsigned int milk;

	vector<unsigned int> milks;

	cin >> cows;

	while(cows--)
	{
		cin >> milk;
		milks.push_back(milk);
	}

	sort(milks.begin(), milks.end());

	cout << milks[milks.size()/2] << endl;
	
	return 0;
}

